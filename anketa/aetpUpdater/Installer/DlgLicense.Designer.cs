﻿namespace aetpUpdater.Installer
{
    partial class DlgLicense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTxtLicense = new System.Windows.Forms.RichTextBox();
            this.lblRequireAccept = new System.Windows.Forms.Label();
            this.pictureButton1 = new aetpUpdater.PictureButton();
            this.SuspendLayout();
            // 
            // richTxtLicense
            // 
            this.richTxtLicense.BackColor = System.Drawing.Color.White;
            this.richTxtLicense.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTxtLicense.Location = new System.Drawing.Point(45, 26);
            this.richTxtLicense.Name = "richTxtLicense";
            this.richTxtLicense.ReadOnly = true;
            this.richTxtLicense.Size = new System.Drawing.Size(567, 416);
            this.richTxtLicense.TabIndex = 1;
            this.richTxtLicense.Text = "";
            // 
            // lblRequireAccept
            // 
            this.lblRequireAccept.AutoSize = true;
            this.lblRequireAccept.BackColor = System.Drawing.Color.Transparent;
            this.lblRequireAccept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRequireAccept.ForeColor = System.Drawing.Color.White;
            this.lblRequireAccept.Location = new System.Drawing.Point(42, 484);
            this.lblRequireAccept.Name = "lblRequireAccept";
            this.lblRequireAccept.Size = new System.Drawing.Size(444, 32);
            this.lblRequireAccept.TabIndex = 2;
            this.lblRequireAccept.Text = "Для использования информационно-аналитической системы " + ApplicationName + "\r\nнеобходимо при" +
    "нять данное лицензионное соглашение.\r\n";
            // 
            // pictureButton1
            // 
            this.pictureButton1.BackColor = System.Drawing.Color.Transparent;
            this.pictureButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureButton1.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pictureButton1.ImageDisabled = null;
            this.pictureButton1.ImageDown = global::aetpUpdater.Properties.Resources.button_ok_down;
            this.pictureButton1.ImageOver = global::aetpUpdater.Properties.Resources.button_ok_over;
            this.pictureButton1.ImageUp = global::aetpUpdater.Properties.Resources.button_ok_up;
            this.pictureButton1.Location = new System.Drawing.Point(568, 484);
            this.pictureButton1.Name = "pictureButton1";
            this.pictureButton1.Size = new System.Drawing.Size(44, 48);
            this.pictureButton1.TabIndex = 0;
            this.pictureButton1.Click += new System.EventHandler(this.pictureButton1_Click);
            // 
            // DlgLicense
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::aetpUpdater.Properties.Resources.bg_lic;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(657, 544);
            this.Controls.Add(this.pictureButton1);
            this.Controls.Add(this.richTxtLicense);
            this.Controls.Add(this.lblRequireAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "DlgLicense";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Установка " + ApplicationName + " - Лицензионное соглашение";            
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTxtLicense;
        private System.Windows.Forms.Label lblRequireAccept;
        private PictureButton pictureButton1;
        private string ApplicationName = global::aetpUpdater.Properties.Resources.ApplicationName;

    }
}