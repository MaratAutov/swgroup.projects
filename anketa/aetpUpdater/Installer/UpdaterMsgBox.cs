﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using aetpUpdater.Utilities;

namespace aetpUpdater.Installer
{
    public partial class UpdaterMsgBox : AetpUpdaterForm
    {
        public UpdaterMsgBox(String title, String header, String message, String confirmationText, bool isChecked)
        {
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            InitializeComponent();
            WinGraphics.ApplyFormStopResizing(this);
            
            Text = title;
            lblHeader.Text = header;
            lblMessage.Text = message;
            if (!String.IsNullOrEmpty(confirmationText))
            {
                cbConfirmation.Text = confirmationText;
                cbConfirmation.Checked = isChecked;
            }
            else
                cbConfirmation.Visible = false;
        }

        public bool IsConfirmed
        {
            get
            {
                return cbConfirmation.Checked;
            }
        }

        private void pbtnYes_Click(object sender, EventArgs e)
        {
            // Hides this form, but does not calls Close();
            DialogResult = DialogResult.Yes;
        }

        private void pbtnNo_Click(object sender, EventArgs e)
        {
            // Hides this form, but does not calls Close();
            DialogResult = DialogResult.No;
        }

    }
}
