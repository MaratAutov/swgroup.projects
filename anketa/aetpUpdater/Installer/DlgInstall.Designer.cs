﻿namespace aetpUpdater.Installer
{
    partial class DlgInstall
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgInstall));
            this.fbd = new System.Windows.Forms.FolderBrowserDialog();
            this.ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.spInstallPages = new aetpUpdater.StackPanel();
            this.tabPageBegin = new System.Windows.Forms.TabPage();
            this.lblInstallerVersion = new System.Windows.Forms.Label();
            this.cheLicense = new System.Windows.Forms.CheckBox();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.linkLblLicense = new System.Windows.Forms.LinkLabel();
            this.btnBeginNext = new aetpUpdater.PictureButton();
            this.pbtnBeginExit = new aetpUpdater.PictureButton();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.linkLblConnection = new System.Windows.Forms.LinkLabel();
            this.pbtnSettingsBack = new aetpUpdater.PictureButton();
            this.pbtnSettingsNext = new aetpUpdater.PictureButton();
            this.pbtnSettingsExit = new aetpUpdater.PictureButton();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtInstallPath = new System.Windows.Forms.TextBox();
            this.cheRunOnComplete = new System.Windows.Forms.CheckBox();
            this.cheAddToStartMenu = new System.Windows.Forms.CheckBox();
            this.cheAddDesktopIcon = new System.Windows.Forms.CheckBox();
            this.pbSettingsVBar = new System.Windows.Forms.PictureBox();
            this.lblInstallPath = new System.Windows.Forms.Label();
            this.lblSettings = new System.Windows.Forms.Label();
            this.tabPageReview = new System.Windows.Forms.TabPage();
            this.picRunOnComplete = new System.Windows.Forms.PictureBox();
            this.picAddToStartMenu = new System.Windows.Forms.PictureBox();
            this.picAddDesktopIcon = new System.Windows.Forms.PictureBox();
            this.ptbnReviewBack = new aetpUpdater.PictureButton();
            this.pbtnReviewNext = new aetpUpdater.PictureButton();
            this.pbtnReviewExit = new aetpUpdater.PictureButton();
            this.lblInstallPathValue = new System.Windows.Forms.Label();
            this.pbReviewVBar = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblInstallPath1 = new System.Windows.Forms.Label();
            this.lblCheckSettings = new System.Windows.Forms.Label();
            this.tabPageProgress = new System.Windows.Forms.TabPage();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.pbtnProgressAbort = new aetpUpdater.PictureButton();
            this.lblProgressProcess = new System.Windows.Forms.Label();
            this.tabPageComplete = new System.Windows.Forms.TabPage();
            this.pbtnCompleteDone = new aetpUpdater.PictureButton();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblCompletePressDone = new System.Windows.Forms.Label();
            this.lblCompleteSuccessful = new System.Windows.Forms.Label();
            this.tabPageConnection = new System.Windows.Forms.TabPage();
            this.pbtnConnectionCheck = new aetpUpdater.PictureButton();
            this.label10 = new System.Windows.Forms.Label();
            this.ch_UseProxyAuth = new System.Windows.Forms.CheckBox();
            this.ch_UseProxy = new System.Windows.Forms.CheckBox();
            this.ProxyAuthTypeBox = new System.Windows.Forms.ComboBox();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.UserTextBox = new System.Windows.Forms.TextBox();
            this.ProxyPortNumBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ProxyUrlTextBox = new System.Windows.Forms.TextBox();
            this.ServerUrlComboBox = new System.Windows.Forms.ComboBox();
            this.pbtnConnectionOK = new aetpUpdater.PictureButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPageError = new System.Windows.Forms.TabPage();
            this.lblErrorHeader = new System.Windows.Forms.Label();
            this.pbtnErrorBack = new aetpUpdater.PictureButton();
            this.pbtnExitError = new aetpUpdater.PictureButton();
            this.pbErrorVBar = new System.Windows.Forms.PictureBox();
            this.lblErrMsg = new System.Windows.Forms.Label();
            this.pbErrorError = new System.Windows.Forms.PictureBox();
            this.backgroundInstaller = new aetpUpdater.Utilities.PausableBackgroundWorker();
            this.backgroundUninstaller = new aetpUpdater.Utilities.PausableBackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
            this.spInstallPages.SuspendLayout();
            this.tabPageBegin.SuspendLayout();
            this.tabPageSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSettingsVBar)).BeginInit();
            this.tabPageReview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRunOnComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAddToStartMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAddDesktopIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReviewVBar)).BeginInit();
            this.tabPageProgress.SuspendLayout();
            this.tabPageComplete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.tabPageConnection.SuspendLayout();
            this.tabPageError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbErrorVBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbErrorError)).BeginInit();
            this.SuspendLayout();
            // 
            // ErrorProvider
            // 
            this.ErrorProvider.ContainerControl = this;
            // 
            // spInstallPages
            // 
            this.spInstallPages.Controls.Add(this.tabPageBegin);
            this.spInstallPages.Controls.Add(this.tabPageSettings);
            this.spInstallPages.Controls.Add(this.tabPageReview);
            this.spInstallPages.Controls.Add(this.tabPageProgress);
            this.spInstallPages.Controls.Add(this.tabPageComplete);
            this.spInstallPages.Controls.Add(this.tabPageConnection);
            this.spInstallPages.Controls.Add(this.tabPageError);
            this.spInstallPages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spInstallPages.Location = new System.Drawing.Point(0, 0);
            this.spInstallPages.Name = "spInstallPages";
            this.spInstallPages.SelectedIndex = 0;
            this.spInstallPages.Size = new System.Drawing.Size(650, 350);
            this.spInstallPages.TabIndex = 0;
            this.spInstallPages.TabStop = false;
            this.spInstallPages.Selected += new System.Windows.Forms.TabControlEventHandler(this.spInstallPages_Selected);
            // 
            // tabPageBegin
            // 
            this.tabPageBegin.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageBegin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageBegin.Controls.Add(this.lblInstallerVersion);
            this.tabPageBegin.Controls.Add(this.cheLicense);
            this.tabPageBegin.Controls.Add(this.lblWelcome);
            this.tabPageBegin.Controls.Add(this.linkLblLicense);
            this.tabPageBegin.Controls.Add(this.btnBeginNext);
            this.tabPageBegin.Controls.Add(this.pbtnBeginExit);
            this.tabPageBegin.Location = new System.Drawing.Point(4, 22);
            this.tabPageBegin.Name = "tabPageBegin";
            this.tabPageBegin.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBegin.Size = new System.Drawing.Size(642, 324);
            this.tabPageBegin.TabIndex = 0;
            this.tabPageBegin.UseVisualStyleBackColor = true;
            // 
            // lblInstallerVersion
            // 
            this.lblInstallerVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInstallerVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblInstallerVersion.Location = new System.Drawing.Point(5, 208);
            this.lblInstallerVersion.Name = "lblInstallerVersion";
            this.lblInstallerVersion.Size = new System.Drawing.Size(595, 28);
            this.lblInstallerVersion.TabIndex = 19;
            this.lblInstallerVersion.Text = "Версия инсталлятора: 10.10.10.10";
            this.lblInstallerVersion.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // cheLicense
            // 
            this.cheLicense.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cheLicense.BackColor = System.Drawing.Color.Transparent;
            this.cheLicense.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cheLicense.ForeColor = System.Drawing.Color.White;
            this.cheLicense.Location = new System.Drawing.Point(45, 286);
            this.cheLicense.Name = "cheLicense";
            this.cheLicense.Size = new System.Drawing.Size(305, 25);
            this.cheLicense.TabIndex = 1;
            this.cheLicense.Text = "Принимаю условия лицензионного соглашения";
            this.cheLicense.UseVisualStyleBackColor = false;
            this.cheLicense.Visible = false;
            this.cheLicense.CheckedChanged += new System.EventHandler(this.cheLicense_CheckedChanged);
            // 
            // lblWelcome
            // 
            this.lblWelcome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.BackColor = System.Drawing.Color.Transparent;
            this.lblWelcome.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWelcome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblWelcome.Location = new System.Drawing.Point(34, 112);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(563, 38);
            this.lblWelcome.TabIndex = 17;
            this.lblWelcome.Text = "Рекомендуется закрыть все прочие приложения перед началом установки.\r\nНажимите «П" +
    "родолжить», чтобы перейти к следующему шагу.";
            // 
            // linkLblLicense
            // 
            this.linkLblLicense.ActiveLinkColor = System.Drawing.Color.DeepSkyBlue;
            this.linkLblLicense.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.linkLblLicense.AutoSize = true;
            this.linkLblLicense.BackColor = System.Drawing.Color.Transparent;
            this.linkLblLicense.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLblLicense.LinkColor = System.Drawing.Color.White;
            this.linkLblLicense.Location = new System.Drawing.Point(43, 265);
            this.linkLblLicense.Name = "linkLblLicense";
            this.linkLblLicense.Size = new System.Drawing.Size(161, 16);
            this.linkLblLicense.TabIndex = 0;
            this.linkLblLicense.TabStop = true;
            this.linkLblLicense.Text = "Прочитайте эту лицензию";
            this.linkLblLicense.Visible = false;
            this.linkLblLicense.VisitedLinkColor = System.Drawing.Color.White;
            this.linkLblLicense.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLblLicense_LinkClicked);
            // 
            // btnBeginNext
            // 
            this.btnBeginNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBeginNext.BackColor = System.Drawing.Color.Transparent;
            this.btnBeginNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBeginNext.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBeginNext.ImageDisabled = global::aetpUpdater.Properties.Resources.button_next_disabled;
            this.btnBeginNext.ImageDown = global::aetpUpdater.Properties.Resources.button_next_down;
            this.btnBeginNext.ImageOver = global::aetpUpdater.Properties.Resources.button_next_over;
            this.btnBeginNext.ImageUp = global::aetpUpdater.Properties.Resources.button_next_up;
            this.btnBeginNext.Location = new System.Drawing.Point(368, 263);
            this.btnBeginNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnBeginNext.Name = "btnBeginNext";
            this.btnBeginNext.Size = new System.Drawing.Size(173, 48);
            this.btnBeginNext.TabIndex = 2;
            this.btnBeginNext.Click += new System.EventHandler(this.btnBeginNext_Click);
            // 
            // pbtnBeginExit
            // 
            this.pbtnBeginExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnBeginExit.BackColor = System.Drawing.Color.Transparent;
            this.pbtnBeginExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnBeginExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.pbtnBeginExit.ImageDisabled = null;
            this.pbtnBeginExit.ImageDown = global::aetpUpdater.Properties.Resources.button_cancel_down;
            this.pbtnBeginExit.ImageOver = global::aetpUpdater.Properties.Resources.button_cancel_over;
            this.pbtnBeginExit.ImageUp = global::aetpUpdater.Properties.Resources.button_cancel_up;
            this.pbtnBeginExit.Location = new System.Drawing.Point(556, 263);
            this.pbtnBeginExit.Margin = new System.Windows.Forms.Padding(4);
            this.pbtnBeginExit.Name = "pbtnBeginExit";
            this.pbtnBeginExit.Size = new System.Drawing.Size(44, 49);
            this.pbtnBeginExit.TabIndex = 3;
            this.pbtnBeginExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageSettings.Controls.Add(this.linkLblConnection);
            this.tabPageSettings.Controls.Add(this.pbtnSettingsBack);
            this.tabPageSettings.Controls.Add(this.pbtnSettingsNext);
            this.tabPageSettings.Controls.Add(this.pbtnSettingsExit);
            this.tabPageSettings.Controls.Add(this.btnBrowse);
            this.tabPageSettings.Controls.Add(this.txtInstallPath);
            this.tabPageSettings.Controls.Add(this.cheRunOnComplete);
            this.tabPageSettings.Controls.Add(this.cheAddToStartMenu);
            this.tabPageSettings.Controls.Add(this.cheAddDesktopIcon);
            this.tabPageSettings.Controls.Add(this.pbSettingsVBar);
            this.tabPageSettings.Controls.Add(this.lblInstallPath);
            this.tabPageSettings.Controls.Add(this.lblSettings);
            this.tabPageSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSettings.Size = new System.Drawing.Size(642, 324);
            this.tabPageSettings.TabIndex = 1;
            this.tabPageSettings.UseVisualStyleBackColor = true;
            // 
            // linkLblConnection
            // 
            this.linkLblConnection.ActiveLinkColor = System.Drawing.Color.DeepSkyBlue;
            this.linkLblConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLblConnection.AutoSize = true;
            this.linkLblConnection.BackColor = System.Drawing.Color.Transparent;
            this.linkLblConnection.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLblConnection.LinkColor = System.Drawing.Color.White;
            this.linkLblConnection.Location = new System.Drawing.Point(18, 263);
            this.linkLblConnection.Name = "linkLblConnection";
            this.linkLblConnection.Size = new System.Drawing.Size(287, 16);
            this.linkLblConnection.TabIndex = 6;
            this.linkLblConnection.TabStop = true;
            this.linkLblConnection.Text = "Настройки подключения к серверу обновлений";
            this.linkLblConnection.VisitedLinkColor = System.Drawing.Color.White;
            this.linkLblConnection.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLblConnection_LinkClicked);
            // 
            // pbtnSettingsBack
            // 
            this.pbtnSettingsBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnSettingsBack.BackColor = System.Drawing.Color.Transparent;
            this.pbtnSettingsBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnSettingsBack.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnSettingsBack.ImageDisabled = null;
            this.pbtnSettingsBack.ImageDown = global::aetpUpdater.Properties.Resources.button_back_down;
            this.pbtnSettingsBack.ImageOver = global::aetpUpdater.Properties.Resources.button_back_over;
            this.pbtnSettingsBack.ImageUp = global::aetpUpdater.Properties.Resources.button_back_up;
            this.pbtnSettingsBack.Location = new System.Drawing.Point(307, 263);
            this.pbtnSettingsBack.Margin = new System.Windows.Forms.Padding(1);
            this.pbtnSettingsBack.Name = "pbtnSettingsBack";
            this.pbtnSettingsBack.Size = new System.Drawing.Size(44, 48);
            this.pbtnSettingsBack.TabIndex = 7;
            this.pbtnSettingsBack.Click += new System.EventHandler(this.pbtnSettingsBack_Click);
            // 
            // pbtnSettingsNext
            // 
            this.pbtnSettingsNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnSettingsNext.BackColor = System.Drawing.Color.Transparent;
            this.pbtnSettingsNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnSettingsNext.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnSettingsNext.ImageDisabled = global::aetpUpdater.Properties.Resources.button_next_disabled;
            this.pbtnSettingsNext.ImageDown = global::aetpUpdater.Properties.Resources.button_next_down;
            this.pbtnSettingsNext.ImageOver = global::aetpUpdater.Properties.Resources.button_next_over;
            this.pbtnSettingsNext.ImageUp = global::aetpUpdater.Properties.Resources.button_next_up;
            this.pbtnSettingsNext.Location = new System.Drawing.Point(368, 263);
            this.pbtnSettingsNext.Margin = new System.Windows.Forms.Padding(1);
            this.pbtnSettingsNext.Name = "pbtnSettingsNext";
            this.pbtnSettingsNext.Size = new System.Drawing.Size(173, 48);
            this.pbtnSettingsNext.TabIndex = 0;
            this.pbtnSettingsNext.Click += new System.EventHandler(this.pbtnSettingsNext_Click);
            // 
            // pbtnSettingsExit
            // 
            this.pbtnSettingsExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnSettingsExit.BackColor = System.Drawing.Color.Transparent;
            this.pbtnSettingsExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnSettingsExit.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnSettingsExit.ImageDisabled = null;
            this.pbtnSettingsExit.ImageDown = global::aetpUpdater.Properties.Resources.button_cancel_down;
            this.pbtnSettingsExit.ImageOver = global::aetpUpdater.Properties.Resources.button_cancel_over;
            this.pbtnSettingsExit.ImageUp = global::aetpUpdater.Properties.Resources.button_cancel_up;
            this.pbtnSettingsExit.Location = new System.Drawing.Point(556, 263);
            this.pbtnSettingsExit.Margin = new System.Windows.Forms.Padding(4);
            this.pbtnSettingsExit.Name = "pbtnSettingsExit";
            this.pbtnSettingsExit.Size = new System.Drawing.Size(44, 48);
            this.pbtnSettingsExit.TabIndex = 8;
            this.pbtnSettingsExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(530, 136);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Обзор";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtInstallPath
            // 
            this.txtInstallPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInstallPath.Location = new System.Drawing.Point(156, 138);
            this.txtInstallPath.Name = "txtInstallPath";
            this.txtInstallPath.Size = new System.Drawing.Size(353, 20);
            this.txtInstallPath.TabIndex = 1;
            // 
            // cheRunOnComplete
            // 
            this.cheRunOnComplete.AutoSize = true;
            this.cheRunOnComplete.BackColor = System.Drawing.Color.Transparent;
            this.cheRunOnComplete.Checked = true;
            this.cheRunOnComplete.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cheRunOnComplete.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cheRunOnComplete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.cheRunOnComplete.Location = new System.Drawing.Point(46, 228);
            this.cheRunOnComplete.Name = "cheRunOnComplete";
            this.cheRunOnComplete.Size = new System.Drawing.Size(264, 20);
            this.cheRunOnComplete.TabIndex = 5;
            this.cheRunOnComplete.Text = "Запустить приложение после установки";
            this.cheRunOnComplete.UseVisualStyleBackColor = false;
            // 
            // cheAddToStartMenu
            // 
            this.cheAddToStartMenu.AutoSize = true;
            this.cheAddToStartMenu.BackColor = System.Drawing.Color.Transparent;
            this.cheAddToStartMenu.Checked = true;
            this.cheAddToStartMenu.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cheAddToStartMenu.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cheAddToStartMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.cheAddToStartMenu.Location = new System.Drawing.Point(46, 202);
            this.cheAddToStartMenu.Name = "cheAddToStartMenu";
            this.cheAddToStartMenu.Size = new System.Drawing.Size(206, 20);
            this.cheAddToStartMenu.TabIndex = 4;
            this.cheAddToStartMenu.Text = "Создать ярлык в меню «Пуск»";
            this.cheAddToStartMenu.UseVisualStyleBackColor = false;
            // 
            // cheAddDesktopIcon
            // 
            this.cheAddDesktopIcon.AutoSize = true;
            this.cheAddDesktopIcon.BackColor = System.Drawing.Color.Transparent;
            this.cheAddDesktopIcon.Checked = true;
            this.cheAddDesktopIcon.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cheAddDesktopIcon.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cheAddDesktopIcon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.cheAddDesktopIcon.Location = new System.Drawing.Point(46, 176);
            this.cheAddDesktopIcon.Name = "cheAddDesktopIcon";
            this.cheAddDesktopIcon.Size = new System.Drawing.Size(224, 20);
            this.cheAddDesktopIcon.TabIndex = 3;
            this.cheAddDesktopIcon.Text = "Создать ярлык на рабочем столе";
            this.cheAddDesktopIcon.UseVisualStyleBackColor = false;
            // 
            // pbSettingsVBar
            // 
            this.pbSettingsVBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbSettingsVBar.BackColor = System.Drawing.Color.Transparent;
            this.pbSettingsVBar.Location = new System.Drawing.Point(352, 263);
            this.pbSettingsVBar.Margin = new System.Windows.Forms.Padding(0);
            this.pbSettingsVBar.Name = "pbSettingsVBar";
            this.pbSettingsVBar.Size = new System.Drawing.Size(15, 48);
            this.pbSettingsVBar.TabIndex = 18;
            this.pbSettingsVBar.TabStop = false;
            // 
            // lblInstallPath
            // 
            this.lblInstallPath.BackColor = System.Drawing.Color.Transparent;
            this.lblInstallPath.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblInstallPath.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblInstallPath.Location = new System.Drawing.Point(43, 138);
            this.lblInstallPath.Name = "lblInstallPath";
            this.lblInstallPath.Size = new System.Drawing.Size(109, 21);
            this.lblInstallPath.TabIndex = 16;
            this.lblInstallPath.Text = "Путь установки:";
            // 
            // lblSettings
            // 
            this.lblSettings.AutoSize = true;
            this.lblSettings.BackColor = System.Drawing.Color.Transparent;
            this.lblSettings.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblSettings.Location = new System.Drawing.Point(42, 112);
            this.lblSettings.Name = "lblSettings";
            this.lblSettings.Size = new System.Drawing.Size(91, 19);
            this.lblSettings.TabIndex = 10;
            this.lblSettings.Text = "Параметры";
            // 
            // tabPageReview
            // 
            this.tabPageReview.BackColor = System.Drawing.Color.Transparent;
            this.tabPageReview.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageReview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageReview.Controls.Add(this.picRunOnComplete);
            this.tabPageReview.Controls.Add(this.picAddToStartMenu);
            this.tabPageReview.Controls.Add(this.picAddDesktopIcon);
            this.tabPageReview.Controls.Add(this.ptbnReviewBack);
            this.tabPageReview.Controls.Add(this.pbtnReviewNext);
            this.tabPageReview.Controls.Add(this.pbtnReviewExit);
            this.tabPageReview.Controls.Add(this.lblInstallPathValue);
            this.tabPageReview.Controls.Add(this.pbReviewVBar);
            this.tabPageReview.Controls.Add(this.label7);
            this.tabPageReview.Controls.Add(this.label6);
            this.tabPageReview.Controls.Add(this.label5);
            this.tabPageReview.Controls.Add(this.lblInstallPath1);
            this.tabPageReview.Controls.Add(this.lblCheckSettings);
            this.tabPageReview.Location = new System.Drawing.Point(4, 22);
            this.tabPageReview.Name = "tabPageReview";
            this.tabPageReview.Size = new System.Drawing.Size(642, 324);
            this.tabPageReview.TabIndex = 2;
            // 
            // picRunOnComplete
            // 
            this.picRunOnComplete.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.picRunOnComplete.Image = global::aetpUpdater.Properties.Resources.tick;
            this.picRunOnComplete.Location = new System.Drawing.Point(294, 228);
            this.picRunOnComplete.Name = "picRunOnComplete";
            this.picRunOnComplete.Size = new System.Drawing.Size(16, 16);
            this.picRunOnComplete.TabIndex = 40;
            this.picRunOnComplete.TabStop = false;
            // 
            // picAddToStartMenu
            // 
            this.picAddToStartMenu.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.picAddToStartMenu.Image = global::aetpUpdater.Properties.Resources.tick;
            this.picAddToStartMenu.Location = new System.Drawing.Point(294, 202);
            this.picAddToStartMenu.Name = "picAddToStartMenu";
            this.picAddToStartMenu.Size = new System.Drawing.Size(16, 16);
            this.picAddToStartMenu.TabIndex = 39;
            this.picAddToStartMenu.TabStop = false;
            // 
            // picAddDesktopIcon
            // 
            this.picAddDesktopIcon.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.picAddDesktopIcon.Image = global::aetpUpdater.Properties.Resources.tick;
            this.picAddDesktopIcon.Location = new System.Drawing.Point(294, 176);
            this.picAddDesktopIcon.Name = "picAddDesktopIcon";
            this.picAddDesktopIcon.Size = new System.Drawing.Size(16, 16);
            this.picAddDesktopIcon.TabIndex = 38;
            this.picAddDesktopIcon.TabStop = false;
            // 
            // ptbnReviewBack
            // 
            this.ptbnReviewBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ptbnReviewBack.BackColor = System.Drawing.Color.Transparent;
            this.ptbnReviewBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbnReviewBack.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ptbnReviewBack.ImageDisabled = null;
            this.ptbnReviewBack.ImageDown = global::aetpUpdater.Properties.Resources.button_back_down;
            this.ptbnReviewBack.ImageOver = global::aetpUpdater.Properties.Resources.button_back_over;
            this.ptbnReviewBack.ImageUp = global::aetpUpdater.Properties.Resources.button_back_up;
            this.ptbnReviewBack.Location = new System.Drawing.Point(307, 263);
            this.ptbnReviewBack.Margin = new System.Windows.Forms.Padding(1);
            this.ptbnReviewBack.Name = "ptbnReviewBack";
            this.ptbnReviewBack.Size = new System.Drawing.Size(44, 48);
            this.ptbnReviewBack.TabIndex = 1;
            this.ptbnReviewBack.Click += new System.EventHandler(this.ptbnReviewBack_Click);
            // 
            // pbtnReviewNext
            // 
            this.pbtnReviewNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnReviewNext.BackColor = System.Drawing.Color.Transparent;
            this.pbtnReviewNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnReviewNext.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnReviewNext.ImageDisabled = global::aetpUpdater.Properties.Resources.button_next_disabled;
            this.pbtnReviewNext.ImageDown = global::aetpUpdater.Properties.Resources.button_next_down;
            this.pbtnReviewNext.ImageOver = global::aetpUpdater.Properties.Resources.button_next_over;
            this.pbtnReviewNext.ImageUp = global::aetpUpdater.Properties.Resources.button_next_up;
            this.pbtnReviewNext.Location = new System.Drawing.Point(368, 263);
            this.pbtnReviewNext.Margin = new System.Windows.Forms.Padding(1);
            this.pbtnReviewNext.Name = "pbtnReviewNext";
            this.pbtnReviewNext.Size = new System.Drawing.Size(173, 48);
            this.pbtnReviewNext.TabIndex = 0;
            this.pbtnReviewNext.Click += new System.EventHandler(this.pbtnReviewNext_Click);
            // 
            // pbtnReviewExit
            // 
            this.pbtnReviewExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnReviewExit.BackColor = System.Drawing.Color.Transparent;
            this.pbtnReviewExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnReviewExit.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnReviewExit.ImageDisabled = null;
            this.pbtnReviewExit.ImageDown = global::aetpUpdater.Properties.Resources.button_cancel_down;
            this.pbtnReviewExit.ImageOver = global::aetpUpdater.Properties.Resources.button_cancel_over;
            this.pbtnReviewExit.ImageUp = global::aetpUpdater.Properties.Resources.button_cancel_up;
            this.pbtnReviewExit.Location = new System.Drawing.Point(556, 263);
            this.pbtnReviewExit.Margin = new System.Windows.Forms.Padding(4);
            this.pbtnReviewExit.Name = "pbtnReviewExit";
            this.pbtnReviewExit.Size = new System.Drawing.Size(44, 48);
            this.pbtnReviewExit.TabIndex = 3;
            this.pbtnReviewExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblInstallPathValue
            // 
            this.lblInstallPathValue.AutoSize = true;
            this.lblInstallPathValue.BackColor = System.Drawing.Color.Transparent;
            this.lblInstallPathValue.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblInstallPathValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblInstallPathValue.Location = new System.Drawing.Point(156, 131);
            this.lblInstallPathValue.Name = "lblInstallPathValue";
            this.lblInstallPathValue.Size = new System.Drawing.Size(36, 16);
            this.lblInstallPathValue.TabIndex = 34;
            this.lblInstallPathValue.Text = "sdfdf";
            // 
            // pbReviewVBar
            // 
            this.pbReviewVBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbReviewVBar.BackColor = System.Drawing.Color.Transparent;
            this.pbReviewVBar.Location = new System.Drawing.Point(352, 263);
            this.pbReviewVBar.Margin = new System.Windows.Forms.Padding(0);
            this.pbReviewVBar.Name = "pbReviewVBar";
            this.pbReviewVBar.Size = new System.Drawing.Size(15, 48);
            this.pbReviewVBar.TabIndex = 33;
            this.pbReviewVBar.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.label7.Location = new System.Drawing.Point(43, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(245, 16);
            this.label7.TabIndex = 29;
            this.label7.Text = "Запустить приложение после установки";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.label6.Location = new System.Drawing.Point(43, 202);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(187, 16);
            this.label6.TabIndex = 28;
            this.label6.Text = "Создать ярлык в меню «Пуск»";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.label5.Location = new System.Drawing.Point(43, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(205, 16);
            this.label5.TabIndex = 27;
            this.label5.Text = "Создать ярлык на рабочем столе";
            // 
            // lblInstallPath1
            // 
            this.lblInstallPath1.AutoSize = true;
            this.lblInstallPath1.BackColor = System.Drawing.Color.Transparent;
            this.lblInstallPath1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblInstallPath1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblInstallPath1.Location = new System.Drawing.Point(43, 131);
            this.lblInstallPath1.Name = "lblInstallPath1";
            this.lblInstallPath1.Size = new System.Drawing.Size(103, 16);
            this.lblInstallPath1.TabIndex = 26;
            this.lblInstallPath1.Text = "Путь установки:";
            // 
            // lblCheckSettings
            // 
            this.lblCheckSettings.AutoSize = true;
            this.lblCheckSettings.BackColor = System.Drawing.Color.Transparent;
            this.lblCheckSettings.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCheckSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblCheckSettings.Location = new System.Drawing.Point(42, 112);
            this.lblCheckSettings.Name = "lblCheckSettings";
            this.lblCheckSettings.Size = new System.Drawing.Size(172, 19);
            this.lblCheckSettings.TabIndex = 25;
            this.lblCheckSettings.Text = "Проверьте параметры";
            // 
            // tabPageProgress
            // 
            this.tabPageProgress.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageProgress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageProgress.Controls.Add(this.progressBar1);
            this.tabPageProgress.Controls.Add(this.pbtnProgressAbort);
            this.tabPageProgress.Controls.Add(this.lblProgressProcess);
            this.tabPageProgress.Location = new System.Drawing.Point(4, 22);
            this.tabPageProgress.Name = "tabPageProgress";
            this.tabPageProgress.Size = new System.Drawing.Size(642, 324);
            this.tabPageProgress.TabIndex = 3;
            this.tabPageProgress.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(46, 150);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(554, 38);
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 26;
            // 
            // pbtnProgressAbort
            // 
            this.pbtnProgressAbort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnProgressAbort.BackColor = System.Drawing.Color.Transparent;
            this.pbtnProgressAbort.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnProgressAbort.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnProgressAbort.ImageDisabled = null;
            this.pbtnProgressAbort.ImageDown = global::aetpUpdater.Properties.Resources.button_cancel_down;
            this.pbtnProgressAbort.ImageOver = global::aetpUpdater.Properties.Resources.button_cancel_over;
            this.pbtnProgressAbort.ImageUp = global::aetpUpdater.Properties.Resources.button_cancel_up;
            this.pbtnProgressAbort.Location = new System.Drawing.Point(556, 263);
            this.pbtnProgressAbort.Margin = new System.Windows.Forms.Padding(4);
            this.pbtnProgressAbort.Name = "pbtnProgressAbort";
            this.pbtnProgressAbort.Size = new System.Drawing.Size(44, 48);
            this.pbtnProgressAbort.TabIndex = 0;
            this.pbtnProgressAbort.Click += new System.EventHandler(this.pbtnProgressAbort_Click);
            // 
            // lblProgressProcess
            // 
            this.lblProgressProcess.AutoSize = true;
            this.lblProgressProcess.BackColor = System.Drawing.Color.Transparent;
            this.lblProgressProcess.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProgressProcess.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblProgressProcess.Location = new System.Drawing.Point(42, 112);
            this.lblProgressProcess.Name = "lblProgressProcess";
            this.lblProgressProcess.Size = new System.Drawing.Size(148, 19);
            this.lblProgressProcess.TabIndex = 3;
            this.lblProgressProcess.Text = "Процесс установки";
            // 
            // tabPageComplete
            // 
            this.tabPageComplete.BackColor = System.Drawing.Color.Transparent;
            this.tabPageComplete.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageComplete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageComplete.Controls.Add(this.pbtnCompleteDone);
            this.tabPageComplete.Controls.Add(this.pictureBox6);
            this.tabPageComplete.Controls.Add(this.lblCompletePressDone);
            this.tabPageComplete.Controls.Add(this.lblCompleteSuccessful);
            this.tabPageComplete.Location = new System.Drawing.Point(4, 22);
            this.tabPageComplete.Name = "tabPageComplete";
            this.tabPageComplete.Size = new System.Drawing.Size(642, 324);
            this.tabPageComplete.TabIndex = 4;
            // 
            // pbtnCompleteDone
            // 
            this.pbtnCompleteDone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnCompleteDone.BackColor = System.Drawing.Color.Transparent;
            this.pbtnCompleteDone.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnCompleteDone.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnCompleteDone.ImageDisabled = null;
            this.pbtnCompleteDone.ImageDown = global::aetpUpdater.Properties.Resources.button_finish_down;
            this.pbtnCompleteDone.ImageOver = global::aetpUpdater.Properties.Resources.button_finish_over;
            this.pbtnCompleteDone.ImageUp = global::aetpUpdater.Properties.Resources.button_finish_up;
            this.pbtnCompleteDone.Location = new System.Drawing.Point(442, 267);
            this.pbtnCompleteDone.Margin = new System.Windows.Forms.Padding(4);
            this.pbtnCompleteDone.Name = "pbtnCompleteDone";
            this.pbtnCompleteDone.Size = new System.Drawing.Size(103, 48);
            this.pbtnCompleteDone.TabIndex = 0;
            this.pbtnCompleteDone.Click += new System.EventHandler(this.pbtnCompleteDone_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.Image = global::aetpUpdater.Properties.Resources.ico_ok;
            this.pictureBox6.Location = new System.Drawing.Point(97, 123);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(48, 48);
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            // 
            // lblCompletePressDone
            // 
            this.lblCompletePressDone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCompletePressDone.AutoSize = true;
            this.lblCompletePressDone.BackColor = System.Drawing.Color.Transparent;
            this.lblCompletePressDone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCompletePressDone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblCompletePressDone.Location = new System.Drawing.Point(161, 145);
            this.lblCompletePressDone.Name = "lblCompletePressDone";
            this.lblCompletePressDone.Size = new System.Drawing.Size(384, 16);
            this.lblCompletePressDone.TabIndex = 4;
            this.lblCompletePressDone.Text = "Для завершения работы мастера установки нажмите «Готово»";
            // 
            // lblCompleteSuccessful
            // 
            this.lblCompleteSuccessful.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCompleteSuccessful.AutoSize = true;
            this.lblCompleteSuccessful.BackColor = System.Drawing.Color.Transparent;
            this.lblCompleteSuccessful.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCompleteSuccessful.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblCompleteSuccessful.Location = new System.Drawing.Point(161, 123);
            this.lblCompleteSuccessful.Name = "lblCompleteSuccessful";
            this.lblCompleteSuccessful.Size = new System.Drawing.Size(289, 19);
            this.lblCompleteSuccessful.TabIndex = 3;
            this.lblCompleteSuccessful.Text = "Процесс установки успешно завершен";
            // 
            // tabPageConnection
            // 
            this.tabPageConnection.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageConnection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageConnection.Controls.Add(this.pbtnConnectionCheck);
            this.tabPageConnection.Controls.Add(this.label10);
            this.tabPageConnection.Controls.Add(this.ch_UseProxyAuth);
            this.tabPageConnection.Controls.Add(this.ch_UseProxy);
            this.tabPageConnection.Controls.Add(this.ProxyAuthTypeBox);
            this.tabPageConnection.Controls.Add(this.PasswordTextBox);
            this.tabPageConnection.Controls.Add(this.UserTextBox);
            this.tabPageConnection.Controls.Add(this.ProxyPortNumBox);
            this.tabPageConnection.Controls.Add(this.label8);
            this.tabPageConnection.Controls.Add(this.ProxyUrlTextBox);
            this.tabPageConnection.Controls.Add(this.ServerUrlComboBox);
            this.tabPageConnection.Controls.Add(this.pbtnConnectionOK);
            this.tabPageConnection.Controls.Add(this.label9);
            this.tabPageConnection.Controls.Add(this.label4);
            this.tabPageConnection.Controls.Add(this.label3);
            this.tabPageConnection.Controls.Add(this.label2);
            this.tabPageConnection.Controls.Add(this.label1);
            this.tabPageConnection.Location = new System.Drawing.Point(4, 22);
            this.tabPageConnection.Name = "tabPageConnection";
            this.tabPageConnection.Size = new System.Drawing.Size(642, 324);
            this.tabPageConnection.TabIndex = 5;
            this.tabPageConnection.UseVisualStyleBackColor = true;
            // 
            // pbtnConnectionCheck
            // 
            this.pbtnConnectionCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pbtnConnectionCheck.BackColor = System.Drawing.Color.Transparent;
            this.pbtnConnectionCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnConnectionCheck.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnConnectionCheck.ImageDisabled = global::aetpUpdater.Properties.Resources.button_check_up;
            this.pbtnConnectionCheck.ImageDown = global::aetpUpdater.Properties.Resources.button_check_down;
            this.pbtnConnectionCheck.ImageOver = global::aetpUpdater.Properties.Resources.button_check_over;
            this.pbtnConnectionCheck.ImageUp = global::aetpUpdater.Properties.Resources.button_check_up;
            this.pbtnConnectionCheck.Location = new System.Drawing.Point(37, 263);
            this.pbtnConnectionCheck.Name = "pbtnConnectionCheck";
            this.pbtnConnectionCheck.Size = new System.Drawing.Size(166, 48);
            this.pbtnConnectionCheck.TabIndex = 9;
            this.pbtnConnectionCheck.Click += new System.EventHandler(this.pbtnConnectionCheck_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.label10.Location = new System.Drawing.Point(44, 104);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(363, 19);
            this.label10.TabIndex = 52;
            this.label10.Text = "Параметры подключения к серверу обновлений";
            // 
            // ch_UseProxyAuth
            // 
            this.ch_UseProxyAuth.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ch_UseProxyAuth.AutoSize = true;
            this.ch_UseProxyAuth.BackColor = System.Drawing.Color.Transparent;
            this.ch_UseProxyAuth.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.ch_UseProxyAuth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.ch_UseProxyAuth.Location = new System.Drawing.Point(402, 136);
            this.ch_UseProxyAuth.Name = "ch_UseProxyAuth";
            this.ch_UseProxyAuth.Size = new System.Drawing.Size(187, 20);
            this.ch_UseProxyAuth.TabIndex = 5;
            this.ch_UseProxyAuth.Text = "Аутентификация на прокси";
            this.ch_UseProxyAuth.UseVisualStyleBackColor = false;
            this.ch_UseProxyAuth.CheckedChanged += new System.EventHandler(this.ch_UseProxyAuth_CheckStateChanged);
            // 
            // ch_UseProxy
            // 
            this.ch_UseProxy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ch_UseProxy.AutoSize = true;
            this.ch_UseProxy.BackColor = System.Drawing.Color.Transparent;
            this.ch_UseProxy.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.ch_UseProxy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.ch_UseProxy.Location = new System.Drawing.Point(58, 146);
            this.ch_UseProxy.Name = "ch_UseProxy";
            this.ch_UseProxy.Size = new System.Drawing.Size(189, 20);
            this.ch_UseProxy.TabIndex = 1;
            this.ch_UseProxy.Text = "Подключение через прокси";
            this.ch_UseProxy.UseVisualStyleBackColor = false;
            this.ch_UseProxy.CheckedChanged += new System.EventHandler(this.ProxyCheckBox_CheckedChanged);
            // 
            // ProxyAuthTypeBox
            // 
            this.ProxyAuthTypeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ProxyAuthTypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ProxyAuthTypeBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ProxyAuthTypeBox.FormattingEnabled = true;
            this.ProxyAuthTypeBox.Items.AddRange(new object[] {
            "Basic",
            "Digest",
            "NTLM"});
            this.ProxyAuthTypeBox.Location = new System.Drawing.Point(448, 205);
            this.ProxyAuthTypeBox.Name = "ProxyAuthTypeBox";
            this.ProxyAuthTypeBox.Size = new System.Drawing.Size(121, 21);
            this.ProxyAuthTypeBox.TabIndex = 8;
            this.ProxyAuthTypeBox.SelectedValueChanged += new System.EventHandler(this.ProxyAuthTypeBox_SelectedValueChanged);
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PasswordTextBox.Location = new System.Drawing.Point(448, 178);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.PasswordChar = '*';
            this.PasswordTextBox.Size = new System.Drawing.Size(121, 20);
            this.PasswordTextBox.TabIndex = 7;
            // 
            // UserTextBox
            // 
            this.UserTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.UserTextBox.Location = new System.Drawing.Point(448, 150);
            this.UserTextBox.Name = "UserTextBox";
            this.UserTextBox.Size = new System.Drawing.Size(121, 20);
            this.UserTextBox.TabIndex = 6;
            // 
            // ProxyPortNumBox
            // 
            this.ProxyPortNumBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ProxyPortNumBox.Location = new System.Drawing.Point(116, 199);
            this.ProxyPortNumBox.Name = "ProxyPortNumBox";
            this.ProxyPortNumBox.Size = new System.Drawing.Size(70, 20);
            this.ProxyPortNumBox.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.label8.Location = new System.Drawing.Point(62, 203);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 16);
            this.label8.TabIndex = 44;
            this.label8.Text = "Порт:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ProxyUrlTextBox
            // 
            this.ProxyUrlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ProxyUrlTextBox.Location = new System.Drawing.Point(116, 172);
            this.ProxyUrlTextBox.Name = "ProxyUrlTextBox";
            this.ProxyUrlTextBox.Size = new System.Drawing.Size(131, 20);
            this.ProxyUrlTextBox.TabIndex = 2;
            // 
            // ServerUrlComboBox
            // 
            this.ServerUrlComboBox.FormattingEnabled = true;
            this.ServerUrlComboBox.Location = new System.Drawing.Point(107, 136);
            this.ServerUrlComboBox.Name = "ServerUrlComboBox";
            this.ServerUrlComboBox.Size = new System.Drawing.Size(226, 21);
            this.ServerUrlComboBox.TabIndex = 0;
            // 
            // pbtnConnectionOK
            // 
            this.pbtnConnectionOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnConnectionOK.BackColor = System.Drawing.Color.Transparent;
            this.pbtnConnectionOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnConnectionOK.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnConnectionOK.ImageDisabled = null;
            this.pbtnConnectionOK.ImageDown = global::aetpUpdater.Properties.Resources.button_ok_down;
            this.pbtnConnectionOK.ImageOver = global::aetpUpdater.Properties.Resources.button_ok_over;
            this.pbtnConnectionOK.ImageUp = global::aetpUpdater.Properties.Resources.button_ok_up;
            this.pbtnConnectionOK.Location = new System.Drawing.Point(556, 263);
            this.pbtnConnectionOK.Margin = new System.Windows.Forms.Padding(4);
            this.pbtnConnectionOK.Name = "pbtnConnectionOK";
            this.pbtnConnectionOK.Size = new System.Drawing.Size(44, 48);
            this.pbtnConnectionOK.TabIndex = 10;
            this.pbtnConnectionOK.Click += new System.EventHandler(this.pbtnConnectionOK_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.label9.Location = new System.Drawing.Point(412, 206);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 16);
            this.label9.TabIndex = 16;
            this.label9.Text = "Тип:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.label4.Location = new System.Drawing.Point(391, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "Пароль:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.label3.Location = new System.Drawing.Point(399, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 14;
            this.label3.Text = "Логин:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.label2.Location = new System.Drawing.Point(62, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 13;
            this.label2.Text = "Адрес:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.label1.Location = new System.Drawing.Point(57, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.TabIndex = 12;
            this.label1.Text = "Сервер";
            // 
            // tabPageError
            // 
            this.tabPageError.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageError.Controls.Add(this.lblErrorHeader);
            this.tabPageError.Controls.Add(this.pbtnErrorBack);
            this.tabPageError.Controls.Add(this.pbtnExitError);
            this.tabPageError.Controls.Add(this.pbErrorVBar);
            this.tabPageError.Controls.Add(this.lblErrMsg);
            this.tabPageError.Controls.Add(this.pbErrorError);
            this.tabPageError.Location = new System.Drawing.Point(4, 22);
            this.tabPageError.Margin = new System.Windows.Forms.Padding(1);
            this.tabPageError.Name = "tabPageError";
            this.tabPageError.Size = new System.Drawing.Size(642, 324);
            this.tabPageError.TabIndex = 6;
            this.tabPageError.UseVisualStyleBackColor = true;
            // 
            // lblErrorHeader
            // 
            this.lblErrorHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblErrorHeader.AutoSize = true;
            this.lblErrorHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblErrorHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblErrorHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblErrorHeader.Location = new System.Drawing.Point(195, 123);
            this.lblErrorHeader.Name = "lblErrorHeader";
            this.lblErrorHeader.Size = new System.Drawing.Size(300, 19);
            this.lblErrorHeader.TabIndex = 4;
            this.lblErrorHeader.Text = "В процессе установки возникла ошибка";
            // 
            // pbtnErrorBack
            // 
            this.pbtnErrorBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnErrorBack.BackColor = System.Drawing.Color.Transparent;
            this.pbtnErrorBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnErrorBack.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnErrorBack.ImageDisabled = null;
            this.pbtnErrorBack.ImageDown = global::aetpUpdater.Properties.Resources.button_back_down;
            this.pbtnErrorBack.ImageOver = global::aetpUpdater.Properties.Resources.button_back_over;
            this.pbtnErrorBack.ImageUp = global::aetpUpdater.Properties.Resources.button_back_up;
            this.pbtnErrorBack.Location = new System.Drawing.Point(497, 264);
            this.pbtnErrorBack.Margin = new System.Windows.Forms.Padding(1);
            this.pbtnErrorBack.Name = "pbtnErrorBack";
            this.pbtnErrorBack.Size = new System.Drawing.Size(44, 48);
            this.pbtnErrorBack.TabIndex = 0;
            this.pbtnErrorBack.Click += new System.EventHandler(this.pbtnErrorBack_Click);
            // 
            // pbtnExitError
            // 
            this.pbtnExitError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnExitError.BackColor = System.Drawing.Color.Transparent;
            this.pbtnExitError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnExitError.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnExitError.ImageDisabled = null;
            this.pbtnExitError.ImageDown = global::aetpUpdater.Properties.Resources.button_cancel_down;
            this.pbtnExitError.ImageOver = global::aetpUpdater.Properties.Resources.button_cancel_over;
            this.pbtnExitError.ImageUp = global::aetpUpdater.Properties.Resources.button_cancel_up;
            this.pbtnExitError.Location = new System.Drawing.Point(556, 264);
            this.pbtnExitError.Margin = new System.Windows.Forms.Padding(1);
            this.pbtnExitError.Name = "pbtnExitError";
            this.pbtnExitError.Size = new System.Drawing.Size(44, 49);
            this.pbtnExitError.TabIndex = 1;
            this.pbtnExitError.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pbErrorVBar
            // 
            this.pbErrorVBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbErrorVBar.BackColor = System.Drawing.Color.Transparent;
            this.pbErrorVBar.Location = new System.Drawing.Point(542, 264);
            this.pbErrorVBar.Margin = new System.Windows.Forms.Padding(0);
            this.pbErrorVBar.Name = "pbErrorVBar";
            this.pbErrorVBar.Size = new System.Drawing.Size(13, 48);
            this.pbErrorVBar.TabIndex = 38;
            this.pbErrorVBar.TabStop = false;
            // 
            // lblErrMsg
            // 
            this.lblErrMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblErrMsg.BackColor = System.Drawing.Color.Transparent;
            this.lblErrMsg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblErrMsg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblErrMsg.Location = new System.Drawing.Point(195, 145);
            this.lblErrMsg.Name = "lblErrMsg";
            this.lblErrMsg.Size = new System.Drawing.Size(299, 127);
            this.lblErrMsg.TabIndex = 5;
            this.lblErrMsg.Text = "Не могу создать каталог";
            // 
            // pbErrorError
            // 
            this.pbErrorError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pbErrorError.BackColor = System.Drawing.Color.Transparent;
            this.pbErrorError.Image = global::aetpUpdater.Properties.Resources.ico32_err;
            this.pbErrorError.Location = new System.Drawing.Point(148, 124);
            this.pbErrorError.Name = "pbErrorError";
            this.pbErrorError.Size = new System.Drawing.Size(32, 32);
            this.pbErrorError.TabIndex = 3;
            this.pbErrorError.TabStop = false;
            // 
            // backgroundInstaller
            // 
            this.backgroundInstaller.WorkerReportsProgress = true;
            this.backgroundInstaller.WorkerSupportsCancellation = true;
            this.backgroundInstaller.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundInstaller_DoWork);
            this.backgroundInstaller.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundInstaller_ProgressChanged);
            this.backgroundInstaller.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundInstaller_RunWorkerCompleted);
            // 
            // backgroundUninstaller
            // 
            this.backgroundUninstaller.WorkerReportsProgress = true;
            this.backgroundUninstaller.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundUninstaller_DoWork);
            this.backgroundUninstaller.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundUninstaller_ProgressChanged);
            this.backgroundUninstaller.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundUninstaller_RunWorkerCompleted);
            // 
            // DlgInstall
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(650, 350);
            this.Controls.Add(this.spInstallPages);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DlgInstall";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Установка Анкета.Редактор";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DlgInstall_FormClosing);
            this.Load += new System.EventHandler(this.DlgInstall_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
            this.spInstallPages.ResumeLayout(false);
            this.tabPageBegin.ResumeLayout(false);
            this.tabPageBegin.PerformLayout();
            this.tabPageSettings.ResumeLayout(false);
            this.tabPageSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSettingsVBar)).EndInit();
            this.tabPageReview.ResumeLayout(false);
            this.tabPageReview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRunOnComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAddToStartMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAddDesktopIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReviewVBar)).EndInit();
            this.tabPageProgress.ResumeLayout(false);
            this.tabPageProgress.PerformLayout();
            this.tabPageComplete.ResumeLayout(false);
            this.tabPageComplete.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.tabPageConnection.ResumeLayout(false);
            this.tabPageConnection.PerformLayout();
            this.tabPageError.ResumeLayout(false);
            this.tabPageError.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbErrorVBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbErrorError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private StackPanel spInstallPages;
        private System.Windows.Forms.TabPage tabPageBegin;
        private System.Windows.Forms.TabPage tabPageSettings;
        private System.Windows.Forms.TabPage tabPageReview;
        private System.Windows.Forms.TabPage tabPageProgress;
        private System.Windows.Forms.TabPage tabPageComplete;
        private System.Windows.Forms.TabPage tabPageConnection;
        private System.Windows.Forms.TabPage tabPageError;
        private PictureButton btnBeginNext;
        private PictureButton pbtnBeginExit;
        private System.Windows.Forms.CheckBox cheLicense;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.LinkLabel linkLblLicense;
        private PictureButton pbtnSettingsBack;
        private PictureButton pbtnSettingsNext;
        private PictureButton pbtnSettingsExit;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtInstallPath;
        private System.Windows.Forms.CheckBox cheRunOnComplete;
        private System.Windows.Forms.CheckBox cheAddToStartMenu;
        private System.Windows.Forms.CheckBox cheAddDesktopIcon;
        private System.Windows.Forms.PictureBox pbSettingsVBar;
        private System.Windows.Forms.Label lblInstallPath;
        private System.Windows.Forms.Label lblSettings;
        private System.Windows.Forms.FolderBrowserDialog fbd;
        private aetpUpdater.Utilities.PausableBackgroundWorker backgroundInstaller;
        private PictureButton ptbnReviewBack;
        private PictureButton pbtnReviewNext;
        private PictureButton pbtnReviewExit;
        private System.Windows.Forms.Label lblInstallPathValue;
        private System.Windows.Forms.PictureBox pbReviewVBar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblInstallPath1;
        private System.Windows.Forms.Label lblCheckSettings;
        private System.Windows.Forms.ProgressBar progressBar1;
        private PictureButton pbtnProgressAbort;
        private System.Windows.Forms.Label lblProgressProcess;
        private PictureButton pbtnCompleteDone;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblCompletePressDone;
        private System.Windows.Forms.Label lblCompleteSuccessful;
        private PictureButton pbtnConnectionOK;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private PictureButton pbtnErrorBack;
        private PictureButton pbtnExitError;
        private System.Windows.Forms.PictureBox pbErrorVBar;
        private System.Windows.Forms.Label lblErrMsg;
        private System.Windows.Forms.Label lblErrorHeader;
        private System.Windows.Forms.PictureBox pbErrorError;
        private System.Windows.Forms.LinkLabel linkLblConnection;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox ch_UseProxyAuth;
        private System.Windows.Forms.CheckBox ch_UseProxy;
        private System.Windows.Forms.ComboBox ProxyAuthTypeBox;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.TextBox UserTextBox;
        private System.Windows.Forms.TextBox ProxyPortNumBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox ProxyUrlTextBox;
        private System.Windows.Forms.ComboBox ServerUrlComboBox;
        private System.Windows.Forms.ErrorProvider ErrorProvider;
        private System.Windows.Forms.PictureBox picAddDesktopIcon;
        private System.Windows.Forms.PictureBox picAddToStartMenu;
        private System.Windows.Forms.PictureBox picRunOnComplete;
        private System.Windows.Forms.Label lblInstallerVersion;
        private PictureButton pbtnConnectionCheck;
        private Utilities.PausableBackgroundWorker backgroundUninstaller;
    }
}

