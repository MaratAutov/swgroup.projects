﻿namespace aetpUpdater.Installer
{
    partial class UpdaterMsgBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pboxIcon = new System.Windows.Forms.PictureBox();
            this.lblHeader = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.cbConfirmation = new System.Windows.Forms.CheckBox();
            this.pbtnYes = new aetpUpdater.PictureButton();
            this.pbtnNo = new aetpUpdater.PictureButton();
            ((System.ComponentModel.ISupportInitialize)(this.pboxIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // pboxIcon
            // 
            this.pboxIcon.BackColor = System.Drawing.Color.Transparent;
            this.pboxIcon.Image = global::aetpUpdater.Properties.Resources.exclamation;
            this.pboxIcon.InitialImage = null;
            this.pboxIcon.Location = new System.Drawing.Point(49, 94);
            this.pboxIcon.Name = "pboxIcon";
            this.pboxIcon.Size = new System.Drawing.Size(32, 32);
            this.pboxIcon.TabIndex = 2;
            this.pboxIcon.TabStop = false;
            // 
            // lblHeader
            // 
            this.lblHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblHeader.Location = new System.Drawing.Point(87, 104);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(311, 22);
            this.lblHeader.TabIndex = 3;
            this.lblHeader.Text = "Header";
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblMessage.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblMessage.Location = new System.Drawing.Point(91, 129);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(311, 74);
            this.lblMessage.TabIndex = 4;
            this.lblMessage.Text = "Message";
            // 
            // cbConfirmation
            // 
            this.cbConfirmation.BackColor = System.Drawing.Color.Transparent;
            this.cbConfirmation.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cbConfirmation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.cbConfirmation.Location = new System.Drawing.Point(91, 129);
            this.cbConfirmation.Name = "cbConfirmation";
            this.cbConfirmation.Size = new System.Drawing.Size(311, 20);
            this.cbConfirmation.TabIndex = 5;
            this.cbConfirmation.Text = "Confirmation";
            this.cbConfirmation.UseVisualStyleBackColor = false;
            // 
            // pbtnYes
            // 
            this.pbtnYes.BackColor = System.Drawing.Color.Transparent;
            this.pbtnYes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnYes.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnYes.ImageDisabled = global::aetpUpdater.Properties.Resources.button_yes_up;
            this.pbtnYes.ImageDown = global::aetpUpdater.Properties.Resources.button_yes_down;
            this.pbtnYes.ImageOver = global::aetpUpdater.Properties.Resources.button_yes_over;
            this.pbtnYes.ImageUp = global::aetpUpdater.Properties.Resources.button_yes_up;
            this.pbtnYes.Location = new System.Drawing.Point(139, 219);
            this.pbtnYes.Name = "pbtnYes";
            this.pbtnYes.Size = new System.Drawing.Size(84, 48);
            this.pbtnYes.TabIndex = 0;
            this.pbtnYes.Click += new System.EventHandler(this.pbtnYes_Click);
            // 
            // pbtnNo
            // 
            this.pbtnNo.BackColor = System.Drawing.Color.Transparent;
            this.pbtnNo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnNo.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnNo.ImageDisabled = global::aetpUpdater.Properties.Resources.button_no_up;
            this.pbtnNo.ImageDown = global::aetpUpdater.Properties.Resources.button_no_down;
            this.pbtnNo.ImageOver = global::aetpUpdater.Properties.Resources.button_no_over;
            this.pbtnNo.ImageUp = global::aetpUpdater.Properties.Resources.button_no_up;
            this.pbtnNo.Location = new System.Drawing.Point(262, 219);
            this.pbtnNo.Name = "pbtnNo";
            this.pbtnNo.Size = new System.Drawing.Size(84, 48);
            this.pbtnNo.TabIndex = 1;
            this.pbtnNo.Click += new System.EventHandler(this.pbtnNo_Click);
            // 
            // UpdaterMsgBox
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::aetpUpdater.Properties.Resources.msg_bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(485, 310);
            this.ControlBox = false;
            this.Controls.Add(this.cbConfirmation);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.pboxIcon);
            this.Controls.Add(this.pbtnYes);
            this.Controls.Add(this.pbtnNo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdaterMsgBox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Message Box";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pboxIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureButton pbtnNo;
        private PictureButton pbtnYes;
        private System.Windows.Forms.PictureBox pboxIcon;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.CheckBox cbConfirmation;
    }
}