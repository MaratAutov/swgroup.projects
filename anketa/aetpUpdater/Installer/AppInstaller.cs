﻿using System;
using System.Security.Permissions; // For Registry 
using Microsoft.Win32;             // For Registry
using System.IO;                   // For Installing
using System.Windows.Forms;
using System.Diagnostics;
using System.Security.Principal; // For Aadministrator check

using aetpUpdater.Utilities;
using aetpUpdater.Updater;

namespace aetpUpdater.Installer
{
    static class AppInstaller
    {
        public static void Uninstall()
        {
            if (IsSeldon16Running())
            {
                //UpdaterMsgBox msgBox = new UpdaterMsgBox("Удаление Seldon 1.6", 
                MessageBox.Show("Запущен " + Properties.Resources.ApplicationName + @"\n"
                    + "Завершите приложение и повторите попытку",
                    "Удаление " + Properties.Resources.ApplicationName + @"");
                return;
            }

            if (aetpUpdater.Utilities.CLIParser.uninstall_confirmed == false)
            {
                using (UpdaterMsgBox msgBox = new UpdaterMsgBox("Удаление " + Properties.Resources.ApplicationName,
                                                                "Полное удаление системы " + Properties.Resources.ApplicationName,
                                                                "",
                                                                "Удалить файл настроек",
                                                                true))
                {
                    if (msgBox.ShowDialog() != DialogResult.Yes)
                        return;
                    
                    // Remove logs and settings if user has confirmed to do so.
                    if (msgBox.IsConfirmed)
                    {
                        try
                        {
                            // Proxy settings and user login
                            //File.Delete(Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"AETP\" + Properties.Resources.ApplicationName + @"\"), 
                            //                         ProxySettingsProvider.CONFIG_USER));
                            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "aetpAnketa");
                            if (Directory.Exists(path))
                                Directory.Delete(path, true);
                            //path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "rospotrebnadzor");
                            //path = Path.Combine(path, "config.xml");
                            //File.Delete(path);
                            // Log files created by aetpUpdater
                            //File.Delete(Path.Combine(UpdaterConfiguration.AppPath, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + @".log"));
                            //File.Delete(Path.Combine(UpdaterConfiguration.AppPath, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + @".error.log"));
                            // Log file created by Seldon
                            //File.Delete(Path.Combine(UpdaterConfiguration.AppPath, @"errorlog.txt"));
                            // Consulting user data
                            //File.Delete(Path.Combine(UpdaterConfiguration.AppPath, @"LocalDB.db"));
                            // Delete consulting data from other location. Note that Seldon 1.5 and Seldon 1.6 share the same file.
                        }
                        // Disable warning "The variable 'ex' is declared but never used"	
#pragma warning disable 0168
                        catch (Exception ex)
#pragma warning restore 0168
                        {
#if DEBUG
                            System.Windows.Forms.MessageBox.Show(ex.ToString(), "DEBUG: Uninstall()");
#endif
                        }
                    }
                }
            }

            if (Utilities.RegistryEditor.AetpUpdaterInstallLocation()
                == Application.StartupPath + "\\")
            {
                InitUninstall();
                return;
            }
            else
            {
                ProcessUninstall("");
            }
        }
        public static void InitUninstall()
        {
            string tmpdir = System.IO.Path.GetTempPath();
            string installLocation = Utilities.RegistryEditor.AetpUpdaterInstallLocation();
            string destFile = tmpdir + "\\" + ProgramInfo.ExecutableName();
            File.Copy(installLocation + ProgramInfo.ExecutableName(), destFile, true);
            File.Copy(installLocation + ProgramInfo.ExecutableName() + ".config", destFile + ".config", true);
                        
            string batchfile = tmpdir + @"\aetpUpdaterUninstall.bat";

            if (File.Exists(batchfile))
            {
                try
                {
                    File.Delete(batchfile);
                }
                catch {}
            }

            if (!File.Exists(batchfile))
            {
                using (StreamWriter sw = File.CreateText(batchfile))
                {
#if DEBUG
                    sw.WriteLine("echo on");
#else
                    sw.WriteLine("@echo off");
#endif
                    sw.WriteLine("echo uninstalling Seldon");
                    
                    //sw.WriteLine("choice /C Y /N /D Y /T 10"); //use -waitforpid to wait another process to terminate
                    sw.WriteLine("anketaUpdater /uninstall 1 /waitforpid " + Process.GetCurrentProcess().Id.ToString());
                    sw.WriteLine("choice /C Y /N /D Y /T 2");   // two seconds delay
                    sw.WriteLine("del " + ProgramInfo.ExecutableName() + " /F /Q");
                    sw.WriteLine("del " + ProgramInfo.ExecutableName() + ".config /F /Q");
                    sw.WriteLine("del " + batchfile + " /F /Q");
#if DEBUG
                    sw.WriteLine("pause");
#endif
                }
                ProcessStartInfo newproc = new ProcessStartInfo();
                newproc.FileName = batchfile;
#if DEBUG
#else
                newproc.UseShellExecute = false; // hide cmd window
                //newproc.WindowStyle = ProcessWindowStyle.Minimized;   // hide window
                newproc.CreateNoWindow = true;
#endif
                newproc.WorkingDirectory = tmpdir;
                Process.Start(newproc);
            }
            else // unable to create batch file. simply try to uninstall
            {
                ProcessStartInfo newproc = new ProcessStartInfo();
                newproc.FileName = destFile;
                newproc.Arguments = @"/uninstall";
                newproc.WorkingDirectory = tmpdir;
                Process.Start(newproc);
            }
            return;
        }
        public static void ProcessUninstall(string iloc)
        {
            DlgInstall dlg = new DlgInstall();
            dlg.ShowUninstall();
            
            string installLocation;
            if (String.IsNullOrEmpty(iloc))
                installLocation = Utilities.RegistryEditor.AetpUpdaterInstallLocation();
            else 
                installLocation = iloc;

            if (String.IsNullOrEmpty(installLocation))
                return;
#warning Uninstall as Administrator
            /*
            if (!IsPathWritable(installLocation)
                && !HasAdministratorPermissions())
            {
                RestartAsAdministrator();
            }
            */
            try
            {
#if buuu
                // graceful Uninstall
                string uninstfname = installLocation + @"\uninstall.dat";
                string fname;
                if (File.Exists(uninstfname))
                {
                    // remove files;
                    using (StreamReader file = new StreamReader(uninstfname))
                    {
                        while ((fname = file.ReadLine()) != null)
                        {
                            if (File.Exists(fname))
                            {
                                FileAttributes fas = File.GetAttributes(fname);
                                if ((fas & FileAttributes.Directory) != FileAttributes.Directory)
                                {
                                    File.Delete(fname);
                                }
                            }
                        }
                    }
                    // remove directories
#warning
                    using (StreamReader file = new StreamReader(uninstfname))
                    {
                        while ((fname = file.ReadLine()) != null)
                        {
                            if (Directory.Exists(fname))
                            {
                                FileAttributes fas = File.GetAttributes(fname);
                                if ((fas & FileAttributes.Directory) == FileAttributes.Directory)
                                {
                                    if (Directory.Exists(fname))
                                    {
                                        try
                                        {
                                            Directory.Delete(fname, false);
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                    }
                }

                if (File.Exists(installLocation + @"\uninstall.dat"))
                    File.Delete(installLocation + @"\uninstall.dat");


                if ((Directory.GetFiles(installLocation).Length == 0))
                {
                    Directory.Delete(Path.GetDirectoryName(installLocation), true);
                }
                else
                {
                    if (MessageBox.Show("В каталоге:\n" + installLocation + "\n"
                                            + "остались файлы. Удалить?",
                                            Properties.Resources.InfoMsg_ProgramName,
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        // Brute Uninstall
                        try
                        {
                            System.IO.Directory.Delete(Path.GetDirectoryName(installLocation), true);
                        }
                        catch { }
                    }
                }
#endif
                Utilities.RegistryEditor.UnregisterAetpUpdater();
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Произошла ошибка во время удаления\n"
                                    + ex.Message,
                                Properties.Resources.InfoMsg_ProgramName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Checks if current process can create files in specified directory
        /// of if can write to specified filename.
        /// </summary>
        /// <param name="dest"></param>
        /// <returns></returns>
        public static bool IsPathWritable(string destDir)
        {
            // Since we are going to write AetpUpdater.exe 
            // this filename will be used for testing
            string srcFile = Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);
            string destFile = destDir + "\\" + srcFile;
            try
            {
                if (File.Exists(destFile))
                {
                    using (FileStream fs = File.Open(destFile, FileMode.Open, FileAccess.ReadWrite))
                    {
                        fs.Close();
                    }
                }
                else
                {
                    File.Copy(srcFile, destFile, false);
                    using (FileStream fs = File.Open(destFile, FileMode.Open, FileAccess.ReadWrite))
                    {
                        fs.Close();
                    }
                    File.Delete(destFile);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool HasAdministratorPermissions()
        {
            try
            {
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                return principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch
            {
                return false;
            }
        }

        public static void RestartAsAdministrator()
        {
            string exeFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            ProcessStartInfo proc = new ProcessStartInfo();
            proc.UseShellExecute = true;
            //proc.WorkingDirectory = @"C:\Windows\System32\";
            proc.FileName = exeFileName;
            proc.Verb = "runas";
            Process.Start(proc);
        }

        public static bool IsSeldon16Running()
        {
            Process[] p = Process.GetProcessesByName(Properties.Resources.ExeName.Replace(".exe", ""));
            if (p.Length > 0)
                return true;
            else
                return false;
        }

        public static bool IsAppStartRequested { get; set; }
        public static bool IsInstalledSuccessful { get; set; }
    }
}

