﻿using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;
using System.Configuration;

using aetpUpdater.Updater; 
using aetpUpdater.Utilities;

namespace aetpUpdater.Installer
{
    public partial class DlgInstall : AetpUpdaterForm 
    {

        public DlgInstall()
        {
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            Text = "Установка " + Properties.Resources.ApplicationName;

            InitializeComponent();
            AppInstaller.IsInstalledSuccessful = false;

            WinGraphics.ApplyFormStopResizing(this);
            DialogResult = DialogResult.None;

            tabPageErrorReturnPage = null;
            isUpdateMode = false;
            newAppDir = null;
        }

        #region GUI

        #region Common
        private void DlgInstall_Load(object sender, EventArgs e)
        {
            lblInstallerVersion.Text = @"Версия инсталлятора: " + ProgramInfo.Version();

            txtInstallPath.Text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                               String.Format(@"AETP\{0}\", Properties.Resources.ApplicationName));

            if (Utilities.ProcessKiller.IsAnotherInstanceRunning())
            {
                //this.DialogResult = DialogResult.Abort;
                tabPageErrorReturnPage = null;
                spInstallPages.SelectedTab = tabPageError;
                lblErrMsg.Text = "Инсталлятор уже запущен. Завершите процесс и повторите попытку.";
                return;
            }

            // Proxy and Connection settings
            try
            {
                ServerUrlComboBox.Text = UpdaterConfiguration.Settings.ServerUpdaterUrl;
                if (string.IsNullOrEmpty(UpdaterConfiguration.Settings.ServerUpdaterUrl) == false)
                    this.ServerUrlComboBox.Items.Add(UpdaterConfiguration.Settings.ServerUpdaterUrl);

                if (
                    string.IsNullOrEmpty(ConfigurationManager.AppSettings["ServerUrl"]) == false
                    && UpdaterConfiguration.Settings.ServerUpdaterUrl != ConfigurationManager.AppSettings["ServerUrl"])
                {
                    this.ServerUrlComboBox.Items.Add(ConfigurationManager.AppSettings["ServerUrl"]);
                }

                string[] list = ((string)(UpdaterConfiguration.Settings.ServerList + "")).Split(";".ToCharArray());
                if (list.Length > 0)
                {
                    //this.ServerUrlComboBox.Items.Clear();

                    for (int i = 0; i < list.Length; i++)
                    {
                        if (string.IsNullOrEmpty(list[i]))
                            continue;

                        if (ServerUrlComboBox.Items.Contains(list[i]) == false)
                            ServerUrlComboBox.Items.Add(list[i]);
                    }
                }

                this.ch_UseProxy.Checked = UpdaterConfiguration.Settings.UseProxy;

                this.ProxyUrlTextBox.Text = UpdaterConfiguration.Settings.ProxyUrl;
                this.ProxyPortNumBox.Text = UpdaterConfiguration.Settings.ProxyPort;

                this.ch_UseProxyAuth.Checked = UpdaterConfiguration.Settings.UseProxyAuth;

                this.UserTextBox.Text = UpdaterConfiguration.Settings.ProxyUser;
                this.PasswordTextBox.Text = UpdaterConfiguration.Settings.ProxyPassword;

                int idx = ProxyAuthTypeBox.Items.IndexOf(UpdaterConfiguration.Settings.ProxyAuthType);
                if (idx != -1)
                    this.ProxyAuthTypeBox.SelectedItem = ProxyAuthTypeBox.Items[idx];
                else
                    this.ProxyAuthTypeBox.SelectedItem = ProxyAuthTypeBox.Items[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка\n" + ex.Message,
                "Настройка соединения",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            }
            //            label_Info.Visible = false;

            ProxyCheckBox_CheckedChanged(sender, e);
        }

        private void DlgInstall_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (spInstallPages.SelectedTab == tabPageBegin ||
                spInstallPages.SelectedTab == tabPageSettings ||
                spInstallPages.SelectedTab == tabPageReview ||
                spInstallPages.SelectedTab == tabPageError ||
                spInstallPages.SelectedTab == tabPageConnection
                // spInstallPages.SelectedTab == tabPageProgress || these two have
                // spInstallPages.SelectedTab == tabPageComplete || its own cleanup
                )
            {
                Cleanup();
            }
            else if (spInstallPages.SelectedTab == tabPageProgress)
            {
                this.backgroundInstaller.CancelAsync();
            }
            else // spInstallPages.SelectedTab == tabPageComplete
            {
                //Cleanup();
                //Application.Exit();
                //this.Close();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Cleanup();
            //            Application.Exit();
            this.Close();
        }

        private void spInstallPages_Selected(object sender, TabControlEventArgs e)
        {

            if (e.TabPage == tabPageSettings)
            {
                try
                {
                    if (String.IsNullOrEmpty(newAppDir) == false)
                    {
                        System.IO.Directory.Delete(newAppDir);
                    }
                }
                catch { } // simply ignore
            }
            else if (e.TabPage == tabPageReview)
            {
                // Update values on 'Review' page based on user selection on 'Settings' page.

                // Install Path
                lblInstallPathValue.Text = txtInstallPath.Text;

                // Run Application on Complete 
                if (cheRunOnComplete.Checked)
                {
                    picRunOnComplete.Image = Properties.Resources.tick;
                    AppInstaller.IsAppStartRequested = true;

                }
                else
                {
                    picRunOnComplete.Image = Properties.Resources.slash;
                    AppInstaller.IsAppStartRequested = false;
                }

                // Start Menu Icons
                if (cheAddToStartMenu.Checked)
                    picAddToStartMenu.Image = Properties.Resources.tick;
                else
                    picAddToStartMenu.Image = Properties.Resources.slash;

                // Desktop Icon
                if (cheAddDesktopIcon.Checked)
                    picAddDesktopIcon.Image = Properties.Resources.tick;
                else
                    picAddDesktopIcon.Image = Properties.Resources.slash;
            }
            else if (e.TabPage == tabPageError)
            {
                if (tabPageErrorReturnPage == null)
                {
                    pbtnErrorBack.Enabled = false;
                    pbtnErrorBack.Visible = false;
                    pbErrorVBar.Visible = false;
                }
                else
                {
                    pbtnErrorBack.Enabled = true;
                    pbtnErrorBack.Visible = true;
                    pbErrorVBar.Visible = true;
                }
            }
            //else if (e.TabPage == tabPageComplete)
            //{
            //    
            //}
        }
        #endregion Common

        #region Begin
        private void cheLicense_CheckedChanged(object sender, EventArgs e)
        {
            btnBeginNext.Enabled = cheLicense.Checked;
        }
        private void linkLblLicense_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DlgLicense dlg = new DlgLicense();
            dlg.ShowDialog();
        }
        private void btnBeginNext_Click(object sender, EventArgs e)
        {
            spInstallPages.SelectedTab = tabPageSettings;
        }
        #endregion Begin

        #region Settings
        private void btnBrowse_Click(object sender, EventArgs e)
        {

            if (fbd.ShowDialog(this) == DialogResult.OK)
                txtInstallPath.Text = fbd.SelectedPath;
        }
        private void linkLblConnection_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            spInstallPages.SelectedTab = tabPageConnection;
        }

        private void pbtnSettingsNext_Click(object sender, EventArgs e)
        {
            // Verify user input
            try
            {
                // normalize path
                string path = txtInstallPath.Text + "\\";
                path = System.IO.Path.GetFullPath(path);
                path = System.IO.Path.GetDirectoryName(path); // MSDN: GetDirectoryName() returns null if path contains root directory only
                if (path == null)  // path contains root directory only
                {
                    path = System.IO.Directory.GetDirectoryRoot(txtInstallPath.Text);
                }
                else
                {
                    path += "\\";
                }

                // verify path
                // MAX_PATH =  260 chars. MSDN: http://msdn.microsoft.com/en-us/library/aa365247.aspx
                // Currently the longest relative path for Seldon file is 70 bytes. Add 10 chars for future.
                // it was verified that Seldon 1.6 is unable to run from a very long path
                const int MAX_REL_PATH = 260 - 80;
                if (path.Length > MAX_REL_PATH)
                    throw new Exception(String.Format("Путь слишком длинный. Введите путь меньше {0} символов.", MAX_REL_PATH));

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                    newAppDir = path;
                }

                string srcFile = Utilities.ProgramInfo.ProcessFileName();
                string destFile = path + "\\" + Utilities.ProgramInfo.ExecutableName();

                // Test for write access

                if (System.IO.File.Exists(destFile))
                {
                    using (System.IO.FileStream fs = System.IO.File.Open(destFile, System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite))
                    {
                        fs.Close();
                    }
                }
                else
                {
                    try
                    {
                        System.IO.File.Copy(srcFile, destFile, false);
                        using (System.IO.FileStream fs = System.IO.File.Open(destFile, System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite))
                        {
                            fs.Close();
                        }
                        System.IO.File.Delete(destFile);
                    }
                    catch
                    {
                        if (System.IO.File.Exists(destFile))
                            System.IO.File.Delete(destFile);
                        throw;
                    }
                }
                txtInstallPath.Text = path;

                if (!IsLocalRepository())
                {
                    Settings tempSettings = GetCurrentSettings();
                    if( !Utilities.NetworkTester.TestConnection(ServerUrlComboBox.Text, tempSettings) )
                    {
                        NetworkTester.ResetGlobalProxyToSavedSettings();
                        throw new Exception("Не удается установить соединение с сервером. Проверьте настройки подключения к сети Интернет.");
                    }
                    else
                        SaveSettings();
                }

            }
            catch (Exception ex)
            {
                // clean up
                {
                    if (System.IO.Directory.Exists(newAppDir))
                    {
                        try
                        {
                            System.IO.Directory.Delete(newAppDir, true);
                        }
#warning ignore access error
                        catch { } // Ignore access error
                    }
                }
                // show error message to user
                lblErrMsg.Text = ex.Message;
                tabPageErrorReturnPage = tabPageSettings;
                spInstallPages.SelectedTab = tabPageError;
                return;
            }

            // Let user review the settings
            spInstallPages.SelectedTab = tabPageReview;
        }
        private void pbtnSettingsBack_Click(object sender, EventArgs e)
        {
            spInstallPages.SelectedTab = tabPageBegin;
        }
        #endregion Settings

        #region Review
        private void ptbnReviewBack_Click(object sender, EventArgs e)
        {
            spInstallPages.SelectedTab = tabPageSettings;
        }
        private void pbtnReviewNext_Click(object sender, EventArgs e)
        {
            spInstallPages.SelectedTab = tabPageProgress;
            ProcessInstall(txtInstallPath.Text);
        }
        #endregion Review

        #region Progress
        private void pbtnProgressAbort_Click(object sender, EventArgs e)
        {
            this.backgroundInstaller.Pause(); // need to call continue() even for cancelation
            if (MessageBox.Show("Отменить установку?",
                            "Установка " + Properties.Resources.ApplicationName,
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                this.backgroundInstaller.CancelAsync();
            }
            this.backgroundInstaller.Continue();
        }
        #endregion Progress

        #region Connection
        private void pbtnConnectionOK_Click(object sender, EventArgs e)
        {
            //            UpdaterConfiguration.Settings.ServerUrl = ServerUrlComboBox.Text;
            //            ServerUrlComboBox.Items.Add(ServerUrlComboBox.Text);
            //            ServerUrlComboBox.SelectedIndex = ServerUrlComboBox.Items.Count - 1;
            try
            {
                if (IsUserInputValid())
                {
                    if (!IsLocalRepository())
                    {
                        Settings tempSettings = GetCurrentSettings();
                        if( Utilities.NetworkTester.TestConnection(ServerUrlComboBox.Text, tempSettings) == false )
                        {
                            Utilities.NetworkTester.ResetGlobalProxyToSavedSettings();
                            throw new Exception("Не удается установить соединение с сервером. Проверьте настройки подключения к сети Интернет.");
                        }
                        else
                            SaveSettings();
                    }

                    SaveSettings();

                    if (isUpdateMode)
                    {
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    else
                    {
                        spInstallPages.SelectedTab = tabPageSettings;
                    }
                }
            }
            catch (Exception ex)
            {
                lblErrMsg.Text = ex.Message;
                tabPageErrorReturnPage = tabPageConnection;
                spInstallPages.SelectedTab = tabPageError;
                return;

            }

        }
        private void pbtnConnectionCheck_Click(object sender, EventArgs e)
        {
            if (!IsUserInputValid())
                return;

            if (IsLocalRepository())
                return;

            // Will save settings when user click OK/Accept button
            // SaveSettings();

            try
            {
                Settings tempSettings = GetCurrentSettings();
                if(Utilities.NetworkTester.TestConnection(ServerUrlComboBox.Text, tempSettings))
                {
                    SaveSettings();
                    MessageBox.Show(Properties.Resources.InfoMsg_ConnectionEstablished,
                                    Properties.Resources.InfoMsg_ConnectionTesting,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    //                    label_Info.Visible = false;
                }
                else
                {
                    MessageBox.Show(Properties.Resources.ErrMsg_ConnectionFailed + Environment.NewLine + NetworkTester.ErrorMessage,
                                    Properties.Resources.InfoMsg_ConnectionTesting,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    //                    label_Info.Visible = false;
                }

            }
            catch (Exception ex)
            {
                lblErrMsg.Text = ex.Message;
                tabPageErrorReturnPage = tabPageConnection;
                spInstallPages.SelectedTab = tabPageError;
                return;
            }
        }
        private void ch_UseProxyAuth_CheckStateChanged(object sender, EventArgs e)
        {
            if (ch_UseProxyAuth.Checked)
            {
                UserTextBox.Enabled = true;
                PasswordTextBox.Enabled = true;
                ProxyAuthTypeBox.Enabled = true;
            }
            else
            {
                UserTextBox.Enabled = false;
                PasswordTextBox.Enabled = false;
                ProxyAuthTypeBox.Enabled = false;
            }
        }
        /// <summary>
        /// Handles use proxy checkbox change
        /// </summary>
        private void ProxyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.ch_UseProxy.Checked == false)
            {
                ProxyUrlTextBox.Enabled = false;
                ProxyPortNumBox.Enabled = false;

                ch_UseProxyAuth.Enabled = false;

                UserTextBox.Enabled = false;
                PasswordTextBox.Enabled = false;
                ProxyAuthTypeBox.Enabled = false;
            }
            else
            {
                ProxyUrlTextBox.Enabled = true;
                ProxyPortNumBox.Enabled = true;

                ch_UseProxyAuth.Enabled = true;
                ch_UseProxyAuth_CheckStateChanged(sender, e);
            }
        }
        #endregion Connection

        #region Error
        private void pbtnErrorBack_Click(object sender, EventArgs e)
        {
            if (tabPageErrorReturnPage != null)
                spInstallPages.SelectedTab = tabPageErrorReturnPage;
        }
        #endregion Error

        #region Complete
        private void pbtnCompleteDone_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK; // Automatically hides the form, but do not call Close();
            this.Close();
        }
        #endregion Complete

        #region Methods

        public void EditConnectionSettings()
        {
            string frmText = this.Text;

            isUpdateMode = true;

            this.Text = @"Обновление " + Properties.Resources.ApplicationName;

            if (Utilities.NetworkTester.TestConnection() == false)
            {
                lblErrMsg.Text = @"Не удается установить соединение с сервером. Проверьте настройки подключения к сети Интернет.";
                lblErrorHeader.Text = @"В процессе обновления возникла ошибка";

                tabPageErrorReturnPage = tabPageConnection;
                spInstallPages.SelectedTab = tabPageError;
            }
            else
            {
                spInstallPages.SelectedTab = tabPageConnection;
            }

            ShowDialog();

            Text = Properties.Resources.InfoMsg_ProgramName;
            isUpdateMode = false;

        }

        private void Cleanup()
        {
            try
            {
                // clean up if the install process was not started yet
                if (AppInstaller.IsInstalledSuccessful == false)
                {
                    if (String.IsNullOrEmpty(newAppDir) == false)
                    {
                        if ((System.IO.Directory.GetFiles(newAppDir).Length == 0))
                        {
                            System.IO.Directory.Delete(System.IO.Path.GetDirectoryName(newAppDir), true);
                        }
                        else
                        {
#warning Brute uninstall on unxpected  users steps
                            UninstallList.Instance.Dispose();

                            if (MessageBox.Show("В каталоге:\n" + newAppDir + "\n"
                                                    + "остались файлы. Удалить?",
                                                    Properties.Resources.InfoMsg_ProgramName,
                                                MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question)
                                == DialogResult.Yes)
                            {
                                // Brute Clean up
                                System.IO.Directory.Delete(System.IO.Path.GetDirectoryName(newAppDir), true);
                            }
                            //AppInstaller.ProcessUninstall(newAppDir);
                            //backgroundUninstaller.RunWorkerAsync(newAppDir);
                        }
                    } // if (String.IsNullOrEmpty(newAppDir) == false)
                } // if (AppInstaller.IsInstalledSuccessful == false)
            }
            catch { } // simply ignore clean-up errors
        }
        #endregion Methods

        #endregion

        #region background Installer
        private void backgroundInstaller_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (progressBar1.Minimum <= e.ProgressPercentage && e.ProgressPercentage <= progressBar1.Maximum)
            {
                progressBar1.Value = e.ProgressPercentage;
            }
            else
            {
                AppUpdater.LogError(String.Format("Progress Percentage is out of range: {0}", e.ProgressPercentage));
            }
        }

        private void backgroundInstaller_DoWork(object sender, DoWorkEventArgs e)
        {
            PausableBackgroundWorker worker = sender as PausableBackgroundWorker;
            List<string> modules = (List<string>)e.Argument;
            //try
            //{
            // Assign the result of the computation
            // to the Result property of the DoWorkEventArgs
            // object. This is will be available to the 
            // RunWorkerCompleted eventhandler.
            InstallModules(modules, worker, e);
            //if (e.Cancel)
            //    e.Result = false;
            //else
            e.Result = true;
            //}
            //#if DEBUG_BKUPDATER
            //            catch (Exception ex)
            //            {
            //                MessageBox.Show("Exception!\n" + ex.Message, "backgroundUpdater", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //                e.Result = false;
            //            }
            //#else
            //catch (Exception ex)
            //{
            //    e.Result = false;
            //
            //}
            //#endif
        }

        private void backgroundInstaller_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
#if DEBUG_BKUPDATER
            MessageBox.Show("Work complete!\n Downloaded " + bytesRead + " of " + downloadSize, "backgroundUpdater", MessageBoxButtons.OK, MessageBoxIcon.Information);
#endif
                // Work complete. Now set result and Close ProgressFrom
                if (e.Error != null)
                {
                    // First, handle the case where an exception was thrown.
                    //this.DialogResult = DialogResult.Abort;
                    tabPageErrorReturnPage = null;
                    spInstallPages.SelectedTab = tabPageError;
                    lblErrMsg.Text = e.Error.Message;
                    return;
                }
                else if (e.Cancelled)
                {
                    // Next, handle the case where the user canceled 
                    // the operation.
                    // Note that due to a race condition in 
                    // the DoWork event handler, the Cancelled
                    // flag may not have been set, even though
                    // CancelAsync was called.
                    //this.DialogResult = DialogResult.Cancel;
                    UninstallList.Instance.Dispose();
                    lblProgressProcess.Text = "Удаление";
                    backgroundUninstaller.RunWorkerAsync(newAppDir);
                    return;
                }
                else if ((bool)e.Result == false)
                {
                    //this.DialogResult = DialogResult.Retry;
                    tabPageErrorReturnPage = null;
                    spInstallPages.SelectedTab = tabPageError;
                    lblErrMsg.Text = "Ошибка установки";
                    return;
                }
                else
                {
                    // Finally, handle the case where the operation 
                    // succeeded.
                    FinilizeInstall();
                    
                    AppInstaller.IsInstalledSuccessful = true;
                    spInstallPages.SelectedTab = tabPageComplete;
                    return;
                }
            }
            catch (Exception ex)
            {
                tabPageErrorReturnPage = null;
                spInstallPages.SelectedTab = tabPageError;
                lblErrMsg.Text = "Необработанная ошибка\n" + ex.Message;
            }
        }

        private void ProgressReporter(ulong bytes)
        {
            bytesRead += bytes;
            // each file is reported twice: after download and after unzip
            worker.ReportProgress((int)(100 * (bytesRead) / (downloadSize << 1)));
        }

        public void InstallModules(List<string> modules, PausableBackgroundWorker w, DoWorkEventArgs e)
        {
            Exception exception = null;

            string srvr = UpdaterConfiguration.Settings.ServerUpdaterUrl;

            worker = w;

            // Abort the operation if the user has canceled.
            // Note that a call to CancelAsync may have set 
            // CancellationPending to true just after the
            // last invocation of this method exits, so this 
            // code will not have the opportunity to set the 
            // DoWorkEventArgs.Cancel flag to true. This means
            // that RunWorkerCompletedEventArgs.Cancelled will
            // not be set to true in your RunWorkerCompleted
            // event handler. This is a race condition.
            //if (worker.CancellationPending)
            //{
            //    e.Cancel = true;
            //    return;
            //}

            int count = modules.Count;

            worker.ReportProgress((int)0);
            bytesRead = 0;
            downloadSize = 0;

            List<ModuleUpdater> updaters = new List<ModuleUpdater>();

            foreach (string modname in modules)
            {
                worker.WaitOne();
                if (worker.CancellationPending)
                {
                    AppUpdater.LogInfo("Installation Canceled...");
                    e.Cancel = true;
                    break;
                }
                else
                {
                    AppUpdater.LogInfo("Initializing installation of " + modname);
#warning re-implement locat install using localupdate = ModuleUpdate(modname, localpath, true);
                    ModuleUpdater updater = new ModuleUpdater(modname, srvr, false);

                    // to repair set targetVersion as the current server version
                    if (Utilities.CLIParser.RepairModeEnabled)
                        updater.TargetVersion = updater.ServerVersion;

                    // Note that HasNewerVersion() also updates targetVersion, if newer version is available
                    if (Utilities.CLIParser.RepairModeEnabled || updater.HasNewerVersion())
                    {
                        AppUpdater.LogInfo("Update required for " + modname + " "
                                        + updater.LocalVersion + " -> " + updater.TargetVersion);
                        //if (Updater.TestingModeEnabled)
                        //    MessageBox.Show(Properties.Resources.InfoMsg_UpdatesAvailable
                        //                       + " " + modname + "-" + updater.TargetVersion,
                        //                    Properties.Resources.InfoMsg_ProgramName,
                        //                    MessageBoxButtons.OK);
                        int fcnt = 0;
                        if ((fcnt = updater.HasObsoleteFiles()) > 0)
                        {
                            AppUpdater.LogInfo(fcnt + " obsolete files found for " + modname);
                            updaters.Add(updater);
                            downloadSize += updater.DownloadSize;
                        }
                        else
                        {
                            // newer version available but all files are up to date
                            AppUpdater.LogInfo(fcnt + " obsolete files found. Updating " + modname + ".version.txt file only");
                            updater.UpdateLocalVersionFile();
                            updater.Dispose();
                        }
                    }
                    else
                    {
                        AppUpdater.LogInfo("Nothing to do for " + modname + " " + updater.LocalVersion);
                    }
                }
            }

            AppUpdater.LogInfo(String.Format("Total download size for all modules: {0} bytes", downloadSize));

            if (updaters.Count > 0)
            {
                AppUpdater.LogInfo("Installing modules...");
                foreach (ModuleUpdater updater in updaters)
                {
                    worker.WaitOne();
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        AppUpdater.LogInfo("Installation Canceled...");
                        break;
                    }
                    else
                    {
                        try
                        {
                            AppUpdater.LogInfo("Installing new files found for " + updater.ModuleName);
                            updater.Install(ProgressReporter, worker);
                        }
                        catch (Exception ex)
                        {
                            AppUpdater.LogError("Exception raised while installing files for " + updater.ModuleName
                                                + " : " + ex.Message);

                            // Save exception to re-throw it to gui later
                            exception = ex;

                            // Abort Installation
                            break;
                        }
                    }
                }
            }

            AppUpdater.LogInfo("Cleaning up...");
            foreach (ModuleUpdater updater in updaters)
                updater.Dispose();

            if (exception != null)
                throw exception;
        }

        private ulong bytesRead;            // already downloaded
        private ulong downloadSize;         // total to download
        private PausableBackgroundWorker worker;    // for ProgressReporter
        #endregion

        #region Installation
        void ProcessInstall(string installDest)
        {
            /* 
             * Install process
             * 1. Copy files
             * 2. On Success, Update registry
             * 3. Create desktop icon
             * 4. Create Start Menu group
             * 5. Install a root certificate
             * 6. Run application
             */

            UpdaterConfiguration.AppPath = installDest;

            // 1. Copy files
            string uninstfname = installDest + @"\\" + Properties.Resources.Builtin_UninstallFilename;
            if (System.IO.File.Exists(uninstfname))
            {
                try
                {
                    System.IO.File.Delete(uninstfname);
                }
                catch
                {
#warning Add exception handler here
                }
            }
            UninstallList.Filename = uninstfname;

            List<string> modules = Updater.AppUpdater.GetModulesNames();

            backgroundInstaller.RunWorkerAsync(modules);

        }

        void FinilizeInstall()
        {
            /*
             * Files are copied
             * 
             * 2. Update registry
             * 3. Create desktop icon
             * 4. Create Start Menu group
             * 5. Install a root certificate
             * 6. Run application
             */

            string installDest = UpdaterConfiguration.AppPath;
            System.IO.Directory.CreateDirectory(installDest);
            string fname = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            string dest = System.IO.Path.Combine(installDest, ProgramInfo.ExecutableName());
            if (!System.IO.File.Exists(dest))
                System.IO.File.Copy(fname, dest, false);

            // 2. Update registry
            Utilities.RegistryEditor.RegisterAetpUpdater(installDest);

            // 3. Create desktop icon
            if (cheAddDesktopIcon.Checked == true)
            {
                string deskDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                string shortcutFileName = deskDir + @"\" + Properties.Resources.ApplicationName + @".lnk";
                Utilities.ShellShortcut m_Shortcut = new Utilities.ShellShortcut(shortcutFileName);
                m_Shortcut.Path = installDest + "\\" + ProgramInfo.ExecutableName();
                m_Shortcut.WorkingDirectory = installDest;
                //m_Shortcut.Arguments = "";
                //m_Shortcut.Description = "";
                //m_Shortcut.IconPath = "";
                //m_Shortcut.IconIndex = Convert.ToInt32("0");
                //m_Shortcut.WindowStyle = ProcessWindowStyle.Normal;
                m_Shortcut.Save();
                UninstallList.Instance.AddItem(shortcutFileName);
            }

            // 4. Create Start Menu group
            if (cheAddToStartMenu.Checked == true)
            {
                string menuDir = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu);
                string progDir = menuDir + @"\AETP";
                System.IO.Directory.CreateDirectory(progDir);
                // Start app
                {
                    string shortcutFileName = progDir + @"\" + Properties.Resources.ApplicationName + @".lnk";
                    Utilities.ShellShortcut m_Shortcut = new Utilities.ShellShortcut(shortcutFileName);
                    m_Shortcut.Path = installDest + "\\" + ProgramInfo.ExecutableName();
                    m_Shortcut.WorkingDirectory = installDest;
                    //m_Shortcut.Arguments = "";
                    //m_Shortcut.Description = "";
                    //m_Shortcut.IconPath = "";
                    //m_Shortcut.IconIndex = Convert.ToInt32("0");
                    //m_Shortcut.WindowStyle = ProcessWindowStyle.Normal;
                    m_Shortcut.Save();
                    UninstallList.Instance.AddItem(shortcutFileName);
                    UninstallList.Instance.AddItem(progDir);
                }
                // Update locally
                {
                    string shortcutFileName = progDir + @"\" + Properties.Resources.ApplicationName + @" Обновить с диска.lnk";
                    Utilities.ShellShortcut m_Shortcut = new Utilities.ShellShortcut(shortcutFileName);
                    m_Shortcut.Path = installDest + "\\" + ProgramInfo.ExecutableName();
                    m_Shortcut.WorkingDirectory = installDest;
                    m_Shortcut.Arguments = @" /locally";
                    //m_Shortcut.Description = "";
                    //m_Shortcut.IconPath = "";
                    //m_Shortcut.IconIndex = Convert.ToInt32("0");
                    //m_Shortcut.WindowStyle = ProcessWindowStyle.Normal;
                    m_Shortcut.Save();
                    UninstallList.Instance.AddItem(shortcutFileName);
                    UninstallList.Instance.AddItem(progDir);
                }

            }

            // 5. Install a root certificate

            Updater.AppUpdater.LogInfo("Certificate check. ");

            if (!IsCertInstalled())
            {
                Updater.AppUpdater.LogInfo("Certificate installation. ");
                CertInstall();
            }

            // 6. Add uninstall records for files, created by Seldon1.6 executable
            UninstallList.Instance.AddItem(Path.Combine(installDest, @"xulrunner\components\compreg.dat"));
            UninstallList.Instance.AddItem(Path.Combine(installDest, @"xulrunner\components\xpti.dat")); 
             

        }
        #endregion

        #region Proxy Settings
        #region IsUserInputValid
        private bool IsLocalRepository()
        {
            Uri src;
            if (Uri.TryCreate(this.ServerUrlComboBox.Text, UriKind.Absolute, out src) == false)
            {
                src = new Uri(Application.StartupPath + "\\" + this.ServerUrlComboBox.Text);
                if (!System.IO.File.Exists(src.OriginalString + @"\repo.info"))
                {
                    throw new Exception("");
                }
                else if (String.Compare(System.IO.File.ReadAllText(src.OriginalString + @"\repo.info"),
                                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name)
                    != 0)
                {
                    throw new Exception("");
                }
                else
                    return true;
            }
            return false;
        }

        private bool IsUserInputValid()
        {
            // validate user input
            this.ErrorProvider.Clear();
            bool valid = true;

            if (this.ServerUrlComboBox.Text.Trim().Length == 0)
            {
                this.ErrorProvider.SetError(this.ServerUrlComboBox, "обязательное поле");
                valid = false;
            }

            if (this.ch_UseProxy.Checked)
            {
                if (this.ProxyUrlTextBox.Text.Trim().Length == 0)
                {
                    this.ErrorProvider.SetError(this.ProxyUrlTextBox, "обязательное поле");
                    valid = false;
                }
                if (this.ProxyPortNumBox.Text.Trim().Length == 0)
                {
                    this.ErrorProvider.SetError(this.ProxyPortNumBox, "обязательное поле");
                    valid = false;
                }
            }

            if (this.ch_UseProxyAuth.Checked)
            {
                if (this.UserTextBox.Text.Trim().Length == 0)
                {
                    this.ErrorProvider.SetError(this.UserTextBox, "обязательное поле");
                    valid = false;
                }
                if (this.PasswordTextBox.Text.Trim().Length == 0)
                {
                    this.ErrorProvider.SetError(this.PasswordTextBox, "обязательное поле");
                    valid = false;
                }
            }

            if (valid)
            {
                try
                {
                    IsLocalRepository();
                }
                catch (Exception ex)
                {
                    string msg = ex.Message; // Escape comiler warning
                    lblErrMsg.Text = @"Неправильно задан адрес сервера";
                    tabPageErrorReturnPage = tabPageConnection;
                    spInstallPages.SelectedTab = tabPageError;
                    valid = false;
                }
            }

            return valid;
        }
        #endregion

        #region SaveSettings
        private void SaveSettings()
        {
            UpdaterConfiguration.Settings.UseProxy = this.ch_UseProxy.Checked;

            UpdaterConfiguration.Settings.ServerUpdaterUrl = this.ServerUrlComboBox.Text;

            UpdaterConfiguration.Settings.ProxyUrl = this.ProxyUrlTextBox.Text;
            UpdaterConfiguration.Settings.ProxyPort = this.ProxyPortNumBox.Text;

            UpdaterConfiguration.Settings.UseProxyAuth = this.ch_UseProxyAuth.Checked;

            UpdaterConfiguration.Settings.ProxyUser = this.UserTextBox.Text;
            UpdaterConfiguration.Settings.ProxyPassword = this.PasswordTextBox.Text;
            UpdaterConfiguration.Settings.ProxyAuthType = this.ProxyAuthTypeBox.Text;
            
            UpdaterConfiguration.Settings.Save();

            UpdaterConfiguration.SetGlobalProxy();
        }

        private Settings GetCurrentSettings()
        {
            Settings settings = new Settings();
            settings.UseProxy = this.ch_UseProxy.Checked;
            settings.ServerUpdaterUrl = this.ServerUrlComboBox.Text;
            settings.ProxyUrl = this.ProxyUrlTextBox.Text;
            settings.ProxyPort = this.ProxyPortNumBox.Text;
            settings.UseProxyAuth = this.ch_UseProxyAuth.Checked;
            settings.ProxyUser = this.UserTextBox.Text;
            settings.ProxyPassword = this.PasswordTextBox.Text;
            settings.ProxyAuthType = this.ProxyAuthTypeBox.Text;

            UpdaterConfiguration.SetGlobalProxy(settings);

            return settings;
        }
        #endregion
#endregion Proxy Settings

        public TabPage TabPageErrorReturnPage
        {
            get
            {
                return tabPageErrorReturnPage;
            }
            set
            {
                if (tabPageErrorReturnPage == value)
                    return;
                tabPageErrorReturnPage = value;
            }
        }
        private TabPage tabPageErrorReturnPage;
        private bool isUpdateMode;
        private string newAppDir;  // stores path of newly created destination directory

        #region Uninstall
        #region backgroundUninstaller
        private void backgroundUninstaller_DoWork(object sender, DoWorkEventArgs e)
        {
            PausableBackgroundWorker worker = sender as PausableBackgroundWorker;

            UninstallFiles(worker, e);

            e.Result = true;
        }

        private void backgroundUninstaller_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (progressBar1.Minimum <= e.ProgressPercentage && e.ProgressPercentage <= progressBar1.Maximum)
            {
                progressBar1.Value = 100 - e.ProgressPercentage;
            }
            else
            {
                AppUpdater.LogError(String.Format("Progress Percentage is out of range: {0}", e.ProgressPercentage));
            }
        }

        private void backgroundUninstaller_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            try
            {
                // Work complete. Now set result and Close ProgressFrom
                if (e.Error != null)
                {
                    // First, handle the case where an exception was thrown.

                    //this.DialogResult = DialogResult.Abort;
                    tabPageErrorReturnPage = null;
                    spInstallPages.SelectedTab = tabPageError;
                    lblErrMsg.Text = e.Error.Message;
                    return;
                }
                /* Uninstall does not support cancellation
                else if (e.Cancelled)
                {
                    // Next, handle the case where the user canceled 
                    // the operation.
                    // Note that due to a race condition in 
                    // the DoWork event handler, the Cancelled
                    // flag may not have been set, even though
                    // CancelAsync was called.
                    
                    this.DialogResult = DialogResult.Cancel;
                    tabPageErrorReturnPage = null;
                    spInstallPages.SelectedTab = tabPageError;
                    lblErrorHeader.Text = "Отмена установки";
                    lblErrMsg.Text = "";
                    return;

                }*/
                else if ((bool)e.Result == false)
                {
                    //this.DialogResult = DialogResult.Retry;
                    tabPageErrorReturnPage = null;
                    spInstallPages.SelectedTab = tabPageError;
                    lblErrMsg.Text = "Ошибка установки";
                    return;
                }
                else
                {
                    // Finally, handle the case where the operation 
                    // succeeded.
                    //this.DialogResult = DialogResult.OK;
                    lblCompleteSuccessful.Text = "Отмена установки";
                    spInstallPages.SelectedTab = tabPageComplete;
                    return;
                }
            }
            catch (Exception ex)
            {
                tabPageErrorReturnPage = null;
                spInstallPages.SelectedTab = tabPageError;
                lblErrMsg.Text = "Необработанная ошибка\n" + ex.Message;
            }
        }

        private bool UninstallFiles(PausableBackgroundWorker w, DoWorkEventArgs e)
        {
            try
            {
                worker = w; // for progress reporter

                AppUpdater.LogInfo("Uninstalling files...");

                string installLocation;
                if (String.IsNullOrEmpty((string)e.Argument))
                    installLocation = Utilities.RegistryEditor.AetpUpdaterInstallLocation();
                else
                    installLocation = (string)e.Argument;

                if (String.IsNullOrEmpty(installLocation))
                {
                    AppUpdater.LogError("Installation path is empty");
                    return false;
                }

                List<String> itemlist = new List<String>();

                string uninstfname = installLocation + @"\uninstall.dat";

                if (System.IO.File.Exists(uninstfname))
                {
                    using (System.IO.StreamReader r = new System.IO.StreamReader(uninstfname))
                    {
                        string line;
                        while ((line = r.ReadLine()) != null)
                        {
                            itemlist.Add(line);
                        }
                    }
                    itemlist.Sort();
                    itemlist.Reverse();
                }

                int totalFiles = itemlist.Count;
                int removedFiles = 0;

                worker.ReportProgress((int)0);

                // graceful Uninstall

                // remove files;
                foreach (string fname in itemlist)
                {
                    if (File.Exists(fname))
                    {
                        FileAttributes fas = File.GetAttributes(fname);
                        if ((fas & FileAttributes.Directory) != FileAttributes.Directory)
                        {
                            try
                            {
                                File.Delete(fname);
                                AppUpdater.LogFileInfo("File deleted " + fname);
                                worker.ReportProgress((int)(100 * ++removedFiles / totalFiles));
                            }
                            catch (Exception ex)
                            {
                                // skip access errors
                                //AppUpdater.LogFileInfo("Unable delete " + fname + " : " + ex.Message);
                                AppUpdater.LogError("Unable delete " + fname + " : " + ex.Message);
                            }
                        }
                    }
                }

                // remove directories
                // using second pass to remove empty directories only
                foreach (string fname in itemlist)
                {
                    if (Directory.Exists(fname))
                    {
                        FileAttributes fas = File.GetAttributes(fname);
                        if ((fas & FileAttributes.Directory) == FileAttributes.Directory)
                        {
                            if (Directory.Exists(fname))
                            {
                                try
                                {
                                    // all registered files were removed on the first pass
                                    // remove empty directories only
                                    if (Directory.GetFiles(fname).Length == 0)
                                    {
                                        Directory.Delete(fname, false);
                                        AppUpdater.LogFileInfo("Directory deleted " + fname);
                                        worker.ReportProgress((int)(100 * ++removedFiles / totalFiles));
                                    }
                                    else
                                    {
                                        AppUpdater.LogFileInfo("skipping directory since it is not empty " + fname);
                                    }
                                }
#warning Uninstall: skipping access errors
                                catch (Exception ex)
                                {
                                    // skip access errors
                                    AppUpdater.LogError("Unable delete " + fname + " : " + ex.Message);
                                }
                            }
                        }
                    }
                }

                if (File.Exists(installLocation + @"\uninstall.dat"))
                    File.Delete(installLocation + @"\uninstall.dat");

                if ((Directory.GetFiles(installLocation).Length == 0))
                {
                    Directory.Delete(Path.GetDirectoryName(installLocation), true);
                }
                else
                {
                    if (MessageBox.Show(this, "В каталоге:\n" + installLocation + "\n"
                                            + "остались файлы. Удалить?",
                                            Properties.Resources.InfoMsg_ProgramName,
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        // Brute Uninstall
                        try
                        {
                            System.IO.Directory.Delete(Path.GetDirectoryName(installLocation), true);
                        }
                        catch (Exception ex)
                        {
                            AppUpdater.LogError("Unable delete " + installLocation + " : " + ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppUpdater.LogError("Uninstall error: " + ex.Message);

                MessageBox.Show(this,
                                "Произошла ошибка во время удаления\n"
                                    + ex.Message,
                                 Properties.Resources.InfoMsg_ProgramName,
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
                return false;
            }

            AppUpdater.LogInfo("Uninstalling files completed");
            return true;
        }

        public void ShowUninstall()
        {
            //string frmText = this.Text;
            //this.Text = aetpUpdater.Properties.Resources.InfoMsg_ProgramName;
            progressBar1.Value = progressBar1.Maximum;
            lblProgressProcess.Text = "Удаление";
            spInstallPages.SelectedTab = tabPageProgress;
            backgroundUninstaller.RunWorkerAsync();
            this.ShowDialog();
        }
        #endregion backgroundUninstaller

        #endregion Uninstall


        #region Certificate functions
        public static void CertInstall()
        {
            try
            {
                X509Store store = new X509Store(StoreName.Root, StoreLocation.CurrentUser);
                store.Open(OpenFlags.ReadWrite);

                X509Certificate2 certificate = new X509Certificate2((byte[])Properties.Resources.serverBase_root_cert);


                store.Add(certificate);
                store.Close();
            }
            catch (Exception)
            {


            }

            #region oldCode
            //String AppPath = Application.StartupPath + "\\CertMgr.exe";
            //String CertPath = Application.StartupPath + "\\serverBase_root_cert.cer";
            //System.IO.File.WriteAllBytes(AppPath, Properties.Resources.CertMgr);
            //System.IO.File.WriteAllBytes(CertPath, Properties.Resources.serverBase_root_cert);

            //try
            //{
            //    String cmd = String.Format("-add \"{0}\" -c -s -r localMachine Root", CertPath);
            //    System.Diagnostics.Process.Start(AppPath,cmd);
            //}
            //catch(Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            //int p = 0;
            //while (true)
            //{
            //    if (p >= 3) break;
            //    try
            //    {
            //        System.IO.File.Delete(AppPath);
            //        System.IO.File.Delete(CertPath);
            //        break;
            //    }
            //    catch
            //    {
            //        System.Threading.Thread.Sleep(2000);
            //    }
            //    p++;
            //} 
            #endregion
        }

        public static bool IsCertInstalled()
        {
            bool result = false;
            try
            {
                X509Store store = new X509Store(StoreName.Root, StoreLocation.CurrentUser);
                store.Open(OpenFlags.ReadOnly);

                X509Certificate2 certificate = new X509Certificate2((byte[])Properties.Resources.serverBase_root_cert);

                X509Certificate2Collection collection = store.Certificates.Find(X509FindType.FindBySerialNumber, certificate.SerialNumber, true);
                result = collection.Count > 0;
                store.Close();
            }
            catch (Exception)
            {


            }
            return result;
        } 
        #endregion

        private static Dictionary<string, IAuthenticationModule> modules = null;
        private static string previousAuthModule = null;
        private void ProxyAuthTypeBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (modules == null)
            {
                modules = new Dictionary<string,IAuthenticationModule>();
                IEnumerator registeredModules = AuthenticationManager.RegisteredModules;
                registeredModules.Reset();
                while(registeredModules.MoveNext())
                {
                    IAuthenticationModule module = registeredModules.Current as IAuthenticationModule;
                    modules.Add(module.AuthenticationType, module);
                }
                foreach (var module in modules)
                    AuthenticationManager.Unregister(module.Key);
            }

            String selectedAuthMethod = previousAuthModule;
            if ((sender as ComboBox).SelectedItem != null)
                selectedAuthMethod = (sender as ComboBox).SelectedItem.ToString();

            if (!modules.ContainsKey(selectedAuthMethod))
            {
                (sender as ComboBox).SelectedIndex = 0;
                return;
            }

            if (previousAuthModule != null)
                AuthenticationManager.Unregister(previousAuthModule);
            AuthenticationManager.Register(modules[selectedAuthMethod]);

            previousAuthModule = selectedAuthMethod;

        }
    }
}
