﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace aetpUpdater.Installer
{
    public partial class DlgLicense : AetpUpdaterForm
    {
        public DlgLicense()
        {
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            InitializeComponent();
            richTxtLicense.Text = Properties.Resources.LicenseText;
            Utilities.WinGraphics.ApplyFormStopResizing(this);
            DialogResult = DialogResult.None;
        }

        private void pictureButton1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
