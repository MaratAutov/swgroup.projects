﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aetpUpdater.Utilities
{
    public static class ProgramInfo
    {
        #region Program Process File Name
        /// <summary>
        /// Shortcut to get file name of executing process
        /// </summary>
        /// <returns>string containg startup path to current process</returns>
        public static string ProcessFileName()
        {
            return System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
        }
        #endregion
        #region Program Executable Name
        /// <summary>
        /// Shortcut to get program (assembly) name with ".exe" extension.
        /// </summary>
        /// <returns>String containing assembly name e.g. "aetpUpdater.exe"</returns>
        public static string ExecutableName()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + @".exe";
        }
        #endregion
        #region Program Version
        /// <summary>
        /// Shortcut to get program (assembly) version
        /// </summary>
        /// <returns>String containing assembly version e.g. "1.2.3.4"</returns>
        public static string Version()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
        #endregion
    }
}
