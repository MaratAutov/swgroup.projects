﻿using System;
using System.Net;
using System.IO;
using System.Windows.Forms;
using updaterSeldonNTLM;

namespace aetpUpdater.Utilities
{
    static class NetworkTester
    {
        public static String ErrorMessage = "";

        private static ExtendedWebClient wc = new ExtendedWebClient();        
        private static WebException _connectionError = null;
        public static bool TestConnection(string serverUrl="", Settings settings=null)
        {
            if (!ExtendedWebClient.UseSeldonNTLM)
                _connectionError = null;

            if( settings == null )
                settings = UpdaterConfiguration.Settings;

            bool proxyAuthNTLM = settings.UseProxyAuth &&
                                 settings.ProxyAuthType == "NTLM";

            if( String.IsNullOrEmpty(serverUrl) )
                serverUrl = UpdaterConfiguration.Settings.ServerUpdaterUrl;

            bool result = false;

            string fname = String.Format(@"{0}updater16/{1}/updater16.version.txt",
                                             (serverUrl.EndsWith("/") ? serverUrl : serverUrl + "/"),
                                             Updater.AppUpdater.DefaultVersionPath);
            Uri uriInfo = new Uri(fname);
            try
            {
                string s = wc.DownloadString(uriInfo);
                if( wc.StatusCode == HttpStatusCode.OK && !String.IsNullOrEmpty(s) )
                {
                    Version ver = new Version(s);
#if DEBUG
                    System.Windows.Forms.MessageBox.Show(ver.ToString(), "DEBUG: IsConnectionOk()");
#endif
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch( WebException exc )
            {
                _connectionError = _connectionError ?? exc;
                if (!ExtendedWebClient.UseSeldonNTLM && ((exc.Message.Contains("(407)") || wc.StatusCode == HttpStatusCode.ProxyAuthenticationRequired) && proxyAuthNTLM))
                {
                    ExtendedWebClient.UseSeldonNTLM = true;
                    result = TestConnection(serverUrl, settings);
                    if( !result )
                        ExtendedWebClient.UseSeldonNTLM = false;
                    else
                    {
                        _connectionError = null;
                        return true;
                    }
                }
                if( wc.StatusCode == 0 ) // we've failed establishing connection to proxy or server
                {
                    WebException webExc = _connectionError;
                    if( webExc.Status == WebExceptionStatus.ProtocolError && webExc.Response != null )
                        webExc = exc;
                    if( webExc.Status == WebExceptionStatus.NameResolutionFailure || webExc.Status == WebExceptionStatus.ConnectFailure || webExc.Response != null )
                    {
                        String msg = webExc.Message;
                        String hint = "";
                        if( msg.Contains("(503)") || msg.Contains("(406)") || webExc.Status == WebExceptionStatus.ConnectFailure )
                            hint = "Проверьте корректность адреса сервера или работу DNS";
                        else if( msg.Contains("(407)") )
                            hint = "Проверьте корректность логина или пароля";
                        if( webExc.InnerException != null )
                            msg += Environment.NewLine + webExc.InnerException.Message;

                        ErrorMessage = msg + Environment.NewLine + hint;

                        return false;
                    }
                }
                else // Connection has been established but server returned an error
                {
                    ErrorMessage = String.Format("{0}", exc.Message);
                    return false;
                }
            }
            catch (Exception exc)
            {
#if DEBUG
                System.Windows.Forms.MessageBox.Show(exc.Message, "DEBUG: IsConnectionOk()");
#endif
                return false;
            }

            return false;
        }

        public static void ResetGlobalProxyToSavedSettings()
        {
            UpdaterConfiguration.SetGlobalProxy();
        }
    }
}
