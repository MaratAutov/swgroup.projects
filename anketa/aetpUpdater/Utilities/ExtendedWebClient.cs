﻿using System;
using System.Net; // for WebClient
using updaterSeldonNTLM;

namespace aetpUpdater.Utilities
{
    public class ExtendedWebClient : System.Net.WebClient
    {
        public HttpStatusCode StatusCode;

        public static bool UseSeldonNTLM = false;
        private int timeout = 2 * 60 * 60 * 1000;//In Milli seconds

        public int TimeoutMilliseconds
        {
            get
            {
                return timeout;
            }
            set
            {
                timeout = value;
            }
        }
        public int TimeoutSeconds
        {
            set
            {
                timeout = value * 1000;
            }
        }
        
        public ExtendedWebClient()
        {
        }
        
        public ExtendedWebClient(Uri address)
        {
            //this.timeout = 600000;//In Milli seconds
            WebRequest objWebClient = GetWebRequest(address);
        }

        protected override void OnDownloadFileCompleted(System.ComponentModel.AsyncCompletedEventArgs e)
        {
            base.OnDownloadFileCompleted(e);
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            StatusCode = 0;

            if( UseSeldonNTLM && address.Scheme != "https" )
                address = new Uri(address.AbsoluteUri.Replace("http", "seldon-http"));

            WebRequest objWebRequest = base.GetWebRequest(address);
            objWebRequest.Timeout = this.timeout;

            return objWebRequest;
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);

            Type responseType = response.GetType();
            StatusCode = responseType == typeof(SHttpWebResponse)
                ? (response as SHttpWebResponse).StatusCode : (response as HttpWebResponse).StatusCode;

            return response;
        }
    }
}
