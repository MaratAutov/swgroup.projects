﻿using System;
using System.ComponentModel;
using System.Threading;

namespace aetpUpdater.Utilities
{
    /// <summary>
    /// PausableBackgroundWorker
    /// </summary>
    public class PausableBackgroundWorker : BackgroundWorker
    {
        private ManualResetEvent runningEvent = new ManualResetEvent(true);
        public void WaitOne()
        {
            runningEvent.WaitOne();
        }
        public void Continue()
        {
            runningEvent.Set();
        }
        public void Pause()
        {
            runningEvent.Reset();
        }
    }
}
