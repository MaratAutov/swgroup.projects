﻿using System;
using System.IO;
using System.Collections.Generic;


namespace aetpUpdater.Utilities
{
    public class UninstallList : IDisposable
    {
#region public interface
        public static UninstallList Instance
        {
            get
            {
                if (instance == null)
                {
                    if (String.IsNullOrEmpty(filename))
                        filename = Properties.Resources.Builtin_UninstallFilename;
                    if (String.IsNullOrEmpty(filename))
                        filename = @"uninstall.dat";
                    instance = new UninstallList(filename);
                }
                return instance;
            }
        }

        public static String Filename
        {
            set
            {
                if (String.IsNullOrEmpty(filename) && !String.IsNullOrEmpty(value))
                    filename = value;
            }
            get
            {
                return filename;
            }
        }

        public void AddItem(String newitem)
        {
            if (disposed)
                throw new ObjectDisposedException(null);

            int pos = itemlist.BinarySearch(newitem.ToUpper());
            if (pos < 0)
                itemlist.Insert(~pos, newitem.ToUpper());
        }

        public void RemoveItem(String item)
        {
            if (disposed)
                throw new ObjectDisposedException(null);
            
            int pos = itemlist.BinarySearch(item.ToUpper());
            if (pos >= 0)
                itemlist.RemoveAt(pos);
        }

        public void clear()
        {
            if (disposed)
                throw new ObjectDisposedException(null);

            itemlist.Clear();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

#endregion

        ~UninstallList()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Release managed resources.
                }
               
                // Release unmanaged resources.
                try
                {
                    using (StreamWriter w = new StreamWriter(filename))
                    {
                        foreach (string line in itemlist)
                            w.WriteLine(line);
                    }
                }
                catch { } // ignore error;

                disposed = true;
                itemlist = null;
                filename = null;
                instance = null;
            }
        }

        private UninstallList(String fname)
        {
            disposed = false;
            filename = fname;
            itemlist = new List<String>();

            if (String.IsNullOrEmpty(fname))
                throw new ArgumentException();

            if (File.Exists(fname))
            {
                using (StreamReader r = new StreamReader(filename))
                {
                    string line;
                    while ((line = r.ReadLine()) != null)
                    {
                        itemlist.Add(line);
                    }
                }
                itemlist.Sort();
            }
        }

        private static UninstallList instance;  // Singleton reference
        private static String filename;         // file to update
        private List<String> itemlist;          // sorted list for faster operations
        private bool disposed;                  // Dispose() call guard

    }
}
