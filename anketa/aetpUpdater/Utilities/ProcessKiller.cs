﻿using System;
using System.Diagnostics;

namespace aetpUpdater.Utilities
{
    class ProcessKiller
    {
        /// <summary>
        /// Kills all process with given name excluding current process.
        /// </summary>
        /// <param name="procName">Case insensitive process name without extension, e.g. "NotePAD"</param>
        public static void KillAllProcessesByName(string procName)
        {
            foreach (Process proc in Process.GetProcesses())
                if (String.Compare(proc.ProcessName, procName, true) == 0
                      && proc.Id != Process.GetCurrentProcess().Id)
                {
                    #region proc.Kill();
                    try
                    {
                        // there should be only few instances of updater
                        proc.Kill();
                        if (!proc.WaitForExit(60000))
                            throw new SystemException("Timeout 60 seconds expired. Could not terminate " + proc.ProcessName);
                    }
                    catch (System.ComponentModel.Win32Exception)
                    {

                        // MSDN Kill()
                        // The associated process could not be terminated.
                        // or the process is terminating.
                        // or the associated process is a Win16 executable.
                        if (!proc.WaitForExit(60000)) // wait for termination. Timeout 60s.
                            throw; // Process could not be terminated.
                        // MSDN Process.WaitForExit()
                        // The wait setting could not be accessed. 
                    }
                    catch (NotSupportedException)
                    {
                        // You are attempting to call Kill for a process that is running on a remote computer. 
                        // The method is available only for processes running on the local computer.
                        throw;
                    }
                    catch (InvalidOperationException)
                    {
                        // MSDN: The process has already exited 
                        // or there is no process associated with this Process object.
                        // Nothing to do   
                    }
                    catch (SystemException)
                    {
                        // MSDN Process.WaitForExit()
                        // No process Id has been set, and a Handle from which the Id property can be determined does not exist.
                        // or there is no process associated with this Process object.
                        // or You are attempting to call WaitForExit() for a process that is running on a remote computer. This method is available only for processes that are running on the local computer. 
                        throw;
                    }
                    #endregion;
                }
        }

        /// <summary>
        /// Pauses current process until process with pid is running
        /// no error is thrown if the pid is not running now
        /// </summary>
        /// <param name="pid"></param>

        public static void WaitProcForExit(int pid)
        {
            try
            {
                using (Process proc = Process.GetProcessById(pid))
                {
                    try
                    {
                        if (!proc.WaitForExit(60000))
                            throw new SystemException("Timeout 60 seconds expired. Could not terminate " + proc.ProcessName);
                    }
                    catch (System.ComponentModel.Win32Exception)
                    {
                        throw;
                    }
                    catch (SystemException)
                    {
                        throw;
                    }
                }
            }
            catch (ArgumentException)
            {
                // MSDN: The process specified by the processId parameter is not running. The identifier might be expired.
                // do not report an error
            }
        }

        public static void WaitAllProcForExit(string procName)
        {
            foreach (Process proc in Process.GetProcesses())
                if (String.Compare(proc.ProcessName, procName, true) == 0
                      && proc.Id != Process.GetCurrentProcess().Id)
                {
                    try
                    {
                        // there should be only few instances of updater
                        if (!proc.WaitForExit(60000))
                            throw new SystemException("Timeout 60 seconds expired. Could not terminate " + proc.ProcessName);
                    }
                    catch (System.ComponentModel.Win32Exception)
                    {
                        throw;
                    }
                    catch (SystemException)
                    {
                        // MSDN Process.WaitForExit()
                        // No process Id has been set, and a Handle from which the Id property can be determined does not exist.
                        // or there is no process associated with this Process object.
                        // or You are attempting to call WaitForExit() for a process that is running on a remote computer. This method is available only for processes that are running on the local computer. 
                        throw;
                    }
                }
        }

        /// <summary>
        /// Checks if another process with the same name is running on the local system
        /// </summary>
        /// <returns>true if running, false otherwise</returns>
        public static bool IsAnotherInstanceRunning()
        {
            Process[] p = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
            if (p.Length > 1)
                return true;
            else
                return false;
        }
    }
}
