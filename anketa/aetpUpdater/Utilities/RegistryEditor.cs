﻿using System;
using System.Security.Permissions;
using Microsoft.Win32;
using System.IO;

namespace aetpUpdater.Utilities
{
    static class RegistryEditor
    {
        //[assembly: RegistryPermissionAttribute(SecurityAction.RequestMinimum, Read = "HKEY_CURRENT_USER")]
        public static bool IsAetpAdminConsoleInstalled()
        {
            try
            {
                using (RegistryKey uninstKey = Registry.CurrentUser.OpenSubKey(keyNameSeldon15))
                {
                    // Read file path from registry uninstall entry
                    //TODO
                    string appPath = uninstKey.GetValue("InstallLocation") + "Editor.exe";

                    // check if the file present under specified path
                    if (File.Exists(appPath))
                        return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public static string GetAetpAdminConsoleInstallPath()
        {
            
            using (RegistryKey uninstKey = Registry.CurrentUser.OpenSubKey(keyNameSeldon15))
            {
                // Read file path from registry uninstall entry
                return uninstKey.GetValue("InstallLocation") + "";
            }

        }

        //[assembly: RegistryPermissionAttribute(SecurityAction.RequestMinimum, All = "HKEY_LOCAL_MACHINE")]
        public static void RegisterAetpUpdater(string dest)
        {
            if (!dest.EndsWith("\\"))
                dest += "\\";
            //using (RegistryKey uninstKey = Registry.LocalMachine.CreateSubKey(keyNameSeldon16))
            using (RegistryKey uninstKey = Registry.CurrentUser.CreateSubKey(RegKeyName))
            {
                //Version v = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                //TODO Sberbank Consultant
                uninstKey.SetValue("DisplayName", Properties.Resources.ApplicationName, RegistryValueKind.String);
                uninstKey.SetValue("DisplayIcon", dest + Properties.Resources.ExeName, RegistryValueKind.String);
                uninstKey.SetValue("DisplayVersion", "1.0" , RegistryValueKind.String);
                uninstKey.SetValue("Publisher", Properties.Resources.Copyright, RegistryValueKind.String);
                
                uninstKey.SetValue("InstallDate", DateTime.Today.ToShortDateString(), RegistryValueKind.String);
                uninstKey.SetValue("InstallLocation", dest, RegistryValueKind.String);
                uninstKey.SetValue("Contact", "8-800-2000-100", RegistryValueKind.String);
                uninstKey.SetValue("UninstallString", String.Format(@"{0}{1} /uninstall", dest, ProgramInfo.ExecutableName()), RegistryValueKind.String);
            }
        }

        //[assembly: RegistryPermissionAttribute(SecurityAction.RequestMinimum, All = "HKEY_LOCAL_MACHINE")]
        public static void UnregisterAetpUpdater()
        {
            #warning Test if the Registry sub tree does not exists
            //Registry.LocalMachine.DeleteSubKeyTree(keyNameSeldon16);
            Registry.CurrentUser.DeleteSubKeyTree(RegKeyName);
        }

        //[assembly: RegistryPermissionAttribute(SecurityAction.RequestMinimum, Read = "HKEY_LOCAL_MACHINE")]
        public static string AetpUpdaterInstallLocation()
        {
            string installLocation = "";
            try
            {
                //using (RegistryKey uninstKey = Registry.LocalMachine.OpenSubKey(keyNameSeldon16))
                using (RegistryKey uninstKey = Registry.CurrentUser.OpenSubKey(RegKeyName))
                {
                    // Read file path from registry uninstall entry
                    installLocation += uninstKey.GetValue("InstallLocation");
                    if (!installLocation.EndsWith("\\"))
                        installLocation += "\\";
                    /*
                    //string appPath = installLocation + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".exe";
                    string appPath = installLocation + "AetpAdminConsole.exe";

                    // check if the file present under specified path
                    if (File.Exists(appPath))
                     */
                        return installLocation;
                }
            }
            catch { }
            // Dont care about any exceptions, since the task is to return true only if the app is available
            return installLocation;
        }

        public static bool IsAetpUpdaterInstalled()
        {
            try
            {
                string installLocation = AetpUpdaterInstallLocation();
                if (!String.IsNullOrEmpty(installLocation)
                    && File.Exists(Path.Combine(installLocation, ProgramInfo.ExecutableName())))
                {
                    return true;
                }
            }
            catch {} // Do not care
            
            return false;
        }

        // MSDN: Uninstall information should be stored in HKEY_LOCAL_MACHINE using GUID instead of textual name
        //private static string guid = "{79EE2638-A796-42CD-98A1-09F63BB34A43}";
        //private static string keyNameSeldon16 = @"Software\Microsoft\Windows\CurrentVersion\Uninstall\Aetp Updater";
        // Eliminate Request for Admins permissions
        //TODO

#warning менять при настройке на новое приложение
        private static string RegKeyName = @"Software\Microsoft\Windows\CurrentVersion\Uninstall\Anketa Updater";
        

        // This key is used by Seldon.2010 v1.5 Installer "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Uninstall\Информационно-аналитическая система Seldon.2012_is1"
        //TODO
        private static string keyNameSeldon15 = @"Software\Microsoft\Windows\CurrentVersion\Uninstall\Анкета.Редактор";
    }
}
