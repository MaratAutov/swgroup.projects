﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aetpUpdater.Utilities
{
    static class FileHasher
    {
        static public string GetMd5Hash(string fname)
        {
            MD5 md5 = new MD5() { ValueAsByte = System.IO.File.ReadAllBytes(fname) };
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5.BytesHash;

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            System.Text.StringBuilder sBuilder = new System.Text.StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        /// Verify a hash against a string.
        static public bool VerifyMd5Hash(string fname, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(fname);

            // Create a StringComparer an comare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            return (0 == comparer.Compare(hashOfInput, hash));
        }
    }
}
