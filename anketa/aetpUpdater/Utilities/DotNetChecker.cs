﻿using System;
using Microsoft.Win32;

namespace aetpUpdater.Utilities
{
    public static class DotNetChecker
    {
        // Registry Keys to detect .NET Framework version
        // http://support.microsoft.com/kb/318785
        private static string keyNameDotNet4Client = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Client";
        private static string keyNameDotNet4Full = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full";

        private static string keyNameDotNet35 = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.5";
        private static string keyNameDotNet30 = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.0";
        private static string keyNameDotNet20 = @"Software\Microsoft\NET Framework Setup\NDP\v2.0.50727";
        private static string keyNameDotNet11 = @"\SOFTWARE\Microsoft\NET Framework Setup\NDP\v1.1.4322";

        public static bool IsDotNet4ClientInstalled()
        {
            try
            {
                using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(keyNameDotNet4Client))
                {
                    // Read file path from registry uninstall entry
                    if (rk.GetValue("Install").ToString() == "1")
                        return true;
                }
            }
            catch { }
 
            return false;
        }
        public static bool IsDotNet4FullInstalled()
        {
            try
            {
                using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(keyNameDotNet4Full))
                {
                    // Read file path from registry uninstall entry
                    if (rk.GetValue("Install").ToString() == "1")
                        return true;
                }
            }
            catch { }

            return false;
        }
        public static bool IsDotNet4Installed()
        {
            return (IsDotNet4FullInstalled());
        }
        public static bool IsDotNet35Installed()
        {
            try
            {
                using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(keyNameDotNet35))
                {
                    // Read file path from registry uninstall entry
                    if (rk.GetValue("Install").ToString() == "1")
                        return true;
                }
            }
            catch { }

            return false;
        }
        public static bool IsDotNet30Installed()
        {
            try
            {
                using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(keyNameDotNet30))
                {
                    // Read file path from registry uninstall entry
                    if (rk.GetValue("Install").ToString() == "1")
                        return true;
                }
            }
            catch { }

            return false;
        }
        public static bool IsDotNet20Installed()
        {
            try
            {
                using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(keyNameDotNet20))
                {
                    // Read file path from registry uninstall entry
                    if (rk.GetValue("Install").ToString() == "1")
                        return true;
                }
            }
            catch { }

            return false;
        }
        public static bool IsDotNet11Installed()
        {
            try
            {
                using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(keyNameDotNet11))
                {
                    // Read file path from registry uninstall entry
                    if (rk.GetValue("Install").ToString() == "1")
                        return true;
                }
            }
            catch { }

            return false;
        }

        public static bool IsDotNetRequiredVersionInstalled()
        {
            return (IsDotNet4Installed());
        }
    }
}
