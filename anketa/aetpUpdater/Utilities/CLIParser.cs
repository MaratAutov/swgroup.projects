﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace aetpUpdater.Utilities
{
    static class CLIParser
    {
        public static bool ParseCLI(string[] args)
        {
            // Update help message after changing this function
            int idx = 0;
            string param = "";
            string value = "";

            for (idx = 0; idx < args.Length; idx++)
            {
                if (args[idx][0] == '/' || args[idx][0] == '-')
                {
                    param = args[idx].Substring(1).ToLower();
                    value = "";
                }
                else
                    value = args[idx];

                if (!string.IsNullOrEmpty(param))
                {
                    switch (param)
                    {
                        case "?":
                        case "h":
                        case "help": help = true; continue;
                        case "s":
//                        case "silent": silent_mode = true; continue;
                        case "r":
                        case "repair": repair_mode = true; continue;

                        case "selfupdate": selfupdate_mode = true; continue; // Due to specific usage do not show this option in help message
                        case "testing": test_mode = true; continue;   // Due to specific usage do not show this option in help message
                        case "development": test_mode = true; dev_mode = true; continue; // Due to specific usage do not show this option in help message
                        case "uninstall": uninstall_mode = true; break; // no continue
                        case "locally": localupdate_mode = true; continue;
                        case "killseldon": killseldon = true; continue;
                        case "waitforseldon": WaitSeldonForExit = true; continue;

                        default:
                            // intentionally no default. simply ignore unrecognized "/param" , "-param"
                            break;
                    }
                    if (!string.IsNullOrEmpty(value))
                    {
                        switch (param)
                        {
                            case "l":
                            case "log":
                            case "loglevel": /*log_enabled = true;*/ logLevel = value; value = ""; break;
                            case "uninstall": uninstall_confirmed = true; waitPidForExit = value; value = ""; break;
                            case "waitforpid": waitPidForExit = value; value = ""; break;
//                            case "u":
//                            case "user":
//                            case "login": cliUsername = value; value = ""; break;
//                            case "p":
//                            case "pass":
//                            case "password": cliPassword = value; value = ""; break;
                            default: // unrecognized option
                                //MessageBox.Show("Unrecognized option" + args[idx]);
                                break;
                        }
                    }
                }
            }
#if DEBUG
            System.Windows.Forms.MessageBox.Show("Recognized CLI arguments:\n"
//                                + "\nusername: " + cliUsername.ToString()
//                                + "\npassword: " + cliPassword.ToString()
                                + "\nhelp:\t" + help
//                                + "\nsilent mode: " + silent_mode
                                + "\nrepair:\t" + repair_mode
                                + "\nselfupd:\t" + selfupdate_mode
                                + "\ntesting:\t" + test_mode
                                + "\nlog level:\t" + logLevel
                                + "\nuninstall:\t" + uninstall_mode
                                + "\nlocally:\t" + localupdate_mode,
                            Properties.Resources.InfoMsg_ProgramName);
#endif

            return true;
        }
        
        public static void ShowHelp()
        {
            /*
#warning help message on English
            System.Windows.Forms.MessageBox.Show(Properties.Resources.InfoMsg_Help + "\n\n"
                                + "/? /h /help- shows this help message box\n"
                                + "/l /log /loglevel <0..4> - sets level of log output to ./updater.log file\n"
                                + "    0 - log disabled, 1 - errors only, 2 - warnings, 3 - info messages, 4 - file operations\n"
                                + "/r /repair - repair all modules\n"
                                + "/s /silent - silent mode\n"
                                + "/u /user /login <user name> - login for server authorization\n"
                                + "/p /pass /password <passord> - password for server authorization\n"
                                + "\nuninstall Uninstalls program from this computer"
                                + "\nAll switches can be used in slash-style or dash-style.\n\n"
                                + "Example:\n"
                                + "    updater /u supervisor -pass 123sss /s /log 3",
                                Properties.Resources.InfoMsg_ProgramName
                                + " - " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                                System.Windows.Forms.MessageBoxButtons.OK,
                                System.Windows.Forms.MessageBoxIcon.Information);
             */
            System.Windows.Forms.MessageBox.Show(Properties.Resources.InfoMsg_Help + "\n\n"
                    + "/? /h /help - показывает окно помощи\n"
                    + "/l /log /loglevel <0..4> - включает лог в файл ./updater.log\n"
                    + "    0 - лог выключен, 1 - только ошибки, 2 - предупреждения, 3 - сообщения, 4 - файловые операции\n"
                    + "/r /repair - восстанавливает поврежденные файлы\n"
                    + "/locally - обновляет приложение с локального диска\n"
                    + "\n/uninstall - удаляет " + Properties.Resources.ApplicationName + @" c этого компьютера\n"
                    + "\nЛюбой ключ может быть использован через '-' или '/'\n\n"
                    + "Пример:\n"
                    +  "  " + Utilities.ProgramInfo.ExecutableName() + " -r /log 3",
                    Properties.Resources.InfoMsg_ProgramName
                    + " - версия " + Utilities.ProgramInfo.Version(),
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Information);

        }

        public static string GetLogLevel()
        {
            return logLevel;
        }

        #region switches
        // Automatic properties has no initializers;
        public static bool RepairModeEnabled { get { return repair_mode; } }
        public static bool SelfUpdateEnabled { get { return selfupdate_mode; } }
        public static bool SilentModeEnabled { get { return silent_mode; } }
        public static bool TestingModeEnabled { get { return test_mode; } }

        #endregion
        // program wide variables
        public static bool silent_mode = false;
        public static bool repair_mode = false;
        public static bool selfupdate_mode = false;    //self update is requested via CLI
        public static bool uninstall_mode = false;
        public static bool uninstall_confirmed = false;
        public static bool dev_mode = false;
        public static bool localupdate_mode = false;
        public static bool killseldon = false;
        public static bool WaitSeldonForExit = false;

        // selfupdate_trigger shows that new AetpUpdater was downloaded. Thus need to perform self update.
        // selfupdate_trigger is set by ModuleUpdater::ExtractFiles();
        public static bool selfupdate_trigger = false;

        public static string cliUsername = "";
        public static string cliPassword = "";
        public static bool help = false;
        
        //        private static bool log_enabled = false;
        public static string logLevel = "0";
        public static string waitPidForExit = "";  // pid of process which must terminate before aetpUpdater continue working;
        public static bool test_mode = false;      // select path to the latest version release or test.
    }
}
