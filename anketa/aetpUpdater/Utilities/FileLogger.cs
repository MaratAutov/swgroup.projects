﻿using System;
using System.IO;

namespace aetpUpdater.Utilities
{
    /// <summary>
    /// Very simple file logger
    /// </summary>
    class FileLogger
    {
        public enum LogLevels : int { NONE = 0, ERROR, WARNING, INFO, FILEINFO }
        public LogLevels FileLogLevel { get; set; }


        public FileLogger(string fname, bool clear) : this(fname, clear, LogLevels.ERROR) { }
       
        public FileLogger(string fname, bool clear, LogLevels level)
        {
            outs = new StreamWriter(fname, !clear) { AutoFlush = true };

            FileLogLevel = level;
            pid = System.Diagnostics.Process.GetCurrentProcess().Id;
        }

        public void Log(string tag, string msg)
        {
            outs.WriteLine(String.Format("{0} | {1} | {2} | {3}", TimeStamp(), pid, tag, msg));
        }

        public void Log(LogLevels level, string msg)
        {
            if (level <= FileLogLevel && level != LogLevels.NONE)
            {
                Log(TagName(level), msg);
            }
        }

        public static string TimeStamp()
        {
            return DateTime.Now.ToString("yyyy'-'MM'-'dd' 'HH':'mm':'ss'.'ffff");
        }

        protected static string TagName(LogLevels level)
        {
            switch (level)
            {
                case LogLevels.NONE:     return "  NONE  ";
                case LogLevels.ERROR:    return " ERROR  ";
                case LogLevels.WARNING:  return "WARNING ";
                case LogLevels.INFO:     return "  INFO  ";
                case LogLevels.FILEINFO: return "FILEINFO";
               
                default: return "UNKNOWN";
            }
        }

        protected StreamWriter outs; //output stream
        protected int pid;
    }
}
