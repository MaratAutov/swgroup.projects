﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace aetpUpdater.Utilities
{
    static class WinGraphics
    {
        #region ApplyFormStopResizing
        public static void ApplyFormStopResizing(System.Windows.Forms.Form control)
        {
            System.Drawing.Graphics gr = control.CreateGraphics();
            float scale = 96.0F / gr.DpiX;
            gr.Dispose();
            if (scale != 1.0F)
                AdjustLabelFonts(scale, control.Controls);
        }
        #endregion

        #region AdjustLabelFonts
        private static void AdjustLabelFonts(float scale, Control.ControlCollection ctls)
        {
            foreach (Control ctl in ctls)
            {
                if (ctl is Label ||
                    ctl is TextBox ||
                    // ctl is DevExpress.XtraEditors.TextEdit || we do no use DevExpress
                    ctl is System.Windows.Forms.ComboBox ||
                    ctl is CheckBox)
                    ctl.Font = new System.Drawing.Font(ctl.Font.FontFamily, ctl.Font.Size * scale, ctl.Font.Style);
                if (ctl.Controls.Count > 0) AdjustLabelFonts(scale, ctl.Controls);
            }
        }
        #endregion
    }
}
