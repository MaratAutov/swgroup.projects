﻿using System;
using System.Linq;
using System.Collections;
using System.Security.Cryptography;

namespace updaterSeldonNTLM.Cipher
{
    public class DES
    {
        public static byte[] Create64BitKeyFrom56Bits(byte[] half, bool applyOddParity=true)
        {
            string[] binary = new string[7];
            for (int i = 0; i < 7; i++)
            {
                binary[i] = Convert.ToString(half[i], 2);
                binary[i] = new string('0', 8 - binary[i].Length) + binary[i];
            }

            string bitsString = string.Join("", binary);

            byte[] desKey = new byte[8];
            for( int i = 0; i < 8; i++ )
            {
                int bValue = 0;
                for (int j = 0; j < 7; j++)
                    bValue += bitsString[i * 7 + j] == '1' ? (int)Math.Pow(2, 7 - j) : 0;
                desKey[i] = (byte)(bValue);
            }

            if( applyOddParity )
                desKey = ApplyOddParity(desKey);

            return desKey;
        }

        private static byte[] ApplyOddParity(byte[] desKey)
        {
            int summ = 0;
            BitArray bits = new BitArray(desKey);
            for (int i = 0; i < bits.Count; i++)
            {
                if (bits[i] == true)
                    ++summ;
                if ((i + 1) % 8 == 0)
                {
                    if (summ % 2 == 0)
                        ++desKey[i / 8];
                    summ = 0;
                }
            }
            return desKey;
        }

        public static byte[] Encrypt(byte[] value, byte[] key)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider()
            {
                IV = new byte[8],
                Key = key
            };
            ICryptoTransform transform = des.CreateEncryptor();

            byte[] b = transform.TransformFinalBlock(value, 0, 8);
            byte[] result =  new byte[8];
            Array.Copy(b, result, 8);

            return result;
        }
    }
}