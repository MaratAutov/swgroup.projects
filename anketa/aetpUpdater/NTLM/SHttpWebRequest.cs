﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Runtime.Serialization;

namespace updaterSeldonNTLM
{

    public class AsyncResult : IAsyncResult
    {
        object _result;
        public object Session;

        public AsyncResult(object result)
        {
            _result = result;
        }

        public bool IsCompleted
        {
            get { return (Session as AsyncFileDownloadSession).IsCompleted; }
        }

        public WaitHandle AsyncWaitHandle
        {
            get { throw new NotImplementedException(); }
        }

        public object AsyncState
        {
            get { return _result; }
        }

        public bool CompletedSynchronously
        {
            get { return false; }
        }
    }

    internal class SWebProxy
    {
        public string Url { get; set; }
        public int Port { get; set; }
        public string User { get; set; }
        public string Pwd { get; set; }
    }

    internal class AsyncFileDownloadSession
    {
        public bool IsCompleted;

        public AsyncResult AsyncResult;
        public AsyncCallback AsyncCallback;
        public SocketAsyncEventArgs AsyncEventArgs;
        public Socket AsyncSocket;
        public int AsyncReceived;
        public byte[] AsyncBuffer;
        private byte[] receiveBuffer;

        public AsyncFileDownloadSession(AsyncCallback callback, object state)
        {
            AsyncCallback = callback;
            AsyncResult = new AsyncResult(state);
            AsyncResult.Session = this;

            using( Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp) )
            {
                AsyncBuffer = new byte[sock.ReceiveBufferSize * 16];
                receiveBuffer = new byte[sock.ReceiveBufferSize * 16];
            }
            AsyncEventArgs = new SocketAsyncEventArgs();
            AsyncEventArgs.Completed += SockAsyncEventArgs_Completed;
            AsyncEventArgs.SetBuffer(receiveBuffer, 0, receiveBuffer.Length);

            AsyncReceived = 0;
            IsCompleted = false;
        }

        public void PostReceive()
        {
            bool raiseEvent = AsyncSocket.ReceiveAsync(AsyncEventArgs);
            if( !raiseEvent )
                throw new Exception(raiseEvent.ToString());
        }

        public void SockAsyncEventArgs_Completed(object sender, SocketAsyncEventArgs e)
        {
            if( e.LastOperation == SocketAsyncOperation.Receive && e.BytesTransferred != 0 )
            {
                long offset = AsyncReceived;
                AsyncReceived += e.BytesTransferred;

                Array.Resize(ref AsyncBuffer, AsyncReceived);
                Array.Copy(e.Buffer, 0, AsyncBuffer, offset, e.BytesTransferred);

                SHttpWebResponse tmp = new SHttpWebResponse(AsyncBuffer, AsyncReceived);
                if( tmp.Complete )
                {
                    IsCompleted = true;
                    AsyncCallback.Invoke(AsyncResult);
                }
                else
                    PostReceive();
            }
        }
    }

    public class SeldonWebRequestCreator : IWebRequestCreate
    {
        public WebRequest Create(Uri uri)
        {
            SHttpWebRequest request = new SHttpWebRequest(
                new Uri(uri.AbsoluteUri.Replace("seldon-http", "http"))
            );
            request.Proxy = WebRequest.DefaultWebProxy;
            return (WebRequest)request;
        }
    }

    public class SHttpWebRequest : WebRequest
    {
        internal Socket _proxySocket;
        internal byte[] _streamBuffer;
        internal byte[] _responseBuffer;
        internal int _responseReceived;

        internal void Init(Uri uri)
        {
            if (uri.Scheme.EndsWith("https"))
                throw new WebException("(501) Legacy NTLM over HTTPS is not supported", WebExceptionStatus.ProtocolError);

            Uri requestUri;
            Match m = _sessionPattern.Match(uri.AbsoluteUri);

            if (Session == null)
                Session = new Hashtable();

            string sKey = String.Format("{0}:{1}", uri.Host, uri.Port);
            if (Session[sKey] != null && !m.Success)
                requestUri = new Uri(
                    uri.AbsoluteUri.Replace(
                        uri.Host, uri.Host + "/" + Session[sKey]
                    )
                );
            else
                requestUri = uri;

            _requestUri = requestUri;
            _contentType = "text/html";
            using (Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                _streamBuffer = new byte[s.SendBufferSize * 16];
                _responseBuffer = new byte[s.ReceiveBufferSize * 16];
            }
        }

        public SHttpWebRequest(Uri uri)
        {
            Init(uri);

            Headers = new WebHeaderCollection();
            Headers.Add("Host", uri.Host + ":" + uri.Port);
            Method = "GET";
        }

        public SHttpWebRequest(HttpWebRequest webRequest)
        {
            Init(webRequest.RequestUri);

            Headers = webRequest.Headers;
            Method = webRequest.Method;
        }

        internal static Regex _sessionPattern = new Regex(@"\(S\([a-z0-9]+\)\)");
        public static Hashtable Session { get; set; }

        public override string ConnectionGroupName { get; set; }
        public override long ContentLength { get; set; }

        internal string _contentType;
        public override string ContentType
        {
            get
            {
                return _contentType;
            }
            set
            {
                _contentType = value;
            }
        }

        public override ICredentials Credentials { get; set; }
        public override WebHeaderCollection Headers { get; set; }

        internal string _method;
        public override string Method
        {
            get
            {
                return _method;
            }
            set
            {
                _method = value.ToUpper();
            }
        }

        public override bool PreAuthenticate { get; set; }

        internal SWebProxy _simpleProxy;
        internal IWebProxy _proxy;
        public override IWebProxy Proxy
        {
            get
            {
                return _proxy;
            }
            set
            {
                if (value == null)
                    return;

                _proxy = value;
                if ((_proxy as WebProxy) == null)
                {
                    _proxy = null;
                    return;
                }
                Uri address = (_proxy as WebProxy).Address;
                NetworkCredential cred = _proxy.Credentials.GetCredential(_requestUri, "NTLM");
                _simpleProxy = new SWebProxy()
                {
                    Port = address.Port,
                    Url = address.Host,
                    User = cred.UserName,
                    Pwd = cred.Password
                };
            }
        }

        internal bool m_keepAlive;
        public bool KeepAlive
        {
            get
            {
                return m_keepAlive;
            }
            set
            {
                m_keepAlive = value;
                if (m_keepAlive)
                    Headers.Add("Connection", "Keep-Alive");
                else if (Headers["Connection"] != null)
                    Headers["Connection"] = "Close";
            }
        }

        internal Uri _requestUri;
        public override Uri RequestUri
        {
            get
            {
                return _requestUri;
            }
        }

        public override int Timeout { get; set; }
        public override System.Net.Cache.RequestCachePolicy CachePolicy { get; set; }
        public override bool UseDefaultCredentials { get; set; }

        public override void Abort()
        {
            // TODO: ?
        }

        public override IAsyncResult BeginGetRequestStream(AsyncCallback callback, object state)
        {
            return base.BeginGetRequestStream(callback, state);
        }

        public override Stream EndGetRequestStream(IAsyncResult asyncResult)
        {
            return base.EndGetRequestStream(asyncResult);
        }

        public override IAsyncResult BeginGetResponse(AsyncCallback callback, object state)
        {
            AsyncFileDownloadSession download = new AsyncFileDownloadSession(
                callback, state
            );

            InitSocket(ref download.AsyncSocket, RequestUri);
            if( _authHeaders == null && _proxy != null )
            {
                GetAuthorizedResponse(
                    RequestUri, _simpleProxy.User, _simpleProxy.Pwd, ref download.AsyncSocket, true
                );
                InitSocket(ref download.AsyncSocket, RequestUri);
            }
            SendRequest(download.AsyncSocket, RequestUri);

            download.PostReceive();
           
            return download.AsyncResult;
        }

        public override WebResponse EndGetResponse(IAsyncResult asyncResult)
        {
            AsyncFileDownloadSession download = ((asyncResult as AsyncResult).Session as AsyncFileDownloadSession);
            SHttpWebResponse response = new SHttpWebResponse(download.AsyncBuffer, download.AsyncReceived);

            if (response.Headers["Proxy-Connection"] != null &&
                response.Headers["Proxy-Connection"].ToLower() == "close")
            {
                download.AsyncSocket.Disconnect(true);
                download.AsyncSocket = null;
            }

            return response;
        }

        public override Stream GetRequestStream()
        {
            return new MemoryStream(_streamBuffer);
        }

        public override WebResponse GetResponse()
        {
            SHttpWebResponse response = GetSResponse(_requestUri, ref _proxySocket);
            if( response.StatusCode == HttpStatusCode.ProxyAuthenticationRequired )
            {
                response = GetAuthorizedResponse(
                    _requestUri, _simpleProxy.User, _simpleProxy.Pwd, ref _proxySocket
                );
            }

            WebExceptionStatus? _webExcStatus = null;
            switch (response.StatusCode)
            {
                case HttpStatusCode.Moved:
                case HttpStatusCode.Redirect:
                case HttpStatusCode.RedirectKeepVerb:
                case HttpStatusCode.RedirectMethod:
                    Match m = _sessionPattern.Match(response.Headers["Location"]);
                    if (m.Success)
                        Session[String.Format("{0}:{1}", _requestUri.Host, _requestUri.Port)] = m.Value;

                    Uri redirectUri = new Uri(
                        string.Format(
                            "{0}://{1}/{2}",
                            RequestUri.Scheme,
                            RequestUri.Host,
                            response.Headers["Location"].TrimStart('/')
                        )
                    );
                    return GetSResponse(redirectUri, ref _proxySocket);
                case HttpStatusCode.OK:
                    return response;
                case HttpStatusCode.NotFound:
                case HttpStatusCode.ServiceUnavailable:
                    if (response.StatusCode == HttpStatusCode.NotFound)
                        _webExcStatus = WebExceptionStatus.NameResolutionFailure;
                    if (response.Headers["X-Squid-Error"] != null &&
                        response.Headers["X-Squid-Error"].Contains("ERR_DNS_FAIL"))
                        _webExcStatus = WebExceptionStatus.ConnectFailure;
                    break;
                case HttpStatusCode.BadRequest:
                    if (response.Headers["X-Squid-Error"] != null &&
                        response.Headers["X-Squid-Error"].Contains("ERR_INVALID_URL"))
                        _webExcStatus = WebExceptionStatus.NameResolutionFailure;
                    break;
                default:
                    _webExcStatus = WebExceptionStatus.ProtocolError;
                    break;
            }
            if (_webExcStatus != null)
                throw new WebException(
                    string.Format(
                        "Удаленный сервер вернул ошибку: ({0}) {1}",
                        (int)response.StatusCode,
                        response.StatusCode.ToString()
                    ),
                    null,
                    (WebExceptionStatus)_webExcStatus,
                    response
                );
            return response;
        }

        internal void InitSocket(ref Socket socket, Uri requestUri)
        {
            if( socket == null )//|| !socket.Connected )
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.ReceiveBufferSize *= 16;
                socket.SendTimeout = 10000000;
                socket.ReceiveTimeout = 10000000;
                if( _proxy != null )
                    socket.Connect(_simpleProxy.Url, _simpleProxy.Port);
                else
                    socket.Connect(requestUri.Host, requestUri.Port);
            }
        }

        internal String GetHeadersString(WebHeaderCollection headers)
        {
            String result = "";
            foreach( string key in headers )
            {
                result += String.Format(
                    "{0}: {1}\r\n",
                    key, headers[key]
                );
            }
            return result;
        }

        private int SendRequest(Socket connection, Uri requestUri, WebHeaderCollection injectedHeaders = null)
        {
            bool isMethodConnect = Method.Equals("CONNECT");
            bool isMethodPost    = Method.Equals("POST");

            string uri = isMethodConnect ? requestUri.Host + ":" + requestUri.Port : requestUri.AbsoluteUri;
            string request = String.Format(
                "{0} {1} HTTP/1.1\r\n",
                Method,
                uri
            );

            if( !isMethodConnect )
                request += GetHeadersString(Headers);
            if( injectedHeaders != null )
                request += GetHeadersString(injectedHeaders);
            if( _authHeaders != null )
                request += GetHeadersString(_authHeaders);

            if( isMethodPost )
            {
                request += String.Format("Content-Length: {0}\r\n", ContentLength);
                request += String.Format("Content-Type: {0}\r\n", "application/x-www-form-urlencoded");
            }
            request += "\r\n";

            byte[] data;
            Encoding headEncoding = Encoding.GetEncoding(1251);
            if( isMethodPost )
            {
                int HeadLength = headEncoding.GetByteCount(request);
                data = new byte[HeadLength + ContentLength];
                headEncoding.GetBytes(request).CopyTo(data, 0);
                Array.Copy(_streamBuffer, 0, data, HeadLength, ContentLength);
            }
            else
                data = headEncoding.GetBytes(request);

            return connection.Send(data);           
        }

        private SHttpWebResponse GetSResponse(Uri requestUri, ref Socket connection, WebHeaderCollection injectedHeaders=null)
        {
            /*
            if (_proxySocket == null || !_proxySocket.Connected)
            {
                _proxySocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _proxySocket.SendTimeout = 10000000;
                _proxySocket.ReceiveTimeout = 10000000;
                if (_proxy != null)
                    _proxySocket.Connect(_simpleProxy.Url, _simpleProxy.Port);
                else
                    _proxySocket.Connect(requestUri.Host, requestUri.Port);
            }*/
            InitSocket(ref connection, requestUri);

            SendRequest(connection, requestUri, injectedHeaders);

            _responseReceived = 0;
            while (true)
            {
                int readed = connection.Receive(
                    _responseBuffer,
                    _responseReceived,
                    connection.ReceiveBufferSize,
                    SocketFlags.None
                );

                _responseReceived += readed;
                Array.Resize(ref _responseBuffer, _responseReceived + connection.ReceiveBufferSize);

                using( SHttpWebResponse tmp = new SHttpWebResponse(_responseBuffer, _responseReceived) )
                {
                    if( tmp.Complete )
                        break;
                }
            }

            SHttpWebResponse webResponse = new SHttpWebResponse(_responseBuffer, _responseReceived);
            if( webResponse.Headers["Proxy-Connection"] != null &&
                webResponse.Headers["Proxy-Connection"].ToLower() == "close" )
            {
                connection.Disconnect(true);
                connection = null;
            }

            webResponse._responseUri = requestUri;
            return webResponse;
        }

        private WebHeaderCollection _authHeaders;
        public SHttpWebResponse GetAuthorizedResponse(Uri requestUri, string userName, string userPass, ref Socket connection, bool authorizationOnly=false)
        {
            string method = Method;
            Method = "CONNECT";
            SHttpWebResponse response = GetSResponse(requestUri, ref connection);
            if( response.StatusCode == HttpStatusCode.OK )
            {
                Method = method;
                return response;
            }

            WebHeaderCollection webHeaders = new WebHeaderCollection();
            string msg1 = new NTLM.Message1().GetString();

            webHeaders.Add("Proxy-Connection", "Keep-Alive");
            webHeaders.Add(
                "Proxy-Authorization",
                 string.Format("NTLM {0}", msg1)
            );

            SHttpWebResponse resp1 = GetSResponse(requestUri, ref connection, webHeaders);
            NTLM.Message2 msg2 = new NTLM.Message2(resp1.Headers["Proxy-Authenticate"] as String);

            string msg3 = new NTLM.Message3(msg2, userName, userPass).GetString();
            webHeaders["Proxy-Authorization"] = string.Format("NTLM {0}", msg3);

            SHttpWebResponse result = GetSResponse(requestUri, ref connection, webHeaders);
            Method = method;

            if( authorizationOnly || result.StatusCode == HttpStatusCode.NotFound )
            {
                _authHeaders = webHeaders;
                return result;
            }
            else
                return GetSResponse(requestUri, ref connection, webHeaders);
        }
    }
}
