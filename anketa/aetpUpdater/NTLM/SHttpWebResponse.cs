﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace updaterSeldonNTLM
{
    public class SHttpWebResponse : WebResponse
    {
        internal Regex response_head = new Regex(@"^HTTP/(?<version>\d\.\d)\s+(?<code>\d+)"); 
        internal byte[] responseBuffer;
        
        public string Version { get; set; }
        public string Content { get; set; }
        public HttpStatusCode StatusCode { get; set; }

        internal WebHeaderCollection _headers;
        public override WebHeaderCollection Headers
        {
            get
            {
                return _headers;
            }
        }

        public bool isChunked
        {
            get
            {
                return ( Headers != null && Headers["Transfer-Encoding"] != null &&
                         Headers["Transfer-Encoding"].ToLower() == "chunked" );
            }
        }

        public bool hasAllChunks
        {
            get
            {
                return ( Content != null && Content.EndsWith("0\r\n\r\n") );
            }
        }

        public bool Complete
        {
            get
            {
                if( isChunked && hasAllChunks )
                    return true;

                long sz = HeadSize + 4 + ContentLength;

                if( !isChunked && responseBuffer.LongLength >= sz )
                    return true;

                return false;
            }
        }

        internal long? _contentLength = null;
        public override long ContentLength
        {
            get
            {
                if( Headers != null && Headers["Content-Length"] != null )
                    return Convert.ToInt64(Headers["Content-Length"]);
                else
                    return _contentLength ?? Encoding.UTF8.GetByteCount(Content ?? "");
            }
            set
            {
                _contentLength = value;
            }
        }

        public override string ContentType
        {
            get
            {
                if( Headers != null && Headers["Content-Type"] != null )
                    return Headers["Content-Type"];
                return null;
            }
            set
            {
                if( Headers != null )
                    Headers["Content-Type"] = value;
            }
        }

        public override bool IsMutuallyAuthenticated
        {
            get
            {
                return base.IsMutuallyAuthenticated;
            }
        }

        public override bool IsFromCache
        {
            get
            {
                return base.IsFromCache;
            }
        }

        private Stream stream;

        public SHttpWebResponse(WebResponse webResponse)
        {
            HttpWebResponse resp = (HttpWebResponse)webResponse;
            StatusCode = resp.StatusCode;
            _headers = resp.Headers;
            using (Stream stream = resp.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
                Content = reader.ReadToEnd();
        }

        public long HeadSize = 0;
        public SHttpWebResponse(byte[] buffer, int length)
        {
            responseBuffer = new byte[length];
            Array.Copy(buffer, responseBuffer, length);

            string data = Encoding.UTF8.GetString(buffer, 0, length);
            if( length == 0 )
                return;

            string[] raw = data.Split(new string[] { "\r\n\r\n" }, 2, StringSplitOptions.None);
            string header = raw[0];
            HeadSize = Encoding.UTF8.GetByteCount(header);
            if( raw.Length == 2 )
            {
                Content = raw[1];
                _contentLength = length - HeadSize - 4;
            }

            _headers = new WebHeaderCollection();
            foreach( string line in header.Split(new string[] { "\r\n" }, StringSplitOptions.None) )
            {
                if( line.StartsWith("HTTP") )
                {
                    Match m = response_head.Match(line);
                    Version = m.Groups["version"].Value;
                    StatusCode = (HttpStatusCode)Convert.ToInt32(m.Groups["code"].Value);
                    continue;
                }
                string[] arr = line.Split(':');
                _headers.Add(arr[0].Trim(), arr[1].Trim());
            }
        }

        public override Stream GetResponseStream()
        {
            string data = "";
            if( isChunked && Content != null )
            {
                string[] lines = Content.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                for( int i = 0; i < lines.Length - 1; i += 2 )
                {
                    data += lines[i + 1];
                }
                data = data.Trim();
                stream = new MemoryStream(
                    Encoding.UTF8.GetBytes(data)
                );
            }
            else
            {
                stream = new MemoryStream(responseBuffer, (int)HeadSize + 4, (int)ContentLength);
            }
            return stream;
        }

        public override void Close()
        {
            if( stream != null )
            {
                stream.Close();
                stream = null;
            }
        }

        internal Uri _responseUri;
        public override Uri ResponseUri
        {
            get
            {
                return _responseUri;
            }
        }
    }
}
