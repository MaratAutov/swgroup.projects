﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace updaterSeldonNTLM.NTLM
{
    static class BinaryReaderExtension
    {
        public static string ReadNTLMSignature(this BinaryReader bReader)
        {
            bReader.BaseStream.Position = 0;
            return Encoding.ASCII.GetString(bReader.ReadBytes(8));
        }

        public static MESSAGE_TYPE ReadNTLMMessageType(this BinaryReader bReader)
        {
            bReader.BaseStream.Position = 8;
            return (MESSAGE_TYPE)Utils.ReverseBytes(bReader.ReadUInt32());
        }

        public static FLAGS ReadNTLMFlags(this BinaryReader bReader, int offset)
        {
            bReader.BaseStream.Position = offset;
            return (FLAGS)(bReader.ReadUInt32());
        }

        public static void ReadNTLMSecurityBuffer(this BinaryReader bReader, ref SecurityBuffer buff, int offset)
        {
            bReader.BaseStream.Position = offset;
            buff.Length = bReader.ReadInt16();
            buff.AllocatedSpace = bReader.ReadInt16();
            buff.Offset = bReader.ReadInt32();
        }

        public static byte[] ReadNTLMSecurityBufferContent(this BinaryReader bReader, SecurityBuffer buff)
        {
            bReader.BaseStream.Position = buff.Offset;
            return bReader.ReadBytes(buff.Length);
        }
    }
}
