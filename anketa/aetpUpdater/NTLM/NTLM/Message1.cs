﻿using System;
using System.IO;
using System.Text;

namespace updaterSeldonNTLM.NTLM
{
    class Message1 : Message
    {
        public Message1()
        {
            MsgType = MESSAGE_TYPE.TYPE_1;
            Flags   = FLAGS.NEGOTIATE_UNICODE | 
                      FLAGS.NEGOTIATE_OEM     |
                      FLAGS.REQUEST_TARGET    |
                      FLAGS.NEGOTIATE_NTLM    |
                      FLAGS.NEGOTIATE_ALWAYS_SIGN;
        }

        public string GetString()
        {
            byte[] msg1 = new byte[48];

            MemoryStream stream = new MemoryStream(msg1);
            BinaryWriter bWriter = new BinaryWriter(stream);

            // 0/8 -- NTLMSSP Signature (Null-terminated ASCII "NTLMSSP")
            bWriter.WriteNTLMSignature(Signature);

            // 8/4 -- NTLM Message Type
            bWriter.WriteNTLMMessageType(MsgType);

            // 12/4 -- Flags (uint)
            bWriter.WriteNTLMFlags(Flags, 12);

            // 32/8 -- OS Version structure
            bWriter.WriteNTLMOsVersion(32);

            return Encode(msg1);
        }
    }
}
