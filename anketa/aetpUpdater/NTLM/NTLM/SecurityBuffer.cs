﻿using System;
using System.IO;

namespace updaterSeldonNTLM.NTLM
{
    class SecurityBuffer
    {
        public Int16 Length;
        public Int16 AllocatedSpace;
        public Int32 Offset;

        public byte[] Content;

        public SecurityBuffer()
        {
        }

        public SecurityBuffer(ref int offset, byte[] data, int allocated = -1)
        {
            Offset = offset;
            Length = (Int16)data.Length;
            AllocatedSpace = (Int16)((allocated == -1) ? Length : allocated);
            Content = data;

            offset += Length;
        }
    }
}
