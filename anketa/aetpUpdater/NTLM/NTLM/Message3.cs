﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace updaterSeldonNTLM.NTLM
{
    class Message3 : Message
    {
        Message2 Msg2;

        SecurityBuffer LMResponseBlock;
        SecurityBuffer NTLMResponseBlock;
        SecurityBuffer TargetBlock;
        SecurityBuffer UserBlock;
        SecurityBuffer WorkstationBlock;
        SecurityBuffer SessionKeyBlock;

        string Workstation;
        byte[] lmHash;

        bool includeSessionKey = true;
        bool includeFlags      = true;
        bool includeOSVersion  = false;

        int  DataOffset;
        int  MsgSize;

        public Message3(Message2 msg2, string userName, string userPwd)
        {
            MsgType = MESSAGE_TYPE.TYPE_3;
            Msg2 = msg2;

            // Mirroring server message flags
            Flags = Msg2.Flags;

            DataOffset = 52;
            if( includeSessionKey )
                DataOffset += 8;
            if( includeFlags )
                DataOffset += 4;
            if( includeOSVersion )
                DataOffset += 8;

            userName = userName.ToUpper();

            byte[] LMData = LMResponse(userPwd);
            byte[] NTLMData = NTLMResponse(userPwd);

            Workstation = System.Environment.MachineName;

            // /!\ Order of SecurityBuffer creation defines content position 
            // in the message data block ( not the actual blocks position in
            // the message header block ).
            TargetBlock = new SecurityBuffer(ref DataOffset, GetStringBytes(Msg2.Target));
            UserBlock = new SecurityBuffer(ref DataOffset, GetStringBytes(userName));
            WorkstationBlock = new SecurityBuffer(ref DataOffset, GetStringBytes(Workstation));
            LMResponseBlock = new SecurityBuffer(ref DataOffset, LMData);
            NTLMResponseBlock = new SecurityBuffer(ref DataOffset, NTLMData);

            if( includeSessionKey )
            {
                // First 8 bytes of lmHash zero-padded to 16 bytes
                byte[] sessionKey = new byte[16];
                Array.Copy(lmHash, sessionKey, 8);

                SessionKeyBlock = new SecurityBuffer(ref DataOffset, sessionKey);
            }

            MsgSize = DataOffset;
        }

        private byte[] LMResponse(string userPass)
        {
            // The user's password (OEM string) is converted to uppercase.
            byte[] pwd = Encoding.ASCII.GetBytes(userPass.ToUpper());

            lmHash = Hash.ComputeLMHash(pwd);
            return Hash.ComputeLMHash(lmHash, Msg2.Challenge);
        }

        private byte[] NTLMResponse(string userPass)
        {
            // The user's password (Unicode string)
            byte[] pwd = Encoding.Unicode.GetBytes(userPass);

            byte[] ntlmHash = Hash.ComputeNTLMHash(pwd);
            return Hash.ComputeLMHash(ntlmHash, Msg2.Challenge);
        }

        private byte[] LMv2Response(string userName, string userPass)
        {
            /*
            userPass = "SecREt01";
            Msg2.Challenge = new byte[8] { 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef };
            Msg2.Target = "DOMAIN";
            string userName = "user";
            */

            // The user's password (Unicode string)
            byte[] pwd = Encoding.Unicode.GetBytes(userPass);
            byte[] ntlmHash = Hash.ComputeNTLMHash(pwd);

            string tmp = userName.ToUpper() + Msg2.Target;
            HMACMD5 hmacMD5 = new HMACMD5(ntlmHash);
            byte[] ntlmv2Hash = hmacMD5.ComputeHash(Encoding.Unicode.GetBytes(tmp));

            byte[] nonce = new byte[8] { 0xff, 0xff, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44 };

            byte[] challenge = new byte[Msg2.Challenge.Length + nonce.Length];
            Msg2.Challenge.CopyTo(challenge, 0);
            nonce.CopyTo(challenge, Msg2.Challenge.Length);

            hmacMD5.Key = ntlmv2Hash;
            byte[] lmv2Hash = hmacMD5.ComputeHash(challenge);

            byte[] lmv2Response = new byte[24];
            lmv2Hash.CopyTo(lmv2Response, 0);
            nonce.CopyTo(lmv2Response, lmv2Hash.Length);

            return lmv2Response;
        }

        private byte[] CreateBlob(byte[] nonce, byte[] targetInfo)
        {
            byte[] blob = new byte[32 + targetInfo.Length];
            byte[] signature = new byte[4] { 0x10, 0x10, 0x00, 0x00 };
            byte[] timestamp = BitConverter.GetBytes(
                Utils.ReverseBytes((UInt64)DateTime.Now.ToBinary())
            );

            signature.CopyTo(blob, 0);
            timestamp.CopyTo(blob, 8);
            nonce.CopyTo(blob, 16);
            targetInfo.CopyTo(blob, 28);

            return blob;
        }

        private byte[] NTLMv2Response(string userName, string userPass)
        {
            // The user's password (Unicode string)
            byte[] pwd = Encoding.Unicode.GetBytes(userPass);
            byte[] ntlmHash = Hash.ComputeNTLMHash(pwd);

            string tmp = userName.ToUpper() + Msg2.Target;
            HMACMD5 hmacMD5 = new HMACMD5(ntlmHash);
            byte[] ntlmv2Hash = hmacMD5.ComputeHash(Encoding.Unicode.GetBytes(tmp));

            byte[] nonce = new byte[8] { 0xff, 0xff, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44 };

            byte[] blob = CreateBlob(nonce, Msg2.TargetInfo);

            byte[] challenge = new byte[Msg2.Challenge.Length + blob.Length];
            Msg2.Challenge.CopyTo(challenge, 0);
            blob.CopyTo(challenge, Msg2.Challenge.Length);

            hmacMD5.Key = ntlmv2Hash;
            byte[] lmv2Hash = hmacMD5.ComputeHash(challenge);

            byte[] ntlmv2Response = new byte[lmv2Hash.Length + blob.Length];
            lmv2Hash.CopyTo(ntlmv2Response, 0);
            blob.CopyTo(ntlmv2Response, lmv2Hash.Length);

            return ntlmv2Response;
        }

        public string GetString()
        {
            byte[] msg = new byte[MsgSize];

            MemoryStream stream = new MemoryStream(msg);
            BinaryWriter bWriter = new BinaryWriter(stream);

            // 0/8 -- NTLMSSP Signature (Null-terminated ASCII "NTLMSSP")
            bWriter.WriteNTLMSignature(Signature);

            // 8/4 -- NTLM Message Type
            bWriter.WriteNTLMMessageType(MsgType);

            // 12/8 -- LM Response security buffer
            bWriter.WriteNTLMSecurityBuffer(LMResponseBlock, 12);

            // 20/8 -- NTLM Response security buffer
            bWriter.WriteNTLMSecurityBuffer(NTLMResponseBlock, 20);

            // 28/8 -- Target Name security buffer
            bWriter.WriteNTLMSecurityBuffer(TargetBlock, 28);

            // 36/8 -- User Name security buffer
            bWriter.WriteNTLMSecurityBuffer(UserBlock, 36);

            // 44/8 -- Workstation Name security buffer
            bWriter.WriteNTLMSecurityBuffer(WorkstationBlock, 44);

            // 52/8 -- Session Key security buffer
            if( includeSessionKey )
                bWriter.WriteNTLMSecurityBuffer(SessionKeyBlock, 52);

            // 60/4 -- Flags
            if( includeFlags )
                bWriter.WriteNTLMFlags(Flags, 60);

            // 64/8 -- OS Version structure
            if( includeOSVersion )
                bWriter.WriteNTLMOsVersion(64);

            // 52(64)(72)/? -- data block
            bWriter.WriteNTLMSecurityBufferContent(TargetBlock);

            bWriter.WriteNTLMSecurityBufferContent(UserBlock);

            bWriter.WriteNTLMSecurityBufferContent(WorkstationBlock);

            bWriter.WriteNTLMSecurityBufferContent(LMResponseBlock);

            bWriter.WriteNTLMSecurityBufferContent(NTLMResponseBlock);

            if( includeSessionKey )
                bWriter.WriteNTLMSecurityBufferContent(SessionKeyBlock);

            return Encode(msg);
        }
    }
}
