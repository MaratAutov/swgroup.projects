﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using updaterSeldonNTLM.Cipher;

namespace updaterSeldonNTLM.NTLM
{
    public static class Hash
    {
        // "KGS!@#$%"
        private static readonly byte[] ZeroChallenge = { 0x4B, 0x47, 0x53, 0x21, 0x40, 0x23, 0x24, 0x25 };

        private static int GetPaddingSize(byte[] str)
        {
            int a = str.Length / 7;
            int b = str.Length % 7;

            return b == 0 ? a : a + 1;
        }

        public static byte[] ComputeNTLMHash(byte[] data)
        {
            MD4 md4 = new MD4();
            return md4.ComputeHash(data);
        }

        public static byte[] ComputeLMHash(byte[] data, byte[] encryptionString = null)
        {
            // 2. Base string is null-padded to 14 or 21 bytes
            // nearest integer % 7 bigger then baseString.Length
            int partsNumber = GetPaddingSize(data);
            Array.Resize(ref data, partsNumber * 7);

            byte[] cipherText = new byte[partsNumber * 8];
            // 3. This "fixed" password is split into 7-byte parts.           
            for (int i = 0; i < partsNumber; i++)
            {
                byte[] part = new byte[7];
                Array.Copy(data, i * 7, part, 0, 7);

                // 4. These values are used to create DES keys. A DES key is
                // 8 bytes long; each byte contains seven bits of key material
                // and one odd-parity bit (the parity bit may or may not
                // be checked, depending on the underlying DES implementation).
                byte[] desKey = DES.Create64BitKeyFrom56Bits(part);

                // 5. Each of these keys is used to DES-encrypt the constant ASCII
                // string "KGS!@#$%" (resulting in two 8-byte ciphertext values).
                byte[] cipherPart = DES.Encrypt(
                    encryptionString ?? ZeroChallenge, desKey
                );
                Array.Copy(cipherPart, 0, cipherText, i * 8, 8);
            }

            // 6. Joined cipherParts is our LM hash
            return cipherText;
        }
    }
}
