﻿using System;
using System.IO;
using System.Text;

namespace updaterSeldonNTLM.NTLM
{
    public enum MESSAGE_TYPE: uint
    {
        TYPE_1 = 0x01000000,
        TYPE_2 = 0x02000000,
        TYPE_3 = 0x03000000
    }

    class Message
    {
        public string       Signature = "NTLMSSP\0";
        public FLAGS        Flags;
        public MESSAGE_TYPE MsgType;

        public static string Encode(byte[] toEncode)
        {
            return Convert.ToBase64String(toEncode);
        }

        public bool HasFlags(FLAGS flag)
        {
            return (Flags & flag) == flag;
        }

        public byte[] GetStringBytes(string str)
        {
            if( HasFlags(FLAGS.NEGOTIATE_OEM) )
                return Encoding.ASCII.GetBytes(str);
            if( HasFlags(FLAGS.NEGOTIATE_UNICODE) )
                return Encoding.Unicode.GetBytes(str);
            else
                throw new InvalidDataException("NTLM_FLAGS: Encoding not set");
        }

        public string GetEncodedString(byte[] bytes)
        {
            if( HasFlags(FLAGS.NEGOTIATE_OEM) )
                return Encoding.ASCII.GetString(bytes);
            if( HasFlags(FLAGS.NEGOTIATE_UNICODE) )
                return Encoding.Unicode.GetString(bytes);
            else
                throw new InvalidDataException("NTLM_FLAGS: Encoding not set");
        }
    }
}
