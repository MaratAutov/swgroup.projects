﻿using System;
using System.IO;
using System.Text;

namespace updaterSeldonNTLM.NTLM
{
    static class BinaryWriterExtension
    {
        public static void WriteNTLMSignature(this BinaryWriter bWriter, string signature)
        {
            bWriter.Seek(0, SeekOrigin.Begin);
            bWriter.Write(Encoding.ASCII.GetBytes(signature));
        }

        public static void WriteNTLMMessageType(this BinaryWriter bWriter, MESSAGE_TYPE msgType)
        {
            bWriter.Seek(8, SeekOrigin.Begin);
            bWriter.Write(Utils.ReverseBytes((uint)msgType));
        }

        public static void WriteNTLMFlags(this BinaryWriter bWriter, FLAGS flags, int offset)
        {
            bWriter.Seek(offset, SeekOrigin.Begin);
            bWriter.Write((uint)flags);
        }

        public static void WriteNTLMOsVersion(this BinaryWriter bWriter, int offset)
        {
            bWriter.Seek(offset, SeekOrigin.Begin);
            bWriter.Write((byte)Environment.OSVersion.Version.Major);
            bWriter.Write((byte)Environment.OSVersion.Version.Minor);
            bWriter.Write((short)Environment.OSVersion.Version.Build);
            bWriter.Write(0x0000000f); // Unknown/Reserved
        }

        public static void WriteNTLMSecurityBuffer(this BinaryWriter bWriter, SecurityBuffer buff, int offset)
        {
            bWriter.Seek(offset, SeekOrigin.Begin);
            bWriter.Write(buff.Length);
            bWriter.Write(buff.AllocatedSpace);
            bWriter.Write(buff.Offset);
        }

        public static void WriteNTLMSecurityBufferContent(this BinaryWriter bWriter, SecurityBuffer buff)
        {
            bWriter.Seek(buff.Offset, SeekOrigin.Begin);
            bWriter.Write(buff.Content, 0, buff.Length);
        }
    }
}
