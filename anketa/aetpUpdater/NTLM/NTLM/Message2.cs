﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace updaterSeldonNTLM.NTLM
{
    class Message2 : Message
    {
        SecurityBuffer TargetBlock;
        SecurityBuffer TargetInformationBlock;

        public string  Target;
        public byte[]  Challenge;
        public byte[]  Context;
        public byte[]  TargetInfo;

        public Message2(string authHeader)
        {
            MsgType = MESSAGE_TYPE.TYPE_2;
            TargetBlock = new SecurityBuffer();
            TargetInformationBlock = new SecurityBuffer();
            Challenge = new byte[8];
            Context = new byte[8];

            ParseAuthHeader(authHeader);
        }

        public bool ParseAuthHeader(string authHeader)
        {
            if( string.IsNullOrEmpty(authHeader) || !authHeader.StartsWith("NTLM ") )
                return false;

            string base64string = authHeader.Substring(5);
            byte[] data = Convert.FromBase64String(base64string);

            MemoryStream stream = new MemoryStream(data);
            BinaryReader bReader = new BinaryReader(stream);

            // 0/8 -- NTLMSSP Signature (Null-terminated ASCII "NTLMSSP")
            if( bReader.ReadNTLMSignature() != Signature )
                return false;

            // 8/4 -- NTLM Message Type
            if( bReader.ReadNTLMMessageType() != MsgType )
                return false;

            // 12/8 -- Target Name
            bReader.ReadNTLMSecurityBuffer(ref TargetBlock, 12);

            // 20/4 -- Flags
            Flags = bReader.ReadNTLMFlags(20);

            // 24/8 -- Challenge
            Challenge = bReader.ReadBytes(8);

            // 32/8 -- Context
            //Context = bReader.ReadBytes(8);

            // 40/8 -- Target Information security buffer
            bReader.ReadNTLMSecurityBuffer(ref TargetInformationBlock, 40);

            // Target
            byte[] targetBytes = bReader.ReadNTLMSecurityBufferContent(TargetBlock);
            Target = GetEncodedString(targetBytes);

            // TargetInformation
            TargetInfo = bReader.ReadNTLMSecurityBufferContent(TargetInformationBlock);

            return true;
        }
    }
}
