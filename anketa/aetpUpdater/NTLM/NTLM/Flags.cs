﻿using System;

namespace updaterSeldonNTLM.NTLM
{
    [Flags]
    public enum FLAGS : uint
    {
        NEGOTIATE_UNICODE = 0x00000001, // The client sets this flag to indicate that it supports Unicode strings.
        NEGOTIATE_OEM = 0x00000002, // This is set to indicate that the client supports OEM strings.
        REQUEST_TARGET = 0x00000004, // This requests that the server send the authentication target with the Type 2 reply.
        NEGOTIATE_NTLM = 0x00000200, // Indicates that NTLM authentication is supported.
        NEGOTIATE_DOMAIN_SUPPLIED = 0x00001000, // When set, the client will send with the message the name of the domain in which the workstation has membership.
        NEGOTIATE_WORKSTATION_SUPPLIED = 0x00002000, // Indicates that the client is sending its workstation name with the message.
        NEGOTIATE_ALWAYS_SIGN = 0x00008000, // Indicates that communication between the client and server after authentication should carry a "dummy" signature.
        NEGOTIATE_NTLM2_KEY = 0x00080000, // Indicates that this client supports the NTLM2 signing and sealing scheme; if negotiated, this can also affect the response calculations.
        NEGOTIATE_128 = 0x20000000, // Indicates that this client supports strong (128-bit) encryption.
        NEGOTIATE_56 = 0x80000000, // Indicates that this client supports medium (56-bit) encryption.
        NEGOTIATE_LOCAL_CALL = 0x00004000, // The server sets this flag to inform the client that the server and client are on the same machine. The server provides a local security context handle with the message.
    }
}
