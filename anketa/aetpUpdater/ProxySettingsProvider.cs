﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace aetpUpdater
{

    internal class ProxySettingsProvider : SettingsProvider, IApplicationSettingsProvider
    {
        private string _providerAppName;
        private SyncNameValueCollection _userAppSettings;
        private string _protectionProviderName;
        private object _lockTarget = new object();
        private string _previousVersionPath;

        #region Constants
        private const string PROVIDER_NAME = "ProxySettingsProvider";
        private const string CONFIG_EXTENSION = ".config";
        public const string CONFIG_USER = "user.config";    // export filename
        private const string ERR_SCOPE_INVALID = "The setting: {0}, has an invalid scope.";
        // Assumes two bytes per char.
        // Allows upto the figure below for the total size of key name and value data.
        // Total size is calculated on plain text NOT the encrypted value.
        private const int SIZE_LIMIT = 1048576;
        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the ProxySettingsProvider class with
        /// default values.
        /// </summary>
        public ProxySettingsProvider()
            : base()
        {
            _providerAppName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            _protectionProviderName = string.Empty;
            _previousVersionPath = string.Empty;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Overridden. Gets or sets the provider's application name.
        /// </summary>
        /// <value>The provider's application name. The default value is the
        /// simple name of the assembly. This is, typically, the file name of
        /// the manifest file of the assembly, minus its extension.</value>
        public override string ApplicationName
        {
            get { return _providerAppName; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _providerAppName = value;
                }
            }
        }
        #endregion
        #region Helpers
        // Attempts to find the user.config path of the previous version of the
        // application.
        // Assigns the path to the _previousVersionPath field if found, assigns
        // null if not found.
        // Returns True if found, otherwise False.
        private bool CanResolvePreviousVersion()
        {
            bool resolved = false;
            // If null, already looked and can't find the previous version...
            if (_previousVersionPath != null)
            {
                if (_previousVersionPath.Length == 0)
                {
                    // Try to find the previous version of user.config...
                    _previousVersionPath = GetPreviousVersionPath();
                }
                resolved = (_previousVersionPath != null);
            }
            return resolved;
        }
        // Searches collection for first item of objectType.
        // Returns True if found, otherwise False.
        private static bool ContainsType(ICollection collection, Type objectType)
        {
            bool containsType = false;
            foreach (object collected in collection)
            {
                if (collected.GetType() == objectType)
                {
                    containsType = true;
                    break;
                }
            }
            return containsType;
        }
        // Opens a configuration using the default file map returned by the
        // GetConfigurationFileMap() method.
        // Returns UpdaterConfiguration on success, null on failure.
        private static Configuration GetConfiguration()
        {
            ExeConfigurationFileMap fileMap = GetConfigurationFileMap();
            return GetConfiguration(fileMap);
        }
        // Opens a configuration using the specified file map.
        // Returns UpdaterConfiguration on success, null on failure.
        private static Configuration GetConfiguration(ExeConfigurationFileMap fileMap)
        {
            // Use ConfigurationUserLevel.PerUserRoamingAndLocal if you have enabled
            // appSettings at the user level.
            return ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
        }
        // Creates a default file map for use in opening a product specific
        // configuration in the user's local application data folder.
        // Returns ExeConfigurationFileMap.
        private static ExeConfigurationFileMap GetConfigurationFileMap()
        {
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = Path.Combine(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                                            @"AETP\" +
                                                            #if SberbankConsulting
                                                                "Sberbank-AST.Consultant"
                                                            #else
                                                                "Seldon 1.6"
                                                            #endif
                                                             + @"\"), CONFIG_USER);
            // If the appSettings section has (attribute) allowExeDefinition="MachineToLocalUser"
            // in the machine.config file you can comment out the statement above and reinstate the
            // code below. You should also use ConfigurationUserLevel.PerUserRoamingAndLocal
            // in the GetConfiguration method.

            //fileMap.ExeConfigFilename = string.Concat(Application.ExecutablePath, CONFIG_EXTENSION);
            //fileMap.LocalUserConfigFilename = Path.Combine(Application.LocalUserAppDataPath, CONFIG_USER);
            //fileMap.RoamingUserConfigFilename = Path.Combine(Application.UserAppDataPath, CONFIG_USER);
            return fileMap;
        }
        // Searches for the product specific folder of the previous version of
        // the application.
        // Returns the path of previous version user.config on success, otherwise null.
        private string GetPreviousVersionPath()
        {
            string previousVersionPath = null;
            Version currentVersion = new Version(Application.ProductVersion);
            if (!IsFirstRelease(currentVersion))
            {
                Version previousVersion = null;
                DirectoryInfo userAppRoot = Directory.GetParent(Application.LocalUserAppDataPath);
                if (userAppRoot.Exists)
                {
                    DirectoryInfo[] subDirs = userAppRoot.GetDirectories();
                    // Must be at least one, the current version...
                    if (subDirs.Length > 1)
                    {
                        Version testVersion = null;
                        foreach (DirectoryInfo dir in subDirs)
                        {
                            testVersion = GetVersion(dir.Name);
                            if (testVersion != null)
                            {
                                if (testVersion < currentVersion)
                                {
                                    if ((previousVersion == null) || (testVersion > previousVersion))
                                    {
                                        previousVersion = testVersion;
                                    }
                                }
                            }
                        }
                    }
                }
                if (previousVersion != null)
                {
                    previousVersionPath = Path.Combine(Path.Combine(userAppRoot.FullName, previousVersion.ToString()), CONFIG_USER);
                }
            }
            return previousVersionPath;
        }
        // Creats a settings property value collection of the user-scoped settings from the
        // previous version of the application.
        // Returns SettingsPropertyValueCollection on success, otherwise null.
        private SettingsPropertyValueCollection GetUpgradeValues(SettingsPropertyCollection properties)
        {
            SettingsPropertyValueCollection userPropertyValues = null;
            SettingsPropertyValue propertyValue = null;
            NameValueCollection userAppSettings = GetUserSettings(_previousVersionPath);
            if ((userAppSettings.Count > 0) && (properties.Count > 0))
            {
                string serializedValue = null;
                userPropertyValues = new SettingsPropertyValueCollection();
                if (_userAppSettings == null)
                {
                    _userAppSettings = new SyncNameValueCollection(new NameValueCollection());
                }
                foreach (SettingsProperty property in properties)
                {
                    if (property.SerializeAs == SettingsSerializeAs.String)
                    {
                        if (IsUserScopedSetting(property))
                        {
                            if (!ContainsType(property.Attributes.Values, typeof(System.Configuration.NoSettingsVersionUpgradeAttribute)))
                            {
                                serializedValue = userAppSettings[property.Name];
                                if (serializedValue != null)
                                {
                                    if (_userAppSettings[property.Name] == null)
                                    {
                                        // Add it...
                                        _userAppSettings.Add(property.Name, serializedValue);
                                    }
                                    else
                                    {
                                        // Update it...
                                        _userAppSettings[property.Name] = serializedValue;
                                    }
                                    propertyValue = new SettingsPropertyValue(property);
                                    // Make sure it gets deserialized and saved...
                                    propertyValue.Deserialized = false;
                                    propertyValue.SerializedValue = serializedValue;
                                    propertyValue.IsDirty = true;
                                    userPropertyValues.Add(propertyValue);
                                }
                            }
                        }
                    }
                }
            }
            return userPropertyValues;
        }
        // Retrieves the appSettings section from the user.config file for the current user
        // and the current product version.
        // Returns AppSettingsSection on success, otherwise null.
        private AppSettingsSection GetUserAppSettingsSection()
        {
            ExeConfigurationFileMap fileMap = GetConfigurationFileMap();
            return GetUserAppSettingsSection(fileMap);
        }
        // Retrieves the appSettings section from the user.config file specified in fileMap.
        // Returns AppSettingsSection on success, otherwise null.
        private AppSettingsSection GetUserAppSettingsSection(ExeConfigurationFileMap fileMap)
        {
            System.Configuration.Configuration config = GetConfiguration(fileMap);
            if (config != null)
            {
                return config.AppSettings;
            }
            else
            {
                return null;
            }
        }
        // Returns the (intialized) _userAppSettings field.
        // Will load the user-scoped settings if required.
        private NameValueCollection GetUserAppSettings()
        {
            if (_userAppSettings == null)
            {
                LoadUserAppSettings();
            }
            return _userAppSettings;
        }
        // Returns the serialized value of the specified user-scoped setting, if found,
        // otherwise null.
        // Will load the user-scoped settings if required.
        private string GetUserSetting(string settingName)
        {
            string userSetting = null;
            NameValueCollection userAppSettings = GetUserAppSettings();
            if (userAppSettings.Count > 0)
            {
                userSetting = userAppSettings[settingName];
            }
            return userSetting;
        }
        // Retrieves the appSettings key | value pairs from the specified configuration
        // file.
        // Returns NameValueCollection with settings or empty if no settings are found.
        private NameValueCollection GetUserSettings(string configFile)
        {
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = configFile;
            NameValueCollection userAppSettings = new NameValueCollection();
            lock (_lockTarget)
            {
                AppSettingsSection appSettings = GetUserAppSettingsSection(fileMap);
                if ((appSettings != null) && (appSettings.Settings.Count > 0))
                {
                    foreach (KeyValueConfigurationElement element in appSettings.Settings)
                    {
                        userAppSettings.Add(element.Key, element.Value);
                    }
                }
                return userAppSettings;
            }
        }
        // Retrieves the user-scoped setting property value of the specified property from the
        // previous version of the user.config file.
        // Returns SettingsPropertyValue on success, otherwise null.
        private SettingsPropertyValue GetValue(SettingsProperty property)
        {
            SettingsPropertyValue propertyValue = null;
            NameValueCollection userAppSettings = GetUserSettings(_previousVersionPath);
            string serializedValue = userAppSettings[property.Name];
            if (serializedValue != null)
            {
                if (property.SerializeAs == SettingsSerializeAs.String)
                {
                    if (IsUserScopedSetting(property))
                    {
                        propertyValue = new SettingsPropertyValue(property);
                        // Make sure it gets deserialized...
                        propertyValue.Deserialized = false;
                        propertyValue.SerializedValue = serializedValue;
                    }
                }
            }
            return propertyValue;
        }
        // Retrieves the application-scoped and user-scoped settings property values.
        // Retrieves application-scoped settings from:
        // app.config
        // or DefaultSettingValueAttribute if not found in app.config.
        // Retrieves user-scoped settings from:
        // user.config
        // or app.config if not found in user.config
        // or DefaultSettingValueAttribute if not found in app.config.
        // Returns SettingsPropertyValueCollection with settings or empty if no settings are found.
        private SettingsPropertyValueCollection GetValues(SettingsPropertyCollection collection)
        {
            string serializedValue;
            SettingsPropertyValueCollection settingsValues = new SettingsPropertyValueCollection();
            SettingsPropertyValue settingValue = null;
            bool userScoped, applicationScoped;

            foreach (SettingsProperty settingProperty in collection)
            {
                settingValue = new SettingsPropertyValue(settingProperty);
                // Must be serialized as String, otherwise we just return
                // the default value, if there is one...
                if (settingProperty.SerializeAs == SettingsSerializeAs.String)
                {
                    userScoped = IsUserScopedSetting(settingProperty);
                    applicationScoped = IsApplicationScopedSetting(settingProperty);
                    if ((applicationScoped) && (userScoped))
                    {
                        throw new ConfigurationErrorsException(string.Format(CultureInfo.InvariantCulture, ERR_SCOPE_INVALID, settingProperty.Name));
                    }
                    // We assume application-scope as the default...
                    if (userScoped)
                    {
                        serializedValue = GetUserSetting(settingProperty.Name);
                        // If it is not in user.config, look in app.config...
                        if (serializedValue == null)
                        {
                            serializedValue = ConfigurationManager.AppSettings[settingProperty.Name];
                        }
                    }
                    else
                    {
                        // Application-scoped setting...
                        serializedValue = ConfigurationManager.AppSettings[settingProperty.Name];
                    }
                    if (serializedValue != null)
                    {
                        // Make sure it gets deserialized...
                        settingValue.Deserialized = false;
                        settingValue.SerializedValue = serializedValue;
                    }
                }
                settingsValues.Add(settingValue);
            }
            return settingsValues;
        }
        // Creates a Version instance from the specified versionString.
        // Returns Version on success, otherwise null.
        private static Version GetVersion(string versionString)
        {
            Version version = null;
            try
            {
                version = new Version(versionString);
            }
            catch (ArgumentException) { }
            catch (FormatException) { }
            catch (OverflowException) { }

            return version;
        }
        // Checks if the specified settings property is application-scoped.
        // Returns True if application-scoped, otherwise False.
        private static bool IsApplicationScopedSetting(SettingsProperty property)
        {
            return ContainsType(property.Attributes.Values, typeof(ApplicationScopedSettingAttribute));
        }
        // Checks if the specified currentVersion is the first release.
        // Returns True if first release, otherwise False.
        private static bool IsFirstRelease(Version currentVersion)
        {
            // Either 0.0.0.0, if an application does not have a version or 1.0.0.0
            // is considered to be the lowest version number and therefore
            // represents the first release of an application. You can, of course,
            // change this to fit your requirements.
            Version versionZero = new Version("0.0.0.0");
            Version versionOne = new Version("1.0.0.0");
            return ((currentVersion == versionZero) || (currentVersion == versionOne));
        }
        // Checks if the specified settings property is user-scoped.
        // Returns True if user-scoped, otherwise False.
        private static bool IsUserScopedSetting(SettingsProperty property)
        {
            return ContainsType(property.Attributes.Values, typeof(UserScopedSettingAttribute));
        }
        // Assigns the user-scoped settings from the current product version
        // and user's user.config file to the _userAppSettings field.
        // _userAppSettings will contain settings or be empty if there are no settings.
        private void LoadUserAppSettings()
        {
            _userAppSettings = new SyncNameValueCollection(new NameValueCollection());
            lock (_lockTarget)
            {
                AppSettingsSection appSettings = GetUserAppSettingsSection();
                if ((appSettings != null) && (appSettings.Settings.Count > 0))
                {
                    foreach (KeyValueConfigurationElement element in appSettings.Settings)
                    {
                        _userAppSettings.Add(element.Key, element.Value);
                    }
                }
            }
        }
        // Resets the user.config settings to the default user-scoped setting values from
        // the app.config file.
        // NOTE: This method resets the user-scoped property values for all of the
        //		 ApplicationSettingsBase derived classes in the application and therefore
        //		 need only be called once.
        private void ResetDefaults()
        {
            if ((_userAppSettings != null) && (_userAppSettings.Count > 0))
            {
                bool hasChanged = false;
                string serializedValue = null;
                string[] keys = _userAppSettings.AllKeys;
                foreach (string key in keys)
                {
                    serializedValue = ConfigurationManager.AppSettings[key];
                    if (serializedValue != null)
                    {
                        _userAppSettings[key] = serializedValue;
                        hasChanged = true;
                    }
                }
                if (hasChanged)
                {
                    SaveUserSettings();
                }
            }
        }
        // Checks the specified context for the name of a protected configuration provider.
        // Assigns the _protectionProviderName field with provider name, if specified,
        // otherwise empty.
        private void ResolveProtectionProvider(SettingsContext context)
        {
            _protectionProviderName = string.Empty;
            if (context["ProtectionProvider"] != null)
            {
                string providerName = context["ProtectionProvider"] as string;
                if (providerName != null)
                {
                    _protectionProviderName = providerName;
                }
            }
        }
        // Saves changed user-scoped settings to user.config.
        private void SaveValues(SettingsPropertyValueCollection collection)
        {
            bool hasChanged = false;
            SettingsProperty settingProperty = null;
            string serializedValue;
            foreach (SettingsPropertyValue settingValue in collection)
            {
                if (settingValue.IsDirty)
                {
                    settingProperty = settingValue.Property;
                    // Must be serialized as String...
                    if (settingProperty.SerializeAs == SettingsSerializeAs.String)
                    {
                        // We do NOT write application-scope settings...
                        if (IsUserScopedSetting(settingProperty))
                        {
                            if (settingValue.UsingDefaultValue)
                            {
                                // Remove it...
                                _userAppSettings.Remove(settingValue.Name);
                            }
                            else
                            {
                                serializedValue = (settingValue.SerializedValue == null) ? string.Empty : (string)settingValue.SerializedValue;
                                if (_userAppSettings.Get(settingValue.Name) == null)
                                {
                                    // Add it...
                                    _userAppSettings.Add(settingValue.Name, serializedValue);
                                }
                                else
                                {
                                    // Update it...
                                    _userAppSettings[settingValue.Name] = serializedValue;
                                }
                            }
                            settingValue.IsDirty = false;
                            hasChanged = true;
                        }
                    }
                }
            }
            if (hasChanged)
            {
                SaveUserSettings();
            }
        }
        // Writes user-scoped settings to user.config.
        private void SaveUserSettings()
        {
            System.Configuration.Configuration config = GetConfiguration();
            if (config != null)
            {
                lock (_lockTarget)
                {
                    AppSettingsSection appSettings = config.AppSettings;
                    appSettings.Settings.Clear();
                    string[] settingsKeys = _userAppSettings.AllKeys;
                    foreach (string key in settingsKeys)
                    {
                        appSettings.Settings.Add(new KeyValueConfigurationElement(key, _userAppSettings[key]));
                    }
                    if ((!appSettings.SectionInformation.IsProtected) && (_protectionProviderName.Length > 0))
                    {
                        appSettings.SectionInformation.ProtectSection(_protectionProviderName);
                    }
                    appSettings.SectionInformation.ForceSave = true;
                    config.Save();
                }
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// IApplicationSettingsProvider.GetPreviousVersion implementation.
        /// </summary>
        /// <param name="context">A SettingsContext describing the current application usage.</param>
        /// <param name="property">The SettingsProperty whose value is to be returned.</param>
        /// <returns>A SettingsPropertyValue containing the value of the specified property setting
        /// as it was last set in the previous version of the application or null if the setting cannot
        /// be found.</returns>
        public SettingsPropertyValue GetPreviousVersion(SettingsContext context, SettingsProperty property)
        {
            SettingsPropertyValue propertyValue = null;
            if (CanResolvePreviousVersion())
            {
                propertyValue = GetValue(property);
            }
            return propertyValue;
        }
        /// <summary>
        /// Overridden. Returns the collection of appSettings property values for the current application
        /// instance. 
        /// </summary>
        /// <param name="context">A SettingsContext describing the current application usage.</param>
        /// <param name="collection">A SettingsPropertyCollection containing the appSettings property
        /// group whose values are to be retrieved.</param>
        /// <returns>A SettingsPropertyValueCollection containing the values for the appSettings
        /// property group.</returns>
        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection)
        {
            _userAppSettings = null;
            return GetValues(collection);
        }
        /// <summary>
        /// Overridden. Initializes the provider.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name | value pairs representing the provider-specific
        /// attributes specified in the configuration for this provider.</param>
        public override void Initialize(string name, NameValueCollection config)
        {
            // The base class call prevents multiple instances...
            if (string.IsNullOrEmpty(name))
                base.Initialize(PROVIDER_NAME, config);
            else
                base.Initialize(name, config);
        }
        /// <summary>
        /// IApplicationSettingsProvider.Reset implementation.
        /// </summary>
        /// <param name="context">A SettingsContext describing the current application usage.</param>
        /// <remarks>The Reset method resets the values of all ApplicationSettingsBase derived classes
        /// in the application. User-scoped settings are reset to the default value that is specified
        /// in the app.config file.</remarks>
        public void Reset(SettingsContext context)
        {
            ResolveProtectionProvider(context);
            ResetDefaults();
        }
        /// <summary>
        /// Overridden. Saves user-scoped appSettings property values.
        /// </summary>
        /// <param name="context">A SettingsContext describing the current application usage.</param>
        /// <param name="collection">A SettingsPropertyValueCollection representing the appSettings
        /// property values to save.</param>
        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
        {
            ResolveProtectionProvider(context);
            SaveValues(collection);
        }
        /// <summary>
        /// IApplicationSettingsProvider.Upgrade implementation.
        /// </summary>
        /// <param name="context">A SettingsContext describing the current application usage.</param>
        /// <param name="properties">A SettingsPropertyCollection containing the appSettings
        /// properties to upgrade.</param>
        /// <remarks>The Upgrade method migrates all user-scoped settings properties from
        /// the previous application version, if a previous user.config version can be found. Only
        /// the specified properties i.e. typically, the properties of a single ApplicationSettingsBase
        /// derived class are upgraded. Therefore, if the application contains more than one settings
        /// class, a call to this method may be required for each class.</remarks>
        public void Upgrade(SettingsContext context, SettingsPropertyCollection properties)
        {
            if (CanResolvePreviousVersion())
            {
                SettingsPropertyValueCollection userPropertyValues = GetUpgradeValues(properties);
                if (userPropertyValues != null)
                {
                    SetPropertyValues(context, userPropertyValues);
                }
            }
        }
        #endregion
        #region Nested collection
        // Nested synchronized (thread-safe) wrapper class for NameValueCollection.
        // Note. The enumerator is not thread-safe. By default, the collection may be
        //		 changed, by other threads, during enumeration.
        //		 An attempt to add a name and/or value, when the size limit has been exceeded
        //		 fails silently, an exception is not thrown.
        private class SyncNameValueCollection : NameValueCollection
        {
            private NameValueCollection _wrappedCollection;
            private object _root;
            private int _currentDataSize;
            #region Constructor
            public SyncNameValueCollection(NameValueCollection collectionToWrap)
                : base()
            {
                _wrappedCollection = collectionToWrap;
                _root = ((ICollection)collectionToWrap).SyncRoot;
            }
            #endregion
            #region Properties
            public override string[] AllKeys
            {
                get
                {
                    lock (_root)
                    {
                        return _wrappedCollection.AllKeys;
                    }
                }
            }
            public override int Count
            {
                get
                {
                    lock (_root)
                    {
                        return _wrappedCollection.Count;
                    }
                }
            }
            new public string this[int index]
            {
                get
                {
                    lock (_root)
                    {
                        return _wrappedCollection[index];
                    }
                }
            }
            new public string this[string name]
            {
                get
                {
                    lock (_root)
                    {
                        return _wrappedCollection[name];
                    }
                }
                set
                {
                    if ((name == null) || (value == null))
                    {
                        throw new ArgumentNullException();
                    }
                    // Replaces an existing value or adds a new name, value...
                    lock (_root)
                    {
                        ResolveAddOrReplace(name, value);
                    }
                }
            }
            public override NameObjectCollectionBase.KeysCollection Keys
            {
                get
                {
                    lock (_root)
                    {
                        return _wrappedCollection.Keys;
                    }
                }
            }
            #endregion
            #region Helpers
            private bool AllowAdd(string name, string value)
            {
                bool allowAdd = false;
                int dataToAdd = 0;
                if (_currentDataSize < SIZE_LIMIT)
                {
                    // Is it an append...
                    if (_wrappedCollection.Get(name) == null)
                    {
                        // No...
                        dataToAdd = (name.Length * 2);
                    }
                    else
                    {
                        // Yes, so add 2 for the comma...
                        dataToAdd = 2;
                    }
                    dataToAdd += (value.Length * 2);
                    if ((_currentDataSize + dataToAdd) <= SIZE_LIMIT)
                    {
                        _currentDataSize += dataToAdd;
                        allowAdd = true;
                    }
                }
                return allowAdd;
            }
            private void ResolveAddOrReplace(string name, string value)
            {
                string existingValue = _wrappedCollection.Get(name);
                if (existingValue == null)
                {
                    // Add the name and value...
                    if (AllowAdd(name, value))
                    {
                        _wrappedCollection.Add(name, value);
                    }
                }
                else
                {
                    // Replace the value...
                    int currentSize = _currentDataSize;
                    currentSize -= (existingValue.Length * 2);
                    int proposedSize = (currentSize + (value.Length * 2));
                    if (proposedSize <= SIZE_LIMIT)
                    {
                        _wrappedCollection.Set(name, value);
                        _currentDataSize = proposedSize;
                    }
                }
            }
            #endregion
            #region Methods
            new public void Add(NameValueCollection c)
            {
                throw new NotImplementedException("This member is not available in the current implementation.");
            }
            public override void Add(string name, string value)
            {
                if ((name == null) || (value == null))
                {
                    throw new ArgumentNullException();
                }
                // If the name already exists, value is appended...
                lock (_root)
                {
                    if (AllowAdd(name, value))
                    {
                        _wrappedCollection.Add(name, value);
                    }
                }
            }
            public override void Clear()
            {
                lock (_root)
                {
                    _wrappedCollection.Clear();
                    _currentDataSize = 0;
                }
            }
            new public void CopyTo(Array dest, int index)
            {
                lock (_root)
                {
                    _wrappedCollection.CopyTo(dest, index);
                }
            }
            public override string Get(int index)
            {
                lock (_root)
                {
                    return _wrappedCollection.Get(index);
                }
            }
            public override string Get(string name)
            {
                lock (_root)
                {
                    return _wrappedCollection.Get(name);
                }
            }
            public override IEnumerator GetEnumerator()
            {
                return _wrappedCollection.GetEnumerator();
            }
            public override string GetKey(int index)
            {
                lock (_root)
                {
                    return _wrappedCollection.GetKey(index);
                }
            }
            public override string[] GetValues(int index)
            {
                lock (_root)
                {
                    return _wrappedCollection.GetValues(index);
                }
            }
            public override string[] GetValues(string name)
            {
                lock (_root)
                {
                    return _wrappedCollection.GetValues(name);
                }
            }
            new public bool HasKeys()
            {
                lock (_root)
                {
                    return _wrappedCollection.HasKeys();
                }
            }
            public override void Remove(string name)
            {
                if (name == null)
                {
                    throw new ArgumentNullException();
                }
                lock (_root)
                {
                    string existingValue = _wrappedCollection.Get(name);
                    if (existingValue != null)
                    {
                        _wrappedCollection.Remove(name);
                        _currentDataSize -= (name.Length * 2);
                        _currentDataSize -= (existingValue.Length * 2);
                    }
                }
            }
            public override void Set(string name, string value)
            {
                if ((name == null) || (value == null))
                {
                    throw new ArgumentNullException();
                }
                // Replaces an existing value or adds a new name, value...
                lock (_root)
                {
                    ResolveAddOrReplace(name, value);
                }
            }
            #endregion
        #endregion
        }
    }
}
