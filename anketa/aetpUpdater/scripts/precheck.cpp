// precheck.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "precheck.h"

#define REGKEY_DOTNET40F TEXT("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Client")
#define REGKEY_DOTNET40C TEXT("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full")
#define REGKEY_DOTNET35  TEXT("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v3.5")

// Forward declarations of functions included in this code module:
bool IsInstalled(LPTSTR rgKeyName);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                       HINSTANCE hPrevInstance,
                       LPTSTR    lpCmdLine,
                       int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	if (!(IsInstalled(REGKEY_DOTNET35) ||
		  IsInstalled(REGKEY_DOTNET40C)	|| 
		  IsInstalled(REGKEY_DOTNET40F)))
	{
		if (MessageBox(NULL, 
		               TEXT("��� ������ ���������� ��������� .NET Framework 3.5.\n\n������ ��� ���������� � www.microsoft.com?"),
					   TEXT("�������� ��������� ����������"), 
					   MB_SYSTEMMODAL|MB_ICONEXCLAMATION | MB_OKCANCEL) == IDOK)
		{
			CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
			ShellExecute(NULL, TEXT("open"), TEXT("http://www.microsoft.com/download/en/details.aspx?id=21"), NULL, NULL, SW_SHOWNORMAL);
		}
	
		return 1;
	}
	else
	{
		if (_tcslen(lpCmdLine) > 0)
		{
			STARTUPINFOW siStartupInfo;
			PROCESS_INFORMATION piProcessInfo;
			memset(&siStartupInfo, 0, sizeof(siStartupInfo));
			memset(&piProcessInfo, 0, sizeof(piProcessInfo));
			siStartupInfo.cb = sizeof(siStartupInfo); 

			if (!CreateProcess(NULL,
							   lpCmdLine,
							   NULL,
							   NULL,
							   FALSE,
							   0,
							   NULL,
							   NULL,
							   &siStartupInfo,
							   &piProcessInfo))
			{
				int err = GetLastError();
				MessageBox(NULL, TEXT("�� ������� ��������� ������� ���������"), TEXT("�������� ��������� ����������"), MB_SYSTEMMODAL|MB_ICONINFORMATION);
				return err;
			}
			else
			{
					WaitForSingleObject( piProcessInfo.hProcess, INFINITE );
  
				  // Close process and thread handles. 
				  CloseHandle( piProcessInfo.hProcess );
				  CloseHandle( piProcessInfo.hThread );

			}
		}
	}
    return 0;
}

bool IsInstalled(LPTSTR rgKeyName)
  {
    HKEY keyHandle;
    DWORD rgValue = 0;
	DWORD rgValueSize = sizeof(rgValue);
    DWORD rgValueType = 0;
	bool res = false;

    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
			            rgKeyName,
						0, 
						KEY_QUERY_VALUE, 
						&keyHandle) == ERROR_SUCCESS)
	{
		if (RegQueryValueEx(keyHandle, TEXT("Install"), NULL, &rgValueType, (LPBYTE)&rgValue, &rgValueSize) == ERROR_SUCCESS)
		{
			if (rgValueType == REG_DWORD && rgValue == 1)
				res = true;
		}
		RegCloseKey(keyHandle);
    }

	return res;
} 
  

