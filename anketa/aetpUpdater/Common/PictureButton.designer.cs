﻿namespace aetpUpdater
{
    partial class PictureButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbox)).BeginInit();
            this.SuspendLayout();
            // 
            // pbox
            // 
            this.pbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbox.Location = new System.Drawing.Point(0, 0);
            this.pbox.Name = "pbox";
            this.pbox.Size = new System.Drawing.Size(177, 80);
            this.pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbox.TabIndex = 0;
            this.pbox.TabStop = false;
            this.pbox.Click += new System.EventHandler(this.pbox_Click);
            this.pbox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbox_MouseClick);
            this.pbox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pbox_MouseDoubleClick);
            this.pbox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbox_MouseDown);
            this.pbox.MouseEnter += new System.EventHandler(this.pbox_MouseEnter);
            this.pbox.MouseLeave += new System.EventHandler(this.pbox_MouseLeave);
            this.pbox.MouseHover += new System.EventHandler(this.pbox_MouseHover);
            this.pbox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbox_MouseMove);
            this.pbox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbox_MouseUp);
            // 
            // PictureButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.pbox);
            this.DoubleBuffered = true;
            this.Name = "PictureButton";
            this.Size = new System.Drawing.Size(177, 80);
            this.EnabledChanged += new System.EventHandler(this.PictureButton_EnabledChanged);
            this.Enter += new System.EventHandler(this.PictureButton_Enter);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PictureButton_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PictureButton_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.PictureButton_KeyUp);
            this.Leave += new System.EventHandler(this.PictureButton_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.pbox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbox;

    }
}
