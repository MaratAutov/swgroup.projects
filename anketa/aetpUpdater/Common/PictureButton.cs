﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace aetpUpdater
{
    public partial class PictureButton : UserControl, IButtonControl
    {
        private Image imgUp;
        private Image imgDown;
        private Image imgOver;
        private Image imgDisabled;

        private bool mouseOver;
        private bool mouseLeftDown;

        public PictureButton()
        {
            InitializeComponent();
            mouseOver = false;
            mouseLeftDown = false;
            myDialogResult = DialogResult.None;
        }

        #region Image Properties

        public Image ImageUp
        {
            get
            {
                return Enabled ? imgUp : ImageDisabled;
            }
            set
            {
                imgUp = value;
                pbox.Image = ImageUp;
            }
        }

        public Image ImageDown
        {
            get
            {
                return Enabled ? imgDown : ImageDisabled;
            }
            set
            {
                imgDown = value;
            }
        }

        public Image ImageOver
        {
            get
            {
                return Enabled ? imgOver : ImageDisabled;
            }
            set
            {
                imgOver = value;
            }
        }

        public Image ImageDisabled
        {
            get
            {
                return imgDisabled;
            }
            set
            {
                imgDisabled = value;
            }
        }

        #endregion

        #region Image Control

        private void pbox_MouseLeave(object sender, EventArgs e)
        {
            mouseOver = false;
            pbox.Image = ImageUp;
            OnMouseLeave(e);
        }

        private void pbox_MouseEnter(object sender, EventArgs e)
        {
            mouseOver = true;
            if (mouseLeftDown)
                pbox.Image = ImageDown;
            else
                pbox.Image = ImageOver;
            OnMouseEnter(e);
        }

        private void pbox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseLeftDown = true;
                pbox.Image = ImageDown;
            }
            OnMouseDown(e);
        }

        private void pbox_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseLeftDown = false;
                if (mouseOver)
                    pbox.Image = ImageOver;
                else
                    pbox.Image = ImageUp;
            }
            OnMouseUp(e);
        }

        private void pbox_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseOver == true &&
               (e.X < 0 || e.X >= pbox.Width ||
                e.Y < 0 || e.Y >= pbox.Height))
            {
                pbox_MouseLeave(sender, e);
            }

            if (mouseOver == false &&
               (e.X >= 0 && e.X < pbox.Width &&
                e.Y >= 0 && e.Y < pbox.Height))
            {
                pbox_MouseEnter(sender, e);
            }
            OnMouseMove(e);

        }

        private void pbox_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left ||
               (e.Button == MouseButtons.Right && SystemInformation.MouseButtonsSwapped))
            {
                OnClick(e);
            }
            OnMouseClick(e);
        }

        private void pbox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left ||
               (e.Button == MouseButtons.Right && SystemInformation.MouseButtonsSwapped))
            {
                //simulate button's click,click as shown in MSDN Control.Click Event
                OnClick(e);
                OnClick(e);
            }
            OnMouseDoubleClick(e);
        }

        private void pbox_MouseHover(object sender, EventArgs e)
        {
            OnMouseHover(e);
        }
        
        private void pbox_Click(object sender, EventArgs e)
        {
            // do not forward PictureBox'es click event to PictureButton
        }

        #endregion
                
        private void PictureButton_EnabledChanged(object sender, EventArgs e)
        {
            if (Enabled)
                pbox.Image = ImageUp;
            else
                pbox.Image = ImageDisabled;
        }

        #region Keyboard control
        
        private void PictureButton_Enter(object sender, EventArgs e)
        {
            pbox_MouseEnter(sender, e);
        }

        private void PictureButton_Leave(object sender, EventArgs e)
        {
            pbox_MouseLeave(sender, e);
        }

        private void PictureButton_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space)
            {
                pbox.Image = ImageDown;
            }
        }

        private void PictureButton_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == (char)Keys.Enter || e.KeyValue == (char)Keys.Space)
            {
                if (mouseOver)
                    pbox.Image = ImageOver;
                else
                    pbox.Image = ImageUp;
            }
        }

        #endregion

        private void PictureButton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Space)
            {
                OnClick(e);
            }
        }

        // Add implementation to the IButtonControl.DialogResult property.

        public DialogResult DialogResult
        {
            get
            {
                return myDialogResult;
            }

            set
            {
                if (Enum.IsDefined(typeof(DialogResult), value))
                {
                    myDialogResult = value;
                }
            }
        }

        // Add implementation to the IButtonControl.NotifyDefault method.

        public void NotifyDefault(bool value)
        {
            if (IsDefault != value)
            {
                IsDefault = value;
            }
        }

        // Add implementation to the IButtonControl.PerformClick method.

        public void PerformClick()
        {
            if (CanSelect)
            {
                OnClick(EventArgs.Empty);
            }
        }

        private bool IsDefault;
        private DialogResult myDialogResult;

    }
}
