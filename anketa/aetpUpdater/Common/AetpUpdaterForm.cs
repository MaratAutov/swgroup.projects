﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace aetpUpdater
{
    /************************************************************************
    *																		*
    *  Класс необходим для замены иконок окон Seldon и Sberbank-Cnosulting  *
    *																		*
    ************************************************************************/

    public class AetpUpdaterForm : Form
    {
        public AetpUpdaterForm()
        {
            Icon = Properties.Resources.Icon;
        }
    }
}
