﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.296
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace aetpUpdater.Properties {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("aetpUpdater.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Анкета.Редактор.
        /// </summary>
        internal static string ApplicationName {
            get {
                return ResourceManager.GetString("ApplicationName", resourceCulture);
            }
        }
        
        internal static System.Drawing.Bitmap bg_lic {
            get {
                object obj = ResourceManager.GetObject("bg_lic", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap bg_server {
            get {
                object obj = ResourceManager.GetObject("bg_server", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на http://95.79.54.222:9003/Updater16/_current/preinstall.xml.
        /// </summary>
        internal static string Builtin_PreinstallXmlUrl {
            get {
                return ResourceManager.GetString("Builtin_PreinstallXmlUrl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на http://95.79.54.222:9003/.
        /// </summary>
        internal static string Builtin_ServerUrl {
            get {
                return ResourceManager.GetString("Builtin_ServerUrl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на uninstall.dat.
        /// </summary>
        internal static string Builtin_UninstallFilename {
            get {
                return ResourceManager.GetString("Builtin_UninstallFilename", resourceCulture);
            }
        }
        
        internal static System.Drawing.Bitmap button_back {
            get {
                object obj = ResourceManager.GetObject("button_back", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_back_down {
            get {
                object obj = ResourceManager.GetObject("button_back_down", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_back_over {
            get {
                object obj = ResourceManager.GetObject("button_back_over", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_back_up {
            get {
                object obj = ResourceManager.GetObject("button_back_up", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_cancel_down {
            get {
                object obj = ResourceManager.GetObject("button_cancel_down", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_cancel_over {
            get {
                object obj = ResourceManager.GetObject("button_cancel_over", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_cancel_up {
            get {
                object obj = ResourceManager.GetObject("button_cancel_up", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_check_down {
            get {
                object obj = ResourceManager.GetObject("button_check_down", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_check_over {
            get {
                object obj = ResourceManager.GetObject("button_check_over", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_check_up {
            get {
                object obj = ResourceManager.GetObject("button_check_up", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_finish_down {
            get {
                object obj = ResourceManager.GetObject("button_finish_down", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_finish_over {
            get {
                object obj = ResourceManager.GetObject("button_finish_over", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_finish_up {
            get {
                object obj = ResourceManager.GetObject("button_finish_up", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_next_disabled {
            get {
                object obj = ResourceManager.GetObject("button_next_disabled", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_next_down {
            get {
                object obj = ResourceManager.GetObject("button_next_down", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_next_over {
            get {
                object obj = ResourceManager.GetObject("button_next_over", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_next_up {
            get {
                object obj = ResourceManager.GetObject("button_next_up", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_no_down {
            get {
                object obj = ResourceManager.GetObject("button_no_down", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_no_over {
            get {
                object obj = ResourceManager.GetObject("button_no_over", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_no_up {
            get {
                object obj = ResourceManager.GetObject("button_no_up", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_ok_down {
            get {
                object obj = ResourceManager.GetObject("button_ok_down", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_ok_over {
            get {
                object obj = ResourceManager.GetObject("button_ok_over", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_ok_up {
            get {
                object obj = ResourceManager.GetObject("button_ok_up", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_yes_down {
            get {
                object obj = ResourceManager.GetObject("button_yes_down", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_yes_over {
            get {
                object obj = ResourceManager.GetObject("button_yes_over", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap button_yes_up {
            get {
                object obj = ResourceManager.GetObject("button_yes_up", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на ЗАО АЦ Фонд.
        /// </summary>
        internal static string Copyright {
            get {
                return ResourceManager.GetString("Copyright", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Ошибка подключения.
        /// </summary>
        internal static string ErrMsg_ConnectionFailed {
            get {
                return ResourceManager.GetString("ErrMsg_ConnectionFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Невозможно обновить основные компоненты.
        ///Выбран неправильный сервер обновлений или сервер недоступен..
        /// </summary>
        internal static string ErrMsg_UpdateCommonFailed {
            get {
                return ResourceManager.GetString("ErrMsg_UpdateCommonFailed", resourceCulture);
            }
        }
        
        internal static System.Drawing.Bitmap exclamation {
            get {
                object obj = ResourceManager.GetObject("exclamation", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Editor.exe.
        /// </summary>
        internal static string ExeName {
            get {
                return ResourceManager.GetString("ExeName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Неправильное имя в описании модуля..
        /// </summary>
        internal static string ExMsg_NameMismatch {
            get {
                return ResourceManager.GetString("ExMsg_NameMismatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Не задан сервер обновлений..
        /// </summary>
        internal static string ExMsg_UpdateServerIsEmpty {
            get {
                return ResourceManager.GetString("ExMsg_UpdateServerIsEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Неправильная версия в описании модуля..
        /// </summary>
        internal static string ExMsg_VersionMismatch {
            get {
                return ResourceManager.GetString("ExMsg_VersionMismatch", resourceCulture);
            }
        }
        
        internal static System.Drawing.Bitmap ico_ok {
            get {
                object obj = ResourceManager.GetObject("ico_ok", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap ico32_err {
            get {
                object obj = ResourceManager.GetObject("ico32_err", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Icon Icon {
            get {
                object obj = ResourceManager.GetObject("Icon", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на В системе выполняется другой экземпляр AetpUpdater. Текущий процесс будет завершен.
        ///Завершите все остальные экземпляры AetpUpdater и повторите попытку..
        /// </summary>
        internal static string InfoMsg_AnotherInstanceIsRunning {
            get {
                return ResourceManager.GetString("InfoMsg_AnotherInstanceIsRunning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Нет авторизации. Завершение работы программы..
        /// </summary>
        internal static string InfoMsg_AuthentificationError {
            get {
                return ResourceManager.GetString("InfoMsg_AuthentificationError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Соединение установленно.
        /// </summary>
        internal static string InfoMsg_ConnectionEstablished {
            get {
                return ResourceManager.GetString("InfoMsg_ConnectionEstablished", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Проверка соединения.
        /// </summary>
        internal static string InfoMsg_ConnectionTesting {
            get {
                return ResourceManager.GetString("InfoMsg_ConnectionTesting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Загрузить.
        /// </summary>
        internal static string InfoMsg_Download {
            get {
                return ResourceManager.GetString("InfoMsg_Download", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Произошла ошибка. Завершение работы программы..
        /// </summary>
        internal static string InfoMsg_ExitOnError {
            get {
                return ResourceManager.GetString("InfoMsg_ExitOnError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Помощь.
        /// </summary>
        internal static string InfoMsg_Help {
            get {
                return ResourceManager.GetString("InfoMsg_Help", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Кб.
        /// </summary>
        internal static string InfoMsg_Kilobytes {
            get {
                return ResourceManager.GetString("InfoMsg_Kilobytes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Мб.
        /// </summary>
        internal static string InfoMsg_Megabytes {
            get {
                return ResourceManager.GetString("InfoMsg_Megabytes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Необходимо загрузить.
        /// </summary>
        internal static string InfoMsg_NeedToDownload {
            get {
                return ResourceManager.GetString("InfoMsg_NeedToDownload", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Программа обновления.
        /// </summary>
        internal static string InfoMsg_ProgramName {
            get {
                return ResourceManager.GetString("InfoMsg_ProgramName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Доступно обновление для модуля.
        /// </summary>
        internal static string InfoMsg_UpdateAvailable {
            get {
                return ResourceManager.GetString("InfoMsg_UpdateAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Доступны обновления.
        /// </summary>
        internal static string InfoMsg_UpdatesAvailable {
            get {
                return ResourceManager.GetString("InfoMsg_UpdatesAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Обновление успешно завершено..
        /// </summary>
        internal static string InfoMsg_UpdateSuccess {
            get {
                return ResourceManager.GetString("InfoMsg_UpdateSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на ЛИЦЕНЗИОННОЕ СОГЛАШЕНИЕ
        ///
        ///Настоящее Лицензионное соглашение (далее - соглашение) является юридическим документом, заключаемым между лицензиатом - Пользователем (физическим или юридическим лицом) Программы для ЭВМ «Информационно-аналитическая система «Seldon.2012» (далее - Программа) и Правообладателем - разработчиком Программы ЗАО «АЦ Фонд».
        ///Устанавливая, копируя или иным образом используя Программу, Вы тем самым принимаете на себя условия настоящего соглашения на весь срок использования Программы.
        ///
        ///1.  [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string LicenseText {
            get {
                return ResourceManager.GetString("LicenseText", resourceCulture);
            }
        }
        
        internal static System.Drawing.Bitmap loader {
            get {
                object obj = ResourceManager.GetObject("loader", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap loader_ajax {
            get {
                object obj = ResourceManager.GetObject("loader_ajax", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap msg_bg {
            get {
                object obj = ResourceManager.GetObject("msg_bg", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap Rosteh_install_1 {
            get {
                object obj = ResourceManager.GetObject("Rosteh_install_1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap Rosteh_install_2 {
            get {
                object obj = ResourceManager.GetObject("Rosteh_install_2", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static byte[] serverBase_root_cert {
            get {
                object obj = ResourceManager.GetObject("serverBase_root_cert", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        internal static System.Drawing.Bitmap slash {
            get {
                object obj = ResourceManager.GetObject("slash", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap tick {
            get {
                object obj = ResourceManager.GetObject("tick", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Консалтинг.
        /// </summary>
        internal static string Сonsulting {
            get {
                return ResourceManager.GetString("Сonsulting", resourceCulture);
            }
        }
    }
}
