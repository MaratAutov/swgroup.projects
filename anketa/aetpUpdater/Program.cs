﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using aetpUpdater.Installer;
using System.Resources;
using System.Net;
using aetpUpdater.Updater;

namespace aetpUpdater
{
    static class Program
    {
        static void SeldonUnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show("UnhandledException " + e.ExceptionObject);
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            WebRequest.RegisterPrefix("seldon-http", new updaterSeldonNTLM.SeldonWebRequestCreator());

            AppDomain.CurrentDomain.UnhandledException += SeldonUnhandledExceptionHandler;

            #if DEBUG
            MessageBox.Show("Debug message:\n\nProgram start watchdog", Utilities.ProgramInfo.ExecutableName());
            #endif
            // Parse CLI
            if (!Utilities.CLIParser.ParseCLI(args) || Utilities.CLIParser.help)
            {
                Utilities.CLIParser.ShowHelp();
                return; //and exit
            }

            Updater.AppUpdater.CreateLogger(Utilities.CLIParser.GetLogLevel());

            Updater.AppUpdater.LogInfo("Started " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                       + " v" + Utilities.ProgramInfo.Version() 
                                       + ", Path = " + Utilities.ProgramInfo.ProcessFileName()
                                       + " " + string.Join(" ", args));

            if (!String.IsNullOrEmpty(Utilities.CLIParser.waitPidForExit))
            {
                try
                {
                    int pid = 0;
                    if (int.TryParse(Utilities.CLIParser.waitPidForExit, out pid))
                    {
                        Utilities.ProcessKiller.WaitProcForExit(pid);
                    }
                    else
                    {
                        Updater.AppUpdater.LogError(String.Format("Unable to wait for PID = {0}. Unable to parse PID.", Utilities.CLIParser.waitPidForExit));
                        Updater.AppUpdater.LogInfo("AetpUpdater exiting");
                        return;
                    }
                }
                catch (Exception ex)
                {
                    Updater.AppUpdater.LogError(String.Format("Unable to wait for PID = {0}. Error: {1}", Utilities.CLIParser.waitPidForExit, ex.Message));
                    Updater.AppUpdater.LogInfo("AetpUpdater exiting");
                    return;
                }
            }

            if (Utilities.CLIParser.WaitSeldonForExit)
            {
                Updater.AppUpdater.LogInfo("Waiting for " + Properties.Resources.ApplicationName + @" to terminate");
                try
                {
                    Utilities.ProcessKiller.WaitAllProcForExit(Properties.Resources.ExeName.Replace(".exe", ""));
                }
                catch (Exception ex)
                {
                    Updater.AppUpdater.LogError("Unable to wait for " + Properties.Resources.ExeName.Replace(".exe", "") + @" process: " + ex.Message);
                    Updater.AppUpdater.LogInfo("AetpUpdater exiting");
                    return;
                }
            }
            
            if (Utilities.CLIParser.killseldon)
            {
                Updater.AppUpdater.LogInfo("Stopping seldon");
                try
                {
                    Utilities.ProcessKiller.KillAllProcessesByName(Properties.Resources.ExeName.Replace(".exe", ""));
                }
                catch (Exception ex)
                {
                    Updater.AppUpdater.LogError("Unable to kill " + Properties.Resources.ExeName.Replace(".exe", "") + @" process: " + ex.Message);
                    Updater.AppUpdater.LogInfo("AetpUpdater exiting");
                    return;
                }
            }
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            #warning use Application.Run(ApplicationContext);
            //Application.Run();

            if (Utilities.CLIParser.uninstall_mode)
            {
                Installer.AppInstaller.Uninstall();
                return;
            }

            if (!Utilities.RegistryEditor.IsAetpUpdaterInstalled())
            {
                if (!Utilities.DotNetChecker.IsDotNetRequiredVersionInstalled())
                {
                    MessageBox.Show("Для продолжения требуется .NET Framework 4.0");
                    return;
                }

                DialogResult instDlgResult = DialogResult.None;
                //Application.Run(new Installer.DlgInstall());
                using (Installer.DlgInstall instDlg = new Installer.DlgInstall())
                {
                    instDlgResult = instDlg.ShowDialog();
                }
                if (Installer.AppInstaller.IsInstalledSuccessful)
                {
//#warning additional Application.Run() to show message boxes
//                    Application.Run();

                    // Simply skip self update until next load
                    //if (Updater.AppUpdater.selfupdate_trigger == true)
                    //{
                        //Updater.AppUpdater.InitiateSelfUpdate(args);
                        //return;
                    //}
                    if (Installer.AppInstaller.IsAppStartRequested)
                    {
                        // starting aetpUpdater instead of Seldon to update to the latest version after disk installation.
                        string appPath1 = UpdaterConfiguration.AppPath + "\\" + Utilities.ProgramInfo.ExecutableName();
                        Updater.AppUpdater.LogInfo("Staring AetpAdminConsole: " + appPath1);
                        if (System.IO.File.Exists(appPath1))
                        {
                            System.Diagnostics.ProcessStartInfo newproc = new System.Diagnostics.ProcessStartInfo();
                            newproc.FileName = appPath1;
                            newproc.UseShellExecute = true;
                            newproc.WorkingDirectory = UpdaterConfiguration.AppPath + "\\";
                            System.Diagnostics.Process.Start(newproc);
                            return;
                        }
                        else
                        {
                            Updater.AppUpdater.LogInfo("File was not found");
                            MessageBox.Show(Properties.Resources.InfoMsg_ExitOnError,
                                            Properties.Resources.InfoMsg_ProgramName,
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Information);
                            return;
                        }
                    }
                    Updater.AppUpdater.LogInfo("AetpUpdater exiting...");
                    return;
                }
                else
                {
                    if (instDlgResult != DialogResult.OK)
                    {
                        Updater.AppUpdater.LogInfo("Installation FAILED. AetpUpdater exiting...");
                    }
                    else
                    {
                        Updater.AppUpdater.LogInfo("Installation Canceled. AetpUpdater exiting...");
                    }
                    return;      
                }

            }
            else
            {
                // Perform regular update or self-update
                string installLocation = System.IO.Path.GetDirectoryName(Utilities.RegistryEditor.AetpUpdaterInstallLocation() + "\\");
                // MSDN: GetDirectoryName() returns Directory information for path, 
                // or null if path denotes a root directory or is null. 
                // Returns 'String.Empty' if path does not contain directory information.
                if (installLocation == null) // installed into root directory (e.g. c:\)
                {
                    installLocation = Utilities.RegistryEditor.AetpUpdaterInstallLocation().TrimEnd('\\') + "\\";
                }
                if (String.Compare(installLocation,
                                   Application.StartupPath,
                                   StringComparison.CurrentCultureIgnoreCase)
                    != 0)
                {
                    if (MessageBox.Show("AetpUpdater уже установлен на этом компьютере.\n"
                                   + "Хотите запустить AetpUpdater из:\n"
                                   + installLocation,
                               Properties.Resources.InfoMsg_ProgramName,
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start(String.Format("{0}\\{1}", installLocation, Utilities.ProgramInfo.ExecutableName()));
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                if (AppInstaller.IsSeldon16Running())
                {
                    MessageBox.Show("Запущен " + Properties.Resources.ApplicationName + ". Автоматическое обновление невозможно.\n"
                        + "Завершите приложение и повторите попытку.",
                        "Обновление " + Properties.Resources.ApplicationName);
                }
                else
                {
                    if (!Updater.AppUpdater.Update(args))
                    {
                        return;
                    }
                    if (Updater.AppUpdater.selfupdate_trigger == true)
                    {
                        Updater.AppUpdater.InitiateSelfUpdate(args);
                        return;
                    }
                }
            }

            // 5. Install a root certificate

            //Updater.AppUpdater.LogInfo("Certificate check. ");

            //if (!DlgInstall.IsCertInstalled())
            //{
            //    Updater.AppUpdater.LogInfo("Certificate installation. ");
            //    DlgInstall.CertInstall();
            //}

            string appPath = String.Format("{0}\\{1}", Application.StartupPath, Properties.Resources.ExeName);
            Updater.AppUpdater.LogInfo("Staring AetpAdminConsole: " + appPath);
            if (System.IO.File.Exists(appPath))
            {
                if (ModuleUpdater.ExiststModelsUpdate)
                    System.Diagnostics.Process.Start(appPath, "-refresh_models");
                else
                    System.Diagnostics.Process.Start(appPath);
            }
            else
            {
                Updater.AppUpdater.LogInfo(Properties.Resources.ExeName + " was not found");
                MessageBox.Show(Properties.Resources.InfoMsg_ExitOnError,
                                Properties.Resources.InfoMsg_ProgramName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                return;
            }
            Updater.AppUpdater.LogInfo("AetpUpdater exiting...");
            return;
        }


    }
}
