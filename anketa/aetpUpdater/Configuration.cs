﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;
using System.Net;
using System.DirectoryServices.AccountManagement;
using System.IO;

namespace aetpUpdater
{
    /// <summary>
    /// UpdaterConfiguration settings holder
    /// </summary>
    public static class UpdaterConfiguration
    {
        private static string appPath;
        public static string AppPath
        {
            get
            {
                if (String.IsNullOrEmpty(appPath))
                    appPath = Application.StartupPath + "\\";
                return appPath;
            }
            set
            {
                appPath = value;
                if (!appPath.EndsWith("\\") && !appPath.EndsWith("/"))
                    appPath += "\\";
            }
        }

        private static string modelPath;
        public static string ModelPath
        {
            get
            {
                if (string.IsNullOrEmpty(modelPath))
                    modelPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "aetpAnketa\\Models");
                return modelPath;
            }
        }

        #region IsLocalVersion

        static Nullable<bool> _IsLocalVersion = null;
        /// <summary>
        /// IsLocalVersion
        /// </summary>
        public static bool IsLocalVersion
        {
            get
            {
                if (_IsLocalVersion == null)
                {
                    try
                    {
                        _IsLocalVersion = bool.Parse(appConfig.IsLocalVersion);
                    }
                    catch
                    {
                        _IsLocalVersion = false;
                    }
                }

                return (bool)_IsLocalVersion;
            }
        }

        #endregion

        #region UpdaterConfiguration
        /// <summary>
        /// Reads configuration settings from app.config
        /// </summary>
        static UpdaterConfiguration()
        {
            UpdaterConfiguration.Settings = new Settings();
            UpdaterConfiguration.appConfig = new AppConfigSettings();
        }
        #endregion

        #region Settings
        /// <summary>
        /// Contains application settings
        /// </summary>
        public static Settings Settings;
        public static AppConfigSettings appConfig;
        #endregion

        #region CurrentVersion
        /// <summary>
        /// CurrentVersion
        /// </summary>
        public static string CurrentVersion
        {
            get
            {
                return (string)ConfigurationManager.AppSettings["CurrentVersion"];
            }
        }
        #endregion

        #region ExportUser
        /// <summary>
        /// Server url
        /// </summary>
        static string _ExportUser = ConfigurationManager.AppSettings["ExportUserName"];
        public static string ExportUser
        {
            get
            {
                return _ExportUser;
            }
            set
            {
                _ExportUser = value;
            }
        }
        #endregion

        #region ExportPassword
        /// <summary>
        /// ExportPassword
        /// </summary>
        static string _ExportPassword = ConfigurationManager.AppSettings["ExportPassword"];
        public static string ExportPassword
        {
            get
            {
                return _ExportPassword;
            }
            set
            {
                _ExportPassword = value;
            }
        }
        #endregion

        #region SetGlobalProxy
        static WebProxy _WebProxy = null;

        private static void SetGlobalProxy(WebProxy proxy)
        {
            ServicePointManager.Expect100Continue = false;
            WebRequest.DefaultWebProxy =
            HttpWebRequest.DefaultWebProxy =
            FileWebRequest.DefaultWebProxy =
            FtpWebRequest.DefaultWebProxy = _WebProxy;
        }

        /// <summary>
        /// SetGlobalProxy
        /// </summary>
        /// <returns></returns>
        public static bool SetGlobalProxy()
        {
            return SetGlobalProxy(Settings);
        }

        public static bool SetGlobalProxy(Settings settings)
        {
            try
            {
                if( settings.UseProxy )
                {
                    int port = int.Parse(settings.ProxyPort);
                    _WebProxy = new WebProxy(settings.ProxyUrl, port);

                    bool isDefaultCredentials = false;
                    try {
                        if( Settings.UseProxyAuth && Settings.ProxyAuthType == "NTLM")
                        {
                            using( PrincipalContext pc = new PrincipalContext(ContextType.Domain) )
                                isDefaultCredentials = pc.ValidateCredentials(settings.ProxyUser, settings.ProxyPassword);
                            isDefaultCredentials &= (Environment.UserName.ToLower() == settings.ProxyUser.ToLower());
                        }
                    }
                    catch( Exception )
                    {
                    }

                    if( settings.UseProxyAuth && !isDefaultCredentials )
                    {
                        _WebProxy.Credentials = new
                            NetworkCredential(settings.ProxyUser, settings.ProxyPassword).
                            GetCredential(settings.ProxyUrl, port, settings.ProxyAuthType);
                    }
                    else
                        _WebProxy.UseDefaultCredentials = true;
                }
                else
                    _WebProxy = null;

                SetGlobalProxy(_WebProxy);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
