using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

using ICSharpCode.SharpZipLib.Zip;

using aetpUpdater.Utilities;

namespace aetpUpdater.Updater
{
    /// <summary>
    /// Splash form for application startup
    /// </summary>
    public partial class ProgressForm : AetpUpdaterForm
    {
        #region ProgressForm Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public ProgressForm()
        {
            InitializeComponent();

            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            label_version.Text = "������ ��������� ����������: " + Utilities.ProgramInfo.Version();
            
            Utilities.WinGraphics.ApplyFormStopResizing(this);
            DialogResult = DialogResult.None;
        }
        #endregion

        #region attributes

        private string tmpdir;          // Temporary directory to exctract local updates to.

        private void ProgressForm_Load(object sender, EventArgs e)
        {
            if (CLIParser.localupdate_mode)
            {
                // Add code to pre-select latest directory used
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    // Create temporary directory
                    tmpdir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                    Directory.CreateDirectory(tmpdir); // Directory will be deleted when backgroundUpdater completes its work
                    // unpack seleted file
                    FastZip packer = new FastZip();
                    packer.ExtractZip(openFileDialog.FileName, tmpdir, null);

                    // install updates
                    UpdaterConfiguration.Settings.LocalUpdatePath = tmpdir;
                    UpdaterConfiguration.Settings.Save();

                    backgroundUpdater.RunWorkerAsync(Updater.AppUpdater.GetModulesNames());
                }
                else
                {
                    Close();
                }
            }
            else
            {
                backgroundUpdater.RunWorkerAsync(Updater.AppUpdater.GetModulesNames());
            }
        }           
        #endregion

        /************************************************************************
        *																		*
        *  ����� �� ��������� ������ ������� ����� ������ ������                *
        *																		*
        ************************************************************************/

        private void backgroundUpdater_DoWork(object sender, DoWorkEventArgs e)
        {
            DialogResult Result = DialogResult.OK; // ���������� � ������ ���������� ������������� ���������� ������

            List<string> modules = (List<string>)e.Argument;
            string srvr = CLIParser.localupdate_mode ? UpdaterConfiguration.Settings.LocalUpdatePath : UpdaterConfiguration.Settings.ServerUpdaterUrl;

            if (Utilities.CLIParser.RepairModeEnabled)
            {
                using (Updater.DlgUpdate UpdateDlg = new Updater.DlgUpdate(srvr))
                {
                    Result = UpdateDlg.ShowDialog();
                }
            }
            else
            {
                foreach (string modname in modules)
                {
                    using (ModuleUpdater updater = new ModuleUpdater(modname, srvr, CLIParser.localupdate_mode))
                    {
                        if (updater.HasNewerVersion())
                        {
                            using (Updater.DlgUpdate UpdateDlg = new Updater.DlgUpdate(srvr))
                            {
                                Result = UpdateDlg.ShowDialog(this);
                            }
                            break;
                        }
                    }
                }
            }
            // Recursive delete temporary directory
            if (!String.IsNullOrEmpty(tmpdir) && Directory.Exists(tmpdir))
            {
                AppUpdater.LogInfo("Recursive delete temporary directory");
                Directory.Delete(tmpdir);
            }
            DialogResult = Result;
            Close();
        }
    }
}