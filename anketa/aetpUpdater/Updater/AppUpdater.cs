﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Xml; // for preinstall actions XML File
using System.Xml.Linq;
using System.Linq;
using System.Net;

using ICSharpCode.SharpZipLib.Zip;

using aetpUpdater.Utilities;

namespace aetpUpdater.Updater
{
    public static class AppUpdater
    {
        /************************************************************************
        *																		*
        *  Начинаем проверку обновления Seldon или Updater                      *
        *																		*
        ************************************************************************/

        public static bool Update(string[] args)
        {
            /* 
             * 3. Perform self update
             * 7. Install or update selected modules
             * 8. Start Application
             */

            UninstallList.Filename = String.Format("{0}\\{1}", 
                Utilities.RegistryEditor.AetpUpdaterInstallLocation(), 
                Properties.Resources.Builtin_UninstallFilename);

            // 3. Run SelfUpdate
            if (CLIParser.selfupdate_mode) // exit on success, on any failure ignore selfupdate switch and continue
                try
                {
                    if (OverwriteExecutable(args))
                        return true;
                }
                catch { }
            // In regular mode
            DeleteTempExecutable();

            // 6. Select modules
            // will read modules names from ProgressForm_Load directly
            //List<string> modules = Updater.AppUpdater.GetModulesNames();
            //

            // 7. Install or update selected modules
            // will call UpdateModules(modules) in backgroundWorker
            LogInfo("Updating Modules");

            // Set proxy settings
            NetworkTester.ResetGlobalProxyToSavedSettings();
            if (!CLIParser.localupdate_mode && !NetworkTester.TestConnection())
            {
                using (Installer.DlgInstall connectionForm = new Installer.DlgInstall())
                {
                    connectionForm.EditConnectionSettings();
                }
                // не удалось починить соединение, равершаем процесс обновления с ошибкой
                if (!NetworkTester.TestConnection()) return false;
            }
            // запускаем процесс проверки обновления
            using (ProgressForm dlg = new ProgressForm())
            {
                return dlg.ShowDialog() == DialogResult.OK;
            }
        }

        /************************************************************************
        *																		*
        *                                                                       *
        *																		*
        ************************************************************************/
       
        public static string CurrentVersionPath
        {
            get
            {
                if (CLIParser.dev_mode)
                    return "_development";
                else if (CLIParser.test_mode)
                    return "_testing";
                else
                    return "_current";
            }
        }

        /************************************************************************
        *																		*
        *                                                                       *
        *																		*
        ************************************************************************/

        public static string DefaultVersionPath
        {
            get
            {
                return "_current";
            }
        }

        /************************************************************************
        *																		*
        *                                                                       *
        *																		*
        ************************************************************************/
        
        public static int InitiateSelfUpdate(string[] args)
        {
            LogInfo("Self update triggered");
#if DEBUG_SELFUPDATE
            MessageBox.Show("Newer version available. Perform self update.",
                            ProcessFileName(),
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
#endif
            /* Newer Updater version is available
                * terminate current process and
                * run temporary updater binary in selfupdate mode
                */

            // Sanity check. "_new.exe" is created in ModuleUpdater::ExtractFiles()
            string newUpdaterExe = Path.Combine(UpdaterConfiguration.AppPath, 
                                                ProgramInfo.ExecutableName() + "_new.exe");
            if (System.IO.File.Exists(newUpdaterExe))
            {
                LogInfo("Initiating self update");
                System.Diagnostics.Process.Start(newUpdaterExe,
                                                    string.Join(" ", args) + " /selfupdate");
#if DEBUG_SELFUPDATE
            MessageBox.Show("Newer version available. Now exit current instance.",
                            ProcessFileName(),
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
#endif
                LogInfo("Self update Initiated. Now Exiting...");
                return 0;
            }
                
            // Should never reach this code
            LogError("Unable to update updater app");
            MessageBox.Show("Unable to update updater app",
                            Properties.Resources.InfoMsg_ProgramName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
            return 1;
                    
        }

        /************************************************************************
        *																		*
        *                                                                       *
        *																		*
        ************************************************************************/

        private static bool OverwriteExecutable(string[] args)
        {
            
            /* overwrite Updater executable with newer version
            * start newer version
            * exit temporary executable
            * 
            * Temporary Updater executable files are removed in regular mode (e.g. by just statred newer version)
            */
            LogInfo("Running in self update mode");
            // It is assumed that temporary Updater full name is "<old_version_exe_full_path>_new.exe", e.g. AetpUpdater.exe_new.exe
            string exeName = ProgramInfo.ProcessFileName();
            string uppercaseExeName = exeName.ToUpper();
            // sanity check. "/selfupdate" must be called only for AetpUpdater.exe_new.exe
            if (CLIParser.selfupdate_mode && uppercaseExeName.EndsWith("_NEW.EXE"))
            {
                // To safely overwrite Updater with newer version need to be sure that old Updater process is not running 
                string procName = Process.GetCurrentProcess().ProcessName.ToUpper();
                string oldProcName = procName.Remove(procName.LastIndexOf(".EXE_NEW"));
                // Kill all process instances of old Updater
                Utilities.ProcessKiller.KillAllProcessesByName(oldProcName);

                // Get new substring from case-sensetive ProcessFileName, but index is calculated from case-insensitive path
                string destName = exeName.Remove(uppercaseExeName.LastIndexOf("_NEW.EXE"));
                System.IO.File.Copy(exeName, destName, true);

                //Update version file
                string versionFilename = Utilities.ProgramInfo.ExecutableName() + "_new.version.txt";
                if (File.Exists(versionFilename))
                {
                    
                    string[] fileData = File.ReadAllLines(versionFilename);
                    File.WriteAllText(fileData[1], fileData[0]);
                    File.Delete(versionFilename);
                }

                string newargs = "";
                //Remove selfupdate from args
                foreach( string arg in args)
                {
                    if (!arg.ToUpper().EndsWith("SELFUPDATE"))
                        newargs += arg + " ";
                }
#warning Updated version restart disabled
                //System.Diagnostics.Process.Start(destName, newargs);
#if DEBUG
                MessageBox.Show("Original File updated. Now exit current instance.", 
                                ProgramInfo.ProcessFileName(),
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);
#endif
                LogInfo("Self update completed. Exiting...");
                return true;
            }
            LogInfo("Wrong call to self update.");
#if DEBUG
                MessageBox.Show("SelfUpdate requested for a wrong file. Now exit.",
                                ProgramInfo.ProcessFileName(),
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);
#endif
            return false;
        }

        /************************************************************************
        *																		*
        *                                                                       *
        *																		*
        ************************************************************************/

        private static void DeleteTempExecutable()
        {
            // To safely remove temporary files need to be sure that no temporary Updater process is running 
            // Kill all process instances of temporary Updater
            LogInfo("Running in regular mode.");
            string tmpProcName = Process.GetCurrentProcess().ProcessName + ".EXE_NEW"; //AetpUpdater.exe_new
            Utilities.ProcessKiller.KillAllProcessesByName(tmpProcName);
//#if DEBUG
//                MessageBox.Show("Delete temporary updater.",
//                                ProgramInfo.ProcessFileName(),
//                                MessageBoxButtons.OK,
//                                MessageBoxIcon.Exclamation);
//#endif
            // Remove temporary Updater executable files
            string tmpName = ProgramInfo.ProcessFileName().ToUpper() + "_NEW.EXE";
            if (System.IO.File.Exists(tmpName))
                System.IO.File.Delete(tmpName);
        }

        /************************************************************************
        *																		*
        *                                                                       *
        *																		*
        ************************************************************************/

        private static bool IsActionReqiured(XElement file, Version targetVersion)
        {
            bool isReq = false;
            XAttribute att = file.Attribute("thisversion");
            isReq |= (att != null) && (targetVersion == new Version(att.Value));
            att = file.Attribute("before");
            isReq |= (att != null) && (targetVersion < new Version(att.Value));
            att = file.Attribute("after");
            isReq |= (att != null) && (targetVersion > new Version(att.Value));

            return isReq;
        }

        /************************************************************************
        *																		*
        *                                                                       *
        *																		*
        ************************************************************************/

        public static void ProcessPreinstallAction(Dictionary<string, Version> versions)
        {
            string strUri = "adasda";
            Uri src;
            if (!Uri.TryCreate(strUri, UriKind.Absolute, out src))
                src = new Uri(Properties.Resources.Builtin_PreinstallXmlUrl);
            using (WebClient downloader = new WebClient())
            {
                downloader.Proxy = System.Net.WebRequest.DefaultWebProxy;
                downloader.Headers.Add("pragma", "no-cache");
                downloader.Headers.Add("cache-control", "no-cache");
                XDocument preinstallXML = new XDocument();
                try
                {
                    preinstallXML = XDocument.Load(XmlReader.Create(downloader.OpenRead(src)));
                }
                catch (WebException ex)
                {
                    AppUpdater.LogError("Preinstall. Connection error: " + ex.Message);
                    return;
                }
                catch (XmlException ex)
                {
                    AppUpdater.LogError("Preinstall. Error in XML file: " + ex.Message);
                    return;
                }
                catch (Exception ex)
                {
                    AppUpdater.LogError("Preinstall. Unexpected error: " + ex.ToString());
                    return;
                }

                Version tgtVer;

                var removeFilesList = from file in preinstallXML.Root.Elements("removefile")
                                      where file.Attribute("module") != null && versions.TryGetValue(file.Attribute("module").Value, out tgtVer) == true && IsActionReqiured(file, tgtVer)
                                      select file;

                var removeDirectoriesList = from file in preinstallXML.Root.Elements("removedir")
                                            where file.Attribute("module") != null && versions.TryGetValue(file.Attribute("module").Value, out tgtVer) == true && IsActionReqiured(file, tgtVer)
                                            select file;


                foreach (var file in removeFilesList)
                {
                    try
                    {
                        XAttribute attName = file.Attribute("name");
                        if (attName != null)
                        {
                            string fname = attName.Value;
                            AppUpdater.LogFileInfo(String.Format("Preinstall. Removing file {0}", fname));
                            if (File.Exists(fname))
                                File.Delete(fname);
                        }
                    }
                    catch (Exception ex)
                    {
                        // log error and skip to the next entry
                        AppUpdater.LogError(String.Format("Preinstall. Error: {0}", ex.Message));
                    }
                }

                foreach (var dir in removeDirectoriesList)
                {
                    try
                    {
                        XAttribute attName = dir.Attribute("name");
                        if (attName != null)
                        {
                            XAttribute attRecursive = dir.Attribute("recursive");
                            bool isRecursive = (attRecursive != null) ? String.Compare(attRecursive.Value, "true", true) == 0 : false;
                            string fname = attName.Value;
                            AppUpdater.LogFileInfo(String.Format("Preinstall. Removing directory {0}, recursive = {1}", fname, isRecursive));
                            if (Directory.Exists(fname))
                            {
                                if (isRecursive || Directory.GetFiles(fname).Length == 0)
                                    Directory.Delete(fname, isRecursive);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // log error and skip to the next entry
                        AppUpdater.LogError(String.Format("Preinstall. Error: {0}", ex.Message));
                    }
                }
            }

        }

        public static void BuildLocalUpdateZipFile(string destpackage)
        {
            // Create temporary directory
            string tmpdir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tmpdir); // Directory will be deleted when backgroundUpdater completes its work

            List<String> modules = GetModulesNames();
            foreach (var moduleName in modules)
            {
                string src = Path.Combine(System.IO.Path.GetTempPath(), "Update_" + moduleName);
                src = Path.Combine(src, moduleName);
                Directory.Move(src, Path.Combine(tmpdir, moduleName));
            }

            FastZip packer = new FastZip();
            packer.CreateZip(destpackage, tmpdir, true, null);

            Directory.Delete(tmpdir, true);

        }

        /************************************************************************
        *																		*
        *                                                                       *
        *																		*
        ************************************************************************/

        public static void LogError(string msg)
        {
            if (errorlogger == null)
                CreateErrorLogger();
            
            if (errorlogger != null)
                errorlogger.Log(FileLogger.LogLevels.ERROR, msg);

            if (logger != null)
                logger.Log(FileLogger.LogLevels.ERROR, msg);
        }

        public static void LogWarning(string msg)
        {
            if (logger != null)
                logger.Log(FileLogger.LogLevels.WARNING, msg);
        }

        public static void LogInfo(string msg)
        {
            if (logger != null)
                logger.Log(FileLogger.LogLevels.INFO, msg);
        }

        public static void LogFileInfo(string msg)
        {
            if (logger != null)
                logger.Log(FileLogger.LogLevels.FILEINFO, msg);
        }        

        public static void LogDebug(string msg)
        {
            if (logger != null)
                logger.Log(" DEBUG  ", msg);
        }

        // Shortcuts

        
        // selfupdate_trigger shows that new AetpUpdater was downloaded. Thus need to perform self update.
        // selfupdate_trigger is set by ModuleUpdater::ExtractFiles();
        public static bool selfupdate_trigger = false; 

        private static FileLogger logger; // Program wide static Updater.Log() method uses this logger for output.
        private static FileLogger errorlogger;

        public static void CreateLogger(string logLevel)
        {
            FileLogger.LogLevels l = FileLogger.LogLevels.NONE;

            if (logLevel.CompareTo("0") == 0)
                l = FileLogger.LogLevels.NONE;
            else if (logLevel.CompareTo("1") == 0)
                l = FileLogger.LogLevels.ERROR;
            else if (logLevel.CompareTo("2") == 0)
                l = FileLogger.LogLevels.WARNING;
            else if (logLevel.CompareTo("3") == 0)
                l = FileLogger.LogLevels.INFO;
            else if (logLevel.CompareTo("4") == 0)
                l = FileLogger.LogLevels.FILEINFO;
            
            if (l != FileLogger.LogLevels.NONE)
            {
                string fname = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + @".log";
                logger = new FileLogger(fname, false, l);
            }
        }
        public static void CreateErrorLogger()
        {
            string fname = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + @".error.log";
            errorlogger = new FileLogger(fname, true);
        }

        public static List<string> GetModulesNames()
        {
            List<string> modules = new List<string>();
            // Seldon 1.5
            //modules.Add("Seldon.common");
            //modules.Add("Seldon");
            //modules.Add("devexpress");
            //modules.Add("consulting");
            //modules.Add("updater");

            // Selodn 1.6
            //TODO : Seldon (назовём его Core),Consultant,DevExpress
            
                modules.Add("seldon16");
                modules.Add("model");
                modules.Add("updater16");
            

                return modules;
        }
    }

}


