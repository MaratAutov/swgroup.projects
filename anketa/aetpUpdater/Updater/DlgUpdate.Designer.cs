﻿namespace aetpUpdater.Updater
{
    partial class DlgUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgUpdate));
            this.ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.backgroundUpdater = new aetpUpdater.Utilities.PausableBackgroundWorker();
            this.spUpdatePages = new aetpUpdater.StackPanel();
            this.tabPageBegin = new System.Windows.Forms.TabPage();
            this.lblUpdateNotice = new System.Windows.Forms.Label();
            this.lblInstallerVersion = new System.Windows.Forms.Label();
            this.lblUpdating = new System.Windows.Forms.Label();
            this.btnBeginNext = new aetpUpdater.PictureButton();
            this.pbtnBeginExit = new aetpUpdater.PictureButton();
            this.tabPageProgress = new System.Windows.Forms.TabPage();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.pbtnProgressAbort = new aetpUpdater.PictureButton();
            this.lblProgressProcess = new System.Windows.Forms.Label();
            this.tabPageComplete = new System.Windows.Forms.TabPage();
            this.pbtnCompleteDone = new aetpUpdater.PictureButton();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblCompletePressDone = new System.Windows.Forms.Label();
            this.lblCompleteSuccessful = new System.Windows.Forms.Label();
            this.tabPageError = new System.Windows.Forms.TabPage();
            this.lblErrorHeader = new System.Windows.Forms.Label();
            this.pbtnExitError = new aetpUpdater.PictureButton();
            this.lblErrMsg = new System.Windows.Forms.Label();
            this.pbErrorError = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
            this.spUpdatePages.SuspendLayout();
            this.tabPageBegin.SuspendLayout();
            this.tabPageProgress.SuspendLayout();
            this.tabPageComplete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.tabPageError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbErrorError)).BeginInit();
            this.SuspendLayout();
            // 
            // ErrorProvider
            // 
            this.ErrorProvider.ContainerControl = this;
            // 
            // backgroundUpdater
            // 
            this.backgroundUpdater.WorkerReportsProgress = true;
            this.backgroundUpdater.WorkerSupportsCancellation = true;
            this.backgroundUpdater.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundUpdater_DoWork);
            this.backgroundUpdater.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundUpdater_ProgressChanged);
            this.backgroundUpdater.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundUpdater_RunWorkerCompleted);
            // 
            // spUpdatePages
            // 
            this.spUpdatePages.Controls.Add(this.tabPageBegin);
            this.spUpdatePages.Controls.Add(this.tabPageProgress);
            this.spUpdatePages.Controls.Add(this.tabPageComplete);
            this.spUpdatePages.Controls.Add(this.tabPageError);
            this.spUpdatePages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spUpdatePages.Location = new System.Drawing.Point(0, 0);
            this.spUpdatePages.Name = "spUpdatePages";
            this.spUpdatePages.SelectedIndex = 0;
            this.spUpdatePages.Size = new System.Drawing.Size(650, 350);
            this.spUpdatePages.TabIndex = 0;
            this.spUpdatePages.TabStop = false;
            this.spUpdatePages.Selected += new System.Windows.Forms.TabControlEventHandler(this.spUpdatePages_Selected);
            // 
            // tabPageBegin
            // 
            this.tabPageBegin.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageBegin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageBegin.Controls.Add(this.lblUpdateNotice);
            this.tabPageBegin.Controls.Add(this.lblInstallerVersion);
            this.tabPageBegin.Controls.Add(this.lblUpdating);
            this.tabPageBegin.Controls.Add(this.btnBeginNext);
            this.tabPageBegin.Controls.Add(this.pbtnBeginExit);
            this.tabPageBegin.Location = new System.Drawing.Point(4, 22);
            this.tabPageBegin.Name = "tabPageBegin";
            this.tabPageBegin.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBegin.Size = new System.Drawing.Size(642, 324);
            this.tabPageBegin.TabIndex = 0;
            this.tabPageBegin.UseVisualStyleBackColor = true;
            // 
            // lblUpdateNotice
            // 
            this.lblUpdateNotice.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUpdateNotice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblUpdateNotice.Location = new System.Drawing.Point(34, 135);
            this.lblUpdateNotice.Name = "lblUpdateNotice";
            this.lblUpdateNotice.Size = new System.Drawing.Size(576, 73);
            this.lblUpdateNotice.TabIndex = 20;
            this.lblUpdateNotice.Text = resources.GetString("lblUpdateNotice.Text");
            // 
            // lblInstallerVersion
            // 
            this.lblInstallerVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInstallerVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblInstallerVersion.Location = new System.Drawing.Point(5, 217);
            this.lblInstallerVersion.Name = "lblInstallerVersion";
            this.lblInstallerVersion.Size = new System.Drawing.Size(595, 19);
            this.lblInstallerVersion.TabIndex = 19;
            this.lblInstallerVersion.Text = "Версия программы обновления: 0.0.0.0";
            this.lblInstallerVersion.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblUpdating
            // 
            this.lblUpdating.AutoSize = true;
            this.lblUpdating.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUpdating.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblUpdating.Location = new System.Drawing.Point(34, 112);
            this.lblUpdating.Name = "lblUpdating";
            this.lblUpdating.Size = new System.Drawing.Size(236, 19);
            this.lblUpdating.TabIndex = 17;
            this.lblUpdating.Text = "Доступно обновление системы";
            // 
            // btnBeginNext
            // 
            this.btnBeginNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBeginNext.BackColor = System.Drawing.Color.Transparent;
            this.btnBeginNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBeginNext.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBeginNext.ImageDisabled = global::aetpUpdater.Properties.Resources.button_next_disabled;
            this.btnBeginNext.ImageDown = global::aetpUpdater.Properties.Resources.button_next_down;
            this.btnBeginNext.ImageOver = global::aetpUpdater.Properties.Resources.button_next_over;
            this.btnBeginNext.ImageUp = global::aetpUpdater.Properties.Resources.button_next_up;
            this.btnBeginNext.Location = new System.Drawing.Point(368, 263);
            this.btnBeginNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnBeginNext.Name = "btnBeginNext";
            this.btnBeginNext.Size = new System.Drawing.Size(173, 48);
            this.btnBeginNext.TabIndex = 2;
            this.btnBeginNext.Click += new System.EventHandler(this.btnBeginNext_Click);
            // 
            // pbtnBeginExit
            // 
            this.pbtnBeginExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnBeginExit.BackColor = System.Drawing.Color.Transparent;
            this.pbtnBeginExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnBeginExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.pbtnBeginExit.ImageDisabled = null;
            this.pbtnBeginExit.ImageDown = global::aetpUpdater.Properties.Resources.button_cancel_down;
            this.pbtnBeginExit.ImageOver = global::aetpUpdater.Properties.Resources.button_cancel_over;
            this.pbtnBeginExit.ImageUp = global::aetpUpdater.Properties.Resources.button_cancel_up;
            this.pbtnBeginExit.Location = new System.Drawing.Point(556, 263);
            this.pbtnBeginExit.Margin = new System.Windows.Forms.Padding(4);
            this.pbtnBeginExit.Name = "pbtnBeginExit";
            this.pbtnBeginExit.Size = new System.Drawing.Size(44, 49);
            this.pbtnBeginExit.TabIndex = 3;
            this.pbtnBeginExit.Click += new System.EventHandler(this.pbtnBeginExit_Click);
            // 
            // tabPageProgress
            // 
            this.tabPageProgress.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageProgress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageProgress.Controls.Add(this.progressBar1);
            this.tabPageProgress.Controls.Add(this.pbtnProgressAbort);
            this.tabPageProgress.Controls.Add(this.lblProgressProcess);
            this.tabPageProgress.Location = new System.Drawing.Point(4, 22);
            this.tabPageProgress.Name = "tabPageProgress";
            this.tabPageProgress.Size = new System.Drawing.Size(642, 324);
            this.tabPageProgress.TabIndex = 3;
            this.tabPageProgress.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(46, 150);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(554, 38);
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 26;
            // 
            // pbtnProgressAbort
            // 
            this.pbtnProgressAbort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnProgressAbort.BackColor = System.Drawing.Color.Transparent;
            this.pbtnProgressAbort.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnProgressAbort.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnProgressAbort.ImageDisabled = null;
            this.pbtnProgressAbort.ImageDown = global::aetpUpdater.Properties.Resources.button_cancel_down;
            this.pbtnProgressAbort.ImageOver = global::aetpUpdater.Properties.Resources.button_cancel_over;
            this.pbtnProgressAbort.ImageUp = global::aetpUpdater.Properties.Resources.button_cancel_up;
            this.pbtnProgressAbort.Location = new System.Drawing.Point(556, 263);
            this.pbtnProgressAbort.Margin = new System.Windows.Forms.Padding(4);
            this.pbtnProgressAbort.Name = "pbtnProgressAbort";
            this.pbtnProgressAbort.Size = new System.Drawing.Size(44, 48);
            this.pbtnProgressAbort.TabIndex = 0;
            this.pbtnProgressAbort.Click += new System.EventHandler(this.pbtnProgressAbort_Click);
            // 
            // lblProgressProcess
            // 
            this.lblProgressProcess.AutoSize = true;
            this.lblProgressProcess.BackColor = System.Drawing.Color.Transparent;
            this.lblProgressProcess.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProgressProcess.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblProgressProcess.Location = new System.Drawing.Point(42, 112);
            this.lblProgressProcess.Name = "lblProgressProcess";
            this.lblProgressProcess.Size = new System.Drawing.Size(161, 19);
            this.lblProgressProcess.TabIndex = 3;
            this.lblProgressProcess.Text = "Процесс обновления";
            // 
            // tabPageComplete
            // 
            this.tabPageComplete.BackColor = System.Drawing.Color.Transparent;
            this.tabPageComplete.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageComplete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageComplete.Controls.Add(this.pbtnCompleteDone);
            this.tabPageComplete.Controls.Add(this.pictureBox6);
            this.tabPageComplete.Controls.Add(this.lblCompletePressDone);
            this.tabPageComplete.Controls.Add(this.lblCompleteSuccessful);
            this.tabPageComplete.Location = new System.Drawing.Point(4, 22);
            this.tabPageComplete.Name = "tabPageComplete";
            this.tabPageComplete.Size = new System.Drawing.Size(642, 324);
            this.tabPageComplete.TabIndex = 4;
            // 
            // pbtnCompleteDone
            // 
            this.pbtnCompleteDone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnCompleteDone.BackColor = System.Drawing.Color.Transparent;
            this.pbtnCompleteDone.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnCompleteDone.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnCompleteDone.ImageDisabled = null;
            this.pbtnCompleteDone.ImageDown = global::aetpUpdater.Properties.Resources.button_finish_down;
            this.pbtnCompleteDone.ImageOver = global::aetpUpdater.Properties.Resources.button_finish_over;
            this.pbtnCompleteDone.ImageUp = global::aetpUpdater.Properties.Resources.button_finish_up;
            this.pbtnCompleteDone.Location = new System.Drawing.Point(500, 266);
            this.pbtnCompleteDone.Margin = new System.Windows.Forms.Padding(4);
            this.pbtnCompleteDone.Name = "pbtnCompleteDone";
            this.pbtnCompleteDone.Size = new System.Drawing.Size(103, 48);
            this.pbtnCompleteDone.TabIndex = 0;
            this.pbtnCompleteDone.Click += new System.EventHandler(this.pbtnCompleteDone_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.Image = global::aetpUpdater.Properties.Resources.ico_ok;
            this.pictureBox6.Location = new System.Drawing.Point(97, 123);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(48, 48);
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            // 
            // lblCompletePressDone
            // 
            this.lblCompletePressDone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCompletePressDone.AutoSize = true;
            this.lblCompletePressDone.BackColor = System.Drawing.Color.Transparent;
            this.lblCompletePressDone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCompletePressDone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblCompletePressDone.Location = new System.Drawing.Point(161, 145);
            this.lblCompletePressDone.Name = "lblCompletePressDone";
            this.lblCompletePressDone.Size = new System.Drawing.Size(388, 16);
            this.lblCompletePressDone.TabIndex = 4;
            this.lblCompletePressDone.Text = "Для завершения работы мастера установки нажмите «Готово».";
            // 
            // lblCompleteSuccessful
            // 
            this.lblCompleteSuccessful.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCompleteSuccessful.AutoSize = true;
            this.lblCompleteSuccessful.BackColor = System.Drawing.Color.Transparent;
            this.lblCompleteSuccessful.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCompleteSuccessful.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblCompleteSuccessful.Location = new System.Drawing.Point(161, 123);
            this.lblCompleteSuccessful.Name = "lblCompleteSuccessful";
            this.lblCompleteSuccessful.Size = new System.Drawing.Size(302, 19);
            this.lblCompleteSuccessful.TabIndex = 3;
            this.lblCompleteSuccessful.Text = "Процесс обновления успешно завершен";
            // 
            // tabPageError
            // 
            this.tabPageError.BackgroundImage = global::aetpUpdater.Properties.Resources.Rosteh_install_1;
            this.tabPageError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPageError.Controls.Add(this.lblErrorHeader);
            this.tabPageError.Controls.Add(this.pbtnExitError);
            this.tabPageError.Controls.Add(this.lblErrMsg);
            this.tabPageError.Controls.Add(this.pbErrorError);
            this.tabPageError.Location = new System.Drawing.Point(4, 22);
            this.tabPageError.Margin = new System.Windows.Forms.Padding(1);
            this.tabPageError.Name = "tabPageError";
            this.tabPageError.Size = new System.Drawing.Size(642, 324);
            this.tabPageError.TabIndex = 6;
            this.tabPageError.UseVisualStyleBackColor = true;
            // 
            // lblErrorHeader
            // 
            this.lblErrorHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblErrorHeader.AutoSize = true;
            this.lblErrorHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblErrorHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblErrorHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(137)))));
            this.lblErrorHeader.Location = new System.Drawing.Point(195, 123);
            this.lblErrorHeader.Name = "lblErrorHeader";
            this.lblErrorHeader.Size = new System.Drawing.Size(313, 19);
            this.lblErrorHeader.TabIndex = 4;
            this.lblErrorHeader.Text = "В процессе обновления возникла ошибка";
            // 
            // pbtnExitError
            // 
            this.pbtnExitError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbtnExitError.BackColor = System.Drawing.Color.Transparent;
            this.pbtnExitError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbtnExitError.DialogResult = System.Windows.Forms.DialogResult.None;
            this.pbtnExitError.ImageDisabled = null;
            this.pbtnExitError.ImageDown = global::aetpUpdater.Properties.Resources.button_cancel_down;
            this.pbtnExitError.ImageOver = global::aetpUpdater.Properties.Resources.button_cancel_over;
            this.pbtnExitError.ImageUp = global::aetpUpdater.Properties.Resources.button_cancel_up;
            this.pbtnExitError.Location = new System.Drawing.Point(556, 264);
            this.pbtnExitError.Margin = new System.Windows.Forms.Padding(1);
            this.pbtnExitError.Name = "pbtnExitError";
            this.pbtnExitError.Size = new System.Drawing.Size(44, 49);
            this.pbtnExitError.TabIndex = 1;
            this.pbtnExitError.Load += new System.EventHandler(this.pbtnExitError_Load);
            // 
            // lblErrMsg
            // 
            this.lblErrMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblErrMsg.BackColor = System.Drawing.Color.Transparent;
            this.lblErrMsg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblErrMsg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.lblErrMsg.Location = new System.Drawing.Point(195, 145);
            this.lblErrMsg.Name = "lblErrMsg";
            this.lblErrMsg.Size = new System.Drawing.Size(299, 127);
            this.lblErrMsg.TabIndex = 5;
            this.lblErrMsg.Text = "Не могу создать каталог";
            // 
            // pbErrorError
            // 
            this.pbErrorError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pbErrorError.BackColor = System.Drawing.Color.Transparent;
            this.pbErrorError.Image = global::aetpUpdater.Properties.Resources.ico32_err;
            this.pbErrorError.Location = new System.Drawing.Point(148, 124);
            this.pbErrorError.Name = "pbErrorError";
            this.pbErrorError.Size = new System.Drawing.Size(32, 32);
            this.pbErrorError.TabIndex = 3;
            this.pbErrorError.TabStop = false;
            // 
            // DlgUpdate
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(650, 350);
            this.Controls.Add(this.spUpdatePages);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "DlgUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Обновление Анкета.Редактор";
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
            this.spUpdatePages.ResumeLayout(false);
            this.tabPageBegin.ResumeLayout(false);
            this.tabPageBegin.PerformLayout();
            this.tabPageProgress.ResumeLayout(false);
            this.tabPageProgress.PerformLayout();
            this.tabPageComplete.ResumeLayout(false);
            this.tabPageComplete.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.tabPageError.ResumeLayout(false);
            this.tabPageError.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbErrorError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private aetpUpdater.Utilities.PausableBackgroundWorker backgroundUpdater;
        private System.Windows.Forms.ErrorProvider ErrorProvider;
        private StackPanel spUpdatePages;
        private System.Windows.Forms.TabPage tabPageBegin;
        private System.Windows.Forms.Label lblInstallerVersion;
        private System.Windows.Forms.Label lblUpdating;
        private PictureButton btnBeginNext;
        private PictureButton pbtnBeginExit;
        private System.Windows.Forms.TabPage tabPageProgress;
        private System.Windows.Forms.ProgressBar progressBar1;
        private PictureButton pbtnProgressAbort;
        private System.Windows.Forms.Label lblProgressProcess;
        private System.Windows.Forms.TabPage tabPageComplete;
        private PictureButton pbtnCompleteDone;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblCompletePressDone;
        private System.Windows.Forms.Label lblCompleteSuccessful;
        private System.Windows.Forms.TabPage tabPageError;
        private System.Windows.Forms.Label lblErrorHeader;
        private PictureButton pbtnExitError;
        private System.Windows.Forms.Label lblErrMsg;
        private System.Windows.Forms.PictureBox pbErrorError;
        private System.Windows.Forms.Label lblUpdateNotice;
    }
}

