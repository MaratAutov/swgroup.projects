﻿using System;
using System.Collections.Generic;

using System.Xml; // for Module Description XML File
//using System.Security; //for exceptions in xmlReader
using System.IO;

using System.Net;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

using ICSharpCode.SharpZipLib.Zip;  // for ZIP

using aetpUpdater.Utilities;

namespace aetpUpdater.Updater
{

    public class ModuleUpdater : IDisposable
    {
        #region Constructor, Destructor and Disposer
        /// <summary>
        /// Initialize members and create temporary directory
        /// </summary>
        /// <param name="mname">Module name</param>
        /// <param name="srvr">update server URI</param>
        /// <param name="localStorage">true if srvr points to local file system</param>
        public ModuleUpdater(string mname, string srvr, bool isLocalStorage)
        {
            if (string.IsNullOrEmpty(mname))
                throw new Exception("Module name is empty");
            if (string.IsNullOrEmpty("srvr"))
                throw new Exception("Server URI is empty");

            moduleName = mname;
            obsoleteFiles = new List<string>();
            packages = new List<string>();
            packagesSize = 0;

            if (isLocalStorage)
            {
                updateServerUri = null;
                useLocalStorage = srvr;
            }
            else
            {
                // Base URI for *.version.txt and <module>.xml files
                // Actual file is accessible as "updateServerUri/moduleName/currentVersionPath"
                updateServerUri = srvr;
                useLocalStorage = null;
            }


            // Create temporary directory
            tmpdir = Path.Combine(System.IO.Path.GetTempPath(),
                                            "Update_" + moduleName);
            if (Directory.Exists(tmpdir) == false)
                Directory.CreateDirectory(tmpdir);

            downloader = new ExtendedWebClient();
            downloader.Proxy = WebRequest.DefaultWebProxy;
            downloader.Headers.Add("pragma", "no-cache");
            downloader.Headers.Add("cache-control", "no-cache");
            downloader.DownloadProgressChanged += downloader_DownloadProgressChanged;
            downloader.DownloadFileCompleted += downloader_DownloadFileCompleted;

            downloadLock = new Semaphore(1, 1);
        }

        ~ModuleUpdater()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);  
        }

        /// <summary>
        /// Clean up temporary folder
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // If you need thread safety, use a lock around these 
            // operations, as well as in your methods that use the resource.

            if (!disposed)
            {
                if (disposing)
                {
                    if (Directory.Exists(tmpdir))
                        Directory.Delete(tmpdir, true);
                }
                // Indicate that the instance has been disposed.
                disposed = true;
            }
        }

        #endregion

        #region Basic accessors

        public static bool ExiststModelsUpdate = false;

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
        }

        public ulong DownloadSize
        {
            get
            {
                return packagesSize;
            }
        }
        #endregion

        #region Version Control
        //BUG: Has Newer version return true if both are 0.
        /// <summary>
        /// Compares local version to server version
        /// and sets target version to server version
        /// </summary>
        /// <returns>true if greater version available</returns>
        public bool HasNewerVersion()
        {
            Version v = (String.IsNullOrEmpty(useLocalStorage)) ? ServerVersion : LocalUpdateVersion;

            if (v != LocalVersion)
            {
                targetVersion = v;
                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// Reads current version from Server
        /// </summary>
        public Version ServerVersion
        {
            get
            {
                // Do not override an error with "0.0.0.0". To recognize that there is no new version, but connection to the server failed.
                // Otherwise if both local and server version cannot be read, HasNewerVersion() will show that there is nothing to update.
                string fname = String.Format("{0}/{1}/{0}.version.txt", moduleName, AppUpdater.CurrentVersionPath);
                if (!DownloadFile(updateServerUri, fname)
                    && CLIParser.TestingModeEnabled) 
                {
                    fname = String.Format("{0}/{1}/{0}.version.txt", moduleName, AppUpdater.DefaultVersionPath);
                    DownloadFile(updateServerUri, fname); 
                }

                fname = Path.Combine(tmpdir, fname);
                
                // throw exception on failure
                string strver = File.ReadAllText(fname);
                return new Version(strver);
            }
        }
        /// <summary>
        /// Version for update from local directory
        /// </summary>
        public Version LocalUpdateVersion
        {
            get
            {
                //string fname = moduleName + ".version.txt";
                string fname = String.Format("{0}/{1}/{0}.version.txt", moduleName, AppUpdater.DefaultVersionPath);
                fname = Path.Combine(useLocalStorage, fname);
                // throw exception on failure
                string strver = File.ReadAllText(fname);
                return new Version(strver);
            }
        }
        /// <summary>
        /// Returns module version stored in module_name.version.txt
        /// returns "0.0.0.0" if file cannot be read;
        /// </summary>
        public Version LocalVersion
        {
            get
            {
                string fname = moduleName + ".version.txt";
                string strver = "0.0.0.0";
                fname = Path.Combine(UpdaterConfiguration.AppPath, fname);
                if (File.Exists(fname))
                    strver = File.ReadAllText(fname);
                else
                    strver = "0.0.0.0";

                return new Version(strver);
            }
        }
        /// <summary>
        /// Returns version to update to
        /// </summary>
        public Version TargetVersion
        {
            get
            {
                if (targetVersion == null)
                    return new Version("0.0.0.0");
                else
                    return targetVersion;
            }
            set
            {
                targetVersion = value;
            }
        }

        #endregion

        private string GetXMLFileName()
        {
            string xmlFileName = null;

            if (String.IsNullOrEmpty(useLocalStorage))
            {
                xmlFileName = String.Format("{0}/{1}/{0}.xml", moduleName, targetVersion);

                DownloadFile(updateServerUri, xmlFileName);
                AppUpdater.LogFileInfo("Successful download: " + xmlFileName);
                xmlFileName = Path.Combine(tmpdir, xmlFileName);
            }
            else
            {
                xmlFileName = String.Format("{0}/{1}/{0}.xml", moduleName, targetVersion);
                xmlFileName = Path.Combine(useLocalStorage, xmlFileName);
            }

            return xmlFileName;
        }

        /// <summary>
        /// Compares MD5 checksums of local files and server files. Server files hashes are stored in xml file.
        /// </summary>
        /// <returns>
        /// true if one or mor files has different hash. Updates obsoleteFiles and packages lists.
        /// false if there are no files to update or error occured.
        /// </returns>
        public int HasObsoleteFiles()
        {
            //Use targetVersion to support update to specific version

            string xmlFileName = GetXMLFileName();

            try
            {
                using (XmlReader reader = XmlReader.Create(xmlFileName))
                {
                    #region walk through xml and check files
                    while (reader.Read())
                    {
                        if (reader.IsStartElement() && reader.Name == "module")
                        {
                            // Read module attributes
                            // scan files defined within <module> </module>

                            string xmlName = reader.GetAttribute("name");
                            Version xmlVersion = new Version(reader.GetAttribute("version"));
                            string xmlUristr = reader.GetAttribute("URL");
                            //data validation
                            if (moduleName.ToUpper() != xmlName.ToUpper())
                            {
                                string msg = String.Format("{0}got \"{1}\"expected \"{2}\"", Properties.Resources.ExMsg_NameMismatch, xmlName, moduleName);
                                AppUpdater.LogError(msg);
                                throw new Exception(msg);
                            }
                            if (xmlVersion != TargetVersion)
                            {
                                string msg = String.Format("{0}: got \"{1}\", expected \"{2}\"", Properties.Resources.ExMsg_VersionMismatch, xmlVersion, TargetVersion);
                                AppUpdater.LogError(msg);
                                throw new Exception(msg);
                            }

                            storeBaseUri = xmlUristr;
#if DEBUG_XML_PARSE
                            MessageBox.Show("Name: " + xmlName + "\nVersion: " + xmlVersion + "\nURI: " + xmlUristr,
                                            "Module Info");
#endif
                            reader.MoveToContent();

                            while (reader.Read())
                            {
                                if (reader.IsEmptyElement && reader.Name == "file")
                                {
                                    string fname = reader.GetAttribute("name");
                                    string fmd5 = reader.GetAttribute("md5");
                                    string fstore = reader.GetAttribute("storage");
                                    string fstoresize = reader.GetAttribute("storagesize");
                                    bool isFileOk = false;

                                    string fullfname = Path.Combine(UpdaterConfiguration.AppPath, fname);

                                    if (File.Exists(fullfname))
                                    {
                                        try
                                        {
                                            isFileOk = Utilities.FileHasher.VerifyMd5Hash(fullfname, fmd5);
                                        }
                                        catch (FileNotFoundException)
                                        {
                                            MessageBox.Show(fname,
                                                        "Файл не существует",
                                                        MessageBoxButtons.OK);
                                        }
                                        catch (DirectoryNotFoundException)
                                        {
                                            MessageBox.Show(fname,
                                                         "Путь к файлу не существует",
                                                         MessageBoxButtons.OK);
                                        }
                                    } // silently add missing files to update list

                                    if (!isFileOk && fname != null && fstore != null)
                                    {
                                        obsoleteFiles.Add(fname.ToUpper().Replace('\\','/'));
                                        if (!packages.Contains(fstore))
                                        {
                                            packages.Add(fstore);
                                            packagesSize += Convert.ToUInt64(fstoresize);
                                        }
                                    }
#if DEBUG_XML_PARSE
                                    MessageBox.Show(fname + "\nMD5: " + fmd5.ToUpper() + " - " + isFileOk + "\nStorage: " + fstore + " (" + fstoresize + " bytes)",
                                                    "Проверка файла",
                                                    MessageBoxButtons.OK);

#endif
                                    if (isFileOk)
                                        AppUpdater.LogFileInfo("MD5 Verification passed for " + fname);
                                    else
                                        AppUpdater.LogFileInfo(String.Format("MD5 Verification FAILED for {0}, MD5= {1}", fname, fmd5.ToUpper()));
                                }
                            }


                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)// XmlReader.Create() may throw: ArgumentNullException SecurityException FileNotFoundException UriFormatException 
            {
#warning fix invalid xml file exception
                MessageBox.Show("Обратитесь к разработчику. Код UPD001\n" + ex.Message,
                                "Ошибка",
                                MessageBoxButtons.OK);
                throw;
            }
            return obsoleteFiles.Count;
        }

        long FileSize(string relativeUri)
        {
            string fName = Path.Combine(tmpdir, relativeUri);
            FileInfo fInfo = new FileInfo(fName);
            return fInfo.Length;
        }

        private void downloader_DownloadProgressChanged(Object sender, DownloadProgressChangedEventArgs e)
        {
            DownloadReport((ulong)(e.BytesReceived - prevReceivedBytes));
            prevReceivedBytes = e.BytesReceived;
#if DEBUG
            AppUpdater.LogFileInfo(String.Format("Downloaded {0} of {1} bytes", e.BytesReceived, e.TotalBytesToReceive));
#endif
        }
        private void downloader_DownloadFileCompleted(Object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                AppUpdater.LogFileInfo("Download canceled");
            }
            else if (e.Error != null)
            {
                AppUpdater.LogError("Download failed:: " + e.Error.Message);
            }
            else // Successful
            {
                AppUpdater.LogFileInfo("Download successful "/* + src*/);
            }

            /*
            rtbLog.Text += String.Format(@"{0:yyyy'-'MM'-'dd' 'HH':'mm':'ss'.'ffff}
                                         Cancelled: {1} 
                                         Error: {2}", DateTime.Now, e.Cancelled, e.Error);
             */
            downloadLock.Release();
            prevReceivedBytes = 0;
        }

        /// <summary>
        /// Downloads file from baseUri/relativeUri to temporary_location/relativeUri
        /// </summary>
        /// <param name="baseUri"></param>
        /// <param name="relativeUri"></param>
        /// <returns></returns>
        private bool DownloadFile(string baseUri, string relativeUri)
        {
            string dest = Path.Combine(tmpdir, relativeUri);

            string destdir = Path.GetDirectoryName(dest);
            if (Directory.Exists(destdir) == false)
                Directory.CreateDirectory(destdir);
            #if DEBUG_DOWNLOAD    
            MessageBox.Show("Source:\n" + baseUri + relativeUri + "\n\nDestination:\n" + dest, "Downloading File");
            #endif
            if (File.Exists(dest))
                File.Delete(dest);

            if (!baseUri.EndsWith("/"))
                baseUri += "/";

            //Uri src = new Uri(baseUri + relativeUri);
            Uri src;
            if (Uri.TryCreate(baseUri + relativeUri, UriKind.Absolute, out src) == false)
            {
                src = new Uri(String.Format("{0}\\{1}{2}", Application.StartupPath, baseUri, relativeUri));
            }
            
            AppUpdater.LogFileInfo("Attempting download: " + src);
            
            // ExtendedWebClient downloader = new ExtendedWebClient();
            // downloader.Proxy = System.Net.WebRequest.DefaultWebProxy;
            // downloader.Headers.Add("pragma", "no-cache");
            // downloader.Headers.Add("cache-control", "no-cache");

            try
            {
                downloader.DownloadFile(src, dest);
                // AppUpdater.LogFileInfo("Successful download: " + src);
            }
            catch (Exception ex)
            {
                AppUpdater.LogError(String.Format("Failed while downloading file {0}:{1}", src, ex.Message));
                #if DEBUG_DOWNLOAD 
                MessageBox.Show("Произошла ошибка при попытке обращения на сервер.\n" + ex.ToString());
                //#else
                //  MessageBox.Show("Произошла ошибка при попытке обращения на сервер.\nURI=" + src);
                #endif
                throw ex;
                //return false;
            }
            return true;
        }

        private bool DownloadFileAsync(string baseUri, string relativeUri)
        {
            string dest = Path.Combine(tmpdir, relativeUri);

            string destdir = Path.GetDirectoryName(dest);
            if (Directory.Exists(destdir) == false)
                Directory.CreateDirectory(destdir);
            #if DEBUG_DOWNLOAD    
            MessageBox.Show("Source:\n" + baseUri + relativeUri + "\n\nDestination:\n" + dest, "Downloading File");
            #endif
            if (File.Exists(dest))
                File.Delete(dest);

            if (!baseUri.EndsWith("/"))
                baseUri += "/";

            //Uri src = new Uri(baseUri + relativeUri);
            Uri src;
            if (Uri.TryCreate(baseUri + relativeUri, UriKind.Absolute, out src) == false)
            {
                src = new Uri(String.Format("{0}\\{1}{2}", Application.StartupPath, baseUri, relativeUri));
            }

            AppUpdater.LogFileInfo("Attempting download: " + src);

            // ExtendedWebClient downloader = new ExtendedWebClient();
            // downloader.Proxy = System.Net.WebRequest.DefaultWebProxy;
            // downloader.Headers.Add("pragma", "no-cache");
            // downloader.Headers.Add("cache-control", "no-cache");

            try
            {
                prevReceivedBytes = 0;
                downloader.DownloadFileAsync(src, dest);
                // AppUpdater.LogFileInfo("Successful download: " + src);
            }
            catch (Exception ex)
            {
                AppUpdater.LogError(String.Format("Failed while downloading file {0}:{1}", src, ex.Message));
#if DEBUG_DOWNLOAD 
                MessageBox.Show("Произошла ошибка при попытке обращения на сервер.\n" + ex.ToString());
//#else
//                    MessageBox.Show("Произошла ошибка при попытке обращения на сервер.\nURI=" + src);
#endif
                throw ex;
                //return false;
            }
            return true;
        }

        #region Download All files
        public void CreateDownloadList()
        {
        string xmlFileName = GetXMLFileName();

        try
        {
            using (XmlReader reader = XmlReader.Create(xmlFileName))
            {
                #region walk through xml and check files
                while (reader.Read())
                {
                    if (reader.IsStartElement() && reader.Name == "module")
                    {
                        // Read module attributes
                        // scan files defined within <module> </module>

                        string xmlName = reader.GetAttribute("name");
                        Version xmlVersion = new Version(reader.GetAttribute("version"));
                        string xmlUristr = reader.GetAttribute("URL");
                        //data validation
                        if (moduleName.ToUpper() != xmlName.ToUpper())
                        {
                            string msg = String.Format("{0}got \"{1}\"expected \"{2}\"", 
                                Properties.Resources.ExMsg_NameMismatch, 
                                xmlName, 
                                moduleName);
                            AppUpdater.LogError(msg);
                            throw new Exception(msg);
                        }
                        if (xmlVersion != TargetVersion)
                        {
                            string msg = String.Format("{0}: got \"{1}\", expected \"{2}\"", 
                                Properties.Resources.ExMsg_VersionMismatch, 
                                xmlVersion, 
                                TargetVersion);
                            AppUpdater.LogError(msg);
                            throw new Exception(msg);
                        }

                        storeBaseUri = xmlUristr;

                        reader.MoveToContent();

                        while (reader.Read())
                        {
                            if (reader.IsEmptyElement && reader.Name == "file")
                            {
                                string fname = reader.GetAttribute("name");
                                string fstore = reader.GetAttribute("storage");
                                string fstoresize = reader.GetAttribute("storagesize");

                                //string fullfname = Path.Combine(UpdaterConfiguration.AppPath, fname);

                                if (fname != null && fstore != null)
                                {
                                    obsoleteFiles.Add(fname.ToUpper().Replace('\\', '/'));
                                    if (!packages.Contains(fstore))
                                    {
                                        packages.Add(fstore);
                                        packagesSize += Convert.ToUInt64(fstoresize);
                                    }
                                }
                            }
                        }


                    }
                }
                #endregion
            }
        }
        catch (Exception ex)// XmlReader.Create() may throw: ArgumentNullException SecurityException FileNotFoundException UriFormatException 
        {
            System.Windows.Forms.MessageBox.Show("Обратитесь к разработчику. Код UPD001\n" + ex.Message,
                            "Ошибка",
                            MessageBoxButtons.OK);
            throw;
        }
        }
        public void DownloadEntireModule(ProgressReport Report, PausableBackgroundWorker worker)
        {
                // internet install
                //AppUpdater.LogInfo(String.Format("Download size {0} bytes", packagesSize));
                DownloadPackages(Report, worker);
                foreach (var name in packages)
                {
                    File.Move(String.Format("{0}/{1}", tmpdir, name), 
                              String.Format("{0}/{1}/{2}/{3}", tmpdir, moduleName, targetVersion, name));
                }
        }
        #endregion

        #region Install Files
        public delegate void ProgressReport(ulong size);

        private void DownloadPackages(ProgressReport Report, PausableBackgroundWorker worker)
        {
            DownloadReport = Report;
            if (string.IsNullOrEmpty(storeBaseUri))
            {
                AppUpdater.LogError(Properties.Resources.ExMsg_UpdateServerIsEmpty);
                throw new Exception(Properties.Resources.ExMsg_UpdateServerIsEmpty);
            }

            foreach (string name in packages)
            {
                worker.WaitOne();
                if (worker.CancellationPending)
                {
                    break;
                }
                else
                {
                    for (int retryCnt = 2; retryCnt >= 0; --retryCnt)
                    {
                        downloadLock.WaitOne(); // released on download complete or failure
                        // WebClient.DownloadFile does not report progress
                        // There is already separate thread for entire update thus it is lock while a file is being downloaded
                        DownloadFileAsync(storeBaseUri, name);
                        downloadLock.WaitOne();
                        downloadLock.Release();

                        // Report how many bytes were downloaded. Actual progress is calculated by callee
                        try
                        {
                            using (ZipFile zf = new ZipFile(System.IO.Path.Combine(tmpdir, name)))
                            {
                                if (zf.TestArchive(false))
                                    break;
                                // else try to fetch the file again
                            }
                        }
                        catch (Exception ex)
                        {
                            if (retryCnt == 0)
                                throw new Exception(String.Format("Невозможно загрузить файл {0}", name), ex);
                            // else try to fetch the file again
                        }
                        AppUpdater.LogFileInfo("File validity test failed. Retry count: " + retryCnt);
                    } 
                }
            }
        }

        /// <summary>
        /// extracts files from packages, 
        /// </summary>
        /// <param name="path">points to directory with archives (uses tmpdir if null or empty)</param>
        private void ExtractFiles(string pkgspath, ProgressReport Report, PausableBackgroundWorker worker)
        {
            bool retry = false;
            // Preapare list for binary search in order to rewrite files from obsoleteFiles list only
            obsoleteFiles.Sort();
            foreach (string pkgname in packages)
            {
                long pkgSize = FileSize(Path.Combine(pkgspath, pkgname));
                
                using (ZipInputStream s = new ZipInputStream(File.OpenRead(pkgspath + "\\" + pkgname)))
                {
                    ZipEntry theEntry;
                    string prgName = Utilities.ProgramInfo.ExecutableName();
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        string directoryName = Path.GetDirectoryName(theEntry.Name);
                        string fileName = Path.GetFileName(theEntry.Name);
                        string relativeName = "";
                        if (!string.IsNullOrEmpty(directoryName))
                            relativeName = directoryName + '\\' + fileName;
                        else
                            relativeName = fileName;
                        relativeName = relativeName.Replace('\\','/');
#if DEBUG_ZIP
                        string files = "";
                        foreach (string fname in obsoleteFiles)
                        {
                            files += "[" + fname + "]\n";
                        }
                        bool isObsolete = (obsoleteFiles.BinarySearch(relativeName.ToUpper()) >= 0);
                        MessageBox.Show("Processing Entry: " + theEntry.Name
                                        + "\nRelName: " + relativeName.ToUpper()
                                        + "\nUpdate required:  " + isObsolete + "\n" + files,
                                        "Extracting");
#endif
                        // create directory now. So it will be possible to create empty folders
                        if (directoryName.Length > 0)
                        {
                            string dirname = UpdaterConfiguration.AppPath + directoryName;
                            if (!Directory.Exists(dirname))
                            {
                                Directory.CreateDirectory(UpdaterConfiguration.AppPath + directoryName);
                                
                                dirname = directoryName;
                                do
                                {
                                    UninstallList.Instance.AddItem(UpdaterConfiguration.AppPath + dirname);
                                }
                                while (!String.IsNullOrEmpty(dirname = Path.GetDirectoryName(dirname)));
                            }
                        }

                        // Rewrite files from obsoleteFiles list only
                        // Although ZIP format requires unix stype                         
                        //if (obsoleteFiles.BinarySearch(theEntry.Name.ToUpper()) < 0)
                        if (obsoleteFiles.BinarySearch(relativeName.ToUpper()) < 0)
                            continue;

                        if (fileName != String.Empty)
                        {
                            string dest= String.Empty;
                            if (moduleName == "model")
                            {
                                dest = Path.Combine(UpdaterConfiguration.ModelPath, theEntry.Name);
                                if (!Directory.Exists(UpdaterConfiguration.ModelPath))
                                    Directory.CreateDirectory(UpdaterConfiguration.ModelPath);
                                ExiststModelsUpdate = true;
                            }
                            else
                                dest = Path.Combine(UpdaterConfiguration.AppPath, theEntry.Name);
                            // handle updater executables
                            if (String.Compare(fileName, prgName, true) == 0
                                && File.Exists(dest))
                            {
#warning re-design selfupdate
                                if (String.Compare(moduleName, "Updater16", true) != 0)
                                    continue;

                                AppUpdater.selfupdate_trigger = true;
                                this.isSelfUpdateRequested = true;
                                dest += "_new.exe";
                                // Need to create aetpUpdater.exe_new.exe.config file to allow running aetpUpdater.exe_new.exe in .NET 4.0 environment. Otherwise selfupdate will be not possible on .NET 4.0
                                if (!File.Exists(dest + ".config") )
                                {
                                    string cfgSrc = Path.Combine(UpdaterConfiguration.AppPath, prgName + ".config");
                                    if (!File.Exists(cfgSrc))
                                    {
                                        string [] cfgFile =   { "<?xml version=\"1.0\"?>",
                                                                "<configuration>",
                                                                "  <startup useLegacyV2RuntimeActivationPolicy=\"true\">",
                                                                "    <supportedRuntime version=\"v4.0\" sku=\".NETFramework,Version=v4.0\"/>",
                                                                "    <supportedRuntime version=\"v2.0.50727\"/>",
                                                                "  </startup>",
                                                                "  <runtime>",
                                                                "    <NetFx40_LegacySecurityPolicy enabled=\"true\"/>",
                                                                "  </runtime>",
                                                                "</configuration>"};
                                        File.WriteAllLines(cfgSrc, cfgFile/*, System.Text.Encoding.UTF8*/); // We do not use UTF-8 in our XML file. Otherwise encoding must be specified in <xml> tag.
                                        UninstallList.Instance.AddItem(cfgSrc);
                                    }

                                    File.Copy(cfgSrc, dest + ".config");
                                    UninstallList.Instance.AddItem(dest + ".config");

                                }
                            }
                            AppUpdater.LogFileInfo("Extracting from: " + pkgspath + "\\" + pkgname + " to " + dest);
#if DEBUG_ZIP
                            MessageBox.Show("From: " + tmpdir + "\\" + pkgname +
                                            "\n\nTo: " + dest,
                                            "Extracting");
#endif
                            do
                            {
                                retry = false;
                                try
                                {
                                    //remove file for to ensure that it can be owerwritten
                                    if (File.Exists(dest)) 
                                        File.Delete(dest);

                                    using (FileStream streamWriter = File.Create(dest))
                                    {
                                        long totalSize = s.Length;
                                        int size = 0;
                                        long writtenSize = 0;
                                        byte[] data = new byte[2048];
                                        while ((size = s.Read(data, 0, data.Length)) > 0)
                                        {
                                            streamWriter.Write(data, 0, size);
                                            writtenSize += size;
                                            //Report((ulong)pkgSize * (ulong)size / (ulong)totalSize);
#if DEBUG
                                            AppUpdater.LogFileInfo(String.Format("Extracted: {0:0.00}%, {1} of {2}", 100.0 * writtenSize / totalSize, writtenSize, totalSize));
#endif
                                        }
                                    }
                                    
                                    // Common ZIP file stores Last Modification time only
                                    File.SetLastWriteTime(dest, theEntry.DateTime);
                                    // File.SetCreationTime(dest, theEntry.DateTime);
                                    // File.SetLastAccessTime(dest, theEntry.DateTime);
                                    UninstallList.Instance.AddItem(dest);
                                }
                                catch (Exception e)
                                {
                                    //remove invalid file
                                    try
                                    {
                                        if (File.Exists(dest))
                                            File.Delete(dest);
                                    }
                                    catch { } // ignore error
                                    AppUpdater.LogError(String.Format("Unexpected exception while extracting file {0} from {1}. Message: {2}", dest, pkgname, e.Message));
                                    switch (MessageBox.Show("Ошибка распаковки архива: " + pkgname + "\n" + e.Message,
                                                    Properties.Resources.InfoMsg_ProgramName,
                                                    MessageBoxButtons.AbortRetryIgnore,
                                                    MessageBoxIcon.Error))
                                    {
                                        case DialogResult.Abort: throw;
                                        case DialogResult.Retry: retry = true; break;
                                        case DialogResult.Ignore: retry = false; throw;
                                        default:
                                            // shouldn't reach this
                                            AppUpdater.LogError("Unpacking ERROR");
                                            throw new Exception("Unpacking ERROR");
                                    }
                                }
                            } while (retry == true);
                        }
                    }
                    s.Close();
                }

                // not sure, how to show progress on big multifile archives, when few entries are not extracted
                // so simply log when entire archive is processed
                Report((ulong)FileSize(Path.Combine(pkgspath, pkgname)));
            }
        }

        /************************************************************************
        *																		*
        *  Обновляем текст версии в файле (*.version.txt)                       *
        *																		*
        ************************************************************************/

        public void UpdateLocalVersionFile()
        {
            // Delay LocalVersionFile actual update until selfupdate is not completed. See AppUpdater Selfupdate section.
            string fname = moduleName + ".version.txt";
            string fileData = targetVersion.ToString();

            if (isSelfUpdateRequested)
            {
                // Add original filename to uninstall list
                UninstallList.Instance.AddItem(Path.Combine(UpdaterConfiguration.AppPath, fname));
                fileData += Environment.NewLine + fname; // store original file name
                fname = Utilities.ProgramInfo.ExecutableName() + "_new.version.txt";
            }
            
            AppUpdater.LogFileInfo(String.Format("Updating version file {0} to {1}", fname, fileData.Replace("\r\n","\\r\\n").Replace("\n", "\\n")));
            
            fname = Path.Combine(UpdaterConfiguration.AppPath, fname);
            File.WriteAllText(fname, fileData);
            UninstallList.Instance.AddItem(fname);
        }

        public void Install(ProgressReport Report, PausableBackgroundWorker worker)
        {
            if (String.IsNullOrEmpty(storeBaseUri))
                return;

            if (String.IsNullOrEmpty(useLocalStorage))
            {
                // internet install
                AppUpdater.LogInfo(String.Format("Download size {0} bytes", packagesSize));
                DownloadPackages(Report, worker);
                worker.WaitOne();
                if (!worker.CancellationPending)
                {
                    ExtractFiles(tmpdir, Report, worker);
                    UpdateLocalVersionFile();
                }
            }
            else
            {
                // local update
                worker.WaitOne();
                if (!worker.CancellationPending)
                {
                    ExtractFiles(useLocalStorage + @"\" + moduleName + @"\" + targetVersion, Report, worker);
                    UpdateLocalVersionFile();
                }
            }
        }
        #endregion

        private string moduleName;            // name of updating module
        private List<string> obsoleteFiles;   // files to update
        private List<string> packages;        // files to download
        private ulong packagesSize;           // total size of downloading packages in bytes
        private string tmpdir;                // temporary location for downloaded files
        private string storeBaseUri;          // location of module packages. Updated from module.xml by HasObsoleteFiles
        private string updateServerUri;       // Points to most recent version.txt and module.xml files for any module
        private Version targetVersion;        // module version to update to. ServerVersion is always updated from server.
        private bool disposed;                // Dispose() call guard
        private string useLocalStorage;       // points to local storage and thus enables local update
        private bool isSelfUpdateRequested;   // flag to prevent update aetpupdater.exe.verstion.txt until selfupdate is not fully completed.
        private ExtendedWebClient downloader; // http, ftp, file client
        private ProgressReport DownloadReport;// delegate to update progress bar
        private long prevReceivedBytes;       // to calc progress increment from last Download Progress Changed Event
        private Semaphore downloadLock;       // locks backgroundWorker thread while file is being downloaed. backgroundworker allows zip exctraction in background.
    }
}

