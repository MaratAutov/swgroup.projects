﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

using aetpUpdater.Utilities;

namespace aetpUpdater.Updater
{
    public partial class DlgUpdate : AetpUpdaterForm
    {
        /************************************************************************
        *																		*
        *  Конструктор. Устанавливаем заголовок окна                            *
        *																		*
        ************************************************************************/

        private readonly string Server;

        public DlgUpdate(string srvr)
        {
            InitializeComponent();

            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            Server = srvr;
            Text = "Обновление " + Properties.Resources.ApplicationName;
            lblInstallerVersion.Text = @"Версия программы обновления: " + ProgramInfo.Version();

            WinGraphics.ApplyFormStopResizing(this);
            DialogResult = DialogResult.None;
        }

        /************************************************************************
        *																		*
        *  Переключаемся по вкладкам                                            *
        *																		*
        ************************************************************************/

        private void spUpdatePages_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == tabPageProgress)
            {
                backgroundUpdater.RunWorkerAsync(Updater.AppUpdater.GetModulesNames());
            }
        }

        /************************************************************************
        *																		*
        *  Пользователь отказывается от обновления приложения                   *
        *																		*
        ************************************************************************/

        private void pbtnBeginExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        /************************************************************************
        *																		*
        *  Пользователь согласился с условиями, начинаем процесс обновления     *
        *																		*
        ************************************************************************/

        private void btnBeginNext_Click(object sender, EventArgs e)
        {
            spUpdatePages.SelectedTab = tabPageProgress;
        }

        /************************************************************************
        *																		*
        *  Прерываем процесс обновления                                         *
        *																		*
        ************************************************************************/

        private void pbtnProgressAbort_Click(object sender, EventArgs e)
        {
            backgroundUpdater.CancelAsync();
        }

        /************************************************************************
        *																		*
        *  Пользователь подтверждает успешное обновление программы              *
        *																		*
        ************************************************************************/

        private void pbtnCompleteDone_Click(object sender, EventArgs e)
        {
            // успешное выполнение обновленя
            DialogResult = DialogResult.OK;
            Close();
        }

        /************************************************************************
        *																		*
        *  Пользователь подтверждает ошибку обновления программы                *
        *																		*
        ************************************************************************/

        private void pbtnExitError_Load(object sender, EventArgs e)
        {
            // выполнение обновления завершилось неудачей
            DialogResult = DialogResult.Abort;
            Close();
        }

        #region логика потока обновления приложения

        /************************************************************************
        *																		*
        *																		*
        *																		*
        ************************************************************************/

        private ulong bytesRead;        // already downloaded
        private ulong downloadSize;     // total to download

        private void backgroundUpdater_DoWork(object sender, DoWorkEventArgs e)
        {
            List<string> modules = (List<string>)e.Argument;
            try
            {
                e.Result = true;

                // maps module name and target version (version to update to)
                Dictionary<String, Version> versions = new Dictionary<String, Version>();
                
                bytesRead = 0;
                downloadSize = 0;

                List<ModuleUpdater> updaters = new List<ModuleUpdater>();

                foreach (string modname in modules)
                {
                    try // skip failed modules
                    {
                        AppUpdater.LogInfo("Initializing update for " + modname);
                        ModuleUpdater updater = new ModuleUpdater(modname, Server, CLIParser.localupdate_mode);

                        // to repair set targetVersion as the current server version
                        if (Utilities.CLIParser.RepairModeEnabled)
                            updater.TargetVersion = updater.ServerVersion;

                        // Note that HasNewerVersion() also updates targetVersion, if newer version is available
                        if (Utilities.CLIParser.RepairModeEnabled || updater.HasNewerVersion())
                        {
                            AppUpdater.LogInfo(String.Format("Update required for {0} {1} -> {2}", modname, updater.LocalVersion, updater.TargetVersion));

                            // always add module to versions list in order to delete required files properly
                            versions.Add(updater.ModuleName, updater.TargetVersion);

                            int fcnt = 0;
                            if ((fcnt = updater.HasObsoleteFiles()) > 0)
                            {
                                AppUpdater.LogInfo(String.Format("{0} obsolete files found for {1}", fcnt, modname));
                                updaters.Add(updater);
                                downloadSize += updater.DownloadSize;
                            }
                            else
                            {
                                // newer version available but all files are up to date
                                AppUpdater.LogInfo(String.Format("{0} obsolete files found. Updating {1}.version.txt file only", fcnt, modname));
                                updater.UpdateLocalVersionFile();
                                updater.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        e.Result = false;
                        AppUpdater.LogError(String.Format("Exception raised while initializing update for {0} : {1}", modname, ex.Message));
                    }
                }
                AppUpdater.LogInfo(String.Format("Total update size for all modules: {0} bytes", downloadSize));

                AppUpdater.LogInfo("Removing obsolete files...");
                AppUpdater.ProcessPreinstallAction(versions);

                AppUpdater.LogInfo("Updating modules...");

                if (updaters.Count > 0)
                {
                    foreach (ModuleUpdater updater in updaters)
                    {
                        // реализация возможности прерывания обновления файлов
                        backgroundUpdater.WaitOne();
                        if (backgroundUpdater.CancellationPending)
                        {
                            e.Cancel = true;
                            AppUpdater.LogInfo("Installation Canceled...");
                            break;
                        }
                        else
                        {
                            try // skip failed modules
                            {
                                AppUpdater.LogInfo("Installing new files found for " + updater.ModuleName);
                                updater.Install(ProgressReporter, backgroundUpdater);
                            }
                            catch (Exception ex)
                            {
                                e.Result = false;
                                AppUpdater.LogError(String.Format("Exception raised while updating files for {0} : {1}", updater.ModuleName, ex.Message));
                                break;
                            }
                        }
                    }
                }
                else
                {
                    e.Result = true;
                    downloadSize = 0; // Reset download size if user said not to download
                }
                AppUpdater.LogInfo("Cleaning up...");

                foreach (ModuleUpdater updater in updaters)
                {
                    updater.Dispose();
                }
            }
            catch
            {
                #if DEBUG_BKUPDATER
                MessageBox.Show("Exception!\n" + ex.Message, "backgroundUpdater", MessageBoxButtons.OK, MessageBoxIcon.Error);
                #endif
                e.Result = false;
                spUpdatePages.SelectedTab = tabPageError;
            }
        }

        /************************************************************************
        *																		*
        *																		*
        *																		*
        ************************************************************************/

        private void backgroundUpdater_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage < progressBar1.Minimum)
            {
                progressBar1.Value = progressBar1.Minimum;
                AppUpdater.LogError(String.Format("Progress Percentage is out of range: {0}", e.ProgressPercentage));
            }
            else if (e.ProgressPercentage > progressBar1.Maximum)
            {
                progressBar1.Value = progressBar1.Maximum;
                AppUpdater.LogError(String.Format("Progress Percentage is out of range: {0}", e.ProgressPercentage));
            }
            else
            {
                progressBar1.Value = e.ProgressPercentage;
            }
        }

        /************************************************************************
        *																		*
        *  Завершаем работу обновления каким-либо способом          				*
        *																		*
        ************************************************************************/

        private void backgroundUpdater_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            #if DEBUG_BKUPDATER
            MessageBox.Show("Work complete!\n Downloaded " + bytesRead + " of " + downloadSize, "backgroundUpdater", MessageBoxButtons.OK, MessageBoxIcon.Information);
            #endif
            // Work complete. Now set result and Close ProgressFrom

            if (e.Error != null)
            {
                // First, handle the case where an exception was thrown.

                AppUpdater.LogError("Error during update: " + e.Error);
                spUpdatePages.SelectedTab = tabPageError;
            }
            else if (e.Cancelled)
            {
                // Next, handle the case where the user canceled 
                // the operation.
                // Note that due to a race condition in 
                // the DoWork event handler, the Cancelled
                // flag may not have been set, even though
                // CancelAsync was called.

                AppUpdater.LogInfo("Update Canceled");
                spUpdatePages.SelectedTab = tabPageError;
            }
            else if ((bool)e.Result == false)
            {
                AppUpdater.LogInfo("Update Completed. Result: false");
                spUpdatePages.SelectedTab = tabPageError;
            }
            else
            {
                // Finally, handle the case where the operation succeeded.

                AppUpdater.LogInfo("Update Completed. Result: true");
                spUpdatePages.SelectedTab = tabPageComplete;
            }
        }
        #endregion

        #region callbacks for UpdateModule

        private void ProgressReporter(ulong bytes)
        {
            bytesRead += bytes;
            // each file is reported twice: after download and after unzip
            backgroundUpdater.ReportProgress((int)(100 * bytesRead / (downloadSize << 1)));
        }
        #endregion
    }
}
