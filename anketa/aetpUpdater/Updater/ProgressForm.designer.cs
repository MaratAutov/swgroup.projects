namespace aetpUpdater.Updater
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_version = new System.Windows.Forms.Label();
            this.fbd = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.pbLoad = new System.Windows.Forms.PictureBox();
            this.backgroundUpdater = new aetpUpdater.Utilities.PausableBackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoad)).BeginInit();
            this.SuspendLayout();
            // 
            // label_version
            // 
            this.label_version.AutoSize = true;
            this.label_version.BackColor = System.Drawing.Color.Transparent;
            this.label_version.ForeColor = System.Drawing.Color.White;
            this.label_version.Location = new System.Drawing.Point(270, 76);
            this.label_version.Name = "label_version";
            this.label_version.Size = new System.Drawing.Size(94, 13);
            this.label_version.TabIndex = 14;
            this.label_version.Text = "������ not visible";
            this.label_version.Visible = false;
            // 
            // fbd
            // 
            this.fbd.ShowNewFolderButton = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Title = "�������� ���� ����������";
            // 
            // pbLoad
            // 
            this.pbLoad.BackColor = System.Drawing.Color.Transparent;
            this.pbLoad.Image = global::aetpUpdater.Properties.Resources.loader_ajax;
            this.pbLoad.Location = new System.Drawing.Point(283, 24);
            this.pbLoad.Name = "pbLoad";
            this.pbLoad.Size = new System.Drawing.Size(33, 33);
            this.pbLoad.TabIndex = 15;
            this.pbLoad.TabStop = false;
            // 
            // backgroundUpdater
            // 
            this.backgroundUpdater.WorkerReportsProgress = true;
            this.backgroundUpdater.WorkerSupportsCancellation = true;
            this.backgroundUpdater.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundUpdater_DoWork);
            // 
            // ProgressForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::aetpUpdater.Properties.Resources.loader;
            this.ClientSize = new System.Drawing.Size(331, 75);
            this.ControlBox = false;
            this.Controls.Add(this.pbLoad);
            this.Controls.Add(this.label_version);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProgressForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "�������� ������� ����������";
            this.Load += new System.EventHandler(this.ProgressForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbLoad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label_version;
        public aetpUpdater.Utilities.PausableBackgroundWorker backgroundUpdater;
        private System.Windows.Forms.FolderBrowserDialog fbd;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.PictureBox pbLoad;


    }
}