﻿using System;
using System.Collections.Generic;
using System.Configuration;

#region aetpUpdater Settings
namespace aetpUpdater
{

    public class AppConfigSettings : ApplicationSettingsBase
    {
        [ApplicationScopedSetting]
        //[global::System.Configuration.DefaultSettingValueAttribute("FilePath/")]
        public string AppCfgServeUpdaterUrl
        {
            get
            {
                return (string)this["AppCfgServeUpdaterUrl"];
            }
        }
        [ApplicationScopedSetting]
        public string IsLocalVersion
        {
            get
            {
                if (this["IsLocalVersion"] != null)
                    return (string)this["IsLocalVersion"];
                else
                    return "";
            }
        }
    }
    /// <summary>
    /// Accessor for application settings
    /// </summary>
    [global::System.Configuration.SettingsProviderAttribute(typeof(global::aetpUpdater.ProxySettingsProvider))]
    public class Settings : ApplicationSettingsBase
    {
        #region ServerUrl
        /// <summary>
        /// Proxy url
        /// </summary>
        [UserScopedSetting]
        public string ServerUrl
        {
            get
            {
                if (this["ServerUrl"] == null)
                    this["ServerUrl"] = ConfigurationManager.AppSettings["ServerUrl"];
                if (this["ServerUrl"] == null)
                    this["ServerUrl"] = "http://new.seldon2010.ru:80";
                //if (this["ServerUrl"].ToString().ToLower().Contains("//aetp.ru"))
                //{
                //    this["ServerUrl"] = "http://seldon.aetp.ru/AdminQuery.rem";
                //    Save();
                //}
                return (string)this["ServerUrl"];
            }
            set
            {
                this["ServerUrl"] = value;
            }
        }
        #endregion

        #region LocalUpdatePath

        /// <summary>
        /// Proxy url
        /// </summary>
        [UserScopedSetting]
        public string LocalUpdatePath
        {
            get
            {
                if (this["LocalUpdatePath"] == null)
                    this["LocalUpdatePath"] = System.Windows.Forms.Application.StartupPath + @"\updates\";
                return (string)this["LocalUpdatePath"];
            }
            set
            {
                this["LocalUpdatePath"] = value;
            }
        }
        #endregion

        #region ServerUpdaterUrl

        /// <summary>
        /// Proxy url
        /// </summary>
        [UserScopedSetting]
        public string ServerUpdaterUrl
        {
            get
            {
                if (this["ServerUpdaterUrl"] == null 
                    && !String.IsNullOrEmpty(UpdaterConfiguration.appConfig.AppCfgServeUpdaterUrl))
                    return UpdaterConfiguration.appConfig.AppCfgServeUpdaterUrl;
                
                if (this["ServerUpdaterUrl"] == null)
                    this["ServerUpdaterUrl"] = aetpUpdater.Properties.Resources.Builtin_ServerUrl;

                return (string)this["ServerUpdaterUrl"];
            }
            set
            {
                if (String.IsNullOrEmpty(UpdaterConfiguration.appConfig.AppCfgServeUpdaterUrl))
                    this["ServerUpdaterUrl"] = value;
            }
        }
        #endregion

        #region ServerList
        /// <summary>
        /// ServerList
        /// </summary>
        [UserScopedSetting]
        public string ServerList
        {
            get
            {
                return (string)this["ServerList"];
            }
            set
            {
                this["ServerList"] = value;
            }
        }
        #endregion

        #region UseProxy
        /// <summary>
        /// UseProxy
        /// </summary>
        [UserScopedSetting]
        public bool UseProxy
        {
            get
            {
                try
                {
                    if (this["UseProxy"] == null)
                        return false;

                    return (bool)this["UseProxy"];
                }
                catch
                {
                    this["UseProxy"] = false;
                    return (bool)this["UseProxy"];
                }
            }
            set
            {
                this["UseProxy"] = value;
            }
        }
        #endregion

        #region ProxyUrl
        /// <summary>
        /// Proxy url
        /// </summary>
        [UserScopedSetting]
        public string ProxyUrl
        {
            get
            {
                return (string)this["proxyUrl"];
            }
            set
            {
                this["proxyUrl"] = value;
            }
        }
        #endregion

        #region ProxyPort
        /// <summary>
        /// Proxy port
        /// </summary>
        [UserScopedSetting]
        public string ProxyPort
        {
            get
            {
                return (string)this["proxyPort"];
            }
            set
            {
                this["proxyPort"] = value;
            }
        }
        #endregion

        #region Proxy_Expect100Continue
        /// <summary>
        /// UseProxy
        /// </summary>
        [UserScopedSetting]
        public bool Proxy_Expect100Continue
        {
            get
            {
                try
                {
                    if (this["Proxy_Expect100Continue"] == null)
                        return false;

                    return (bool)this["Proxy_Expect100Continue"];
                }
                catch
                {
                    this["Proxy_Expect100Continue"] = false;
                    return (bool)this["Proxy_Expect100Continue"];
                }
            }
            set
            {
                this["Proxy_Expect100Continue"] = value;
            }
        }
        #endregion

        #region UseProxyAuth
        /// <summary>
        /// UseProxyAuth
        /// </summary>
        [UserScopedSetting]
        public bool UseProxyAuth
        {
            get
            {
                try
                {
                    if (this["UseProxyAuth"] == null)
                        return false;

                    return (bool)this["UseProxyAuth"];
                }
                catch
                {
                    this["UseProxyAuth"] = false;
                    return (bool)this["UseProxyAuth"];
                }
            }
            set
            {
                this["UseProxyAuth"] = value;
            }
        }
        #endregion

        #region ProxyUser
        /// <summary>
        /// ProxyUser
        /// </summary>
        [UserScopedSetting]
        public string ProxyUser
        {
            get
            {
                return (string)this["ProxyUser"];
            }
            set
            {
                this["ProxyUser"] = value;
            }
        }
        #endregion

        #region ProxyPassword
        /// <summary>
        /// ProxyPassword
        /// </summary>
        [UserScopedSetting]
        public string ProxyPassword
        {
            get
            {
                return (string)this["ProxyPassword"];
            }
            set
            {
                this["ProxyPassword"] = value;
            }
        }
        #endregion

        #region ProxyAuthType
        /// <summary>
        /// ProxyAuthType
        /// </summary>
        [UserScopedSetting]
        public string ProxyAuthType
        {
            get
            {
                if (this["ProxyAuthType"] == null)
                    return "Basic";
                //else
                return (string)this["ProxyAuthType"];
            }
            set
            {
                this["ProxyAuthType"] = value;
            }
        }
        #endregion
    }
}
#endregion
