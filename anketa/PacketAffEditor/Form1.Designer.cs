﻿namespace PacketAffEditor
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.bePath = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.teVersion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.beXsdPath = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.ceGeneratingXsd = new DevExpress.XtraEditors.CheckEdit();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.ceNormalizeGrid = new DevExpress.XtraEditors.CheckEdit();
            this.ceNormalizeFormula = new DevExpress.XtraEditors.CheckEdit();
            this.ceAddRegion = new DevExpress.XtraEditors.CheckEdit();
            this.ceNormalizeComboBox = new DevExpress.XtraEditors.CheckEdit();
            this.ceRefactorSerTag = new DevExpress.XtraEditors.CheckEdit();
            this.ceNormalizeAlign = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.bePath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teVersion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beXsdPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGeneratingXsd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNormalizeGrid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNormalizeFormula.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceAddRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNormalizeComboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceRefactorSerTag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNormalizeAlign.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // bePath
            // 
            this.bePath.EditValue = "d:\\models";
            this.bePath.Location = new System.Drawing.Point(158, 12);
            this.bePath.Name = "bePath";
            this.bePath.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.bePath.Size = new System.Drawing.Size(497, 20);
            this.bePath.TabIndex = 0;
            this.bePath.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.bePath_ButtonClick);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(42, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Каталог";
            // 
            // teVersion
            // 
            this.teVersion.Location = new System.Drawing.Point(158, 55);
            this.teVersion.Name = "teVersion";
            this.teVersion.Size = new System.Drawing.Size(100, 20);
            this.teVersion.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Версия";
            // 
            // beXsdPath
            // 
            this.beXsdPath.EditValue = "d:\\models\\xsd";
            this.beXsdPath.Enabled = false;
            this.beXsdPath.Location = new System.Drawing.Point(158, 245);
            this.beXsdPath.Name = "beXsdPath";
            this.beXsdPath.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.beXsdPath.Size = new System.Drawing.Size(497, 20);
            this.beXsdPath.TabIndex = 0;
            this.beXsdPath.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.beXsdPath_ButtonClick);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 248);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(84, 13);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "Каталог для xsd";
            // 
            // ceGeneratingXsd
            // 
            this.ceGeneratingXsd.Location = new System.Drawing.Point(158, 110);
            this.ceGeneratingXsd.Name = "ceGeneratingXsd";
            this.ceGeneratingXsd.Properties.Caption = "Генерировать xsd";
            this.ceGeneratingXsd.Size = new System.Drawing.Size(124, 19);
            this.ceGeneratingXsd.TabIndex = 4;
            this.ceGeneratingXsd.CheckedChanged += new System.EventHandler(this.ceGeneratingXsd_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(158, 279);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(239, 279);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ceNormalizeGrid
            // 
            this.ceNormalizeGrid.Location = new System.Drawing.Point(158, 85);
            this.ceNormalizeGrid.Name = "ceNormalizeGrid";
            this.ceNormalizeGrid.Properties.Caption = "Нормализировать теги в таблицах";
            this.ceNormalizeGrid.Size = new System.Drawing.Size(223, 19);
            this.ceNormalizeGrid.TabIndex = 4;
            this.ceNormalizeGrid.CheckedChanged += new System.EventHandler(this.ceGeneratingXsd_CheckedChanged);
            // 
            // ceNormalizeFormula
            // 
            this.ceNormalizeFormula.Location = new System.Drawing.Point(158, 135);
            this.ceNormalizeFormula.Name = "ceNormalizeFormula";
            this.ceNormalizeFormula.Properties.Caption = "Нормализовать формулы";
            this.ceNormalizeFormula.Size = new System.Drawing.Size(205, 19);
            this.ceNormalizeFormula.TabIndex = 4;
            this.ceNormalizeFormula.CheckedChanged += new System.EventHandler(this.ceGeneratingXsd_CheckedChanged);
            // 
            // ceAddRegion
            // 
            this.ceAddRegion.Location = new System.Drawing.Point(158, 160);
            this.ceAddRegion.Name = "ceAddRegion";
            this.ceAddRegion.Properties.Caption = "Добавить регионы";
            this.ceAddRegion.Size = new System.Drawing.Size(205, 19);
            this.ceAddRegion.TabIndex = 6;
            // 
            // ceNormalizeComboBox
            // 
            this.ceNormalizeComboBox.Location = new System.Drawing.Point(158, 185);
            this.ceNormalizeComboBox.Name = "ceNormalizeComboBox";
            this.ceNormalizeComboBox.Properties.Caption = "Нормализировать списки";
            this.ceNormalizeComboBox.Size = new System.Drawing.Size(205, 19);
            this.ceNormalizeComboBox.TabIndex = 7;
            // 
            // ceRefactorSerTag
            // 
            this.ceRefactorSerTag.Location = new System.Drawing.Point(158, 210);
            this.ceRefactorSerTag.Name = "ceRefactorSerTag";
            this.ceRefactorSerTag.Properties.Caption = "Исправить сопроводительное письмо";
            this.ceRefactorSerTag.Size = new System.Drawing.Size(205, 19);
            this.ceRefactorSerTag.TabIndex = 8;
            // 
            // ceNormalizeAlign
            // 
            this.ceNormalizeAlign.Location = new System.Drawing.Point(419, 85);
            this.ceNormalizeAlign.Name = "ceNormalizeAlign";
            this.ceNormalizeAlign.Properties.Caption = "Нормализовать выравнивание";
            this.ceNormalizeAlign.Size = new System.Drawing.Size(205, 19);
            this.ceNormalizeAlign.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 315);
            this.Controls.Add(this.ceNormalizeAlign);
            this.Controls.Add(this.ceRefactorSerTag);
            this.Controls.Add(this.ceNormalizeComboBox);
            this.Controls.Add(this.ceAddRegion);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.ceNormalizeGrid);
            this.Controls.Add(this.ceNormalizeFormula);
            this.Controls.Add(this.ceGeneratingXsd);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.teVersion);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.beXsdPath);
            this.Controls.Add(this.bePath);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bePath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teVersion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beXsdPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGeneratingXsd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNormalizeGrid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNormalizeFormula.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceAddRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNormalizeComboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceRefactorSerTag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNormalizeAlign.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ButtonEdit bePath;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit teVersion;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ButtonEdit beXsdPath;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.CheckEdit ceGeneratingXsd;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.CheckEdit ceNormalizeGrid;
        private DevExpress.XtraEditors.CheckEdit ceNormalizeFormula;
        private DevExpress.XtraEditors.CheckEdit ceAddRegion;
        private DevExpress.XtraEditors.CheckEdit ceNormalizeComboBox;
        private DevExpress.XtraEditors.CheckEdit ceRefactorSerTag;
        private DevExpress.XtraEditors.CheckEdit ceNormalizeAlign;
    }
}

