﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using aetp.Common.Serializer;
using aetp.Common.ControlModel;
using aetp.Common;
using System.Xml;
using DevExpress.Utils;

namespace PacketAffEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void bePath_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                bePath.Text = fbd.SelectedPath;
            }
        }

        private void ceGeneratingXsd_CheckedChanged(object sender, EventArgs e)
        {
            beXsdPath.Enabled = ceGeneratingXsd.Checked;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (File.Exists(logfile))
                File.Delete(logfile);
            var f = File.Create(logfile);
            f.Close();

            if (String.IsNullOrEmpty(teVersion.Text) && !ceGeneratingXsd.Checked && !ceNormalizeGrid.Checked && !ceNormalizeFormula.Checked &&
               !ceAddRegion.Checked && !ceNormalizeComboBox.Checked && !ceRefactorSerTag.Checked && !ceNormalizeAlign.Checked)
                return;
            string version = teVersion.Text;
            FormSerializer ser = new FormSerializer();
            string[] files = Directory.GetFiles(bePath.Text, "*.aff", SearchOption.AllDirectories);
            int count = 0;
            
            foreach (var file in files)
            {
                Report report = ser.Deserialize(File.ReadAllText(file));
                bool saving = false;
                if (!String.IsNullOrEmpty(version))
                {
                    report.SerialaizerConfig.appVersion = version;
                    saving = true;
                }
                if (ceNormalizeGrid.Checked)
                {
                    NormalizeGrids(report);
                    saving = true;
                }
                if (ceNormalizeFormula.Checked)
                {
                    NormalizeFormula(report);
                    saving = true;
                }
                if (ceNormalizeAlign.Checked)
                {
                    NormalizeAlign(report);
                    saving = true;
                }
                if (ceAddRegion.Checked)
                {
                    if (AddRegions(report))
                    {
                        saving = true;
                    }
                }
                if (ceNormalizeComboBox.Checked)
                {
                    if (NormalizeComboBox(report))
                    {
                        saving = true;
                    }
                }
                if (ceRefactorSerTag.Checked)
                {
                    if (RefactorSerTag(report))
                    {
                        saving = true;
                        count++;
                    }
                }

                if (saving) File.WriteAllText(file, ser.Serialize(report));

                if (ceGeneratingXsd.Checked)
                {
                    string xsd_name = String.Format("{0}_{1}.xsd", report.SerialaizerConfig.Model, report.SerializingTag);
                    string xsd_path = Path.Combine(beXsdPath.Text, report.SerialaizerConfig.appVersion, report.SerialaizerConfig.Model.ToString());
                    if (!Directory.Exists(xsd_path))
                    {
                        Directory.CreateDirectory(xsd_path);
                    }
                    xsd_path = Path.Combine(xsd_path, xsd_name);
                    var xsdgen = new XSDGenerator(report);
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Indent = true;
                    settings.OmitXmlDeclaration = false;
                    Stream xsdstream = File.Create(xsd_path);
                    XmlWriter xmlTextWriter = XmlWriter.Create(xsdstream, settings);
                    xsdgen.Generate(xmlTextWriter);
                    xmlTextWriter.Flush();
                    xmlTextWriter.Close();
                    xsdstream.Close();
                }
            }
            MessageBox.Show(count.ToString());
        }

        bool RefactorSerTag(Report report)
        {
            BaseControl letter = report.Children.Where(x => x.Caption.ToLower() == "сопроводительное письмо").FirstOrDefault();
            if (letter != null)
            {
                foreach (var ctrl in letter.Children)
                {
                    char c = ctrl.SerializingTag.Last();
                    int num;
                    if (int.TryParse(c.ToString(), out num))
                    {
                        Console.WriteLine(ctrl.SerializingTag);
                        ctrl.AlternativeSerializingTag = ctrl.SerializingTag;
                        char[] massivToTrim = {'1', '2', '3','4','5','6','7','8','9','0'};
                        ctrl.SerializingTag = ctrl.SerializingTag.TrimEnd(massivToTrim);
                        return true;
                    }
                }
            }
            return false;
        }

        bool NormalizeComboBox(Report report)
        {
            //var dict = report.MetaData.Dictionarys.Where(x => x.Name == "Voc002Код").SingleOrDefault();
            //if (dict == null) return false;
            
            bool is_normalized = false;
            ReportUtils.DoAction(report.Children, (bc) =>
            {
                if (bc is aetp.Common.ControlModel.ComboBox)
                {
                    var combo = bc as aetp.Common.ControlModel.ComboBox;
                    var dict1 = report.MetaData.Dictionarys.Where(x => x.Name == combo.Source.Name).SingleOrDefault();
                    if (dict1 != null && dict1 != combo.Source)
                    {
                        combo.Source = dict1;
                        is_normalized = true;
                    }
                }
                else if (bc is GridCellItem && (bc as GridCellItem).Type == GridCellType.List)
                {
                    var cell = bc as GridCellItem;
                    var dict1 = report.MetaData.Dictionarys.Where(x => x.Name == cell.Source.Name).SingleOrDefault();
                    if (dict1 != null && dict1 != cell.Source)
                    {
                        cell.Source = dict1;
                        is_normalized = true;
                    }
                }
                else if (bc is GridAddedRowColumn && (bc as GridAddedRowColumn).Type == GridCellType.List)
                {
                    var column = bc as GridAddedRowColumn;
                    var dict1 = report.MetaData.Dictionarys.Where(x => x.Name == column.Source.Name).SingleOrDefault();
                    if (dict1 != null && dict1 != column.Source)
                    {
                        column.Source = dict1;
                        is_normalized = true;
                    }
                }
            });
            return is_normalized;
        }


        public static string logfile = "d:\\xsd.log";

        bool AddRegions(Report report)
        {
            var dict = report.MetaData.Dictionarys.Where(x => x.Name == "Voc002Код").SingleOrDefault();
            if (dict != null)
            {
                dict.Add(new DictionaryListItem() { Active = true, Code = "82", Description = "Республика Крым" });
                dict.Add(new DictionaryListItem() { Active = true, Code = "92", Description = "Севастополь" });
                return true;
            }
            else
            {
                return false;
            }
        }

        void NormalizeGrids(Report report)
        {
            ReportUtils.DoAction(report.Children, (x) =>
            {
                if (x is Grid)
                {
                    var grid = x as Grid;
                    string tabletag = grid.SerializingTag;
                    int rowdelta = 0;
                    if (grid.metagrid != null)
                    {
                        tabletag = grid.metagrid.SerializingTag;
                        foreach (var table in grid.metagrid.Tables)
                        {
                            if (grid == table) break;
                            if (table is Grid)
                                rowdelta += (table as Grid).Rows.Count;
                            else if (table is GridAddedPage || table is GridAddedRow)
                                rowdelta++;
                        }
                    }

                    for (int j = 0; j < grid.Rows.Count; j++)
                    {
                        string rowtag = String.Format("{0}_{1}", tabletag, rowdelta + j + 1);
                        grid.Rows[j].SerializingTag = rowtag;
                        for (int i = 0; i < grid.Rows[j].Cells.Count; i++)
                        {
                            string tag = String.Format("Col{0:D2}", i + 1);
                            grid.Rows[j].Cells[i].SerializingTag = tag;
                        }
                    }
                }
                if (x is GridAddedRow)
                {
                    var grid = x as GridAddedRow;
                    string tabletag = grid.SerializingTag;
                    int rowdelta = 0;
                    
                    if (grid.metagrid != null)
                    {
                        tabletag = grid.metagrid.SerializingTag;
                        foreach (var table in grid.metagrid.Tables)
                        {
                            if (grid == table) break;
                            if (table is Grid)
                                rowdelta += (table as Grid).Rows.Count;
                            else if (table is GridAddedPage || table is GridAddedRow)
                                rowdelta++;
                        }
                    }

                    string rowtag = String.Format("{0}_{1}", tabletag, rowdelta + 1);
                    grid.SerializingRowTag = rowtag;
                    for (int i = 0; i < grid.Columns.Count; i++)
                    {
                        string tag = String.Format("Col{0:D2}", i + 1);
                        grid.Columns[i].SerializingTag = tag;
                    }
                }
            });
        }

        void NormalizeFormula(Report report)
        {
            ReportUtils.DoAction(report.Children, (x) =>
            {
                if (x is GridCellItem)
                {
                    var cell = x as GridCellItem;
                    if (cell.CellValidating != null && (cell.CellValidating.FormulaForValidating ?? "").Contains("Sum"))
                    {
                        Console.WriteLine(cell.CellValidating.FormulaForValidating);
                        cell.CellValidating.FormulaForValidating = cell.CellValidating.FormulaForValidating.Replace("Sum", "sum");
                        Console.WriteLine(cell.CellValidating.FormulaForValidating);
                    }
                }
            });
        }

        void NormalizeAlign(Report report)
        {
            ReportUtils.DoAction(report.Children, (x) =>
            {
                if (x is GridCellItem)
                {
                    var cell = x as GridCellItem;
                    if (cell.CellAlign != HorzAlignment.Far && cell.Type != GridCellType.Label)
                    {
                        Console.WriteLine(cell.Name);
                        cell.CellAlign = HorzAlignment.Far;
                    }
                }
            });
        }

        private void beXsdPath_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                beXsdPath.Text = fbd.SelectedPath;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        
    }
}
