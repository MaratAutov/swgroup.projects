﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Pkcs;
using System.ComponentModel;
using System.Threading;

namespace aetp.ElectroSignature
{
    /// <summary>
    /// Реализация подписи для КриптоПро
    /// </summary>
    public class CryptoProElectroSignatureModule : IElectroSignatureModule
    {
        
        SignProgress splash;
        
        /// <summary>
        /// Подписать блоб дефолтной подписью для данного СКЗИ
        /// </summary>
        /// <param name="body">тело, блоб</param>
        /// <returns>cms sign, blob</returns>
        public byte[] Sign(byte[] body)
        {
            // Получаем сертификат ключа подписи;
            // он будет использоваться для получения 
            // секретного ключа подписи.
            X509Certificate2 signerCert = GetSignerCert();
            if (signerCert == null)
                return null;
            // Создаем объект ContentInfo по сообщению.
            // Это необходимо для создания объекта SignedCms.
            var contentInfo = new ContentInfo(body);

            // Создаем объект SignedCms по только что созданному
            // объекту ContentInfo.
            // SubjectIdentifierType установлен по умолчанию в 
            // IssuerAndSerialNumber.
            // Свойство Detached устанавливаем явно в true, таким 
            // образом сообщение будет отделено от подписи.
            
            var signedCms = new SignedCms(contentInfo, true);

            // Определяем подписывающего, объектом CmsSigner.
            var cmsSigner = new CmsSigner(signerCert);
            
            //cmsSigner.IncludeOption = X509IncludeOption. X509IncludeOption.None;
            // Подписываем CMS/PKCS #7 сообение.
            
            Thread background = new Thread(new ThreadStart(AnimateComputingSignature));
            try
            {

                background.Start();
                signedCms.ComputeSignature(cmsSigner, false);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString());
                background.Abort();
                return null;
            }
            finally
            {
                SafeClose();
            }
                        
            // Кодируем CMS/PKCS #7 подпись сообщения.
            return signedCms.Encode();
        }

        public byte[] AddSign(byte[] src, byte[] msg)
        {
            // Получаем сертификат ключа подписи;
            // он будет использоваться для получения 
            // секретного ключа подписи.
            X509Certificate2 signerCert = GetSignerCert();
            if (signerCert == null)
                return null;
            // Создаем объект SignedCms 
            ContentInfo contentInfo = new ContentInfo(src);

            SignedCms signedCms = new SignedCms(contentInfo, true);

            // Декодируем
            signedCms.Decode(msg);

            // В этом месте чаще всего стоит проверка 
            // предыдущих подписей, для простоты опускаем.
            
            // Определяем подписывающего, объектом CmsSigner.
            CmsSigner cmsSigner = new CmsSigner(signerCert);
            // Подписываем CMS/PKCS #7 сообение.
            Thread background = new Thread(new ThreadStart(AnimateComputingSignature));
            try
            {
                background.Start();
                signedCms.ComputeSignature(cmsSigner, false);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString());
                background.Abort();
                return null;
            }
            finally
            {
                SafeClose();
            }

            // Кодируем CMS/PKCS #7 сообщение.
            return signedCms.Encode();
            
        }

        void SafeClose()
        {
            if (splash != null)
            {
                if (splash.InvokeRequired) splash.Invoke(new Action(() => splash.Close()));
                else splash.Close();
            }
        }

        void AnimateComputingSignature()
        {
            splash = new SignProgress();
            splash.ShowDialog();
        }

        X509Certificate2 GetSignerCert()
        {
            // Формуруем коллекцию отображаемых сертификатов.
            X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            X509Certificate2Collection collection =
                (X509Certificate2Collection)store.Certificates;

            // Отображаем окно выбора сертификата.
            X509Certificate2Collection scollection =
                X509Certificate2UI.SelectFromCollection(collection,
                "Выбор секретного ключа по сертификату",
                "Выберите сертификат соответствующий Вашему секретному ключу.",
                X509SelectionFlag.SingleSelection);

            // Проверяем, что выбран сертификат
            if (scollection.Count == 0)
            {
                return null;
            }

            // Выбран может быть только один сертификат.
            return scollection[0];
        }
    }
}
