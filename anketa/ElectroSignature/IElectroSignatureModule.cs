﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aetp.ElectroSignature
{
    /// <summary>
    /// Интерфейс, реализуемый любым работы с ЭЦП
    /// </summary>
    public interface IElectroSignatureModule
    {
        /// <summary>
        /// Подписать блоб дефолтной подписью для данного СКЗИ
        /// </summary>
        /// <param name="body">тело, блоб</param>
        /// <returns>cms sign, blob</returns>
        byte[] Sign(byte[] body);
        byte[] AddSign(byte[] src, byte[] body);
    }
}
