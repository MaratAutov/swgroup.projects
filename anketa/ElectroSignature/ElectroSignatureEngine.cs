﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Windows.Forms;

namespace aetp.ElectroSignature
{
    public class ElectroSignatureEngine
    {
        //сделать в будущем анимацию в центре родительского окна
        public static Form ParentForm { get; set; } 
        private IElectroSignatureModule _defaultSignatureModule = new CryptoProElectroSignatureModule();
        
        /// <summary>
        /// Подписывает дефолтным алгоритмом
        /// </summary>
        /// <param name="?">Файл, который необходимо подписать</param>
        /// <returns>Файл <see ref=SignedPackage></see> с файлом и эцп</returns>
        public byte[] Sign(string name, byte[] body)
        {
            // 0. упаковать!
            using (var m = new MemoryStream())
            {
                using (var s = new ZipOutputStream(m))
                {
                    var bodyEntry = new ZipEntry(name);
                    s.PutNextEntry(bodyEntry);
                    s.Write(body, 0, body.Length);
                }
                body = m.GetBuffer();
                name = Path.ChangeExtension(name, "zip");
            }


            // 1. подписать
            byte[] sign = _defaultSignatureModule.Sign(body);
            if (sign != null)
            {
                // 2. упаковать
                using (var m = new MemoryStream())
                {
                    using (var s = new ZipOutputStream(m))
                    {
                        var bodyEntry = new ZipEntry(name);
                        s.PutNextEntry(bodyEntry);
                        s.Write(body, 0, body.Length);
                        var signEntry = new ZipEntry(name + ".sig");
                        s.PutNextEntry(signEntry);
                        s.Write(sign, 0, sign.Length);
                    }
                    return m.GetBuffer();
                }
            }
            else
                return null;
        }

        public byte[] AddSign(byte[] zipBytes)
        {
            byte[] newSignatureBody = null;
            byte[] signatureBody = null;
            byte[] body = null;
            ZipEntry cms = null;
            ZipEntry bodyEntry = null;
            List<ZipEntry> entrys = new List<ZipEntry>();
            List<byte[]> signatures = new List<byte[]>();
            using (var s = new MemoryStream(zipBytes))
            using (ZipFile zip = new ZipFile(s))
            {
                // ищем подписи
                foreach (ZipEntry e in zip)
                {
                    if (Path.GetExtension(e.Name).ToLower() == ".sig") {
                        byte[] buffer = Decompress(zip, e);
                        signatures.Add(buffer);
                        entrys.Add(e);
                    }
                }
                /*
                if (entrys.Count > 1)
                    throw new InvalidOperationException("Найдено больше одного файла подписи .sig");
                */ 
                if (entrys.Count == 0)
                    throw new InvalidOperationException("Файл подписи .sig не найден");
                cms = entrys[0];
                bodyEntry = zip.GetEntry(Path.GetFileNameWithoutExtension(cms.Name));
                if (bodyEntry == null)
                    throw new InvalidOperationException("Файл отчета не найден");
                signatureBody = Decompress(zip, cms);
                body = Decompress(zip, bodyEntry);
                newSignatureBody = _defaultSignatureModule.AddSign(body, signatureBody);
            }
            //System.Windows.Forms.MessageBox.Show(newSignatureBody == null ? "null" : newSignatureBody.Length.ToString());
            if (newSignatureBody != null)
            {
                String xname = Path.GetFileNameWithoutExtension(cms.Name) + entrys.Count + Path.GetExtension(cms.Name);
                //System.Windows.Forms.MessageBox.Show(xname);
                //  упаковать
                using (var m = new MemoryStream())
                {
                    using (var s = new ZipOutputStream(m))
                    {
                        var newBodyEntry = new ZipEntry(bodyEntry.Name);
                        s.PutNextEntry(newBodyEntry);
                        s.Write(body, 0, body.Length);
                        for (int i = 0; i < entrys.Count; i++)
                        {
                            ZipEntry e = entrys[i];
                            var newEntry = new ZipEntry(e.Name);
                            s.PutNextEntry(newEntry);
                            byte[] buffer = signatures[i];
                            s.Write(buffer, 0, buffer.Length);
                        }
                        var signEntry = new ZipEntry(xname);
                        s.PutNextEntry(signEntry);
                        s.Write(newSignatureBody, 0, newSignatureBody.Length);
                    }
                    return m.GetBuffer();
                }
            }
            else
                return null;
        }

        private static byte[] Decompress(ZipFile file, ZipEntry entry)
        {
            using (Stream s = file.GetInputStream(entry))
            using (BinaryReader br = new BinaryReader(s))
                return br.ReadBytes((int)entry.Size);
        }
    }
}
