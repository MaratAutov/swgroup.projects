﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using aetp.Common.View.Dialog;

namespace aetp.Common
{
    public static class DialogHelper
    {
        public static void ShowProxyConfigDialog()
        {
            if (MessageBox.Show("Произошла ошибка при подключении к серверу обновлений. Желаете ли вы настроить сетевое подключение?",
                "Ошибка подключения", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ProxyServerDialog dlg = new ProxyServerDialog();
                dlg.ShowDialog();
            }
        }
    }
}
