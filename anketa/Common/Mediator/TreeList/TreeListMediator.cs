﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.Runtime.InteropServices;
using DevExpress.XtraTreeList.Nodes;
using aetp.Common.View;
using aetp.Common.Control;
using aetp.Common.Validator;
using System.Drawing;

namespace aetp.Common.Mediator
{
    public class TreeListMediator
    {
        public List<BaseControl> list_control = new List<BaseControl>();
        public List<Page> list_page = new List<Page>();
        TreeListNode rootNode = null;
        TreeListNode reportNode = null;
        BaseMediator formMediator { get; set; }
        
        private TreeListMediator() { }

        public TreeListMediator(BaseMediator formMediator) 
        {
            formMediator.View.tlControls.GetStateImage += new DevExpress.XtraTreeList.GetStateImageEventHandler(tlControls_GetStateImage);
            formMediator.View.tlControls.NodeCellStyle += new DevExpress.XtraTreeList.GetCustomNodeCellStyleEventHandler(tlControls_NodeCellStyle);
            FontFree = formMediator.View.tlControls.Appearance.Row.Font;
            if (formMediator.View is IEditorWindow)
            {
                IEditorWindow editorView = formMediator.View as IEditorWindow;
                editorView.PageVisibleChanged += new PageViewInfoCollectionChangeEventHandler(PagePanels_Changed);
                editorView.TreeListChanged += new EventHandler(editorView_EditValueChanged);
                
            }
            this.formMediator = formMediator;
        }

        public void editorView_EditValueChanged(object sender,EventArgs e)
        {
            tlControls_Style();
        }

        private void tlControls_Style()
        {
            bool IsDesigner = formMediator.Report.RunMode == ViewType.Constructor;
            if (!IsDesigner)
            {
                Page page = formMediator.View.tlControls.FocusedNode.Tag as Page;
                        
                //GetIsMandatoryElement(page);
                //if (page.IsMandatoryElementExist)
                //if (ValidationCollection.ExistsError(formMediator.Report.guid, page))
                if (ExistErrors(page, page.Children))
                {
                    formMediator.View.tlControls.Appearance.SelectedRow.Font = new Font(FontFree, FontStyle.Bold);
                }
                else
                {
                    formMediator.View.tlControls.Appearance.SelectedRow.Font = new Font(FontFree, FontFree.Style);                               
                }
            }
        }

        private Font _fontTree;
        public Font FontFree
        {
            get
            {
                return _fontTree;
            }
            set
            {
                _fontTree = value;
            }        
        }

        private void tlControls_NodeCellStyle(object sender, DevExpress.XtraTreeList.GetCustomNodeCellStyleEventArgs e)
        {
            bool IsDesigner = formMediator.Report.RunMode == ViewType.Constructor;

            if (!IsDesigner)
            {
                TreeListNode node = e.Node;

                Page page = node.Tag as Page;
                
                //GetIsMandatoryElement(page);
                //if (page.IsMandatoryElementExist)
                //if (ValidationCollection.ExistsError(formMediator.Report.guid, page))
                if (ExistErrors(page, page.Children))
                {
                    e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
                }
                else
                {
                    e.Appearance.Font = new Font(FontFree, FontFree.Style);
                }

            }
        }

        //при создании разделов строим дерево или трем
        void PagePanels_Changed(object sender, PageViewInfoCollectionChangeEventArgs e)
        {
            if (e.type == ChangeType.Add)
            {
                createTreeList(e.page.page.tlItem, e.page.page.Children, false, true);
                PageVisibleChange();
                tlControls_Style();
            }
            else
            {
                removeTreeList(e.page.page);
                // снимает сохраненные валидации на странице
                ValidationCollection.RemovePage(e.page.page);
                tlControls_Style();
            }
        }
                
        void tlControls_GetStateImage(object sender, DevExpress.XtraTreeList.GetStateImageEventArgs e)
        {
            if (e.Node.Tag == null)
                return;
            BaseControl control = (BaseControl)e.Node.Tag;
            if (control is Page)
                e.NodeImageIndex = 0;
            else if (control is SimpleEdit)
            {
                if ((control as SimpleEdit).DataType == SimpleDataType.DateTime)
                e.NodeImageIndex = 9;
                if ((control as SimpleEdit).DataType == SimpleDataType.Bool)
                    e.NodeImageIndex = 13;
                if ((control as SimpleEdit).DataType == SimpleDataType.Double)
                    e.NodeImageIndex = 10;
                if ((control as SimpleEdit).DataType == SimpleDataType.Integer)
                    e.NodeImageIndex = 12;

                if ((control as SimpleEdit).DataType == SimpleDataType.String)
                    e.NodeImageIndex = 11;

            }
            else if (control is Grid)
                e.NodeImageIndex = 5;
            else if (control is GridAddedRow)
                e.NodeImageIndex = 6;
            else if (control is GridAddedPage || control is MetaGrid)
                e.NodeImageIndex = 7;
            else if (control is ComboBox)
                e.NodeImageIndex = 2;
            else if (control is Link)
                e.NodeImageIndex = 3;
            else if (control is DictionaryList)
                e.NodeImageIndex = 4;
            else if (control is MetaConrol)
                e.NodeImageIndex = 8;
            else if (control is SimpleRichEdit)
                e.NodeImageIndex = 11;
            else if (control is SimpleLabel)
                e.NodeImageIndex = 14;
        }
        
        void MetaData_ListAdded(object sender, MetaDataListAddedEventArgs e)
        {
            BaseControl parentControl = formMediator.Report;
            DictionaryList control = e.newItem;
            TreeListNode parentNode = rootNode;
                                    
            TreeListNode node = formMediator.View.tlControls.AppendNode(new string[] { control.Name, control.Caption }, parentNode, control);
            control.tlItem = node;
            control.ListRemoving += new AddListHandler(control_ListRemoving);
        }

        void control_ListRemoving(object sender, MetaDataListAddedEventArgs e)
        {
            formMediator.View.tlControls.DeleteNode(e.newItem.tlItem);
        }

        void Report_ControlRemoving(object sender, aetpControlsEventHandlerArgs args)
        {
            formMediator.View.tlControls.DeleteNode(args.control.tlItem);
        }

        void removeTreeList(Page page)
        {
            foreach (BaseControl bc in page.Children)
            {
                if (bc is GridAddedPage)
                {
                    GridAddedPage grid = bc as GridAddedPage;
                                            
                    for (int i = grid.Children.Count -1; i > 0 ; i--)
                    {
                        formMediator.View.tlControls.DeleteNode(grid.Children[i].tlItem);
                        
                        grid.Children[i].Remove();
                    }
                }
                formMediator.View.tlControls.DeleteNode(bc.tlItem);
            }
        }

        public void RefreshTreeList()
        {
            list_control = new List<BaseControl>();
            list_page = new List<Page>();
            formMediator.View.tlControls.ClearNodes();
            rootNode = null;
            
            if (formMediator.Report != null)
            {
                bool IsDesigner = formMediator.Report.RunMode == ViewType.Constructor;
                if (IsDesigner)
                    reportNode = formMediator.View.tlControls.AppendNode(new string[] { formMediator.Report.Name, formMediator.Report.Caption }, rootNode, formMediator.Report);
                else if (!IsDesigner)
                    reportNode = formMediator.View.tlControls.AppendNode(new string[] { formMediator.Report.Caption, formMediator.Report.Caption }, rootNode, formMediator.Report);
                list_page.Add(formMediator.Report);
                this.formMediator.Report.tlItem = reportNode;
                createTreeList(reportNode, formMediator.Report.Children, IsDesigner);
                createTreeList(rootNode, formMediator.Report.MetaData.Dictionarys.ToList<BaseControl>(), IsDesigner);
                PageVisibleChange();
                this.formMediator.Report.ControlAdded += new aetpControlsEventHandler(Report_ControlAdded);
                this.formMediator.Report.MetaData.ListAdded += new AddListHandler(MetaData_ListAdded);
            }
            formMediator.View.tlControls.ExpandAll();
        }
                
        private void PageVisibleChange()
        {
            bool IsDesigner = formMediator.Report.RunMode == ViewType.Constructor;
            if (!IsDesigner)
            {
                foreach (Page page in list_page)
                {
                    foreach (BaseControl control2 in list_control)
                    {
                        if (page.ListControl != null && control2 == page.ListControl)
                        {
                            if (control2 is SimpleEdit && (control2 as SimpleEdit).DataType == SimpleDataType.Bool)
                            {
                                AddHandlerCheck_Changed(control2 as SimpleEdit, page);
                                Check_Change(control2 as SimpleEdit);
                            }
                            if (control2 is ComboBox)
                            {
                                AddHandlerComboBox_Changed(control2 as ComboBox, page);
                                aetp_ComboBox_Changed(control2, null);
                            }
                        }
                    }
                }
            }
        }

        void Report_ControlAdded(object sender, aetpControlsEventHandlerArgs args)
        {
            BaseControl control = args.control;
            TreeListNode parentNode = control.ParentPage.tlItem;
            CreateTreeListControl(control, parentNode, formMediator.Report.RunMode == ViewType.Constructor);
        }

        private void createTreeList(TreeListNode parentNode, List<BaseControl> children, bool IsDesigner, bool CreateNotMandatoryPage = false)
        {
            if (parentNode != null && parentNode.Tag is Page)
            {
                Page p = parentNode.Tag as Page;
                if (!p.IsShownPanel && IsDesigner == false)
                    return;
            }
            foreach (BaseControl control in children)
            {
                CreateTreeListControl(control, parentNode, IsDesigner);
            }
            
        }

        private bool ExistErrors(Page p, List<BaseControl> children)
        {
            if (!p.IsShownPanel)
            {
                return false;
            }
            else
            {
                foreach (aetp.Common.ControlModel.BaseControl ctrl in children)
                {
                    if (ctrl is MetaConrol)
                    {
                        if (ExistErrors(p, (ctrl as MetaConrol).Children)) return true;
                    }
                    else if (ctrl is MetaGrid)
                    {
                        if (ExistErrors(p, (ctrl as MetaGrid).Tables.OfType<BaseControl>().ToList())) return true;
                    }
                    else if (ctrl is Grid)
                    {
                        foreach (aetp.Common.ControlModel.GridRow row in (ctrl as Grid).Rows)
                        {
                            foreach (GridCellItem cell in row.Cells)
                            {
                                if ((cell.IsMandatoryToFill) && (cell.EditValue == null))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                    else if (ctrl is GridAddedRow)
                    {

                        if (ctrl.IsMandatoryToFill && ((ctrl as GridAddedRow).Data == null ||(ctrl as GridAddedRow).Data.Rows.Count == 0))
                        {
                            return true;
                        }
                        else if ((ctrl as GridAddedRow).Data != null && (ctrl as GridAddedRow).Data.Rows.Count > 0)
                        {
                            var grid = ctrl as GridAddedRow;
                            System.Data.DataTable dt = grid.Data;
                            for (int c = 0; c < dt.Columns.Count; c++)
                            {
                                if (grid.Columns[c].Type == GridCellType.Label || !grid.Columns[c].IsMandatoryToFill) continue;
                                for (int r = 0; r < dt.Rows.Count; r++)
                                {
                                    if (dt.Rows[r][c] == null || dt.Rows[r][c].ToString() == String.Empty) return true;
                                }
                            }
                        }
                        
                    }
                    else if (ctrl is SimpleEdit || ctrl is ComboBox)
                    {
                        if ((ctrl.IsMandatoryToFill) && ((ctrl.EditValue == null) || (ctrl.EditValue.ToString() == "")))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        
        void CreateTreeListControl(BaseControl control, TreeListNode parentNode, bool IsDesigner)
        {
            if (control is Page)
                list_page.Add(control as Page);

            if (!list_control.Contains(control)) list_control.Add(control);

            if (control is DictionaryList && IsDesigner)
            {
                (control as DictionaryList).ListRemoving -= control_ListRemoving;
                (control as DictionaryList).ListRemoving += control_ListRemoving;
                control.Parent = reportNode.Tag as BaseControl;
                TreeListNode node = formMediator.View.tlControls.AppendNode(new string[] { control.Name, control.Caption }, parentNode, control);
                control.tlItem = node;
            }
            else
            {
                addChangeHandler(IsDesigner, control);
                if (!IsDesigner && !(control is Page) && !(control is MetaConrol) && !(control is GridAddedPage))
                    return;

                if (!IsDesigner && (control is Page))
                {
                    if (control is GridPage)
                    {
                        TreeListNode node = formMediator.View.tlControls.AppendNode(new string[] { control.Caption, 
                        control.Caption }, parentNode, control);
                        control.tlItem = node;
                        formMediator.PostProcessingControlAdd(control);

                        node.Visible = (control as Page).Visible_Page;

                        int index = 0;

                        for (int i = 0; i < (control.Parent as GridAddedPage).Pages.Count;i++ )
                        {
                            if ((control.Parent as GridAddedPage).Pages[i].Name == control.Name)
                            {
                                index = i; break;
                            }
                        }

                        formMediator.View.tlControls.SetNodeIndex(node, index);
                        if (control.Children.Count > 0)
                            createTreeList(node, control.Children, IsDesigner);                       
                                               
                    }
                    else
                    {
                        TreeListNode node = formMediator.View.tlControls.AppendNode(new string[] { control.Caption, 
                        control.Caption }, parentNode, control);
                        control.tlItem = node;
                        formMediator.PostProcessingControlAdd(control);

                        node.Visible = (control as Page).Visible_Page;

                        if (control.Children.Count > 0)
                            createTreeList(node, control.Children, IsDesigner);
                        //GetIsMandatoryElement(control as Page);
                    }
                }

                if (!IsDesigner && (control is GridAddedPage))
                {
                    var grid = control as GridAddedPage;
                    if (grid.Children.Count > 1)
                    {
                        createTreeList(parentNode, grid.Pages, IsDesigner);
                    }
                }

                if (!IsDesigner && (control is MetaConrol))
                {
                    foreach (BaseControl ctrl in control.Children)
                    {
                        if (ctrl is Page)
                            list_page.Add(control as Page);

                        if (!list_control.Contains(ctrl)) list_control.Add(ctrl);
                        addChangeHandler(IsDesigner, ctrl);
                    }
                }

                if (IsDesigner)
                {
                    TreeListNode node = formMediator.View.tlControls.AppendNode(new string[] { control.Name, control.Caption }, parentNode, control);
                    control.tlItem = node;
                    formMediator.PostProcessingControlAdd(control);
                    if (control.Children.Count > 0)
                        createTreeList(node, control.Children, IsDesigner);
                }
            }
            if (parentNode != null)
                parentNode.Expanded = true;
        }

        void addChangeHandler(bool IsDesigner, BaseControl control)
        {
            //(!IsDesigner && control is Page && control.Parent is GridAddedPage) для совместимости со старыми отчетами
            if (IsDesigner || (control is GridPage && !IsDesigner) || (control is GridAddedPage && !IsDesigner)
                || (!IsDesigner && control is Page && control.Parent is GridAddedPage))
            {
                control.ControlAdded -= Report_ControlAdded;
                control.ControlAdded += Report_ControlAdded;
                control.ControlRemoving -= Report_ControlRemoving;
                control.ControlRemoving += Report_ControlRemoving;
            }
        }

        void AddHandlerCheck_Changed(SimpleEdit editor,Page p)
        {
            (editor as SimpleEdit).PropertyChanged += aetp_Check_Changed;
        }

        void AddHandlerComboBox_Changed(ComboBox editor, Page p)
        {
            (editor as ComboBox).PropertyChanged += aetp_ComboBox_Changed;
        }

        void aetp_Check_Changed(object sender, EventArgs e)
        {
            Check_Change(sender as SimpleEdit);
        }

        void Check_Change(SimpleEdit se)
        {
            foreach (BaseControl ctrl in list_control)
            {
                if (ctrl is Page)
                {
                    //if ((ctrl as Page).ListControl != null &&
                    //    Utils.Utils.GetFieldName((ctrl as Page).ListControl.Name) == Utils.Utils.GetFieldName(se.Name))
                    if ((ctrl as Page).ListControl != null &&
                        (ctrl as Page).ListControl == se)
                    {
                        TreeListNode node = (ctrl as Page).tlItem;
                        if (se.EditValue != null)
                        {
                            if (se.EditValue.ToString().Trim() == (ctrl as Page).IsChecked.ToString().Trim())
                            {
                                node.Visible = (bool)((ctrl as Page).IsVisible);
                            }
                            else if ((se.EditValue.ToString().Trim() != "False") && (se.EditValue.ToString().Trim() != "True") && ((ctrl as Page).IsChecked.ToString().Trim() == "None"))
                            {
                                node.Visible = (bool)((ctrl as Page).IsVisible);
                            }
                            else node.Visible = (bool)((ctrl as Page).Visible_Page);
                        }
                        else node.Visible = (bool)((ctrl as Page).Visible_Page);
                    }
                }
            }
        }

        void aetp_ComboBox_Changed(object sender, EventArgs e)
        {
            foreach (BaseControl ctrl in list_control)
            {
                if (ctrl is Page)
                {
                    if ((ctrl as Page).ListControl != null)
                    {
                        //if (Utils.Utils.GetFieldName((ctrl as Page).ListControl.Name) == Utils.Utils.GetFieldName((sender as ComboBox).Name))
                        if ((ctrl as Page).ListControl == sender)
                        {
                            TreeListNode node = (ctrl as Page).tlItem;
                            if ((sender as ComboBox).EditValue != null && (sender as ComboBox).EditValue != DBNull.Value)
                            {
                                if ((ctrl as Page).Items != null && (ctrl as Page).Items.Contains(
                                    ReportUtils.GetComboBoxValue((sender as aetp.Common.ControlModel.ComboBox).EditValue).Code.Trim()))
                                {
                                    node.Visible = (bool)((ctrl as Page).IsVisible);
                                }                               
                                else node.Visible = (bool)((ctrl as Page).Visible_Page);
                            }
                            else
                            {
                                node.Visible = (bool)((ctrl as Page).Visible_Page);
                            }
                        }
                    }
                }
            }
        }
    }
}
