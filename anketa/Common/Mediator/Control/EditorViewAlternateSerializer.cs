﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using aetp.Common.ControlModel;
using System.Xml.XPath;
using System.Data;

namespace aetp.Common.Mediator
{
    public class EditorViewAlternateSerializer
    {
        string fileName;
        XmlHelper helper;
        
        public EditorViewAlternateSerializer(string fileName)
        {
            this.fileName = fileName;
            helper = new XmlHelper(fileName);
            
        }

        public bool Desereilize(XmlReader reader, BaseControl ctrl)
        {
            if (isOldForm1100(reader, ctrl))
            {
                processOldForm1100(reader, ctrl);
                return true;
            }
            return false;
        }

        bool isOldForm1100(XmlReader reader, BaseControl ctrl)
        {
            if (ctrl is Page && ctrl.SerializingTag == "Ф5БЧ1ГлАР5Т5" && reader.LocalName == "Ф5БЧ1ГлАР5Т5")
            {
                XPathNavigator node = helper.GetNode("*/Форма1100/Раздел5/Форма5Б/Ф5БЧ1/Ф5БЧ1ГлА/Ф5БЧ1ГлАР5/Ф5БЧ1ГлАР5Т5");
                if (node == null) return false; 
                XmlReader newreader = node.ReadSubtree();
                newreader.ReadStartElement();
                if (newreader.LocalName == "Ф5БЧ1ГлАР5Т4_1") return true;
            }
            return false;
        }

        void processOldForm1100(XmlReader reader, BaseControl ctrl)
        {
            bool IsEmpty = reader.IsEmptyElement;
            Page page = ctrl as Page;
            reader.ReadStartElement();
            if (IsEmpty)
            {
                if (!page.IsMandatoryToFill) page.IsShownPanel = false;
                return;
            }
            page.IsShownPanel = true;
            processOldForm1100Grid(reader, ctrl.Children[0]);
            reader.ReadEndElement();
            
        }

        void processOldForm1100Grid(XmlReader reader, BaseControl ctrl)
        {
            bool IsEmpty = reader.IsEmptyElement;
            if (reader.LocalName != EditorViewSerializer.getCorrectName("Ф5БЧ1ГлАР5Т4_1"))
                return;
            else
                reader.ReadStartElement();
            if (IsEmpty)
                return;
            DeserOldForm1100GridBody(reader, ctrl);
            reader.ReadEndElement();
        }
        string[] row_tags = { "Ф5БЧ1ГлАР5Т4_1_51", "Ф5БЧ1ГлАР5Т4_1_52", "Ф5БЧ1ГлАР5Т4_1_53", "Ф5БЧ1ГлАР5Т4_1_54", "Ф5БЧ1ГлАР5Т4_1_55" };
        private void DeserOldForm1100GridBody(XmlReader reader, aetp.Common.ControlModel.BaseControl ctrl)
        {
            Grid grid = ctrl as Grid;
            int r = 0;
            grid.Data = new DataTable();
            foreach (var col in grid.Columns)
            {
                grid.Data.Columns.Add(col.Name);
            }
            foreach (aetp.Common.ControlModel.GridRow row in grid.Rows)
            {
                bool empty = reader.IsEmptyElement;
                reader.ReadStartElement(row_tags[r], EditorViewSerializer.config.namespace_av);
                if (empty)
                {
                    foreach (var cell in grid.Rows[r].Cells)
                    {
                        if (cell.Type == GridCellType.Label)
                        {
                            cell.EditValue = cell.Caption;
                        }
                        else
                        {
                            cell.EditValue = null;
                        }
                    }
                    var empty_row_items = grid.Rows[r].Cells.Select<GridCellItem, object>(x =>
                    {
                        if (x.Type == GridCellType.Label) return x.Caption;
                        else return DBNull.Value;
                    }).ToArray();
                    grid.Data.Rows.Add(empty_row_items);
                    r++;
                    continue;
                }
                int c = -1;
                foreach (GridCellItem cell in row.Cells)
                {
                    c++;
                    if (reader.LocalName != cell.SerializingTag)
                        continue;

                    object value = null;

                    if (!reader.IsEmptyElement)
                    {
                        switch (cell.Type)
                        {
                            case GridCellType.String:
                            case GridCellType.Label:
                                value = reader.ReadElementContentAsString();
                                break;
                            case GridCellType.Check:
                                if (cell.boolSerializationType == BoolSerializtionType.yesno)
                                {
                                    YesNoClass yesno = new YesNoClass(EditorViewSerializer.config.namespace_av);
                                    yesno.ReadXml(reader);
                                    value = (bool)yesno.EditValue;
                                }
                                else
                                {
                                    if (!reader.IsEmptyElement)
                                        value = reader.ReadElementContentAsBoolean();
                                    else
                                        reader.ReadStartElement();
                                }
                                break;
                            case GridCellType.Date:
                                if (!reader.IsEmptyElement)
                                    value = reader.ReadElementContentAsDateTime();
                                else
                                    reader.ReadStartElement();
                                break;
                            case GridCellType.Double:
                                if (!reader.IsEmptyElement)
                                    value = reader.ReadElementContentAsDouble();
                                else
                                    reader.ReadStartElement();
                                break;
                            case GridCellType.Integer:
                                if (!reader.IsEmptyElement)
                                    value = reader.ReadElementContentAsLong();
                                else
                                    reader.ReadStartElement();
                                break;
                            case GridCellType.List:
                                if (!reader.IsEmptyElement)
                                {
                                    reader.ReadStartElement();
                                    value = EditorViewSerializer.ReadDictionaryListItem(reader).Code;
                                    reader.ReadEndElement();
                                }
                                else
                                    reader.ReadStartElement();
                                break;

                        }
                    }
                    else
                        reader.Read();

                    if (cell.Type != GridCellType.Label)
                    {
                        grid.Rows[r].Cells[c].EditValue = value;
                    }
                    else
                    {
                        grid.Rows[r].Cells[c].EditValue = grid.Rows[r].Cells[c].Caption;
                    }
                }
                var items = grid.Rows[r].Cells.Select(x =>
                {
                    if (x.Type == GridCellType.Label) return x.Caption;
                    else return x.EditValue;
                }).ToArray();
                grid.Data.Rows.Add(items);
                r++;
                reader.ReadEndElement();
            }
        }
    }

    public class XmlHelper
    {
        /// <summary>
        /// XPathNavigator object
        /// </summary>
        private XPathNavigator Navigator;
        /// <summary>
        /// NamespaceManager
        /// </summary>
        private XmlNamespaceManager nsManager;
        private string nsLocal;
        private bool useLocalNamespace = true;

        public XmlHelper(string fileName)
        {
            using (XmlReader reader = XmlReader.Create(fileName))
            {
                Initialize(reader);
            }
        }

        public XmlHelper(XmlReader reader)
        {
            Initialize(reader);
        }

        void Initialize(XmlReader reader)
        {
            XPathDocument doc = new XPathDocument(reader);
            Navigator = doc.CreateNavigator();
            Navigator.MoveToFirstChild();
            nsManager = new XmlNamespaceManager(Navigator.NameTable);
            while (String.IsNullOrEmpty(nsLocal))
            {

                if (!string.IsNullOrEmpty(Navigator.Prefix))
                {
                    nsManager.AddNamespace(
                        Navigator.Prefix, Navigator.NamespaceURI
                    );
                    nsLocal = Navigator.Prefix;
                }
                else if (Navigator.NamespaceURI != null)
                {
                    nsLocal = "av";
                    nsManager.AddNamespace(
                        nsLocal, Navigator.NamespaceURI
                    );
                }
                if (!Navigator.MoveToChild(XPathNodeType.All))
                    break;
            }
            Navigator.MoveToRoot();
        }

        private string XPathString(string s, string ns)
        {
            if (string.IsNullOrEmpty(ns) || !useLocalNamespace)
                return s;
            else
            {
                var items = s.Split('/');
                if (items.Count() == 1)
                {
                    return ns + ":" + items[0];
                }
                else
                {
                    return string.Join("/" + ns + ":", items);
                }
            }
        }

        public XPathNavigator GetNode(string xpath)
        {
            xpath = XPathString(xpath, nsLocal);
            return Navigator.SelectSingleNode(xpath, nsManager);
        }

        public string GetValue(string xpath, XPathNavigator navigator = null)
        {
            xpath = XPathString(xpath, nsLocal);
            // берем относительный путь если передали навигатор
            if (navigator != null && xpath.StartsWith("/"))
            {
                xpath = xpath.Remove(0, 1);
            }
            var e = (navigator ?? Navigator).SelectSingleNode(xpath, nsManager);
            if (e == null)
                return null;
            else
                return e.Value.Trim();
        }
    }
}
