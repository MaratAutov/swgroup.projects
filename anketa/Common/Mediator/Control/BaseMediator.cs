﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using aetp.Common.View;
using DevExpress.XtraBars.Ribbon;
using System.Windows.Forms;
using System.Threading;

namespace aetp.Common.Mediator
{
    public abstract class BaseMediator
    {
        protected TreeListMediator treeListMediator;
        public Form Splash { get; set; }

        #region public propertys

        private Report _report;
        public Report Report 
        {
            get
            {
                return _report;
            }
            set
            {
                _report = value;
                if (treeListMediator != null)
                    treeListMediator.RefreshTreeList();
            }
        }

        ITreeListWindow _view;
        public ITreeListWindow View 
        {
            get
            {
                return _view;
            }
            set
            {
                value.tlControls.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(tlControls_FocusedNodeChanged);
                (value as System.Windows.Forms.Form).Load += new EventHandler(BaseMediator_Load);
                _view = value;
            }
        }

        void BaseMediator_Load(object sender, EventArgs e)
        {
            Thread.Sleep(1000);
            if (Splash != null)
                Splash.Close();
        }

        public abstract void PostProcessingControlAdd(BaseControl control);

        protected virtual void tlControls_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            
        }

        #endregion

        #region Constructors

        private BaseMediator() { }

        public BaseMediator(ITreeListWindow view)
        {
            this.View = view;
            treeListMediator = new TreeListMediator(this);
        }

        #endregion

        
    }
}
