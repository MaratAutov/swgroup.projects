﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using aetp.Common.Control;
using aetp.Common.Print;
using aetp.Common.View;
using aetp.Common.ControlModel;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Windows.Forms;
using aetp.ElectroSignature;
using aetp.DocumentsSubmitter.Views;
using aetp.Common.View.Dialog;
using aetp.Common.Serializer;
using aetp.Common.Validator;
using DevExpress.XtraGrid.Views.Grid;
using aetp.Utils;


namespace aetp.Common.Mediator
{
	public class EditorMediator : BaseMediator
	{
        Printing printer;
        public static DependantControl dependantControls = new DependantControl();
        IEditorWindow window;
		#region Constructors

		public EditorMediator(ITreeListWindow view, Report report,string filename)
			: base(view)
		{
			IEditorWindow v = View as IEditorWindow;
            window = v;
			v.SaveAs += new EventHandler(v_SaveAs);
            v.ExportToPdf += new EventHandler(v_ExportToPdf);
            v.ExportToRtf += new EventHandler(v_ExportToRtf);
            v.ExportToExcel += new EventHandler(v_ExportToExcel);
            v.AboutProgram += new EventHandler(v_AboutProgram);
            //v.Help += new EventHandler(v_HelpForm);
            v.NetworkSettings += new EventHandler(v_NetworkSettings);
            v.Save += new EventHandler(v_Save);
			v.LoadFile += new EventHandler(v_LoadFile);
            v.OpenModels += new EventHandler(v_OpenModels);
			v.SavePacketAs += new EventHandler(v_SavePacketAs);
			v.SendPacket += new EventHandler(v_SendPacket);
			v.DocumentsSubmitterOpen += new EventHandler(v_DocumentsSubmitterOpen);
			v.CreateNew += new EventHandler(v_CreateNew);
			v.Validating += new ValidatingEventHandler(v_Validating);
			v.PrintPacket += new EventHandler(v_Print);
            v.AddSign += new EventHandler(v_AddSign);
            v.OrganizationData += new EventHandler(v_OrganizationData);
            v.AttachFile += new EventHandler(v_AttachFile);
            v.ExportXtddExcel += new EventHandler(v_ExportXtddExcel);
            v.LoadSection += new EventHandler(v_LoadSection);
            v.SaveSection += new EventHandler(v_SaveSection);
            v.ConvertPacket += new EventHandler(v_ConvertPacket);
			Report = report;

            printer = new Printing(this);

            ImageList imageList = new ImageList();           
            imageList.Images.Add(global::aetp.Common.Properties.Resources.attach20);
            lv.Width = 100;
            lv.BorderStyle = BorderStyle.None;
            lv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            lv.ShowItemToolTips = true;
            lv.SmallImageList = imageList;
           
            lv.MultiSelect = false;            
            lv.View = System.Windows.Forms.View.SmallIcon;
            lv.MouseClick += new MouseEventHandler(lv_MouseClick);  
            lv.MouseDoubleClick += new MouseEventHandler(lv_MouseDoubleClick);

            if (filename.Length > 0)
            {
                try
                {
                    LoadFile(filename);
                }
                catch (Exception ex)
                {
                    ErrorHelper.Show(ex);
                }
            }
		}

        void v_SaveSection(object sender, EventArgs e)
        {
            var model = sender as EditorModel;
            if (model.CurrentPage != null)
            {
                CustomErrorInfo error = GetError(model.CurrentPage);
                if (error != null)
                    if (MessageBox.Show(error.ToString() + Environment.NewLine + "Все равно продолжить?", "Ошибки!", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
                        return;
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "(*.xtdd)|*.xtdd";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        File.WriteAllText(sfd.FileName, SerializeReport(model.CurrentPage));
                        Report.EditorSave = true;
                        (View as IEditorWindow).SaveButton.Enabled = false;
                        Report_FileName = sfd.FileName;
                    }
                    catch (Exception ex)
                    {
                        ErrorHelper.Show(ex);
                    }
                }
            }
        }

        void v_LoadSection(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "(*.xtdd)|*.xtdd";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    EditorViewSerializer serializer = new EditorViewSerializer();
                    EditorViewSerializer.config = Report.SerialaizerConfig;
                    aetp.Common.ControlModel.Page page = (sender as EditorModel).CurrentPage;
                    serializer.Deserialize(ofd.FileName, page);
                    var pmanager = new PageManager(window);
                    pmanager.RefreshPage(page);
                    ReportUtils.DoAction(page.Children, x => { 
                        if (x is aetp.Common.ControlModel.Page) 
                        { 
                            pmanager.DisposePage(x as aetp.Common.ControlModel.Page); 
                        } 
                    });
                    Report.EditorSave = true;
                }
                catch (Exception ex)
                {
                    ErrorHelper.Show(ex);
                }
            }
        }

        void LoadFile(string file)
        {
            Report = null;
            GC.Collect();
            EditorViewSerializer serializer = new EditorViewSerializer();
            Report = serializer.Deserialize(file, this);
            if (Report != null)
            {
                Report.ReportChange = false;
                Editor_Title();
                (View as IEditorWindow).ReInit();
                (View as IEditorWindow).SaveButton.Enabled = false;
                View.Report = Report;
                Report.Report_Change += new EventHandler(v_Report_Change);
                Report_FileName = file;
                RefreshFilePanelVisibility();
                
            }
        }

        void v_NetworkSettings(object sender, EventArgs e)
        {
            new ProxyServerDialog().ShowDialog();
        }

        #endregion

        public override void PostProcessingControlAdd(BaseControl control) { }
        
        public string _report_filename;
        public string Report_FileName
        {
            get
            {
                return _report_filename;
            }
            set
            {
                _report_filename = value;
            }
        }

        void v_AboutProgram(object sender, EventArgs e)
        {
            AboutProgram about = new AboutProgram();
            about.ShowDialog();
        }

        void v_HelpForm(object sender, EventArgs e)
        {
            HelpForm help = new HelpForm();
            help.ShowDialog();
        }

        void v_CreateNew(object sender, EventArgs e)
		{
			SelectModelFormDialog selectorModel = new SelectModelFormDialog();
			if (selectorModel.ShowDialog() == DialogResult.OK)
			{
				FormSerializer serializer = new FormSerializer();
				Report report;
				using (XmlTextReader reportReader = new XmlTextReader(selectorModel.SelectedModelFullPath))
				{
					report = new FormSerializer().Deserialize(reportReader, true);
				}
				if (report != null)
					report.RunMode = ViewType.Editor;
                report.InitSouceMetaControlHandler();
                dependantControls.Init(report);
                report.InitialDefaultValues();
                printer = new Printing(this);
                (View as IEditorWindow).ReInit();
                report.ReportChange = false;
                this.Report = report;
				View.Report = report;
                Report.Report_Change += new EventHandler(v_Report_Change);
				(View as IEditorWindow).ShowFirstPage();                
                Editor_Title();
                (View as IEditorWindow).SaveButton.Enabled = false;
                Report_FileName = "";
                RefreshFilePanelVisibility();
			}
		}

        void Editor_Title()
        {
            if (Report != null)
            {
                (View as IEditorWindow).Title = (View as IEditorWindow).title + " - " + Report.Caption;
            }
        }

        void v_AttachFile(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Прикрепить файл";
            ofd.Filter = "All files (*.*)|*.*";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string file_name = ofd.FileName.Remove(0, ofd.FileName.LastIndexOf("\\") + 1);
                bool exist = false;
                for (int i = 0; i < Report.File.Count; i++)
                {
                    if (((FileList)Report.File[i]).Name == file_name)
                    {
                        exist = true;
                        MessageBox.Show("Файл с таким именем уже добавлен");
                    }
                }

                if (!exist)
                {
                    if (!string.IsNullOrEmpty(ofd.FileName))
                    {
                        FileStream fs = new FileStream(ofd.FileName,
                                                       FileMode.Open,
                                                       FileAccess.Read);
                        byte[] filebytes = new byte[fs.Length];
                        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
                        string encodedData =
                            Convert.ToBase64String(filebytes,
                                                   Base64FormattingOptions.InsertLineBreaks);

                        Report.File.Add(new FileList(file_name, encodedData, "File"));
                        RefreshFileList();
                        Report.ReportChange = true;
                    }
                }
            }             
        }

        public ListView lv = new ListView();

        void RefreshFileList()
        {            
            lv.Items.Clear();
            
            if (Report.File.Count > 0)
            {
                (View as IEditorWindow).PanelFile.Visible = true;
                if ((View as IEditorWindow).splitterControl.SplitterPosition == 1)
                (View as IEditorWindow).splitterControl.SplitterPosition = 80;
                (View as IEditorWindow).splitterControl.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;

                foreach (FileList item in Report.File)
                {
                    ListViewItem lvi = new ListViewItem(new string[] { "" });
                    
                    lvi.SubItems[0].Text = item.Name;
                    lvi.ToolTipText = item.Name;
                    lvi.ImageIndex = 0;                   
                    lv.Items.Add(lvi);
                }
                //lv.Height = (View as IEditorWindow).PanelFile.Height;
                lv.Dock = DockStyle.Left;
                lv.Width = 1050;                
                (View as IEditorWindow).PanelFile.Controls.Add(lv);
            }
            else
            {
                (View as IEditorWindow).PanelFile.Visible = false;
                (View as IEditorWindow).splitterControl.SplitterPosition = 1;
                (View as IEditorWindow).splitterControl.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel1;
            }           
            
        }

        private void lv_MouseClick(object sender, MouseEventArgs e)
        {
            ListView listView = sender as ListView;
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ListViewItem item = listView.GetItemAt(e.X, e.Y);
                if (item != null)
                {
                    item.Selected = true;
                    ContextMenu menu = new ContextMenu();
                    MenuItem menu_open = new MenuItem("Открыть");
                    menu_open.Click += new EventHandler(menu_open_Click);
                    MenuItem menu_save = new MenuItem("Сохранить как");
                    menu_save.Click += new EventHandler(menu_save_Click);
                    MenuItem menu_delete = new MenuItem("Удалить");
                    menu_delete.Click += new EventHandler(menu_delete_Click);
                    menu.MenuItems.Add(menu_open);
                    menu.MenuItems.Add(menu_save);
                    menu.MenuItems.Add(menu_delete);
                    menu.Show(listView, e.Location);
                }
            }
        }


        private void lv_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            SaveOpen_File(); 
        }


        void SaveOpen_File()
        {
                if (lv.SelectedItems.Count == 1)
                {
                    if (Report.File[lv.SelectedItems[0].Index].Data != null)
                    {
                        string path = Path.Combine(aetp.Utils.Config.GetAppPath(), "temp");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string filename = Path.Combine(path, Report.File[lv.SelectedItems[0].Index].Name.ToString());
                        try
                        {
                            byte[] filebytes = Convert.FromBase64String(Report.File[lv.SelectedItems[0].Index].Data.ToString());
                            FileStream fs = new FileStream(filename,
                                                   FileMode.Create,
                                                   FileAccess.Write,
                                                   FileShare.None);
                            fs.Write(filebytes, 0, filebytes.Length);
                            fs.Close();

                            var proc = new System.Diagnostics.Process();
                            proc.StartInfo.FileName = filename;
                            proc.StartInfo.UseShellExecute = true;
                            proc.Start();
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.Show(ex);
                        }
                    }
                }
        }

        // Сохранить
        void menu_save_Click(object sender, EventArgs e)
        {            
            if (lv.SelectedItems.Count == 1)
                {
                    if (Report.File[lv.SelectedItems[0].Index].Data != null)
                    {
                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.Filter = "All files (*.*)|*.*";
                        sfd.FileName = Report.File[lv.SelectedItems[0].Index].Name.ToString();
                        
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            try
                            {
                                byte[] filebytes = Convert.FromBase64String(Report.File[lv.SelectedItems[0].Index].Data.ToString());
                                FileStream fs = new FileStream(sfd.FileName,
                                                       FileMode.Create,
                                                       FileAccess.Write,
                                                       FileShare.None);
                                fs.Write(filebytes, 0, filebytes.Length);
                                fs.Close();
                            }
                            catch (Exception ex)
                            {
                                ErrorHelper.Show(ex);
                            }
                        }
                    }
                }            
        }

        // Удалить файл
        void menu_delete_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count == 1)
            {
                Report.File.RemoveAt(lv.SelectedItems[0].Index);
                Report.ReportChange = true;
            }            
            RefreshFileList();
        }

        // Открыть файл
        void menu_open_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count == 1)
            {
                SaveOpen_File(); 
            }
        }

        void v_Report_Change(object sender, EventArgs e)
        {
            (View as IEditorWindow).SaveButton.Enabled = Report.ReportChange;
        }

        void v_OrganizationData(object sender, EventArgs e)
        {
            Organization organization_data = new Organization();
            if (organization_data.ShowDialog() != DialogResult.OK)
                return;            
            
        }

        CustomErrorInfo GetError(aetp.Common.ControlModel.Page page)
        {
            if (Report == null)
                return null;
            return ValidationCollection.GetError(Report.guid, page);
        }

		void v_Validating(object sender, ValidatingEventArgs e)
		{
            GC.Collect();
            if (Report != null && Report.AttachFile && Report.AttachFileIsMandatory && Report.File.Count < 1)
            {                
                MessageBox.Show("Не прикреплён ни один файл к отчёту");
                return;                  
            }

            CustomErrorInfo error = GetError(e.Page);
			if (error != null)
				MessageBox.Show(error.ToString());
			else
				MessageBox.Show("Проверка прошла успешно! Ошибок нет!");
		}

		void v_DocumentsSubmitterOpen(object sender, EventArgs e)
		{
			SessionForm frm = new SessionForm();
			frm.Show();
		}

		void v_SendPacket(object sender, EventArgs e)
		{
            if (Report != null)
            {
                CustomErrorInfo error = GetError(null);
                if (error != null)
                    if (MessageBox.Show(error.ToString() + Environment.NewLine + "Все равно продолжить?", "Ошибки!", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
                        return;
                SessionForm frm = new SessionForm(UTF8Encoding.UTF8.GetBytes(SerializeReport()));
                frm.Show();
            }
		}

		void v_SavePacketAs(object sender, EventArgs e)
		{
            if (Report != null)
            {
                CustomErrorInfo error = GetError(null);
                if (error != null)
                    if (MessageBox.Show(error.ToString() + Environment.NewLine + "Все равно продолжить?", "Ошибки!", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
                        return;
			    SaveFileDialog sfd = new SaveFileDialog();
			    sfd.Filter = "(*.zip)|*.zip";
			    if (sfd.ShowDialog() == DialogResult.OK)
			    {
				    try
				    {
					    byte[] uTF8EncodingUTF8GetBytes = UTF8Encoding.UTF8.GetBytes(SerializeReport());
					    if (uTF8EncodingUTF8GetBytes != null)
					    {
						    ElectroSignatureEngine.ParentForm = View as Form;
						    ElectroSignatureEngine ecp = new ElectroSignatureEngine();
						    string name = Path.GetFileNameWithoutExtension(sfd.FileName);
                            byte[] tempFile = ecp.Sign(name + ".xtdd", uTF8EncodingUTF8GetBytes);
                            if (tempFile != null)
                            {
                                File.WriteAllBytes(sfd.FileName, tempFile);
						        MessageBox.Show(string.Format("Пакет '{0}' сохранен успешно.", name));
					        }
                            else
                            {
                                MessageBox.Show(string.Format("Произошла ошибка при сохранении пакета '{0}'.", name), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
					    }
				    }
				    catch (Exception ex)
				    {
                        ErrorHelper.Show(ex);
				    }
			    }
		    }
		}

        void v_AddSign(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "(*.zip)|*.zip|Все файлы(*.*)|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    ElectroSignatureEngine.ParentForm = View as Form;
                    ElectroSignatureEngine ecp = new ElectroSignatureEngine();
                    Byte[] newBody = ecp.AddSign(File.ReadAllBytes(ofd.FileName));
                    if (newBody == null)
                        throw new Exception("Невозможно добавить подпись к пакету");
                    File.Delete(ofd.FileName);
                    File.WriteAllBytes(ofd.FileName, newBody);
                    MessageBox.Show("Подпись к сообщению успешно добавлена", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

		void v_LoadFile(object sender, EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.Filter = "(*.xtdd)|*.xtdd";
			if (ofd.ShowDialog() == DialogResult.OK)
			{
				try
				{
                    LoadFile(ofd.FileName);
                    Report.EditorSave = true;
                    (View as IEditorWindow).ShowFirstPage();
				}
				catch (Exception ex)
				{
                    ErrorHelper.Show(ex);
				}
			}
		}

        void RefreshFilePanelVisibility()
        {
            if (Report.AttachFile)
            {
                (View as IEditorWindow).AttachFileButton.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                RefreshFileList();
            }
            else
            {
                (View as IEditorWindow).AttachFileButton.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                (View as IEditorWindow).PanelFile.Visible = false;
                (View as IEditorWindow).splitterControl.SplitterPosition = 1;
                (View as IEditorWindow).splitterControl.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel1;
            }
        }

        void v_OpenModels(object sender, EventArgs e)
        {
            try
            {
                AnketFormUpdaterFromWeb.Update();
                EditorApp.MainView.SetTitle();
            }
            catch (System.Net.WebException)
            {
                EditorApp.MainView.SetTitleNetworkError();
                DialogHelper.ShowProxyConfigDialog();
            }
            catch (Exception ex)
            {
                ErrorHelper.Show(ex);
            }
        }

		void v_SaveAs(object sender, EventArgs e)
		{
            if (Report != null)
            {
            CustomErrorInfo error = GetError(null);
            if (error != null)
                if (MessageBox.Show(error.ToString() + Environment.NewLine + "Все равно продолжить?", "Ошибки!", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
                    return;
			SaveFileDialog sfd = new SaveFileDialog();
			sfd.Filter = "(*.xtdd)|*.xtdd";
            
			    if (sfd.ShowDialog() == DialogResult.OK)
			    {
				    try
				    {
					    File.WriteAllText(sfd.FileName, SerializeReport());
                        Report.EditorSave = true;
                        (View as IEditorWindow).SaveButton.Enabled = false;
                        Report_FileName = sfd.FileName;
                        Report.ReportChange = false;
				    }
				    catch (Exception ex)
				    {
                        ErrorHelper.Show(ex);
				    }
			    }
		    }
            else
                MessageBox.Show("Невозможно сохранить пустой отчет");
		}

        void v_Save(object sender, EventArgs e)
        {
            if (Report != null)
            {
                CustomErrorInfo error = GetError(null);
                if (error != null)
                    if (MessageBox.Show(error.ToString() + Environment.NewLine + "Все равно продолжить?", "Ошибки!", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
                        return;

                if (Report.EditorSave)
                {
                    string path = string.Empty;
                    if (Report_FileName.Length > 0)
                    {
                        path = Report_FileName;
                    }
                    else
                    {
                        path = Path.Combine(AppGlobal.Instance.Config.ModelsPath, Report.Name + ".aff");
                    }                    
                    File.WriteAllText(path, SerializeReport());
                    (View as IEditorWindow).SaveButton.Enabled = false;
                    Report.ReportChange = false;

                }
                else
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.Filter = "(*.xtdd)|*.xtdd";

                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            File.WriteAllText(sfd.FileName, SerializeReport());
                            Report.EditorSave = true;
                            (View as IEditorWindow).SaveButton.Enabled = false;
                            Report_FileName = sfd.FileName;
                            Report.ReportChange = false;
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.Show(ex);
                        }
                    }
                }
            }
            else
                MessageBox.Show("Невозможно сохранить пустой отчет");
        }

        private void v_ExportToRtf(object sender, EventArgs e)
        {            
            try
            {
                printer.Export(ExportEnum.Rtf,"rtf");
            }
            catch (Exception ex)
            {
                ErrorHelper.Show(ex);
            }
        }

        private void v_ExportToExcel(object sender, EventArgs e)
        {
            try
            {
                printer.Export(ExportEnum.Excel, "xls");
            }
            catch (Exception ex)
            {
                ErrorHelper.Show(ex);
            }
        }

        private void v_ExportToPdf(object sender, EventArgs e)
        {
            try
            {
                printer.Export(ExportEnum.Pdf, "pdf");
            }
            catch (Exception ex)
            {
                ErrorHelper.Show(ex);
            }
        }

		private void v_Print(object sender, EventArgs e)
		{
            try
            {
                printer.Print();
            }
            catch (Exception ex)
            {
                ErrorHelper.Show(ex);
            }
		}

        private void v_ConvertPacket(object sender, EventArgs e)
        {
            ConvertPacket convert = new ConvertPacket();
            convert.ShowDialog();
        }

        private void v_ExportXtddExcel(object sender, EventArgs e)
		{
            printer.ExportXtddToExcel();
		}

        string SerializeReport()
        {
            return SerializeReport(Report);
        }

        string SerializeReport(aetp.Common.ControlModel.Page page)
        {
            if (page != null)
            {
                EditorView ev = View as EditorView;

                EditorViewSerializer serializer = new EditorViewSerializer();
                MemoryStream memoryStream = new MemoryStream();
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = Report.SerialaizerConfig.GetEncoding();
                settings.Indent = true;
                settings.OmitXmlDeclaration = false;
                XmlWriter xmlTextWriter = XmlWriter.Create(memoryStream, settings);
                serializer.Serialize(xmlTextWriter, page, Report.SerialaizerConfig);
                return Encoding.UTF8.GetString(memoryStream.ToArray());
            }
            else return string.Empty;
        }

	}
}
