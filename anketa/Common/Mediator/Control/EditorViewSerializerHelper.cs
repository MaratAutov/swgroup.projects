﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace aetp.Common.Mediator
{
    class EditorViewSerializerHelper
    {
        public static string DateToStr(DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd");
        }


        public static string DateToStr(object value)
        {
            if (value == null) return null;
            DateTime dt;
            if (DateTime.TryParse(value.ToString(), out dt))
            {
                return DateToStr(dt);
            }
            else
            {
                return null;
            }
            
        }
        
    }
}
