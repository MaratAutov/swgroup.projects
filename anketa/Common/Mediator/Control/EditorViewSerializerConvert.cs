﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Diagnostics;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using System.Data;
using aetp.Common.View;
using aetp.Common.ControlModel;
using aetp.Common.Control;
using System.IO;
using aetp.Common.Serializer;
using DevExpress.XtraGrid;
using aetp.Utils;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.Export;
using DevExpress.XtraRichEdit.Services;
using DevExpress.XtraRichEdit.API.Native;
using System.Reflection;


namespace aetp.Common.Mediator
{
    
    public class EditorViewSerializerConvert
    {
        
        EditorViewAlternateSerializer alternateSerializer;
        public List<string> error_node = new List<string> { };
        public EditorViewSerializerConvert()
        {
            
        }

        #region Public methods
        
        public void Serialize(XmlWriter writer, Page report)
        {
            EditorViewSerializerConfig config = new EditorViewSerializerConfig();
            Serialize(writer, report, config);
        }        

        public void Serialize(XmlWriter writer, Page report, EditorViewSerializerConfig config)
        {
            EditorViewSerializer.config = config;
            
            SerPage(writer, report, report.Children,config);
            
            writer.Flush();
            writer.Close();
        }
                
        

        public Report DeserializeConvert(string FileName)
        {
            alternateSerializer = new EditorViewAlternateSerializer(FileName);
            Report report = FindModel(new FileInfo(FileName));
            if (report != null)
            {
                report.InitSouceMetaControlHandler();
                report.InitialDefaultValues();
                EditorMediator.dependantControls.Init(report);
                report.RunMode = ViewType.Editor;
                EditorViewSerializerConfig config = report.SerialaizerConfig;
                Deserialize(CreateXmlReader(FileName), report, FileName,config);                
            }
            return report;
        }       

        

        //return reader with position in Report
        XmlReader CreateXmlReader(string fileName)
        {
            XmlReader reader = XmlReader.Create(fileName, new XmlReaderSettings()
            {
                IgnoreComments = true,
                IgnoreWhitespace = true,
                IgnoreProcessingInstructions = true
            });
            while (reader.NodeType != XmlNodeType.Element)
            {
                reader.Read();
            }
            return reader;
        }

        public void Deserialize(XmlReader reader, Page report, string fileName, EditorViewSerializerConfig config)
        {
            if (alternateSerializer == null)
            {
                alternateSerializer = new EditorViewAlternateSerializer(fileName);
            }
            try
            {
                DeserPage(reader, report, report.Children,config);                
                reader.Close();
            }
            catch (Exception ex)
            {                
                MessageBox.Show("Данный документ содержит ошибки", "Ошибка");                
            }
        }       

        #endregion

        #region private methods

        #region find model

        Report FindModel(FileInfo file)
        {
            XmlReader reader = CreateXmlReader(file.FullName);
                        
            string reportName = reader.LocalName;
            string reportVersion = reader.GetAttribute("appVersion");
            
            reader.Close();

            FormSerializer serializer = new FormSerializer();
            Report report;
            string reportPath = string.Empty;
            
            reportPath = AppGlobal.Instance.Config.ListModelInfo.Find(new ModelConfig.ModelInfoSearcher(reportName, "", reportVersion,reportName).Condition_Save).Path;
            
            if (!File.Exists(reportPath))
            {                
                MessageBox.Show("Данный документ не может быть открыт, так как не удалось найти соответсвующую модель данных", "Ошибка");
                report = null;
            }
            else
            {
                using (XmlTextReader reportReader = new XmlTextReader(reportPath))
                {
                    report = new FormSerializer().Deserialize(reportReader, true);
                }
            }
            return report;
        }
        
        #endregion

        #region utils function

        ScrollableControl GetPage(IEditorWindow form, Page page)
        {
            return form.GetPage(page);
        }

        //удаляет ненужные символы из имени страниц
        public string getCorrectName(string name)
        {
            string _name = name;
            int starts  = _name.IndexOf('$');
            if (starts != -1)
            {
                _name = _name.Remove(starts);
            }
            return _name;
        }

        void WriteStartElement(XmlWriter writer, string localName, EditorViewSerializerConfig config)
        {
            writer.WriteStartElement(config.namespace_av_prefix, localName, config.namespace_av);
        }

        //записывает элемент словаря в xmlwriter
        void WriteDictionaryListItem(XmlWriter writer, DictionaryListItem item, EditorViewSerializerConfig config)
        {
            WriteStartElement(writer, DictionaryListItem.Code_tag,config);
            writer.WriteValue(item.Code);
            writer.WriteEndElement();
            WriteStartElement(writer, DictionaryListItem.Description_tag,config);
            writer.WriteValue(item.Description);
            writer.WriteEndElement();
            WriteStartElement(writer, DictionaryListItem.Active_tag,config);
            writer.WriteValue(item.Active);
            writer.WriteEndElement();
        }

        public DictionaryListItem ReadDictionaryListItem(XmlReader reader, EditorViewSerializerConfig config)
        {
            string code = reader.ReadElementContentAsString(DictionaryListItem.Code_tag, config.namespace_av);
            string description = "";
            if (reader.LocalName == DictionaryListItem.Description_tag)
                description = reader.ReadElementContentAsString(DictionaryListItem.Description_tag, config.namespace_av);
            bool active = true;
            if (reader.LocalName == DictionaryListItem.Active_tag)
                active = reader.ReadElementContentAsBoolean(DictionaryListItem.Active_tag, config.namespace_av);
            return new DictionaryListItem() { Code = code, Description = description, Active = active };
        }

        #endregion

        #region serializing

        protected void SerPage(XmlWriter writer, Page page, List<aetp.Common.ControlModel.BaseControl> controls, EditorViewSerializerConfig config)
        {
            try
            {
                //если страницы нет то нефиг ее и сериализовать
                if (!page.IsShownPanel)
                    return;

                if (page is Report)
                {
                    writer.WriteStartElement(config.namespace_av_prefix, page.SerializingTag, config.namespace_av);
                    writer.WriteAttributeString("appVersion", config.appVersion);
                    //namespaces add
                    writer.WriteAttributeString("xmlns", "xserializer", null, config.namespace_xserializer);
                    writer.WriteAttributeString("externalId", (page as Report).guid);
                    writer.WriteAttributeString("xmlns", "xsi", null, config.namespace_xsi);
                    writer.WriteAttributeString("xmlns", "av", null, config.namespace_av);
                    writer.WriteAttributeString("xsi", "schemaLocation", null, config.namespace_schemaLocation);

                    if ((page as Report).AttachFile)
                    {
                        if ((page as Report).File.Count > 0)
                        {
                            WriteStartElement(writer, "Files", config);

                            for (int i = 0; i < (page as Report).File.Count; i++)
                            {
                                WriteStartElement(writer, (page as Report).File[i].Tag, config);
                                writer.WriteAttributeString("Name", (page as Report).File[i].Name);
                                writer.WriteValue((page as Report).File[i].Data);
                                writer.WriteEndElement();
                            }
                            writer.WriteEndElement();
                        }
                    }
                }
                else
                    WriteStartElement(writer, getCorrectName(page.SerializingTag), config);
                SerControls(writer, controls, config);

                writer.WriteEndElement();
            }
            catch
            {
            }
        }

        void SerControls(XmlWriter writer, List<aetp.Common.ControlModel.BaseControl> controls, EditorViewSerializerConfig config)
        {
            try
            {
                foreach (aetp.Common.ControlModel.BaseControl ctrl in controls)
                {
                    if (ctrl is Link)
                        continue;

                    if (ctrl is Page)
                    {
                        SerPage(writer, ctrl as Page, ctrl.Children, config);
                    }

                    if (ctrl is SimpleEdit)
                        SerSimpleEdit(ctrl as SimpleEdit, writer, config);

                    if (ctrl is aetp.Common.ControlModel.MemoEdit)
                        SerMemoEdit(ctrl as aetp.Common.ControlModel.MemoEdit, writer, config);
                    if (ctrl is SimpleRichEdit)
                        SerRichEdit(ctrl as SimpleRichEdit, writer, config);

                    if (ctrl is Grid)
                        SerGrid(ctrl as Grid, writer, config);

                    if (ctrl is GridAddedRow)
                        SerGridAddedRow(ctrl as GridAddedRow, writer, config);

                    if (ctrl is GridAddedPage)
                        SerGridAddedPage(ctrl as GridAddedPage, writer, config);

                    if (ctrl is aetp.Common.ControlModel.ComboBox)
                        SerComboBox(writer, ctrl, config);
                    if (ctrl is MetaGrid)
                        SerMetaGrid(ctrl as MetaGrid, writer, config);
                    if (ctrl is MetaConrol)
                    {
                        if ((ctrl as MetaConrol).Visible)
                            SerControls(writer, (ctrl as MetaConrol).Children, config);
                    }
                }
            }
            catch
            {
            }
        }

        void SerSimpleEdit(SimpleEdit ctrl, XmlWriter writer, EditorViewSerializerConfig config)
        {
            try
            {
                if (ctrl.EditValue != null && ctrl.EditValue.ToString() != String.Empty)
                {
                    // Сериализация чекбокса
                    if (ctrl.DataType == SimpleDataType.Bool)
                    {
                        //пока нужно проверять на единицу это среднее положение
                        if (ctrl.EditValue.Equals(1))
                            return;
                        WriteStartElement(writer, ctrl.SerializingTag, config);
                        if (ctrl.boolSerializationType == BoolSerializtionType.yesno)
                        {
                            YesNoClass yesno = new YesNoClass(config.namespace_av, (bool)ctrl.EditValue);
                            yesno.WriteXml(writer);
                        }
                        else
                        {
                            writer.WriteValue((bool)ctrl.EditValue);
                        }
                    }
                    else if (ctrl.DataType == SimpleDataType.DateTime)
                    {
                        WriteStartElement(writer, ctrl.SerializingTag, config);
                        writer.WriteValue(EditorViewSerializerHelper.DateToStr(ctrl.EditValue));
                    }
                    else
                    {
                        WriteStartElement(writer, ctrl.SerializingTag, config);
                        writer.WriteValue(ctrl.EditValue);
                    }
                    writer.WriteEndElement();
                }
            }
            catch
            {
            }
        }

        void SerMemoEdit(aetp.Common.ControlModel.MemoEdit ctrl, XmlWriter writer, EditorViewSerializerConfig config)
        {
            try
            {
                if (ctrl.EditValue != null && ctrl.EditValue.ToString() != String.Empty)
                {
                    WriteStartElement(writer, ctrl.SerializingTag, config);
                    writer.WriteValue(ctrl.EditValue);
                    writer.WriteEndElement();
                }
            }
            catch
            {
            }
        }

        void SerRichEdit(SimpleRichEdit ctrl, XmlWriter writer, EditorViewSerializerConfig config)
        {
            try
            {
                if (ctrl.EditValue != null && ctrl.EditValue.ToString() != String.Empty)
                {
                    string path = Path.Combine(aetp.Utils.Config.GetAppPath(), "temp");
                    if (!(Directory.Exists(path)))
                        Directory.CreateDirectory(path);
                    string filename = Path.Combine(path, "RichEditData.rtf");
                    var richEdit = new RichEditControl();
                    richEdit.Text = ctrl.EditValue.ToString();
                    richEdit.SaveDocument(filename, DocumentFormat.Rtf);

                    FileStream fs = new FileStream(filename,
                                                           FileMode.Open,
                                                           FileAccess.Read);
                    byte[] filebytes = new byte[fs.Length];
                    fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
                    string encodedData =
                        Convert.ToBase64String(filebytes,
                                               Base64FormattingOptions.InsertLineBreaks);
                    fs.Close();
                    WriteStartElement(writer, ctrl.SerializingTag, config);
                    writer.WriteValue(encodedData);
                    writer.WriteEndElement();
                }
            }
            catch
            {
            }
        }

        void SerGrid(Grid grid, XmlWriter writer, EditorViewSerializerConfig config)
        {
            try
            {
                WriteStartElement(writer, grid.SerializingTag, config);
                SerGridBody(grid, writer, config);
                writer.WriteEndElement();
            }
            catch
            {
            }
        }

        void SerGridBody(Grid grid, XmlWriter writer, EditorViewSerializerConfig config)
        {
            try
            {
                for (int i = 0; i < grid.Rows.Count; i++)
                {
                    try
                    {
                        WriteStartElement(writer, grid.Rows[i].SerializingTag, config);
                        for (int j = 0; j < grid.Columns.Count; j++)
                        {
                            try
                            {
                                object value = null;
                                object o = null;
                                //object o = cell.EditValue;
                                try
                                {
                                    o = grid.Data == null ? null : grid.Data.Rows[i][j] == null ? null : grid.Data.Rows[i][j].ToString();
                                }
                                catch
                                {
                                    o = null;
                                }
                                string str = o != null ? o.ToString() : String.Empty;
                                if (String.IsNullOrEmpty(str))
                                    continue;
                                WriteStartElement(writer, grid.Rows[i].Cells[j].SerializingTag, config);
                                if (grid.Rows[i].Cells[j].Type == GridCellType.List)
                                {
                                    if (!String.IsNullOrEmpty(str))
                                    {
                                        var item = (from y in grid.Rows[i].Cells[j].Source.Items where y.Code == str select y).FirstOrDefault();
                                        WriteDictionaryListItem(writer, item, config);
                                    }
                                    writer.WriteEndElement();
                                    continue;
                                }

                                switch (grid.Rows[i].Cells[j].Type)
                                {
                                    case GridCellType.String:
                                    case GridCellType.Label:
                                        value = str;
                                        break;
                                    case GridCellType.Check:
                                        if (grid.Rows[i].Cells[j].boolSerializationType == BoolSerializtionType.yesno)
                                        {
                                            YesNoClass yesno = new YesNoClass(config.namespace_av, str == "True");
                                            yesno.WriteXml(writer);
                                        }
                                        else
                                        {
                                            bool bres;
                                            if (Boolean.TryParse(str, out bres))
                                                writer.WriteValue(bres);
                                        }
                                        break;
                                    case GridCellType.Date:
                                        DateTime dtTemp;
                                        if (DateTime.TryParse(str, out dtTemp))
                                            value = EditorViewSerializerHelper.DateToStr(dtTemp);
                                        break;
                                    case GridCellType.Double:
                                        //double dres;
                                        //if (Double.TryParse(str, out dres))
                                        //    value = dres;
                                        str = str.Replace(" ", "");
                                        if (str.Contains(','))
                                            str = str.Replace(',', '.');
                                        value = str;
                                        break;
                                    case GridCellType.Integer:
                                        Int64 ires;
                                        if (Int64.TryParse(str, out ires))
                                            value = ires;
                                        break;
                                }

                                if (grid.Rows[i].Cells[j].Type != GridCellType.Check && value != DBNull.Value && value != null)
                                    writer.WriteValue(value);
                                writer.WriteEndElement();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                        writer.WriteEndElement();
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            catch
            {
            }
        }

        void SerGridAddedRow(GridAddedRow grid, XmlWriter writer, EditorViewSerializerConfig config)
        {
            try
            {
                WriteStartElement(writer, grid.SerializingTag, config);
                SerGridAddedRowBody(grid, writer, config);
                writer.WriteEndElement();
            }
            catch
            {
            }
        }

        //сериализует без обертывания в корневой тег
        void SerGridAddedRowBody(GridAddedRow grid, XmlWriter writer, EditorViewSerializerConfig config)
        {
            try
            {
                if (grid.Data == null) return;
                for (int r = 0; r < grid.Data.Rows.Count; r++)
                {
                    try
                    {
                        WriteStartElement(writer, grid.SerializingRowTag, config);
                        int c = 0;
                        foreach (GridAddedRowColumn col in grid.Columns)
                        {
                            try
                            {
                                object value = null;
                                object o = grid.Data.Rows[r][c];
                                string str = o != null ? o.ToString() : String.Empty;

                                if (String.IsNullOrEmpty(str))
                                {
                                    c++;
                                    continue;
                                }

                                WriteStartElement(writer, col.SerializingTag, config);
                                if (col.Type == GridCellType.List)
                                {
                                    if (!String.IsNullOrEmpty(str))
                                    {
                                        var item = (from i in col.Source.Items where i.Code == str select i).FirstOrDefault();
                                        if (item != null)
                                        {
                                            WriteDictionaryListItem(writer, item, config);
                                        }
                                    }
                                    writer.WriteEndElement();
                                    c++;
                                    continue;
                                }
                                switch (col.Type)
                                {
                                    case GridCellType.String:
                                    case GridCellType.Label:
                                        value = str;
                                        break;
                                    case GridCellType.Check:
                                        if (col.boolSerializationType == BoolSerializtionType.yesno)
                                        {
                                            YesNoClass yesno = new YesNoClass(config.namespace_av, str == "True");
                                            yesno.WriteXml(writer);
                                        }
                                        else
                                        {
                                            bool bres;
                                            if (Boolean.TryParse(str, out bres))
                                                writer.WriteValue(bres);
                                        }
                                        break;
                                    case GridCellType.Date:
                                        DateTime dtTemp;
                                        if (DateTime.TryParse(str, out dtTemp))
                                            value = EditorViewSerializerHelper.DateToStr(dtTemp);
                                        break;
                                    case GridCellType.Double:
                                        double dres;
                                        if (Double.TryParse(str, out dres))
                                            value = dres;
                                        break;
                                    case GridCellType.Integer:
                                        Int64 ires;
                                        if (Int64.TryParse(str, out ires))
                                            value = ires;
                                        break;
                                }

                                if (col.Type != GridCellType.Check && value != DBNull.Value && value != null)
                                    writer.WriteValue(value);
                                writer.WriteEndElement();
                                c++;
                            }
                            catch
                            {
                            }
                        }
                        writer.WriteEndElement();
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }
        }

        void SerGridAddedPage(GridAddedPage grid, XmlWriter writer, EditorViewSerializerConfig config)
        {
            try
            {
                WriteStartElement(writer, grid.SerializingTag, config);
                for (int i = 1; i < grid.Children.Count; i++)
                {
                    SerPage(writer, grid.Children[i] as Page, grid.Children[i].Children, config);
                }
                writer.WriteEndElement();
            }
            catch
            {
            }
        }

        void SerMetaGrid(MetaGrid grid, XmlWriter writer, EditorViewSerializerConfig config)
        {
            try
            {
                WriteStartElement(writer, grid.SerializingTag, config);
                foreach (BaseGrid children in grid.Tables)
                {
                    if (children is Grid)
                        SerGridBody(children as Grid, writer, config);
                    else if (children is GridAddedRow)
                        SerGridAddedRowBody(children as GridAddedRow, writer, config);
                }
                writer.WriteEndElement();
            }
            catch
            {
            }
        }

        private void SerComboBox(XmlWriter writer, aetp.Common.ControlModel.BaseControl ctrl, EditorViewSerializerConfig config)
        {
            try
            {
                if (ctrl.EditValue != null && ctrl.EditValue != DBNull.Value)
                {
                    WriteStartElement(writer, ctrl.SerializingTag, config);
                    DictionaryListItem item = ctrl.EditValue as DictionaryListItem;
                    if (item == null && (ctrl as aetp.Common.ControlModel.ComboBox).Items.Count > 0 &&
                        ctrl.EditValue is aetpComboBoxItem)
                    {
                        var comboItem = ctrl.EditValue as aetpComboBoxItem;
                        item = (ctrl as aetp.Common.ControlModel.ComboBox).Source.Items.Where(x => x.Code == comboItem.ID).FirstOrDefault();
                    }
                    WriteDictionaryListItem(writer, item, config);
                    writer.WriteEndElement();
                }
            }
            catch
            {
            }
        }

        #endregion

        #region deserializing

        void DeserPage(XmlReader reader, Page page, List<aetp.Common.ControlModel.BaseControl> controls, EditorViewSerializerConfig config)
        {
            try
            {
                bool IsEmpty = reader.IsEmptyElement;
                string correctName = getCorrectName(page.SerializingTag);
                //если страница не сохранена то и нах ее
                if (reader.LocalName != correctName)
                {
                    if (!page.IsMandatoryToFill) page.IsShownPanel = false;
                    return;
                }
                if (!alternateSerializer.Desereilize(reader, page))
                {
                    reader.ReadStartElement(correctName, config.namespace_av);

                    if (IsEmpty)
                    {
                        if (!page.IsMandatoryToFill) page.IsShownPanel = false;
                        return;
                    }
                    page.IsShownPanel = true;
                    if (page is Report)
                    {
                        if ((page as Report).AttachFile)
                        {
                            DeserFile(reader, page);
                        }
                    }
                    try
                    {

                        DeserControls(reader, page, controls, config);
                    }
                    catch
                    {
                    }
                    reader.ReadEndElement();
                }
            }
            catch
            {
            }
        }

        void DeserFile(XmlReader reader, Page page)
        {            
                List<FileList> list = new List<FileList> { };
                bool IsEmpty = reader.IsEmptyElement;

                if (reader.LocalName == "Files")
                {
                    reader.ReadStartElement();
                    while (reader.LocalName == "File")
                    {
                        string s = reader.GetAttribute(0).ToString();
                        reader.ReadStartElement();                        
                        IsEmpty = reader.IsEmptyElement;
                        if (!IsEmpty)
                        {
                            list.Add(new FileList(s, reader.ReadContentAsString(), reader.LocalName));
                        }                        
                        reader.ReadEndElement();                       
                    }
                    reader.ReadEndElement();
                }
                (page as Report).File = list;            
        }

        void DeserControls(XmlReader reader, Page page, List<aetp.Common.ControlModel.BaseControl> controls, EditorViewSerializerConfig config)
        {
            try
            {
                foreach (aetp.Common.ControlModel.BaseControl ctrl in controls)
                {
                    try
                    {
                        if (ctrl is Link)
                            continue;
                        if (ctrl is Page)
                            DeserPage(reader, ctrl as Page, ctrl.Children, config);
                        else if (ctrl is MetaConrol)
                        {
                            DeserControls(reader, page, (ctrl as MetaConrol).Children, config);
                        }
                        else
                        {
                            //пропускаем элементы если нужных тегов нет в отчете
                            if ((reader.LocalName != ctrl.SerializingTag) && (reader.LocalName != ctrl.AlternativeSerializingTag))
                                continue;
                        }
                        if (ctrl is SimpleEdit)
                            DeserSimpleEdit(reader, ctrl, config);
                        if (ctrl is SimpleRichEdit)
                            DeserSimpleRichEdit(reader, ctrl as SimpleRichEdit);
                        if (ctrl is aetp.Common.ControlModel.MemoEdit)
                            DeserMemoEdit(reader, page, ctrl);

                        if (ctrl is Grid)
                            DeserGrid(reader, ctrl, config);

                        if (ctrl is GridAddedRow)
                            DeserGridAddedRow(reader, ctrl, config);

                        if (ctrl is GridAddedPage)
                        {
                            DeserGridAddedPage(reader, ctrl, config);
                        }
                        if (ctrl is aetp.Common.ControlModel.ComboBox)
                        {
                            DeserComboBox(reader, ctrl, config);
                        }
                        if (ctrl is MetaGrid)
                        {
                            DeserMetaGrid(reader, ctrl, config);
                        }
                        else
                        {
                            continue;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }
        }

        private void DeserComboBox(XmlReader reader, aetp.Common.ControlModel.BaseControl editor, EditorViewSerializerConfig config)
        {
            try
            {
                bool IsEmpty = reader.IsEmptyElement;
                if (reader.LocalName != editor.SerializingTag)
                    return;
                else
                    reader.ReadStartElement();

                if (IsEmpty)
                {
                    editor.EditValue = null;
                    return;
                }

                DictionaryListItem item = ReadDictionaryListItem(reader, config);
                var currentItem = (editor as aetp.Common.ControlModel.ComboBox).Source.Items.Where(x => x.Code == item.Code).SingleOrDefault();
                if (currentItem == null)
                {
                    editor.EditValue = null;
                }
                else
                {
                    editor.EditValue = new aetpComboBoxItem() { ID = currentItem.Code, Name = currentItem.Description };
                }
                reader.ReadEndElement();
            }
            catch
            {
            }
        }

        private void DeserGridAddedPage(XmlReader reader, aetp.Common.ControlModel.BaseControl editor, EditorViewSerializerConfig config)
        {
            try
            {

                bool IsEmpty = reader.IsEmptyElement;
                if (reader.LocalName != editor.SerializingTag)
                    return;
                else
                    reader.ReadStartElement();
                if (IsEmpty)
                    return;
                GridAddedPage grid = editor as GridAddedPage;

                int i = 1;
                DataTable dt = new DataTable();
                foreach (GridAddedPageColumn col in grid.Columns)
                {
                    dt.Columns.Add(col.FieldName);
                }
                grid.Data = dt;
                while (reader.IsStartElement() && reader.LocalName == grid.Children[0].SerializingTag)
                {
                    grid.NewPage();
                    DeserPage(reader, grid.Children[i] as Page, grid.Children[i].Children, config);
                    i++;
                }

                reader.ReadEndElement();
            }
            catch
            {
            }
        }

        private void DeserGridAddedRow(XmlReader reader, aetp.Common.ControlModel.BaseControl ctrl, EditorViewSerializerConfig config)
        {
            try
            {
                bool IsEmpty = reader.IsEmptyElement;
                if (reader.LocalName != ctrl.SerializingTag)
                    return;
                else
                    reader.ReadStartElement();
                if (IsEmpty)
                    return;
                DeserGridAddedRowBody(reader, ctrl, config);
                reader.ReadEndElement();
            }
            catch
            {
                reader.ReadEndElement();
            }
        }

        private void DeserGridAddedRowBody(XmlReader reader, aetp.Common.ControlModel.BaseControl ctrl, EditorViewSerializerConfig config)
        {
            try
            {
                GridAddedRow grid = ctrl as GridAddedRow;
                if (grid.Data == null)
                {
                    grid.Data = new DataTable();
                }
                else
                {
                    grid.Data.Columns.Clear();
                    grid.Data.Rows.Clear();
                }

                foreach (var col in grid.Columns)
                {
                    grid.Data.Columns.Add(new DataColumn(col.Name, col.GetType()));
                }

                int r = -1;
                while (reader.IsStartElement() && reader.LocalName == grid.SerializingRowTag)
                {
                    r++;
                    DataRow dr = grid.Data.NewRow();
                    grid.Data.Rows.Add(dr);
                    bool empty = reader.IsEmptyElement;
                    reader.ReadStartElement(grid.SerializingRowTag, config.namespace_av);
                    if (empty)
                        continue;

                    foreach (GridAddedRowColumn col in grid.Columns)
                    {
                        try
                        {
                            if (col.Type == GridCellType.Label)
                            {
                                dr[col.Name] = col.TextLabel;
                            }
                            if (reader.LocalName != col.SerializingTag)
                            {
                                continue;
                            }
                            object value = DBNull.Value;
                            if (!reader.IsEmptyElement)
                            {
                                if (col.Type == GridCellType.List)
                                {
                                    if (!reader.IsEmptyElement)
                                    {
                                        reader.ReadStartElement();
                                        dr[col.Name] = ReadDictionaryListItem(reader, config).Code;
                                        reader.ReadEndElement();
                                        continue;
                                    }
                                    else
                                        reader.ReadStartElement();
                                }
                                switch (col.Type)
                                {
                                    case GridCellType.String:
                                    case GridCellType.Label:
                                        value = reader.ReadElementContentAsString();
                                        break;
                                    case GridCellType.Check:
                                        if (col.boolSerializationType == BoolSerializtionType.yesno)
                                        {
                                            YesNoClass yesno = new YesNoClass(config.namespace_av);
                                            yesno.ReadXml(reader);
                                            value = (bool)yesno.EditValue;
                                        }
                                        else
                                        {
                                            if (!reader.IsEmptyElement)
                                                value = reader.ReadElementContentAsBoolean();
                                            else
                                                reader.ReadStartElement();
                                        }
                                        break;
                                    case GridCellType.Date:
                                        if (!reader.IsEmptyElement)
                                            value = reader.ReadElementContentAsDateTime();
                                        else
                                            reader.ReadStartElement();
                                        break;
                                    case GridCellType.Double:
                                        if (!reader.IsEmptyElement)
                                            try
                                                {
                                                    value = reader.ReadElementContentAsDouble();
                                                }
                                                catch
                                                {
                                                    reader.ReadStartElement();
                                                }
                                            
                                        else
                                            reader.ReadStartElement();
                                        break;
                                    case GridCellType.Integer:
                                        if (!reader.IsEmptyElement)
                                            value = reader.ReadElementContentAsLong();
                                        else
                                            reader.ReadStartElement();
                                        break;
                                }
                            }
                            else
                                reader.Read();
                            if (col.Type != GridCellType.Label)
                            {
                                dr[col.Name] = value;
                            }
                        }
                        catch
                        {
                            reader.Read();
                        }

                    }

                    try
                    {
                        reader.ReadEndElement();
                    }
                    catch
                    {
                        reader.ReadElementContentAsString();
                        reader.ReadEndElement();
                    }
                }
                grid.Data.AcceptChanges();
            }
            catch
            {
                reader.ReadEndElement();
            }
        }

        private void DeserGrid(XmlReader reader, aetp.Common.ControlModel.BaseControl ctrl, EditorViewSerializerConfig config)
        {
            try
            {
                bool IsEmpty = reader.IsEmptyElement;
                if (reader.LocalName != getCorrectName(ctrl.SerializingTag))
                    return;
                else
                    reader.ReadStartElement();
                if (IsEmpty)
                    return;
                prefix = reader.LocalName;
                DeserGridBody(reader, ctrl,config);
                reader.ReadEndElement();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "err");
                reader.ReadEndElement();
            }
        }
        public string prefix = string.Empty;

        private void DeserGridBody(XmlReader reader, aetp.Common.ControlModel.BaseControl ctrl, EditorViewSerializerConfig config)
        { 
            try
            {
                Grid grid = ctrl as Grid;
                int r = 0;
                grid.Data = new DataTable();
                foreach (var col in grid.Columns)
                {
                    grid.Data.Columns.Add(col.Name);
                }
                foreach (aetp.Common.ControlModel.GridRow row in grid.Rows)
                {
                    try
                    {
                        bool empty = reader.IsEmptyElement;
                        reader.ReadStartElement(row.SerializingTag, config.namespace_av);
                        if (empty)
                        {
                            foreach (var cell in grid.Rows[r].Cells)
                            {
                                try
                                {
                                    if (cell.Type == GridCellType.Label)
                                    {
                                        cell.EditValue = cell.Caption;
                                    }
                                    else
                                    {
                                        cell.EditValue = null;
                                    }
                                }
                                catch
                                {
                                }
                            }
                            var empty_row_items = grid.Rows[r].Cells.Select<GridCellItem, object>(x =>
                            {
                                if (x.Type == GridCellType.Label) return x.Caption;
                                else return DBNull.Value;
                            }).ToArray();
                            grid.Data.Rows.Add(empty_row_items);
                            r++;
                            continue;
                        }
                        int c = -1;
                        foreach (GridCellItem cell in row.Cells)
                        {
                            try
                            {
                                c++;
                                if (reader.LocalName != cell.SerializingTag)
                                    continue;

                                //if (!String.IsNullOrEmpty((cell.FormulaForCalculating ?? "").Trim()))
                                //{
                                //    reader.ReadElementContentAsString();
                                //    continue;
                                //}
                                object value = null;

                                if (!reader.IsEmptyElement)
                                {
                                    switch (cell.Type)
                                    {
                                        case GridCellType.String:
                                        case GridCellType.Label:
                                            value = reader.ReadElementContentAsString();
                                            break;
                                        case GridCellType.Check:
                                            if (cell.boolSerializationType == BoolSerializtionType.yesno)
                                            {
                                                YesNoClass yesno = new YesNoClass(config.namespace_av);
                                                yesno.ReadXml(reader);
                                                value = (bool)yesno.EditValue;
                                            }
                                            else
                                            {
                                                if (!reader.IsEmptyElement)
                                                    value = reader.ReadElementContentAsBoolean();
                                                else
                                                    reader.ReadStartElement();
                                            }
                                            break;
                                        case GridCellType.Date:
                                            if (!reader.IsEmptyElement)
                                                value = reader.ReadElementContentAsDateTime();
                                            else
                                                reader.ReadStartElement();
                                            break;
                                        case GridCellType.Double:
                                            if (!reader.IsEmptyElement)
                                                try
                                                {
                                                    value = reader.ReadElementContentAsDouble();
                                                }
                                                catch
                                                {
                                                    reader.ReadEndElement();
                                                    reader.ReadStartElement();
                                                }
                                            else
                                                reader.ReadStartElement();
                                            break;
                                        case GridCellType.Integer:
                                            if (!reader.IsEmptyElement)
                                                value = reader.ReadElementContentAsLong();
                                            else
                                                reader.ReadStartElement();
                                            break;
                                        case GridCellType.List:
                                            if (!reader.IsEmptyElement)
                                            {
                                                reader.ReadStartElement();
                                                value = ReadDictionaryListItem(reader, config).Code;
                                                reader.ReadEndElement();
                                            }
                                            else
                                                reader.ReadStartElement();
                                            break;

                                    }
                                }
                                else
                                    reader.Read();

                                if (cell.Type != GridCellType.Label)
                                {
                                    grid.Rows[r].Cells[c].EditValue = value;
                                }
                                else
                                {
                                    grid.Rows[r].Cells[c].EditValue = grid.Rows[r].Cells[c].Caption;
                                }
                            }
                            catch
                            {
                                //reader.Read();
                                //reader.MoveToElement();
                            }
                        }
                        var items = grid.Rows[r].Cells.Select(x =>
                        {
                            if (x.Type == GridCellType.Label) return x.Caption;
                            else return x.EditValue;
                        }).ToArray();
                        grid.Data.Rows.Add(items);
                        r++;
                        try
                        {
                            reader.ReadEndElement();
                        }
                        catch
                        {
                            reader.ReadElementContentAsString();
                            reader.ReadEndElement();
                        }
                    }
                    catch
                    {
                        //reader.ReadStartElement();
                        //reader.MoveToElement();
                    }

                }
                
            }
            catch (Exception ex)
            {
                error_node.Add(prefix + "\\" + reader.LocalName);
                //reader.ReadStartElement();
                //MessageBox.Show(ex.Message, "err");
            }
        }

        

        private void DeserMetaGrid(XmlReader reader, aetp.Common.ControlModel.BaseControl editor, EditorViewSerializerConfig config)
        {
            try
            {
                bool IsEmpty = reader.IsEmptyElement;

                if (reader.LocalName != editor.SerializingTag)
                    return;
                else
                    reader.ReadStartElement();
                prefix = reader.LocalName;
                if (IsEmpty)
                    return;
                foreach (BaseGrid children in (editor as MetaGrid).Tables)
                {
                    if (children is Grid)
                    {

                        DeserGridBody(reader, children, config);
                    }
                    else if (children is GridAddedRow)
                        DeserGridAddedRowBody(reader, children, config);
                }
                reader.ReadEndElement();
            }
            catch
            {
            }
        }

        private void DeserSimpleEdit(XmlReader reader, aetp.Common.ControlModel.BaseControl editor, EditorViewSerializerConfig config)
        {
            try
            {
                bool IsEmpty = reader.IsEmptyElement;
                if (IsEmpty)
                {
                    editor.EditValue = null;
                    return;
                }
                switch ((editor as SimpleEdit).DataType)
                {
                    case SimpleDataType.String:
                        editor.EditValue = reader.ReadElementContentAsString();
                        break;
                    case SimpleDataType.Bool:
                        if ((editor as SimpleEdit).boolSerializationType == BoolSerializtionType.yesno)
                        {
                            YesNoClass yesno = new YesNoClass(config.namespace_av);
                            yesno.ReadXml(reader);
                            editor.EditValue = (bool)yesno.EditValue;
                        }
                        else
                        {
                            editor.EditValue = reader.ReadElementContentAsBoolean();
                        }
                        break;
                    case SimpleDataType.DateTime:
                        reader.ReadStartElement();
                        if (reader.Value == string.Empty)
                        {
                            editor.EditValue = null;
                        }
                        else
                        {
                            editor.EditValue = reader.ReadContentAsDateTime();
                        }
                        reader.ReadEndElement();
                        break;
                    case SimpleDataType.Double:
                        editor.EditValue = reader.ReadElementContentAsDouble();
                        break;
                    case SimpleDataType.Integer:
                        editor.EditValue = reader.ReadElementContentAsLong();
                        break;
                }
            }
            catch
            {
            }
        }

        private void DeserMemoEdit(XmlReader reader, Page page, aetp.Common.ControlModel.BaseControl editor)
        {
            try
            {
                bool IsEmpty = reader.IsEmptyElement;
                if (IsEmpty)
                {
                    editor.EditValue = null;
                    return;
                }
                string s;
                s = reader.ReadElementContentAsString();
                char[] massiv = s.ToCharArray();
                string memoValue = s.Replace("\n", "\r\n");
                editor.EditValue = memoValue;
            }
            catch
            {
            }
        }

        private void DeserSimpleRichEdit(XmlReader reader, SimpleRichEdit editor)
        {
            try
            {
                bool IsEmpty = reader.IsEmptyElement;
                if (IsEmpty)
                {
                    editor.EditValue = null;
                    return;
                }
                string s;
                s = reader.ReadElementContentAsString();

                string path = Path.Combine(aetp.Utils.Config.GetAppPath(), "temp");
                if (!(Directory.Exists(path)))
                    Directory.CreateDirectory(path);
                string filename = Path.Combine(path, "RichEditData.rtf");

                byte[] filebytes = Convert.FromBase64String(s);
                FileStream fs = new FileStream(filename,
                                       FileMode.Create,
                                       FileAccess.Write,
                                       FileShare.None);
                fs.Write(filebytes, 0, filebytes.Length);
                fs.Close();
                var richEdit = new RichEditControl();
                richEdit.LoadDocument(filename, DocumentFormat.Rtf);
                editor.EditValue = richEdit.Text;
            }
            catch
            {
            }
        }

        #endregion

        #endregion

    }
}
