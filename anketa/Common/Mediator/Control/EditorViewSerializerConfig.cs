﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using aetp.Common.ControlModel.TypeConverters;

namespace aetp.Common.Mediator
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class EditorViewSerializerConfig
    {
        
        public Encoding GetEncoding ()
        {
            switch (Encode)
            {
                case EncodingStr.UTF8:
                    return Encoding.UTF8;
                case EncodingStr.Win1251:
                    return Encoding.GetEncoding(1251);
                default:
                    return Encoding.UTF8;
            }   
        }
        [DisplayName("Версия")]
        public string appVersion { get; set; }
        [DisplayName("Описание")]
        public string appDescription { get; set; }
        [DisplayName("Пространство имен xserializer")]
        public string namespace_xserializer { get; set; }
        [DisplayName("Пространство имен xsi")]
        public string namespace_xsi { get; set; }
        [Browsable(false)] // выставляется из значения Model поэтому скрываю
        [DisplayName("Пространство имен av")]
        public string namespace_av { get; set; }
        [Browsable(false)]
        public string namespace_av_prefix { get; set; }
        [DisplayName("Пространство имен schemaLocation")]
        [Browsable(false)] // выставляется из значения Model поэтому скрываю
        public string namespace_schemaLocation { get; set; }
        [DisplayName("Кодировка")]
        [TypeConverter(typeof(EnumTypeConverter))]
        public EncodingStr Encode { get; set; }

        Dictionary<AiTiModelType, string[]> namespaces = new Dictionary<AiTiModelType, string[]>()
            { 
                { AiTiModelType.ЗПУ,    new string[] { "http://www.it.ru/Schemas/Avior/ЗПУ",    "http://www.it.ru/Schemas/Avior/ЗПУ ЗПУ.xsd" }},
                { AiTiModelType.ПУРЦБ,  new string[] { "http://www.it.ru/Schemas/Avior/ПУРЦБ",  "http://www.it.ru/Schemas/Avior/ПУРЦБ ПУРЦБ.xsd" }},
                { AiTiModelType.УРКИ,   new string[] { "http://www.it.ru/Schemas/Avior/УРКИ",   "http://www.it.ru/Schemas/Avior/УРКИ УРКИ.xsd" }},
                { AiTiModelType.УСИО,   new string[] { "http://www.it.ru/Schemas/Avior/УСИО",   "http://www.it.ru/Schemas/Avior/УСИО УСИО.xsd" }},
                { AiTiModelType.Регистраторы,   new string[] { "http://www.it.ru/Schemas/Avior/Регистраторы",   "http://www.it.ru/Schemas/Avior/Регистраторы Регистраторы.xsd" }},
                { AiTiModelType.УММ,   new string[] { "http://www.it.ru/Schemas/Avior/УММ",   "http://www.it.ru/Schemas/Avior/УММ УММ.xsd" }},
                { AiTiModelType.МФО,   new string[] { "http://www.it.ru/Schemas/Avior/МФО",   "http://www.it.ru/Schemas/Avior/МФО МФО.xsd" }},
                { AiTiModelType.АЛ,   new string[] { "http://www.it.ru/Schemas/Avior/АЛ",   "http://www.it.ru/Schemas/Avior/АЛ АЛ.xsd" }},
                { AiTiModelType.ЛКИ,   new string[] { "http://www.it.ru/Schemas/Avior/ЛКИ",   "http://www.it.ru/Schemas/Avior/ЛКИ ЛКИ.xsd" }},
                { AiTiModelType.ПФР,   new string[] { "http://www.it.ru/Schemas/Avior/ПФР",   "http://www.it.ru/Schemas/Avior/ПФР ПФР.xsd" }}
            };

        
        AiTiModelType _model;
        [DisplayName("Модель")]
        public AiTiModelType Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value;
                namespace_av = namespaces[value][0];
                namespace_schemaLocation = namespaces[value][1];
            }
        }
        
        public EditorViewSerializerConfig()
        {
            Model = AiTiModelType.ЗПУ;
            Encode = EncodingStr.UTF8;
            appVersion = "2.16.2";
            appDescription = "";
            namespace_xserializer = "http://www.it.ru/Schemas/Avior/XSerializer";
            namespace_xsi = "http://www.w3.org/2001/XMLSchema-instance";
            namespace_av_prefix = "av";
        }
    }

    public enum AiTiModelType
    {
        ЗПУ = 0,
        ПУРЦБ,
        УСИО,
        УРКИ,
        Регистраторы,
        УММ,
        МФО,
        АЛ,
        ЛКИ,
        ПФР
    }

    public enum EncodingStr
    {
        [Description("UTF-8")]
        UTF8 = 0,
        [Description("Windows-1251")]
        Win1251
    }
}
