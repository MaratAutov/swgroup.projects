﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Diagnostics;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using System.Data;
using aetp.Common.View;
using aetp.Common.ControlModel;
using aetp.Common.Control;
using System.IO;
using aetp.Common.Serializer;
using DevExpress.XtraGrid;
using aetp.Utils;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.Export;
using DevExpress.XtraRichEdit.Services;
using DevExpress.XtraRichEdit.API.Native;
using System.Reflection;


namespace aetp.Common.Mediator
{
    
    public class EditorViewSerializer
    {
        public static EditorViewSerializerConfig config;
        EditorViewAlternateSerializer alternateSerializer; 

        public EditorViewSerializer()
        {
            
        }

        #region Public methods
        
        public void Serialize(XmlWriter writer, Page report)
        {
            EditorViewSerializerConfig config = new EditorViewSerializerConfig();
            Serialize(writer, report, config);
        }        

        public void Serialize(XmlWriter writer, Page report, EditorViewSerializerConfig config)
        {
            EditorViewSerializer.config = config;
            
            SerPage(writer, report, report.Children);
            
            writer.Flush();
            writer.Close();
        }
                
        public Report Deserialize(string FileName, EditorMediator editor)
        {
            alternateSerializer = new EditorViewAlternateSerializer(FileName);
            Report report = FindModel(new FileInfo(FileName));
            if (report != null)
            {
                report.InitSouceMetaControlHandler();
                report.InitialDefaultValues();
                EditorMediator.dependantControls.Init(report);
                report.RunMode = ViewType.Editor;
                EditorViewSerializer.config = report.SerialaizerConfig;
                Deserialize(CreateXmlReader(FileName), report, FileName);
            }
            return report;
        }

        //return reader with position in Report
        XmlReader CreateXmlReader(string fileName)
        {
            XmlReader reader = XmlReader.Create(fileName, new XmlReaderSettings()
            {
                IgnoreComments = true,
                IgnoreWhitespace = true,
                IgnoreProcessingInstructions = true
            });
            while (reader.NodeType != XmlNodeType.Element)
            {
                reader.Read();
            }
            return reader;
        }

        public void Deserialize(XmlReader reader, Page report, string fileName)
        {
            if (alternateSerializer == null)
            {
                alternateSerializer = new EditorViewAlternateSerializer(fileName);
            }
            try
            {
                DeserPage(reader, report, report.Children);                
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show("Данный документ не может быть открыт, так как модель данных не соответствует отчету", "Ошибка");
            }
        }

        public void Deserialize(string fileName, Page report)
        {
            if (alternateSerializer == null)
            {
                alternateSerializer = new EditorViewAlternateSerializer(fileName);
            }
            try
            {
                DeserPage(CreateXmlReader(fileName), report, report.Children);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show("Данный документ не может быть открыт, так как модель данных не соответствует отчету", "Ошибка");
            }
        }

        #endregion

        #region private methods

        #region find model

        Report FindModel(FileInfo file)
        {
            XmlReader reader = CreateXmlReader(file.FullName);
                        
            string reportName = reader.LocalName;
            string reportVersion = reader.GetAttribute("appVersion");
            
            reader.Close();

            FormSerializer serializer = new FormSerializer();
            Report report;
            string reportPath = string.Empty;
            
            reportPath = AppGlobal.Instance.Config.ListModelInfo.Find(new ModelConfig.ModelInfoSearcher(reportName, "", reportVersion,reportName).Condition_Save).Path;
            
            if (!File.Exists(reportPath))
            {                
                MessageBox.Show("Данный документ не может быть открыт, так как не удалось найти соответсвующую модель данных", "Ошибка");
                report = null;
            }
            else
            {
                using (XmlTextReader reportReader = new XmlTextReader(reportPath))
                {
                    report = new FormSerializer().Deserialize(reportReader, true);
                }
            }
            return report;
        }
        
        #endregion

        #region utils function

        ScrollableControl GetPage(IEditorWindow form, Page page)
        {
            return form.GetPage(page);
        }

        //удаляет ненужные символы из имени страниц
        public static string getCorrectName(string name)
        {
            string _name = name;
            int starts  = _name.IndexOf('$');
            if (starts != -1)
            {
                _name = _name.Remove(starts);
            }
            return _name;
        }

        void WriteStartElement(XmlWriter writer, string localName)
        {
            writer.WriteStartElement(config.namespace_av_prefix, localName, config.namespace_av);
        }

        //записывает элемент словаря в xmlwriter
        void WriteDictionaryListItem(XmlWriter writer, DictionaryListItem item)
        {
            WriteStartElement(writer, DictionaryListItem.Code_tag);
            writer.WriteValue(item.Code);
            writer.WriteEndElement();
            WriteStartElement(writer, DictionaryListItem.Description_tag);
            writer.WriteValue(item.Description);
            writer.WriteEndElement();
            WriteStartElement(writer, DictionaryListItem.Active_tag);
            writer.WriteValue(item.Active);
            writer.WriteEndElement();
        }

        public static DictionaryListItem ReadDictionaryListItem(XmlReader reader)
        {
            string code = reader.ReadElementContentAsString(DictionaryListItem.Code_tag, config.namespace_av);
            string description = "";
            if (reader.LocalName == DictionaryListItem.Description_tag)
                description = reader.ReadElementContentAsString(DictionaryListItem.Description_tag, config.namespace_av);
            bool active = true;
            if (reader.LocalName == DictionaryListItem.Active_tag)
                active = reader.ReadElementContentAsBoolean(DictionaryListItem.Active_tag, config.namespace_av);
            return new DictionaryListItem() { Code = code, Description = description, Active = active };
        }

        #endregion

        #region serializing

        protected void SerPage(XmlWriter writer, Page page, List<aetp.Common.ControlModel.BaseControl> controls)
        {
            //если страницы нет то нефиг ее и сериализовать
            if (!page.IsShownPanel || !page.tlItem.Visible)
                return;
            
            if (page is Report)
            {
                writer.WriteStartElement(config.namespace_av_prefix, page.SerializingTag, config.namespace_av);
                writer.WriteAttributeString("appVersion", config.appVersion);
                //namespaces add
                writer.WriteAttributeString("xmlns", "xserializer", null, config.namespace_xserializer);
                writer.WriteAttributeString("externalId", (page as Report).guid);
                writer.WriteAttributeString("xmlns", "xsi", null, config.namespace_xsi);
                writer.WriteAttributeString("xmlns", "av", null, config.namespace_av);
                writer.WriteAttributeString("xsi", "schemaLocation", null, config.namespace_schemaLocation);

                if ((page as Report).AttachFile)
                {
                    if ((page as Report).File.Count > 0)
                    {
                        WriteStartElement(writer, "Files");

                        for (int i = 0; i < (page as Report).File.Count; i++)
                        {
                            WriteStartElement(writer, (page as Report).File[i].Tag);
                            writer.WriteAttributeString("Name", (page as Report).File[i].Name);
                            writer.WriteValue((page as Report).File[i].Data);                            
                            writer.WriteEndElement();                           
                        }
                        writer.WriteEndElement(); 
                    }
                }
            }
            else
                WriteStartElement(writer, getCorrectName(page.SerializingTag));
            SerControls(writer, controls);
            
            writer.WriteEndElement();
        }

        void SerControls(XmlWriter writer, List<aetp.Common.ControlModel.BaseControl> controls)
        {
            foreach (aetp.Common.ControlModel.BaseControl ctrl in controls)
            {
                if (ctrl is Link)
                    continue;

                if (ctrl is Page)
                {
                    SerPage(writer, ctrl as Page, ctrl.Children);
                }
                
                if (ctrl is SimpleEdit)
                    SerSimpleEdit(ctrl as SimpleEdit, writer);
                
                if (ctrl is aetp.Common.ControlModel.MemoEdit)
                    SerMemoEdit(ctrl as aetp.Common.ControlModel.MemoEdit, writer);
                if (ctrl is SimpleRichEdit)
                    SerRichEdit(ctrl as SimpleRichEdit, writer);
                
                if (ctrl is Grid)
                    SerGrid(ctrl as Grid, writer);
                
                if (ctrl is GridAddedRow)
                    SerGridAddedRow(ctrl as GridAddedRow, writer);
                
                if (ctrl is GridAddedPage)
                    SerGridAddedPage(ctrl as GridAddedPage, writer);
                
                if (ctrl is aetp.Common.ControlModel.ComboBox)
                    SerComboBox(writer, ctrl);
                if (ctrl is MetaGrid)
                    SerMetaGrid(ctrl as MetaGrid, writer);
                if (ctrl is MetaConrol)
                {
                    if ((ctrl as MetaConrol).Visible)
                        SerControls(writer, (ctrl as MetaConrol).Children);
                }
            }
        }

        void SerSimpleEdit(SimpleEdit ctrl, XmlWriter writer)
        {
            
            if (ctrl.EditValue != null && ctrl.EditValue.ToString() != String.Empty)
            {
                // Сериализация чекбокса
                if (ctrl.DataType == SimpleDataType.Bool)
                {
                    //пока нужно проверять на единицу это среднее положение
                    if (ctrl.EditValue.Equals(1))
                        return;
                    WriteStartElement(writer, ctrl.SerializingTag);
                    if (ctrl.boolSerializationType == BoolSerializtionType.yesno)
                    {
                        YesNoClass yesno = new YesNoClass(config.namespace_av, (bool)ctrl.EditValue);
                        yesno.WriteXml(writer);
                    }
                    else
                    {
                        writer.WriteValue((bool)ctrl.EditValue);
                    }
                }
                else if (ctrl.DataType == SimpleDataType.DateTime)
                {
                    WriteStartElement(writer, ctrl.SerializingTag);
                    writer.WriteValue(EditorViewSerializerHelper.DateToStr(ctrl.EditValue));
                }
                else
                {
                    WriteStartElement(writer, ctrl.SerializingTag);
                    writer.WriteValue(ctrl.EditValue);
                }
                writer.WriteEndElement();
            }
        }       

        void SerMemoEdit(aetp.Common.ControlModel.MemoEdit ctrl, XmlWriter writer)
        {
            if (ctrl.EditValue != null && ctrl.EditValue.ToString() != String.Empty)
            {
                WriteStartElement(writer, ctrl.SerializingTag);
                writer.WriteValue(ctrl.EditValue);               
                writer.WriteEndElement();
            }
        }

        void SerRichEdit(SimpleRichEdit ctrl, XmlWriter writer)
        {
            if (ctrl.EditValue != null && ctrl.EditValue.ToString() != String.Empty)
            {
                string path = Path.Combine(aetp.Utils.Config.GetAppPath(), "temp");
                if (!(Directory.Exists(path)))
                    Directory.CreateDirectory(path);
                string filename = Path.Combine(path, "RichEditData.rtf");
                var richEdit = new RichEditControl();
                richEdit.Text = ctrl.EditValue.ToString();
                richEdit.SaveDocument(filename, DocumentFormat.Rtf);

                FileStream fs = new FileStream(filename,
                                                       FileMode.Open,
                                                       FileAccess.Read);
                byte[] filebytes = new byte[fs.Length];
                fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
                string encodedData =
                    Convert.ToBase64String(filebytes,
                                           Base64FormattingOptions.InsertLineBreaks);
                fs.Close();
                WriteStartElement(writer, ctrl.SerializingTag);
                writer.WriteValue(encodedData);
                writer.WriteEndElement();
            }
        }

        void SerGrid(Grid grid, XmlWriter writer)
        {
            WriteStartElement(writer, grid.SerializingTag);
            SerGridBody(grid, writer);
            writer.WriteEndElement();
        }

        void SerGridBody(Grid grid, XmlWriter writer)
        {
            for (int i = 0; i < grid.Rows.Count; i++)
            {
                WriteStartElement(writer, grid.Rows[i].SerializingTag);
                for (int j = 0; j < grid.Columns.Count; j++) 
                {
                    object value = null;
                    //object o = cell.EditValue;
                    object o = grid.Data == null ? null : grid.Data.Rows[i][j];
                    string str = o != null ? o.ToString() : String.Empty;
                    if (String.IsNullOrEmpty(str))
                        continue;
                    WriteStartElement(writer, grid.Rows[i].Cells[j].SerializingTag);
                    if (grid.Rows[i].Cells[j].Type == GridCellType.List)
                    {
                        if (!String.IsNullOrEmpty(str))
                        {
                            var item = (from y in grid.Rows[i].Cells[j].Source.Items where y.Code == str select y).FirstOrDefault();
                            WriteDictionaryListItem(writer, item);
                        }
                        writer.WriteEndElement();
                        continue;
                    }

                    switch (grid.Rows[i].Cells[j].Type)
                    {
                        case GridCellType.String:
                        case GridCellType.Label:
                            value = str;
                            break;
                        case GridCellType.Check:
                            if (grid.Rows[i].Cells[j].boolSerializationType == BoolSerializtionType.yesno)
                            {
                                YesNoClass yesno = new YesNoClass(config.namespace_av, str == "True");
                                yesno.WriteXml(writer);
                            }
                            else
                            {
                                bool bres;
                                if (Boolean.TryParse(str, out bres))
                                    writer.WriteValue(bres);
                            }
                            break;
                        case GridCellType.Date:
                            DateTime dtTemp;
                            if (DateTime.TryParse(str, out dtTemp))
                                value = EditorViewSerializerHelper.DateToStr(dtTemp);
                            break;
                        case GridCellType.Double:
                            //double dres;
                            //if (Double.TryParse(str, out dres))
                            //    value = dres;
                            str = str.Replace(" ", "");
                            if (str.Contains(','))
                                str = str.Replace(',', '.');
                            value = str;
                            break;
                        case GridCellType.Integer:
                            Int64 ires;
                            if (Int64.TryParse(str, out ires))
                                value = ires;
                            break;
                    }

                    if (grid.Rows[i].Cells[j].Type != GridCellType.Check && value != DBNull.Value && value != null)
                        writer.WriteValue(value);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

        void SerGridAddedRow(GridAddedRow grid, XmlWriter writer)
        {
            WriteStartElement(writer, grid.SerializingTag);
            SerGridAddedRowBody(grid, writer);
            writer.WriteEndElement();
        }

        //сериализует без обертывания в корневой тег
        void SerGridAddedRowBody(GridAddedRow grid, XmlWriter writer)
        {
            if (grid.Data == null) return;
            for (int r = 0; r < grid.Data.Rows.Count; r++)
            {
                WriteStartElement(writer, grid.SerializingRowTag);
                int c = 0;
                foreach (GridAddedRowColumn col in grid.Columns)
                {
                    object value = null;
                    object o = grid.Data.Rows[r][c];
                    string str = o != null ? o.ToString() : String.Empty;

                    if (String.IsNullOrEmpty(str))
                    {
                        c++;
                        continue;
                    }

                    WriteStartElement(writer, col.SerializingTag);
                    if (col.Type == GridCellType.List)
                    {
                        if (!String.IsNullOrEmpty(str))
                        {
                            var item = (from i in col.Source.Items where i.Code == str select i).FirstOrDefault();
                            if (item != null)
                            {
                                WriteDictionaryListItem(writer, item);
                            }
                        }
                        writer.WriteEndElement();
                        c++;
                        continue;
                    }
                    switch (col.Type)
                    {
                        case GridCellType.String:
                        case GridCellType.Label:
                            value = str;
                            break;
                        case GridCellType.Check:
                            if (col.boolSerializationType == BoolSerializtionType.yesno)
                            {
                                YesNoClass yesno = new YesNoClass(config.namespace_av, str == "True");
                                yesno.WriteXml(writer);
                            }
                            else
                            {
                                bool bres;
                                if (Boolean.TryParse(str, out bres))
                                    writer.WriteValue(bres);
                            }
                            break;
                        case GridCellType.Date:
                            DateTime dtTemp;
                            if (DateTime.TryParse(str, out dtTemp))
                                value = EditorViewSerializerHelper.DateToStr(dtTemp);
                            break;
                        case GridCellType.Double:
                            double dres;
                            if (Double.TryParse(str, out dres))
                                value = dres;
                            break;
                        case GridCellType.Integer:
                            Int64 ires;
                            if (Int64.TryParse(str, out ires))
                                value = ires;
                            break;
                    }

                    if (col.Type != GridCellType.Check && value != DBNull.Value && value != null)
                        writer.WriteValue(value);
                    writer.WriteEndElement();
                    c++;
                }
                writer.WriteEndElement();
            }
        }

        void SerGridAddedPage(GridAddedPage grid, XmlWriter writer)
        {
            WriteStartElement(writer, grid.SerializingTag);
            for (int i = 1; i < grid.Children.Count; i++)
            {
                SerPage(writer, grid.Children[i] as Page, grid.Children[i].Children);
            }
            writer.WriteEndElement();
        }

        void SerMetaGrid(MetaGrid grid, XmlWriter writer)
        {
            WriteStartElement(writer, grid.SerializingTag);
            foreach (BaseGrid children in grid.Tables)
            {
                if (children is Grid)
                    SerGridBody(children as Grid, writer);
                else if (children is GridAddedRow)
                    SerGridAddedRowBody(children as GridAddedRow, writer);
            }
            writer.WriteEndElement();
        }

        private void SerComboBox(XmlWriter writer, aetp.Common.ControlModel.BaseControl ctrl)
        {
            if (ctrl.EditValue != null && ctrl.EditValue != DBNull.Value)
            {
                WriteStartElement(writer, ctrl.SerializingTag);
                DictionaryListItem item = ctrl.EditValue as DictionaryListItem;
                if (item == null && (ctrl as aetp.Common.ControlModel.ComboBox).Items.Count > 0 &&
                    ctrl.EditValue is aetpComboBoxItem)
                {
                    var comboItem = ctrl.EditValue as aetpComboBoxItem;
                    item = (ctrl as aetp.Common.ControlModel.ComboBox).Source.Items.Where(x => x.Code == comboItem.ID).FirstOrDefault();
                }
                WriteDictionaryListItem(writer, item);
                writer.WriteEndElement();
            }
        }

        #endregion

        #region deserializing

        void DeserPage(XmlReader reader, Page page, List<aetp.Common.ControlModel.BaseControl> controls)
        {
            bool IsEmpty = reader.IsEmptyElement;             
            string correctName = getCorrectName(page.SerializingTag);
            //если страница не сохранена то и нах ее
            if (reader.LocalName != correctName)
            {
                if (!page.IsMandatoryToFill) page.IsShownPanel = false;
                return;
            }
            if (!alternateSerializer.Desereilize(reader, page))
            {
                reader.ReadStartElement(correctName, config.namespace_av);

                if (IsEmpty)
                {
                    if (!page.IsMandatoryToFill) page.IsShownPanel = false;
                    return;
                }
                page.IsShownPanel = true;
                if (page is Report)
                {
                    if ((page as Report).AttachFile)
                    {
                        DeserFile(reader, page);
                    }
                }
                DeserControls(reader, page, controls);
                reader.ReadEndElement();
            }
        }

        void DeserFile(XmlReader reader, Page page)
        {            
                List<FileList> list = new List<FileList> { };
                bool IsEmpty = reader.IsEmptyElement;

                if (reader.LocalName == "Files")
                {
                    reader.ReadStartElement();
                    while (reader.LocalName == "File")
                    {
                        string s = reader.GetAttribute(0).ToString();
                        reader.ReadStartElement();                        
                        IsEmpty = reader.IsEmptyElement;
                        if (!IsEmpty)
                        {
                            list.Add(new FileList(s, reader.ReadContentAsString(), reader.LocalName));
                        }                        
                        reader.ReadEndElement();                       
                    }
                    reader.ReadEndElement();
                }
                (page as Report).File = list;            
        }

        void DeserControls(XmlReader reader, Page page, List<aetp.Common.ControlModel.BaseControl> controls)
        {
            foreach (aetp.Common.ControlModel.BaseControl ctrl in controls)
            {
                if (ctrl is Link)
                    continue;
                if (ctrl is Page)
                    DeserPage(reader, ctrl as Page, ctrl.Children);
                else if (ctrl is MetaConrol)
                {
                    DeserControls(reader, page, (ctrl as MetaConrol).Children);
                } 
                else
                {
                    //пропускаем элементы если нужных тегов нет в отчете
                    if ((reader.LocalName != ctrl.SerializingTag) && (reader.LocalName != ctrl.AlternativeSerializingTag))
                        continue;
                }
                if (ctrl is SimpleEdit)
                    DeserSimpleEdit(reader, ctrl);
                if (ctrl is SimpleRichEdit)
                    DeserSimpleRichEdit(reader, ctrl as SimpleRichEdit);
                if (ctrl is aetp.Common.ControlModel.MemoEdit)
                    DeserMemoEdit(reader, page, ctrl);

                if (ctrl is Grid)
                    DeserGrid(reader, ctrl);

                if (ctrl is GridAddedRow)
                    DeserGridAddedRow(reader, ctrl);

                if (ctrl is GridAddedPage)
                {
                    DeserGridAddedPage(reader, ctrl);
                }
                if (ctrl is aetp.Common.ControlModel.ComboBox)
                {
                    DeserComboBox(reader, ctrl);
                }
                if (ctrl is MetaGrid)
                {
                    DeserMetaGrid(reader, ctrl);
                }
                else
                {
                    continue;
                }
            }
        }

        private void DeserComboBox(XmlReader reader, aetp.Common.ControlModel.BaseControl editor)
        {
            bool IsEmpty = reader.IsEmptyElement;
            if (reader.LocalName != editor.SerializingTag)
                return;
            else
                reader.ReadStartElement();
            
            if (IsEmpty)
            {
                editor.EditValue = null;
                return;
            }
            
            DictionaryListItem item = ReadDictionaryListItem(reader);
            var currentItem = (editor as aetp.Common.ControlModel.ComboBox).Source.Items.Where(x => x.Code == item.Code).SingleOrDefault();
            if (currentItem == null)
            {
                editor.EditValue = null;
            }
            else
            {
                editor.EditValue = new aetpComboBoxItem() { ID = currentItem.Code, Name = currentItem.Description };
            }
            reader.ReadEndElement();
        }

        private void DeserGridAddedPage(XmlReader reader, aetp.Common.ControlModel.BaseControl editor)
        {
            bool IsEmpty = reader.IsEmptyElement;
            if (reader.LocalName != editor.SerializingTag)
                return;
            else
                reader.ReadStartElement();
            if (IsEmpty)
                return;
            GridAddedPage grid = editor as GridAddedPage;
            
            int i = 1;
            DataTable dt = new DataTable();
            foreach (GridAddedPageColumn col in grid.Columns)
            {
                dt.Columns.Add(col.FieldName);
            }
            grid.Data = dt;
            while (reader.IsStartElement() && reader.LocalName == grid.Children[0].SerializingTag)
            {
                grid.NewPage();
                DeserPage(reader, grid.Children[i] as Page, grid.Children[i].Children);
                i++;
            }
            
            reader.ReadEndElement();
        }

        private void DeserGridAddedRow(XmlReader reader, aetp.Common.ControlModel.BaseControl ctrl)
        {
            bool IsEmpty = reader.IsEmptyElement;
            if (reader.LocalName != ctrl.SerializingTag)
                return;
            else
                reader.ReadStartElement();
            if (IsEmpty)
                return;
            DeserGridAddedRowBody(reader, ctrl);
            reader.ReadEndElement();
        }

        private void DeserGridAddedRowBody(XmlReader reader, aetp.Common.ControlModel.BaseControl ctrl)
        {
            GridAddedRow grid = ctrl as GridAddedRow;
            if (grid.Data == null)
            {
                grid.Data = new DataTable();
            }
            else
            {
                grid.Data.Columns.Clear();
                grid.Data.Rows.Clear();
            }

            foreach (var col in grid.Columns)
            {
                grid.Data.Columns.Add(new DataColumn(col.Name, col.GetType()));
            }

            int r = -1;
            while (reader.IsStartElement() && reader.LocalName == grid.SerializingRowTag)
            {
                r++;
                DataRow dr = grid.Data.NewRow();
                grid.Data.Rows.Add(dr);
                bool empty = reader.IsEmptyElement;
                reader.ReadStartElement(grid.SerializingRowTag, config.namespace_av);
                if (empty)
                    continue;
                
                foreach (GridAddedRowColumn col in grid.Columns)
                {
                    if (col.Type == GridCellType.Label)
                    {
                        dr[col.Name] = col.TextLabel;
                    }
                    if (reader.LocalName != col.SerializingTag)
                    {
                        continue;
                    }
                    object value = DBNull.Value;
                    if (!reader.IsEmptyElement)
                    {
                        if (col.Type == GridCellType.List)
                        {
                            if (!reader.IsEmptyElement)
                            {
                                reader.ReadStartElement();
                                dr[col.Name] = ReadDictionaryListItem(reader).Code;
                                reader.ReadEndElement();
                                continue;
                            }
                            else
                                reader.ReadStartElement();
                        }
                        switch (col.Type)
                        {
                            case GridCellType.String:
                            case GridCellType.Label:
                                value = reader.ReadElementContentAsString();
                                break;
                            case GridCellType.Check:
                                if (col.boolSerializationType == BoolSerializtionType.yesno)
                                {
                                    YesNoClass yesno = new YesNoClass(config.namespace_av);
                                    yesno.ReadXml(reader);
                                    value = (bool)yesno.EditValue;
                                }
                                else
                                {
                                    if (!reader.IsEmptyElement)
                                        value = reader.ReadElementContentAsBoolean();
                                    else
                                        reader.ReadStartElement();
                                }
                                break;
                            case GridCellType.Date:
                                if (!reader.IsEmptyElement)
                                    value = reader.ReadElementContentAsDateTime();
                                else
                                    reader.ReadStartElement();
                                break;
                            case GridCellType.Double:
                                if (!reader.IsEmptyElement)
                                    value = reader.ReadElementContentAsDouble();
                                else
                                    reader.ReadStartElement();
                                break;
                            case GridCellType.Integer:
                                if (!reader.IsEmptyElement)
                                    value = reader.ReadElementContentAsLong();
                                else
                                    reader.ReadStartElement();
                                break;
                        }
                    }
                    else
                        reader.Read();
                    if (col.Type != GridCellType.Label)
                    {
                        dr[col.Name] = value;
                    }
                    
                }
                reader.ReadEndElement();
            }
            grid.Data.AcceptChanges();
        }

        private void DeserGrid(XmlReader reader, aetp.Common.ControlModel.BaseControl ctrl)
        {
            bool IsEmpty = reader.IsEmptyElement;
            if (reader.LocalName != getCorrectName(ctrl.SerializingTag))
                return;
            else
                reader.ReadStartElement();
            if (IsEmpty)
                return;
            DeserGridBody(reader, ctrl);
            reader.ReadEndElement();
        }

        private void DeserGridBody(XmlReader reader, aetp.Common.ControlModel.BaseControl ctrl)
        {
            Grid grid = ctrl as Grid;
            int r = 0;
            grid.Data = new DataTable();
            foreach (var col in grid.Columns)
            {
                grid.Data.Columns.Add(col.Name);
            }
            foreach (aetp.Common.ControlModel.GridRow row in grid.Rows)
            {
                bool empty = reader.IsEmptyElement;
                reader.ReadStartElement(row.SerializingTag, config.namespace_av);
                if (empty)
                {
                    foreach (var cell in grid.Rows[r].Cells)
                    {
                        if (cell.Type == GridCellType.Label)
                        {
                            cell.EditValue = cell.Caption;
                        }
                        else
                        {
                            cell.EditValue = null;
                        }
                    }
                    var empty_row_items = grid.Rows[r].Cells.Select<GridCellItem, object>(x =>
                    {
                        if (x.Type == GridCellType.Label) return x.Caption;
                        else return DBNull.Value;
                    }).ToArray();
                    grid.Data.Rows.Add(empty_row_items);
                    r++;
                    continue;
                }
                int c = -1;
                foreach (GridCellItem cell in row.Cells)
                {
                    c++;
                    if (reader.LocalName != cell.SerializingTag)
                        continue;
                    
                    //if (!String.IsNullOrEmpty((cell.FormulaForCalculating ?? "").Trim()))
                    //{
                    //    reader.ReadElementContentAsString();
                    //    continue;
                    //}
                    object value = null;
                    
                    if (!reader.IsEmptyElement)
                    {
                        switch (cell.Type)
                        {
                            case GridCellType.String:
                            case GridCellType.Label:
                                value = reader.ReadElementContentAsString();
                                break;
                            case GridCellType.Check:
                                if (cell.boolSerializationType == BoolSerializtionType.yesno)
                                {
                                    YesNoClass yesno = new YesNoClass(config.namespace_av);
                                    yesno.ReadXml(reader);
                                    value = (bool)yesno.EditValue;
                                }
                                else
                                {
                                    if (!reader.IsEmptyElement)
                                        value = reader.ReadElementContentAsBoolean();
                                    else
                                        reader.ReadStartElement();
                                }
                                break;
                            case GridCellType.Date:
                                if (!reader.IsEmptyElement)
                                    value = reader.ReadElementContentAsDateTime();
                                else
                                    reader.ReadStartElement();
                                break;
                            case GridCellType.Double:
                                if (!reader.IsEmptyElement)
                                    value = reader.ReadElementContentAsDouble();
                                else
                                    reader.ReadStartElement();
                                break;
                            case GridCellType.Integer:
                                if (!reader.IsEmptyElement)
                                    value = reader.ReadElementContentAsLong();
                                else
                                    reader.ReadStartElement();
                                break;
                            case GridCellType.List:
                                if (!reader.IsEmptyElement)
                                {
                                    reader.ReadStartElement();
                                    value = ReadDictionaryListItem(reader).Code;
                                    reader.ReadEndElement();
                                }
                                else
                                    reader.ReadStartElement();
                                break;

                        }
                    }
                    else
                        reader.Read();

                    if (cell.Type != GridCellType.Label)
                    {
                        grid.Rows[r].Cells[c].EditValue = value;
                    }
                    else
                    {
                        grid.Rows[r].Cells[c].EditValue = grid.Rows[r].Cells[c].Caption;
                    }
                }
                var items = grid.Rows[r].Cells.Select(x =>
                {
                    if (x.Type == GridCellType.Label) return x.Caption;
                    else return x.EditValue;
                }).ToArray();
                grid.Data.Rows.Add(items);
                r++;
                reader.ReadEndElement();
            }
        }

        private void DeserMetaGrid(XmlReader reader, aetp.Common.ControlModel.BaseControl editor)
        {
            bool IsEmpty = reader.IsEmptyElement;
            if (reader.LocalName != editor.SerializingTag)
                return;
            else
                reader.ReadStartElement();
            if (IsEmpty)
                return;
            foreach (BaseGrid children in (editor as MetaGrid).Tables)
            {
                if (children is Grid)
                    DeserGridBody(reader, children);
                else if (children is GridAddedRow)
                    DeserGridAddedRowBody(reader, children);
            }
            reader.ReadEndElement();
        }

        private void DeserSimpleEdit(XmlReader reader, aetp.Common.ControlModel.BaseControl editor)
        {
            bool IsEmpty = reader.IsEmptyElement;
            if (IsEmpty)
            {
                editor.EditValue = null;
                return;
            }
            switch ((editor as SimpleEdit).DataType)
            {
                case SimpleDataType.String:
                    editor.EditValue = reader.ReadElementContentAsString();                    
                    break;
                case SimpleDataType.Bool:
                    if ((editor as SimpleEdit).boolSerializationType == BoolSerializtionType.yesno)
                    {
                        YesNoClass yesno = new YesNoClass(config.namespace_av);
                        yesno.ReadXml(reader);
                        editor.EditValue = (bool)yesno.EditValue;
                    }
                    else
                    {
                        editor.EditValue = reader.ReadElementContentAsBoolean();
                    }
                    break;
                case SimpleDataType.DateTime:
                    reader.ReadStartElement();
                    if (reader.Value == string.Empty)
                    {
                        editor.EditValue = null;
                    }
                    else
                    {
                        editor.EditValue = reader.ReadContentAsDateTime();
                    }
                    reader.ReadEndElement();
                    break;
                case SimpleDataType.Double:
                    editor.EditValue = reader.ReadElementContentAsDouble();
                    break;
                case SimpleDataType.Integer:
                    editor.EditValue = reader.ReadElementContentAsLong();
                    break;
            }
        }

        private void DeserMemoEdit(XmlReader reader, Page page, aetp.Common.ControlModel.BaseControl editor)
        {
            bool IsEmpty = reader.IsEmptyElement;
            if (IsEmpty)
            {
                editor.EditValue = null;
                return;
            }
            string s;
            s = reader.ReadElementContentAsString();
            char[] massiv  = s.ToCharArray();
            string memoValue = s.Replace("\n", "\r\n");                       
            editor.EditValue = memoValue;
        }

        private void DeserSimpleRichEdit(XmlReader reader, SimpleRichEdit editor)
        {
            bool IsEmpty = reader.IsEmptyElement;
            if (IsEmpty)
            {
                editor.EditValue = null;
                return;
            }
            string s;
            s = reader.ReadElementContentAsString();

            string path = Path.Combine(aetp.Utils.Config.GetAppPath(), "temp");
            if (!(Directory.Exists(path)))
                Directory.CreateDirectory(path);
            string filename = Path.Combine(path, "RichEditData.rtf");

            byte[] filebytes = Convert.FromBase64String(s);
            FileStream fs = new FileStream(filename,
                                   FileMode.Create,
                                   FileAccess.Write,
                                   FileShare.None);
            fs.Write(filebytes, 0, filebytes.Length);
            fs.Close();
            var richEdit = new RichEditControl();
            richEdit.LoadDocument(filename, DocumentFormat.Rtf);
            editor.EditValue = richEdit.Text;
        }

        #endregion

        #endregion

    }
}
