﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;

namespace aetp.Common.Mediator
{
    class YesNoClass : IXmlSerializable
    {
        string n;

        public YesNoClass(string n)
        {
            this.n = n;
            Активно = true;
        }

        public YesNoClass(string n, bool value)
        {
            this.n = n;
            Активно = true;
            EditValue = value;
        }

        public XmlSchema GetSchema()
        {
            return null;
        }
        

        public object EditValue
        {
            get
            {
                return Код == "Да";
            }
            set
            {
                if ((bool)value)
                {
                    Код = "Да";
                    Описание = "Да";
                }
                else
                {
                    Код = "Нет";
                    Описание = "Нет";
                }
            }
        }

        public string Код { get; set; }
        public string Описание { get; set; }
        public bool Активно { get; set; }
        
        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            Код = reader.ReadElementContentAsString("Код", n);
            if (reader.LocalName == "Описание")
                Описание = reader.ReadElementContentAsString("Описание", n);
            if (reader.LocalName == "Активно")
                Активно = reader.ReadElementContentAsBoolean("Активно", n);
            reader.ReadEndElement();
        }
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("Код", n, Код);
            writer.WriteElementString("Описание", n, Описание);
            writer.WriteStartElement("Активно", n);
            writer.WriteValue(Активно);
            writer.WriteEndElement();
        }
    }
}
