﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Windows;
using System.Net;
using System.Xml.Serialization;
using System.IO;
using aetp.Common.View.Dialog;
using System.Windows.Forms;

namespace aetp.Common
{
    internal static class AnketFormUpdaterFromWeb
    {
        public static string AnketFormUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["AnketFormUpdaterUrl"] + "ModelConfig.xml";
            }
        }

        public static string AnketFormVersionUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["AnketFormUpdaterUrl"] + "version.txt";
            }
        }

        public static string AnketModelDirectoryUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["AnketFormUpdaterUrl"] + "ModelDirectoryConfig.xml";
            }
        }

        public static string AnketMaskUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["AnketFormUpdaterUrl"] + "MaskConfig.xml";
            }
        }

        public static string AnketFormDirectoryUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["AnketFormUpdaterUrl"];
            }
        }

        static ModelConfig GetServerModelConfig()
        {
            Stream stream = null;
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Proxy = ProxyServer;
                    stream = wc.OpenRead(AnketFormUrl);
                    XmlSerializer serializer = new XmlSerializer(typeof(ModelConfig));
                    ModelConfig cfg = (ModelConfig)serializer.Deserialize(stream);
                    return cfg;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }

        public static string GetServerVersion()
        {
            Stream stream = null;
            string version;
            using (WebClient wc = new WebClient())
            {
                wc.Proxy = ProxyServer;
                stream = wc.OpenRead(AnketFormVersionUrl);
                StreamReader sr = new StreamReader(stream);
                version = sr.ReadToEnd();
            }
            return version;
        }

        static ModelConfigCompareResult GetChanges(ModelConfig localConfig, ModelConfig serverConfig)
        {
            ModelConfigCompareResult compareResult = new ModelConfigCompareResult();
            try
            {
                foreach (aetp.Common.ModelConfig.ModelInfo servermi in serverConfig.ListModelInfo)
                {
                    var eqs = localConfig.ListModelInfo.Where(x => x.Tag == servermi.Tag && x.Version == servermi.Version);
                    if (eqs.Count() == 0)
                        compareResult.OnlyServer.Add(servermi);
                    else if (eqs.Count() == 1)
                    {
                        ModelConfig.ModelInfo localmi = eqs.First();
                        if (localmi.Hash != servermi.Hash)
                            compareResult.Changes.Add(new ModelConfigChangePair(servermi, localmi));
                    }
                    else
                        throw new ConfigurationErrorsException("В локальном конфигурационном файле содержаться пересекающиеся отчеты");
                }

                foreach (aetp.Common.ModelConfig.ModelInfo localmi in localConfig.ListModelInfo)
                {
                    var eqs = serverConfig.ListModelInfo.Where(x => x.Tag == localmi.Tag && x.Version == localmi.Version);
                    if (eqs.Count() == 0)
                        compareResult.OnlyLocal.Add(localmi);
                    else if (eqs.Count() > 1)
                        throw new ConfigurationErrorsException("В серверном конфигурационном файле содержаться пересекающиеся отчеты");
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            return compareResult;
        }

        static string verify_file_name(string file_name)
        {
            string full_path = Path.Combine(AppGlobal.Instance.Config.ModelsPath, file_name);
            if (File.Exists(full_path))
                throw new UpdateException(String.Format("Невозможно скачать файл {0}, так как файл с таким именем существует в папке {1}",
                    file_name, AppGlobal.Instance.Config.ModelsPath));
            else
                return full_path;
        }

        static void Update(List<UpdateItemWrapper> items)
        {
            WebClient wc = new WebClient();
            wc.Proxy = ProxyServer;
            foreach (var item in items)
            {
                try
                {
                    switch (item.Action)
                    {
                        case UpdateAction.Update:
                            ModelInfo mi = item.UpdateItem as ModelInfo;
                            string file_name = Path.GetFileName(mi.Path);
                            string local_file_path = verify_file_name(file_name);
                            string url1 = AnketFormDirectoryUrl + mi.Path;
                            wc.DownloadFile(url1, local_file_path);
                            break;
                        case UpdateAction.Delete:
                            ModelInfo delete_mi = item.UpdateItem as ModelInfo;
                            File.Delete(delete_mi.Path);
                            break;
                        case UpdateAction.Replace:
                            ModelConfigChangePair pair = item.UpdateItem as ModelConfigChangePair;
                            string url2 = AnketFormDirectoryUrl + pair.Server.Path;
                            wc.DownloadFile(url2, pair.Local.Path);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    item.ex = ex;
                }
            }
        }

        private static IWebProxy ProxyServer
        {
            get
            {
                switch (AppGlobal.Instance.ProxyServerConfig.ProxyType)
                {
                    case ProxyServerConfig.ProxyTypes.None:
                        return null;
                    case ProxyServerConfig.ProxyTypes.Default:
                        return WebRequest.GetSystemWebProxy();
                    case ProxyServerConfig.ProxyTypes.Http:
                        return new WebProxy(
                            String.Format("{0}:{1}", AppGlobal.Instance.ProxyServerConfig.HttpUrl, AppGlobal.Instance.ProxyServerConfig.HttpPort), 
                            AppGlobal.Instance.ProxyServerConfig.NotUsingProxyForLocalPort);
                }
                return null;
            }
        }

        private static void UpdateFile(string localPath, string remotePath)
        {
            WebClient wc = new WebClient();
            wc.Proxy = ProxyServer;
            string local_dirConfPath = localPath;
            string temp_file = null;
            if (File.Exists(local_dirConfPath))
            {
                var file_name = Path.GetFileName(local_dirConfPath);
                var dir_name = Path.GetDirectoryName(local_dirConfPath);
                temp_file = Path.Combine(dir_name, "_" + file_name);
                if (File.Exists(temp_file))
                    File.Delete(temp_file);
                File.Move(local_dirConfPath, temp_file);
            }
            try
            {
                wc.DownloadFile(remotePath, local_dirConfPath);
                if (temp_file != null && File.Exists(temp_file))
                {
                    File.Delete(temp_file);
                }
            }
            catch
            {
                if (temp_file != null && File.Exists(temp_file))
                    File.Move(temp_file, local_dirConfPath);
                throw;
            }
        }
                
        public static void Update()
        {
            if (!EditorApp.UseUpdating)
                return;
            try
            {
                ReportUtils.RefreshModelsList();
            }
            catch
            {
                System.Windows.MessageBox.Show("Ошибка чтения конфигурации списка моделей");
            }
            ModelConfig localConfig = new ModelConfig();
            localConfig.Load();
            UpdateFile(Path.Combine(aetp.Utils.Config.GetAppPath(), "ModelDirectoryConfig.xml"), AnketModelDirectoryUrl);
            UpdateFile(Path.Combine(aetp.Utils.Config.GetAppPath(), "MaskConfig.xml"), AnketMaskUrl);
            ModelConfigCompareResult changes = GetChanges(localConfig, GetServerModelConfig());
            if (changes.OnlyLocal.Count == 0 && changes.OnlyServer.Count == 0 && changes.Changes.Count == 0)
            {
                System.Windows.MessageBox.Show("Не найдено ни одного обновления");
                string version = GetServerVersion();
                AppGlobal.Instance.Config.Version = version;
                AppGlobal.Instance.Config.SuspedVersion = version;
                AppGlobal.Instance.Config.Save();
                return;
            }
            UpdaterDialog dialog = new UpdaterDialog(changes);
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                Update(dialog.ItemsForUpdate);

                string version = GetServerVersion();
                AppGlobal.Instance.Config.Version = version;
                AppGlobal.Instance.Config.SuspedVersion = version;
                AppGlobal.Instance.Config.Save();
                UpdaterResult resultDlg = new UpdaterResult(dialog.ItemsForUpdate);
                resultDlg.Show();
            }
            ReportUtils.RefreshModelsList();
        }

        public static void CheckUpdates()
        {
            
            ModelConfig localConfig = new ModelConfig();
            localConfig.Load();
            string version = GetServerVersion();
            if (localConfig.Version != version && localConfig.SuspedVersion != version)
            {
                UpdaterCheckDialog checkDialog = new UpdaterCheckDialog(version);
                DialogResult res = checkDialog.ShowDialog();
                if (checkDialog.IsNotRemindeMe)
                {
                    AppGlobal.Instance.Config.SuspedVersion = version;
                    AppGlobal.Instance.Config.Save();
                }
                if (res == DialogResult.OK)
                {
                    Update();
                }
            }
        }
    }

    public class ModelConfigCompareResult
    {
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (OnlyServer.Count > 0) sb.Append(String.Format(" - {0} новых очтетов", OnlyServer.Count));
            if (Changes.Count > 0) sb.Append(String.Format(" - найдено {0} отличй в отчетных формах", Changes.Count));
            return sb.ToString();
        }

        public ModelConfigCompareResult()
        {
            OnlyServer = new List<ModelConfig.ModelInfo>();
            OnlyLocal = new List<ModelConfig.ModelInfo>();

            Changes = new List<ModelConfigChangePair>();
        }

        public List<ModelConfig.ModelInfo> OnlyServer;
        public List<ModelConfig.ModelInfo> OnlyLocal;

        public List<ModelConfigChangePair> Changes;
    }

    public class ModelConfigChangePair
    {
        public aetp.Common.ModelConfig.ModelInfo Server;
        public aetp.Common.ModelConfig.ModelInfo Local;

        public ModelConfigChangePair()
        {

        }

        public ModelConfigChangePair(aetp.Common.ModelConfig.ModelInfo Server, aetp.Common.ModelConfig.ModelInfo Local)
        {
            this.Server = Server;
            this.Local = Local;
        }

    }

    public class UpdateItemWrapper
    {
        public UpdateAction Action;
        public Object UpdateItem;
        Exception _ex;
        public Exception ex
        {
            get
            {
                return _ex;
            }
            set
            {
                _ex = value;
                IsUpdating = false;
            }
        }
        public bool IsUpdating = true;

        public UpdateItemWrapper() { }

        public UpdateItemWrapper(object updateItem, UpdateAction action)
        {
            UpdateItem = updateItem;
            Action = action;
        }

    }

    public enum UpdateAction
    {
        Update = 0,
        Replace,
        Delete
    }

    public class ModelInfo
    {
        public string Name;
        public string Path;
        public string Version;
        public string Description;
        public string Tag;
        public string Hash;

        public ModelInfo(ModelConfig.ModelInfo mi)
        {
            Name = mi.Name;
            Path = mi.Path;
            Version = mi.Version;
            Description = mi.Description;
            Tag = mi.Tag;
            Hash = mi.Hash;
        }
    }

    public class UpdateException : Exception
    {
        string message = "Ошибка при обновлении отчетных форм";
        public override string Message
        {
            get
            {
                return message;
            }
        }

        public UpdateException(string message)
        {
            this.message = message;
        }
    }
}
