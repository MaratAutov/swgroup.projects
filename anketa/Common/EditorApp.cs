﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.Mediator;
using aetp.Common.View;
using aetp.Common.ControlModel;
using aetp.Common.View.Splash;
using System.Windows.Forms;
using aetp.Common.View.Dialog;
using System.Configuration;

namespace aetp.Common
{
    public class EditorApp : IAnketaApp
    {
        Report Report;
        public EditorApp(Report report)
        {
            Report = report;
            DevExpress.XtraEditors.Controls.Localizer.Active = new RussianControlsLocalizer();
        }
        public void Run()
        {
            GetMainForm().ShowDialog();
        }

        public static bool UseUpdating
        {
            get
            {
                return ConfigurationManager.AppSettings["UseUpdating"] == "true";
            }
        }

        public static EditorView MainView { get; set; }

        public Form GetMainForm()
        {
            EditorView view = new EditorView();
            MainView = view;
            EditorMediator mediator = new EditorMediator(view, Report, "");
            Splash splash = new Splash(view, ViewType.Editor);
            return splash;
        }

        public Form GetMainForm(string filename)
        {
            EditorView view = new EditorView();
            MainView = view;
            EditorMediator mediator = new EditorMediator(view, Report,filename);
            Splash splash = new Splash(view, ViewType.Editor);
            return splash;
        }
    }
}
