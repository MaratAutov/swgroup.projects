﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting;

namespace aetp.Common.Control
{
	public class TextLink : Link
	{
        public String Text { get; set; }

        public StringAlignment HAlignment { get; set; }
        public float emSize { get; set; }
        public FontStyle FontStyle { get; set; }
        string Fontfamily { get; set; }

        public TextLink(String text,string font,float size = 11)
            : base()
        {
            this.Text = text;
            HAlignment = StringAlignment.Near;
            emSize = size;
            FontStyle = FontStyle.Regular;
            Fontfamily = font;            
        }

        public override void CreateDocument(PrintingSystem ps)
        {
            if (Text == string.Empty) return;
            base.CreateDocument(ps);
        }

        protected override void CreateDetail(BrickGraphics graph)
        {
            List<TextLinkItem> list = new List<TextLinkItem>{};
            if (Text.Contains("<b>"))
            {
                while (Text.Contains("<b>"))
                {
                    string text = Text.Substring(0, Text.IndexOf("<b>"));
                    TextLinkItem item = new TextLinkItem(text, false);
                    list.Add(item);
                   
                    string bold_text = Text.Substring(Text.IndexOf("<b>") + 3, Text.IndexOf("</b>") - Text.IndexOf("<b>") - 3);
                    
                    TextLinkItem item_bold = new TextLinkItem(bold_text, true);
                    list.Add(item_bold);
                    Text = Text.Substring(Text.IndexOf("</b>") + 3);
                }
            }
            else
            {
                TextLinkItem item = new TextLinkItem(Text, false);
                list.Add(item);
            }
            float height = 0;
            foreach (TextLinkItem item in list)
            {                
                base.CreateDetail(graph);
                if (emSize <= 0)
                    emSize = 8.0f;
                graph.StringFormat = new BrickStringFormat(HAlignment);
                graph.BackColor = Color.Transparent;
                graph.BorderWidth = 0;
                Font font = new Font(Fontfamily, emSize, item.Bold ? FontStyle.Bold : FontStyle.Regular);
                
                graph.Font = font;
                SizeF textSize = graph.MeasureString(item.Text, (int)graph.ClientPageSize.Width);
                RectangleF rect = new RectangleF(0, height, graph.ClientPageSize.Width, textSize.Height);
                
                graph.DrawString(item.Text, rect);
                height += rect.Height;
            }
        }
	}

    public class TextLinkItem
    {
        private string text;
        private bool bold;

        public TextLinkItem(string text,bool bold)
        {
            this.text = text;
            this.bold = bold;
        }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }

        public bool Bold
        {
            get
            {
                return bold;
            }
            set
            {
                bold = value;
            }
        }
    }
}
