﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting;
using aetp.Common.ControlModel;
using System.Data;
using System.Windows;

namespace aetp.Common.Control
{
    public class GridAddedPageLink : DevExpress.XtraPrinting.Link
    {
        GridAddedPage grid;
        DataTable dt;
        Font font;
        float[] columnwidth;
        bool calcOptimal;

        public GridAddedPageLink(GridAddedPage grid, Font font, bool calcOptimal)
        {
            this.grid = grid;
            this.dt = grid.Data;
            this.font = font;
            this.calcOptimal = calcOptimal;
        }

        protected override void CreateReportHeader(BrickGraphics graph)
        {
            
                base.CreateReportHeader(graph);
                columnwidth = GridLink.getColumnWidth(graph, dt, calcOptimal);
                graph.BackColor = Color.Gray;
                graph.Font = font;
                graph.BorderColor = Color.Black;
                graph.BorderWidth = 1;
                float x = 0;
                float max_height = 0;
                int i = 0;
                foreach (var col in grid.Columns)
                {
                    SizeF textSize = graph.MeasureString(col.Caption, (int)columnwidth[i]);
                    if (textSize.Height > max_height)
                    {
                        max_height = textSize.Height;
                    }
                    i++;
                }

                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    string caption = grid.Columns[c].Caption;
                    graph.DrawString(caption, Color.Black,
                        new RectangleF(x, 0, columnwidth[c], max_height),
                        BorderSide.All);
                    x += columnwidth[c];
                }
            
        }

        protected override void CreateDetail(BrickGraphics graph)
        {
            graph.BackColor = Color.Transparent;
            graph.Font = font;
            graph.BorderColor = Color.Black;
            graph.BorderWidth = 1;
            float y = 0;
            float max_height = 0;
            float width = (graph.ClientPageSize.Width - 1) / dt.Columns.Count;
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                float x = 0;
                max_height = GridLink.getMaxRowHeight(graph, dt.Rows[r], columnwidth);
                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    graph.DrawString(dt.Rows[r][c].ToString(), Color.Black,
                        new RectangleF(x, y, columnwidth[c], 
                        max_height), 
                        BorderSide.All);
                    x += columnwidth[c];
                }
                y += max_height;
            }
        }

        
    }
}
