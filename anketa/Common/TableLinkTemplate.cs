﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting;
using aetp.Common.ControlModel;
using aetp.Common;
using System.Data;
using System.Windows;

namespace aetp.Common.Control
{
    public class TableLinkTemplate : DevExpress.XtraPrinting.Link
    {
        DataTable dt;
        Font[] font;
        float[] columnwidth;
        Color[] backgroundColor;
        BrickStringFormat[] brickSFormat;
        aetp.Common.Printing.PrintElementContext item;

        public TableLinkTemplate(aetp.Common.Printing.PrintElementContext item,DataTable dt)
        {
            this.item = item;
            this.dt = dt;
        }

        protected override void CreateReportHeader(BrickGraphics graph)
        {
            base.CreateReportHeader(graph);            
        }

        public static string GetFont(PrintingFont font)
        {
            string font_family = string.Empty;
            if (font == PrintingFont.Courier)
                font_family = "Courier New";
            else if (font == PrintingFont.Arial)
            {
                font_family = "Arial";
            }
            else font_family = "Times New Roman";
            return font_family;
        }

        protected override void CreateDetail(BrickGraphics graph)
        {
            BorderSide side = BorderSide.All;
            if (item.IsNoBorder) side = BorderSide.None;
            columnwidth = new float[2];  
            columnwidth[0] = item.Option.WidthFirstColumn;
            columnwidth[1] = item.Option.WidthLastColumn;
            backgroundColor = new Color[dt.Rows.Count];
            font = new Font[dt.Rows.Count];
            brickSFormat = new BrickStringFormat[dt.Rows.Count];
            float sum_width = columnwidth[0] + columnwidth[1];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //выравнивание
                StringFormat sf = new StringFormat();
                switch (item.list_table[i].Option.VAlignment)
                {
                    case aetp.Common.ControlModel.VerticalAlignment.Bottom: sf.LineAlignment = StringAlignment.Far; break;
                    case aetp.Common.ControlModel.VerticalAlignment.Center: sf.LineAlignment = StringAlignment.Center; break;
                    case aetp.Common.ControlModel.VerticalAlignment.Top: sf.LineAlignment = StringAlignment.Near; break;
                }                    
                sf.Alignment = item.list_table[i].Option.Alignment;
                brickSFormat[i] = new BrickStringFormat(sf);

                //заливка                
                backgroundColor[i] = item.list_table[i].Option.IsBackGround ? Color.LightGray : Color.Transparent;
                //шрифт
                FontStyle style = FontStyle.Regular;
                if (item.list_table[i].Option.IsItalic) style = FontStyle.Italic;
                font[i] = new Font(GetFont(item.list_table[i].Option.Font), Convert.ToInt32(item.list_table[i].Option.TextSize), style);                
            }

            float k = (graph.ClientPageSize.Width - 1) / sum_width;

            for (int i = 0; i < columnwidth.Length; i++)
            {
                columnwidth[i] = columnwidth[i] * k;
            }


            graph.BorderColor = Color.Black;
            graph.BorderWidth = 1;
            float y = 0;
            float max_height = 0;
            float width = (graph.ClientPageSize.Width - 1) / 2;
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                float x = 0;
                graph.Font = font[0];
                max_height = GridLink.getMaxRowHeight(graph, dt.Rows[r], columnwidth);
                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    graph.StringFormat = brickSFormat[r];
                    graph.BackColor = backgroundColor[r];
                    graph.Font = font[r];

                    graph.DrawString(dt.Rows[r][c].ToString(), Color.Black,
                        new RectangleF(x, y, columnwidth[c],
                        max_height),
                        side);
                    x += columnwidth[c];
                }
                y += max_height;
            }
        }            
    }
}
