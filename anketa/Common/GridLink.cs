﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting;
using aetp.Common.ControlModel;
using System.Data;

namespace aetp.Common.Control
{
    public class GridLink : DevExpress.XtraPrinting.Link
    {
        internal int top = 0;
        Grid grid;
        DataTable dt;
        Font font;
        float[] columnwidth;
        bool calcOptimal;

        public GridLink() : base()
        {
            
        }

        public GridLink(Grid grid, Font font, bool calcOptimal)
            : base()
        {
            this.grid = grid;
            dt = PrepareDataTableFromGrid(this.grid);
            this.font = font;
            this.calcOptimal = calcOptimal;
        }

        protected override void CreateReportHeader(BrickGraphics graph)
        {
            base.CreateReportHeader(graph);
            columnwidth = getColumnWidth(graph, dt, calcOptimal);
            if (grid.HideHeader) return;
            graph.BackColor = Color.Gray;
            graph.BorderColor = Color.Black;
            graph.BorderWidth = 1;
            float x = 0;
            float max_height = 0;
            int i = 0;
            foreach (var col in grid.Columns)
            {
                SizeF textSize = graph.MeasureString(col.Caption, (int)columnwidth[i]);
                if (textSize.Height > max_height)
                {
                    max_height = textSize.Height;
                }
                i++;
            }

            for (int c = 0; c < grid.Columns.Count; c++)
            {
                string caption = grid.Columns[c].Caption;
                graph.DrawString(caption, Color.Black,
                    new RectangleF(x, 0, columnwidth[c], max_height),
                    BorderSide.All);
                x += columnwidth[c];
            }
        }

        protected override void CreateDetail(BrickGraphics graph)
        {
            graph.Font = font;
            graph.BackColor = Color.Transparent;
            graph.BorderColor = Color.Black;
            graph.BorderWidth = 1;
            float y = 0;
            float max_height = 0;
            for (int r = 0; r < grid.Rows.Count; r++)
            {
                float x = 0;
                max_height = getMaxRowHeight(graph, dt.Rows[r], columnwidth);
                for (int c = 0; c < grid.Rows[r].Cells.Count; c++)
                {
                    graph.DrawString(dt.Rows[r][c].ToString(), Color.Black,
                        new RectangleF(x, y, columnwidth[c], 
                        max_height), 
                        BorderSide.All);
                    x += columnwidth[c];
                }
                y += max_height;
            }
        }

        public static float getMaxRowHeight(BrickGraphics graph, DataRow row, float[] columnswidth)
        {
            float width = (graph.ClientPageSize.Width - 1) / row.Table.Columns.Count;
            float max_height = 0;
            for (int i = 0; i < row.Table.Columns.Count; i++)
            {
                SizeF textSize = graph.MeasureString(row[i].ToString(), (int)columnswidth[i]);
                if (textSize.Height > max_height)
                {
                    max_height = textSize.Height;
                }
            }
            return max_height;
        }
        static float min_col_width = 50;
        public static float[] getColumnWidth(BrickGraphics graph, DataTable dt, bool calcOptimal = true)
        {            
                if (calcOptimal)
                {
                    return getOptimalColumnWidth(graph, dt);
                }
                else
                {
                    float width = (graph.ClientPageSize.Width - 1) / dt.Columns.Count;
                    float[] result = new float[dt.Columns.Count];
                    for (int i = 0; i < result.Length; i++)
                    {
                        result[i] = width;
                    }
                    return result;
                }            
        }

        static float[] getOptimalColumnWidth(BrickGraphics graph, DataTable dt)
        {
            float[] result = new float[dt.Columns.Count];
            float width = (graph.ClientPageSize.Width - 1) / dt.Columns.Count;
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    float cellwidth = graph.MeasureString(dt.Rows[r][c].ToString()).Width + 1;
                    if (cellwidth > result[c])
                    {
                        if (cellwidth < min_col_width)
                        {
                            cellwidth = min_col_width;
                        }
                        result[c] = cellwidth; 
                    }
                }
            }
            List<int> columnForMinimize = new List<int>();
            for (int i = 0; i < result.Length; i++)
            {
                if (result[i] < width)
                {
                    columnForMinimize.Add(i);
                }
            }
            if (columnForMinimize.Count < result.Length)
            {
                float planwidth = columnForMinimize.Count * width;
                float currentwidth = 0;
                foreach (int i in columnForMinimize)
                {
                    currentwidth += result[i];
                }
                float delta = (planwidth - currentwidth) / (result.Length - columnForMinimize.Count);
                for (int i = 0; i < result.Length; i++)
                {
                    if (!columnForMinimize.Contains(i))
                    {
                        result[i] = width + delta;
                    }
                }
            }
            else
            {
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = width;
                }
            }
            return result;
        }

        /// <summary>
        /// Приведение данных в фиксированных таблицах к виду необходимому для печати/экспорта
        /// Например поменять разделительв дробных числах
        /// </summary>
        /// <param name="grid">Grid</param>
        /// <param name="dt">DataTable</param>
        /// <returns></returns>
        DataTable PrepareDataTableFromGrid(Grid grid)
        {
            DataTable newdt = new System.Data.DataTable();

            for (int r = 0; r < grid.Columns.Count; r++)
            {
                DataColumn col = new System.Data.DataColumn();
                col.ColumnName = grid.Columns[r].Name;
                newdt.Columns.Add(col);
            }
            if (grid.Data == null)
                return newdt;
            for (int r = 0; r < grid.Rows.Count; r++)
            {
                DataRow dr = newdt.NewRow();
                for (int c = 0; c < grid.Columns.Count; c++)
                {
                    if (grid.Rows[r].Cells[c].Type == GridCellType.Label)
                    {
                        dr[c] = grid.Rows[r].Cells[c].Caption;
                        continue;
                    }
                    if (grid.Rows[r].Cells[c].EditValue != null)
                    {
                        if (grid.Rows[r].Cells[c].Type == GridCellType.Double)
                        {
                            dr[c] = grid.Rows[r].Cells[c].EditValue.ToString().Replace(".", ",");

                            dr[c] = Printing.FormatDoubleForPrint(dr[c].ToString());
                        }
                        else if (grid.Rows[r].Cells[c].Type == GridCellType.Date)
                        {
                            dr[c] = Convert.ToDateTime(grid.Rows[r].Cells[c].EditValue.ToString()).ToShortDateString();
                        }
                        else if (grid.Rows[r].Cells[c].Type == GridCellType.Check)
                        {

                            bool value = (bool)Convert.ToBoolean(grid.Rows[r].Cells[c].EditValue.ToString());
                            if (value == true)
                                dr[c] = "Да";
                            else
                                dr[c] = "Нет";

                        }
                        else if (grid.Rows[r].Cells[c].Type == GridCellType.List)
                        {
                            var item = ReportUtils.GetDictionaryItemDescription(grid.Rows[r].Cells[c].Source, grid.Rows[r].Cells[c].EditValue.ToString());
                            if (item != null)
                                dr[c] = item.ToString();
                        }
                        else if (grid.Rows[r].Cells[c].Type == GridCellType.Integer)
                        {
                            dr[c] = grid.Rows[r].Cells[c].EditValue.ToString();
                            dr[c] = Printing.FormatDoubleForPrint(dr[c].ToString());

                        }
                        else dr[c] = grid.Rows[r].Cells[c].EditValue.ToString();

                    }
                    else if (grid.Rows[r].Cells[c].Type == GridCellType.Date)
                    {
                        DateTime date;
                        if (grid.Rows[r].Cells[c].EditValue != null && DateTime.TryParse(grid.Rows[r].Cells[c].EditValue.ToString(), out date))
                        {
                            dr[c] = date.ToString("dd.MM.yyyy");
                        }
                    }
                    else if (grid.Rows[r].Cells[c].Type == GridCellType.List)
                    {
                        dr[c] = (grid.Rows[r].Cells[c].EditValue ?? "").ToString();
                    }

                }
                newdt.Rows.Add(dr);
            }
            return newdt;
        }
    }
}
