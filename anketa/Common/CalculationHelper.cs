﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using NCalc;
using aetp.Common.ControlModel;
using System.ComponentModel;
using System.Globalization;

namespace aetp.Common
{
	
	public class CalculationHelper
	{
		
		public Dictionary<String, GridCellItem> Cells = new Dictionary<string, GridCellItem>();
        public Dictionary<String, GridAddedRowColumn> Columns = new Dictionary<string, GridAddedRowColumn>();
		public Dictionary<String, GridCellItem> Formulas = new Dictionary<string, GridCellItem>();

		public Hashtable Grid2DT = new Hashtable();

		public CalculationHelper()
		{
            DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
		}

		public CalculationHelper(Hashtable Grid2DT): this()
		{
			this.Grid2DT = Grid2DT;

			foreach (DictionaryEntry dictionaryEntry in Grid2DT)
			{
				GetAllCellsAndColumns(dictionaryEntry.Key as BaseGrid, Cells, Columns, Formulas);
			}
		}

		public void AddToCollection(BaseGrid baseGrid, DataTable dt)
		{
			Grid2DT[baseGrid] = dt;
			GetAllCellsAndColumns(baseGrid, Cells, Columns, Formulas);
		}

		public bool CheckFormula(string formula, Grid Grid)
		{
			if ((formula ?? string.Empty) == string.Empty) return true;

            try
            {
                AddToCollection(Grid, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return false;
            }

			string t = CalculateFormula(formula, true);

			string[] tmp =
				t.Replace("sum", "")
					   .Replace("avg", "")
					   .Replace("min", "")
					   .Replace("max", "")
					   .Replace("count", "")
					   .Split('(', ')', '*', '/', '+', '-',':');

			foreach (string s in tmp)
			{
				string ss = s.Trim();

				if (ss == string.Empty) continue;

				if (Cells.Keys.Contains(ss) && Columns.Keys.Contains(ss))
				{
					MessageBox.Show("Неоднозначное имя ячейки/колонки в формуле");
					return false;
				}

				if (!Cells.Keys.Contains(ss) && !Columns.Keys.Contains(ss))
				{
					MessageBox.Show("Имя ячейки/колонки в формуле не существует в таблице");
					return false;
				}
			}

			return true;
		}

		public void GetAllCellsAndColumns(BaseGrid Grid,
											Dictionary<String, GridCellItem> Cells,
											Dictionary<String, GridAddedRowColumn> Columns,
											Dictionary<String, GridCellItem> Formulas
		)
		{
            lock (Formulas)
            {
                if (Grid is MetaGrid)
                {
                    foreach (BaseGrid bg in (Grid as MetaGrid).Tables)
                    {
                        GetAllCellsAndColumns(bg, Cells, Columns, Formulas);
                    }
                }
                else if (Grid is GridAddedRow)
                {
                    #region GridAddedRow
                    GridAddedRow grid = Grid as GridAddedRow;

                    foreach (GridAddedRowColumn col in grid.Columns)
                    {
                        if (col.Type == GridCellType.Double || col.Type == GridCellType.Integer)
                        {
                            Columns.Add(GetColumnName(col), col);
                        }
                    }
                    #endregion
                }
                else if (Grid is Grid)
                {
                    #region Grid
                    Grid grid = Grid as Grid;

                    foreach (GridRow row in grid.Rows)
                    {
                        foreach (GridCellItem cell in row.Cells)
                        {
                            if (!Cells.ContainsKey(cell.Name))
                            {
                                Cells.Add(cell.Name, cell);
                                try
                                {
                                    if (cell.FormulaType == FormulaType.Calculate && (cell.FormulaForCalculating ?? String.Empty) != String.Empty)
                                    {
                                        Formulas.Add(CalculateFormula(cell.FormulaForCalculating, true), cell);
                                    }
                                    else if (cell.FormulaType == FormulaType.Validate && cell.CellValidating.ComparisonValueType == ComparisonValueType.Formula &&
                                        (cell.CellValidating.FormulaForValidating ?? string.Empty) != String.Empty)
                                    {
                                        Formulas.Add(CalculateFormula(cell.CellValidating.FormulaForValidating, true), cell);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(string.Format("Ошибка добавления формулы в ячейку {0}. {1}", cell.Name, ex.Message));
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
		}

		public void SetCellValue<T>(GridCellItem cell, T value)
		{
			foreach (DictionaryEntry dictionaryEntry in Grid2DT)
			{
				if (dictionaryEntry.Key is Grid)
				{
					Grid grid = dictionaryEntry.Key as Grid;
                    foreach (GridRow gridRow in grid.Rows)
					{
                        if (gridRow.Cells.IndexOf(cell) >= 0)
						{
                            try
                            {
                                int index = grid.Rows.IndexOf(gridRow);
                                grid.Data.Rows[index][gridRow.Cells.IndexOf(cell)] = value;
                                grid.RaiseRefreshing(grid);
                                //(dictionaryEntry.Value as DataTable).AcceptChanges();
                                UpdateTotalAsync(cell);
                                return;
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
						}
					}                    
				}
			}
		}

		public void UpdateTotal(object sender)
		{
            new Action<object>(UpdateTotalAsync).Invoke(sender);
		}

        public void UpdateTotalAsync(object sender)
        {
            string cell_name = "";
            if (sender is GridCellItem)
            {
                cell_name = (sender as GridCellItem).Name;
            }
            else if (sender is string)
            {
                cell_name = sender as string;
            }
            else
            {
                return;
            }

            foreach (KeyValuePair<string, GridCellItem> keyValuePair in Formulas)
            {
                if (keyValuePair.Key.Contains(cell_name.Trim()))
                {
                    RecalcCellValue(keyValuePair.Value);
                    //не выходим т.к. ячейка может участвовать в нескольких формулах
                }
            }
        }

        object GetCellValue(GridCellItem cell)
        {
            object value = "NaN";
            if (cell.FormulaType == FormulaType.Calculate && (cell.FormulaForCalculating ?? String.Empty) != String.Empty)
            {
                value = CalculateFormula(cell.FormulaForCalculating);
            }
            else if (cell.FormulaType == FormulaType.Validate && (cell.CellValidating.FormulaForValidating ?? String.Empty) != String.Empty)
            {
                value = CalculateFormula(cell.CellValidating.FormulaForValidating);
            }
            return value;
        }

        void AssignCellValue(GridCellItem cell, object value)
        {
            if (value.ToString() != "NaN" && value.ToString() != string.Empty)
            {
                double result = 0;
                try
                { 
                    double.TryParse(new Expression(value.ToString().Replace(',', '.')).Evaluate().ToString(), out result);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                                
                if (cell.FormulaType == FormulaType.Calculate)
                {
                    cell.EditValue = result;
                    SetCellValue(cell, result);
                }
                else
                {
                    cell.CellValidating.ValidatingCalculationValue = (double)result;
                }
            }
        }

        public void RecalcCellValue(GridCellItem cell)
		{
            lock (cell)
            {
                object returned = GetCellValue(cell);
                AssignCellValue(cell, returned);
            }
		}

        static string ObjectName = @"[a-zA-Zа-яА-Я]{1}[a-zA-Zа-яА-Я0-9_\$]*";

		static string OnlyObjectName = string.Format("^{0}$", ObjectName);
		static string RangeName = string.Format("^{0}:{0}$", ObjectName);

		readonly string[] begin_chars = new string[] { "^", @"[\s+\-*/)]{1}" };
		readonly string[] end_chars = new string[] { "$", @"[\s+\-*/)]{1}" };
		readonly string[] funcs = new string[] { "sum", "avg", "count" };
		readonly string[] func_value = new string[] { "({0})", "({0}):({0})" };

		private string CalculateFormula(string formula, bool OnlyExpandFormula = false)
		{
			List<string> masks = new List<string>();

			foreach (string beginChar in begin_chars)
			{
				foreach (string func in funcs)
				{
					foreach (string fv in func_value)
					{
						foreach (string endChar in end_chars)
						{
							string tmp = string.Format(@"{0}((?'func'{2})[/(]{1}(?'func_value'{3})[/)]{1}){4}", beginChar, "{1}", func, fv, endChar);
							masks.Add(string.Format(tmp, ObjectName, "{1}"));
						}
					}
				}
			}

            for (int i = 0; i < masks.Count; i++)
            {
                foreach (string mask in masks)
                {
                    bool isReplaced = false;
                    formula = RegexReplace(formula, mask, OnlyExpandFormula, ref isReplaced);
                    if (isReplaced)
                        i = 0;
                }
            }
            

			if (formula == "NaN")
			{
				return formula;
			}
			else
			{
				if (OnlyExpandFormula) return formula;
				else return ReplaceCells(formula);	
			}
		}

		string ReplaceCells(string formula)
		{
			String[] excludeStrings = new string[]
				{
					"sum", "avg", "min", "max", "count"
				};

			for (int i = 0; i < excludeStrings.Length; i++)
			{
				formula = formula.Replace(excludeStrings[i], "");
			}
			string[] tmp = formula.Split('(', ')', '*', '/', '+', '-');

			double result = 0;

			foreach (string s in tmp)
			{
				string ss = s.Trim();

				if (ss == string.Empty || double.TryParse(ss.Replace('.', ',').TrimEnd(','), out result)) continue;

                if (!Cells.ContainsKey(ss))
                    continue;

                //if (Cells[ss].EditValue == null)
                //{
                //    Cells[ss].EditValue = 0;
                //}

                object val = Cells[ss].EditValue ?? 0;

                if (val.ToString() == ".")
                    val = "0";
                if (double.TryParse((val ?? "").ToString().Replace('.', ',').TrimEnd(','), out result))
                {
                    if (GetTrancate(result) > 0)                    
                    formula = formula.Replace(ss, result.ToString());
                    else formula = formula.Replace(ss, result.ToString() + ".00");
                }
                else
                {
                    return "NaN";
                }
			}
			return formula;
		}

        double GetTrancate(double d)
        {
            int i = (int)d;
            return d - i;
        }

		string RegexReplace(string formula, string mask, bool OnlyExpandFormula, ref bool IsReplaced)
		{
			Regex regex = new Regex(mask);
            IsReplaced = false;
			foreach (Match match in regex.Matches(formula))
			{
				GroupCollection groups = match.Groups;
				string S1 = "", S2 = "", func = "";

				foreach (string groupname in regex.GetGroupNames())
				{
					if (groupname == "func_value" && Regex.IsMatch(groups[groupname].Value, OnlyObjectName))
					{
						S1 = groups[groupname].Value.ToString();
						S2 = "";
					}
					else if (groupname == "func_value" && Regex.IsMatch(groups[groupname].Value, RangeName))
					{
						S1 = groups[groupname].Value.ToString().Split(':')[0];
						S2 = groups[groupname].Value.ToString().Split(':')[1];
					}

					if (groupname == "func" && (groups[groupname].Value == "sum" || groups[groupname].Value == "count" || groups[groupname].Value == "avg"))
					{
						func = groups[groupname].Value;
					}
				}

				if (S1 != string.Empty && func != string.Empty)
				{
					formula = formula.Replace(
								string.Format("{0}({1})", func, groups["func_value"]),
								CalcRange(S1, S2, func, OnlyExpandFormula).ToString()
								);
                    IsReplaced = true;
				}
			}
			return formula;
		}

		string CalcRange(string S1, string S2, string func, bool OnlyExpandFormula)
		{
			decimal sum = 0, count = 0;
			decimal tmp;
			string cells = "";

			if (S2 == "")
			{
				if (OnlyExpandFormula) return S1;

				#region столбец
				//ищем данный столбец и проверяем тип

                string[] names = S1.Split(TableNameSplitter);
                string table_name = names[0];
                string column_name = names[1];
				foreach (DictionaryEntry dictionaryEntry in Grid2DT)
				{
                    if (dictionaryEntry.Key is GridAddedRow && (dictionaryEntry.Key as GridAddedRow).Name == table_name)
					{
						GridAddedRow grid = dictionaryEntry.Key as GridAddedRow;

						if (grid.Columns.Contains(Columns[S1]))
						{
							foreach (DataRow row in grid.Data.Select())
							{
								if (decimal.TryParse(row[column_name].ToString(), out tmp))
								{
									sum += tmp;
									count += 1;
								}
							}

							break;
						}
					}
				}
				#endregion
			}
			else
			{
				#region ячейка
				//ищем координаты ячеек и проверяем весь диапазон на соответствие типа

				int row1 = -1, row2 = -1, col1 = -1, col2 = -1;

				foreach (DictionaryEntry dictionaryEntry in Grid2DT)
				{
					if (dictionaryEntry.Key is Grid)
					{
						Grid grid = dictionaryEntry.Key as Grid;

						foreach (GridRow gridRow in grid.Rows)
						{
                            try
                            {
                                if (gridRow.Cells.Contains(Cells[S1]))
                                {
                                    row1 = grid.Rows.IndexOf(gridRow);
                                    col1 = gridRow.Cells.IndexOf(Cells[S1]);
                                }

                                if (gridRow.Cells.Contains(Cells[S2]))
                                {
                                    row2 = grid.Rows.IndexOf(gridRow);
                                    col2 = gridRow.Cells.IndexOf(Cells[S2]);
                                }
                            }
                            catch
                            {
                                return "NaN";
                            }
						}

						if (col1 + col2 + row1 + row2 == -4)
						{
							//пока не нашли. продолжаем
							continue;
						}
						else if (col1 + row1 == -2 || col2 + row2 == -2)
						{
							//ячейки принадлежат разным объектам
							MessageBox.Show("Ячейки принадлежат разным таблицам!", "Ошибка");
							return "NaN";
						}
						else if (row1 == row2 && col1 == col2)
						{
							MessageBox.Show("Ячейки принадлежат разным таблицам!", "Ошибка");
						}
						else
						{
							if (OnlyExpandFormula) //нашли. разворачиваем формулу
							{
								for (int row = row1; row <= row2; row++)
								{
									for (int col = col1; col <= col2; col++)
									{
										cells += (cells == string.Empty ? "" : "+") + grid.Rows[row].Cells[col].Name;
									}
								}	
							}
							else //нашли. считаем.
							{								
								for (int row = row1; row <= row2; row++)
								{
									for (int col = col1; col <= col2; col++)
									{
										if (grid.Rows[row].Cells[col].Type != GridCellType.Double &&
											grid.Rows[row].Cells[col].Type != GridCellType.Integer)
										{
											continue;
										}

										if (decimal.TryParse((grid.Rows[row].Cells[col].EditValue ?? "").ToString().Replace('.', ',').TrimEnd(','), out tmp))
										{
											sum += tmp;
											count += 1;
										}
									}
								}	
							}
							

							break;
						}
					}
				}
				#endregion
			}

			if (OnlyExpandFormula)
			{
				return cells;
			}

			if (func == "sum")
			{
				return Convert.ToString(sum);
			}
			else if (func == "count")
			{
				return Convert.ToString(count);
			}
			else if (func == "avg")
			{
				if (count == 0) 
				{
					return "NaN";
				}
				else
				{
					return Convert.ToString(sum / count);	
				}
			}
			else
			{
				return "NaN";	
			}
		}

		internal void SetEvent(DevExpress.XtraEditors.Repository.RepositoryItem repositoryItem, GridCellItem se)
		{
			if (!Cells.Keys.Contains(se.Name)) return;

            repositoryItem.EditValueChanged -= delegate(object sender, EventArgs args)
            {
                se.EditValue = (sender as DevExpress.XtraEditors.BaseEdit).EditValue;
                UpdateTotal(se);
            };

			repositoryItem.EditValueChanged += delegate(object sender, EventArgs args)
			{
                se.EditValue = (sender as DevExpress.XtraEditors.BaseEdit).EditValue;
				UpdateTotal(se);
			};
		}

		internal void SetEvent(DevExpress.XtraEditors.Repository.RepositoryItem repositoryItem, GridAddedRowColumn col)
		{
			if (!Columns.Keys.Contains(GetColumnName(col))) return;

			repositoryItem.EditValueChanging += delegate(object sender, ChangingEventArgs args)
			{
				UpdateTotal(col);
			};
		}

        string GetColumnName(GridAddedRowColumn col)
        {
            return String.Format("{0}{2}{1}", col.Grid.Name, col.Name, TableNameSplitter);
        }

        public static char TableNameSplitter
        {
            get
            {
                return '$';
            }
        }
	}
}