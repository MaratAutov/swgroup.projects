﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;

namespace aetp.Common
{
    public class ModelConfig
    {
        const string ConfigFileName = "ModelConfig.xml";
        public string ModelsPath { get; set; }
        public string Version = "";
        public string SuspedVersion = "";

        public struct ModelInfo  
        {
            
            public string Name;
            string _path;
            
            public string Path
            {
                get { return _path; }
                set 
                { 
                    _path = value;
                }
            }
            public string Version;
            public string Description;
            public string Tag;
            public string Hash;
            public bool Visible;

            public void GenerateHash()
            {
                var md5 = MD5.Create();
                byte[] result = md5.ComputeHash(File.ReadAllBytes(_path));
                Hash = BitConverter.ToString(result).Replace("-", string.Empty);
                md5.Dispose();
            }

            public void GenerateHash(string path)
            {
                var md5 = MD5.Create();
                byte[] result = md5.ComputeHash(File.ReadAllBytes(path));
                Hash = BitConverter.ToString(result).Replace("-", string.Empty);
                md5.Dispose();
            }
        }
        public List<ModelInfo> ListModelInfo {get; set; }

        public class ModelInfoSearcher
        {
            private string name;
            private string path;
            private string version;
            private string tag;
            
            public ModelInfoSearcher(string name, string path, string version,string tag)
            {
                this.name = name;
                this.path = path;
                this.version = version;
                this.tag = tag;
                
            }

            public string Path
            {
                get { return path; }
                set { path = value; }
            }

            public string Version
            {
                get { return version; }
                set { version = value; }
            }

            public string Name
            {
                get { return name; }
                set { name = value; }
            }

            public string Tag
            {
                get { return tag; }
                set { tag = value; }
            }

            public bool Condition(ModelConfig.ModelInfo modelinfo)
            {
                return modelinfo.Name == name && modelinfo.Path == path && modelinfo.Version == version && modelinfo.Tag == tag;
            }

            public bool Condition_Save(ModelConfig.ModelInfo modelinfo)
            {
                return (modelinfo.Version == version || (modelinfo.Version == "" && version == null)) && modelinfo.Tag == tag;
            }
        }

        public ModelConfig()
        {
            // init default 
            ModelsPath = Path.Combine(aetp.Utils.Config.GetCommonAppPath(), "Models");
            if (!Directory.Exists(ModelsPath))
                Directory.CreateDirectory(ModelsPath);
        }
                
        public string GetConfigFilePath()
        {
            return Path.Combine(aetp.Utils.Config.GetAppPath(), ConfigFileName);
        }

        public void Save()
        {
            FileStream fs = new FileStream(GetConfigFilePath(), FileMode.Create);
            XmlSerializer serializer = new XmlSerializer(typeof(ModelConfig));
            serializer.Serialize(fs, this);
            fs.Close();
        }

        public void Load()
        {
            string fileName = GetConfigFilePath();
            if (!File.Exists(fileName))
            {
                Save();
            }
            FileStream fs = new FileStream(fileName, FileMode.Open);
            XmlSerializer serializer = new XmlSerializer(typeof(ModelConfig));
            ModelConfig cfg = (ModelConfig)serializer.Deserialize(fs);
            fs.Close();
            Version = cfg.Version;
            SuspedVersion = cfg.SuspedVersion;
            //ModelsPath = cfg.ModelsPath;
            ListModelInfo = cfg.ListModelInfo;
        }

        public void AddModel(string name, string path, string version, string description, string tag, bool visible = true)
        {
            ModelConfig.ModelInfo item = new ModelConfig.ModelInfo();
            item.Name = name;
            item.Path = path;
            item.Version = version;
            item.Description = description;
            item.Tag = tag;
            item.GenerateHash();
            item.Visible = visible;
            //AppGlobal.Instance.Config.ListModelInfo.Remove(AppGlobal.Instance.Config.ListModelInfo.Find(new ModelInfoSearcher(name, path, version,tag).Condition));
            AppGlobal.Instance.Config.ListModelInfo.Add(item);
            //AppGlobal.Instance.Config.Save();
        }
    }
}
