﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting;
using aetp.Common.ControlModel;
using System.Data;
using System.Windows;

namespace aetp.Common.Control
{
    public class EmptyGridLinkTemplate : DevExpress.XtraPrinting.Link
    {      
        float[] columnwidth;        
        PrintTableCollectionItem item;
        Font font;

        public EmptyGridLinkTemplate(PrintTableCollectionItem item, Font font)
        {          
            this.item = item;
            this.font = font;
        }

        protected override void CreateReportHeader(BrickGraphics graph)
        {
            base.CreateReportHeader(graph);            
        }

        protected override void CreateDetail(BrickGraphics graph)
        {
            columnwidth = new float[item.Columns.Count];
            float sum_width = 0;
            for (int j = 0, i = item.Columns.Count - 1; i > -1; i--, j++)
            {
                columnwidth[j] = item.Columns[i].Width;
                sum_width += item.Columns[i].Width;
            }

            float k = (graph.ClientPageSize.Width -1 )/ sum_width;

            for (int i = 0; i < columnwidth.Length; i++)
            {
                columnwidth[i] = columnwidth[i] * k;                
            }            

            graph.BackColor = Color.Transparent;
            graph.Font = font;
            graph.BorderColor = Color.Black;
            graph.BorderWidth = 1;
            float y = 0;
            float max_height = 0;
            float width = (graph.ClientPageSize.Width - 1) / item.Columns.Count;
            for (int r = 0; r < 1; r++)
            {
                float x = 0;
                max_height = 20;
                for (int c = 0; c < item.Columns.Count; c++)
                {
                    graph.DrawString("", Color.Black,
                        new RectangleF(x, y, columnwidth[c], 
                        max_height), 
                        BorderSide.All);
                    x += columnwidth[c];
                }
                y += max_height;
            }
        }

        /// <summary>
        /// Приведение данных в таблицах с добавлением строк к виду необходимому для печати/экспорта
        /// Например поменять разделительв дробных числах
        /// </summary>
        /// <param name="grid">GridAddedRow</param>
        /// <param name="dt">DataTable</param>
        /// <returns></returns>
        
    }
}
