﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;

namespace aetp.Common.Validator
{
    [DataContract]
    public class RegexValidationItem : IValidationItem
    {
        [DataMember]
        MaskItem mask;
        [DataMember]
        Regex regex;

        public bool Editable { get { return true; }}

        public RegexValidationItem(MaskItem mask)
        {
            this.mask = mask;
            regex = new Regex(mask.Mask);
        }
        
        public string Execute(object value)
        {
            if (value != null && value.ToString() != "" && !regex.IsMatch(value.ToString()))
                return mask.ErrorMsg;
            return null;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
