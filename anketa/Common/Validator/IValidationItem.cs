﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aetp.Common.Validator
{
    public interface IValidationItem
    {
        string Execute(object value);
        object Clone();
        bool Editable { get; }
    }
}
