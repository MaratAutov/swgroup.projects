﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aetp.Common.Validator
{
    public class EmptyValidationItem : IValidationItem
    {
        ValidatorItemType itemType = ValidatorItemType.str;
        public bool Editable { get { return false; } }

        string ErrMsg 
        {
            get
            {
                switch(itemType)
                {
                    case ValidatorItemType.num:
                        return "Значение должно быть установлено. При необходимости установите его в ноль";
                    case ValidatorItemType.str: 
                    default:
                        return "Значение должно быть установлено.";
                }
            }
        }

        public EmptyValidationItem()
        {

        }

        public EmptyValidationItem(ValidatorItemType type)
        {
            itemType = type;
        }

        public string Execute(object value)
        {
            if (value == null || value == DBNull.Value || String.IsNullOrEmpty(value.ToString()))
            {
                return ErrMsg;
            }
            return null;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public enum ValidatorItemType
    {
        str = 0,
        num
    }
}
