﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using aetp.Common.ControlModel.TypeConverters;
using aetp.Common.ControlModel.Attributes;
using System.Text.RegularExpressions;

namespace aetp.Common.Validator
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    [DisplayName("Маска")]
    public class MaskItem
    {
        // пользовательского типа не должно быть что бы он не перетерал значения     

        public MaskItem()
        {
            MaskType = "Пользовательский";
        }

        public string ToXSDPattern()
        {
            if (String.IsNullOrEmpty(Mask))
                return Mask;
            string pattern = Mask;
            if (pattern.StartsWith("^"))
                pattern = pattern.Substring(1);
            if (pattern.EndsWith("$"))
                pattern = pattern.Remove(pattern.Length - 1);

            while (pattern.Contains('^'))
            {
                pattern = pattern.Remove(pattern.IndexOf('^'), 1);
            }

            while (pattern.Contains('$'))
            {
                pattern = pattern.Remove(pattern.IndexOf('$'), 1);
            }

            Regex re = new Regex(@"^\(.*\)$");
            if (re.IsMatch(pattern))
            {
                pattern = pattern.Substring(1);
                pattern = pattern.Remove(pattern.Length - 1);
            }
            return pattern;
        }        

        string _maskType;
        [DisplayName("Тип маски")]
        [TypeConverter(typeof(MaskConverter))]
        public string MaskType
        {
            get
            {
                return _maskType;
            }
            set
            {
                _maskType = value;                

                foreach (MaskConfig.MaskInfo item in AppGlobal.Instance.MaskConfig.ListMaskInfo)
                {
                    if (item.Name == value)
                    {                        
                        _mask = item.Mask;
                        _description = item.Description;
                        _errorMsg = item.ErrorMessage;
                    }
                }
                RaiseMaskChanged();
            }
        }

        void resetToUserMaskType()
        {
            MaskType = "Пользовательский";
        }

        string _mask;
        [DisplayName("Маска")]
        public string Mask 
        {
            get
            {
                return _mask;
            }
            set
            {
                if (_mask == value)
                    return;
                _mask = value;
                resetToUserMaskType();
                RaiseMaskChanged();
            }
        }

        public event EventHandler MaskChanged;
        protected void RaiseMaskChanged()
        {
            if (MaskChanged != null)
                MaskChanged(this, null);
        }

        string _errorMsg;
        [DisplayName("Сообщение при ошибке")]
        public string ErrorMsg 
        {
            get
            {
                return _errorMsg;
            }
            set
            {
                if (_errorMsg == value)
                    return;
                _errorMsg = value;
                resetToUserMaskType();
            }
        }
        string _description;
        [DisplayName("Строка описания")]
        public string Description 
        {
            get
            {
                return _description;
            }
            set
            {
                if (_description == value)
                    return;
                _description = value;
                resetToUserMaskType();
            }
        }

        public override string ToString()
        {
            return Mask;
        }
    }  
    
}
