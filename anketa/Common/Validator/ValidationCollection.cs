﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors.DXErrorProvider;
using aetp.Common.ControlModel;
using aetp.Common.Control;
using System.Windows.Forms;

namespace aetp.Common.Validator
{
    public static class ValidationCollection
    {
        static Dictionary<string, List<ValidationItem>> validations = new Dictionary<string, List<ValidationItem>>();

        public static void Clear()
        {
            validations.Clear();
        }

        public static void ReInit(string guid)
        {
            if (validations.ContainsKey(guid))
            {
                List<ValidationItem> list = validations[guid];
                list.Clear();
                GC.Collect();
            }            
        }

        public static void RemoveWindow(string guid)
        {
            if (validations.ContainsKey(guid))
                validations.Remove(guid);
        }

        public static void RemovePage(Page page)
        {
            string guid = null;
            if (page is Report)
                guid = (page as Report).guid;
            else if (page.Root is Report)
                guid = (page.Root as Report).guid;
            else
                return;
            if (validations.ContainsKey(guid))
            {
                List<ValidationItem> list = validations[guid];
                List<ValidationItem> items = (from ValidationItem item in list where item.page == page select item).ToList();
                foreach (ValidationItem item in items)
                {
                    list.Remove(item);
                }
            }
        }

        public static void AddProvider(BaseControl control, DXErrorProvider provider)
        {
            string guid = (control.Root as Report).guid;
            if (!validations.ContainsKey(guid))
                validations.Add(guid, new List<ValidationItem>());
            
            List<ValidationItem> list = validations[guid];
            var item = list.Where(x => x.control == control).FirstOrDefault();
            if (item != null)
            {
                list.Remove(item);
            }
            list.Add(new ValidationItem() { control = control, provider = provider, page = control.ParentPage, type = ValidationItemType.provider });
        }        

        public static void AddGrid(IGrid control, BaseGrid grid)
        {
            string guid = (grid.Root as Report).guid;
            if (!validations.ContainsKey(guid))
                validations.Add(guid, new List<ValidationItem>());
            List<ValidationItem> list = validations[guid];
            var item = list.Where(x => x.control == grid).FirstOrDefault();
            if (item != null)
            {
                list.Remove(item);
            }
            list.Add(new ValidationItem() { control = grid, page = grid.ParentPage, type = ValidationItemType.grid, grid = control });
        }

        public static CustomErrorInfo GetError(string guid, Page page = null)
        {
            if (!validations.ContainsKey(guid))
                return null;
            List<ValidationItem> list = validations[guid];
            List<ValidationItem> items;
            if (page != null)
            {
                // ограничить коллекцию страницей
                items = (from ValidationItem item in list where item.page == page select item).ToList();
            }
            else
                items = list;
            foreach (ValidationItem item in items)
            {
                if (item.page.tlItem.Visible)
                {
                    if (item.control is SimpleRichEdit && (item.control as SimpleRichEdit).IsMandatoryToFill)
                    {
                        if (((item.control as SimpleRichEdit).EditValue == null) || ((item.control as SimpleRichEdit).EditValue.ToString() == ""))
                        {
                            CustomErrorInfo info = new CustomErrorInfo();
                            info.bs = item.control;
                            info.error = "Значение должно быть установлено";
                            info.page = item.page;
                            info.control = null;
                            return info;
                        }
                    }
                    if (item.HasError())
                    {
                        CustomErrorInfo error = item.GetError();
                        if (error != null)
                            return error;
                    }
                }
            }
            return null;
        }

        public static bool ExistsError(string guid, Page page = null)
        {
            if (!validations.ContainsKey(guid))
                return false;
            List<ValidationItem> list = validations[guid];
            List<ValidationItem> items;
            if (page != null)
            {
                // ограничить коллекцию страницей
                items = (from ValidationItem item in list where item.page == page select item).ToList();
            }
            else
                items = list;
            foreach (ValidationItem item in items)
            {
                if (item.page.tlItem.Visible)
                {
                    if (item.control is SimpleRichEdit && (item.control as SimpleRichEdit).IsMandatoryToFill)
                    {
                        if (((item.control as SimpleRichEdit).EditValue == null) || ((item.control as SimpleRichEdit).EditValue.ToString() == ""))
                        {
                            return true;
                        }
                    }
                    if (item.HasError())
                    {
                        CustomErrorInfo error = item.GetError();
                        if (error != null)
                            return true;
                    }
                }
            }
            return false;
        }

#warning Сделать добавление валидаторов тут, убрать подсветку невалидных разделов из TreeListMediator
        static void InitValidators(Report report)
        {
            ReportUtils.DoAction(report.Children, x =>
                {
                    if (x is SimpleEdit)
                    {
                        SimpleEditorValidation.AddValidation(x as SimpleEdit);
                    }
                    
                }
            );
        }
    }

    public class ValidationItem
    {
        public ValidationItemType type { get; set; }
        public DXErrorProvider provider { get; set; }
        public BaseControl control { get; set; }
        public Page page { get; set; }
        public IGrid grid { get; set; }

        public bool HasError()
        {
            // если элемент скрыт то не проверять его
            if (control.Parent is MetaConrol && !(control.Parent as MetaConrol).Visible)
                return false;
            if (type == ValidationItemType.provider)
                return provider.HasErrors;
            //грид всегда опрашивать
            return true;
        }

        public CustomErrorInfo GetError()
        {
            if (type == ValidationItemType.provider)
            {
                if (!provider.HasErrors)
                    return null;
                IList<System.Windows.Forms.Control> controls = provider.GetControlsWithError();
                if (controls.Count > 0)
                {
                    CustomErrorInfo info = new CustomErrorInfo();
                    info.control = controls[0];
                    info.error = provider.GetError(info.control);
                    info.type = provider.GetErrorType(info.control);
                    info.bs = control;
                    info.page = page;
                    return info;
                }


                
            }
            else if (type == ValidationItemType.grid)
            {
                ErrorInfo error = grid.GetErrorInfo();
                if (error != null && !String.IsNullOrEmpty(error.ErrorText))
                {
                    CustomErrorInfo info = new CustomErrorInfo();
                    info.bs = control;
                    info.error = error.ErrorText;
                    info.type = error.ErrorType;
                    info.page = page;
                    info.control = grid as System.Windows.Forms.Control;
                    return info;
                }
                if (grid is aetpGridAddedRow)
                {
                    if ((grid as aetpGridAddedRow).IsMandatoryToFill)
                    {
                        if ((grid as aetpGridAddedRow).gv.RowCount == 0)
                        {
                            CustomErrorInfo info = new CustomErrorInfo();
                            info.bs = control;
                            info.error = "Не добавлены строки в таблице";
                            info.page = page;
                            info.control = grid as System.Windows.Forms.Control;
                            return info;
                        }
                    }
                }
            }
            return null;
        }
    }

    

    public enum ValidationItemType
    {
        provider = 0,
        grid
    }

    public class CustomErrorInfo
    {
        public System.Windows.Forms.Control control { get; set; }
        public BaseControl bs { get; set; }
        public ErrorType type { get; set; }
        public string error { get; set; }
        public Page page { get; set; }
        public override string ToString()
        {
            string caption = "";

            if (bs != null)
            {
                if (bs is BaseGrid)
                {
                    if ((bs as BaseGrid).metagrid != null)
                        caption = (bs as BaseGrid).metagrid.Caption;
                    else caption = bs.Caption;
                }
                else caption = bs.Caption;
            }
            return String.Format("Раздел: {0};\nЭлемент {1};\nОшибка: {2};", page == null ? "" : page.Caption, bs == null ? "" : caption, error);
        }
    }    
}
