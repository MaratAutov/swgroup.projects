﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using DevExpress.XtraEditors;
using aetp.Common.Control;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Windows.Forms;
using System.Collections;

namespace aetp.Common.Validator
{
    public static class RichEditorValidation
    {
        //что бы не передавать как параметр с учетом того что приложение выполняется в один поток
        static ContainerControl container;
        static BindingSource bindingSource;

        public static void AddValidation(SimpleRichEdit se, XtraUserControl editor, ContainerControl container)
        {
            RichEditorValidation.container = container;
            //нужно что бы связать элемент управления с моделью не удалять
            if (addBinding(se, editor as aetpRichEdit))
            {
                if (se.IsMandatoryToFill)
                    AddValidationOnMandatoryFill(se, editor as aetpRichEdit);                
            }
        }

        static bool addBinding(SimpleRichEdit se, aetpRichEdit editor)
        {
            if (editor.Editor.DataBindings.Count == 0)
            {
                bindingSource = new BindingSource();
                bindingSource.DataSource = typeof(SimpleRichEdit);
                bindingSource.Add(se);
                editor.Editor.DataBindings.Add(new Binding("Text", bindingSource, "EditValue", false, DataSourceUpdateMode.OnPropertyChanged));
                return true;
            }
            else
                return false;
        }

        static void AddValidationOnMandatoryFill(SimpleRichEdit se, aetpRichEdit editor)
        {
            se.EditorValidators.Add(new EmptyValidationItem());
            AddErrorBinding(se, editor);
        }

        static void AddErrorBinding(SimpleRichEdit se, aetpRichEdit editor)
        {
            DXErrorProvider errorProvider = new DXErrorProvider();
            errorProvider.DataSource = bindingSource;
            errorProvider.ContainerControl = container;
            ValidationCollection.AddProvider(se, errorProvider);   
        }
    }
}
