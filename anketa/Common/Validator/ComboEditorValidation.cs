﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using DevExpress.XtraEditors;
using aetp.Common.Control;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Windows.Forms;

namespace aetp.Common.Validator
{
    public static class ComboEditorValidation
    {
        static ContainerControl container;
        static BindingSource bindingSource;

        public static void AddValidation(aetp.Common.ControlModel.ComboBox se, XtraUserControl editor, ContainerControl container)
        {
            ComboEditorValidation.container = container;
            if (addBinding(se, editor as aetpBaseEdit))
            {
                if (se.IsMandatoryToFill)
                    AddValidationOnMandatoryFill(se, editor as aetpBaseEdit);
            }
        }

        static bool addBinding(aetp.Common.ControlModel.ComboBox cb, aetpBaseEdit editor)
        {
            if (editor.Editor.DataBindings.Count == 0)
            {
                bindingSource = new BindingSource();
                bindingSource.DataSource = typeof(aetp.Common.ControlModel.BaseControl);
                bindingSource.Add(cb);
                editor.Editor.DataBindings.Add(new Binding("EditValue", bindingSource, "EditValue", false, DataSourceUpdateMode.OnPropertyChanged));
                return true;
            }
            else
                return false;
        }

        static void AddValidationOnMandatoryFill(aetp.Common.ControlModel.ComboBox se, aetpBaseEdit editor)
        {
            se.EditorValidators.Add(new EmptyValidationItem());
            AddErrorBinding(se, editor);
        }

        static void AddErrorBinding(aetp.Common.ControlModel.ComboBox se, aetpBaseEdit editor)
        {
            DXErrorProvider errorProvider = new DXErrorProvider();
            errorProvider.DataSource = bindingSource;
            errorProvider.ContainerControl = container;
            ValidationCollection.AddProvider(se, errorProvider);   
        }
    }
}
