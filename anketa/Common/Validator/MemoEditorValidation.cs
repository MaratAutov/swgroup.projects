﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using DevExpress.XtraEditors;
using aetp.Common.Control;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Windows.Forms;
using System.Collections;

namespace aetp.Common.Validator
{
    public static class MemoEditorValidation
    {
        //что бы не передавать как параметр с учетом того что приложение выполняется в один поток
        static ContainerControl container;
        static BindingSource bindingSource;

        public static void AddValidation(aetp.Common.ControlModel.MemoEdit se, XtraUserControl editor, ContainerControl container)
        {
            MemoEditorValidation.container = container;
            //нужно что бы связать элемент управления с моделью не удалять
            if (addBinding(se, editor as aetpBaseEdit))
            {
                if (se.IsMandatoryToFill)
                    AddValidationOnMandatoryFill(se, editor as aetpBaseEdit);
                if (!String.IsNullOrEmpty(se.Mask.Mask))
                    AddMaskValidation(se, editor as aetpBaseEdit);
            }
        }

        static bool addBinding(aetp.Common.ControlModel.MemoEdit se, aetpBaseEdit editor)
        {
            if (editor.Editor.DataBindings.Count == 0)
            {
                bindingSource = new BindingSource();
                bindingSource.DataSource = typeof(aetp.Common.ControlModel.BaseControl);
                bindingSource.Add(se);
                editor.Editor.DataBindings.Add(new Binding("EditValue", bindingSource, "EditValue", false, DataSourceUpdateMode.OnPropertyChanged));
                return true;
            }
            else
                return false;
        }

        static void AddMaskValidation(aetp.Common.ControlModel.MemoEdit se, aetpBaseEdit editor)
        {
            se.EditorValidators.Add(new RegexValidationItem(se.Mask));
            (editor.Editor as DevExpress.XtraEditors.MemoEdit).Properties.NullValuePrompt = se.Mask.Description;
            AddErrorBinding(se, editor);
        }

        static void AddValidationOnMandatoryFill(aetp.Common.ControlModel.MemoEdit se, aetpBaseEdit editor)
        {
            se.EditorValidators.Add(new EmptyValidationItem());
            AddErrorBinding(se, editor);
        }

        static void AddErrorBinding(aetp.Common.ControlModel.MemoEdit se, aetpBaseEdit editor)
        {
            DXErrorProvider errorProvider = new DXErrorProvider();
            errorProvider.DataSource = bindingSource;
            errorProvider.ContainerControl = container;
            ValidationCollection.AddProvider(se, errorProvider);   
        }
    }
}
