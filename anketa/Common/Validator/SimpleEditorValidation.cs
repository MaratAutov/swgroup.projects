﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using DevExpress.XtraEditors;
using aetp.Common.Control;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Windows.Forms;
using System.Collections;

namespace aetp.Common.Validator
{
    public static class SimpleEditorValidation
    {
        //что бы не передавать как параметр с учетом того что приложение выполняется в один поток
        static ContainerControl container;
        static BindingSource bindingSource;

        public static void AddValidation(SimpleEdit se, XtraUserControl editor, ContainerControl container)
        {
            SimpleEditorValidation.container = container;
            //нужно что бы связать элемент управления с моделью не удалять
            if (addBinding(se, editor as aetpBaseEdit))
            {
                if (se.IsMandatoryToFill)
                    AddValidationOnMandatoryFill(se, editor as aetpBaseEdit);
                if (se.DataType == SimpleDataType.String && !String.IsNullOrEmpty(se.Mask.Mask))
                    AddMaskValidation(se, editor as aetpBaseEdit);
            }
        }

        static bool addBinding(SimpleEdit se, aetpBaseEdit editor)
        {
            if (editor.Editor.DataBindings.Count == 0)
            {
                bindingSource = new BindingSource();
                bindingSource.DataSource = typeof(aetp.Common.ControlModel.BaseControl);
                bindingSource.Add(se);
                editor.Editor.DataBindings.Add(new Binding("EditValue", bindingSource, "EditValue", false, DataSourceUpdateMode.OnPropertyChanged));
                return true;
            }
            else
                return false;
        }

        static void AddMaskValidation(SimpleEdit se, aetpBaseEdit editor)
        {
            AddMaskValidation(se);
            (editor.Editor as TextEdit).Properties.NullValuePrompt = se.Mask.Description;
            AddErrorBinding(se, editor);
        }

        static void AddMaskValidation(SimpleEdit se)
        {
            /* mahalin 19-09-2013
             * Защита от возможного забития списка валидаторов дублями
             * вообще излишне так как поправил функцию клонирования валидаторов в BaseControl
             * но решил оставить для страховки
             */
            List<IValidationItem> list = se.EditorValidators.Where(x => { return x is RegexValidationItem; }).ToList();
            foreach (IValidationItem v in list)
            {
                se.EditorValidators.Remove(v);
            }

            se.EditorValidators.Add(new RegexValidationItem(se.Mask));
        }

        static void AddValidationOnMandatoryFill(SimpleEdit se, aetpBaseEdit editor)
        {
            AddValidationOnMandatoryFill(se);
            AddErrorBinding(se, editor);
        }

        static void AddValidationOnMandatoryFill(SimpleEdit se)
        {
            /* mahalin 19-09-2013
             * Защита от возможного забития списка валидаторов дублями
             * вообще излишне так как поправил функцию клонирования валидаторов в BaseControl
             * но решил оставить для страховки
             */
            List<IValidationItem> list = se.EditorValidators.Where(x => { return x is EmptyValidationItem; }).ToList();
            foreach (IValidationItem v in list)
            {
                se.EditorValidators.Remove(v);
            }

            se.EditorValidators.Add(new EmptyValidationItem());
        }

        public static void AddValidation(SimpleEdit se)
        {
            if (se.IsMandatoryToFill)
                AddValidationOnMandatoryFill(se);
            if (se.DataType == SimpleDataType.String && !String.IsNullOrEmpty(se.Mask.Mask))
                AddMaskValidation(se);
        }

        static void AddErrorBinding(SimpleEdit se, aetpBaseEdit editor)
        {
            DXErrorProvider errorProvider = new DXErrorProvider();
            errorProvider.DataSource = bindingSource;
            errorProvider.ContainerControl = container;
            ValidationCollection.AddProvider(se, errorProvider);   
        }
    }
}
