﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using aetp.Common.ControlModel;

namespace aetp.Common.Validator
{
    /// <summary>
    /// Валидатор для проверки ячеек таблиц с применением уловий равно, больше... и в качесвте операнда использующих константу либо формулу
    /// </summary>
    [DataContract]
    public class FormulaValidationItem : IValidationItem
    {
        GridCellItem cell;

        public bool Editable { get { return true; }  }

        public FormulaValidationItem(GridCellItem cell)
        {
            this.cell = cell;
        }
        
        public string Execute(object value)
        {
            if (cell == null)
                return null;
            double d = 0;
            double calcValue;
            if (value != null)
                double.TryParse(value.ToString(), out d);
            
            
            if (cell.CellValidating.ComparisonValueType == ComparisonValueType.Formula)
                calcValue = cell.CellValidating.ValidatingCalculationValue;
            else
                calcValue = cell.CellValidating.ValueForComparison ?? 0;

            bool comparisionResult = false;
            switch (cell.CellValidating.Comparison)
            {
                case ComparisonType.Eq:
                    if (d == calcValue)
                        comparisionResult = true;
                    break;
                case ComparisonType.NotEq:
                    if (d != calcValue)
                        comparisionResult = true;
                    break;
                case ComparisonType.Gt:
                    if (d > calcValue)
                        comparisionResult = true;
                    break;
                case ComparisonType.Ge:
                    if (d >= calcValue)
                        comparisionResult = true;
                    break;
                case ComparisonType.Ls:
                    if (d < calcValue)
                        comparisionResult = true;
                    break;
                case ComparisonType.Le:
                    if (d <= calcValue)
                        comparisionResult = true;
                    break;
            }
            if (!comparisionResult)
                return cell.CellValidating.ResultComparisonMessage;
            return null;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
