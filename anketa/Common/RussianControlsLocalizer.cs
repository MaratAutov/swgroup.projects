﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors.Controls;

namespace aetp.Common
{
    public class RussianControlsLocalizer : Localizer
    {
        public override string Language { get { return "Russian"; } }
        public override string GetLocalizedString(StringId id)
        {
            switch (id)
            {
                // ... 
                case StringId.DateEditClear: return "Очистить";
                case StringId.DateEditToday: return "Сегодня";
                case StringId.TextEditMenuPaste: return "Вставить";
                case StringId.TextEditMenuCopy: return "Копировать";
                case StringId.TextEditMenuCut: return "Вырезать";
                case StringId.TextEditMenuDelete: return "Удалить";
                case StringId.TextEditMenuSelectAll: return "Выделить всё";
                case StringId.TextEditMenuUndo: return "Отменить";
                // ... 
            }
            return "";
        }
    }
}
