﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting;
using aetp.Common.ControlModel;
using System.Data;
using System.Windows;

namespace aetp.Common.Control
{
    public class TableCollectionLinkTemplate : DevExpress.XtraPrinting.Link
    {
        DataTable dt;
        Font[,] font;
        float[] columnwidth;
        BrickStringFormat[,] brickSFormat;
        Color[,] backgroundColor;        
        PrintTableCollectionItem item;

        public TableCollectionLinkTemplate(PrintTableCollectionItem item,DataTable dt)
        {
            this.dt = dt;
            this.item = item;            
        }

        protected override void CreateReportHeader(BrickGraphics graph)
        {
            base.CreateReportHeader(graph);            
        }


        public static string GetFont(PrintingFont font)
        {
            string font_family = string.Empty;
            if (font == PrintingFont.Courier)
                font_family = "Courier New";
            else if (font == PrintingFont.Arial)
            {
                font_family = "Arial";
            }
            else font_family = "Times New Roman";
            return font_family;
        }

        private DataRow GetDataRowHeight(DataRow dr,int index)
        {
            DataRow drt = dt.NewRow();

            for(int i=0; i < dt.Columns.Count;i++)
            {
                if (item.Columns[dt.Columns.Count - i-1].IsMerge) 
                {
                    if (index == 0)
                    {
                        drt[i] = dr[i];
                    }
                    else 
                    {
                        drt[i] = "";
                    }
                }
                else drt[i] = dr[i];
            }
            return drt;
        }

        protected override void CreateDetail(BrickGraphics graph)
        {
            columnwidth = new float[item.Columns.Count];
            brickSFormat = new BrickStringFormat[item.Rows.Count,item.Columns.Count];
            backgroundColor = new Color[item.Rows.Count, item.Columns.Count];
            font = new Font[item.Rows.Count, item.Columns.Count];

            float sum_width = 0;

            for (int r = 0; r < item.Rows.Count; r++)
            {
                for (int j = 0, i = item.Columns.Count - 1; i > -1; i--, j++)
                {
                    //выравнивание
                    StringFormat sf = new StringFormat();
                    switch (item.Rows[r].Cells[i].VAlignment)
                    {
                        case aetp.Common.ControlModel.VerticalAlignment.Bottom: sf.LineAlignment = StringAlignment.Far; break;
                        case aetp.Common.ControlModel.VerticalAlignment.Center: sf.LineAlignment = StringAlignment.Center; break;
                        case aetp.Common.ControlModel.VerticalAlignment.Top: sf.LineAlignment = StringAlignment.Near; break;
                    }
                    sf.Alignment = item.Rows[r].Cells[i].Alignment;
                    brickSFormat[r,i] = new BrickStringFormat(sf);

                    //заливка
                    backgroundColor[r, i] = (item.Rows[r].Cells[i].IsBackGround) ? Color.LightGray : Color.Transparent;

                    //шрифт
                    FontStyle style = FontStyle.Regular;

                    if (item.Rows[r].Cells[i].IsItalic) style  = style ^ FontStyle.Italic;
                    if (item.Rows[r].Cells[i].IsBold) style = style ^ FontStyle.Bold;
                    if (item.Rows[r].Cells[i].IsUnderLine) style = style ^ FontStyle.Underline;
                   

                    //if (item.Rows[r].Cells[i].IsItalic && item.Rows[r].Cells[i].IsBold) style = FontStyle.Italic | FontStyle.Bold;
                    //else if (!item.Rows[r].Cells[i].IsItalic && item.Rows[r].Cells[i].IsBold) style = FontStyle.Bold;
                    //else if (item.Rows[r].Cells[i].IsItalic && !item.Rows[r].Cells[i].IsBold) style = FontStyle.Italic;
                    
                    font[r,i] = new Font(GetFont(item.Rows[r].Cells[i].Font), item.Rows[r].Cells[i].TextSize, style);
                    
                }
                
                
            }
            for (int j = 0, i = item.Columns.Count - 1; i > -1; i--, j++)
            {
                //ширина столбца
                columnwidth[j] = item.Columns[i].Width;
                sum_width += item.Columns[i].Width;
            }
            float k = (graph.ClientPageSize.Width -1 )/ sum_width;

            for (int i = 0; i < columnwidth.Length; i++)
            {
                columnwidth[i] = columnwidth[i] * k;                
            }        
            
            
            graph.BorderColor = Color.Black;
            graph.BorderWidth = 1;
            float y = 0;
            float max_height = 0;
            float width = (graph.ClientPageSize.Width - 1) / item.Columns.Count;

            BorderSide borderSide = new BorderSide();

            for (int r = 0; r < dt.Rows.Count; r++)
            {
                float x = 0;
                graph.Font = font[r,0];
                max_height = GridLink.getMaxRowHeight(graph, GetDataRowHeight(dt.Rows[r],r), columnwidth);

                string temp = string.Empty;

                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    if ((item.Rows[r].IsMerge) && (c + 1 < dt.Columns.Count))
                    {
                        temp = dt.Rows[r][c + 1].ToString();
                    }
                    graph.StringFormat = brickSFormat[r,c];
                    graph.BackColor = backgroundColor[r,c];
                    graph.Font = font[r,c];
                    if (!item.Columns[dt.Columns.Count - 1 - c].IsInVisible)
                    {
                        if (item.Columns[dt.Columns.Count - 1 - c].IsMerge)
                        {
                            List<BorderSide> borderSides = new List<BorderSide> { };
                            
                            borderSides.Add(BorderSide.Left);
                            if (item.IsHideLines)
                            {
                                borderSides[0] = BorderSide.None;
                            }
                            else
                            {
                                if (c == dt.Columns.Count - 1) borderSides[0] = BorderSide.Right;
                                else if ((c > 0) && (c < dt.Columns.Count - 1))
                                {
                                    borderSides.Add(BorderSide.Right);
                                }
                            }
                            if (r == 0)
                            {
                                if (item.IsHideLines) borderSide = BorderSide.None; else borderSide = BorderSide.Top;
                                                              

                                DrawString(graph, max_height, dt.Rows[r][c].ToString(), columnwidth[c], x, y, borderSide);
                                foreach (BorderSide side in borderSides)
                                {
                                    DrawString(graph, max_height, "", columnwidth[c], x, y, side);
                                }

                            }
                            else if (r == dt.Rows.Count - 1)
                            {
                                if (item.IsHideLines) borderSide = BorderSide.None; else borderSide = BorderSide.Bottom;
                                DrawString(graph, max_height, "", columnwidth[c], x, y, borderSide);
                                foreach (BorderSide side in borderSides)
                                {
                                    DrawString(graph, max_height, "", columnwidth[c], x, y, side);
                                }
                            }
                            else
                            {
                                foreach (BorderSide side in borderSides)
                                {
                                    DrawString(graph, max_height, "", columnwidth[c], x, y, side);
                                }
                            }
                            x += columnwidth[c];
                        }
                        else
                        {

                            if (item.IsHideLines) borderSide = BorderSide.None; else borderSide = BorderSide.All;
                            if ((item.Rows[r].IsMerge) && (temp == dt.Rows[r][c].ToString()))
                            {
                                DrawString(graph, max_height, dt.Rows[r][c].ToString(), columnwidth[c] + columnwidth[c + 1], x, y, borderSide);
                                x += columnwidth[c] + columnwidth[c + 1];
                                c += 1;
                            }
                            else
                            {
                                DrawString(graph, max_height, dt.Rows[r][c].ToString(), columnwidth[c], x, y, borderSide);
                                x += columnwidth[c];
                            }

                        }
                    }
                    else
                    {
                        DrawString(graph, max_height, "", columnwidth[c], x, y, BorderSide.None);
                        x += columnwidth[c];
                    }
                }
                y += max_height;
            }
        }

        public void DrawString(BrickGraphics graph, float max_height,string s,float columnwidth,float x,float y,BorderSide borderSide)
        {
            graph.DrawString(s, Color.Black,
                        new RectangleF(x, y, columnwidth,
                        max_height),
                        borderSide);
        }
        
    }
}
