﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;

namespace aetp.Common.Serializer
{
    public class FormSerializer
    {
        public string Serialize(Report report)
        {
            NetDataContractSerializer ds = new NetDataContractSerializer();
            using (MemoryStream stream = new MemoryStream())
            {
                ds.WriteObject(stream, report);
                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }
        
        public void Serialize(XmlTextWriter writer, Report report)
        {
            NetDataContractSerializer ds = new NetDataContractSerializer();
            using (writer)
            {
                ds.WriteObject(writer, report);
            }
        }

        public Report Deserialize(string source)
        {
            NetDataContractSerializer ds = new NetDataContractSerializer();
            return (Report)ds.ReadObject(XmlReader.Create(new StringReader(source)));
        }

        public Report Deserialize(TextReader source)
        {
            NetDataContractSerializer ds = new NetDataContractSerializer();
            return (Report)ds.ReadObject(XmlReader.Create(source));
        }

        public Report Deserialize(XmlTextReader source, bool NormalizeGrid)
        {
            NetDataContractSerializer ds = new NetDataContractSerializer();            
            Report report = (Report)ds.ReadObject(source);
            if (NormalizeGrid)
            {
                ReportUtils.NormalizeGrid(report.Children);
            }
            return report;
        }
    }
}
