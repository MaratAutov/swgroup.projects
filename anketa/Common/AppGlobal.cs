﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aetp.Common
{    
    public class AppGlobal
    {
        private AppGlobal() { }
        public ModelConfig Config { get; set; }
        public OrganizationDataConfig OrganizationConfig { get; set; }
        public MaskConfig MaskConfig { get; set; }
        public ProxyServerConfig ProxyServerConfig { get; set; }

        static AppGlobal()
        {
            instance = new AppGlobal();
            instance.Config = new ModelConfig();
            instance.Config.Load();
            instance.OrganizationConfig = new OrganizationDataConfig();
            instance.OrganizationConfig.Load();
            instance.MaskConfig = new MaskConfig();
            instance.MaskConfig.Load();
            instance.ProxyServerConfig = new ProxyServerConfig();
            instance.ProxyServerConfig.Load();
        }
        private static AppGlobal instance;
        public static AppGlobal Instance
        {
            get
            {
                return instance;
            }
        }
    }
    
}
