﻿namespace aetp.Common.Control
{
    partial class aetpTextEdit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teEdit = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.teEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCaption
            // 
            this.lblCaption.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblCaption.Location = new System.Drawing.Point(16, 11);
            // 
            // teEdit
            // 
            this.teEdit.Location = new System.Drawing.Point(255, 6);
            this.teEdit.Name = "teEdit";
            this.teEdit.Size = new System.Drawing.Size(541, 20);
            this.teEdit.TabIndex = 1;
            this.teEdit.EditValueChanged += new System.EventHandler(this.teEdit_EditValueChanged);
            // 
            // aetpTextEdit
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.teEdit);
            this.Name = "aetpTextEdit";
            this.Controls.SetChildIndex(this.lblCaption, 0);
            this.Controls.SetChildIndex(this.teEdit, 0);
            ((System.ComponentModel.ISupportInitialize)(this.teEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

		internal DevExpress.XtraEditors.TextEdit teEdit;
    }
}
