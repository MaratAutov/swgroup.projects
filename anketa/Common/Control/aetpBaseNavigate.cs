﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.ControlModel;

namespace aetp.Common.Control
{
    public partial class aetpBaseNavigate : DevExpress.XtraEditors.XtraUserControl
    {
        public new  aetp.Common.ControlModel.BaseControl Parent { get; set; }
        public Page CurrentPage { get; set; }
        public Page Link { get; set; }
        public event NavigateEventHandler Navigate;
        protected void RaiseNavigate()
        {
            if (Navigate != null)
                Navigate(this, new NavigateEventArgs(Link, CurrentPage));

        }

        public virtual string Caption { get; set; }

        public virtual object EditValue
        {
            get;
            set;
        }

        public aetpBaseNavigate()
        {
            InitializeComponent();
        }
    }

    

    public delegate void NavigateEventHandler(object sender, NavigateEventArgs args);
    public class NavigateEventArgs
    {
        public Page newPage { get; set; }
        public Page CurrentPage { get; set; }

        public NavigateEventArgs(Page page, Page CurrentPage)
        {
            this.newPage = page;
            this.CurrentPage = CurrentPage;
        }
    }
}
