﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.Validator;

namespace aetp.Common.Control
{
    public partial class aetpBaseEdit : DevExpress.XtraEditors.XtraUserControl
    {
        public virtual BaseEdit Editor { get; set; }
        
        public virtual event EventHandler EditValueChanged;
        protected void RaiseEditValueChanged()
        {
            if (EditValueChanged != null)
            {
                EditValueChanged(this, null);
            }
        }
        
        public string Caption
        {
            get
            {
                return lblCaption.Text;
            }
            set
            {
                lblCaption.Text = value;
            }
        }

        public Font LabelFont
        {
            get
            {
                return lblCaption.Font;
            }
            set
            {
                lblCaption.Font = value;
            }
        }

        public new virtual string Text
        {
            get
            {
                if (Editor != null)
                    return Editor.Text;
                else return "";
            }            
        }

        public int WidthCaption
        {
            get
            {
                return lblCaption.Width;
            }
            set
            {
                lblCaption.Width = value;
            }
        }

        public int HeightCaption
        {
            get
            {
                return lblCaption.Height;
            }
            set
            {
                lblCaption.Height= value;
            }
        }

        public int Interval
        {
            get
            {
                return Editor.Left - (lblCaption.Left + lblCaption.Width);
            }
            set
            {
                Editor.Left = lblCaption.Left + lblCaption.Width + value;
            }
        }

        public virtual object EditValue
        {
            get;
            set;
        }

        public aetpBaseEdit()
        {
            InitializeComponent();
        }
    }

    
}
