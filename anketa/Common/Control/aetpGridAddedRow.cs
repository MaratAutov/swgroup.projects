﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.ControlModel;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using aetp.Common.Validator;
using DevExpress.XtraEditors.Repository;
using System.Linq;
using aetp.Common.View;

namespace aetp.Common.Control
{
    
	public partial class aetpGridAddedRow : DevExpress.XtraEditors.XtraUserControl, IGrid
	{
       
        public delegate void AddedGridChangedHandler(object sender, DataAddedRowChangeEventArgs e);
        public event AddedGridChangedHandler GridChanged;
        void RaiseGridChanged(DataRowChangeEventArgs e)
        {
            if (GridChanged != null)
                GridChanged(this, new DataAddedRowChangeEventArgs(e, e.Row.Table.Rows.IndexOf(e.Row), LastChangedColumnIndex));
        }

		public GridAddedRow model { get; set; }
		public DataTable dt = new DataTable();
		public CalculationHelper CalculationHelper { get; set; }

        public event EventHandler ChangeCell;
        protected void RaiseChangeCell()
        {
            if (ChangeCell != null)
                ChangeCell(this, null);
        }

		public int Interval
		{
			get
			{
				return gc.Left - (lblCaption.Left + lblCaption.Width);
			}
			set
			{
				gc.Left = lblCaption.Left + lblCaption.Width + value;
                panel.Left = lblCaption.Left + lblCaption.Width + value - 1;
			}
		}

		public int WidthCaption
		{
			get
			{
				return lblCaption.Width;
			}
			set
			{
				lblCaption.Width = value;
			}
		}

        private bool _isMandatoryToFill = false;
        public bool IsMandatoryToFill
        {
            get
            {
                return _isMandatoryToFill;
            }
            set
            {
                _isMandatoryToFill = value;
            }
        }


		public int WidthGrid
		{
			get
			{
				return gc.Width;
			}
			set
			{
				if ((Convert.ToInt32(value) == 0) || (Convert.ToInt32(value) < 0)) gc.Width = 100;
				else
					gc.Width = value;
			}
		}        

		public string Caption
		{
			get
			{
				return lblCaption.Text;
			}
			set
			{
				lblCaption.Text = value;
			}
		}

        public bool HideCaption
        {
            get
            {
                return model.HideCaption;
            }
            set
            {
                model.HideCaption = value;
            }
        }

		public int HeightGrid
		 {
			get
			{
				return gc.Height;
			}
			set
			{
				if ((value == 0) || (value < 0)) gc.Height = 200;
				else
					gc.Height = value;

				if (model.AutoHeight)
				{
                    if (gv.RowCount * 21 + 30 + (model.HideHeader ? 0 : HeightHeaderGrid) > model.HeightGrid)
                        {
                            gc.Height = model.HeightGrid;
                        }
                        else
                        {
                            gc.Height = gv.RowCount * 21 + 30 + (model.HideHeader ? 0 : HeightHeaderGrid);
                        }                    
				}                
			}
		}

        public int HeightHeaderGrid
        {
            get
            {
                return gv.ColumnPanelRowHeight;
            }
            set
            {
                if ((value == 0) || (value < 0)) gv.ColumnPanelRowHeight = 10;
                else
                    gv.ColumnPanelRowHeight = value;
            }
        }

		private string LastChangedColumn = "";
        int _lastChangedColumnIndex;
        public int LastChangedColumnIndex
        {
            get
            {
                return _lastChangedColumnIndex;
            }
        }

        #region paging

        int CurrentPage = 0;
        int RowPerPage = 20;

        public DataTable DataSource
        {
            get { return (DataTable)gc.DataSource; }
            set {
                paging(value);
            }
        }

        int TotalPages
        {
            get
            {
                return Convert.ToInt32(Math.Ceiling((double)model.Data.Rows.Count / (double)RowPerPage));
            }
        }

        void paging(DataTable data)
        {
            int curPage = 0;
            if (data.Rows.Count != 0)
            {
                curPage = CurrentPage + 1;
            }
            
            string text = String.Format("Страница {0} из {1} (Всего строк: {2})", curPage, TotalPages, data.Rows.Count);
            lblStatus.Text = text;
            if (data.Rows.Count > 0)
            {
                var query = data.AsEnumerable().Skip(CurrentPage * RowPerPage).Take(RowPerPage).CopyToDataTable();
                query.RowChanged += new DataRowChangeEventHandler(query_RowChanged);
                gc.DataSource = query;
            }
            else
            {
                gc.DataSource = data;
            }
        }

        void CopyInCurrentDt(DataTable temp, DataRow row)
        {
            int startRow = CurrentPage * RowPerPage;
            int r = temp.Rows.IndexOf(row);
            
            for (int c = 0; c < temp.Columns.Count; c++)
            {
                model.Data.Rows[startRow + r][c] = temp.Rows[r][c];
            }
        }

        void query_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Change)
            {
                CopyInCurrentDt(sender as DataTable, e.Row);
            }
        }

        #endregion

        public void Configure(GridAddedRow model)
		{
			this.model = model;
            model.gc = gc;
            gv.KeyDown -= new KeyEventHandler(gv_KeyDown);
            gv.KeyDown += new KeyEventHandler(gv_KeyDown);
            DataTable dt1 = model.Data ?? dt;
            if (model.metagrid == null)
            {
                CalculationHelper = new CalculationHelper(new Hashtable() { { model, dt1 } });
            }
            else
            {
                CalculationHelper.AddToCollection(model, dt1);
            }
            
			if (CalculationHelper != null)
			{
				dt1.ColumnChanging += delegate(object sender, DataColumnChangeEventArgs args)
				{
                    _lastChangedColumnIndex = dt1.Columns.IndexOf(args.Column);
					if (!CalculationHelper.Columns.ContainsKey(GetColumnName(args.Column.ColumnName)))
					{
						return;
					}

					if (args.Column.ColumnName == string.Empty)
					{
						return;
					}

					LastChangedColumn = args.Column.ColumnName;
					CalculationHelper.UpdateTotal(GetColumnName(args.Column.ColumnName));
				};

                dt1.RowChanged += RowChangedHandler;
			}            

            dt1.TableNewRow += new DataTableNewRowEventHandler(dt_TableNewRow);
			ToolTipController toolTip = new ToolTipController();
			toolTip.GetActiveObjectInfo += new ToolTipControllerGetActiveObjectInfoEventHandler(toolTip_GetActiveObjectInfo);
			gc.ToolTipController = toolTip;
			AddValidatingItems();
            
			gc.BeginUpdate();
			gv.BeginUpdate();
			int i = 0;
            
			foreach (GridAddedRowColumn col in model.Columns)
			{
                DevExpress.XtraGrid.Columns.GridColumn gcol = new DevExpress.XtraGrid.Columns.GridColumn()
				{
					Name = col.Name,
					Caption = col.Caption,
					Visible = true,
					VisibleIndex = i++,
                    FieldName = col.Name,
                    Width = col.WidthColumn,
                    
                   
				};
                
                if (model.CanResizeColumn == false)
                {
                    gcol.OptionsColumn.AllowSize = false;                   
                }                

                gcol.OptionsColumn.AllowSort = DefaultBoolean.False;
                gcol.OptionsFilter.AllowFilter = false;
				gv.Columns.Add(gcol);

                dt.Columns.Add(col.Name, col.GetType());

				if (col.AggregateFunction != AggregateFunctions.None)
				{
					gv.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[]
						{
							new DevExpress.XtraGrid.GridGroupSummaryItem(
						                              (DevExpress.Data.SummaryItemType)
						                              Enum.Parse(typeof (DevExpress.Data.SummaryItemType),
						                                         col.AggregateFunction.ToString()), col.Name, null, "")
						});

					gcol.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] 
						{
							new DevExpress.XtraGrid.GridColumnSummaryItem
								(
									(DevExpress.Data.SummaryItemType)Enum.Parse(typeof (DevExpress.Data.SummaryItemType), col.AggregateFunction.ToString())
								)
						});
					this.gv.OptionsView.ShowFooter = true;
				}
			}
            if (model.Data == null)
            {
                model.Data = dt;
            }
            paging(dt);
            gc.EndUpdate();
			gv.EndUpdate();
			Name = model.Name;
			Caption = model.Caption;
			WidthCaption = model.WidthCaption;
			Interval = model.Interval;
			WidthGrid = model.WidthGrid;
			
            HeightHeaderGrid = model.HeightHeaderGrid;
            HeightGrid = model.HeightGrid;
            gv.OptionsView.AllowHtmlDrawHeaders = true;
            gv.Appearance.HeaderPanel.TextOptions.WordWrap = WordWrap.Wrap;
            gv.OptionsMenu.EnableColumnMenu = false;  
			gv.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(gv_ValidatingEditor);
			gv.InvalidValueException += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(gv_InvalidValueException);
			gv.RowCellStyle += new RowCellStyleEventHandler(gv_RowCellStyle);

            gv.CustomDrawColumnHeader += new ColumnHeaderCustomDrawEventHandler(gv_CustomDrawColumnHeader);            
            gv.MouseDown += new MouseEventHandler(gv_MouseDown);
            gv.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(gv_FocusedColumnChanged);
            gv.OptionsPrint.UsePrintStyles = true;
            gv.OptionsView.ShowColumnHeaders = !model.HideHeader;
            gv.OptionsPrint.PrintHeader = !model.HideHeader;
            IsMandatoryToFill = model.IsMandatoryToFill;
            if (model.HideCaption)
            {
                Caption = " ";
            }
            RefreshAllFormulas();
        }
                
        public void RefreshAllFormulas()
        {
            foreach (var col in model.Columns)
            {
                CalculationHelper.UpdateTotal(col.Name);
            }
        }

        void gv_FocusedColumnChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs e)
        {
            gv.UpdateCurrentRow();
        }

        string GetColumnName(string column)
        {
            return string.Format("{0}{2}{1}", this.Name, column, CalculationHelper.TableNameSplitter);
        }

        void RowChangedHandler(object sender, DataRowChangeEventArgs args)
        {
            if (LastChangedColumn == string.Empty || !CalculationHelper.Columns.ContainsKey(GetColumnName(LastChangedColumn)))
            {
                return;
            }
            CalculationHelper.UpdateTotal(GetColumnName(LastChangedColumn));
            LastChangedColumn = "";
            //RaiseGridChanged(args);
        }

        void gv_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == 0x16);
        }

        void gv_KeyDown(object sender, KeyEventArgs e)
        {
            // для того что бы по enter и стрелочкам вызвать событие изменения значения для пересчета формул
            if (e.KeyValue == 13 || e.KeyCode == Keys.Right || e.KeyCode == Keys.Left)
            {
                if (gv.ActiveEditor != null)
                    gv.GetFocusedDataRow()[gv.FocusedColumn.AbsoluteIndex] = gv.ActiveEditor.EditValue ?? DBNull.Value;
            }
            else if (e.Control && e.KeyCode == Keys.V)
            {
                try
                {
                    PasteData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        void PasteData()
        {
            IDataObject dataInClipboard = Clipboard.GetDataObject();
            char[] rowSplitter = { '\r', '\n' };
            string stringInClipboard =
                (string)dataInClipboard.GetData(DataFormats.Text);

            string[] rowsInClipboard =
                stringInClipboard.Split(rowSplitter,
                StringSplitOptions.RemoveEmptyEntries);
            if (rowsInClipboard.Length < 1)
            {
                throw new InvalidOperationException("Невозможно вставить данные в таблицу. В буфере обмена нет данных для вставки.");
            }
            List<string[]> rows = new List<string[]>();
            int columnCount = -1;
            foreach (string strRow in rowsInClipboard)
            {
                string[] cells = strRow.Split('\t');
                if (columnCount == -1)
                {
                    columnCount = cells.Length;
                }
                if (cells.Length < 1 || cells.Length != columnCount)
                {
                    throw new InvalidOperationException("Невозможно вставить данные в таблицу. В буфере обмена нет данных для вставки.");
                }
                rows.Add(cells);
            }
            //int endRow = gv.FocusedRowHandle + rows.Count;
            
            int endColumn = gv.FocusedColumn.VisibleIndex + columnCount;
            if (endColumn > model.Columns.Count)
            {
                endColumn = model.Columns.Count;
            }

            rows = CorrectDataForImport(rows, gv.FocusedColumn.VisibleIndex, endColumn);
            int r = gv.FocusedRowHandle >= 0 ? gv.FocusedRowHandle : 0;
            int r1 = 0;
            foreach (var row in rows)
            {
                if (r >= model.Data.Rows.Count)
                {
                    model.Data.Rows.Add(dt.NewRow());
                }
                int c1 = 0;
                for (int c = gv.FocusedColumn.VisibleIndex; c < endColumn; c++)
                {
                    if (rows[r1][c1] == null)
                    {
                        model.Data.Rows[r][c] = DBNull.Value;
                    }
                    else
                    {
                        model.Data.Rows[r][c] = rows[r1][c1];
                    }
                    c1++;
                }
                r++;
                r1++;
            }
            model.Data.AcceptChanges();
        }

        List<string[]> CorrectDataForImport(List<string[]> rows, int startColumn, int endColumn)
        {
            foreach (var row in rows)
            {
                int c1 = 0;
                for (int c = startColumn; c < endColumn; c++)
                {
                    switch (model.Columns[c].Type)
                    {
                        case GridCellType.Date:
                            DateTime dt;
                            if (!DateTime.TryParse(row[c1], out dt))
                            {
                                row[c1] = null;
                            }
                            break;
                        case GridCellType.Integer:
                            long val;
                            if (!long.TryParse(row[c1], out val))
                            {
                                row[c1] = null;
                            }
                            break;
                        case GridCellType.Double:
                            Double dval;
                            if (!Double.TryParse(row[c1], out dval))
                            {
                                row[c1] = null;
                            }
                            break;
                        case GridCellType.Label:
                            row[c1] = model.Columns[c1].TextLabel ?? "";
                            break;
                        case GridCellType.List:
                            var item = ReportUtils.GetDictionaryItem(model.Columns[c].Source, row[c1]);
                            if (item == null) row[c1] = null;
                            else row[c1] = item.Code;
                            break;
                        case GridCellType.Check:
                            row[c1] = null;
                            break;
                    }
                    c1++;
                }
            }
            return rows;
        }

        

        void dt_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            // заполнение лайбела данными
            for (int i = 0; i < model.Columns.Count; i++)
            {
                if (model.Columns[i].Type == GridCellType.Label)
                {                    
                    if (model.Columns[i].TextLabel != null)
                    {
                        e.Row[i] = model.Columns[i].TextLabel;
                    }
                    else
                    {
                        e.Row[i] = "";
                    }
                }
                if (model.Columns[i].DefaultValue != null)
                {
                    CellDefaultValue(model.Columns[i].DefaultValue, e.Row, i);                   
                }
            }
        }

        public void CellDefaultValue(object DefaultValue, DataRow dr, int index)
        {
            if (DefaultValue is aetp.Common.ControlModel.UITypeEditors.OrganizationDataItem)
            {
                dr[index] = DefaultValueControl.GetDefaultValue(DefaultValue.ToString());
            }
            else
                dr[index] = DefaultValue;
        }

		#region Validating

        public DevExpress.XtraEditors.DXErrorProvider.ErrorInfo GetErrorInfo()
        {
            for (int i = 0; i < model.Columns.Count; i++)
                for (int j = 0; j < model.Data.Rows.Count; j++)
                {
                    DevExpress.XtraEditors.DXErrorProvider.ErrorInfo errinfo = new DevExpress.XtraEditors.DXErrorProvider.ErrorInfo();
                    if (model.Columns[i].Type != GridCellType.Label)
                    model.Columns[i].GetPropertyError("EditValue", errinfo,
                        model.Data.Rows[j][i]);
                    if (!String.IsNullOrEmpty(errinfo.ErrorText))
                        return errinfo;
                }   
            return null;
        }

		//Применяет правила валидации к ячейкам
		void AddValidatingItems()
		{
            ValidationCollection.AddGrid(this, model);
			for (int i = 0; i < model.Columns.Count; i++)
			{
                if (model.Columns[i].IsMandatoryToFill)
                {
                    /* mahalin 19-09-2013
                    * Защита от возможного забития списка валидаторов дублями
                    */
                    List<IValidationItem> list = model.Columns[i].EditorValidators.Where(x => { return x is EmptyValidationItem; }).ToList();
                    foreach (IValidationItem v in list)
                    {
                        model.Columns[i].EditorValidators.Remove(v);
                    }

                    if (model.Columns[i].Type == GridCellType.Double || model.Columns[i].Type == GridCellType.Integer)
                        model.Columns[i].EditorValidators.Add(new EmptyValidationItem(ValidatorItemType.num));
                    else
                        model.Columns[i].EditorValidators.Add(new EmptyValidationItem());
                }
                if (model.Columns[i].Type == GridCellType.String && !String.IsNullOrEmpty(model.Columns[i].Mask.Mask))
                {
                    /* mahalin 19-09-2013
                    * Защита от возможного забития списка валидаторов дублями
                    */
                    List<IValidationItem> list = model.Columns[i].EditorValidators.Where(x => { return x is RegexValidationItem; }).ToList();
                    foreach (IValidationItem v in list)
                    {
                        model.Columns[i].EditorValidators.Remove(v);
                    }

                    model.Columns[i].EditorValidators.Add(new RegexValidationItem(model.Columns[i].Mask));
                }
			}
		}

		void toolTip_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
		{
			if (e.SelectedControl != gc) return;

			ToolTipControlInfo info = null;
			GridView view = gc.GetViewAt(e.ControlMousePosition) as GridView;
			if (view == null) return;
			GridHitInfo hi = view.CalcHitInfo(e.ControlMousePosition);			
			if (hi.HitTest == GridHitTest.RowCell)
			{				
				object o = hi.HitTest.ToString() + hi.RowHandle.ToString();
				string text = "Row " + hi.RowHandle.ToString();
				DevExpress.XtraEditors.DXErrorProvider.ErrorInfo errinfo = new DevExpress.XtraEditors.DXErrorProvider.ErrorInfo();
				model.Columns[hi.Column.VisibleIndex].GetPropertyError("EditValue", errinfo,
					model.Data.Rows[hi.RowHandle][hi.Column.VisibleIndex]);
				if (!String.IsNullOrEmpty(errinfo.ErrorText))
				{
					info = new ToolTipControlInfo(o, errinfo.ErrorText);
				}
			}			
			if (info != null)
				e.Info = info;
		}

		void gv_InvalidValueException(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
		{
			e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.DisplayError;
		}

		void gv_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
		{
            GridView view = sender as GridView;
			DevExpress.XtraEditors.DXErrorProvider.ErrorInfo info = new DevExpress.XtraEditors.DXErrorProvider.ErrorInfo();
			model.Columns[view.FocusedColumn.VisibleIndex].GetPropertyError("EditValue", info, e.Value);

			if (!String.IsNullOrEmpty(info.ErrorText))
			{
				e.ErrorText = info.ErrorText;
				e.Valid = false;
			}

            RaiseChangeCell();
		}

		void gv_RowCellStyle(object sender, RowCellStyleEventArgs e)
		{
            // если это метка то не проверяем а закрашиваем серым
            if (model.Columns[e.Column.VisibleIndex].Type == GridCellType.Label)
            {
               
                AppearanceHelper.Apply(e.Appearance, aetpGrid.AppDisabled);
                return;
            }
            if (model.Data.Rows[e.RowHandle].RowState == DataRowState.Deleted)
                return;
            // проверка на ошибки
            object val = model.Data.Rows[RowIndex(e.RowHandle)][e.Column.VisibleIndex];
			DevExpress.XtraEditors.DXErrorProvider.ErrorInfo info = new DevExpress.XtraEditors.DXErrorProvider.ErrorInfo();
			model.Columns[e.Column.VisibleIndex].GetPropertyError("EditValue", info, val);
			if (!String.IsNullOrEmpty(info.ErrorText))
			{
				AppearanceHelper.Apply(e.Appearance, aetpGrid.AppError);
			}
		}

        int RowIndex(int visibleIndex)
        {
            return CurrentPage * RowPerPage + visibleIndex;
        }

		#endregion

		public aetpGridAddedRow()
		{
			InitializeComponent();

            this.repositoryItemSpinEdit1.Mask.Culture = new System.Globalization.CultureInfo("ru-RU")
            {
                NumberFormat = new System.Globalization.NumberFormatInfo() { CurrencyDecimalSeparator = ".", NumberDecimalSeparator = ".", NumberGroupSeparator = "" }
            };
            
            this.repositoryItemSpinEdit2.Mask.Culture = new System.Globalization.CultureInfo("ru-RU")
            {
                NumberFormat = new System.Globalization.NumberFormatInfo() { CurrencyDecimalSeparator = ".", NumberDecimalSeparator = ".", NumberGroupSeparator = "" }
            };

            this.repositoryItemSpinEdit1.Buttons.Clear();
            this.repositoryItemSpinEdit2.Buttons.Clear();
		}
        
        #region Check All Cell

        private void gv_CustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == null) return;
            foreach (GridAddedRowColumn col in model.Columns)
            {
                if (col.Name == e.Column.Name)
                {
                    if (col.CheckColumn)
                    {
                        e.Info.InnerElements.Clear();
                        e.Info.Appearance.ForeColor = Color.Blue;
                        e.Painter.DrawObject(e.Info);
                        
                        DrawCheckBox(e.Graphics, e.Bounds, (getCheckedCount(e.Column) == gv.DataRowCount) && (gv.DataRowCount != 0));
                        e.Handled = true;
                    }
                }
            }
        }

        RepositoryItemCheckEdit chkedit = new RepositoryItemCheckEdit();

        protected void DrawCheckBox(Graphics g, Rectangle r, bool Checked)
        {
            DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo info;
            DevExpress.XtraEditors.Drawing.CheckEditPainter painter;
            DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs args;
            info = chkedit.CreateViewInfo() as DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo;
            painter = chkedit.CreatePainter() as DevExpress.XtraEditors.Drawing.CheckEditPainter;
            info.EditValue = Checked;

            info.Bounds = r;
            info.PaintAppearance.ForeColor = Color.Black;
            info.CalcViewInfo(g);
            args = new DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs(info, new DevExpress.Utils.Drawing.GraphicsCache(g), r);
            painter.Draw(args);
            args.Cache.Dispose();
        }

        int getCheckedCount(DevExpress.XtraGrid.Columns.GridColumn col)
        {
            int count = 0;
            for (int i = 0; i < gv.DataRowCount; i++)
            {
                GridViewInfo vi = gv.GetViewInfo() as GridViewInfo;
                GridDataRowInfo ri = vi.RowsInfo.FindRow(i) as GridDataRowInfo;
                if (ri != null)
                {
                    RepositoryItem item = ri.Cells[col].Editor;
                    if (item is RepositoryItemCheckEdit)
                    {
                        if (gv.GetRowCellValue(i, col).ToString() == "True")
                            count++;
                    }
                    else count++;
                }
                else count++;
            }
            return count;
        }

        void SetCheckAll(DevExpress.XtraGrid.Columns.GridColumn col, bool check, int width)
        {
            for (int i = 0; i < gv.DataRowCount; i++)
            {
                GridViewInfo vi = gv.GetViewInfo() as GridViewInfo;
                GridDataRowInfo ri = vi.RowsInfo.FindRow(i) as GridDataRowInfo;
                if (ri != null)
                {
                    RepositoryItem item = ri.Cells[col].Editor;
                    if (item is RepositoryItemCheckEdit)
                        gv.SetRowCellValue(i, col, check);
                }
            }


            if (model.metagrid != null)
            {
                foreach (BaseGrid grid in model.metagrid.Tables)
                {
                    if (grid.Name != model.Name)
                    {
                        for (int i = 0; i < grid.gc.Views[0].DataRowCount; i++)
                        {
                            GridViewInfo vi = grid.gc.Views[0].GetViewInfo() as GridViewInfo;
                            GridDataRowInfo ri = vi.RowsInfo.FindRow(i) as GridDataRowInfo;

                            if (ri != null)
                            {
                                int width_column = 0;

                                foreach (DevExpress.XtraGrid.Columns.GridColumn column in (grid.gc.Views[0] as GridView).Columns)
                                {
                                    width_column += column.Width;
                                    if (width == width_column)
                                    {
                                        if (grid is Grid)
                                        {
                                            GridCellCollection cells = (grid as Grid).Rows[i].Cells;
                                            GridCellItem se = cells[column.VisibleIndex];
                                            if (se.Type == GridCellType.Check)
                                                (grid.gc.Views[0] as GridView).SetRowCellValue(i, column, check);
                                        }
                                        else if (grid is GridAddedRow)
                                        {
                                            GridAddedRowColumn grid_column = (grid as GridAddedRow).Columns[column.VisibleIndex];
                                            if (grid_column.Type == GridCellType.Check)
                                                (grid.gc.Views[0] as GridView).SetRowCellValue(i, column, check);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void gv_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks == 1 && e.Button == MouseButtons.Left)
            {
                GridHitInfo info;
                Point pt = gv.GridControl.PointToClient(System.Windows.Forms.Control.MousePosition);
                info = gv.CalcHitInfo(pt);

                int width = 0;

                if (info.Column != null)
                {
                    foreach (GridAddedRowColumn col in model.Columns)
                    {
                        width += col.WidthColumn;

                        if (col.Name == info.Column.Name)
                        {
                            if ((col.CheckColumn) && info.InColumn)
                            {
                                if (getCheckedCount(info.Column) == gv.DataRowCount)
                                    SetCheckAll(info.Column, false, width);
                                else
                                    SetCheckAll(info.Column, true, width);

                            }
                        }
                    }
                }
            }
        }

        #endregion


        private void gv_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
		{
			GridAddedRowColumn col = model.Columns[e.Column.VisibleIndex];
            
			switch (col.Type)
			{
				case GridCellType.Date:
					e.RepositoryItem = repositoryItemDateEdit1;
                    e.RepositoryItem.Appearance.TextOptions.HAlignment = HorzAlignment.Far;
                     (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemDateEdit).EditMask = "dd.MM.yyyy";
                            (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemDateEdit).Mask.UseMaskAsDisplayFormat = true;
					break;
				case GridCellType.String:
					e.RepositoryItem = repositoryItemTextEdit1;
                    if (col.Mask != null)
                        e.RepositoryItem.NullText = col.Mask.Description;  
					break;
				case GridCellType.Check:
					e.RepositoryItem = repositoryItemCheckEdit1;
					break;
				case GridCellType.Double:
					e.RepositoryItem = repositoryItemSpinEdit1.Clone() as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit;
					if (CalculationHelper != null)
					{
						//this.CalculationHelper.SetEvent(e.RepositoryItem, col);
					}
                    
                        string mask = string.Empty;
                        mask = "# ### ### ### ### ### ### ### ##0.";
                        if (col.MaxFloatLength > 0)
                        {                            
                            for (int i = 0; i < col.MaxFloatLength; i++)
                            {
                                mask += "#";
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).EditMask = mask;
                            }
                            (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).EditMask = mask;
                        }
                        else mask += "###########";
                        (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).EditMask = mask;
                        (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Mask.EditMask = mask;
                        (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).IsFloatValue = true;
                        (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).DisplayFormat.FormatType = FormatType.Numeric;
                        (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).DisplayFormat.FormatString = mask;
                        (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Spin -= new DevExpress.XtraEditors.Controls.SpinEventHandler(edit_Spin);
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Spin += new DevExpress.XtraEditors.Controls.SpinEventHandler(edit_Spin);
                    
					break;
				case GridCellType.Integer:
					e.RepositoryItem = repositoryItemSpinEdit2.Clone() as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit;
                    string mask_int = string.Empty;
                    mask_int = "# ### ### ### ### ### ### ### ##0";
                    
                    (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).MaxValue = Int64.MaxValue;
                    (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).MinValue = Int64.MinValue;
                    (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).EditMask = mask_int;
                    (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Mask.EditMask = mask_int;                        
                    (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).DisplayFormat.FormatType = FormatType.Numeric;
                    (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).DisplayFormat.FormatString = mask_int;
                    (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Spin -= new DevExpress.XtraEditors.Controls.SpinEventHandler(edit_Spin);
                    (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Spin += new DevExpress.XtraEditors.Controls.SpinEventHandler(edit_Spin);
					break;
				case GridCellType.List:
					RepositoryItemLookUpEdit lu = new RepositoryItemLookUpEdit();
                    lu.KeyDown += new KeyEventHandler(lu_KeyDown);
                    var buttonDelete = new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete);
                    lu.Buttons.Add(buttonDelete);
                    lu.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(lu_ButtonClick);
                    lu.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                    lu.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoFilter;
                    lu.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {                                             
                                            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description","Описание",-1,DevExpress.Utils.FormatType.None,"",true,DevExpress.Utils.HorzAlignment.Default)  
                                            });

					lu.NullText = "[Значение не задано]";
                    if (col.Source != null)
					{
						lu.DataSource = col.Source.Items;
						lu.ValueMember = "Code";
					}
					else
						lu.DataSource = null;
					e.RepositoryItem = lu;
                    break;                
			}
            e.RepositoryItem.Appearance.Options.UseTextOptions = true;
            e.RepositoryItem.Appearance.TextOptions.HAlignment = HorzAlignment.Far;
            e.Column.AppearanceCell.Options.UseTextOptions = true;
            e.Column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
		}

        void lu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                gv.LayoutChanged();
            }
        }
                
        void lu_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                (sender as DevExpress.XtraEditors.LookUpEdit).EditValue = null;
            }
        }

        void edit_Spin(object sender, DevExpress.XtraEditors.Controls.SpinEventArgs e)
        {
            e.Handled = true;
        }

        private void gv_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = sender as GridView;
            if (model.Columns[view.FocusedColumn.VisibleIndex].Type == GridCellType.Label)
                e.Cancel = true;
        }

        private void gc_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.Tag.ToString() == "MoveForward")
            {
                gv.CloseEditor();
                (gc.DataSource as DataTable).AcceptChanges();
                if (CurrentPage < TotalPages - 1)
                {
                    CurrentPage++;
                    paging(model.Data);
                }
            }
            else if (e.Button.Tag.ToString() == "MoveBack")
            {
                gv.CloseEditor();
                (gc.DataSource as DataTable).AcceptChanges();
                if (CurrentPage > 0)
                {
                    CurrentPage--;
                    paging(model.Data);
                }
            }
            else if (e.Button.Tag.ToString() == "Add")
            {
                
                int newFocusRow = 0;
                int[] selected = gv.GetSelectedRows();
                if (selected.Length > 0)
                {
                    model.NewRow(RowIndex(selected[0] + 1));

                    model.Data.AcceptChanges();
                    gv.CloseEditor();

                    if (gv.FocusedRowHandle == RowPerPage - 1)
                    {
                        CurrentPage++;
                    }
                    else
                    {
                        newFocusRow = selected[0] + 1;
                    }
                    paging(model.Data);
                    gv.FocusedRowHandle = newFocusRow;
                }
                else
                {
                    model.NewRow();
                    model.Data.AcceptChanges();
                    gv.CloseEditor();
                    paging(model.Data);
                }
                
            }
            else if (e.Button.Tag.ToString() == "Remove")
            {
                int[] selected = gv.GetSelectedRows();
                if (selected.Length > 0)
                {
                    gv.CloseEditor();
                    bool move2up = false;
                    if ((gc.DataSource as DataTable).Rows.Count == 1 && CurrentPage > 0)
                    {
                        move2up = true;
                        CurrentPage--;
                    }
                    model.Data.AcceptChanges();                    
                    model.Data.Rows.RemoveAt(RowIndex(selected[0]));
                    model.Data.AcceptChanges();
                    paging(model.Data);
                    if (move2up || selected[0] >= gv.RowCount)
                    {
                        gv.FocusedRowHandle = gv.RowCount - 1;
                    }
                    else
                    {
                        gv.FocusedRowHandle = selected[0];
                    }
                    foreach (DataColumn col in dt.Columns)
                    {
                        CalculationHelper.UpdateTotal(GetColumnName(col.ColumnName));
                    }
                }
            }           
        }

		private void GvOnRowCountChanged(object sender, EventArgs eventArgs)
		{
            if (model.AutoHeight)
			{
                gc.Height = gv.RowCount * 21 + (model.HideHeader ? 0 : HeightHeaderGrid) + 30;
                panel.Height = gv.RowCount * 21 + (model.HideHeader ? 0 : HeightHeaderGrid) + 32;
                this.Height = gv.RowCount * 21 + (model.HideHeader ? 0 : HeightHeaderGrid) + 52;
			}
		}
                
	}

    public class DataAddedRowChangeEventArgs
    {
        public DataRowChangeEventArgs ChangeArgs;
        public int RowIndex;
        public int ColumnIndex;

        public DataAddedRowChangeEventArgs(DataRowChangeEventArgs args, int rowIndex, int columnIndex)
        {
            ChangeArgs = args;
            RowIndex = rowIndex;
            ColumnIndex = columnIndex;
        }
    }
}
