﻿namespace aetp.Common.Control
{
    partial class aetpCheckEdit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ceEdit = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ceEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCaption
            // 
            this.lblCaption.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblCaption.Location = new System.Drawing.Point(16, 11);
            // 
            // ceEdit
            // 
            this.ceEdit.Location = new System.Drawing.Point(258, 8);
            this.ceEdit.Name = "ceEdit";
            this.ceEdit.Properties.AllowGrayed = true;
            this.ceEdit.Properties.Caption = "";
            this.ceEdit.Size = new System.Drawing.Size(51, 19);
            this.ceEdit.TabIndex = 2;
            this.ceEdit.CheckedChanged += new System.EventHandler(this.ceEdit_CheckedChanged);
            this.ceEdit.CheckStateChanged += new System.EventHandler(this.ceEdit_CheckStateChanged);
            this.ceEdit.EditValueChanged += new System.EventHandler(this.ceEdit_EditValueChanged);
            // 
            // aetpCheckEdit
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.ceEdit);
            this.Name = "aetpCheckEdit";
            this.Size = new System.Drawing.Size(799, 44);
            this.Controls.SetChildIndex(this.lblCaption, 0);
            this.Controls.SetChildIndex(this.ceEdit, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ceEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.CheckEdit ceEdit;
    }
}
