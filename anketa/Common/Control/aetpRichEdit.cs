﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.Validator;
using DevExpress.XtraRichEdit;

namespace aetp.Common.Control
{
    public partial class aetpRichEdit : DevExpress.XtraEditors.XtraUserControl
    {
        public virtual event EventHandler EditValueChanged;
        protected void RaiseEditValueChanged()
        {
            if (EditValueChanged != null)
            {
                EditValueChanged(this, null);
            }
        }

        public RichEditControl Editor
        {
            get
            {
                return rich;
            }
            set
            {

            }
        }

        public object _editValue;
        public object EditValue
        {
            get
            {
                return rich.Text;
            }
            set
            {
                rich.Text = value.ToString();
            }
        }

        public string Text
        {
            get
            {
                return rich.Text != null ? rich.Text : "";
            }
        }

        public string Caption
        {
            get
            {
                return lblCaption.Text;
            }
            set
            {
                lblCaption.Text = value;
            }
        }

        public int WidthCaption
        {
            get
            {
                return lblCaption.Width;
            }
            set
            {
                lblCaption.Width = value;
            }
        }

        public int Interval
        {
            get
            {
                return rich.Left - (lblCaption.Left + lblCaption.Width);
            }
            set
            {
                rich.Left = lblCaption.Left + lblCaption.Width + value;
            }
        }

        private bool _isMandatoryToFil;
        public bool IsMandatoryToFill
        {
            get
            {
                return _isMandatoryToFil;
            }
            set
            {
                _isMandatoryToFil = value;
            }
        }

        public aetpRichEdit()
        {
            InitializeComponent();
        }

        private void rich_TextChanged(object sender, EventArgs e)
        {
            RaiseEditValueChanged();
            if (IsMandatoryToFill)
            {
                if (rich.Text == "")
                {                    
                    this.errorProvider.SetIconAlignment(rich, ErrorIconAlignment.MiddleLeft);
                    this.errorProvider.SetIconPadding(rich, 1);
                    this.errorProvider.SetError(rich, "Значение должно быть установлено");
                    return;
                }                
                this.errorProvider.Clear();
            }

        }           
    }
}
