﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.Validator;
using aetp.Common.ControlModel;

namespace aetp.Common.Control
{
    public partial class aetpNumericEdit : aetpBaseEdit
    {
        public override BaseEdit Editor
        {
            get
            {
                return seEdit;
            }
            set
            {

            }
        }

        public int MaxLength
        {
            get
            {
                return seEdit.Properties.MaxLength;
            }
            set
            {
                seEdit.Properties.MaxLength = value;
            }
        }
        

        public bool IsFloatValue
        {
            get
            {
                return seEdit.Properties.IsFloatValue;
            }
            set
            {
                seEdit.Properties.IsFloatValue = value;
            }
        }

        public string MaxFloatLengthMask
        {
            get
            {
                return seEdit.Properties.EditMask;
            }
            set
            {
                seEdit.Properties.EditMask = value;
            }
        }

        #region validation properties

        public decimal MinValue
        {
            get
            {
                return seEdit.Properties.MinValue;
            }
            set
            {
                seEdit.Properties.MinValue = value;
            }
        }

        public decimal MaxValue
        {
            get
            {
                return seEdit.Properties.MaxValue;
            }
            set
            {
                seEdit.Properties.MaxValue = value;
            }
        }
        
        #endregion

        public override object EditValue
        {
            get
            {
                if (seEdit.EditValue == null)
                    return null;
                if (IsFloatValue)
                    return Double.Parse(seEdit.EditValue.ToString());
                else
                    return Int64.Parse(seEdit.EditValue.ToString());
            }
            set
            {
                seEdit.EditValue = value;
            }
        }

        public aetpNumericEdit(SimpleEdit se)
        {
            InitializeComponent();
            this.seEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.seEdit.EditValue = null;
            this.seEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.seEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("ru-RU") 
            {
                NumberFormat = new System.Globalization.NumberFormatInfo() { CurrencyDecimalSeparator = ".", NumberDecimalSeparator = ".", NumberGroupSeparator = "" } 
            };
            this.seEdit.Properties.Buttons.Clear();
            
            Configure(se);
        }

        public void Configure(SimpleEdit se)
        {
            if (se.DataType == SimpleDataType.Integer)
            {
                IsFloatValue = false;
                if ((se.MaxValue == 0) && (se.MinValue == 0))
                {
                    MaxValue = long.MaxValue;
                    MinValue = long.MinValue;
                }
                else
                {
                    MaxValue = se.MaxValue;
                    MinValue = se.MinValue;
                }
                string mask_int = string.Empty;
                mask_int = "## ### ### ### ### ### ### ### ##0";
                if (MinValue == 0 && MaxValue > 0) mask_int += ";";
                MaxFloatLengthMask = mask_int;
            }
            else if (se.DataType == SimpleDataType.Double)
            {
                IsFloatValue = true;
                if ((se.MaxValue == 0) && (se.MinValue == 0))
                {
                    MaxValue = Decimal.MaxValue;
                    MinValue = Decimal.MinValue;
                }
                else
                {
                    MaxValue = se.MaxValue;
                    MinValue = se.MinValue;
                }
                MaxLength = se.MaxLength;

                string mask = string.Empty;
                mask = "## ### ### ### ### ### ### ### ##0.";
                
                if (se.MaxFloatLength > 0)
                {
                    mask += new String('#', se.MaxFloatLength);
                }
                else mask += "###########";
                if (MinValue == 0 && MaxValue > 0)mask += ";";
                MaxFloatLengthMask = mask;
            }
        }

        private void seEdit_EditValueChanged(object sender, EventArgs e)
        {
            RaiseEditValueChanged();
        }


        private void seEdit_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Delta != 0)
                DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = true; 
        }
    }
}
