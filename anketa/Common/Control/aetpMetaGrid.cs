﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.ControlModel;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;
using aetp.Common.Validator;

namespace aetp.Common.Control
{
    public partial class aetpMetaGrid : DevExpress.XtraEditors.XtraUserControl, IGrid
	{
		public MetaGrid model { get; set; }
        public List<IGrid> Grids { get; set; }
		public CalculationHelper CalculationHelper { get; set; }

		public bool isLoaded = false;

        public DevExpress.XtraEditors.DXErrorProvider.ErrorInfo GetErrorInfo()
        {
            return null;
        }

        public string Caption
        {
            get
            {
                return model.Caption;
            }
            set
            {
                model.Caption = value;
            }
        }

        public bool HideCaption
        {
            get
            {
                return model.HideCaption;
            }
            set
            {
                model.HideCaption = value;
            }
        }

		public void Configure(MetaGrid model)
		{
            Grids = new List<IGrid>();
			CalculationHelper = new CalculationHelper();
			this.model = model;
			int top = 1;
			int height;

			Hashtable Grid2DT = new Hashtable();

            LabelControl caption = new LabelControl();
            caption.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            caption.Location = new System.Drawing.Point(40, 40);            
            
            caption.Width = model.WidthCaption;

            caption.Top = top;

            
            if (model.HideCaption)
            {
                caption.Text = " ";
            }
            else
            {
                caption.Text = model.Caption;
            }
            _flowpanel.Controls.Add(caption);
            height = caption.Height;
            top = top + height + 10;

			foreach (BaseGrid baseGrid in model.Tables)
			{
				height = 0;
				XtraUserControl editor = null;
				if (baseGrid is Grid)
				{
					Grid grid = baseGrid as Grid;
                    grid.CanResizeColumn = false;                    
					aetpGrid gc = new aetpGrid();
                    Grids.Add(gc);
					gc.CalculationHelper = this.CalculationHelper;
					gc.Configure(grid);
					editor = gc as XtraUserControl;
                    var editor_grid = editor as aetpGrid;
                    editor_grid.gc.Width = grid.WidthGrid;
                    editor_grid.Width = grid.WidthGrid + 200;
                    height = editor_grid.HeightGrid;
                    editor.Height = height;
				}
				if (baseGrid is GridAddedRow)
				{
					GridAddedRow grid = baseGrid as GridAddedRow;
                    grid.CanResizeColumn = false;                    
					aetpGridAddedRow gc = new aetpGridAddedRow();
                    Grids.Add(gc);
					gc.CalculationHelper = this.CalculationHelper;
					gc.Configure(grid);
					editor = gc as XtraUserControl;
                    var editor_grid = editor as aetpGridAddedRow;
				    gc.Resize += UpdateHeight;
                    editor_grid.gc.Width = grid.WidthGrid;
                    editor_grid.Width = grid.WidthGrid + 300;
                    height = editor_grid.gc.Height + 3;
					Grid2DT[gc.model] = gc.dt;

                    editor_grid.panel.Width = grid.WidthGrid + 2;
                    editor_grid.panel.Height = gc.gc.Height + 2;
                    editor_grid.panel.Top = gc.gc.Top - 1;
                    editor_grid.Height = editor_grid.panel.Height + 20;
                    if (editor_grid.IsMandatoryToFill)
                    {
                        editor_grid.panel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
                        editor_grid.panel.Appearance.BorderColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        editor_grid.panel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
                    }
                    editor.Height = height + 18;
				}
                    
				if (editor != null)
				{
					editor.Top = top;
                    editor.Margin = new System.Windows.Forms.Padding(0);
                    editor.Padding = new System.Windows.Forms.Padding(1);
					_flowpanel.Controls.Add(editor);
					
					top = top + height + 0;
                    editor.Left = 2;
				}
			}
            RefreshAllFormulas();
			this.Height = top;
			isLoaded = true;
		}

        public void RefreshAllFormulas()
        {
            foreach (IGrid grid in Grids)
            {
                grid.RefreshAllFormulas();
            }
        }

		public aetpMetaGrid()
		{
			InitializeComponent();
		}

		public void UpdateHeight(object sender, EventArgs eventArgs)
		{
			if (!isLoaded)  return;

			int height = 0;
			foreach (System.Windows.Forms.Control control in this._flowpanel.Controls)
			{
                if (control is IGrid)
				{
                    control.Top = height;
					height = height + control.Height;
				}
                if (control is LabelControl)
                {
                    control.Top = height;
                    height = height + control.Height + 10;
                }

			}
            _flowpanel.Height = height;
			this.Height = height;
		}
	}
}
