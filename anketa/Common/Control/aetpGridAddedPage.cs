﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using aetp.Common.ControlModel;
using aetp.Common.Mediator;
using DevExpress.XtraTreeList.Nodes;
using aetp.Common.View;
using DevExpress.Utils;
using System.Text.RegularExpressions;

namespace aetp.Common.Control
{
    public partial class aetpGridAddedPage : DevExpress.XtraEditors.XtraUserControl, IGrid
    {
        public event aetpGridAddedPageChangeEventHandler PageChanged;
        protected void RaisePageChanged(aetpGridAddedPageChange e)
        {
            if (PageChanged != null)
                PageChanged(this, e);
        }

        public string pageName = "aetpGridAddedPage";
        public GridAddedPage model { get; set; }
        public TreeListNode tlParent { get; set; }
        public int PageIndex { get; set; }
        DataTable dt = new DataTable();
        public int Interval_AP
        {
            get
            {
                return gc.Left - (lblCaption.Left + lblCaption.Width);
            }
            set
            {
                gc.Left = lblCaption.Left + lblCaption.Width + value;
            }
        }

        public int HeightHeaderGrid
        {
            get
            {
                return gv.ColumnPanelRowHeight;
            }
            set
            {
                if ((value == 0) || (value < 0)) gv.ColumnPanelRowHeight = 10;
                else
                    gv.ColumnPanelRowHeight = value;
            }
        }

        public int WidthCaption_AP
        {
            get
            {
                return lblCaption.Width;
            }
            set
            {
                lblCaption.Width = value;
            }
        }

        public int WidthGrid_AP
        {
            get
            {
                return gc.Width;
            }
            set
            {
                if ((value == 0) || (value < 0)) gc.Width = 100;
                else
                    gc.Width = value;
            }
        }

        public DevExpress.XtraEditors.DXErrorProvider.ErrorInfo GetErrorInfo()
        {            
            return null;
        }

        public string Caption
        {
            get
            {
                return lblCaption.Text;
            }
            set
            {
                lblCaption.Text = value;
            }
        }

        public bool HideCaption
        {
            get
            {
                return model.HideCaption;
            }
            set
            {
                model.HideCaption = value;
            }
        }


        public int HeightGrid_AP
        {
            get
            {
                return gc.Height;
            }
            set
            {
                if ((value == 0) || (value < 0)) gc.Height = 200;
                else
                    gc.Height = value;
            }
        }

        public void Configure(GridAddedPage model)
        {
            model.gc = gc;
            this.model = model;
            pageName = model.Children[0].Name;
            gc.BeginUpdate();
            gv.BeginUpdate();
            int i = 0;
            foreach (GridAddedPageColumn col in model.Columns)
            {
                DevExpress.XtraGrid.Columns.GridColumn gcol = new DevExpress.XtraGrid.Columns.GridColumn()
                {
                    Name = col.Name,
                    Caption = col.Caption,
                    Visible = true,
                    VisibleIndex = i++,
                    FieldName = col.FieldName,
                    Width = col.WidthColumn                    
                };

                gcol.OptionsColumn.AllowSort = DefaultBoolean.False;
                gcol.OptionsFilter.AllowFilter = false;

                gv.Columns.Add(gcol);
                dt.Columns.Add(col.FieldName);
            }
            if (model.Data == null)
            {
                model.Data = dt;
            }
            
            gc.DataSource = dt;
            gc.EndUpdate();
            gv.EndUpdate();
            Name = model.Name;
            Caption = model.Caption;
            WidthCaption_AP = model.WidthCaption;
            Interval_AP = model.Interval;
            WidthGrid_AP = model.WidthGrid;
            HeightGrid_AP = model.HeightGrid;
            HeightHeaderGrid = model.HeightHeaderGrid;
            gv.OptionsView.AllowHtmlDrawHeaders = true;
            gv.Appearance.HeaderPanel.TextOptions.WordWrap = WordWrap.Wrap;

            gv.OptionsMenu.EnableColumnMenu = false;     
            if (model.HideCaption)
            {
                Caption = " ";
            }
        }

        public void RefreshAllFormulas()
        {
            throw new Exception("Метод который не может быть определен в данном классе");
        }

        public aetpGridAddedPage()
        {
            InitializeComponent();
            PageIndex = 0;
        }

        private void gv_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            
        }

        void ctrl_PropertyChanged(object sender, EventArgs e)
        {
            SetTitlePage((sender as ControlModel.BaseControl).Parent as GridPage);
        }

        void SetTitlePage(GridPage page)
        {
            string caption = (page.Parent as ControlModel.GridAddedPage).Page.Caption;
            foreach (ControlModel.BaseControl ctrl in page.Children)
            {
                if (ctrl.EditValue != null)
                {
                    string name = ctrl.Name.Substring(0, ctrl.Name.IndexOf("$"));
                    caption = (caption.Replace("{" + name + "}", ctrl.EditValue.ToString()));
                }
            }
            
            page.Caption = caption;
            TreeListNode node = page.tlItem;
            node.SetValue("Caption", caption);
        }

        void RemovePage()
        {
            DataRow dr = gv.GetDataRow(gv.FocusedRowHandle);
            for(int i = 1; i < model.Children.Count; i++)
            {
                if (model.Children[i].Tag == dr)
                {
                    (model.Children[i] as GridPage).Dispose();
                    RaisePageChanged(new aetpGridAddedPageChange() { changeType = ChangeType.Remove, page = model.Children[i] as Page });
                    model.Children[i].Remove();
                    
                    break;
                }
            }
            gv.DeleteSelectedRows();
            
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            
        }

        private void gv_ShowingEditor(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

#warning перенести логику удаления в модель
        private void gc_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.Tag.ToString() == "Add")
            {
                int focused_row = gv.FocusedRowHandle;
                model.NewPage(focused_row);
                gv.FocusedRowHandle = focused_row + 1; 
            }
            else if (e.Button.Tag.ToString() == "Remove")
                RemovePage();
        }
    }

    public delegate void aetpGridAddedPageChangeEventHandler(object sender, aetpGridAddedPageChange e);

    public class aetpGridAddedPageChange
    {
        public Page page { get; set; }
        public ChangeType changeType { get; set; }
    }
}
