﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.Mediator;

namespace aetp.Common.Control
{
    public partial class aetpDateEdit : aetpBaseEdit
    {
        public override BaseEdit Editor
        {
            get
            {
                return deEdit;
            }
            set
            {

            }
        }

        public string DateEditMask
        {
            get
            {
                return deEdit.Properties.EditMask;
            }
            set
            {
                deEdit.Properties.EditMask = value;
            }
        }

        public override object EditValue
        {
            get
            {
                if (deEdit.EditValue == null || deEdit.EditValue == DBNull.Value || deEdit.EditValue.ToString() == default(DateTime).ToString())
                    return null;
                return EditorViewSerializerHelper.DateToStr((Convert.ToDateTime(deEdit.EditValue)));
            }
            set
            {
                deEdit.EditValue = value;
            }
        }

        public aetpDateEdit()
        {
            InitializeComponent();
        }

        private void deEdit_EditValueChanged(object sender, EventArgs e)
        {
            RaiseEditValueChanged();
        }
    }
}
