﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;
using aetp.Common.ControlModel;
using System.Collections;

namespace aetp.Common.Control
{
    public partial class aetpComboBoxEdit : aetpBaseEdit
    {
        public virtual event EventHandler SelectedValue_Changed;
        protected void RaiseSelectedValue_Changed()
        {
            if (SelectedValue_Changed != null)
            {
                SelectedValue_Changed(this, null);
            }
        }
        public override BaseEdit Editor
        {
            get
            {
                return comboEdit;
            }
            set
            {

            }
        }

        public override object EditValue
        {
            get
            {
                return comboEdit.EditValue;
            }
            set
            {
                if (value == null)
                {
                    comboEdit.EditValue = null;
                }
                else
                {
                    aetpComboBoxItem newValue = value as aetpComboBoxItem;
                    var item = (from DictionaryListItem i in Items.Items where i.Code == newValue.ID select i).FirstOrDefault();
                    comboEdit.EditValue = item;
                }
            }
        }

        DictionaryList _Items;
        public DictionaryList Items
        {
            set
            {
                _Items = value;
                comboEdit.Properties.Items.Clear();
                if (value != null)
                    comboEdit.Properties.Items.AddRange((value as DictionaryList).Items);
            }
            get
            {
                return _Items;
            }
        }

        public int comboWidth
        {
            get
            {
                return comboEdit.Width;
            }
            set
            {
                comboEdit.Width = value;
            }

        }

        public aetpComboBoxEdit()
        {
            InitializeComponent();
            var buttonDelete = new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete);
            comboEdit.Properties.Buttons.Add(buttonDelete);
            comboEdit.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(Properties_ButtonClick);
        }

        void Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                comboEdit.EditValue = null;
            }
        }

        private void comboEdit_EditValueChanged(object sender, EventArgs e)
        {
            RaiseEditValueChanged();
        }

        private void comboEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            RaiseSelectedValue_Changed();
        }

        private void comboEdit_SelectedValueChanged(object sender, EventArgs e)
        {
            RaiseSelectedValue_Changed();
        }
    }
}
