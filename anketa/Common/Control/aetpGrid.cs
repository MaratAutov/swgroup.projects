﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using aetp.Common.ControlModel;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;
using aetp.Common.Validator;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Reflection;
using aetp.Common.View;
using System.Collections.Generic;
using System.Linq;

namespace aetp.Common.Control
{
    public delegate void GridChangedHandler(object sender, DataRowChangeEventArgs e);
    
	public partial class aetpGrid : DevExpress.XtraEditors.XtraUserControl, IGrid
	{
        public event GridChangedHandler GridChanged;
        void RaiseGridChanged(DataRowChangeEventArgs e)
        {
            if (GridChanged != null)
                GridChanged(this, e);
        }

        DefaultValueControl defaultValueControl = new DefaultValueControl();
		static AppearanceObject _appError;

        public event DataRowChangeEventHandler RowChanged;
        protected void RaiseRowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (RowChanged != null)
                RowChanged(sender, e);
        }

        void dt_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            RaiseGridChanged(e);
        }

		public static AppearanceObject AppError
		{
			get
			{
				if (_appError == null)
				{
					_appError = new AppearanceObject();
					_appError.BackColor = Color.Linen;
					_appError.BackColor2 = Color.Linen;
					_appError.BorderColor = Color.White;
					_appError.ForeColor = Color.Black;
					_appError.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
				}
				return _appError;
			}
			set
			{
				_appError = value;
			}
		}

        public event EventHandler ChangeCell;
        protected void RaiseChangeCell()
        {
            if (ChangeCell != null)
                ChangeCell(this, null);
        }

        static AppearanceObject _appDisabled;
        public static AppearanceObject AppDisabled
        {
            get
            {
                if (_appDisabled == null)
                {
                    _appDisabled = new AppearanceObject();
                    _appDisabled.BackColor = Color.LightGray;
                    _appDisabled.BackColor2 = Color.LightGray;
                    _appDisabled.BorderColor = Color.White;
                }
                return _appDisabled;
            }
            set
            {
                _appError = value;
            }
        }

		private static Color BorderColorForFormula = Color.Pink;

		public int Interval
		{
			get
			{
				return gc.Left - (lblCaption.Left + lblCaption.Width);
			}
			set
			{
				gc.Left = lblCaption.Left + lblCaption.Width + value;
			}
		}

		public int WidthCaption
		{
			get
			{
				return lblCaption.Width;
			}
			set
			{
				lblCaption.Width = value;
			}
		}

		public int WidthGrid
		{
			get
			{
				return gc.Width;
			}
			set
			{
				if ((value == 0) || (value < 0)) gc.Width = 100;
				else
					gc.Width = value;
			}
		}

		public int HeightGrid
		{
			get
			{
				return gc.Height;
			}
			set
			{
				if ((value == 0) || (value < 0)) gc.Height = 200;
				else
					gc.Height = value;

                if (model != null && model.AutoHeight)
                {
                    int getHeight = GetOptimalGridHeight(gv) - (model.HideHeader ? model.HeightHeaderGrid : 0);                     
                    gc.Height = getHeight;
                }
			}
		}

        public int HeightHeaderGrid
        {
            get
            {
                return gv.ColumnPanelRowHeight;
            }
            set
            {
                if ((value == 0) || (value < 0)) gv.ColumnPanelRowHeight = 10;
                else
                    gv.ColumnPanelRowHeight = value;                
            }
        }

		public string Caption
		{
			get
			{
				return lblCaption.Text;
			}
			set
			{
				lblCaption.Text = value;
			}
		}

        public bool HideCaption
        {
            get
            {
                return model.HideCaption;
            }
            set
            {
                model.HideCaption = value;
            }
        }

        public AnchorStyles AnchorGrid
        {
            set
            {
                gc.Anchor = value;
            }
            get
            {
                return gc.Anchor;
            }
        }

        private bool _isLoadView = false;
        public bool IsLoadView
        {
            set
            {
                _isLoadView = value;
            }
            get
            {
                return _isLoadView;
            }
        }

		public Grid model { get; set; }
		public CalculationHelper CalculationHelper { get; set; }
		public DataTable dt = new DataTable();
        

        int columnvisibleindex = 0;

		public void Configure(Grid model)
		{
            // где то вечно нарушается ссылочная целостность элементов внутри таблицы приходится ее восстанавливать
            ReportUtils.NormalizeGrid(model);
            
            model.Refreshing -= model_Refreshing;
            model.Refreshing += new EventHandler(model_Refreshing);
            
            if (!IsPreview)
                model.gc = gc;
            this.model = model;
            if (model.metagrid == null || IsPreview)
            {
                CalculationHelper = new CalculationHelper(new Hashtable() { { model, dt } });
            }
            else
            {
                CalculationHelper.AddToCollection(model, dt);
            }

            gv.OptionsCustomization.AllowColumnMoving = false;
            ToolTipController toolTip = new ToolTipController();
            toolTip.GetActiveObjectInfo += new ToolTipControllerGetActiveObjectInfoEventHandler(toolTip_GetActiveObjectInfo);
            gc.ToolTipController = toolTip;
            //валидация
            AddValidatingItems();
            
            gc.BeginUpdate();
            gv.BeginUpdate();
            
            foreach (GridColumn col in model.Columns)
            {
                DevExpress.XtraGrid.Columns.GridColumn gcol = new DevExpress.XtraGrid.Columns.GridColumn()
                {
                    Name = col.Name,
                    Caption = col.Caption,
                    Visible = true,
                    VisibleIndex = columnvisibleindex++,
                    FieldName = col.Name,
                    Width = col.WidthColumn,
                    Tag = col
                };
                gcol.OptionsColumn.AllowSort = DefaultBoolean.False;
                gcol.OptionsFilter.AllowFilter = false;
                if (model.CanResizeColumn == false)
                {
                   gcol.OptionsColumn.AllowSize = false;                   
                }

                gv.Columns.Add(gcol);
                dt.Columns.Add(col.Name);
                
            }
            
            int r = 0;
            foreach (aetp.Common.ControlModel.GridRow row in model.Rows)
            {
                var items = model.Rows[r].Cells.Select(x =>
                {
                    if (x.Type == GridCellType.Label) return x.Caption;
                    else return x.EditValue;
                }).ToArray();
                dt.Rows.Add(items);
                r++;
            }
            //    // проставляем метки
            //    for (int j = 0; j < model.Columns.Count; j++)
            //    {
            //        if (model.Rows[r].Cells[j].Type == GridCellType.Label)
            //            dtNewRow[j] = model.Rows[r].Cells[j].Caption;
            //        if (model.Rows[r].Cells[j].DefaultValue != null)
            //        {
            //            CellDefaultValue(model.Rows[r].Cells[j].DefaultValue, dtNewRow,j);                        
            //        }                    
            //    }
            //    r++;
            //}
            if (model.Data == null)
            {
                model.Data = dt;
            }
            gc.DataSource = dt;
            
            gc.EndUpdate();
            gv.EndUpdate();
            Name = model.Name;
            Caption = model.Caption;
            WidthCaption = model.WidthCaption;
            Interval = model.Interval;
            WidthGrid = model.WidthGrid;
            HeightHeaderGrid = model.HeightHeaderGrid;
            if (model.AutoHeight)
            {
                int height_grid = 0;
                GridViewInfo viewInfo = gv.GetViewInfo() as GridViewInfo;
                gc.BeginUpdate();
                height_grid = viewInfo.CalcRealViewHeight(new Rectangle(0, 0, WidthGrid + 50, 100000));
                
                gc.EndUpdate();
                HeightGrid = height_grid + (model.HideHeader ? 0 : model.HeightHeaderGrid) - HeightHeaderGrid;
                HeightGrid = GetOptimalGridHeight(gv) - (model.HideHeader ? model.HeightHeaderGrid : 0); 
            }
            else
                HeightGrid = model.HeightGrid;
            
            gv.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(gv_ValidatingEditor);
            gv.CustomDrawColumnHeader += new ColumnHeaderCustomDrawEventHandler(gv_CustomDrawColumnHeader);
            gv.InvalidValueException += gv_InvalidValueException;
            gv.RowCellStyle += new RowCellStyleEventHandler(gv_RowCellStyle);
            gv.RowCellDefaultAlignment += new DevExpress.XtraGrid.Views.Base.RowCellAlignmentEventHandler(gv_RowCellDefaultAlignment);
            gv.MouseDown += new MouseEventHandler(gv_MouseDown);

            gv.OptionsMenu.EnableColumnMenu = false;          
           

            gv.OptionsView.ShowColumnHeaders = !model.HideHeader;
            gv.OptionsPrint.PrintHeader = !model.HideHeader;
            gv.OptionsView.AllowHtmlDrawHeaders = true;
            gv.Appearance.HeaderPanel.TextOptions.WordWrap = WordWrap.Wrap;
            gv.ShowingEditor += new System.ComponentModel.CancelEventHandler(gv_ShowingEditor);
            
            gv.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gv_CellValueChanged);
            gv.OptionsBehavior.ImmediateUpdateRowPosition = false;
            gv.OptionsBehavior.KeepFocusedRowOnUpdate = false;
            gv.OptionsNavigation.AutoFocusNewRow = false;
            gv.OptionsNavigation.AutoMoveRowFocus = false;
            gv.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(gv_CustomRowCellEdit);
            gv.KeyDown += new KeyEventHandler(gv_KeyDown);
            gv.RefreshData();
            if (model.HideCaption)
            {
                Caption = " ";
            }
            RefreshAllFormulas();
            //dt.RowChanged += new DataRowChangeEventHandler(dt_RowChanged);
		}

        public void RefreshAllFormulas()
        {
            foreach (var row in model.Rows)
            {
                foreach (var cell in row.Cells)
                {
                    CalculationHelper.UpdateTotal(cell);
                }
            }
        }
        
        void model_Refreshing(object sender, EventArgs e)
        {
            dt.AcceptChanges();
            gv.RefreshData();
        }

        private void gv_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (!IsLoadView)
                IsLoadView = true;
        }

        void gv_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == 0x16);
        }

        private void gv_KeyDown(object sender, KeyEventArgs e)
        {
            // для того что бы по enter и стрелочкам вызвать событие изменения значения для пересчета формул
            if (e.KeyValue == 13 || e.KeyCode == Keys.Right || e.KeyCode == Keys.Left)
            {
                if (gv.ActiveEditor != null)
                {
                    DataRow dr = gv.GetFocusedDataRow();
                    dr[gv.FocusedColumn.AbsoluteIndex] = gv.ActiveEditor.EditValue ?? DBNull.Value;
                    RaiseGridChanged(new DataRowChangeEventArgs(dr, DataRowAction.Change));
                }
            }
            if (e.KeyValue == 13)
            {
                BeginInvoke(new MethodInvoker(() =>
                {
                    gv.MoveNext();
                    int col = gv.FocusedColumn.VisibleIndex;
                    gv.FocusedColumn = gv.VisibleColumns[col];
                    
                    e.Handled = true;
                    gv.ShowEditor();
                }));
            }
            if (e.Control && e.KeyCode == Keys.V)
            {
                try
                {
                    PasteData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        void PasteData()
        {
            IDataObject dataInClipboard = Clipboard.GetDataObject();
            char[] rowSplitter = { '\r', '\n' };
            string stringInClipboard =
                (string)dataInClipboard.GetData(DataFormats.Text);

            string[] rowsInClipboard =
                stringInClipboard.Split(rowSplitter,
                StringSplitOptions.RemoveEmptyEntries);
            if (rowsInClipboard.Length < 1)
            {
                throw new InvalidOperationException("Невозможно вставить данные в таблицу. В буфере обмена нет данных для вставки.");
            }
            List<string[]> rows = new List<string[]>();
            int columnCount = -1;
            foreach (string strRow in rowsInClipboard)
            {
                string[] cells = strRow.Split('\t');
                if (columnCount == -1)
                {
                    columnCount = cells.Length;
                }
                if (cells.Length < 1 || cells.Length != columnCount)
                {
                    throw new InvalidOperationException("Невозможно вставить данные в таблицу. В буфере обмена нет данных для вставки.");
                }
                rows.Add(cells);
            }
            int endRow = gv.FocusedRowHandle + rows.Count;
            if (endRow > model.Rows.Count)
            {
                endRow = model.Rows.Count;
            }
            int endColumn = gv.FocusedColumn.VisibleIndex + columnCount;
            if (endColumn > model.Columns.Count)
            {
                endColumn = model.Columns.Count;
            }
            rows = CorrectDataForImport(rows, gv.FocusedRowHandle, gv.FocusedColumn.VisibleIndex, endRow, endColumn);
            int r1 = 0;
            for (int r = gv.FocusedRowHandle; r < endRow; r++)
            {
                int c1 = 0;
                for (int c = gv.FocusedColumn.VisibleIndex; c < endColumn; c++)
                {
                    model.Data.Rows[r][c] = rows[r1][c1];
                    c1++;
                }
                r1++;
            }
            model.Data.AcceptChanges();
        }

        List<string[]> CorrectDataForImport(List<string[]> rows, int startRow, int startColumn, int endRow, int endColumn)
        {
            int r1 = 0;
            for (int r = startRow; r < endRow; r++)
            {
                int c1 = 0;
                for (int c = startColumn; c < endColumn; c++)
                {
                    switch (model.Rows[r].Cells[c].Type)
                    {
                        case GridCellType.Date:
                            DateTime dt;
                            if (!DateTime.TryParse(rows[r1][c1], out dt))
                            {
                                rows[r1][c1] = null;
                            }
                            break;
                        case GridCellType.Integer:
                            long val;
                            if (!long.TryParse(rows[r1][c1], out val))
                            {
                                rows[r1][c1] = null;
                            }
                            break;
                        case GridCellType.Double:
                            Double dval;
                            if (!Double.TryParse(rows[r1][c1], out dval))
                            {
                                rows[r1][c1] = null;
                            }
                            break;
                        case GridCellType.Label:
                            rows[r1][c1] = model.Rows[r].Cells[c].Caption ?? " ";
                            break;
                        case GridCellType.List:
                            var item = ReportUtils.GetDictionaryItem(model.Rows[r].Cells[c].Source, rows[r1][c1]);
                            if (item == null) rows[r1][c1] = null;
                            else rows[r1][c1] = item.Code;
                            break;
                        case GridCellType.Check:
                            rows[r1][c1] = null;
                            break;
                    }
                    c1++;
                }
                r1++;
            }
            return rows;
        }

        private int GetOptimalGridHeight(GridView gridView)
        { 
            GridViewInfo info = gridView.GetViewInfo() as GridViewInfo;
            Type type = gridView.GetType();

            int totalRowHeight = 0;
            foreach (GridDataRowInfo ri in info.RowsInfo)
                totalRowHeight += ri.Bounds.Height;

            FieldInfo fi = type.GetField("scrollInfo", BindingFlags.NonPublic | BindingFlags.Instance);
            DevExpress.XtraGrid.Scrolling.ScrollInfo scrollInfo = fi.GetValue(gridView) as DevExpress.XtraGrid.Scrolling.ScrollInfo;

            int height = info.CalcRealViewHeight(new Rectangle(0, 0, Int32.MaxValue, Int32.MaxValue));
            if (scrollInfo.HScrollVisible) height += scrollInfo.HScroll.Height;

            
            return height;
        }

        private int GetOptimalGridHeight2(GridView gridView)
        {
            gc.BeginUpdate();
            GridViewInfo info = gridView.GetViewInfo() as GridViewInfo;
            double totalRowHeight = 0;
            foreach (GridDataRowInfo ri in info.RowsInfo)
                totalRowHeight += ri.Bounds.Height + 0.8;
            gc.EndUpdate();
            return Convert.ToInt32(totalRowHeight);
        }


        //public void CellDefaultValue(object DefaultValue,DataRow dr,int index)
        //{
        //    if (DefaultValue is aetp.Common.ControlModel.UITypeEditors.OrganizationDataItem)
        //    {
        //        dr[index] = defaultValueControl.GetDefaultValue(DefaultValue.ToString());                
        //    }
        //    else
        //        dr[index] = DefaultValue;
        //}

        public bool IsPreview { get; set; }

        public void RefreshCellsData()
        {
            for (int i = 0; i < model.Rows.Count; i++)
            {
                for (int j = 0; j < model.Rows[i].Cells.Count; j++)
                {
                    if (model.Rows[i].Cells[j].Type == GridCellType.Label)
                        dt.Rows[i][j] = model.Rows[i].Cells[j].Caption;
                    if (model.Rows[i].Cells[j].DefaultValue != null)
                    {
                        object o = model.Rows[i].Cells[j].GetFirstDefaultValue();
                        if (o != null)
                            dt.Rows[i][j] = o;
                    }
                }
            }
        }

        public void NewRow()
        {
            dt.Rows.Add(dt.NewRow());
            AddValidatingItems(0, model.Columns.Count, dt.Rows.Count - 1, dt.Rows.Count); 
            if (model.AutoHeight)
                model.HeightGrid = dt.Rows.Count * 21 + 50;
        }


        public void RemoveRowAt(int i)
        {
            dt.Rows.RemoveAt(i);
            if (model.AutoHeight)
                model.HeightGrid = dt.Rows.Count * 21 + 50;
        }

        public void NewColumn(GridColumn col)
        {
            DevExpress.XtraGrid.Columns.GridColumn gcol = new DevExpress.XtraGrid.Columns.GridColumn()
            {
                Name = col.Name,
                Caption = col.Caption,
                Visible = true,
                VisibleIndex = columnvisibleindex++,
                FieldName = col.Name,
                Width = col.WidthColumn,
                Tag = col
            };
            gcol.OptionsColumn.AllowSort = DefaultBoolean.False;

            if (model.CanResizeColumn == false)
            {
                gcol.MaxWidth = col.WidthColumn;
                gcol.MinWidth = col.WidthColumn;
            }

            gv.Columns.Add(gcol);
            dt.Columns.Add(col.Name);
            AddValidatingItems(model.Columns.Count - 1, model.Columns.Count, 0, dt.Rows.Count);
        }

        public void RemoveColumnAt(int i)
        {
            dt.Columns.RemoveAt(i);
            gv.Columns.RemoveAt(i);
        }

        public void SwapRow(int r1, int r2)
        {
            DataRow drnew = dt.NewRow();
            drnew.ItemArray = dt.Rows[r1].ItemArray;
            dt.Rows.RemoveAt(r1);
            dt.Rows.InsertAt(drnew, r2);
        }

        public void SwapColumn(int r1,int r2)
        {           
            dt.Columns[r1].SetOrdinal(r2);                
        }

		#region Validating

        public DevExpress.XtraEditors.DXErrorProvider.ErrorInfo GetErrorInfo()
        {
            for (int i = 0; i < model.Columns.Count; i++)
                for (int j = 0; j < model.Rows.Count; j++)
                {
                    DevExpress.XtraEditors.DXErrorProvider.ErrorInfo errinfo = new DevExpress.XtraEditors.DXErrorProvider.ErrorInfo();
                    if (model.Rows[j].Cells[i].Type != GridCellType.Label)
                    {
                        model.Rows[j].Cells[i].GetPropertyError("EditValue", errinfo);
                    }
                    if (!String.IsNullOrEmpty(errinfo.ErrorText))
                        return errinfo;
                }
            return null;
        }        

        public bool GetIsMandatoryCell()
        {
            for (int i = 0; i < model.Columns.Count; i++)
                for (int j = 0; j < model.Rows.Count; j++)
                {
                    DevExpress.XtraEditors.DXErrorProvider.ErrorInfo errinfo = new DevExpress.XtraEditors.DXErrorProvider.ErrorInfo();
                    model.Rows[j].Cells[i].GetPropertyError("EditValue", errinfo);
                    if (!String.IsNullOrEmpty(errinfo.ErrorText))
                        return true;
                }
            return false;
        }

		//Применяет правила валидации к ячейкам
		void AddValidatingItems()
		{
            AddValidatingItems(0, model.Columns.Count, 0, model.Rows.Count);
		}

        void AddValidatingItems(int col_from, int col_to, int row_from, int row_to)
        {
            ValidationCollection.AddGrid(this, model);
            for (int i = col_from; i < col_to; i++)
                for (int j = 0; j < row_to; j++)
                {
                    model.Rows[j].Cells[i].AddValidators();
                }
        }

		void toolTip_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
		{
			if (e.SelectedControl != gc) return;
			ToolTipControlInfo info = null;			
			GridView view = gc.GetViewAt(e.ControlMousePosition) as GridView;
			if (view == null) return;			
			GridHitInfo hi = view.CalcHitInfo(e.ControlMousePosition);			
			if (hi.HitTest == GridHitTest.RowCell)
			{				
				object o = hi.HitTest.ToString();
				DevExpress.XtraEditors.DXErrorProvider.ErrorInfo errinfo = new DevExpress.XtraEditors.DXErrorProvider.ErrorInfo();
				model.Rows[hi.RowHandle].Cells[hi.Column.VisibleIndex].GetPropertyError("EditValue", errinfo);
				if (!String.IsNullOrEmpty(errinfo.ErrorText))
				{
					info = new ToolTipControlInfo(o, errinfo.ErrorText);
				}
			}			
			if (info != null)
				e.Info = info;
		}

		void gv_InvalidValueException(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
		{
			e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.DisplayError;
		}

		void gv_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
		{
            if (IsPreview)
                return;
            GridView view = sender as GridView;
            DevExpress.XtraEditors.DXErrorProvider.ErrorInfo info = new DevExpress.XtraEditors.DXErrorProvider.ErrorInfo();
            model.Rows[view.FocusedRowHandle].Cells[view.FocusedColumn.VisibleIndex].GetPropertyError("EditValue", info, e.Value);

            if (!String.IsNullOrEmpty(info.ErrorText))
            {
                e.ErrorText = info.ErrorText;                
                e.Valid = (info.ErrorType == DevExpress.XtraEditors.DXErrorProvider.ErrorType.Information) ? true : false; 
            }

            RaiseChangeCell();
		}

        private void gv_RowCellDefaultAlignment(object sender, DevExpress.XtraGrid.Views.Base.RowCellAlignmentEventArgs e)
        {
            if (e.RowHandle < model.Rows.Count)
            {
                GridCellCollection cells = model.Rows[e.RowHandle].Cells;

                if (e.Column.VisibleIndex < cells.Count)
                {
                    GridCellItem se = cells[e.Column.VisibleIndex];
                    if (se != null)
                    {                        
                        e.HorzAlignment = se.CellAlign;                        
                    }
                }
            }
        }

		void gv_RowCellStyle(object sender, RowCellStyleEventArgs e)
		{
            gc.SuspendLayout();
                aetp.Common.Print.GridViewInfoEx GVI = new aetp.Common.Print.GridViewInfoEx(gv);

                if (GVI.GetIsPrinting())
                {
                    return;
                }
                if (e.RowHandle >= 0 && e.RowHandle < model.Rows.Count &&
                    e.Column.VisibleIndex >= 0 && e.Column.VisibleIndex < model.Columns.Count)
                {
                    GridCellCollection cells = model.Rows[e.RowHandle].Cells;
                    GridCellItem se = cells[e.Column.VisibleIndex];
                    if (se.Font != null)
                    {
                        e.Appearance.Font = se.Font.Font;
                    }

                    if (se.Color != null)
                    {
                        e.Appearance.ForeColor = se.Color.Color;
                    }                    

                    // если это метка то не проверяем а закрашиваем серым
                    if (model.Rows[e.RowHandle].Cells[e.Column.VisibleIndex].Type == GridCellType.Label)
                    {
                        AppearanceHelper.Apply(e.Appearance, AppDisabled);
                        return;
                    }
                    
                    // проверка на ошибки
                    DevExpress.XtraEditors.DXErrorProvider.ErrorInfo info = new DevExpress.XtraEditors.DXErrorProvider.ErrorInfo();
                    model.Rows[e.RowHandle].Cells[e.Column.VisibleIndex].GetPropertyError("EditValue", info);


                    if (!String.IsNullOrEmpty(info.ErrorText))
                    {
                        if ((se.FormulaForCalculating ?? string.Empty) != string.Empty)
                        {
                            AppearanceObject tmp = AppError.Clone() as AppearanceObject;
                            tmp.BackColor = BorderColorForFormula;

                            AppearanceHelper.Apply(e.Appearance, tmp);
                        }
                        else
                        {
                            AppearanceHelper.Apply(e.Appearance, AppError);
                        }
                    }
                    else
                    {
                        if ((se.FormulaForCalculating ?? string.Empty) != string.Empty)
                        {
                            e.Appearance.BackColor = BorderColorForFormula;
                        }
                    }
                }

                gc.ResumeLayout();
		}

		#endregion

		public aetpGrid()
		{
			InitializeComponent();
            this.repositoryItemSpinEdit1.Mask.Culture = new System.Globalization.CultureInfo("ru-RU")
            {
                NumberFormat = new System.Globalization.NumberFormatInfo() { CurrencyDecimalSeparator = ".", NumberDecimalSeparator = ".", NumberGroupSeparator = " " }                
                
            };

            this.repositoryItemSpinEdit1.DisplayFormat.Format = new System.Globalization.CultureInfo("ru-RU")
            {
                NumberFormat = new System.Globalization.NumberFormatInfo() { CurrencyDecimalSeparator = ".", NumberDecimalSeparator = ".", NumberGroupSeparator = " " }
            };

            string mask = "# ### ### ### ### ### ### ### ###.###########";            
            this.repositoryItemSpinEdit1.IsFloatValue = true;
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = mask;
            this.repositoryItemSpinEdit1.EditMask = mask;


            this.repositoryItemSpinEdit2.Mask.Culture = new System.Globalization.CultureInfo("ru-RU")
            {
                NumberFormat = new System.Globalization.NumberFormatInfo() { CurrencyDecimalSeparator = ".", NumberDecimalSeparator = ".", NumberGroupSeparator = " "}
            };

            this.repositoryItemSpinEdit1.Buttons.Clear();
            this.repositoryItemSpinEdit2.Buttons.Clear();
		}       

        #region Check All Cell

        private void gv_CustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            gc.SuspendLayout();
            if (e.Column == null) return;
            foreach (GridColumn col in model.Columns)
            {
                if (col.Name == e.Column.Name)
                {
                    if (col.CheckColumn)
                    {
                        e.Info.InnerElements.Clear();
                        e.Info.Appearance.ForeColor = Color.Blue;
                        e.Painter.DrawObject(e.Info);
                        DrawCheckBox(e.Graphics, e.Bounds, (getCheckedCount(e.Column) == gv.DataRowCount) && (gv.DataRowCount != 0));
                        e.Handled = true;
                    }
                }
            }
            gc.ResumeLayout();
        }

        RepositoryItemCheckEdit chkedit = new RepositoryItemCheckEdit();

        protected void DrawCheckBox(Graphics g, Rectangle r, bool Checked)
        {
            DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo info;
            DevExpress.XtraEditors.Drawing.CheckEditPainter painter;
            DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs args;
            info = chkedit.CreateViewInfo() as DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo;
            painter = chkedit.CreatePainter() as DevExpress.XtraEditors.Drawing.CheckEditPainter;
            info.EditValue = Checked;

            info.Bounds = r;
            info.PaintAppearance.ForeColor = Color.Black;
            info.CalcViewInfo(g);
            args = new DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs(info, new DevExpress.Utils.Drawing.GraphicsCache(g), r);
            painter.Draw(args);
            args.Cache.Dispose();
        }

        int getCheckedCount(DevExpress.XtraGrid.Columns.GridColumn col)
        {
            int count = 0;
            for (int i = 0; i < gv.DataRowCount; i++)
            {
                GridViewInfo vi = gv.GetViewInfo() as GridViewInfo;
                GridDataRowInfo ri = vi.RowsInfo.FindRow(i) as GridDataRowInfo;
                if (ri != null)
                {
                    RepositoryItem item = ri.Cells[col].Editor;
                    if (item is RepositoryItemCheckEdit)
                    {
                        if (gv.GetRowCellValue(i, col).ToString() == "True")
                            count++;
                    }
                    else count++;
                }
                else count++;
            }
            return count;
        }

        void SetCheckAll(DevExpress.XtraGrid.Columns.GridColumn col, bool check, int width)
        {
            for (int i = 0; i < gv.DataRowCount; i++)
            {
                GridViewInfo vi = gv.GetViewInfo() as GridViewInfo;
                GridDataRowInfo ri = vi.RowsInfo.FindRow(i) as GridDataRowInfo;
                if (ri != null)
                {
                    RepositoryItem item = ri.Cells[col].Editor;
                    if (item is RepositoryItemCheckEdit)
                        gv.SetRowCellValue(i, col, check);
                }
            }


            if (model.metagrid != null)
            {
                foreach (BaseGrid grid in model.metagrid.Tables)
                {                    
                    if (grid.Name != model.Name)
                    {
                        for (int i = 0; i < grid.gc.Views[0].DataRowCount; i++)
                        {
                            GridViewInfo vi = grid.gc.Views[0].GetViewInfo() as GridViewInfo;
                            GridDataRowInfo ri = vi.RowsInfo.FindRow(i) as GridDataRowInfo;
                            
                            if (ri != null)
                            {
                                int width_column = 0;

                                foreach (DevExpress.XtraGrid.Columns.GridColumn column in (grid.gc.Views[0] as GridView).Columns)
                                {
                                    width_column += column.Width;
                                    if (width == width_column)
                                    {
                                        if (grid is Grid)
                                        {
                                            GridCellCollection cells = (grid as Grid).Rows[i].Cells;
                                            GridCellItem se = cells[column.VisibleIndex];
                                            if (se.Type == GridCellType.Check)
                                                (grid.gc.Views[0] as GridView).SetRowCellValue(i, column, check);
                                        }
                                        else if (grid is GridAddedRow)
                                        {
                                            GridAddedRowColumn grid_column = (grid as GridAddedRow).Columns[column.VisibleIndex];                                            
                                            if (grid_column.Type == GridCellType.Check)
                                                (grid.gc.Views[0] as GridView).SetRowCellValue(i, column, check);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }        

        private void gv_MouseDown(object sender, MouseEventArgs e)
        {
            gc.SuspendLayout();
            if (e.Clicks == 1 && e.Button == MouseButtons.Left)
            {
                GridHitInfo info;
                Point pt = gv.GridControl.PointToClient(System.Windows.Forms.Control.MousePosition);
                info = gv.CalcHitInfo(pt);

                int width = 0;

                if (info.Column != null)
                {
                    foreach (GridColumn col in model.Columns)
                    {
                        width += col.WidthColumn;
                            
                        if (col.Name == info.Column.Name)
                        {
                            if ((col.CheckColumn) && info.InColumn)
                            {
                                if (getCheckedCount(info.Column) == gv.DataRowCount)
                                    SetCheckAll(info.Column, false,width);
                                else
                                    SetCheckAll(info.Column, true,width);
                                
                            }
                        }
                    }
                }
            }
            gc.ResumeLayout();
        }

        #endregion

		private void gv_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
		{
            gc.SuspendLayout();    
                if (e.RowHandle >= 0 && e.RowHandle < model.Rows.Count &&
                    e.Column.VisibleIndex >= 0 && e.Column.VisibleIndex < model.Columns.Count)
                {
                    GridCellCollection cells = model.Rows[e.RowHandle].Cells;
                    GridCellItem se = cells[e.Column.VisibleIndex];
                    switch (se.Type)
                    {
                        case GridCellType.Date:
                            e.RepositoryItem = repositoryItemDateEdit1;                            
                            (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemDateEdit).EditMask = "dd.MM.yyyy";
                            (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemDateEdit).Mask.UseMaskAsDisplayFormat = true;
                            (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemDateEdit).CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(repositoryDate_CustomDisplayText);
                            break;
                        case GridCellType.String:
                            e.RepositoryItem = repositoryItemTextEdit1;
                            if (se.Mask != null)
                                e.RepositoryItem.NullText = se.Mask.Description;
                            se.EditValue = e.CellValue.ToString();
                            break;
                        case GridCellType.Label:
                            e.RepositoryItem = repositoryItemMemoEdit1;
                            break;
                        case GridCellType.Check:
                            e.RepositoryItem = repositoryItemCheckEdit1;
                            break;
                        case GridCellType.Double:
                            e.RepositoryItem = repositoryItemSpinEdit1.Clone() as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit;
                            e.RepositoryItem.Name = se.Name;
                            (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).MinValue = se.MinValue;
                            (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).MaxValue = se.MaxValue;
                            if (CalculationHelper != null)
                            {
                                this.CalculationHelper.SetEvent(e.RepositoryItem, se);
                            }
                            
                                string mask = string.Empty;
                                mask = "# ### ### ### ### ### ### ### ###.";
                                if (se.MaxFloatLength > 0)
                                {
                                    for (int i = 0; i < se.MaxFloatLength; i++)
                                    {
                                        mask += "#";
                                        
                                        //(e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).EditMask = mask;
                                    }
                                }
                                else mask += "###########";
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).IsFloatValue = true;
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).DisplayFormat.FormatType = FormatType.Custom;
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).DisplayFormat.FormatString = mask;
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).EditMask = mask;
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(repository_CustomDisplayText);
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Mask.EditMask = mask;
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Mask.ShowPlaceHolders = true;
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Spin -= new DevExpress.XtraEditors.Controls.SpinEventHandler(edit_Spin);
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Spin += new DevExpress.XtraEditors.Controls.SpinEventHandler(edit_Spin);
            
                                //if (se.EditValue != null)
                                //{
                                //    //Double value;
                                //    //if (Double.TryParse(se.EditValue.ToString().Trim('.').Replace(".", ","), out value))
                                //    //    se.EditValue = value;
                                //    //se.EditValue = value.ToString();                                    
                                //    //if (se.EditValue != value)
                                //    //{

                                //    //}
                                //}
                            break;
                        case GridCellType.Integer:
                            e.RepositoryItem = repositoryItemSpinEdit2.Clone() as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit;
                            if (se.MinValue == 0 && se.MaxValue == 0)
                            {
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).MaxValue = Int64.MaxValue;
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).MinValue = Int64.MinValue;
                            }
                            else
                            {
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).MinValue = se.MinValue;
                                (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).MaxValue = se.MaxValue;
                            }
                            e.RepositoryItem.Name = se.Name;
                            (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Spin -= new DevExpress.XtraEditors.Controls.SpinEventHandler(edit_Spin);
                            (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).Spin += new DevExpress.XtraEditors.Controls.SpinEventHandler(edit_Spin);
                            (e.RepositoryItem as DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit).CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(repository_CustomDisplayText);
                            if (CalculationHelper != null)
                            {
                                this.CalculationHelper.SetEvent(e.RepositoryItem, se);
                            }
                            break;
                        case GridCellType.List:
                            RepositoryItemLookUpEdit lu = new RepositoryItemLookUpEdit();
                            lu.AllowNullInput = DefaultBoolean.True;
                            lu.Tag = se;
                            var buttonDelete = new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete);
                            lu.Buttons.Add(buttonDelete);
                            lu.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(lu_ButtonClick);
                            lu.EditValueChanged += new EventHandler(lu_EditValueChanged);
                            lu.KeyDown += new KeyEventHandler(lu_KeyDown);
                            lu.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                            lu.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoFilter;
                            lu.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {                                             
                                            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description","Описание",-1,DevExpress.Utils.FormatType.None,"",true,DevExpress.Utils.HorzAlignment.Default)  
                                            });

                            lu.NullText = "[Значение не задано]";
                            if (se.Source != null)
                            {
                                lu.DataSource = se.Source.Items;                                
                                lu.ValueMember = "Code";
                            }
                            else
                                lu.DataSource = null;
                            e.RepositoryItem = lu;
                            break;
                    }

                    e.RepositoryItem.Appearance.Options.UseTextOptions = true;
                }

                gc.ResumeLayout();
		}

        void lu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                gv.LayoutChanged();
            }
        }

        void lu_EditValueChanged(object sender, EventArgs e)
        {
            var editor = sender as DevExpress.XtraEditors.LookUpEdit;
            ((editor.Tag as RepositoryItemLookUpEdit).Tag as GridCellItem).EditValue = editor.EditValue;
        }

        void lu_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                (sender as DevExpress.XtraEditors.LookUpEdit).EditValue = null;
            }
        }

        private void repository_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.DisplayText))
            {
                int length = e.DisplayText.Length - 3;
                string temp = e.DisplayText;

                if (e.DisplayText.Contains(","))
                {
                    length = e.DisplayText.IndexOf(',') - 3;
                }
                else if (e.DisplayText.Contains("."))
                {
                    length = e.DisplayText.IndexOf('.') - 3;
                }
                while (length > -1)
                {
                    temp = temp.Insert(length, " ");
                    length -= 3;
                }
                e.DisplayText = temp;
            }
        }

        private void repositoryDate_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(e.Value.ToString()))
                {
                    if (e.Value.ToString().Length == 18)
                    {
                        string temp = e.Value.ToString().Remove(10);
                        e.DisplayText = temp;
                    }
                }
            }
            catch
            {
                e.DisplayText = e.Value.ToString();
            }
        }


		private void repositoryItemCheckEdit1_QueryCheckStateByValue(object sender, DevExpress.XtraEditors.Controls.QueryCheckStateByValueEventArgs e)
		{
			bool res = false;

			if (bool.TryParse(e.Value.ToString(), out res))
			{
				e.CheckState = res == true ? CheckState.Checked : CheckState.Unchecked;
				e.Handled = true;
			}
		}

        // дизейблим лейблы
        private void gv_ShowingEditor(object sender, CancelEventArgs e)
        { 
            GridView view = sender as GridView;
            if (model.Rows[view.FocusedRowHandle].Cells[view.FocusedColumn.VisibleIndex].Type == GridCellType.Label ||
                ((model.Rows[view.FocusedRowHandle].Cells[view.FocusedColumn.VisibleIndex].Type == GridCellType.Integer ||
                  model.Rows[view.FocusedRowHandle].Cells[view.FocusedColumn.VisibleIndex].Type == GridCellType.Double) &&
                  (model.Rows[view.FocusedRowHandle].Cells[view.FocusedColumn.VisibleIndex].FormulaForCalculating ?? String.Empty) != String.Empty))
                e.Cancel = true;
        }

        void edit_Spin(object sender, DevExpress.XtraEditors.Controls.SpinEventArgs e)
        {
            e.Handled = true;
        }
	}
}
