﻿namespace aetp.Common.Control
{
    partial class aetpNumericEdit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.seEdit = new DevExpress.XtraEditors.SpinEdit();
            ((System.ComponentModel.ISupportInitialize)(this.seEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCaption
            // 
            this.lblCaption.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblCaption.Location = new System.Drawing.Point(16, 11);
            // 
            // seEdit
            // 
            this.seEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seEdit.Location = new System.Drawing.Point(255, 8);
            this.seEdit.Name = "seEdit";
            this.seEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seEdit.Properties.Mask.EditMask = "d";
            

            this.seEdit.Size = new System.Drawing.Size(184, 20);
            this.seEdit.TabIndex = 2;
            this.seEdit.EditValueChanged += new System.EventHandler(this.seEdit_EditValueChanged);           
            (this.seEdit.Controls[0] as DevExpress.XtraEditors.TextBoxMaskBox).MouseWheel += new System.Windows.Forms.MouseEventHandler(seEdit_MouseWheel);
            // 
            // aetpNumericEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.seEdit);
            this.Name = "aetpNumericEdit";
            this.Size = new System.Drawing.Size(615, 44);
            this.Controls.SetChildIndex(this.lblCaption, 0);
            this.Controls.SetChildIndex(this.seEdit, 0);
            ((System.ComponentModel.ISupportInitialize)(this.seEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

		internal DevExpress.XtraEditors.SpinEdit seEdit;
    }
}
