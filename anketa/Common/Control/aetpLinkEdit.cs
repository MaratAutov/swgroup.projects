﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace aetp.Common.Control
{
    public partial class aetpLinkEdit : aetpBaseNavigate
    {
        public override string Caption
        {
            get
            {
                return linkLabel1.Text;
            }
            set
            {
                linkLabel1.Text = value;
            }
        }

        public aetpLinkEdit()
        {
            InitializeComponent();
        }       

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            base.RaiseNavigate();
        }
    }    
}
