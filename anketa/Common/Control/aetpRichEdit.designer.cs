﻿using System.Drawing;
using System.Windows.Forms;
namespace aetp.Common.Control
{
    partial class aetpRichEdit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(aetpRichEdit));
            this.rich = new DevExpress.XtraRichEdit.RichEditControl();
            this.lblCaption = new DevExpress.XtraEditors.LabelControl();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // rich
            // 
            this.rich.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.rich.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.errorProvider.SetIconAlignment(this.rich, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.rich.Location = new System.Drawing.Point(147, 1);
            this.rich.Name = "rich";
            this.rich.Size = new System.Drawing.Size(568, 150);
            this.rich.TabIndex = 0;
            this.rich.Views.PrintLayoutView.AllowDisplayLineNumbers = false;
            this.rich.Views.SimpleView.Padding = new System.Windows.Forms.Padding(4, 4, 4, 0);
            this.rich.TextChanged += new System.EventHandler(this.rich_TextChanged);
            // 
            // lblCaption
            // 
            this.lblCaption.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblCaption.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lblCaption.Location = new System.Drawing.Point(13, 20);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(102, 13);
            this.lblCaption.TabIndex = 1;
            this.lblCaption.Text = "32123123131231321";
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            this.errorProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider.Icon")));
            // 
            // aetpRichEdit
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.rich);
            this.Controls.Add(this.lblCaption);
            this.Name = "aetpRichEdit";
            this.Size = new System.Drawing.Size(718, 154);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

      
        internal DevExpress.XtraRichEdit.RichEditControl rich;
        internal DevExpress.XtraEditors.LabelControl lblCaption;
        internal System.Windows.Forms.ErrorProvider errorProvider;

    }
}
