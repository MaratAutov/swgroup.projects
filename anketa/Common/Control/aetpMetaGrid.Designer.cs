﻿using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using System.Windows.Forms;

namespace aetp.Common.Control
{
    partial class aetpMetaGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private Panel _flowpanel;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._flowpanel = new Panel();
            this.SuspendLayout();
            // 
            // _flowpanel
            // 
            this._flowpanel.AutoSize = true;
            this._flowpanel.AutoScroll = true;
            //this._flowpanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._flowpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._flowpanel.Location = new System.Drawing.Point(0, 0);
            this._flowpanel.Margin = new System.Windows.Forms.Padding(0);
            this._flowpanel.Name = "_flowpanel";
            this._flowpanel.Size = new System.Drawing.Size(597, 186);
            this._flowpanel.TabIndex = 0;
            // 
            // aetpMetaGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this._flowpanel);
            this.Name = "aetpMetaGrid";
            this.Size = new System.Drawing.Size(597, 186);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
    }
}
