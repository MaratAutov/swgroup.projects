﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.Validator;

namespace aetp.Common.Control
{
    public partial class aetpTextEdit : aetpBaseEdit
    {
        public override BaseEdit Editor 
        {
            get
            {
                return teEdit;
            }
            set
            {

            }
        }
               
        public int MaxLength
        {
            get
            {
                return teEdit.Properties.MaxLength;
            }
            set
            {
                teEdit.Properties.MaxLength = value;
            }
        }

         
        public object _editValue;
        public override object EditValue
        {
            get
            {
                return teEdit.Text;
            }
            set
            {
                teEdit.EditValue = value;    
            }
        }

        #region validation properties

        public string Mask
        {
            get
            {
                return teEdit.Properties.Mask.EditMask;
            }
            set
            {
                teEdit.Properties.Mask.EditMask = value;
            }
        }

        public DevExpress.XtraEditors.Mask.MaskType MaskType
        {
            get
            {
                return teEdit.Properties.Mask.MaskType;
            }
            set
            {
                teEdit.Properties.Mask.MaskType = value;
            }
        }

        #endregion

        public aetpTextEdit()
        {
            InitializeComponent();
        }

        private void teEdit_EditValueChanged(object sender, EventArgs e)
        {
            RaiseEditValueChanged();
        }       
       
    }
}
