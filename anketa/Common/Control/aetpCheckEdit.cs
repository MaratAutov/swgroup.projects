﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace aetp.Common.Control
{
    public partial class aetpCheckEdit : aetpBaseEdit
    {
        public virtual event EventHandler Check_Changed;
        protected void RaiseCheck_Changed()
        {
            if (Check_Changed != null)
            {
                Check_Changed(this, null);
            }
        }

        public override BaseEdit Editor
        {
            get
            {
                return ceEdit;
            }
            set
            {

            }
        }

        public override string Text
        {
            get
            {
                if (ceEdit.EditValue != null && ceEdit.EditValue != DBNull.Value)
                {
                    bool value = (bool)ceEdit.EditValue;
                    if (value == true)
                        return "Да";
                    else
                        return "Нет";
                }
                else return "";
            }
        }       

        public override object EditValue
        {
            get
            {
                if (ceEdit.CheckState == CheckState.Checked)
                    return true;
                else if (ceEdit.CheckState == CheckState.Unchecked)
                    return false;
                else if (ceEdit.CheckState == CheckState.Indeterminate) return 1;
                else
                    return null;
            }
            set
            {
                ceEdit.EditValue = value;
            }
        }

        public aetpCheckEdit()
        {
            InitializeComponent();
        }

        private void ceEdit_EditValueChanged(object sender, EventArgs e)
        {
            RaiseEditValueChanged();
        }

        private void ceEdit_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ceEdit_CheckStateChanged(object sender, EventArgs e)
        {
            RaiseCheck_Changed();
        }
    }
}
