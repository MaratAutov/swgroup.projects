﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace aetp.Common.Control
{
    public interface IGrid
    {
        DevExpress.XtraEditors.DXErrorProvider.ErrorInfo GetErrorInfo();
        string Caption { get; set; }
        bool HideCaption { get; set; }
        void RefreshAllFormulas();
    }
}
