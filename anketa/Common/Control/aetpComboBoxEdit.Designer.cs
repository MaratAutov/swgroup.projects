﻿namespace aetp.Common.Control
{
    partial class aetpComboBoxEdit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.comboEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCaption
            // 
            this.lblCaption.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblCaption.Location = new System.Drawing.Point(16, 11);
            // 
            // comboEdit
            // 
            this.comboEdit.Location = new System.Drawing.Point(255, 8);
            this.comboEdit.Name = "comboEdit";
            this.comboEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboEdit.Size = new System.Drawing.Size(200, 20);
            this.comboEdit.TabIndex = 2;
            this.comboEdit.SelectedIndexChanged += new System.EventHandler(this.comboEdit_SelectedIndexChanged);
            this.comboEdit.SelectedValueChanged += new System.EventHandler(this.comboEdit_SelectedValueChanged);
            this.comboEdit.EditValueChanged += new System.EventHandler(this.comboEdit_EditValueChanged);

            // 
            // aetpComboBoxEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.comboEdit);
            this.Name = "aetpComboBoxEdit";
            this.Size = new System.Drawing.Size(799, 43);
            this.Controls.SetChildIndex(this.lblCaption, 0);
            this.Controls.SetChildIndex(this.comboEdit, 0);
            ((System.ComponentModel.ISupportInitialize)(this.comboEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.ComboBoxEdit comboEdit;

    }
}
