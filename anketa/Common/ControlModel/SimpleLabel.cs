﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Drawing.Design;
using aetp.Common.ControlModel.UITypeEditors;


namespace aetp.Common.ControlModel
{
    public class SimpleLabel : BaseControl
    {
        [Browsable(false)]
        public override bool IsMandatoryToFill { get; set; }

        [DisplayName("Ширина подписи")]
        [Category("Макет")]
        public int WidthCaption { get; set; }

        private myFont font = new myFont();

        [Editor(typeof(FontEditor), typeof(UITypeEditor))]
        [Category("Appearance")]
        public myFont Font
        {
            get { return this.font; }
            set
            {
                this.font = value;
            }
        }


        public SimpleLabel()
        {
            WidthCaption = 100;            
        }       

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Label_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "Label {0}";
            }
        }
    }
}