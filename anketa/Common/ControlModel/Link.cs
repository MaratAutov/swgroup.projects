﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace aetp.Common.ControlModel
{
    public class Link : BaseControl
    {
        [Browsable(false)]
        public override bool IsMandatoryToFill { get; set; }
        [TypeConverter(typeof(LinkTypeConverter))]        
        [DisplayName("Страница для перехода")]
        [Category("Проектирование")]
        public Page LinkPage { get; set; }

        public Link()
        {
            LinkPages = new LinkPageCollection();
            
        }
        [Browsable(false)]
        [TypeConverter(typeof(LinkPageCollectionConverter))]
        public LinkPageCollection LinkPages { get; set; }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Link_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "Link {0}";
            }
        }
    }

    public class LinkTypeConverter : TypeConverter
    {
        
            /// <summary>
            /// Возвращает значение, показывающее, поддерживает ли объект стандартный набор значений, 
            /// которые можно выбрать из списка.
            /// Если не установить принудительно значение в true, есть вероятность, 
            /// что обработчик не будет воспринимать ваш метод GetStandardValues и вы не увидите 
            /// ваш список значений для выбора
            /// </summary>
            /// <param name="context"></param>
            /// <returns></returns>
            public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
            {
                return true;
            }

            /// <summary>
            /// Этот метод можно исключить, но в перспективе он позволит использовать данный класс конвертора 
            /// для вызова из разных классов.
            /// Для этого достаточно добавить в секцию case имя другого класса, который должен содержать свойство 
            /// JobTitle с функцией связи с выпадающим списком
            /// </summary>
            /// <param name="context"></param>
            /// <returns></returns>
            private LinkPageCollection GetCollection(System.ComponentModel.ITypeDescriptorContext context)
            {
                LinkPageCollection collection = new LinkPageCollection();
                switch (context.Instance.GetType().Name)
                {
                    case "Page":
                        collection = ((Link)context.Instance).LinkPages;
                        break;
                    default:
                        ((Link)context.Instance).LinkPages.Clear();
                        Page page = ((Link)context.Instance).Parent as Page;
                        Report report = new Report();
                        GetParent(page, report, ((Link)context.Instance).LinkPages, page);
                                            
                        collection = ((Link)context.Instance).LinkPages;
                        break;
                }
                return collection;
            }

            private void GetParent(Page p, Report report, LinkPageCollection collection,Page link_parent)
            {
                if (p.Parent is Report)
                {
                    report = (p.Parent as Report);
                    GetParentPage(report as Page, collection,link_parent);  
                }
                else GetParent(p.Parent as Page, report,collection,link_parent);
            }

            private void GetParentPage(Page p,LinkPageCollection collection,Page link_parent)
            {                
                foreach (var item in p.Children)
                {
                    if (item is Page)
                    {
                        if (item != link_parent)
                        {
                            collection.Add(item as Page);
                        }
                        GetParentPage(item as Page, collection,link_parent);
                    }
                }                               
            }          

            /// <summary>
            /// Метод возвращает список значений из коллекции должностей для отображения в выпадающем 
            /// списке значений PropertyGrid для должности сотрудника
            /// </summary>
            /// <param name="context"></param>
            /// <returns></returns>
            public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                return new StandardValuesCollection(GetCollection(context));
            }

            /// <summary>
            /// Метод проверяет, можно ли преобразовывать полученное значение свойства от пользователя 
            /// в нужный нам тип
            /// </summary>
            /// <param name="context"></param>
            /// <param name="destinationType"></param>
            /// <returns></returns>
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
            {
                if (destinationType.Equals(typeof(string)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Преобразование типа JobTitle в строку для отображения значения в поле PropertyGrid
            /// </summary>
            /// <param name="context"></param>
            /// <param name="culture"></param>
            /// <param name="value"></param>
            /// <param name="destinationType"></param>
            /// <returns></returns>
            public override object ConvertTo(ITypeDescriptorContext context,
                System.Globalization.CultureInfo culture, object value, Type destinationType)
            {
                if (destinationType == typeof(string) && value is Page)
                {
                    Page item = (Page)value;
                    return item.Name;
                }
                return base.ConvertTo(context, culture, value, destinationType);
            }

            /// <summary>
            /// Проверка на возможность обратного преобразования строкового представления в тип JobTitle
            /// </summary>
            /// <param name="context"></param>
            /// <param name="sourceType"></param>
            /// <returns></returns>
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                if (sourceType.Equals(typeof(string)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Преобразование строкового представления должности в тип JobTitle.
            /// Недостаток данного преобразования - правильность работы приведения типа зависит от формата 
            /// строкового представления типа JobTitle, а именно, от дублирующихся значений.
            /// </summary>
            /// <param name="context"></param>
            /// <param name="culture"></param>
            /// <param name="value"></param>
            /// <returns></returns>
            public override object ConvertFrom(ITypeDescriptorContext context,
                System.Globalization.CultureInfo culture, object value)
            {
                if (value.GetType() == typeof(string))
                {
                    Page itemSelected = GetCollection(context).Count.Equals(0) ?
                        new Page() : GetCollection(context)[0];

                    foreach (Page Item in GetCollection(context))
                    {
                        string sCraftName = Item.Name;
                        if (sCraftName.Equals((string)value))
                        {
                            itemSelected = Item;
                        }
                    }
                    return itemSelected;
                }
                else
                    return base.ConvertFrom(context, culture, value);
            }

            /// <summary>
            /// Возвращает значение, показывающее, является ли исчерпывающим списком коллекция стандартных значений, 
            /// возвращаемая методом.
            /// 
            /// false - данные можно вводить вручную
            /// true - только выбор из списка
            /// </summary>
            /// <param name="context"></param>
            /// <returns></returns>
            public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
            {
                return true;
            }

            /// <summary>
            /// Конструктор метода
            /// </summary>
            public LinkTypeConverter() { }
        
    }

    [DataContract]
    public class LinkPageCollection : CollectionBase, ICustomTypeDescriptor
    {
        #region Методы коллекции
        public void Add(Page Item)
        {
            this.List.Add(Item);
        }

        public void Remove(Page Item)
        {
            this.List.Remove(Item);
        }

        public Page this[int Index]
        {
            get { return (Page)this.List[Index]; }
        }
        #endregion

        #region Реализация интерфейса ICustomTypeDescriptor
        public String GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        public String GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        public object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        public EventDescriptorCollection GetEvents(System.Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        public PropertyDescriptorCollection GetProperties(System.Attribute[] attributes)
        {
            return GetProperties();
        }

        public PropertyDescriptorCollection GetProperties()
        {
            PropertyDescriptorCollection pds = new PropertyDescriptorCollection(null);

            for (int i = 0; i < this.List.Count; i++)
            {
                LinkPageCollectionPropertyDescriptor pd = new LinkPageCollectionPropertyDescriptor(this, i);
                pds.Add(pd);
            }
            return pds;
        }
        #endregion
    }

    public class LinkPageCollectionPropertyDescriptor : PropertyDescriptor
    {
        private LinkPageCollection collection = null;
        private int index = -1;

        public LinkPageCollectionPropertyDescriptor(LinkPageCollection coll, int idx) :
            base("#" + idx.ToString(), null)
        {
            this.collection = coll;
            this.index = idx;
        }

        public override AttributeCollection Attributes
        {
            get
            {
                return new AttributeCollection(null);
            }
        }

        public override bool CanResetValue(object component)
        {
            return true;
        }

        public override Type ComponentType
        {
            get
            {
                return this.collection.GetType();
            }
        }

        public override string DisplayName
        {
            get
            {
                Page Item = this.collection[index];
                return Item.Name;
            }
        }

        public override string Description
        {
            get
            {
                Page Item = this.collection[index];
                StringBuilder sb = new StringBuilder();
                sb.Append(Item.Name);
                sb.Append(", ");
                sb.Append(index + 1);

                return sb.ToString();
            }
        }

        public override object GetValue(object component)
        {
            return this.collection[index];
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override string Name
        {
            get { return "#" + index.ToString(); }
        }

        public override Type PropertyType
        {
            get { return this.collection[index].GetType(); }
        }

        public override void ResetValue(object component)
        {
        }

        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }

        public override void SetValue(object component, object value)
        {

        }
    }

    public class LinkPageCollectionConverter : ExpandableObjectConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture,
            object value, Type destType)
        {
            if (destType == typeof(string) && value is LinkPageCollection)
            {
                return "(Страницы...)";
            }
            return base.ConvertTo(context, culture, value, destType);
        }
    }

    public class LinkPageConverter : ExpandableObjectConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture,
            object value, Type destType)
        {
            if (destType == typeof(string) && value is Page)
            {
                Page jobtitle = (Page)value;
                return jobtitle.Name;
            }
            return base.ConvertTo(context, culture, value, destType);
        }
    }
}