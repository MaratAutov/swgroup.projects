﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.ComponentModel;
using aetp.Common.ControlModel.TypeConverters;
using aetp.Common.ControlModel.Attributes;
using aetp.Common.Validator;
using aetp.Common.ControlModel.UITypeEditors;
using System.Drawing.Design;
using System.Data;
using aetp.Common.View;

namespace aetp.Common.ControlModel
{
    [DataContract]
    public class GridAddedRow : BaseGrid
    {
        private bool _isMandatoryToFill = false;
        public override bool IsMandatoryToFill
        {
            get
            {
                return _isMandatoryToFill;
            }
            set
            {
                _isMandatoryToFill = value;
            }
        }

        public void NewRow(int pos = -1)
        {
            DataRow dr = Data.NewRow();
            for (int i = 0; i < Data.Columns.Count; i++)
            {
                switch (this.Columns[i].Type)
                {
                    case GridCellType.Label:
                        dr[i] = this.Columns[i].TextLabel;
                        break;
                    default:
                        if (this.Columns[i].DefaultValue != null)
                        {
                            if (this.Columns[i].DefaultValue is aetp.Common.ControlModel.UITypeEditors.OrganizationDataItem)
                            {
                                dr[i] = DefaultValueControl.GetDefaultValue(this.Columns[i].DefaultValue.ToString());
                            }
                            else
                            {
                                dr[i] = this.Columns[i].DefaultValue;
                            }
                        }
                        break;
                }
            }
            if (pos != -1)
            {
                Data.Rows.InsertAt(dr, pos);
            }
            else
            {
                Data.Rows.Add(dr);
            }
            Data.AcceptChanges();
        }

        [DataMember]
        [Editor(typeof(GridAddedRowColumnEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Проектирование")]
        [DisplayName("Колонки")]
        public virtual GridAddedRowColumnCollection Columns { get; set; }       
       

        [DataMember]
        [DisplayName("Высота таблицы")]
		[Browsable(false)]
        [Category("Макет")]
        public override int HeightGrid { get; set; }

		[DataMember]
		private int _HeightGridInRows;

		[DisplayName("Высота таблицы (в строках)")]
		[Category("Макет")]
		public int HeightGridInRows
		{
			get { return _HeightGridInRows; }
			set
			{
				_HeightGridInRows = value;
				HeightGrid = _HeightGridInRows * 21 + 20 + HeightHeaderGrid;
			}
		}

		[DataMember]
		[DisplayName("Автоподбор высоты")]
		[Category("Макет")]
		public bool AutoHeight { get; set; }       


        [DataMember]
        [DisplayName("Скрыть заголовок")]
        [Category("Макет")]
        public bool HideHeader { get; set; }


        [DataMember]
        [DisplayName("ResizeColumn")]
        [Browsable(false)]
        [Category("Макет")]
        public bool CanResizeColumn { get; set; }

        string _serializingRowTag;
        [DisplayName("Имя тэга строки")]
        [Category("Сериализация")]
        [DataMember]
        public string SerializingRowTag
        {
            get
            {
                if (String.IsNullOrEmpty(_serializingRowTag))
                    return Name + "_1";
                else
                    return _serializingRowTag;
            }
            set
            {
                _serializingRowTag = value;
            }
        }

        public GridAddedRow()
        {
            Columns = new GridAddedRowColumnCollection(this);
            Data = new System.Data.DataTable();
            WidthCaption = 100;
            WidthGrid = 480;
            Interval = 6;
            HeightGrid = 200;
            CanResizeColumn = true;
            Meta_Table = false;
            HeightHeaderGrid = 22;
            IsMandatoryToFill = false;
        }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Table_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "Table {0}";
            }
        }

        public override object Clone()
        {
            GridAddedRow newGrid = (GridAddedRow)this.MemberwiseClone();
            newGrid.Columns = new GridAddedRowColumnCollection(newGrid);
            foreach (var column in this.Columns)
            {
                GridAddedRowColumn newColumn = (GridAddedRowColumn)column.Clone();
                newColumn.Grid = newGrid;
                newGrid.Columns.Add(newColumn);
            }
            return newGrid;
        }
    }

    [DataContract]
    public class GridAddedRowColumnCollection : Collection<GridAddedRowColumn> 
    {
                
        [DataMember]
        public GridAddedRow Grid { get; set; }

        public GridAddedRowColumnCollection(GridAddedRow grid)
        {
            this.Grid = grid;
        }

        protected override void InsertItem(int index, GridAddedRowColumn item)
        {
            item.Grid = Grid;
            item.Root = Grid.Root;
            item.Items = new DictionaryCollection((Grid.Root as Report).MetaData.Dictionarys);
            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, GridAddedRowColumn item)
        {
            item.Grid = Grid;
            item.Root = Grid.Root;
            base.SetItem(index, item);
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
        }
    }
    
    public class GridAddedRowColumnEditor : GridCollectionEditor 
    {

        public GridAddedRowColumnEditor(Type type)
            : base(type)
        {

        }

        protected override Type CreateCollectionItemType()
        {
            return typeof(GridAddedRowColumn);
        }

        protected override object CreateInstance(Type itemType)
        {
            return base.CreateInstance(itemType);
        }

        protected override void DestroyInstance(object instance)
        {
            base.DestroyInstance(instance);
        }
                
        protected override void ValidateEditValue(CollectionForm collectionForm)
        {
            if (okayButtons.ContainsKey(collectionForm))
            {
                Button okayButton = okayButtons[collectionForm];
                GridAddedRowColumnCollection items = collectionForm.EditValue as GridAddedRowColumnCollection;
                bool ok = true;
                for (int i = 0; i < items.Count; i++)
                {
                    if (String.IsNullOrEmpty(items[i].Name))
                    {
                        ok = false;
                        break;
                    }
                }
                okayButton.Enabled = ok;
            }
        }
    }

    [DataContract]
    public class GridAddedRowColumn : BaseControl 
    {
        int _widthcolumn;
        bool _checkColumn;
        string _textLabel;

        public GridAddedRowColumn()
        {
            Mask = new MaskItem();
        }

        [DynamicPropertyFilter("Type", "String")]
        [DisplayName("Маска")]
        [Category("Проверка")]
        [DataMember]
        public MaskItem Mask { get; set; }

        [DataMember]
        [DynamicPropertyFilter("Type", "Check")]
        [DisplayName("Checked column")]
        [Category("Проектирование")]
        public bool CheckColumn
        {
            get
            {
                return _checkColumn;
            }
            set
            {
                _checkColumn = value;
            }
        }

        [DynamicPropertyFilter("Type", "Check")]
        [DisplayName("Тип сериализации")]
        [Category("Сериализация")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [DataMember]
        public BoolSerializtionType boolSerializationType { get; set; }

        [DataMember]
        [Browsable(false)]
        public GridAddedRow Grid { get; set; }
        GridCellType _type;
        [DataMember(Order = 0)]
        [DisplayName("Тип")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [Category("Проектирование")]
        public GridCellType Type 
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                DefaultValue = null;
            }
        }
        [DynamicPropertyFilter("Type", "String,Integer,Double,List,DateTime")]
        [DisplayName("Значение по умолчанию")]
        [Editor(typeof(EditValueEditor), typeof(UITypeEditor))]
        [DefaultValue(null)]
        [DataMember(Order = 1)]
        public object DefaultValue { get; set; }

        [DynamicPropertyFilter("Type", "List")]
        [DisplayName("Словарь")]
        [TypeConverter(typeof(DictionaryTypeConverter))]
        [Category("Данные")]
        [DataMember]
        public DictionaryList Source { get; set; }

        [DataMember]
        [DynamicPropertyFilter("Type", "Double")]
        [DisplayName("Число знаков после запятой")]
        [Category("Проверка")]
        public int MaxFloatLength { get; set; }


        [DynamicPropertyFilter("Type", "Label")]
        [DisplayName("Текст")]
        [Category("Данные")]
        [DefaultValue("")]
        [DataMember]
        public string TextLabel
        {
            get
            {
                if (String.IsNullOrEmpty(_textLabel))
                    return " ";
                else
                return _textLabel;
            }
            set
            {
                _textLabel = value;
            }
        }

        [DataMember]
        [DisplayName("Ширина столбца")]
        [Category("Проектирование")]
        [DefaultValue(100)]       
        public int WidthColumn
        {
            get
            {
                return _widthcolumn;
            }
            set
            {
                if (Convert.ToInt32(value) > 0)
                    _widthcolumn = value;
                else _widthcolumn = 100;
            }
        }

        [TypeConverter(typeof(DictionaryCollection))]
        [DataMember]
        DictionaryCollection _items;
        [Browsable(false)]
        public DictionaryCollection Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
            }
        }

        string _name;
        [DataMember]
        [DisplayName("Имя")]
        [Category("Проектирование")]
        public override string Name
        {
            get
            {
                if (String.IsNullOrEmpty(_name))
                    _name = String.Format(TemplateName, new_element_index);
                return _name;
            }
            set
            {
                if (Grid != null)
                    nameValidator.Validate(Grid.Columns.ToList<BaseControl>(), value, this);
                _name = value;
            }
        }

		[DynamicPropertyFilter("Type", "Double,Integer")]
		[DisplayName("Агрегирующая функция для столбца")]
		[TypeConverter(typeof(EnumTypeConverter))]
		[Category("Данные")]
		[DataMember]
		public AggregateFunctions AggregateFunction { get; set; }


        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Column_{0}";
            }
        }

        public override object Clone()
        {
            var c = ((GridAddedRowColumn)base.Clone());
            c.EditorValidators = new List<IValidationItem>();
            c.AddValidators();
            return c;
        }

        public Type GetType()
        {
            switch (Type)
            {
                case GridCellType.Check:
                    return typeof(bool);
                case GridCellType.Double:
                    return typeof(double);
                case GridCellType.Integer:
                    return typeof(long);
                case GridCellType.Date:
                    return typeof(DateTime);
                default:
                    return typeof(string);
            }
        }

        public void AddValidators()
        {
            if (this.IsMandatoryToFill)
            {
                /* mahalin 19-09-2013
                * Защита от возможного забития списка валидаторов дублями
                */
                List<IValidationItem> list = this.EditorValidators.Where(x => { return x is EmptyValidationItem; }).ToList();
                foreach (IValidationItem v in list)
                {
                    this.EditorValidators.Remove(v);
                }

                if (Type == GridCellType.Double || Type == GridCellType.Integer)
                    this.EditorValidators.Add(new EmptyValidationItem(ValidatorItemType.num));
                else
                    this.EditorValidators.Add(new EmptyValidationItem());
            }
            if (this.Type == GridCellType.String && !String.IsNullOrEmpty(this.Mask.Mask))
            {
                /* mahalin 19-09-2013
                * Защита от возможного забития списка валидаторов дублями
                */
                List<IValidationItem> list = this.EditorValidators.Where(x => { return x is RegexValidationItem; }).ToList();
                foreach (IValidationItem v in list)
                {
                    this.EditorValidators.Remove(v);
                }

                this.EditorValidators.Add(new RegexValidationItem(this.Mask));
            }
        }
    }

}
