﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Globalization;
using System.Collections;

namespace aetp.Common.ControlModel.TypeConverters
{
    class ListControlConverter:TypeConverter
    {
        /// <summary>
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            return true;
        }
                
        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {            
            switch (context.Instance.GetType().Name)
            {
                case "MetaConrol":
                    List<BaseControl> list_meta = new List<BaseControl>();
                    Page meta_parent = new Page();
                    GetParent(((MetaConrol)context.Instance).RootPage, (MetaConrol)context.Instance, ref meta_parent);
                    GetMetaControlParent(meta_parent, (MetaConrol)context.Instance, ref list_meta);
                    return new TypeConverter.StandardValuesCollection(list_meta);

                case "Page":
                    List<BaseControl> list = new List<BaseControl>();                    
                    GetPageParent(((Page)context.Instance).RootPage, (Page)context.Instance, ref list);
                    return new TypeConverter.StandardValuesCollection(list);   
            }
            return null;
        }

        void GetParent(Page parentPage, MetaConrol meta, ref Page parent)
        {
            if (parentPage.ParentPage != null)
            {
                GetParent(parentPage.ParentPage as Page, meta, ref parent);
            }
            else
            {
                parent = parentPage;
            }            
        }       

        void GetPageParent(Page parentPage, Page page, ref List<BaseControl> list)
        {
            foreach (var item in parentPage.Children)
            {
                if (item is Page && item != page)
                    GetPageParent(item as Page, page, ref list);
                if (item is MetaConrol)
                {
                    foreach (var meta_item in item.Children)
                    {
                        if ((meta_item is ComboBox && page.type == TypeControl.combo) ||
                    (meta_item is SimpleEdit) && (meta_item as SimpleEdit).DataType == SimpleDataType.Bool && page.type == TypeControl.check)
                            list.Add(meta_item);
                    }
                }
                if ((page.type == TypeControl.check && (item is SimpleEdit) && (item as SimpleEdit).DataType == SimpleDataType.Bool) ||
                    (page.type == TypeControl.combo && item is ComboBox))
                    list.Add(item);
            }            
        }

        void GetMetaControlParent(Page parentPage, MetaConrol meta, ref List<BaseControl> list)
        {
            foreach (var item in parentPage.Children)
                {
                    if (item is Page)
                        GetMetaControlParent(item as Page, meta, ref list);
                    if ((item is ComboBox && meta.type == TypeControl.combo) ||
                        (item is SimpleEdit) && (item as SimpleEdit).DataType == SimpleDataType.Bool && meta.type == TypeControl.check)
                        list.Add(item);
                    
                    if (item is MetaConrol && item.Name != meta.Name)
                    {
                        foreach (var meta_item in item.Children)
                        {
                            if ((meta_item is ComboBox && meta.type == TypeControl.combo) ||
                        (meta_item is SimpleEdit) && (meta_item as SimpleEdit).DataType == SimpleDataType.Bool && meta.type == TypeControl.check)
                                list.Add(meta_item);
                        }
                    }

                    if (item is GridAddedPage)
                    {
                        GridAddedPage gc = item as GridAddedPage;
                        if (gc.Children.Count > 0 && gc.Children[0] is Page)
                        {
                            foreach (var page_item in gc.Children[0].Children)
                            {
                                if ((page_item is ComboBox && meta.type == TypeControl.combo) ||
                        (page_item is SimpleEdit) && (page_item as SimpleEdit).DataType == SimpleDataType.Bool && meta.type == TypeControl.check)
                                    list.Add(page_item);
                                if (page_item is Page)
                                    GetMetaControlParent(page_item as Page, meta, ref list);
                            }                            
                        }
                    }
                }
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override object ConvertTo(ITypeDescriptorContext context,
            System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string) && value is BaseControl)
            {
                BaseControl item = (BaseControl)value;
                return item.Name;
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        /// <summary>
        /// Проверка на возможность обратного преобразования строкового представления
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sourceType"></param>
        /// <returns></returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType.Equals(typeof(string)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Преобразование строкового представления 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext context,
            System.Globalization.CultureInfo culture, object value)
        {
            if (value.GetType() == typeof(string))
            {
                return FindControl((context.Instance as BaseControl).Root.Children, value.ToString());
            }
            return base.ConvertFrom(context, culture, value);
            
        }

        BaseControl FindControl(List<BaseControl> ctrls, string name)
        {
            foreach (var item in ctrls)
            {
                if (item is Page)
                {
                    BaseControl bs = FindControl((item as Page).Children, name);
                    if (bs != null && bs.Name == name)
                        return bs;
                }
                if (item is MetaConrol)
                {
                    BaseControl bs = FindControl((item as MetaConrol).Children, name);
                    if (bs != null && bs.Name == name)
                        return bs;
                }
                if (item is GridAddedPage)
                {
                    GridAddedPage gc = item as GridAddedPage;
                    if (gc.Children.Count > 0 && gc.Children[0] is Page)
                    {
                        BaseControl bs = FindControl(gc.Children[0].Children, name);
                        if (bs != null && bs.Name == name)
                            return bs;
                    }
                }
                if (item.Name == name)
                    return item;
            }
            return null;

        }
        
    }    
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                    