﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Globalization;
using System.Collections;

namespace aetp.Common.ControlModel.TypeConverters
{
    class MaskConverter:TypeConverter
    {
        /// <summary>
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            return true;
        }
                
        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {            
           
                    List<string> list = new List<string>();

                    foreach (MaskConfig.MaskInfo item in AppGlobal.Instance.MaskConfig.ListMaskInfo)
                    {
                        list.Add(item.Name);
                    }
                    return new TypeConverter.StandardValuesCollection(list);              
            
        }

        void GetParent(Page parentPage, MetaConrol meta, ref Page parent)
        {
            if (parentPage.ParentPage != null)
            {
                GetParent(parentPage.ParentPage as Page, meta, ref parent);
            }
            else
            {
                parent = parentPage;
            }            
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override object ConvertTo(ITypeDescriptorContext context,
            System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string) && value is BaseControl)
            {
                BaseControl item = (BaseControl)value;
                return item.Name;
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        /// <summary>
        /// Проверка на возможность обратного преобразования строкового представления
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sourceType"></param>
        /// <returns></returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType.Equals(typeof(string)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Преобразование строкового представления 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext context,
            System.Globalization.CultureInfo culture, object value)
        {
            if (value.GetType() == typeof(string))
            {
                return value.ToString();
            }
            return base.ConvertFrom(context, culture, value);
        }        
        
    }    
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                    