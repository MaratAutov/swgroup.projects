﻿using System;
using System.Collections.Generic;
using aetp.Common.ControlModel.UITypeEditors;
using System.ComponentModel;
using System.Xml.Serialization;
using aetp.Common.Validator;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.Validator;
using DevExpress.XtraTreeList.Nodes;
using aetp.Common.ControlModel.TypeConverters;
using DevExpress.XtraEditors.DXErrorProvider;
using aetp.Common.ControlModel.Attributes;
using System.Data;

namespace aetp.Common.ControlModel
{
	[DataContract, KnownType(typeof (Report))]
	public class BaseGrid : BaseControl
	{
        [Browsable(false)]
        public DataTable Data { get; set; }
        
        [Browsable(false)]
        public DevExpress.XtraGrid.GridControl gc;

		[Browsable(false)]
		[DataMember]
		public MetaGrid metagrid;

        string _caption = null;
        [DynamicPropertyFilter("Meta_Table", "False")]
        [DisplayName("Подпись")]
        [DataMember]
        [Category("Проектирование")]
        public override string Caption
        {
            get
            {
                if ((_caption == null) || (_caption == ""))
                    _caption = String.Format(TemplateCaption, new_element_index);
                return _caption;
            }
            set
            {
                _caption = value;
            }
        }

        [DynamicPropertyFilter("Meta_Table", "False")]
        [DisplayName("Расстояние между подписью и таблицей")]
        [Category("Макет")]
        [DataMember]
        public virtual int Interval { get; set; }

        [DynamicPropertyFilter("Meta_Table", "False")]
        [DisplayName("Ширина подписи")]
        [Category("Макет")]
        [DataMember]
        public virtual int WidthCaption { get; set; }

        [DisplayName("Ширина таблицы")]
        [DataMember]
        [Category("Макет")]
        public virtual int WidthGrid { get; set; }

        [DynamicPropertyFilter("Meta_Table", "False")]        
        [DisplayName("Скрыть подпись")]
        [Category("Макет")]
        [DataMember]
        public bool HideCaption
        {
            get;
            set;
        }

        [DisplayName("Высота таблицы")]
        [Category("Макет")]
        [DataMember]
        public virtual int HeightGrid { get; set; }

        [DisplayName("Высота заголовков таблицы")]
        [Category("Макет")]
        [DataMember]
        public virtual int HeightHeaderGrid { get; set; }


        private bool _meta_table;
        [Browsable (false)]
        public virtual bool Meta_Table
        {
            get
            {
                return _meta_table;
            }
            set
            {
                _meta_table = value;
            }
        }

        public override void ClearProperty()
        {
            if (Data != null)
                Data.Clear();
        }
	}
}
