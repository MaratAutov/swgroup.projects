﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.UITypeEditors;
using System.Drawing.Design;

namespace aetp.Common.ControlModel
{
    public class ComboBox : BaseControl
    {
        int _width;

        [DisplayName("Ширина")]
        [Category("Макет")]        
        public int ComboWidth
        {
            get
            {
                return _width;
            }
            set
            {
                if (value > 20)
                    _width = value;
                else _width = 500;
            }
        }

        [DisplayName("Значение по умолчанию")]
        [Editor(typeof(EditValueEditor), typeof(UITypeEditor))]
        [DefaultValue(null)]
        [DataMember]
        public object DefaultValue { get; set; }

        [DisplayName("Словарь")]
        [TypeConverter(typeof(DictionaryTypeConverter))]
        [Category("Данные")]
        public DictionaryList Source { get; set; }

        [DisplayName("Расстояние между подписью и наполняемой частью")]
        [Category("Макет")]
        public int Interval { get; set; }

        [DisplayName("Ширина подписи")]
        [Category("Макет")]
        public int WidthCaption { get; set; }

        [DisplayName("Имя элемента источника")]
        public string SourceControlName { get; set; }

        [TypeConverter(typeof(DictionaryCollection))]
        DictionaryCollection _items;
        [Browsable(false)]
        public DictionaryCollection Items 
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
            }
        }
        
        
        public ComboBox()
        {
            ComboWidth = 500;
            Interval = 6;
            WidthCaption = 100;
        }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "ComboBox_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "ComboBox {0}";
            }
        }

    }

    [DataContract]
    public class aetpComboBoxItem : IConvertible
    {
        [DataMember]
        [DisplayName("Ключ")]
        public string ID { get; set; }
        [DisplayName("Значение")]
        [DataMember]
        public string Name { get; set; }

        public TypeCode GetTypeCode()
		{
			return TypeCode.Object;
		}

        public override string ToString()
        {
            return Name;
        }

        bool IConvertible.ToBoolean(IFormatProvider provider)
        {
            return Convert.ToBoolean(GetDoubleValue());
        }

        double GetDoubleValue()
        {
            return Convert.ToDouble(ID);
        }

        byte IConvertible.ToByte(IFormatProvider provider)
        {
            return Convert.ToByte(GetDoubleValue());
        }

        char IConvertible.ToChar(IFormatProvider provider)
        {
            return Convert.ToChar(GetDoubleValue());
        }

        DateTime IConvertible.ToDateTime(IFormatProvider provider)
        {
            return Convert.ToDateTime(GetDoubleValue());
        }

        decimal IConvertible.ToDecimal(IFormatProvider provider)
        {
            return Convert.ToDecimal(GetDoubleValue());
        }

        double IConvertible.ToDouble(IFormatProvider provider)
        {
            return GetDoubleValue();
        }

        short IConvertible.ToInt16(IFormatProvider provider)
        {
            return Convert.ToInt16(GetDoubleValue());
        }

        int IConvertible.ToInt32(IFormatProvider provider)
        {
            return Convert.ToInt32(GetDoubleValue());
        }

        long IConvertible.ToInt64(IFormatProvider provider)
        {
            return Convert.ToInt64(GetDoubleValue());
        }

        sbyte IConvertible.ToSByte(IFormatProvider provider)
        {
            return Convert.ToSByte(GetDoubleValue());
        }

        float IConvertible.ToSingle(IFormatProvider provider)
        {
            return Convert.ToSingle(GetDoubleValue());
        }

        string IConvertible.ToString(IFormatProvider provider)
        {
            return Name;
        }

        object IConvertible.ToType(Type conversionType, IFormatProvider provider)
        {
            return Convert.ChangeType(GetDoubleValue(), conversionType);
        }

        ushort IConvertible.ToUInt16(IFormatProvider provider)
        {
            return Convert.ToUInt16(GetDoubleValue());
        }

        uint IConvertible.ToUInt32(IFormatProvider provider)
        {
            return Convert.ToUInt32(GetDoubleValue());
        }

        ulong IConvertible.ToUInt64(IFormatProvider provider)
        {
            return Convert.ToUInt64(GetDoubleValue());
        }
    }

    public class DictionaryTypeConverter : TypeConverter 
    {

        /// <summary>
        /// Возвращает значение, показывающее, поддерживает ли объект стандартный набор значений, 
        /// которые можно выбрать из списка.
        /// Если не установить принудительно значение в true, есть вероятность, 
        /// что обработчик не будет воспринимать ваш метод GetStandardValues и вы не увидите 
        /// ваш список значений для выбора
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// Этот метод можно исключить, но в перспективе он позволит использовать данный класс конвертора 
        /// для вызова из разных классов.
        /// Для этого достаточно добавить в секцию case имя другого класса, который должен содержать свойство 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private DictionaryCollection GetCollection(System.ComponentModel.ITypeDescriptorContext context)
        {
            DictionaryCollection collection = new DictionaryCollection();
            switch (context.Instance.GetType().Name)
            {
                case "ComboBox":
                    collection = ((ComboBox)context.Instance).Items;
                    break;
                case "GridCellItem":
                    collection = ((GridCellItem)context.Instance).Items;
                    break;
                case "GridAddedRowColumn":
                    collection = ((GridAddedRowColumn)context.Instance).Items;
                    break;
                default:
                    collection = ((ComboBox)context.Instance).Items;
                    break;
            }
            return collection;
        }

        /// <summary>
        /// Метод возвращает список значений из коллекции для отображения в выпадающем 
        /// списке значений PropertyGrid
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(GetCollection(context).ToList());
        }

        /// <summary>
        /// Метод проверяет, можно ли преобразовывать полученное значение свойства от пользователя 
        /// в нужный нам тип
        /// </summary>
        /// <param name="context"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Преобразование типа в строку для отображения значения в поле PropertyGrid
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public override object ConvertTo(ITypeDescriptorContext context,
            System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string) && value is DictionaryList)
            {
                DictionaryList item = (DictionaryList)value;
                return item.Name;
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        /// <summary>
        /// Проверка на возможность обратного преобразования строкового представления
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sourceType"></param>
        /// <returns></returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType.Equals(typeof(string)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Преобразование строкового представления 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext context,
            System.Globalization.CultureInfo culture, object value)
        {
            if (value.GetType() == typeof(string))
            {
                DictionaryList itemSelected = GetCollection(context).Count.Equals(0) ?
                    new DictionaryList() : GetCollection(context)[0];

                foreach (DictionaryList Item in GetCollection(context))
                {
                    string sCraftName = Item.Name;
                    if (sCraftName.Equals((string)value))
                    {
                        itemSelected = Item;
                    }
                }
                return itemSelected;
            }
            else
                return base.ConvertFrom(context, culture, value);
        }

        /// <summary>
        /// Возвращает значение, показывающее, является ли исчерпывающим списком коллекция стандартных значений, 
        /// возвращаемая методом.
        /// 
        /// false - данные можно вводить вручную
        /// true - только выбор из списка
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// Конструктор метода
        /// </summary>
        public DictionaryTypeConverter() { }

    }

    [DataContract]
    public class DictionaryCollection : IList<DictionaryList> 
    {
        [DataMember]
        List<DictionaryList> list;

        IList List 
        {
            get
            {
                return list;
            }
        }
        public DictionaryCollection(List<DictionaryList> range)
        {
            this.list = range;
        }

        public DictionaryCollection() 
        {
            list = new List<DictionaryList>();
        }

        #region Методы коллекции
        public void Add(DictionaryList value)
        {
            list.Add(value as DictionaryList);  
        }

        public void Clear()
        {
            list.Clear();
        }

        public bool Contains(DictionaryList value)
        {
            bool inList = false;
            for (int i = 0; i < Count; i++)
            {
                if (list[i] == value)
                {
                    inList = true;
                    break;
                }
            }
            return inList;
        }

        public int IndexOf(DictionaryList value)
        {
            int itemIndex = -1;
            for (int i = 0; i < Count; i++)
            {
                if (list[i] == value)
                {
                    itemIndex = i;
                    break;
                }
            }
            return itemIndex;
        }

        public void Insert(int index, DictionaryList value)
        {
            list.Insert(index, value);
        }

        public bool IsFixedSize
        {
            get
            {
                return true;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public bool Remove(DictionaryList value)
        {
            RemoveAt(IndexOf(value));
            return true;
        }

        public void RemoveAt(int index)
        {
            list.RemoveAt(index);
        }

        
        // ICollection Members

        public void CopyTo(DictionaryList[] array, int index)
        {
            list.CopyTo(array, index);
        }

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        public bool IsSynchronized
        {
            get
            {
                return false;
            }
        }

        // Return the current instance since the underlying store is not
        // publicly available.
        public object SyncRoot
        {
            get
            {
                return this;
            }
        }

        // IEnumerable Members
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)(new DictionaryEnum(list));
        }

        public IEnumerator<DictionaryList> GetEnumerator()
        {
            return new DictionaryEnum(list);
        }

        void Dispose() { }
        
        public DictionaryList this[int Index]
        {
            get { return (DictionaryList)this.list[Index]; }
            set { this.list[Index] = value; }
        }

        #endregion

                                                                                                                                                                                                                                                                                                                                                                                                                                                                    #region Реализация интерфейса ICustomTypeDescriptor
    public String GetClassName()
    {
        return TypeDescriptor.GetClassName(this, true);
    }

    public AttributeCollection GetAttributes()
    {
        return TypeDescriptor.GetAttributes(this, true);
    }

    public String GetComponentName()
    {
        return TypeDescriptor.GetComponentName(this, true);
    }

    public TypeConverter GetConverter()
    {
        return TypeDescriptor.GetConverter(this, true);
    }

    public EventDescriptor GetDefaultEvent()
    {
        return TypeDescriptor.GetDefaultEvent(this, true);
    }

    public PropertyDescriptor GetDefaultProperty()
    {
        return TypeDescriptor.GetDefaultProperty(this, true);
    }

    public object GetEditor(Type editorBaseType)
    {
        return TypeDescriptor.GetEditor(this, editorBaseType, true);
    }

    public EventDescriptorCollection GetEvents(System.Attribute[] attributes)
    {
        return TypeDescriptor.GetEvents(this, attributes, true);
    }

    public EventDescriptorCollection GetEvents()
    {
        return TypeDescriptor.GetEvents(this, true);
    }

    public object GetPropertyOwner(PropertyDescriptor pd)
    {
        return this;
    }

    public PropertyDescriptorCollection GetProperties(System.Attribute[] attributes)
    {
        return GetProperties();
    }

    public PropertyDescriptorCollection GetProperties()
    {
        PropertyDescriptorCollection pds = new PropertyDescriptorCollection(null);

        for (int i = 0; i < this.list.Count; i++)
        {
            DictionaryCollectionPropertyDescriptor pd = new DictionaryCollectionPropertyDescriptor(this, i);
            pds.Add(pd);
        }
        return pds;
    }
    #endregion
        }

    public class DictionaryEnum : IEnumerator<DictionaryList> 
    {
        List<DictionaryList> list;
        int position = -1;
        public DictionaryEnum() { }
        public DictionaryEnum(List<DictionaryList> list)
        {
            this.list = list;
        }
        public bool MoveNext()
        {
            position++;
            return (position < list.Count);
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public DictionaryList Current
        {
            get
            {
                try
                {
                    return list[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public void Dispose() { }
    }

    public class DictionaryCollectionPropertyDescriptor : PropertyDescriptor 
    {
        private DictionaryCollection collection = null;
        private int index = -1;

        public DictionaryCollectionPropertyDescriptor(DictionaryCollection coll, int idx) :
            base("#" + idx.ToString(), null)
        {
            this.collection = coll;
            this.index = idx;
        }

        public override AttributeCollection Attributes
        {
            get
            {
                return new AttributeCollection(null);
            }
        }

        public override bool CanResetValue(object component)
        {
            return true;
        }

        public override Type ComponentType
        {
            get
            {
                return this.collection.GetType();
            }
        }

        public override string DisplayName
        {
            get
            {
                DictionaryList Item = this.collection[index];
                return Item.Name;
            }
        }

        public override string Description
        {
            get
            {
                DictionaryList Item = this.collection[index];
                StringBuilder sb = new StringBuilder();
                sb.Append(Item.Name);
                sb.Append(", ");
                sb.Append(index + 1);

                return sb.ToString();
            }
        }

        public override object GetValue(object component)
        {
            return this.collection[index];
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override string Name
        {
            get { return "#" + index.ToString(); }
        }

        public override Type PropertyType
        {
            get { return this.collection[index].GetType(); }
        }

        public override void ResetValue(object component)
        {
        }

        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }

        public override void SetValue(object component, object value)
        {

        }
    }

    public class DictionaryCollectionConverter : ExpandableObjectConverter 
    {
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture,
            object value, Type destType)
        {
            if (destType == typeof(string) && value is DictionaryCollection)
            {
                return "(Страницы...)";
            }
            return base.ConvertTo(context, culture, value, destType);
        }
    }

    public class DictionaryConverter : ExpandableObjectConverter 
    {
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture,
            object value, Type destType)
        {
            if (destType == typeof(string) && value is DictionaryList)
            {
                DictionaryList jobtitle = (DictionaryList)value;
                return jobtitle.Name;
            }
            return base.ConvertTo(context, culture, value, destType);
        }
    }
}
