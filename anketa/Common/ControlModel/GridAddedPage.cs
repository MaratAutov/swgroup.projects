﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.ComponentModel;
using aetp.Common.ControlModel.TypeConverters;
using System.Text.RegularExpressions;
using DevExpress.XtraTreeList.Nodes;
using System.Data;
using aetp.Common.Control;
using aetp.Common.View;
using System.Threading;

namespace aetp.Common.ControlModel
{
    [DataContract]
    public class GridAddedPage : BaseGrid
    {
        static int _PageNo = 0;

        public event aetpGridAddedPageChangeEventHandler PageChanged;
        protected void RaisePageChanged(aetpGridAddedPageChange e)
        {
            if (PageChanged != null)
                PageChanged(this, e);
        }

        [Browsable(false)]
        public int PageIndex { get; set; }
        public const string pageName = "aetpGridAddedPage";

        [Browsable(false)]
        public override bool IsMandatoryToFill { get; set; }

        private GridAddedPageColumnCollection _columns;

        [DataMember]
        [Editor(typeof(GridAddedPageColumnEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Проектирование")]
        [DisplayName("Колонки")]
        public GridAddedPageColumnCollection Columns {
            get
            {
                return _columns;
            }
            set
            {
                _columns = value;
            }
        }
                
        public override BaseControl Root
        {
            get
            {
                return base.Root;
            }
            set
            {
                base.Root = value;
                if (Children.Count > 0)
                    Children[0].Root = value;
            }
        }

        [DataMember]        
        
        public Page Page { get; set; }
        [DisplayName("PagePrefix")]
        [Browsable(false)]
        public string PagePrefix
        {
            get
            {
                if (this.Parent is Page)
                    return (this.Parent as Page).ParentPrefix;
                return null;
            }
        }

        public List<BaseControl> Pages
        {
            get
            {
                return Children.Skip(1).ToList();
            }
        }

        public GridAddedPage()
        {
            Columns = new GridAddedPageColumnCollection(this);
            WidthCaption = 100;
            WidthGrid = 480;
            Interval = 6;
            HeightGrid = 200;
            Meta_Table = false;
            //добавляем страницу контейнер
            GridPage childPage = new GridPage();
            Page = childPage;
            this.AddChildren(childPage, null);
            HeightHeaderGrid = 22;
        }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Table_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "Table {0}";
            }
        }

        public override object Clone()
        {
            Page newPage = (Page)this.Children[0].Clone();
            GridAddedPage newGrid = (GridAddedPage)this.MemberwiseClone();
            newGrid.Children = new List<BaseControl>();
            newGrid.Children.Add(newPage);            
            
            newGrid.Columns = new GridAddedPageColumnCollection(newGrid);
            foreach (var column in (this.Columns))
            {
                GridAddedPageColumn newColumn = (GridAddedPageColumn)column.Clone();
                newColumn.Grid = newGrid;
                newGrid.Columns.Add(newColumn);
            }
            return newGrid;
        }

        void AddHandler(Page page)
        {
            ReportUtils.DoAction(page.Children, 
                ctrl => {
                    if (ReportUtils.IsNameInColumn(page.Parent as GridAddedPage, ctrl.Name))
                    {
                        ctrl.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(se_PropertyChanged);
                        ctrl.PerfomEditValueChanged();
                    }
                }); 
        }

        void se_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is aetp.Common.ControlModel.BaseControl && e.PropertyName == "EditValue")
            {
                var editor = sender as aetp.Common.ControlModel.BaseControl;
                System.Data.DataRow dr = editor.Parent.Tag as DataRow;
                if (dr != null)
                {
                    dr[Utils.Utils.GetFieldName(editor.Name)] = Convert.ToString(editor.EditValue);
                }
            }
        }

        public void AddChildrenPage(BaseControl control, BaseControl rootNode, int focused_row = -1)
        {
            control.Root = rootNode;
            List<BaseControl> list = new List<BaseControl> { };
            if ((focused_row > -1) && (focused_row != Data.Rows.Count))
            {
                list.Add(Children[0]);
                for (int i = 1; i < Children.Count; i++)
                {
                    if (i != focused_row + 1)
                    {

                        list.Add(Children[i]);
                    }
                    else
                    {

                        list.Add(Children[i]);
                        list.Add(control);
                    }
                }
                Children.Clear();
                Children = list;
            }
            else
            {
                Children.Add(control);
            }
            RaiseAetpControlsEventHandler(control);
        }

        public void NewPage(int focused_row = -1)
        {
            Data.AcceptChanges();
            Page page = (Page)(this.Children[0] as Page).Clone();

            Interlocked.Increment(ref _PageNo);
            PageIndex = _PageNo;
            page.Index = PageIndex;
            page.Name = pageName + PagePrefix + "$" + PageIndex.ToString();
            page.ParentPrefix += "$" + PageIndex.ToString();
            ReportUtils.RenameChildren(page.Children, PagePrefix + "$" + PageIndex.ToString(), "$" + PageIndex.ToString(), PageIndex);
            AddChildrenPage(page, Root, focused_row);
            DataRow dr = Data.NewRow();
            Data.Rows.Add(dr);

           // Data.AcceptChanges();
            


            setTagProperty(page, dr);


            
            AddHandler(page);
            RaisePageChanged(new aetpGridAddedPageChange() { changeType = ChangeType.Add, page = page });

            var regex = new Regex(@"{(?<word>\w+)}");

            foreach (Match match in regex.Matches(page.Caption))
            {
                if (match.Groups["word"].Success)
                {
                    foreach (ControlModel.BaseControl ctrl in page.Children)
                    {
                        if (ctrl.Name == match.Groups["word"].Value + "$" + page.Index.ToString())
                        {
                            ctrl.PropertyChanged += new PropertyChangedEventHandler(ctrl_PropertyChanged);
                        }
                    }
                }
            }
            SetTitlePage(page as GridPage);

            

            if ((focused_row > -1) && (focused_row != Data.Rows.Count - 1))
            {
                //BaseControl children_temp = Children[Children.Count - 1];
                //for (int i = focused_row + 2; i < Children.Count; i++)
                //{
                //    BaseControl children_current = (Page)(this.Children[i] as Page).Clone();
                //    Children[i] = (Page)(children_temp as Page).Clone();
                //    children_temp = (Page)(children_current as Page).Clone();
                //}

                DataRow dr_temp = Data.Rows[Data.Rows.Count - 1];

                for (int i = focused_row + 1; i < Data.Rows.Count; i++)
                {
                    DataRow dr_current = Data.NewRow();

                    foreach (DataColumn dc in Data.Columns)
                    {
                        dr_current[dc] = Data.Rows[i][dc];
                    }

                    foreach (DataColumn dc in Data.Columns)
                    {
                        Data.Rows[i][dc] = dr_temp[dc];
                    }

                    foreach (DataColumn dc in Data.Columns)
                    {
                        dr_temp[dc] = dr_current[dc];
                    }
                    setTagProperty(Children[i+1] as Page, Data.Rows[i]);
                }
                Data.AcceptChanges();


            }
        }        

        void setTagProperty(Page page, DataRow dr)
        {
            page.Tag = dr;
            ReportUtils.DoAction(page.Children, (x) => { if (x is Page) { x.Tag = dr; } });
        }

        void ctrl_PropertyChanged(object sender, EventArgs e)
        {
            SetTitlePage((sender as ControlModel.BaseControl).Parent as GridPage);
        }

        void SetTitlePage(GridPage page)
        {
            string caption = (page.Parent as ControlModel.GridAddedPage).Page.Caption;
            foreach (ControlModel.BaseControl ctrl in page.Children)
            {
                string value = (ctrl.EditValue ?? "").ToString();
                string name = ctrl.Name.Substring(0, ctrl.Name.IndexOf("$"));
                caption = (caption.Replace("{" + name + "}", value));      
            }

            page.Caption = caption;
            TreeListNode node = page.tlItem;
            //при десериализации дерево еще не построено
            if (node != null)
                node.SetValue("Caption", caption);
        }

        public override void ClearProperty()
        {
            base.ClearProperty();

        }
    }

    public class GridPage : Page
    {
        [Browsable(false)]
        public override BaseControl ListControl
        {
            get;
            set;
        }

        [Browsable(false)]
        public override TypeControl type
        {
            get;
            set;
        }

        [Browsable(false)]
        public override bool Visible_Page
        {
            get;
            set;
        }

        [Browsable(false)]
        public override bool CanDrags
        {
            get
            {
                return false;
            }
            set
            {
                
            }
        }

        public void Dispose()
        {
            ReportUtils.DoAction(this.Children, x =>
            {
                if (x is MetaConrol)
                {
                    var meta = x as MetaConrol;
                    if (meta.ListControl != null && meta.ListControl.Tag is List<MetaConrol>)
                    {
                        (meta.ListControl.Tag as List<MetaConrol>).Remove(meta);
                    }
                }
            });
        }
    }

    [DataContract]
    public class GridAddedPageColumnCollection : Collection<GridAddedPageColumn>
    {
        [DataMember]
        public GridAddedPage Grid { get; set; }       
       

        public GridAddedPageColumnCollection(GridAddedPage grid)
        {
            this.Grid = grid;
        }

        protected override void InsertItem(int index, GridAddedPageColumn item)
        {
            item.Grid = Grid;
            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, GridAddedPageColumn item)
        {
            item.Grid = Grid;
            base.SetItem(index, item);
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
        }
    }

    public class GridAddedPageColumnEditor : GridCollectionEditor
    {

        public GridAddedPageColumnEditor(Type type)
            : base(type)
        {

        }

        protected override Type CreateCollectionItemType()
        {
            return typeof(GridAddedPageColumn);
        }

        protected override object CreateInstance(Type itemType)
        {
            return base.CreateInstance(itemType);
        }

        protected override void DestroyInstance(object instance)
        {
            base.DestroyInstance(instance);
        }

        protected override void ValidateEditValue(CollectionForm collectionForm)
        {
            if (okayButtons.ContainsKey(collectionForm))
            {
                Button okayButton = okayButtons[collectionForm];
                GridAddedPageColumnCollection items = collectionForm.EditValue as GridAddedPageColumnCollection;
                bool ok = true;
                for (int i = 0; i < items.Count; i++)
                {
                    if (String.IsNullOrEmpty(items[i].Name))
                    {
                        ok = false;
                        break;
                    }
                }
                okayButton.Enabled = ok;
            }
        }

        public override void collectionForm_Load(object sender, EventArgs e)
        {
            collectionForm.Text = "Таблица с добавлением листов";
            ValidateEditValue((CollectionForm)sender);
        }
        
    }

    [DataContract]
    public class GridAddedPageColumn : BaseControl
    {
        int _widthcolumn;

        [Browsable(false)]
        public override bool IsMandatoryToFill { get; set; }

        [DataMember]
        [Browsable(false)]
        public GridAddedPage Grid { get; set; }
        [DataMember]
        [DisplayName("Имя элемента управления источника данных")]
        [Category("Проектирование")]
        [Description("Имя элемента управления из которого будет браться значение для подстановки в ячейки данной колонки")]
        public string FieldName { get; set; }

        string _name;
        [DataMember]
        [Category("Проектирование")]
        [DisplayName("Имя")]
        public override string Name
        {
            get
            {
                if (String.IsNullOrEmpty(_name))
                    _name = String.Format(TemplateName, new_element_index);
                return _name;
            }
            set
            {
                if (Grid != null)
                    nameValidator.Validate(Grid.Columns.ToList<BaseControl>(), value, this);
                _name = value;
            }
        }

        [DataMember]
        [DisplayName("Ширина столбца")]
        [Category("Проектирование")]
        [DefaultValue(100)]
       
        public int WidthColumn
        {
            get
            {
                return _widthcolumn;
            }
            set
            {
                if (Convert.ToInt32(value) > 0)
                    _widthcolumn = value;
                else _widthcolumn = 100;
            }
        }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Column_{0}";
            }
        }
    }   

}
