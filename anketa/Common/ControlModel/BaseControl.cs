﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using aetp.Common.Mediator;
using aetp.Common.Validator;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.Validator;
using DevExpress.XtraTreeList.Nodes;
using aetp.Common.ControlModel.Attributes;
using aetp.Common.ControlModel.TypeConverters;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Globalization;
using System.Drawing;

namespace aetp.Common.ControlModel
{
    
    [DataContract, KnownType(typeof(Report))]
    public class BaseControl : FilterablePropertyBase, ICloneable, IDXDataErrorInfo, INotifyPropertyChanged
    {       

        public virtual bool CanContains(Type t)
        {
            return false;
        }
        
        // для запрета драг и дропа
        [Browsable(false)]
        public virtual bool CanDrags
        {
            get
            {
                return true;
            }
            set
            {
                
            }
        }

        public virtual void ClearProperty()
        {
            EditValue = null;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public void PerfomEditValueChanged()
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("EditValue"));
            }
        }
      
        object _editValue;
        [DisplayName("EditValue")]  
        [Browsable (true)]
        public virtual object EditValue 
        {
            get
            {
                return _editValue;
            }
            set
            {
                // даты нужно сравнивать через Compare иначе равенства не будет
                if (value is DateTime && _editValue is DateTime &&
                    DateTime.Compare((DateTime)value, (DateTime)_editValue) == 0) return;

                if (_editValue != value)
                {
                    _editValue = value;                    
                    NotifyPropertyChanged("EditValue");
                }                
            }
        }
       
        
        bool isCleaningOnUnvisible = false;
        [DisplayName("Очищать значение при скрытии")]
        [DataMember]
        public bool IsCleaningOnUnvisible
        {
            get
            {
                return isCleaningOnUnvisible;
            }
            set
            {
                isCleaningOnUnvisible = value;
            }
        }
                

        StringAlignment printingAlignment = StringAlignment.Near;
        [DisplayName("Выравнивание")]
        [Browsable(false)]
        [DataMember]
        [Category("Печать")]
        public StringAlignment PrintingAlignment
        {
            get
            {
                return printingAlignment;
            }
            set
            {
                printingAlignment = value;
            }
        }

        bool _isMandatoryToFill = false;
        [DisplayName("Обязательно для заполнения")]
        [DataMember]
        [Category("Проверка")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        //является ли обязательным для заполнения
        public virtual bool IsMandatoryToFill 
        {
            get
            {
                return _isMandatoryToFill;
            }
            set
            {
                _isMandatoryToFill = value;
                RaiseIsMandatoryToFillChanged();
            }
        }

        public event EventHandler IsMandatoryToFillChanged;
        protected void RaiseIsMandatoryToFillChanged()
        {
            if (IsMandatoryToFillChanged != null)
                IsMandatoryToFillChanged(this, null);
        }

        public static int new_element_index = 1;
        [Browsable(false)]
        protected virtual string TemplateName
        {
            get
            {
                return "Element_{0}";
            }
        }

        [Browsable(false)]
        protected virtual string TemplateCaption
        {
            get
            {
                return "Element {0}";
            }
        }

        public string Class
        {
            get
            {
                return this.GetType().ToString();
            }
        }

        [XmlIgnore]
        [Browsable(false)]
        [DataMember]
        public BaseControl Parent { get; set; }
        [Browsable(false)]
        // что бы получить именно страницу выше в дереве элементов
        public Page ParentPage 
        {
            get
            {
                if (this.Parent == null)
                    return null;
                
                BaseControl parent = this.Parent;
                while (true)
                {
                    if (parent is Page)
                        return parent as Page;
                    parent = parent.Parent;
                }

            }
        }
        
        [Browsable(false)]
        public Page RootPage
        {
            get
            {
                if (Root == null)
                    return this as Page;
                
                BaseControl parent = this.Parent;
                while (true)
                {
                    if (parent == null)
                        return Root as Page;
                    if (parent is GridPage && (parent as GridPage).Parent is GridAddedPage)
                        return parent as Page;
                    parent = parent.Parent;
                }
            }
        }

        [XmlIgnore]
        [Browsable(false)]
        [DataMember]
        public virtual BaseControl Root { get; set; }
        [Browsable(false)]
        [DataMember]
        public List<BaseControl> Children { get; set; }
        
        [Browsable(false)]
        [XmlIgnore]
        public string Description
        {
            get
            {                
                return String.Format("{0} - {1}", Name, Caption);
            }
        }

        [Browsable(false)]
        [XmlIgnore]
        [DataMember]
        public object Tag { get; set; }
        
        [Browsable(false)]
        [XmlIgnore]
        public TreeListNode tlItem { get; set; }
                        
        string _name;
        
        #region Validators
        NameValidator _nameValidator;
        protected NameValidator nameValidator
        {
            get
            {
                if (_nameValidator == null)
                    _nameValidator = new NameValidator();
                return _nameValidator;
            }
        }
        
        #endregion

        [DisplayName("Имя")]
        [DataMember]
        [Category("Проектирование")]
        public virtual string Name 
        {
            get
            {
                if (String.IsNullOrEmpty(_name))
                    _name = String.Format(TemplateName, new_element_index);
                return _name;
            }
            set
            {
                if (nameValidator != null)
                nameValidator.Validate(Root, value, this);
                _name = value;
            }
        }

        string _serializingTag;
        [DisplayName("Имя тэга")]
        [Category("Сериализация")]
        [DataMember]
        public string SerializingTag
        {
            get
            {
                if (String.IsNullOrEmpty(_serializingTag))
                    return Name;
                else
                    return _serializingTag;
            }
            set
            {
                _serializingTag = value;
            }
        }

        public virtual string Text
        {
            get
            {
                return (EditValue ?? "").ToString();
            }
        }

        string _alternativeSerializingTag;
        [DisplayName("Имя альтернативного тэга")]
        [Category("Сериализация")]
        [DataMember]
        public string AlternativeSerializingTag
        {
            get
            {
                if (String.IsNullOrEmpty(_alternativeSerializingTag))
                    return "";
                else
                    return _alternativeSerializingTag;
            }
            set
            {
                _alternativeSerializingTag = value;
            }
        }

        string _caption = null;


        [DisplayName("Подпись")]
        [DataMember]
        [Category("Проектирование")]
        public virtual string Caption
        {
            get
            {
                if ((_caption == null) || (_caption == ""))
                    _caption = String.Format(TemplateCaption, new_element_index);
                return _caption;
            }
            set
            {
                _caption = value;
            }
        }

        
        #region null object
        [XmlIgnore]
        public static NullAetpControl Null = new NullAetpControl();
        
        public class NullAetpControl : BaseControl
        {
            public override void AddChildren(BaseControl control, BaseControl rootNode)
            {
                
            }

            public override void Remove()
            {
                
            }
        }

        #endregion
        
        public BaseControl()
        {
            Children = new List<BaseControl>();
            new_element_index++;
            IsMandatoryToFill = true;            
            EditorValidators = new List<IValidationItem>();

            PropertyDescriptor descriptor = TypeDescriptor.GetProperties(this.GetType())["EditValue"];
            BrowsableAttribute attribute = (BrowsableAttribute)
                                          descriptor.Attributes[typeof(BrowsableAttribute)];
            FieldInfo fieldToChange = attribute.GetType().GetField("browsable",
                                             System.Reflection.BindingFlags.NonPublic |
                                             System.Reflection.BindingFlags.Instance);
            fieldToChange.SetValue(attribute, true);
        }

        public BaseControl(bool IsReportChild)
        {
            Children = new List<BaseControl>();
        }

        public override string ToString()
        {
            return String.Format("{0} - {1}", Name, Caption);
        }

        #region public methods

        public virtual void Remove()
        {
            RaiseControlRemovingEventHandler(this);
            if (Parent != null)
                Parent.Children.Remove(this);
        }

        public void Move(BaseControl parent, int index)
        {
            this.Parent.Children.Remove(this);
            this.Parent = parent;
            if (index > parent.Children.Count)
                parent.Children.Insert(parent.Children.Count, this);
            else
                parent.Children.Insert(index, this);
            
        }

        public virtual void AddChildren(BaseControl control, BaseControl rootNode)
        {
            control.Root = rootNode;
            control.Parent = this;
            Children.Add(control);
            RaiseAetpControlsEventHandler(control);
        }

        #endregion

        #region events

        public event aetpControlsEventHandler ControlAdded;
        protected void RaiseAetpControlsEventHandler(BaseControl control)
        {
            if (ControlAdded != null)
                ControlAdded(this, new aetpControlsEventHandlerArgs(control));
        }

        public event aetpControlsEventHandler ControlRemoving;
        protected void RaiseControlRemovingEventHandler(BaseControl control)
        {
            if (ControlRemoving != null)
                ControlRemoving(this, new aetpControlsEventHandlerArgs(control));
        }

        #endregion

        #region IClonamble

        public virtual object Clone(BaseControl parent)
        {
            this.Parent = parent;
            return Clone();
        }

        public virtual object Clone()
        {           
            BaseControl obj = (BaseControl)this.MemberwiseClone();
            obj.EditorValidators = new List<IValidationItem>();
            foreach (var v in this.EditorValidators)
            {
                obj.EditorValidators.Add((IValidationItem)v.Clone());
            }
            obj.PerfomEditValueChanged();
            return obj;
        }

        

        #endregion

        [Browsable(false)]
        [DataMember]
        public List<IValidationItem> EditorValidators { get; set; }
        //для вызова из елементов у которых свойство EditValue не связано с элементом управления
        public void GetPropertyError(string propertyName, ErrorInfo info, object EditValue)
        {
            if (propertyName == "EditValue")
            {
                string error;
                foreach (IValidationItem item in EditorValidators)
                {
                    error = item.Execute(EditValue);
                    if (error != null)
                    {
                        info.ErrorText = error;
                        if (item.Editable)
                            info.ErrorType = ErrorType.Information;
                        return;
                    }
                }
            }
        }
        // Implements the IDXDataErrorInfo.GetPropertyError method.
        public void GetPropertyError(string propertyName, ErrorInfo info)
        {
            if (propertyName == "EditValue")
            {
                string error;
                foreach (IValidationItem item in EditorValidators)
                {
                    error = item.Execute(EditValue);
                    if (error != null)
                    {
                        info.ErrorText = error;
                        if (item.Editable)
                            info.ErrorType = ErrorType.Information;
                        return;
                    }
                }
            }
        }
        // IDXDataErrorInfo.GetError method
        public void GetError(ErrorInfo info) { }
    }

    public class aetpControlsEventHandlerArgs
    {
        public BaseControl control;

        public aetpControlsEventHandlerArgs(BaseControl control)
        {
            this.control = control;
        }
    }

    public delegate void aetpControlsEventHandler(object sender, aetpControlsEventHandlerArgs args);
}
