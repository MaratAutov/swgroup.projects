﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace aetp.Common.ControlModel.Validator
{
    public class NameValidator
    {
        public void Validate(BaseControl root, string name, BaseControl control)
        {
            if (root != null && (root as Report).RunMode == ViewType.Constructor)
            {
                ValidateCorrectName(name);
                var n = Utils.Find(root, name, true, control);
                if (n != null)
                    throw new Exception("Элемент с данным именем уже существует!");
                else RenamePrintFormElement(control.Name, root, name);
            }
        }

        private void RenameElement(PrintItem item, string old_name, string new_name)
        {
            if ((item.ElementName != null) && (item.ElementName == old_name))
            {
                item.ElementName = new_name;
            }
        }


        private void RenameTemplate(PrintItem item, string old_name, string new_name)
        {
            if (item.Values != null)
            {
                for (int i = 0; i < item.Values.Length; i++)
                {
                    if (item.Values[i] == old_name)
                    {
                        item.Values[i] = new_name;
                    }
                }
            }
        }

        private void RenameTable(PrintItem item, string old_name, string new_name)
        {
            foreach (PrintTableItem item_table in item.PrintItems)
            {
                if ((item_table.TableItemType == PrintTableItemType.Element) && (item_table.ElementName != null) && (item_table.ElementName == old_name))
                {
                    item_table.ElementName = new_name;
                }
                else if ((item_table.TableItemType == PrintTableItemType.Template) && (item_table.Values != null))
                {
                    for (int i = 0; i < item_table.Values.Length; i++)
                    {
                        if (item_table.Values[i] == old_name)
                        {
                            item_table.Values[i] = new_name;
                        }
                    }                    
                }
            }
        }

        private void RenameTableCollection(PrintItem item, string old_name, string new_name)
        {
            foreach (PrintTableCollectionItem table_item in item.PrintTableCollectionItems)
            {
                if ((table_item.TableName != null) && (table_item.TableName == old_name))
                {
                    table_item.TableName = new_name;
                }
                if ((table_item.TableNameAddedRow != null) && (table_item.TableNameAddedRow == old_name))
                {
                    table_item.TableNameAddedRow = new_name;
                }

                foreach (PrintTableCollectionItemRow row in table_item.Rows)
                {
                    foreach (PrintTableCollectionItemCell cell in row.Cells)
                    {
                        if ((cell.Value != null) && (cell.Value == "$" + old_name))
                        {
                            cell.Value = "$" + new_name;
                            
                        }
                        if (cell.ValuesTemplate != null)
                        {
                            for (int i = 0; i < cell.ValuesTemplate.Length; i++)
                            {
                                if (cell.ValuesTemplate[i] == "$" + old_name)
                                {
                                    cell.ValuesTemplate[i] = "$" + new_name;
                                }
                            }
                        }
                    }
                }
            }
        }


        private void RenamePrintFormElement(string old_name, BaseControl root,string new_name)
        {
            if ((root as Report).PrintingForm != null)
            {
                foreach (var item in (root as Report).PrintingForm.PrintItems)
                {
                    if (item.ItemType == PrintItemType.Element)
                    {
                        RenameElement(item, old_name, new_name);
                    }
                    else if (item.ItemType == PrintItemType.Template)
                    {
                        RenameTemplate(item, old_name, new_name);
                    }
                    else if (item.ItemType == PrintItemType.Table)
                    {
                        RenameTable(item, old_name, new_name);
                    }
                    else if (item.ItemType == PrintItemType.TableCollection)
                    {
                        RenameTableCollection(item, old_name, new_name);
                    }
                    else if (item.ItemType == PrintItemType.CollectionItem)
                    {
                        foreach (PrintItem collection_item in item.CollectionItems)
                        {
                            if (collection_item.ItemType == PrintItemType.Element)
                            {
                                RenameElement(collection_item, "$" + old_name, "$" + new_name);
                            }
                            else if (collection_item.ItemType == PrintItemType.Template)
                            {
                                RenameTemplate(collection_item, "$" + old_name, "$" + new_name);
                            }
                            else if (collection_item.ItemType == PrintItemType.Table)
                            {
                                RenameTable(collection_item, "$" + old_name, "$" + new_name);
                            }
                            else if (collection_item.ItemType == PrintItemType.TableCollection)
                            {
                                RenameTableCollection(collection_item, old_name, new_name);
                            }
                        }
                    }
                }
            }
        }

        BaseControl Find(IList<BaseControl> list, string name, BaseControl control = null)
        {
            for (int i = 0; i < list.Count(); i++)
            {
                if (list[i] == control)
                    continue;
                if (list[i].Name == name)
                    return list[i];
            }
            return null;
        }

        public void Validate(IList<BaseControl> list, string name, BaseControl control)
        {
            // отказываемся от валидации если структура Report не правильная
            if (control.Root == null)
                return;
            if ((control.Root as Report).RunMode == ViewType.Constructor)
            {
                ValidateCorrectName(name);
                foreach (BaseControl ctrl in list)
                {
                    var n = Find(list, name, control);
                    if (n != null)
                        throw new Exception("Элемент с данным именем уже существует!");
                }
            }
        }

        void ValidateCorrectName(string name)
        {
            Regex rgx = new Regex(@"^[a-zA-Zа-яА-Я]{1}[a-zA-Zа-яА-Я0-9_]*$", RegexOptions.IgnoreCase);
            if (!rgx.IsMatch(name))
                throw new Exception("Невозможно присвоить данное значение!\nИмя должно начинаться с буквы\nи состоять из букв, цифр, сиволов -,_");
        }
    }
}
