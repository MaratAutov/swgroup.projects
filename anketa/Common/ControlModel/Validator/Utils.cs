﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aetp.Common.ControlModel.Validator
{
    public static class Utils
    {
        public static BaseControl Find(BaseControl parentControl, string name, bool IsChildrenFind, BaseControl currentControl = null)
        {
            if (parentControl == null)
                return null;
            foreach (BaseControl control in parentControl.Children)
            {
                BaseControl res = Eq(control, name, IsChildrenFind, currentControl);
                if (res == null)
                    continue;
                else
                    return res;
            }
            if (parentControl is MetaGrid)
            {
                foreach (BaseGrid grid in (parentControl as MetaGrid).Tables)
                {
                    BaseControl res = Eq(grid, name, IsChildrenFind, currentControl);
                    if (res == null)
                        continue;
                    else
                        return res;
                }
            }
            return null;
        }

        static BaseControl Eq(BaseControl control, string name, bool IsChildrenFind, BaseControl currentControl = null)
        {
            if (control == currentControl)
                return null;
            if (control.Name == name)
                return control;
            if ((control.Children.Count > 0 || control is MetaGrid) && IsChildrenFind )
                return Find(control, name, IsChildrenFind, currentControl);
            return null;
        }
    }
}
