﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace aetp.Common.ControlModel.Validator
{
    public class CellValidator
    {
        Report Report;
        List<TableItem> list = new List<TableItem> { };
        public List<aetp.Common.ControlModel.BaseControl> controls = new List<ControlModel.BaseControl>();

        public void GetChildren(Page p)
        {
            if (p == null)
                return;
            foreach (aetp.Common.ControlModel.BaseControl ctrl in p.Children)
            {
                if (ctrl is Page) GetChildren(ctrl as Page);
                if (ctrl is MetaConrol) GetChildrenMetaControl(ctrl as MetaConrol);
                controls.Add(ctrl);
            }
        }

        public void GetChildrenMetaControl(MetaConrol p)
        {
            foreach (aetp.Common.ControlModel.BaseControl ctrl in p.Children)
            {
                if (ctrl is Page) GetChildren(ctrl as Page);
                if (ctrl is MetaConrol)
                    GetChildrenMetaControl(ctrl as MetaConrol);
                controls.Add(ctrl);
            }
        }


        public CellValidator(Report report)
        {
            Report = report;
        }

        public string Operation(Report report)
        {
            controls.Clear();
            GetChildren(report);

            list.Clear();
            for (int i = controls.Count - 1; i >= 0; i--)
            {
                if (controls[i] is Grid)
                {
                    GetGridCell(controls[i] as Grid);
                }

                if (controls[i] is MetaGrid)
                {
                    TableCollection collection = (controls[i] as MetaGrid).Tables;

                    foreach (BaseGrid grid in collection)
                    {
                        if (grid is Grid)
                        {
                            GetMetaGridCell(grid as Grid, controls[i] as MetaGrid);
                        }
                        if (grid is GridAddedRow)
                        {
                            GetMetaGridAddedRowCell(grid as GridAddedRow, controls[i] as MetaGrid);
                        }
                    }
                }
            }

            string equals = string.Empty;

            for (int j = 0; j < list.Count; j++)
            {
                
                    for (int k = 0; k < list.Count; k++)
                    {
                        if ((list[j].Name == list[k].Name) && (list[j].ParentName == list[k].ParentName) && (list[j].GridControl == "Grid") && (list[k].GridControl == "Grid") && (j != k) && (!list[k].MetaTable))
                        {
                            equals += list[j].ToString();
                        }

                        if ((list[j].Name == list[k].Name) && (list[j].MetaTableName == list[k].MetaTableName) && (j != k) && (list[k].MetaTable))
                        {
                            equals += list[j].ToString();
                        }
                    }                
            }

            return equals;            
        }


        public void GetGridCell(Grid grid)
        {
            foreach (GridRow row in grid.Rows)
            {
                for (int j = 0; j < row.Cells.Count; j++)
                {
                    list.Add(new TableItem(row.Cells[j].Name, grid.Name, "ячейка","Grid",false,""));
                }
            }
        }

        public void GetMetaGridCell(Grid grid,MetaGrid parent)
        {
            GridColumnCollection items = grid.Columns;
            for (int j = 0; j < items.Count; j++)
            {
                list.Add(new TableItem(items[j].Name, grid.Name, "колонка", "Grid",true,parent.Name));
            }

            GridRowCollection rows = grid.Rows;
            for (int j = 0; j < rows.Count; j++)
            {
                list.Add(new TableItem(rows[j].Name, grid.Name, "строка", "Grid", true, parent.Name));
            }

            foreach (GridRow row in grid.Rows)
            {
                for (int j = 0; j < row.Cells.Count; j++)
                {
                    list.Add(new TableItem(row.Cells[j].Name, grid.Name, "ячейка", "Grid", true, parent.Name));
                }
            }
        }

        public void GetMetaGridAddedRowCell(GridAddedRow grid, MetaGrid parent)
        {
            GridAddedRowColumnCollection items = grid.Columns;

            for (int j = 0; j < items.Count; j++)
            {
                list.Add(new TableItem(items[j].Name, grid.Name, "колонка", "GridAddedRow", true, parent.Name));
            }
        }
    }

    public class TableItem
    {
        public TableItem(string name, string parent_name, string item,string grid_control,bool metatable,string metatTableName)
        {
            _name = name;
            _parentName = parent_name;
            _item = item;
            _grid_control = grid_control;
            _metatable = metatable;
            _meta_table_name = metatTableName;
        }

        private string _name = string.Empty;
        private string _parentName = string.Empty;
        private string _item = string.Empty;
        private string _grid_control= string.Empty;
        private bool _metatable;
        private string _meta_table_name;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string ParentName
        {
            get
            {
                return _parentName;
            }
        }

        public string Item
        {
            get
            {
                return _item;
            }
        }

        public string GridControl
        {
            get
            {
                return _grid_control;
            }
        }

        public bool MetaTable
        {
            get
            {
                return _metatable;
            }
        }

        public string MetaTableName
        {
            get
            {
                return _meta_table_name;
            }
        }


        public override string ToString()
        {
            if (MetaTableName.Length > 0)
            return "МетаТаблица " + MetaTableName + ": Таблица: " + ParentName + " - " + Item + ":" + Name + "\r\n";
            else return "Таблица: " + ParentName + " - " + Item + ": " + Name + "\r\n";

        }
    }
}
