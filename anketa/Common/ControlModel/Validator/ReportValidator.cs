﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aetp.Common.ControlModel.Validator
{
    public class ReportValidator
    {
        Report Report;

        public ReportValidator(Report report)
        {
            Report = report;
        }

        public void Validate()
        {
            if (Report.RunMode == ViewType.Constructor)
            {
                ValidateOnRequiredProperty(Report);
            }
        }

        void ValidateOnRequiredProperty(BaseControl parentControl)
        {
            foreach (BaseControl control in parentControl.Children)
            {
                if (String.IsNullOrEmpty(control.Name))
                    throw new Exception("Не всем элементам присвоены имена");
                if (control.Children.Count > 0)
                    ValidateOnRequiredProperty(control);

            }
        }
        
    }
}
