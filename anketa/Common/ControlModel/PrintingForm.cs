﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using aetp.Common.ControlModel.TypeConverters;
using System.Drawing;
using aetp.Common.ControlModel.Attributes;
using System.Collections.ObjectModel;
using aetp.Common.ControlModel.UITypeEditors;
using System.Drawing.Design;

namespace aetp.Common.ControlModel
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    [DataContract]
    public class PrintingForm : FilterablePropertyBase
    {
        public Report report;

        public PrintingForm()
        {

        }
        
        public PrintingType _printingType = PrintingType.All;
        [DisplayName("Вид печати")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [DataMember]

        

        public PrintingType PrintingType
        {
            get
            {
                return _printingType;
            }
            set
            {
                _printingType = value;
            }
        }

        PrintItemCollection _printItems = new PrintItemCollection();
        [DynamicPropertyFilter("PrintingType", "Template")]
        [DisplayName("Печатные элементы")]
        [DataMember]
        public PrintItemCollection PrintItems
        {
            get
            {
                foreach(PrintItem item in _printItems)
                {
                    item.report = this.report;
                }
                //_printItems.report = this.report;
                return _printItems;
            }
            set
            {
                _printItems = value;
            }
        }

        public PrintingFont _font = PrintingFont.TNR;
        [DynamicPropertyFilter("PrintingType", "Template")]
        [DisplayName("Шрифт для названия печатной формы")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [DataMember]
        public PrintingFont Font
        {
            get
            {
                return _font;
            }
            set
            {
                _font = value;
            }
        }

        [DisplayName("Выводить заголовок в колонтитулах печатной формы")]
        [DataMember]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool ShowHeader { get; set; }

        [DisplayName("Выводить заголовок печатной формы")]
        [DataMember]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool ShowTitle { get; set; }

    }

    [DataContract]
    public class PrintItemCollection : Collection<PrintItem>
    {
        public Report report;
        public PrintItemCollection()
        {

        }
        protected override void InsertItem(int index, PrintItem item)
        {            
            base.InsertItem(index, item);
            
        }

        protected override void SetItem(int index, PrintItem item)
        {
            base.SetItem(index, item);
        }
    }


    [DataContract]
    public class PrintTableItemCollection : Collection<PrintTableItem>
    {
        public PrintTableItemCollection()
        {

        }
        protected override void InsertItem(int index, PrintTableItem item)
        {
            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, PrintTableItem item)
        {
            base.SetItem(index, item);
        }
    }

    [DataContract]
    public class PrintTableCollection : Collection<PrintTableCollectionItem>
    {
        public PrintTableCollection()
        {

        }
        protected override void InsertItem(int index, PrintTableCollectionItem item)
        {
            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, PrintTableCollectionItem item)
        {
            base.SetItem(index, item);
        }
    }

    [DataContract]
    public class PrintTableItemColumnCollection : Collection<PrintTableCollectionItemColumn>
    {
        public PrintTableItemColumnCollection()
        {

        }
        protected override void InsertItem(int index, PrintTableCollectionItemColumn item)
        {
            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, PrintTableCollectionItemColumn item)
        {
            base.SetItem(index, item);
        }
    }

    [DataContract]
    public class PrintTableItemRowCollection : Collection<PrintTableCollectionItemRow>
    {
        public Report report;
        public PrintTableItemRowCollection()
        {

        }
        protected override void InsertItem(int index, PrintTableCollectionItemRow item)
        {
            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, PrintTableCollectionItemRow item)
        {
            base.SetItem(index, item);
        }       
    }

    [DataContract]
    public class PrintTableItemCellCollection : Collection<PrintTableCollectionItemCell>
    {
        public PrintTableItemCellCollection()
        {

        }
        protected override void InsertItem(int index, PrintTableCollectionItemCell item)
        {
            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, PrintTableCollectionItemCell item)
        {
            base.SetItem(index, item);
        }
    }

    [DataContract]
    public class PrintItem : FilterablePropertyBase
    {
        public Report report;

        [DisplayName("Background")]
        [Browsable(false)]
        [DataMember]
        [Category("Appearance")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsBackGround { get; set; }


        [DynamicPropertyFilter("ItemType", "Template")]
        [DisplayName("Шаблон строки")]
        [Category("Макет")]
        [DataMember]
        public string TextTemplate { get; set; }

        [DynamicPropertyFilter("ItemType", "Template")]
        [DisplayName("Значения")]
        [Category("Макет")]
        [DataMember]
        public string[] Values { get; set; }

        PrintTableItemCollection _printItems = new PrintTableItemCollection();
        [DynamicPropertyFilter("ItemType", "Table")]
        [DisplayName("Элементы таблицы")]
        [Category("Макет")]
        [DataMember]

        public PrintTableItemCollection PrintItems
        {
            get
            {
                return _printItems;
            }
            set
            {
                _printItems = value;
            }
        }

        [DynamicPropertyFilter("ItemType", "Table")]
        [DisplayName("Ширина 1-го столбца")]
        [Category("Appearance")]
        [DataMember]
        public int WidthFirstColumn { get; set; }

        [DynamicPropertyFilter("ItemType", "Table")]
        [DisplayName("Ширина 2-го столбца")]
        [Category("Appearance")]
        [DataMember]
        public int WidthLastColumn { get; set; }


        PrintTableCollection _printTableCollectionItems = new PrintTableCollection();
        [DynamicPropertyFilter("ItemType", "TableCollection")]
        [DisplayName("Таблицы")]
        [Category("Макет")]
        [DataMember]
        public PrintTableCollection PrintTableCollectionItems
        {
            get
            {
                if (_printTableCollectionItems == null)
                {
                    _printTableCollectionItems = new PrintTableCollection();
                }
                foreach (PrintTableCollectionItem item in _printTableCollectionItems)
                {
                    item.report = report;
                }
                return _printTableCollectionItems;
            }
            set
            {
                _printTableCollectionItems = value;
            }
        }

        PrintItemCollection _collectionItems = new PrintItemCollection();
        [DynamicPropertyFilter("ItemType", "CollectionItem")]
        [DisplayName("Элементы")]
        [Category("Макет")]
        [DataMember]
        public PrintItemCollection CollectionItems
        {
            get
            {
                if (_collectionItems == null)
                    _collectionItems = new PrintItemCollection();
                return _collectionItems;

            }
            set
            {
                _collectionItems = value;
            }
        }


        string _tableName = "";
        [DynamicPropertyFilter("ItemType", "CollectionItem")]
        [DisplayName("Таблица")]
        [Category("Макет")]
        [DataMember]
        public string TableName
        {
            get
            {
                if (_tableName == null)
                    _tableName = "";
                return _tableName;

            }
            set
            {
                _tableName = value;
            }
        }


        bool _isItalic = false;
        [DisplayName("Курсив")]
        [DynamicPropertyFilter("ItemType", "Element,Template")]
        [DataMember]
        [Category("Appearance")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public virtual bool IsItalic
        {
            get
            {
                return _isItalic;
            }
            set
            {
                _isItalic = value;
            }
        }

        bool _isLandscape = false;
        [DisplayName("Landscape")]        
        [DataMember]
        [Category("Appearance")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsLandscape
        {
            get
            {
                return _isLandscape;
            }
            set
            {
                _isLandscape = value;
            }
        }


        bool _isVisible = false;
        [DisplayName("Invisible")]
        [DataMember]
        [Category("Видимость")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsInVisible 
        {
            get
            {
                return _isVisible;
            }
            set
            {
                _isVisible = value;
            }
        }

        bool _isNoBorder = false;
        [DynamicPropertyFilter("ItemType", "Table")]
        [DisplayName("Не печатать рамки")]
        [DataMember]
        [Category("Макет")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsNoBorder
        {
            get
            {
                return _isNoBorder;
            }
            set
            {
                _isNoBorder = value;
            }
        }

        [DynamicPropertyFilter("IsInVisible", "True")]
        [DisplayName("Имя управляющего элемента")]
        [Category("Видимость")]
        [DataMember]
        public string VisibilityElementName { get; set; }

        [DynamicPropertyFilter("ItemType", "Element")]
        [DisplayName("Имя элемента")]
        [Category("Макет")]
        [DataMember]
        public string ElementName { get; set; }

        [DynamicPropertyFilter("ItemType", "Element,Template")]
        [DisplayName("Выравнивание")]
        [Category("Appearance")]
        [DataMember]
        public StringAlignment Alignment { get; set; }

        [DynamicPropertyFilter("ItemType", "Element,Template")]
        [DisplayName("Вертикальное выравнивание(для таблиц)")]
        [Category("Appearance")]
        [DataMember]
        public VerticalAlignment VAlignment { get; set; }



        [DynamicPropertyFilter("ItemType", "Element,Template")]
        [DisplayName("Размер текста")]
        [Category("Appearance")]
        [DataMember]
        public float TextSize { get; set; }

        
        public PrintingFont _font = PrintingFont.TNR;
        [DynamicPropertyFilter("ItemType", "Element,Template")]
        [DisplayName("Шрифт")]
        [Category("Appearance")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [DataMember]
        public PrintingFont Font
        {
            get
            {
                return _font;
            }
            set
            {
                _font = value;
            }
        }

        [DisplayName("Тип элемента")]
        [Category("Макет")]
        [DataMember]
        public PrintItemType ItemType { get; set; }

        public PrintItem()
        {
            TextSize = 8;
            IsInVisible = false;
            WidthFirstColumn = 300;
            WidthLastColumn = 300;
        }

        public override string ToString()
        {
            if (ItemType == PrintItemType.Element)
            {
                return ElementName;
            }
            else if (ItemType == PrintItemType.Template)
            {
                if (String.IsNullOrEmpty(TextTemplate))
                    return "ЭлементПечати";
                string name = TextTemplate;
                if (Values != null)
                    for (int i = 0; i < Values.Length; i++)
                    {
                        name = name.Replace("{" + i.ToString() + "}", "{" + Values[i] + "}");
                    }
                return name;
            }
            else if (ItemType == PrintItemType.Table)
            {
                return "Таблица";
            }
            else if (ItemType == PrintItemType.CollectionItem)
            {
                return "Коллекция элементов";
            }
            else
            {
                return "Коллекция таблиц";
            }
        }
    }

    [DataContract]
    public class PrintTableItem : FilterablePropertyBase
    {
        [DynamicPropertyFilter("TableItemType", "Template")]
        [DisplayName("Шаблон строки")]
        [Category("Макет")]
        [DataMember]
        public string TextTemplate { get; set; }

        [DynamicPropertyFilter("TableItemType", "Template")]
        [DisplayName("Заголовок")]
        [Category("Макет")]
        [DataMember]
        public string TemplateHeader { get; set; }

        [DynamicPropertyFilter("TableItemType", "Template")]
        [DisplayName("Значения")]
        [Category("Макет")]
        [DataMember]
        public string[] Values { get; set; }

        PrintTableItemCollection _printItems = new PrintTableItemCollection();
        [DynamicPropertyFilter("TableItemType", "Table")]
        [DisplayName("Элементы таблицы")]
        [Category("Макет")]
        [DataMember]
        public PrintTableItemCollection PrintItems
        {
            get
            {
                return _printItems;
            }
            set
            {
                _printItems = value;
            }
        }

        [DynamicPropertyFilter("TableItemType", "Element,Cell")]
        [DisplayName("Имя элемента")]
        [Category("Макет")]
        [DataMember]
        public string ElementName { get; set; }

        [DisplayName("Горизонтальное выравнивание")]
        [Category("Appearance")]
        [DataMember]
        public StringAlignment Alignment { get; set; }
        
        [DisplayName("Вертикальное выравнивание(для таблиц)")]
        [Category("Appearance")]
        [DataMember]
        public VerticalAlignment VAlignment { get; set; }


        [DisplayName("Background")]
        [DataMember]
        [Category("Appearance")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsBackGround { get; set; }


        [DisplayName("Курсив")]
        [DataMember]
        [Category("Appearance")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsItalic { get; set; }

        [DisplayName("Размер текста")]
        [Category("Appearance")]
        [DataMember]
        public float TextSize { get; set; }

        public PrintingFont _font = PrintingFont.TNR;        
        [DisplayName("Шрифт")]
        [Category("Appearance")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [DataMember]
        public PrintingFont Font
        {
            get
            {
                return _font;
            }
            set
            {
                _font = value;
            }
        }

        [DisplayName("Тип элемента")]
        [Category("Макет")]
        [DataMember]
        public PrintTableItemType TableItemType { get; set; }

        public PrintTableItem()
        {
            TextSize = 8;
        }

        public override string ToString()
        {
            if (TableItemType == PrintTableItemType.Element)
            {
                return ElementName;
            }
            else
            {
                if (String.IsNullOrEmpty(TextTemplate))
                    return "ЭлементПечати";
                string name = TextTemplate;
                if (Values != null)
                    for (int i = 0; i < Values.Length; i++)
                    {
                        name = name.Replace("{" + i.ToString() + "}", "{" + Values[i] + "}");
                    }
                return name;
            }
        }
    }


    [DataContract]
    public class PrintTableCollectionItem : FilterablePropertyBase
    {
        public Report report;

        [DisplayName("Таблица с добавлением листов")]
        [DataMember]
        public bool IsTableAddedPage
        {
            get;
            set;
        }

        [DisplayName("Таблица с добавлением строк")]
        [DataMember]
        public bool IsTableAddedRow
        {
            get;
            set;
        }

        [DisplayName("Hide Vertical/Horizontal Lines")]
        [DataMember]
        public bool IsHideLines
        {
            get;
            set;
        }

        [DataMember]
        [Browsable(false)]
        public BaseControl ctrl { get; set; }


        [DynamicPropertyFilter("IsTableAddedPage", "True")]
        [DisplayName("Имя таблицы с добавлением листов")]
        [DataMember]
        public string TableName { get; set; }

        [DynamicPropertyFilter("IsTableAddedRow", "True")]
        [DisplayName("Имя таблицы с добавлением строк")]
        [DataMember]
        public string TableNameAddedRow { get; set; }

        
        [DisplayName("Имя таблицы")]
        [DataMember]
        public string TableCaption { get; set; }


        PrintTableItemColumnCollection _printItemsColumns = new PrintTableItemColumnCollection();
        [DisplayName("Столбцы")]
        [DataMember]
        public PrintTableItemColumnCollection Columns
        {
            get
            {
                return _printItemsColumns;
            }
            set
            {
                _printItemsColumns = value;
            }
        }

        PrintTableItemRowCollection _printItemsRows = new PrintTableItemRowCollection();
        [Editor(typeof(PrintTableCollectionValueEditor), typeof(UITypeEditor))]
        [DisplayName("Строки")]
        [DataMember]

        public PrintTableItemRowCollection Rows
        {
            get
            {
                foreach (PrintTableCollectionItemRow row in _printItemsRows)
                {
                    row.report = report;
                }
                _printItemsRows.report = report;
                return _printItemsRows;
            }
            set
            {
                //_printItemsRows.report = report;
                _printItemsRows = value;
            }
        }

        public PrintTableCollectionItem()
        {

        }

        public override string ToString()
        {
            return TableCaption;
        }
    }

    [DataContract]
    public class PrintTableCollectionItemColumn : FilterablePropertyBase
    {
        bool _isVisible = false;
        [DisplayName("Invisible")]
        [DataMember]
        [Category("Видимость")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsInVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                _isVisible = value;
            }
        }

        [DisplayName("Ширина")]
        [DataMember]
        public int Width { get; set; }

        [DisplayName("Объединять строки с одинаковыми значениями")]
        [DataMember]
        public bool IsMerge { get; set; }

        public PrintTableCollectionItemColumn()
        {

        }

        public override string ToString()
        {
            return "Колонка";
        }
    }

    [DataContract]
    public class PrintTableCollectionItemRow : FilterablePropertyBase
    {
        public Report report;

        [DisplayName("ЗначенияЯчеек")]
        [DataMember]
        [Browsable(false)]
        public string[] ValuesCells { get; set; }               

        public PrintTableCollectionItemRow()
        {
        }

        [DisplayName("Объединять столбцы с одинаковыми значениями")]
        [DataMember]
        public bool IsMerge { get; set; }

        public override string ToString()
        {
            return "Строка";
        }

        bool _isVisible = false;
        [DisplayName("Invisible")]
        [DataMember]
        [Category("Видимость")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsInVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                _isVisible = value;
            }
        }

        [DynamicPropertyFilter("IsInVisible", "True")]
        [DisplayName("Имя управляющего элемента")]
        [Category("Видимость")]
        [DataMember]
        public string VisibilityElementName { get; set; }


        [DisplayName("Управляющий элемент")]
        [DataMember]
        [Browsable(false)]
        public BaseControl VisibilityElement { get; set; }


        PrintTableItemCellCollection _printItemsCell = new PrintTableItemCellCollection();
        [DisplayName("Ячейки")]
        [DataMember]
        public PrintTableItemCellCollection Cells
        {
            get
            {
                if (_printItemsCell != null)
                    return _printItemsCell;
                else return _printItemsCell = new PrintTableItemCellCollection();
            }
            set
            {
                _printItemsCell = value;
            }
        }
    }


    [DataContract]
    public class PrintTableCollectionItemCell : FilterablePropertyBase
    {
        [DynamicPropertyFilter("IsTemplate", "True")]
        [DisplayName("Шаблон строки")]
        [Category("Макет")]
        [DataMember]
        public string TextTemplate { get; set; }

        [DynamicPropertyFilter("IsTemplate", "True")]
        [DisplayName("Значения")]
        [Category("Макет")]
        [DataMember]
        public string[] ValuesTemplate { get; set; }

        [DisplayName("Шаблон")]
        [DataMember]
        [Category("Макет")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsTemplate { get; set; }

        [DynamicPropertyFilter("IsTemplate", "False")]
        [DisplayName("Значение")]
        [DataMember]
        [Category("Макет")]
        public string Value { get; set; }        


        [DisplayName("Модель")]
        [DataMember]
        [Browsable(false)]
        public BaseControl[] ctrl { get; set; }

        [DisplayName("Элемент")]
        [DataMember]
        [Browsable(false)]
        public System.Windows.Forms.Control[] xuc { get; set; }

        [DisplayName("HAlignment")]
        [Category("Appearance")]
        [DataMember]
        public StringAlignment Alignment { get; set; }

        [DisplayName("VAlignment")]
        [Category("Appearance")]
        [DataMember]
        public VerticalAlignment VAlignment { get; set; }


        [DisplayName("Курсив")]
        [DataMember]
        [Category("Appearance")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsItalic { get; set; }

        [DisplayName("Bold")]
        [DataMember]
        [Category("Appearance")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsBold { get; set; }

        [DisplayName("UnderLine")]
        [DataMember]
        [Category("Appearance")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsUnderLine { get; set; }


        [DisplayName("Background")]
        [DataMember]
        [Category("Appearance")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool IsBackGround { get; set; }

        [DisplayName("Размер текста")]
        [Category("Appearance")]
        [DataMember]
        public float TextSize { get; set; }

        public PrintingFont _font = PrintingFont.TNR;
        [DisplayName("Шрифт")]
        [Category("Appearance")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [DataMember]
        public PrintingFont Font
        {
            get
            {
                return _font;
            }
            set
            {
                _font = value;
            }
        }

        public PrintTableCollectionItemCell()
        {
            TextSize = 8;
        }

        public override string ToString()
        {
            if (!IsTemplate)
            {
                return Value;
            }
            else
            {
                if (String.IsNullOrEmpty(TextTemplate))
                    return "Шаблон";
                string name = TextTemplate;
                if (ValuesTemplate != null)
                    for (int i = 0; i < ValuesTemplate.Length; i++)
                    {
                        name = name.Replace("{" + i.ToString() + "}", "{" + ValuesTemplate[i] + "}");
                    }
                return name;
            }
        }
    }


    public enum PrintItemType
    {
        [Description("Элемент")]
        Element = 0,
        [Description("Шаблон")]
        Template,
        [Description("Таблица")]
        Table,
        [Description("Коллекция таблиц")]
        TableCollection,
        [Description("Коллекция элементов")]
        CollectionItem
    }


    public enum PrintTableItemType
    {
        [Description("Элемент")]
        Element = 0,
        [Description("Шаблон")]
        Template        
    }

    public enum PrintingType
    {
        [Description("Печать всех элементов")]
        All = 0,
        [Description("Печать по шаблону")]
        Template = 1
    }

    public enum PrintingFont
    {
        [Description("Times New Roman")]
        TNR = 0,
        [Description("Courier New")]
        Courier = 1,
        [Description("Arial")]
        Arial = 2
    }

    
    public enum VerticalAlignment
    {
        Center = 0,        
        Top = 1,        
        Bottom = 2,
    }
}
