﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.Dynamic;

namespace aetp.Common.ControlModel
{
    public delegate void AddListHandler(object sender, MetaDataListAddedEventArgs e);

    [DataContract]
    public class ReportMetaData 
    {
#region dictionary

        private List<DictionaryList> _dictionarys = new List<DictionaryList>();

        [DataMember]
        public List<DictionaryList> Dictionarys
        {
            get
            {
                return _dictionarys;
            }
            set
            {
                _dictionarys = value;
            }
        }

        public DictionaryList GetDictionaryByName(string name)
        {
            return (from d in Dictionarys where d.Name == name select d).FirstOrDefault();
        }

        public void RemoveList(DictionaryList item)
        {
            Dictionarys.Remove(item);
        }

        public event AddListHandler ListAdded;
        protected void RaiseListAdded(DictionaryList item)
        {
            if (ListAdded != null)
                ListAdded(this, new MetaDataListAddedEventArgs() { newItem = item });
        }

        public void AddList(DictionaryList item)
        {
           item.MetaData = this;           
           Dictionarys.Add(item);
            RaiseListAdded(item);
        }

#endregion
    }

    [DataContract]
    public class DictionaryListItemCollection : Collection<DictionaryListItem>
    {
        public int index = 1;
        protected override void InsertItem(int index, DictionaryListItem item)
        {
            base.InsertItem(index, item);
        }
        protected override void SetItem(int index, DictionaryListItem item)
        {
            base.SetItem(index, item);
        }
        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
        }
    }

    class DictionaryListItemEqualityComparer : IEqualityComparer<DictionaryListItem>
    {

        public bool Equals(DictionaryListItem dli1, DictionaryListItem dli2)
        {
            if (dli1.Code == dli2.Code)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public int GetHashCode(DictionaryListItem dli)
        {
            return dli.Code.GetHashCode();
        }

    }

    public class DictionaryListEditor : GridCollectionEditor
    {
        public DictionaryListEditor(Type type)
            : base(type)
        {
            
        }

        protected override Type CreateCollectionItemType()
        {
            return typeof(DictionaryListItem);
        }

        protected override object CreateInstance(Type itemType)
        {
            
            return new DictionaryListItem() { Code = ((collectionForm.EditValue as DictionaryListItemCollection).index++).ToString() };
            
        }

        protected override void ValidateEditValue(CollectionForm collectionForm)
        {
            if (okayButtons.ContainsKey(collectionForm))
            {
                Button okayButton = okayButtons[collectionForm];
                DictionaryListItemCollection items = collectionForm.EditValue as DictionaryListItemCollection;
                okayButton.Enabled = items.Distinct(new DictionaryListItemEqualityComparer()).Count() == items.Count;
            }
        }

        public override void collectionForm_Load(object sender, EventArgs e)
        {
            collectionForm.Text = "Словарь";
            ValidateEditValue((CollectionForm)sender);
        }
    }

    [DataContract]
    public class DictionaryList : BaseControl//, ICollection<DictionaryListItem>
    {
        [Browsable(false)]
        public override bool IsMandatoryToFill { get; set; }
        [Browsable(false)]
        [DataMember]
        public ReportMetaData MetaData { get; set; }
        [DisplayName("Элементы")]
        [Category("Проектирование")]
        [DataMember]
        [Editor(typeof(DictionaryListEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public DictionaryListItemCollection Items { get; set; }


        string _caption = null;
        
        [DisplayName("Подпись")]
        [DataMember]
        [Category("Проектирование")]

        public override string Caption
        {
            get
            {
                if ((_caption == null) || (_caption == ""))
                    _caption = String.Format(TemplateCaption, new_element_index);
                return _caption;
            }
            set
            {
                _caption = value;
            }
        }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Dictionary_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "Dictionary {0}";
            }
        }


        public DictionaryList(ReportMetaData MetaData)
        {
            this.MetaData = MetaData;
        }

        public DictionaryList()
        {
            Items = new DictionaryListItemCollection();
        }

        public void Add(DictionaryListItem item)
        {
            Items.Add(item);
        }

        public void Clear()
        {
            Items.Clear();
        }

        public bool Contains(DictionaryListItem item)
        {
            return Items.Contains(item);
        }

        public void CopyTo(DictionaryListItem[] array, int arrayIndex)
        {
            Items.CopyTo(array, arrayIndex);
        }
        [Browsable(false)]
        public int Count
        {
            get { return Items.Count; }
        }
        [Browsable(false)]
        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(DictionaryListItem item)
        {
            return Items.Remove(item);
        }

        public event AddListHandler ListRemoving;
        protected void RaiseListRemoving(DictionaryList item)
        {
            if (ListRemoving != null)
                ListRemoving(this, new MetaDataListAddedEventArgs() { newItem = item });
        }

        public override void Remove()
        {
            RaiseListRemoving(this);
            MetaData.RemoveList(this);
        }

        public IEnumerator<DictionaryListItem> GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
    
    public class DictionaryListItem //: IConvertible
    {
        public static string Code_tag = "Код"; 
        [DisplayName("Код")]
       
        public string Code { get; set; }
        public static string Description_tag = "Описание";
        [DisplayName("Описание")]
        
        public string Description { get; set; }
        public static string Active_tag = "Активно"; 
        [DisplayName("Активно")]
       
        public bool Active { get; set; }

        public dynamic test { get; set; }
       
        public override string ToString()
        {
            Active = true;
            return  Description;
        }
    }

    public class MetaDataListAddedEventArgs
    {
        public Type type { get; set; }
        public DictionaryList newItem { get; set; }
    }

    public class DynamicDictionary : DynamicObject
    {
        Dictionary<string, object> dictionary
            = new Dictionary<string, object>();
                
        public override bool TryGetMember(
            GetMemberBinder binder, out object result)
        {
            string name = binder.Name.ToLower();
            return dictionary.TryGetValue(name, out result);
        }

        public override bool TrySetMember(
            SetMemberBinder binder, object value)
        {
            dictionary[binder.Name.ToLower()] = value;
            return true;
        }
    }
}
