﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using aetp.Common.ControlModel.TypeConverters;
using aetp.Common.Validator;
using aetp.Common.ControlModel.Attributes;
using System.Drawing.Design;
using aetp.Common.ControlModel.UITypeEditors;
using System.Runtime.Serialization;

namespace aetp.Common.ControlModel
{
    public class SimpleEdit : BaseControl
    {
        public virtual event EventHandler Visible_Changed;
        protected void RaiseVisible_Changed()
        {
            if (Visible_Changed != null)
            {
                Visible_Changed(this, null);
            }
        }

        private int _widthControl;
        private int _heigthControl;

        [Browsable(false)]
        public int Index { get; set; }
        //[Browsable(false)]
        //public string Text { get; set; }
        SimpleDataType _dataType;
        [DisplayName("Тип")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [Category("Проектирование")]
        //[ReadOnly(true)]
        public SimpleDataType DataType 
        {
            get
            {
                return _dataType;
            }
            set
            {
                _dataType = value;
                DefaultValue = null;
            }
        }
        [Browsable(false)]
        public override string Text
        {
            get
            {
                if (EditValue == null) return "";
                if (DataType == SimpleDataType.Bool)
                {
                    
                    bool res;
                    if (bool.TryParse(EditValue.ToString(), out res))
                    {
                        if (res) return "Да"; else return "Нет";
                    }
                    return "";
                }
                else
                {
                    return EditValue.ToString();
                }
            }
        }

        [DynamicPropertyFilter("DataType", "Bool")]
        [DisplayName("Тип сериализации")]
        [Category("Сериализация")]
        [TypeConverter(typeof(EnumTypeConverter))]
        public BoolSerializtionType boolSerializationType { get; set; }
        
        [DynamicPropertyFilter("DataType", "String")]
        [DisplayName("Число символов")]
        [Category("Проверка")]
        public int MaxLength { get; set; }

        [DynamicPropertyFilter("DataType", "Double")]
        [DisplayName("Число знаков после запятой")]
        [Category("Проверка")]
        public int MaxFloatLength { get; set; }

        [DynamicPropertyFilter("DataType", "Integer,Double")]
        [DisplayName("Максимальное значение")]
        [Category("Проверка")]
        public Int64 MaxValue { get; set; }

        [DynamicPropertyFilter("DataType", "Integer,Double")]
        [DisplayName("Минимальное значение")]
        [Category("Проверка")]
        public int MinValue { get; set; } 

        [DynamicPropertyFilter("DataType", "String")]
        [DisplayName("Маска")]
        [Category("Проверка")]
        public MaskItem Mask { get; set; }

      
        [DisplayName("Значение по умолчанию")]
        [Editor(typeof(EditValueEditor), typeof(UITypeEditor))]
        [DefaultValue(null)]
        [DataMember]
        public object DefaultValue { get; set; }



        [DynamicPropertyFilter("DataType", "String,Double,Integer,DateTime")]
        [DisplayName("Ширина")]
        [Category("Макет")]        
        public int WidthControl
        {
            get
            {
                return _widthControl;
            }
            set
            {
                if (value > 20)
                    _widthControl = value;
                else _widthControl = 500;
            }
        }

        [DisplayName("Имя элемента источника")]
        public string SourceControlName { get; set; }

        [Browsable(false)]
        [DisplayName("Высота")]
        [Category("Макет")]        
        public int HeigthControl
        {
            get
            {
                return _heigthControl;
            }
            set
            {
                if (Convert.ToInt32(value) > 20)
                    _heigthControl = value;
                else _heigthControl = 20;
            }
        }
        

        [DisplayName("Расстояние между подписью и наполняемой частью")]
        [Category("Макет")]
        public int Interval { get; set; }

        [DisplayName("Ширина подписи")]
        [Category("Макет")]
        public int WidthCaption { get; set; }


        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "SimpleEdit_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "SimpleEdit {0}";
            }
        }

        public SimpleEdit()
        {
            Mask = new MaskItem();
            WidthControl = 500;
            HeigthControl = 20;
            Interval = 6;
            WidthCaption = 100;
        }
    }

    public enum SimpleDataType
    {
        [Description("Текстовое поле")]
        String = 0,
        [Description("Дата")]
        DateTime,
        [Description("Число с плавающей точкой")]
        Double,
        [Description("Целое число")]
        Integer,
        [Description("Чекбокс")]
        Bool
    }

    public enum WidthChange
    {
        [Description("Да")]
        yes= 1,
        [Description("Нет")]
        no = 0      
    }


    public enum BoolSerializtionType
    {
        [Description("Да/Нет")]
        yesno = 0,      
        [Description("Булевое")]
        boolean = 1       
    }
}
