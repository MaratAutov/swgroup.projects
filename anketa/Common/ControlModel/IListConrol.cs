﻿using System;
namespace aetp.Common.ControlModel
{
    public interface IListConrol
    {
        BaseControl ListControl { get; set; }
        void CloneListControl(IListConrol newControl);
    }
}
