﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.Control;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using aetp.Common.Validator;
using System.Reflection;

namespace aetp.Common.ControlModel.UITypeEditors
{
    public partial class PrintTableCollectionEditForm : Form
    {
        PrintTableCollectionItem model = new PrintTableCollectionItem();

        public PrintTableItemRowCollection EditValue
        {
            get
            {
                return model.Rows;
            }
            set
            {
                model.Rows = ((value as PrintTableItemRowCollection) as PrintTableItemRowCollection);
                model.report = ((value as PrintTableItemRowCollection) as PrintTableItemRowCollection).report;
            }
        }

        public PrintTableCollectionEditForm(PrintTableItemRowCollection EditValue)
        {
            InitializeComponent();            
            this.EditValue = EditValue;
        }

        private void lv_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
            {
                pg.SelectedObject = (model as PrintTableCollectionItem).Rows[lv.SelectedItems[0].Index];
                lb_property.Text = "Свойства " + lv.SelectedItems[0].Text.ToString() + " :";
            }
            else if (lv.Items.Count == 0)
            {
                lb_property.Text = "Свойства :";
            }
        }


        private void Refresh_List()
        {
            lv.Items.Clear();
            if ((model as PrintTableCollectionItem).Rows.Count > 0)
            {
                foreach (PrintTableCollectionItemRow row in (model as PrintTableCollectionItem).Rows)
                {
                    lv.Items.Add("Строка");
                }
                lv.Items[0].Selected = true;
            }
        }

        

        private void MetaGridEditForm_Load(object sender, EventArgs e)
        { 
            lv.Columns[0].Text = " ";

            Refresh_List();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
            {
                model.Rows.RemoveAt(lv.SelectedItems[0].Index);
                lv.Items[lv.SelectedItems[0].Index].Remove();              
                Refresh_List();
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
            {
                if (lv.SelectedItems[0].Index > 0)
                {
                    int index = lv.SelectedItems[0].Index - 1;
                    PrintTableCollectionItemRow newTable = (PrintTableCollectionItemRow)(model as PrintTableCollectionItem).Rows[lv.SelectedItems[0].Index];
                    (model as PrintTableCollectionItem).Rows[lv.SelectedItems[0].Index] = (model as PrintTableCollectionItem).Rows[lv.SelectedItems[0].Index - 1];
                    (model as PrintTableCollectionItem).Rows[lv.SelectedItems[0].Index - 1] = newTable;

                    Refresh_List();
                    lv.Items[index].Selected = true;
                }
                
            }
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
            {
                if (lv.SelectedItems[0].Index != lv.Items.Count - 1)
                {
                    var printTable_model = model as PrintTableCollectionItem;
                    int index = lv.SelectedItems[0].Index + 1;
                    PrintTableCollectionItemRow newTable = (PrintTableCollectionItemRow)printTable_model.Rows[lv.SelectedItems[0].Index];
                    printTable_model.Rows[lv.SelectedItems[0].Index] = printTable_model.Rows[lv.SelectedItems[0].Index + 1];
                    printTable_model.Rows[lv.SelectedItems[0].Index + 1] = newTable;

                    Refresh_List();
                    lv.Items[index].Selected = true;
                }
               
            }
        }        

        private void btn_add_Click(object sender, EventArgs e)
        {
            PrintTableCollectionItemRow item = new PrintTableCollectionItemRow();

            if (model.Rows != null)
            {
                model.Rows.Add(item);
                lv.Items.Add(new ListViewItem("Строка"));
                lv.Focus();
                lv.Items[lv.Items.Count - 1].Selected = true;
                lv.Items[lv.SelectedItems[0].Index].EnsureVisible();
            } 
        }


        
        private void btn_import_Click(object sender, EventArgs e)
        { 
            if (model.report != null)
            {
                TableNamePrintForm name = new TableNamePrintForm();
                SelectTableNamePrintForm form = new SelectTableNamePrintForm(name);
                form.ShowDialog();
                Grid grid = null;

                
                if (name.TableName.Length > 0)
                {
                    GetGridTable(model.report, ref grid, name.TableName);
                }

                if (grid != null)
                {
                    foreach (GridRow row in grid.Rows)
                    {
                        PrintTableCollectionItemRow item = new PrintTableCollectionItemRow();
                        item.Cells = new PrintTableItemCellCollection();
                        foreach (GridCellItem cell in row.Cells)
                        {
                            PrintTableCollectionItemCell item_cell = new PrintTableCollectionItemCell();
                            item_cell.Alignment = StringAlignment.Near;
                            item_cell.VAlignment = VerticalAlignment.Center;
                            item_cell.TextSize = 11;
                            if (cell.Type == GridCellType.Label)
                            {
                                item_cell.Value = cell.Caption;
                            }
                            else
                            {
                                item_cell.Value = "$" + cell.Name + ":" + name.TableName;
                            }
                            item.Cells.Add(item_cell);
                        }
                        if (model.Rows != null)
                        {
                            model.Rows.Add(item);
                            lv.Items.Add(new ListViewItem("Строка"));
                            lv.Focus();
                            lv.Items[lv.Items.Count - 1].Selected = true;
                            lv.Items[lv.SelectedItems[0].Index].EnsureVisible();
                        } 
                    }
                }
            }
        }


        private void GetGridTable(Page page,ref Grid grid, string table_name)
        {
            //Grid grid = grid1;
            foreach (BaseControl ctrl in page.Children)
            {
                if ((ctrl.Name == table_name) && (ctrl is Grid))
                {
                    grid = (ctrl as Grid);
                }

                if (ctrl is Page)
                {
                    GetGridTable(ctrl as Page, ref grid, table_name);
                }
            }

            
        }
    }

    public class TableNamePrintForm
    {
        private string _tableName;

        public string TableName
        {
            get
            {
                return _tableName;
            }
            set
            {
                _tableName = value;
            }
        }

        public TableNamePrintForm()
        {

        }
    }
}
