﻿namespace aetp.Common.ControlModel.UITypeEditors
{
    partial class editValueSelectForm
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(editValueSelectForm));
            this.cbeTypes = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelEditValue = new DevExpress.XtraEditors.PanelControl();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.cbeTypes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelEditValue)).BeginInit();
            this.SuspendLayout();
            // 
            // cbeTypes
            // 
            this.cbeTypes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbeTypes.EditValue = "Null";
            this.cbeTypes.Location = new System.Drawing.Point(16, 13);
            this.cbeTypes.Name = "cbeTypes";
            this.cbeTypes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeTypes.Properties.Items.AddRange(new object[] {
            "Null",
            "String",
            "Int",
            "Double",
            "Boolean",
            "DateTime",
            "Реквизит организации"});
            this.cbeTypes.Size = new System.Drawing.Size(188, 20);
            this.cbeTypes.TabIndex = 0;
            this.cbeTypes.SelectedValueChanged += new System.EventHandler(this.cbeTypes_SelectedValueChanged);
            // 
            // panelEditValue
            // 
            this.panelEditValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelEditValue.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelEditValue.Location = new System.Drawing.Point(16, 40);
            this.panelEditValue.Name = "panelEditValue";
            this.panelEditValue.Size = new System.Drawing.Size(188, 39);
            this.panelEditValue.TabIndex = 1;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(73, 93);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "OK";
            // 
            // editValueSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 123);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.panelEditValue);
            this.Controls.Add(this.cbeTypes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "editValueSelectForm";
            ((System.ComponentModel.ISupportInitialize)(this.cbeTypes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelEditValue)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit cbeTypes;
        private DevExpress.XtraEditors.PanelControl panelEditValue;
        private DevExpress.XtraEditors.SimpleButton btnOk;
    }
}
