﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using aetp.Common.ControlModel.UITypeEditors;

namespace aetp.Common.ControlModel.UITypeEditors
{
    class FontEditor : UITypeEditor
    {
        private FontDialog fontDialog = new FontDialog();

        public override object EditValue(ITypeDescriptorContext context,
            IServiceProvider provider, object value)
        {
            if (value != null)
            {
                myFont font = (myFont)value;
                fontDialog.Font = font.Font;
            }
            if (fontDialog.ShowDialog() == DialogResult.OK)
            {
                myFont font = new myFont();
                font.Font = fontDialog.Font;
                return font;
            }
            return base.EditValue(context, provider, value);
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }
    }

    public class myFont
    {
        private Font font = SystemFonts.DefaultFont;

        public Font Font
        {
            get { return font; }
            set { font = value; }
        }

        public override string ToString()
        {
            return this.font.Name + "," + this.font.Size.ToString();
        }
    }
}
