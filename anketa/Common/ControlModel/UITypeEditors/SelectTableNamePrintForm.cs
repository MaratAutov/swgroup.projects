﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace aetp.Common.ControlModel.UITypeEditors
{
    public partial class SelectTableNamePrintForm : Form
    {
        TableNamePrintForm name;

        public SelectTableNamePrintForm(TableNamePrintForm name)
        {
            InitializeComponent();
            this.name = name;
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            this.name.TableName = txt_name.Text;
            this.Close();
        }
    }
}
