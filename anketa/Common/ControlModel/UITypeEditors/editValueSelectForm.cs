﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.TypeConverters;
using aetp.Common.ControlModel.Attributes;
using aetp.Common.Validator;
using aetp.Common.ControlModel.UITypeEditors;
using System.Drawing.Design;

namespace aetp.Common.ControlModel.UITypeEditors
{
    public partial class editValueSelectForm : XtraForm
    {
        public Object EditValue
        {
            get
            {
                if (cbeTypes.SelectedIndex == 1)
                    return (string)(panelEditValue.Controls[0] as BaseEdit).EditValue;
                else if (cbeTypes.SelectedIndex == 2)
                    return Convert.ToInt32((panelEditValue.Controls[0] as BaseEdit).EditValue);
                else if (cbeTypes.SelectedIndex == 3)
                    return Convert.ToDouble((panelEditValue.Controls[0] as BaseEdit).EditValue);
                else if (cbeTypes.SelectedIndex == 4)
                    return (panelEditValue.Controls[0] as ComboBoxEdit).SelectedIndex == 0;
                else if (cbeTypes.SelectedIndex == 5)
                    return Convert.ToDateTime((panelEditValue.Controls[0] as BaseEdit).EditValue);
                else if (cbeTypes.SelectedIndex == 6)
                    return ((OrganizationDataItem)(panelEditValue.Controls[0] as ComboBoxEdit).EditValue);
                return null;
            }
            set
            {
                if (value == null)
                {
                    cbeTypes.SelectedText = "Null";
                    return;
                }
                else if (value is string)
                {
                    cbeTypes.SelectedIndex = 1;
                }
                else if (value is int)
                    cbeTypes.SelectedIndex = 2;
                else if (value is double)
                    cbeTypes.SelectedIndex = 3;
                else if (value is bool)
                {
                    cbeTypes.SelectedIndex = 4;
                    if (Convert.ToBoolean(value))
                        (panelEditValue.Controls[0] as ComboBoxEdit).SelectedIndex = 0;
                    else
                        (panelEditValue.Controls[0] as ComboBoxEdit).SelectedIndex = 1;
                }
                else if (value is DateTime)
                    cbeTypes.SelectedIndex = 5;
                else if (value is OrganizationDataItem)
                    cbeTypes.SelectedIndex = 6;
                (panelEditValue.Controls[0] as BaseEdit).EditValue = value;
            }
        }

        public editValueSelectForm()
        {
            InitializeComponent();
        }

        public editValueSelectForm(object EditValue)
        {

            InitializeComponent();
            this.EditValue = EditValue;
        }

        private void cbeTypes_SelectedValueChanged(object sender, EventArgs e)
        {
            panelEditValue.Controls.Clear();
            if (cbeTypes.SelectedIndex == 1)
            {
                TextEdit te = new TextEdit();
                te.Dock = DockStyle.Fill;
                panelEditValue.Controls.Add(te);
            }
            else if (cbeTypes.SelectedIndex == 2)
            {
                SpinEdit se = new SpinEdit();
                se.Properties.IsFloatValue = false;
                se.Dock = DockStyle.Fill;
                panelEditValue.Controls.Add(se);
            }
            else if (cbeTypes.SelectedIndex == 3)
            {
                SpinEdit se = new SpinEdit();
                se.Dock = DockStyle.Fill;
                panelEditValue.Controls.Add(se);
            }
            else if (cbeTypes.SelectedIndex == 4)
            {
                ComboBoxEdit cbe = new ComboBoxEdit();
                cbe.Properties.Items.AddRange(new object[] { "True", "False" });
                cbe.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
                cbe.SelectedIndex = 0;
                cbe.Dock = DockStyle.Fill;
                panelEditValue.Controls.Add(cbe);
            }
            else if (cbeTypes.SelectedIndex == 5)
            {
                DateEdit de = new DateEdit();
                de.Dock = DockStyle.Fill;
                panelEditValue.Controls.Add(de);
            }
            else if (cbeTypes.SelectedIndex == 6)
            {
                ComboBoxEdit cbe = new ComboBoxEdit();
                cbe.Properties.Items.Add(new OrganizationDataItem("Полное Наименование"));
                cbe.Properties.Items.Add(new OrganizationDataItem("Краткое Наименование"));
                cbe.Properties.Items.Add(new OrganizationDataItem("ИНН"));
                cbe.Properties.Items.Add(new OrganizationDataItem("ОГРН"));
                cbe.Properties.Items.Add(new OrganizationDataItem("Признак Организации"));
                cbe.Properties.Items.Add(new OrganizationDataItem("Адрес Места Нахождения"));
                cbe.Properties.Items.Add(new OrganizationDataItem("Контактный Телефон"));
                cbe.Properties.Items.Add(new OrganizationDataItem("Адрес электронной почты"));
                cbe.Properties.Items.Add(new OrganizationDataItem("ФИО Руководителя"));
                cbe.Properties.Items.Add(new OrganizationDataItem("Должность"));
                cbe.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
                cbe.SelectedIndex = 0;
                cbe.Dock = DockStyle.Fill;
                panelEditValue.Controls.Add(cbe);
            }
        }
    }

    [DataContract]
    public class OrganizationDataItem
    {
        public OrganizationDataItem(string name)
        {
            _name = name;
        }

        private string _name = string.Empty;

        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
