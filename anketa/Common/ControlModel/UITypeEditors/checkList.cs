﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace aetp.Common.ControlModel.UITypeEditors
{
    public partial class checkList : UserControl
    {
        public Anchor anc
        {
            get;
            set;
        }

        public checkList(Anchor s)
        {
            InitializeComponent();
            anc = s;
            foreach (var val in Enum.GetValues(typeof(Anchor)))
            {
                if (s.ToString().Contains(UITypeEditors.Anchor.top.ToString()))
                    ch1.Checked = true;
                if (s.ToString().Contains(UITypeEditors.Anchor.left.ToString()))
                    ch2.Checked = true;
                if (s.ToString().Contains(UITypeEditors.Anchor.right.ToString()))
                    ch3.Checked = true;
                if (s.ToString().Contains(UITypeEditors.Anchor.bottom.ToString()))
                    ch4.Checked = true;
            }

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            anc = UITypeEditors.Anchor.left;
            anc &=  (~UITypeEditors.Anchor.left);
            if (ch1.Checked)
                anc |= UITypeEditors.Anchor.top;
            if (ch2.Checked)
                anc |= UITypeEditors.Anchor.left;
            if (ch3.Checked)
                anc |= UITypeEditors.Anchor.right;
            if (ch4.Checked)
                anc |= UITypeEditors.Anchor.bottom;

            (this.Tag as IWindowsFormsEditorService).CloseDropDown();            
        }
    }

    [Flags]
    public enum Anchor
    {
        [Description("Left")]
        left = 0x01,
        [Description("Right")]
        right = 0x02,
        [Description("Bottom")]
        bottom = 0x04,
        [Description("Top")]
        top = 0x08
    }
}
