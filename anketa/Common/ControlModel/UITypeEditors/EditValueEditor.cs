﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using aetp.Common.ControlModel.UITypeEditors;

namespace aetp.Common.ControlModel.UITypeEditors
{
    class EditValueEditor:UITypeEditor
    {
        public override Object EditValue(
    ITypeDescriptorContext context,
    IServiceProvider provider,
    Object value)
        {
            if ((context != null) && (provider != null))
            {
                if (context.PropertyDescriptor.ComponentType.FullName == "aetp.Common.ControlModel.ComboBox")
                {
                    
                }
                IWindowsFormsEditorService svc =
                  (IWindowsFormsEditorService)
                  provider.GetService(typeof(IWindowsFormsEditorService));

                if (svc != null)
                {
                    using (editValueSelectForm editfrm =
                      new editValueSelectForm(value))
                    {
                        if (svc.ShowDialog(editfrm) == DialogResult.OK)
                        {
                            value = editfrm.EditValue;
                        }
                    }
                }
            }

            return base.EditValue(context, provider, value);
        }

        /// <summary>
        /// Возвращаем стиль редактора - выпадающее окно
        /// </summary>
        public override UITypeEditorEditStyle GetEditStyle(
          ITypeDescriptorContext context)
        {
            if (context != null)
                return UITypeEditorEditStyle.Modal;
            else
                return base.GetEditStyle(context);
        }
    }

}
