﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.Control;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using aetp.Common.Validator;
using System.Reflection;

namespace aetp.Common.ControlModel.UITypeEditors
{
    public partial class MetaGridEditForm : Form
    {
        MetaGrid model = new MetaGrid();
        
        public TableCollection EditValue
        {
            get
            {
                return model.Tables;
            }
            set
            {
                model.Tables = ((value as TableCollection).Clone() as TableCollection);                
            }
        }

        public MetaGridEditForm(TableCollection EditValue)
        {
            InitializeComponent();            
            this.EditValue = EditValue;
        }

        private void lv_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv_grid.SelectedItems.Count > 0)
            {
                pg.SelectedObject = (model as MetaGrid).Tables[lv_grid.SelectedItems[0].Index];
                lb_property.Text = "Свойства " + lv_grid.SelectedItems[0].Text.ToString() + " :";
            }
            else if (lv_grid.Items.Count == 0)
            {
                lb_property.Text = "Свойства :";
            }
        }


        private void Refresh_List()
        {
            lv_grid.Items.Clear();
            if ((model as MetaGrid).Tables.Count > 0)
            {
                foreach (BaseGrid grid in (model as MetaGrid).Tables)
                {
                    lv_grid.Items.Add(grid.Name);
                }
                lv_grid.Items[0].Selected = true;
            }
        }

        private void drop_down_container_Click(object sender, EventArgs e)
        {
            DevExpress.Utils.Menu.DXMenuItem menu_item = sender as DevExpress.Utils.Menu.DXMenuItem;
            BaseGrid item = new BaseGrid();
            
            if (menu_item.Caption == "Фиксированная таблица")
            {
                item = new Grid();
            }
            else if (menu_item.Caption == "Таблица с добавлением строк")
            {
                item = new GridAddedRow();
            }

            item.Root = (model as MetaGrid).Root;
            item.metagrid = (model as MetaGrid);
            item.Meta_Table = true;
            item.Caption = " ";
            item.WidthCaption = 10;
            item.Parent = (model as MetaGrid).Parent;
            if (model.Tables != null)
            {
                model.Tables.Add(item);
                lv_grid.Items.Add(new ListViewItem(item.Name));
                lv_grid.Focus();
                lv_grid.Items[lv_grid.Items.Count - 1].Selected = true;
                lv_grid.Items[lv_grid.SelectedItems[0].Index].EnsureVisible();
            }           
        }


        private void MetaGridEditForm_Load(object sender, EventArgs e)
        {
            
            DevExpress.Utils.Menu.DXPopupMenu drop_down_container = new DevExpress.Utils.Menu.DXPopupMenu();

            DevExpress.Utils.Menu.DXMenuItem item1 = new DevExpress.Utils.Menu.DXMenuItem();
            item1.Caption = "Фиксированная таблица";
            item1.Click += new System.EventHandler(drop_down_container_Click);
            
            DevExpress.Utils.Menu.DXMenuItem item2 = new DevExpress.Utils.Menu.DXMenuItem();
            item2.Caption = "Таблица с добавлением строк";
            item2.Click += new System.EventHandler(drop_down_container_Click);

            drop_down_container.Items.Add(item1);
            drop_down_container.Items.Add(item2);

            this.btn_add_grid.DropDownControl = drop_down_container;
            lv_grid.Columns[0].Text = " ";

            Refresh_List();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (lv_grid.Items.Count > 0)
            {
                var metagrid = model as MetaGrid;
                for (int i = 0; i < metagrid.Tables.Count; i++)
                {
                    if (metagrid.Tables[i].Name == lv_grid.SelectedItems[0].Text)
                    {
                        metagrid.Tables.Remove(metagrid.Tables[i]);
                        lv_grid.Items.Remove(lv_grid.SelectedItems[0]);

                        lv_grid.Focus();
                        if (lv_grid.Items.Count > 0)
                        {
                            lv_grid.Items[lv_grid.Items.Count - 1].Selected = true;
                            lv_grid.Items[lv_grid.SelectedItems[0].Index].EnsureVisible();
                        }
                        else
                            if (lv_grid.Items.Count == 0)
                            {
                                lb_property.Text = "Свойства :";
                                pg.SelectedObject = null;
                            }
                        break;
                    }
                }
            }           
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            if (lv_grid.SelectedItems.Count > 0)
            {
                if (lv_grid.SelectedItems[0].Index > 0)
                {
                    var metagrid = model as MetaGrid;
                    int index = lv_grid.SelectedItems[0].Index - 1;
                    BaseGrid newTable = (BaseGrid)metagrid.Tables[lv_grid.SelectedItems[0].Index].Clone();
                    metagrid.Tables[lv_grid.SelectedItems[0].Index] = metagrid.Tables[lv_grid.SelectedItems[0].Index - 1];
                    metagrid.Tables[lv_grid.SelectedItems[0].Index - 1] = newTable;

                    Refresh_List();
                    lv_grid.Items[index].Selected = true;
                }
                
            }
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (lv_grid.SelectedItems.Count > 0)
            {
                if (lv_grid.SelectedItems[0].Index != lv_grid.Items.Count - 1)
                {
                    var metagrid = model as MetaGrid;
                    int index = lv_grid.SelectedItems[0].Index + 1;
                    BaseGrid newTable = (BaseGrid)metagrid.Tables[lv_grid.SelectedItems[0].Index].Clone();
                    metagrid.Tables[lv_grid.SelectedItems[0].Index] = metagrid.Tables[lv_grid.SelectedItems[0].Index + 1];
                    metagrid.Tables[lv_grid.SelectedItems[0].Index + 1] = newTable;

                    Refresh_List();
                    lv_grid.Items[index].Selected = true;
                }
               
            }
        }

        private void pg_SelectedObjectsChanged(object sender, EventArgs e)
        {
            if (pg.SelectedObject != null)
            {
                PropertyDescriptor descriptor = TypeDescriptor.GetProperties(pg.SelectedObject.GetType())["EditValue"];

                BrowsableAttribute attribute = (BrowsableAttribute)
                                              descriptor.Attributes[typeof(BrowsableAttribute)];
                FieldInfo fieldToChange = attribute.GetType().GetField("browsable",
                                                 System.Reflection.BindingFlags.NonPublic |
                                                 System.Reflection.BindingFlags.Instance);
                fieldToChange.SetValue(attribute, false);

                pg.Refresh();
            }
        }       
    }
}
