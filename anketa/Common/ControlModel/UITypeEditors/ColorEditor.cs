﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using aetp.Common.ControlModel.UITypeEditors;

namespace aetp.Common.ControlModel.UITypeEditors
{
    class ColorEditor : UITypeEditor
    {
        private ColorDialog colorDialog = new ColorDialog();

        public override object EditValue(ITypeDescriptorContext context,
            IServiceProvider provider, object value)
        {
            if (value != null)
            {
                myColor color = (myColor)value;
                colorDialog.Color = color.Color;
            }
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                myColor color = new myColor();
                color.Color = colorDialog.Color;
                return color;
            }
            return base.EditValue(context, provider, value);
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }
    }

    public class myColor
    {
        private Color color = System.Drawing.Color.Black;

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public override string ToString()
        {
            return this.color.Name;
        }
    }
}
