﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing.Design;
using System.ComponentModel.Design;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace aetp.Common.ControlModel.UITypeEditors
{
    class MetaGridValueEditor : UITypeEditor
    {
        public override Object EditValue(
            ITypeDescriptorContext context,
            IServiceProvider provider,
            Object value)
        {
            if ((context != null) && (provider != null))
            {
                IWindowsFormsEditorService svc =
                  (IWindowsFormsEditorService)
                  provider.GetService(typeof(IWindowsFormsEditorService));

                if (svc != null)
                {
                    using (MetaGridEditForm editfrm =
                      new MetaGridEditForm((value as TableCollection)))
                    {
                        if (svc.ShowDialog(editfrm) == DialogResult.OK)
                        {
                            value = editfrm.EditValue;
                        }
                    }
                }
            }
            return base.EditValue(context, provider, value);
        }

        /// <summary>
        /// Возвращаем стиль редактора - выпадающее окно
        /// </summary>
        public override UITypeEditorEditStyle GetEditStyle(
          ITypeDescriptorContext context)
        {
            if (context != null)
                return UITypeEditorEditStyle.Modal;
            else
                return base.GetEditStyle(context);
        }
    }
}
