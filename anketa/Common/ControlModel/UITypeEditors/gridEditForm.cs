﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.Control;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using aetp.Common.Validator;
using System.Reflection;

namespace aetp.Common.ControlModel.UITypeEditors
{
    public partial class gridEditForm : Form
    {
        Grid model;
        aetpGrid grid;
        public Object EditValue 
        {
            get
            {
                return model;
            }
            set
            {
                model = ((value as Grid).Clone() as Grid);
                grid = new Control.aetpGrid();
                grid.IsPreview = true;
                grid.gv.ColumnPositionChanged += new EventHandler(gv_ColumnPositionChanged);
                grid.gc.Dock = DockStyle.Fill;
                grid.lblCaption.Visible = false;
                splitContainer.Panel1.Controls.Add(grid);
                grid.BringToFront();
                grid.Dock = DockStyle.Fill;
                grid.Configure(model);
                grid.gv.OptionsCustomization.AllowColumnMoving = true;
                grid.gv.DragObjectOver += new DragObjectOverEventHandler(gv_DragObjectOver);
                grid.gv.FocusedRowChanged += new FocusedRowChangedEventHandler(gv_FocusedRowChanged);
                grid.gv.FocusedColumnChanged += new FocusedColumnChangedEventHandler(gv_FocusedColumnChanged);
                foreach (var row in model.Rows)
                {
                    foreach (var cell in row.Cells)
                    {
                        cell.IsMandatoryToFillChanged += new EventHandler(cell_IsMandatoryToFillChanged);
                        cell.MaskChanged += new EventHandler(cell_MaskChanged);
                    }
                }
            }
        }

        void gv_DragObjectOver(object sender, DragObjectOverEventArgs e)
        {
            DevExpress.XtraGrid.Columns.GridColumn column = e.DragObject as DevExpress.XtraGrid.Columns.GridColumn;
            if (column != null)
            {
                e.DropInfo.Valid = !(e.DropInfo.Index == -100);
            }
        }

        void gv_ColumnPositionChanged(object sender, EventArgs e)
        {

            var columns = from DevExpress.XtraGrid.Columns.GridColumn col in grid.gv.Columns orderby col.VisibleIndex ascending select col;   
            int k = 0;
            foreach (DevExpress.XtraGrid.Columns.GridColumn gcol in columns)
            {
                model.Columns[k] = gcol.Tag as GridColumn;
                k++;
            }

            foreach (var row in model.Rows)
            {                
                for (int i = 0; i < model.Columns.Count; i++)
                {
                    for (int j = 0; j < row.Cells.Count; j++)
                    {
                        if (row.Cells[j].Column == model.Columns[i])
                        {
                            if (i != j)
                            {
                                GridCellItem temp = row.Cells[i];
                                row.Cells[i] = row.Cells[j];
                                row.Cells[j] = temp;
                            }
                            break;
                        }
                    }
                }
            }
            grid.gc.RefreshDataSource();
        }

        void gv_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            CellChangedHandler(grid.gv, grid.gv.FocusedRowHandle, e.FocusedColumn);
        }

        void gv_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            CellChangedHandler(grid.gv, e.FocusedRowHandle, 
                grid.gv.FocusedColumn);
        }

        private void CellChangedHandler(GridView gridView1, int FocusedRowHandle, DevExpress.XtraGrid.Columns.GridColumn FocusedColumn)
        {
            if (FocusedColumn != null && FocusedRowHandle >= 0 && FocusedRowHandle < model.Rows.Count &&
                FocusedColumn.VisibleIndex >= 0 && FocusedColumn.VisibleIndex < model.Columns.Count)
            {
                if (rbCell.Checked)
                {
                    pg.SelectedObject = model.Rows[FocusedRowHandle].Cells[FocusedColumn.VisibleIndex];
                }
                else if (rbRow.Checked)
                {
                    pg.SelectedObject = model.Rows[FocusedRowHandle];
                }
                else if (rbColumn.Checked)
                {
                    pg.SelectedObject = model.Columns[FocusedColumn.VisibleIndex];
                }
            }
            else pg.SelectedObject = null;
        }

        GridCellItem FocusedCell
        {
            get
            {
                GridCell[] cells = grid.gv.GetSelectedCells();
                if (cells.Length > 0)
                    return model.Rows[cells[0].RowHandle].Cells[cells[0].Column.VisibleIndex];
                return null;
            }
        }
        
        public gridEditForm()
        {
            InitializeComponent();
        }

        public gridEditForm(object EditValue)
        {
            InitializeComponent();
            this.EditValue = EditValue;
        }

        private void btnAddRow_Click(object sender, EventArgs e)
        {
            GridRow gr = new GridRow();
            //хз почему имена одинаковые получаются без этого
            gr.Name = String.Format("Row_{0}", BaseControl.new_element_index.ToString());
            gr.Caption = String.Format("Row_{0}", BaseControl.new_element_index.ToString());
            Grid g = model;
            gr.Root = g.Root;
            gr.Grid = g;
            int col = g.Columns.Count;
            for (int i = 0; i < col; i++)
            {
                GridCellItem cell = new GridCellItem() { Column = g.Columns[i] };
                cell.Root = g.Root;
                cell.Row = gr;
                //хз почему имена одинаковые получаются без этого
                cell.Name = String.Format("Cell_{0}", BaseControl.new_element_index.ToString());
                cell.Caption = String.Format("Cell_{0}", BaseControl.new_element_index.ToString());
                cell.NameChanged += new EventHandler(cell_NameChanged);
                gr.Cells.Add(cell);
                cell.IsMandatoryToFillChanged += new EventHandler(cell_IsMandatoryToFillChanged);
                cell.MaskChanged += new EventHandler(cell_MaskChanged);
            }
            g.Rows.Add(gr);
            grid.NewRow();
        }

        void cell_MaskChanged(object sender, EventArgs e)
        {
            GridCellItem cell = sender as GridCellItem;
            bool issetMaskValidator = IssetMaskValidator(cell);
            if (!String.IsNullOrEmpty(cell.Mask.Mask))
            {
                if (!issetMaskValidator)
                    cell.EditorValidators.Add(new RegexValidationItem(cell.Mask));
            }
            else
            {
                if (issetMaskValidator)
                {
                    for (int i = cell.EditorValidators.Count - 1; i >= 0; i--)
                        if (cell.EditorValidators[i] is RegexValidationItem)
                            cell.EditorValidators.RemoveAt(i);
                }
            }
            grid.gc.RefreshDataSource();
        }

        void cell_IsMandatoryToFillChanged(object sender, EventArgs e)
        {
            GridCellItem cell = sender as GridCellItem;
            bool issetEmptyValidator = IssetEmptyValidator(cell);
            if (cell.IsMandatoryToFill == true)
            {
                if (!issetEmptyValidator)
                    cell.EditorValidators.Add(new EmptyValidationItem());
            }
            else
            {
                if (issetEmptyValidator)
                {
                    for (int i = cell.EditorValidators.Count - 1; i >= 0; i--)
                        if (cell.EditorValidators[i] is EmptyValidationItem)
                            cell.EditorValidators.RemoveAt(i);
                }
            }
            grid.gc.RefreshDataSource();
        }

        bool IssetEmptyValidator(GridCellItem cell)
        {
            bool b = false;
            foreach (var v in cell.EditorValidators)
            {
                if (v is EmptyValidationItem)
                {
                    b = true;
                    break;
                }
            }
            return b;
        }

        bool IssetMaskValidator(GridCellItem cell)
        {
            bool b = false;
            foreach (var v in cell.EditorValidators)
            {
                if (v is RegexValidationItem)
                {
                    b = true;
                    break;
                }
            }
            return b;
        }

        void cell_NameChanged(object sender, EventArgs e)
        {
            ValidateEditValue();
        }

        protected void ValidateEditValue()
        {
            GridRowCollection items = model.Rows;
            bool ok = true;
            for (int i = 0; i < items.Count; i++)
            {
                if (String.IsNullOrEmpty(items[i].Name))
                {
                    ok = false;
                    break;
                }
                foreach (GridCellItem cell in items[i].Cells)
                {
                    if (String.IsNullOrEmpty(cell.Name))
                    {
                        ok = false;
                        break;
                    }
                }
                if (ok == false)
                    break;
            }
            btnOk.Enabled = ok;
            model.HeightGrid = items.Count * 21 + 25;
        }

        private void btnRemoveRow_Click(object sender, EventArgs e)
        {
            if ((grid.gv.FocusedRowHandle < model.Rows.Count) && (model.Rows.Count != 0))
            {
                model.Rows.RemoveAt(grid.gv.FocusedRowHandle);
                grid.RemoveRowAt(grid.gv.FocusedRowHandle);
            }
        }

        private void pg_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            pg.Refresh();
            grid.gc.RefreshDataSource();
            grid.RefreshCellsData();
        }

        private void rbCell_CheckedChanged(object sender, EventArgs e)
        {
            CellChangedHandler(grid.gv, grid.gv.FocusedRowHandle, grid.gv.FocusedColumn);
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            int focusedrow = grid.gv.FocusedRowHandle;
            if (grid.gv.FocusedRowHandle < model.Rows.Count && grid.gv.FocusedRowHandle > 0)
                swap(grid.gv.FocusedRowHandle, grid.gv.FocusedRowHandle - 1);
            grid.gv.FocusedRowHandle = --focusedrow;
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            int focusedrow = grid.gv.FocusedRowHandle;
            if (grid.gv.FocusedRowHandle < model.Rows.Count - 1 && grid.gv.FocusedRowHandle >= 0)
                swap(grid.gv.FocusedRowHandle, grid.gv.FocusedRowHandle + 1);
            grid.gv.FocusedRowHandle = ++focusedrow;
        }

        void swap(int r1, int r2)
        {
            GridRow temp = model.Rows[r1];
            model.Rows[r1] = model.Rows[r2];
            model.Rows[r2] = temp;
            grid.SwapRow(r1, r2);
        }        

        private void btnColumnAdd_Click(object sender, EventArgs e)
        {
            GridColumn column = new GridColumn();
            foreach (var row in model.Rows)
            {
                GridCellItem cell = new GridCellItem() { Column = column, Row = row };
                cell.Name = String.Format("Cell_{0}", BaseControl.new_element_index.ToString());
                cell.Caption = String.Format("Cell_{0}", BaseControl.new_element_index.ToString());
                row.Cells.Add(cell);
                cell.IsMandatoryToFillChanged += new EventHandler(cell_IsMandatoryToFillChanged);
                cell.MaskChanged += new EventHandler(cell_MaskChanged);
            }
            model.Columns.Add(column);
            grid.NewColumn(column);
            grid.gc.RefreshDataSource();
        }

        private void btnColumnRemove_Click(object sender, EventArgs e)
        {
            if (grid.gv.FocusedColumn != null && grid.gv.FocusedColumn.VisibleIndex >= 0 && grid.gv.FocusedColumn.VisibleIndex < model.Columns.Count)
            {
                GridColumn col = model.Columns[grid.gv.FocusedColumn.VisibleIndex];

                foreach (GridRow row in model.Rows)
                {
                    for (int k = row.Cells.Count - 1; k >= 0; k--)
                    {
                        if (row.Cells[k].Column == col)
                            row.Cells.RemoveAt(k);
                    }
                }
                model.Columns.RemoveAt(grid.gv.FocusedColumn.VisibleIndex);
                grid.RemoveColumnAt(grid.gv.FocusedColumn.VisibleIndex);
            }
        }

        private void gridEditForm_Load(object sender, EventArgs e)
        {

        }

        private void pg_SelectedObjectsChanged(object sender, EventArgs e)
        {
            if (pg.SelectedObject != null)
            {
                PropertyDescriptor descriptor = TypeDescriptor.GetProperties(pg.SelectedObject.GetType())["EditValue"];

                BrowsableAttribute attribute = (BrowsableAttribute)
                                              descriptor.Attributes[typeof(BrowsableAttribute)];
                FieldInfo fieldToChange = attribute.GetType().GetField("browsable",
                                                 System.Reflection.BindingFlags.NonPublic |
                                                 System.Reflection.BindingFlags.Instance);
                fieldToChange.SetValue(attribute, false);

                pg.Refresh();
            }
        }

        private void btnColumnLeft_Click(object sender, EventArgs e)
        {           
            if (grid.gv.FocusedColumn.AbsoluteIndex < model.Columns.Count && grid.gv.FocusedColumn.AbsoluteIndex > 0)
            {
                grid.SwapColumn(grid.gv.FocusedColumn.AbsoluteIndex, grid.gv.FocusedColumn.AbsoluteIndex - 1);
                grid.gv.FocusedColumn.VisibleIndex -= 1;
            }           
            
        }

        private void btnColumnRight_Click(object sender, EventArgs e)
        {            
            if (grid.gv.FocusedColumn.AbsoluteIndex < model.Columns.Count-1 && grid.gv.FocusedColumn.AbsoluteIndex >= 0)
            {
                grid.SwapColumn(grid.gv.FocusedColumn.AbsoluteIndex, grid.gv.FocusedColumn.AbsoluteIndex + 1);                
                grid.gv.FocusedColumn.VisibleIndex += 1;
            }
        }

    }
}
