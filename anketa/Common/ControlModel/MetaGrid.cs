﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.TypeConverters;
using aetp.Common.ControlModel.Attributes;
using aetp.Common.ControlModel.UITypeEditors;
using System.Drawing.Design;

namespace aetp.Common.ControlModel
{
	public class MetaGrid : BaseGrid
	{
		[Editor(typeof(TableEditor), typeof(System.Drawing.Design.UITypeEditor))]
		[Category("Проектирование")]
        [Browsable (false)]
		[DisplayName("Таблицы")]
		public TableCollection Tables { get; set; }

        [Browsable(false)]
        public override bool IsMandatoryToFill { get; set; }

        [Editor(typeof(MetaGridValueEditor), typeof(UITypeEditor))]
        [Category("Проектирование")]
        [DisplayName("Конструктор")]
        
        public TableCollection thisGrid
        {
            get
            {
                return this.Tables;
            }
            set
            {
                this.Tables = value;
            }
        }

		public MetaGrid()
		{
			Tables = new TableCollection(this);
            WidthCaption = 500;
            IsMandatoryToFill = false;
		}

		[Browsable(false)]
		protected override string TemplateName
		{
			get
			{
				return "MetaTable_{0}";
			}
		}

		[Browsable(false)]
		protected override string TemplateCaption
		{
			get
			{
				return "MetaTable {0}";
			}
		}


        [DisplayName("Ширина таблицы")]
        [Browsable(false)]
        [DataMember]
        [Category("Макет")]
        public override int WidthGrid { get; set; }

        [DisplayName("Высота таблицы")]
        [Browsable(false)]
        [Category("Макет")]
        [DataMember]
        public override int HeightGrid { get; set; }

        
        [DisplayName("Расстояние между Подписью и таблицей")]
        [Browsable(false)]
        [Category("Макет")]
        [DataMember]
        public override int Interval { get; set; }


        public override object Clone()
        {
            MetaGrid newMetaGrid = (MetaGrid)this.MemberwiseClone();
            newMetaGrid.Tables = new TableCollection(newMetaGrid);
            foreach (BaseGrid table in this.Tables)
            {
                BaseGrid newTable = (BaseGrid)table.Clone();
                newTable.metagrid = newMetaGrid;                
                newMetaGrid.Tables.Add(newTable);

            }
            return newMetaGrid;
        }
	}

	#region collections

	[DataContract]
	public class TableCollection : Collection<BaseGrid>
	{
		[DataMember]
		public MetaGrid MetaGrid { get; set; }
		public TableCollection(MetaGrid metagrid)
		{
			MetaGrid = metagrid;
		}
		protected override void InsertItem(int index, BaseGrid item)
		{
			item.Root = MetaGrid.Root;
			item.metagrid = MetaGrid;
            item.Meta_Table = true;
            item.Caption = " ";
            item.WidthCaption = 10;
            item.Parent = MetaGrid.Parent;
			base.InsertItem(index, item);
		}

		protected override void SetItem(int index, BaseGrid item)
		{
			item.Root = MetaGrid.Root;
			item.metagrid = MetaGrid;
            item.Meta_Table = true;
            item.Caption = " ";
            item.WidthCaption = 10;
            item.Parent = MetaGrid.Parent;
			base.SetItem(index, item);
		}

        public object Clone()
        {
            TableCollection newCollection = new TableCollection(MetaGrid);

            foreach (BaseGrid table in this)
            {
                BaseGrid newTable = (BaseGrid)table.Clone();
                newCollection.Add(newTable);

            }
            return newCollection;
        }
	}

	#endregion

	#region Editors

	public class TableEditor : System.ComponentModel.Design.CollectionEditor
	{
		protected static Dictionary<CollectionForm, Button> okayButtons = new Dictionary<CollectionForm, Button>();
		protected CollectionForm collectionForm;
		protected MetaGrid metagrid;

		public TableEditor(Type type)
			: base(type)
		{

		}

		protected override CollectionForm CreateCollectionForm()
		{
			collectionForm = base.CreateCollectionForm();
			collectionForm.FormClosed +=
				new FormClosedEventHandler(collectionForm_FormClosed);
			collectionForm.Load += new EventHandler(collectionForm_Load);
			if (collectionForm.Controls.Count > 0)
			{
				TableLayoutPanel mainPanel = collectionForm.Controls[0]
					as TableLayoutPanel;
				if ((mainPanel != null) && (mainPanel.Controls.Count > 7))
				{
					// Get a reference to the inner PropertyGrid and hook 
					// an event handler to it.
					PropertyGrid propertyGrid = mainPanel.Controls[5]
						as PropertyGrid;
					if (propertyGrid != null)
					{
						propertyGrid.PropertyValueChanged +=
							new PropertyValueChangedEventHandler(
								propertyGrid_PropertyValueChanged);
					}
					// Also hook to the Add/Remove
					TableLayoutPanel buttonPanel = mainPanel.Controls[1]
						as TableLayoutPanel;
					if ((buttonPanel != null) && (buttonPanel.Controls.Count > 1))
					{
						Button addButton = buttonPanel.Controls[0] as Button;
						if (addButton != null)
						{
							addButton.Click += new EventHandler(addButton_Click);
                            
						}

						Button removeButton = buttonPanel.Controls[1] as Button;
						if (removeButton != null)
						{
							removeButton.Click += new EventHandler(removeButton_Click);
						}
					}

					// Find the OK button, and hold onto it.
					buttonPanel = mainPanel.Controls[6] as TableLayoutPanel;
					if ((buttonPanel != null) && (buttonPanel.Controls.Count > 1))
					{
						Button okayButton = buttonPanel.Controls[0] as Button;
						if (okayButton != null)
						{
							okayButtons[collectionForm] = okayButton;
						}
					}
				}
			}
			return collectionForm;
		}

		private void collectionForm_FormClosed(object sender,
			FormClosedEventArgs e)
		{
			CollectionForm collectionForm = (CollectionForm)sender;
			if (okayButtons.ContainsKey(collectionForm))
			{
				okayButtons.Remove(collectionForm);
			}
		}

		private void collectionForm_Load(object sender, EventArgs e)
		{
			ValidateEditValue((CollectionForm)sender);
		}

		private void propertyGrid_PropertyValueChanged(object sender,
			PropertyValueChangedEventArgs e)
		{
			ValidateEditValue((CollectionForm)(sender as PropertyGrid).ParentForm);
		}

		private void addButton_Click(object sender, EventArgs e)
		{
			Button addButton = (Button)sender;
			ValidateEditValue((CollectionForm)addButton.Parent.Parent.Parent);
		}

		private void removeButton_Click(object sender, EventArgs e)
		{
			Button removeButton = (Button)sender;
			ValidateEditValue((CollectionForm)removeButton.Parent.Parent.Parent);
		}

		protected virtual void ValidateEditValue(CollectionForm collectionForm)
		{

		}

        protected override Type[] CreateNewItemTypes()
        {
            return new Type[] { typeof(GridAddedRow), typeof(Grid) };
        }        
	}
	#endregion
}
