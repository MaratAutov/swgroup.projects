﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using aetp.Common.ControlModel.TypeConverters;
using aetp.Common.Validator;
using aetp.Common.ControlModel.Attributes;
using System.Drawing.Design;
using aetp.Common.ControlModel.UITypeEditors;
using System.Runtime.Serialization;

namespace aetp.Common.ControlModel
{
    public class SimpleRichEdit : BaseControl
    {
        public virtual event EventHandler Visible_Changed;
        protected void RaiseVisible_Changed()
        {
            if (Visible_Changed != null)
            {
                Visible_Changed(this, null);
            }
        }

        private int _widthControl;
        private int _heigthControl;

        [Browsable(false)]
        public int Index { get; set; }
        [Browsable(false)]
        public string Text { get; set; }

        
        [DisplayName("Ширина")]
        [Category("Макет")]        
        public int WidthControl
        {
            get
            {
                return _widthControl;
            }
            set
            {
                if (value > 20)
                    _widthControl = value;
                else _widthControl = 500;
            }
        }
        
        [DisplayName("Высота")]
        [Category("Макет")]        
        public int HeigthControl
        {
            get
            {
                return _heigthControl;
            }
            set
            {
                if (Convert.ToInt32(value) > 20)
                    _heigthControl = value;
                else _heigthControl = 20;
            }
        }
        

        [DisplayName("Расстояние между подписью и наполняемой частью")]
        [Category("Макет")]
        public int Interval { get; set; }

        [DisplayName("Ширина подписи")]
        [Category("Макет")]
        public int WidthCaption { get; set; }


        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "RichEdit_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "RichEdit {0}";
            }
        }

        public SimpleRichEdit()
        {
            WidthControl = 500;
            HeigthControl = 150;
            Interval = 6;
            WidthCaption = 100;
        }
    }    
}
