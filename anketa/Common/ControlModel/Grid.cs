﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.TypeConverters;
using aetp.Common.ControlModel.Attributes;
using aetp.Common.Validator;
using aetp.Common.ControlModel.UITypeEditors;
using System.Drawing.Design;
using System.Reflection;
using System.Data;

namespace aetp.Common.ControlModel
{
    public class Grid : BaseGrid
    {
        
        public event EventHandler Refreshing;
        public void RaiseRefreshing(object sender)
        {
            if (Refreshing != null)
                Refreshing(sender, null);
        }

        [OnDeserialized]
        void OnDeserializedMethod(StreamingContext Context)
        {
            Data = null;
        }

        [Browsable(false)]
        [Editor(typeof(GridColumnEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Проектирование")]
        [DisplayName("Колонки")]
        [DataMember]
        public virtual GridColumnCollection Columns { get; set; }
        
        [Browsable(false)]
        [Category("Проектирование")]
        [DisplayName("Строки")]
        [DataMember(Order=0)]
        public GridRowCollection Rows { get; set; }

        [Editor(typeof(GridValueEditor), typeof(UITypeEditor))]
        [Category("Проектирование")]
        [DisplayName("Конструктор")]
        [DataMember]
        public Grid thisGrid
        {
            get
            {
                return this;
            }
            set
            {

                this.Rows = (value as Grid).Rows;
                this.Columns = (value as Grid).Columns;
#if LikeSlow 
                // так как тормозит рефреш списка моделей сделана нормализация при загрузке одной модели
                // без этого теряется нормальная связь с родительским гридом
                //foreach (GridRow row in this.Rows)
                //{
                //    row.Grid = this;
                //    foreach (GridCellItem cell in row.Cells)
                //    {
                //        cell.Grid = this;
                //    }
                //}
                
                //// первый раз вызываетс с Exception при выполнении linq для Columns при втором вызове срабатывает без эксепшена
                //try
                //{
                //    foreach (GridColumn col in (this as Grid).Columns)
                //    {
                //        col.grid = this;
                //    }
                //}
                //catch
                //{

                //}
#endif
            }
        }

        [DataMember]
		private bool _AutoHeight;

		[DisplayName("Автоподбор высоты")]
		[Category("Макет")]
        [DataMember(Order=1)]
		public bool AutoHeight
		{
			get { return _AutoHeight; }
			set
			{
				if (value)
				{					
                    HeightGrid = Rows.Count * 22 + (HideHeader ? 0 : HeightHeaderGrid) + 30;
				}

				_AutoHeight = value;
			}
		}        

        [DataMember]        
        [DisplayName("Скрыть заголовок")]        
        [Category("Макет")]
        public bool HideHeader { get; set; }

        [DataMember]
        [DisplayName("ResizeColumn")]
        [Browsable(false)]
        [Category("Макет")]
        public bool CanResizeColumn { get; set; }


        [Browsable(false)]
        [DataMember]
        public override bool IsMandatoryToFill { get; set; }

        public Grid()
        {
            Columns = new GridColumnCollection(this);
            Rows = new GridRowCollection(this);
            WidthCaption = 100;
            WidthGrid = 480;
            Interval = 6;
            HeightGrid = 200;
            CanResizeColumn = true;
            Meta_Table = false;
            HeightHeaderGrid = 22;
        }     
       


        #region Clone

        public override object Clone()
        {
            Grid newGrid = (Grid)this.MemberwiseClone();
            newGrid.Refreshing = this.Refreshing;
            newGrid.Columns = new GridColumnCollection(newGrid);
            foreach (var column in this.Columns)
            {
                GridColumn newColumn = (GridColumn)column.Clone();
                newColumn.grid = newGrid;
                newGrid.Columns.Add(newColumn);
            }
            newGrid.Rows = new GridRowCollection(newGrid);
            foreach (var row in this.Rows)
            {
                GridRow newRow = (GridRow)row.Clone();
                newRow.Grid = newGrid;
                newGrid.Rows.Add(newRow);
            }

            /* 
             *  для прохождения теста gridEditFormTest.gridEditForm_ChangeColumnNameTest()
             *  иначе свойство Column у Cell не будет указывать на нужную колонку
             *  возможно дейсвия излишне но это проще чем разобраться во всем механизме клонирования и редактирования коллекций Grid etc Rows, Columns...
             */
            for (int i = 0; i < newGrid.Columns.Count; i++)
                for (int j = 0; j < newGrid.Rows.Count; j++)
                    newGrid.Rows[j].Cells[i].Column = newGrid.Columns[i];

            return newGrid;
        }       

        #endregion

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Table_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "Table {0}";
            }
        }

        public override void ClearProperty()
        {
            if (Data == null) return;
            for (int r = 0; r < this.Rows.Count; r++)
            {
                for (int c = 0; c < this.Rows[r].Cells.Count; c++)
                {
                    if (this.Rows[r].Cells[c].Type != GridCellType.Label)
                    {
                        Data.Rows[r][c] = DBNull.Value;
                    }
                }
            }
        }
    }

    [DataContract]
    public class GridColumn : BaseControl
    {
        [Browsable(false)]
        public override bool IsMandatoryToFill { get; set; }
        [Browsable(false)]
        [DataMember]
        public Grid grid;
        string _name;
        int _widthcolumn;
        bool _checkColumn;
        
        
        [DataMember]
        [DisplayName("Имя")]
        [Category("Проектирование")]
        public override string Name
        {
            get
            {
                if (String.IsNullOrEmpty(_name))
                    _name = String.Format(TemplateName, new_element_index);
                return _name;
            }
            set
            {
                if (grid != null)
                    nameValidator.Validate(grid.Columns.ToList<BaseControl>(), value, this);
                _name = value;
            }
        }
        [DataMember]
        [DisplayName("Ширина столбца")]
        [Category("Проектирование")]
        [DefaultValue(100)]        
        public int WidthColumn
        {
            get
            {
                return _widthcolumn;
            }
            set
            {
                if (Convert.ToInt32(value) > 0)
                    _widthcolumn = value;
                else _widthcolumn = 100;
            }
        }

        [DataMember]
        [DisplayName("Checked column")]
        [Category("Проектирование")]        
        public bool CheckColumn
        {
            get
            {
                return _checkColumn;
            }
            set
            {
                _checkColumn = value;
            }
        }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Column_{0}";
            }
        }

    }
    
    [DataContract]
    public class GridRow : BaseControl
    {
        [Browsable(false)]
        public override bool IsMandatoryToFill { get; set; }

        [XmlIgnore]
        [DataMember]
        [Browsable(false)]
        public Grid Grid { get; set; }

        [Browsable(false)]
        public override string Caption
        {
            get;
            set;
        }

        [Editor(typeof(GridCellEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DataMember]
        [DisplayName("Ячейки")]
        [Category("Проектирование")]
        public GridCellCollection Cells { get; set; }

        

        string _name;
        [DataMember]
        [DisplayName("Имя")]
        [Category("Проектирование")]
        public override string Name
        {
            get
            {
                if (String.IsNullOrEmpty(_name))
                    _name = String.Format(TemplateName, new_element_index);
                return _name;
            }
            set
            {
                if (Grid != null)
                    nameValidator.Validate(Grid.Rows.ToList<BaseControl>(), value, this);
                _name = value;
            }
        }

        public GridRow()
        {
            Cells = new GridCellCollection(this);
        }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Row_{0}";
            }
        }

        public override object Clone()
        {
            GridRow newRow = (GridRow)this.MemberwiseClone();
            newRow.Cells = new GridCellCollection(newRow);
            foreach (var cell in Cells)
            {
                GridCellItem newCell = (GridCellItem)cell.Clone();
                newCell.Row = newRow;
                newRow.Cells.Add(newCell);
            }
            return newRow;
        }
    }

    [DataContract]
    public class GridCellItem : BaseControl
    {
        public GridCellItem()
        {
            Mask = new MaskItem();
        }

        public override void ClearProperty()
        {
            base.ClearProperty();
            CellValidating.ValidatingCalculationValue = 0;
        }

        public override object Clone()
        {
            var c = ((GridCellItem)base.Clone());
            c.EditorValidators = new List<IValidationItem>();
            c.AddValidators();
            c.CellValidating = (CellValidating)this.CellValidating.Clone();
            return c;
        }

        [XmlIgnore]
        [DataMember]
        [Browsable(false)]
        public Grid Grid { get; set; }

        [DynamicPropertyFilter("Type", "Integer,Double")]
        [DisplayName("Максимальное значение")]
        [DataMember]
        public decimal MaxValue { get; set; }
        [DynamicPropertyFilter("Type", "Integer,Double")]
        [DisplayName("Минимальное значение")]
        [DataMember]
        public decimal MinValue { get; set; }

        private myFont font = new myFont() { Font = new System.Drawing.Font("Tahoma", 8) };
        [Editor(typeof(FontEditor), typeof(UITypeEditor))]
        [DisplayName("Шрифт")]
        [Category("Appearance")]
        [DataMember]
        public myFont Font
        {
            get { return this.font; }
            set
            {
                this.font = value;
            }
        }

        private myColor color = new myColor() { Color = System.Drawing.Color.FromArgb(255,32,31,53) };
        [Editor(typeof(ColorEditor), typeof(UITypeEditor))]
        [DisplayName("Цвет")]
        [Category("Appearance")]
        [DataMember]
        public myColor Color
        {
            get { return this.color; }
            set
            {
                this.color = value;
            }
        }

        private DevExpress.Utils.HorzAlignment align = DevExpress.Utils.HorzAlignment.Far;
        [DisplayName("Alignment")]
        [Category("Appearance")]
        [DataMember]
        public DevExpress.Utils.HorzAlignment CellAlign
        {
            get
            {
                if (this.align != DevExpress.Utils.HorzAlignment.Default)
                    return this.align;
                else return align = DevExpress.Utils.HorzAlignment.Far;
            }
            set
            {
                this.align = value;
            }
        }       

        [DynamicPropertyFilter("Type", "Check")]
        [DisplayName("Тип сериализации")]
        [Category("Сериализация")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [DataMember]
        public BoolSerializtionType boolSerializationType { get; set; }

        MaskItem _mask;
        [DynamicPropertyFilter("Type", "String")]
        [DisplayName("Маска")]
        [Category("Проверка")]
        [DataMember]
        public MaskItem Mask 
        {
            get
            {
                return _mask;
            }
            set
            {
                _mask = value;
                _mask.MaskChanged += new EventHandler(_mask_MaskChanged);
                RaiseMaskChanged();
            }
        }

        [DataMember]
        [TypeConverter(typeof(EnumTypeConverter))]
        [DisplayName("Вид автоматических расчетов")]
        [Category("Данные")]
        public FormulaType FormulaType { get; set; }

        void _mask_MaskChanged(object sender, EventArgs e)
        {
            RaiseMaskChanged();
        }
        public event EventHandler MaskChanged;
        protected void RaiseMaskChanged()
        {
            if (MaskChanged != null)
                MaskChanged(this, null);
        }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Cell_{0}";
            }
        }

        GridCellType _type;
        [DisplayName("Тип")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [Category("Проектирование")]
        [DataMember(Order=0)]
        public GridCellType Type 
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                DefaultValue = null;
            }
        }
        object _defaultValue;
        [DynamicPropertyFilter("Type", "String,Integer,Double,List,DateTime")]
        [DisplayName("Значение по умолчанию")]
        [Editor(typeof(EditValueEditor), typeof(UITypeEditor))]
        [DefaultValue(null)]
        [DataMember(Order=1)]
        public object DefaultValue 
        {
            get
            {
                return _defaultValue;
            }
            set
            {
                _defaultValue = value;
                IsGettingDefaultValue = false;
            }
        }

        public object GetFirstDefaultValue()
        {
            if (IsGettingDefaultValue)
                return true;
            else
            {
                IsGettingDefaultValue = true;
                return DefaultValue;
            }
        }

        [Browsable(false)]
        public bool IsGettingDefaultValue = false;

        [DataMember]
        [Browsable(false)]
        public GridColumn Column { get; set; }
        [DataMember]
        [Browsable(false)]
        public GridRow Row { get; set; }

        [DynamicPropertyFilter("Type", "List")]
        [DisplayName("Словарь")]
        [TypeConverter(typeof(DictionaryTypeConverter))]
        [Category("Данные")]
        [DataMember]
        public DictionaryList Source { get; set; }

        [DataMember]
        [DynamicPropertyFilter("Type", "Double")]
        [DisplayName("Число знаков после запятой")]
        [Category("Проверка")]
        public int MaxFloatLength { get; set; }

        String _formulaForCalculating;
		[DynamicPropertyFilter("Type", "Double,Integer")]
		[DisplayName("Формула")]
		[Category("Данные")]
		[DataMember]
		public String FormulaForCalculating 
        {
            get
            {
                return _formulaForCalculating;
            }
            set
            {
                if (_formulaForCalculating != value)
                {
                    _formulaForCalculating = value;
                    FormulaType = ControlModel.FormulaType.Calculate;
                }
            }
        }

        [TypeConverter(typeof(DictionaryCollection))]
        [DataMember]
        DictionaryCollection _items;
        [Browsable(false)]
        public DictionaryCollection Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
            }
        }
        public event EventHandler NameChanged;
        protected void RaiseNameChanged()
        {
            if (NameChanged != null)
                NameChanged(this, null);
        }

        string _name;
        [DataMember]
        [DisplayName("Имя")]
        [Category("Проектирование")]
        public override string Name
        {
            get
            {
                if (String.IsNullOrEmpty(_name))
                    _name = String.Format(TemplateName, new_element_index);
                return _name;
            }
            set
            {
                if (Row != null)
                    nameValidator.Validate(Row.Cells.ToList<BaseControl>(), value, this);
                _name = value;
                RaiseNameChanged();
            }
        }

        CellValidating _cellValidating;
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [DataMember(Order=10)]
        [DisplayName("Условная проверка")]
        [Category("Проверка")]
        [DynamicPropertyFilter("Type", "Double,Integer")]
        public CellValidating CellValidating
        {
            get
            {
                if (_cellValidating == null)
                {
                    _cellValidating = new CellValidating();
                }
                _cellValidating.Refreshing -= _cellValidating_Refreshing;
                _cellValidating.Refreshing += _cellValidating_Refreshing;
                return _cellValidating;
            }
            set
            {
                _cellValidating = value;
            }
        }

        void _cellValidating_Refreshing(object sender, EventArgs e)
        {
            this.Grid.RaiseRefreshing(this);
        }

        public void AddValidators()
        {
            if (this.IsMandatoryToFill)
            {
                /* mahalin 19-09-2013
                * Защита от возможного забития списка валидаторов дублями
                */
                ClearValidators<EmptyValidationItem>();

                if (Type == GridCellType.Double || Type == GridCellType.Integer)
                    this.EditorValidators.Add(new EmptyValidationItem(ValidatorItemType.num));
                else
                    this.EditorValidators.Add(new EmptyValidationItem());
            }
            if (this.Type == GridCellType.String && !String.IsNullOrEmpty(this.Mask.Mask))
            {
                /* mahalin 19-09-2013
                * Защита от возможного забития списка валидаторов дублями
                */
                ClearValidators<RegexValidationItem>();

                this.EditorValidators.Add(new RegexValidationItem(this.Mask));
            }
            if (this.CellValidating.Comparison != ComparisonType.NotSet)
            {
                ClearValidators<FormulaValidationItem>();

                this.EditorValidators.Add(new FormulaValidationItem(this));
            }
        }

        void ClearValidators<ValidationType>()
        {
            List<IValidationItem> list = this.EditorValidators.Where(x => { return x is ValidationType; }).ToList();
            foreach (IValidationItem v in list)
            {
                this.EditorValidators.Remove(v);
            }
        }
    }

    [DataContract]
    public class CellValidating : FilterablePropertyBase, ICloneable
    {
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public event EventHandler Refreshing;
        protected void RaiseRefreshing(object sender)
        {
            if (Refreshing != null)
                Refreshing(sender, null);
        }

        public CellValidating ()
        {
            
        }

        [DataMember]
        [TypeConverter(typeof(EnumTypeConverter))]
        [DisplayName("Тип значения для проверки")]
        public ComparisonValueType ComparisonValueType { get; set; }

        double _validatingCalculationValue;
        [DataMember]
        [Browsable(false)]
        public double ValidatingCalculationValue
        {
            get
            {
                return _validatingCalculationValue;
            }
            set
            {
                if (_validatingCalculationValue != value)
                {
                    _validatingCalculationValue = value;
                    RaiseRefreshing(this);
                }
            }
        }

        String _formulaForValidating;
        [DynamicPropertyFilter("ComparisonValueType", "Formula")]
        [DisplayName("Формула")]
        [PropertyOrder(1)]
        [DataMember(Order = 1)]
        public String FormulaForValidating
        {
            get
            {
                return _formulaForValidating;
            }
            set
            {
                if (_formulaForValidating != value)
                {
                    _formulaForValidating = value;
                }
            }
        }

        [DynamicPropertyFilter("ComparisonValueType", "Value")]
        [DisplayName("Значение для сравнения")]
        [DataMember]
        [PropertyOrder(1)]
        public double? ValueForComparison { get; set; }

        [DisplayName("Сравнение")]
        [DataMember]
        [TypeConverter(typeof(EnumTypeConverter))]
        public ComparisonType Comparison { get; set; }

        [DisplayName("Сообщение при невыполнении условий")]
        [DataMember]
        public string ResultComparisonMessage { get; set; }

        public override string ToString()
        {
            return String.Format("Валидация. Тип сравнения:{0}", Comparison);
        }
    }

    public enum ComparisonValueType
    {
        [Description("Формула")]
        Formula = 0,
        [Description("Число")]
        Value
    }

    public enum FormulaType
    {
        [Description("Расчет по формуле")]
        Calculate = 0,
        [Description("Проверка")]
        Validate
    }

    public enum GridCellType 
    {         
        [Description("Число с плавающей точкой")]
        Double = 0,
        [Description("Метка")]
        Label,
        [Description("Дата")]
        Date,        
        [Description("Текстовое поле")]
        String,
        [Description("Целое число")]
        Integer,
        [Description("Чекбокс")]
        Check,
        [Description("Список")]
        List
       
    }

    public enum ComparisonType
    {
        [Description("не установлено")]
        NotSet = 0,
        [Description("=")]
        Eq,
        [Description("<>")]
        NotEq,
        [Description(">")]
        Gt,
        [Description(">=")]
        Ge,
        [Description("<")]
        Ls,
        [Description("<=")]
        Le
    }

    public enum AggregateFunctions
    {
        [Description("")]
        None = 0,
        [Description("Суммирование")]
        Sum,
        [Description("Среднее")]
        Average,
        [Description("Количество значений")]
        Count
    }



#region collections

    [DataContract]
    public class GridColumnCollection : Collection<GridColumn>
    {
        [DataMember]
        public Grid Grid { get; set; }
        public GridColumnCollection(Grid grid)
        {
            Grid = grid;
        }
        protected override void InsertItem(int index, GridColumn item)
        {
            item.grid = Grid;
            item.Root = Grid.Root;
            base.InsertItem(index, item);
            foreach (GridRow row in Grid.Rows)
            {
                if (row.Cells[index].Column.Name != item.Name)
                {
                    int index1 = GetColumnIndex(row, item);
                    swap(row, index, index1);
                }
                row.Cells[index].Column = item;
            }
        }

        void swap(GridRow row, int index1, int index2)
        {
            try
            {
                GridCellItem cell = row.Cells[index1];
                row.Cells[index1] = row.Cells[index2];
                row.Cells[index2] = cell;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// Для правильной работы у колонок должны быть заданы уникальные имена
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        int GetColumnIndex(GridRow row, GridColumn column)
        {
            for (int i = 0; i < row.Cells.Count; i++)
            {
                if (row.Cells[i].Column.Name == column.Name)
                    return i;
            }
            return -1;
        }

        protected override void SetItem(int index, GridColumn item)
        {
            item.grid = Grid;
            item.Root = Grid.Root;
            base.SetItem(index, item);
        }
        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
        }
    }

    [DataContract]
    public class GridRowCollection : Collection<GridRow>
    {
        [DataMember]
        public Grid Grid { get; set; }
        public GridRowCollection(Grid grid)
        {
            this.Grid = grid;
        }
        protected override void InsertItem(int index, GridRow item)
        {
            item.Grid = Grid;
            item.Root = Grid.Root;
            base.InsertItem(index, item);
        }
        protected override void SetItem(int index, GridRow item)
        {
            item.Grid = Grid;
            item.Root = Grid.Root;
            base.SetItem(index, item);
        }
        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
        }
    }

    [DataContract]
    public class GridCellCollection : Collection<GridCellItem>
    {
        [DataMember]
        public GridRow Row { get; set; }
        public GridCellCollection(GridRow row)
        {
            Row = row;
        }
        protected override void InsertItem(int index, GridCellItem item)
        {
            item.Root = Row.Root;
            base.InsertItem(index, item);
            item.Items = new DictionaryCollection((Row.Grid.Root as Report).MetaData.Dictionarys);
            if (index < Row.Grid.Columns.Count)
                item.Column = Row.Grid.Columns[index];
        }
        protected override void SetItem(int index, GridCellItem item)
        {
            item.Root = Row.Root;
            base.SetItem(index, item);
            if (index < Row.Grid.Columns.Count)
                item.Column = Row.Grid.Columns[index];
        }
        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
        }
    }

#endregion

#region Editors

    public class GridCollectionEditor : System.ComponentModel.Design.CollectionEditor
    {
        protected static Dictionary<CollectionForm, Button> okayButtons = new Dictionary<CollectionForm, Button>();
        protected CollectionForm collectionForm;
        protected Grid grid;

        public GridCollectionEditor(Type type)
            : base(type)
        {
            
        }

        protected override CollectionForm CreateCollectionForm()
        {
            collectionForm = base.CreateCollectionForm();
            
            collectionForm.FormClosed +=
                new FormClosedEventHandler(collectionForm_FormClosed);
            collectionForm.Load += new EventHandler(collectionForm_Load);
            if (collectionForm.Controls.Count > 0)
            {
                TableLayoutPanel mainPanel = collectionForm.Controls[0]
                    as TableLayoutPanel;
                if ((mainPanel != null) && (mainPanel.Controls.Count > 7))
                {
                    // Get a reference to the inner PropertyGrid and hook 
                    // an event handler to it.
                    PropertyGrid propertyGrid = mainPanel.Controls[5]
                        as PropertyGrid;
                    if (propertyGrid != null)
                    {
                        propertyGrid.PropertyValueChanged +=
                            new PropertyValueChangedEventHandler(
                                propertyGrid_PropertyValueChanged);
                        propertyGrid.SelectedObjectsChanged += new EventHandler(propertyGrid_SelectedObjectsChanged);
                    }
                    // Also hook to the Add/Remove
                    TableLayoutPanel buttonPanel = mainPanel.Controls[1]
                        as TableLayoutPanel;
                    if ((buttonPanel != null) && (buttonPanel.Controls.Count > 1))
                    {
                        Button addButton = buttonPanel.Controls[0] as Button;
                        if (addButton != null)
                        {
                            addButton.Click += new EventHandler(addButton_Click);
                        }

                        Button removeButton = buttonPanel.Controls[1] as Button;
                        if (removeButton != null)
                        {
                            removeButton.Click += new EventHandler(removeButton_Click);
                        }
                    }

                    // Find the OK button, and hold onto it.
                    buttonPanel = mainPanel.Controls[6] as TableLayoutPanel;
                    if ((buttonPanel != null) && (buttonPanel.Controls.Count > 1))
                    {
                        Button okayButton = buttonPanel.Controls[0] as Button;
                        if (okayButton != null)
                        {
                            okayButtons[collectionForm] = okayButton;
                        }
                    }

                }

            }

            return collectionForm;
        }

        protected virtual void OnClosing(CollectionForm frm)
        {

        }

        private void collectionForm_FormClosed(object sender,
            FormClosedEventArgs e)
        {
            CollectionForm collectionForm = (CollectionForm)sender;
            OnClosing(collectionForm);
            if (okayButtons.ContainsKey(collectionForm))
            {
                okayButtons.Remove(collectionForm);
            }
        }

        public virtual void collectionForm_Load(object sender, EventArgs e)
        {
            collectionForm.Text = "Таблица с добавлением строк";
            ValidateEditValue((CollectionForm)sender);
        }

        private void propertyGrid_PropertyValueChanged(object sender,
            PropertyValueChangedEventArgs e)
        {
            ValidateEditValue((CollectionForm)(sender as PropertyGrid).ParentForm);
            PropertyGridRefresh();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Button addButton = (Button)sender;
            ValidateEditValue((CollectionForm)addButton.Parent.Parent.Parent);
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            Button removeButton = (Button)sender;
            ValidateEditValue((CollectionForm)removeButton.Parent.Parent.Parent);
        }

        protected virtual void ValidateEditValue(CollectionForm collectionForm)
        {
            
        }

        private void propertyGrid_SelectedObjectsChanged(object sender, EventArgs e)
        {
            PropertyGridRefresh();
        }        

        private void PropertyGridRefresh()
        {
            TableLayoutPanel mainPanel = collectionForm.Controls[0]
                   as TableLayoutPanel;

            PropertyGrid propertyGrid = mainPanel.Controls[5]
                        as PropertyGrid;
            if (propertyGrid.SelectedObject != null)
            {
                try
                {
                    PropertyDescriptor descriptor = TypeDescriptor.GetProperties(propertyGrid.SelectedObject.GetType())["EditValue"];

                    BrowsableAttribute attribute = (BrowsableAttribute)
                                                  descriptor.Attributes[typeof(BrowsableAttribute)];
                    FieldInfo fieldToChange = attribute.GetType().GetField("browsable",
                                                     System.Reflection.BindingFlags.NonPublic |
                                                     System.Reflection.BindingFlags.Instance);
                    fieldToChange.SetValue(attribute, false);

                    propertyGrid.Refresh();
                }
                catch
                {
                }
            }

            propertyGrid.Refresh();
        }
    }

    public class GridColumnEditor : GridCollectionEditor 
    {
        
        public GridColumnEditor(Type type)
            : base(type)
        {
            
        }

        protected override Type CreateCollectionItemType()
        {
            return typeof(GridColumn);
        }

        protected override object CreateInstance(Type itemType)
        {
            grid = (collectionForm.EditValue as GridColumnCollection).Grid;
            GridColumn column = new GridColumn();
            foreach (var row in grid.Rows)
                row.Cells.Add(new GridCellItem() { Column = column, Row = row } );
            
            return column;
        }

        protected override void DestroyInstance(object instance)
        {
            grid = (collectionForm.EditValue as GridColumnCollection).Grid;
            GridColumn col = instance as GridColumn;    

            foreach (GridRow row in grid.Rows)
            {
                for (int k = row.Cells.Count - 1; k >= 0; k-- )
                {
                    if (row.Cells[k].Column == col)
                        row.Cells.RemoveAt(k);
                }
            }   
            base.DestroyInstance(instance);
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            return base.EditValue(context, provider, value);
        }
        
        protected override void ValidateEditValue(CollectionForm collectionForm)
        {
            if (okayButtons.ContainsKey(collectionForm))
            {
                Button okayButton = okayButtons[collectionForm];
                GridColumnCollection items = collectionForm.EditValue as GridColumnCollection;
                bool ok = true;
                for (int i = 0; i < items.Count; i++)
                {
                    if (String.IsNullOrEmpty(items[i].Name))
                    {
                        ok = false;
                        break;
                    }
                }
                okayButton.Enabled = ok; 
            }
        }
    }

    public class GridCellEditor : GridCollectionEditor 
    {
        public GridCellEditor(Type type)
            : base(type)
        {
            
        }

        protected override Type CreateCollectionItemType()
        {
            return typeof(GridCellItem);
        }

        protected override object CreateInstance(Type itemType)
        {
            return new GridCellItem();
        }
        
        protected override CollectionForm CreateCollectionForm()
        {
            collectionForm = base.CreateCollectionForm();

            if (collectionForm.Controls.Count > 0)
            {
                TableLayoutPanel mainPanel = collectionForm.Controls[0]
                    as TableLayoutPanel;
                if ((mainPanel != null) && (mainPanel.Controls.Count > 7))
                {
                    // Also hook to the Add/Remove
                    TableLayoutPanel buttonPanel = mainPanel.Controls[1]
                        as TableLayoutPanel;
                    if ((buttonPanel != null) && (buttonPanel.Controls.Count > 1))
                    {
                        Button addButton = buttonPanel.Controls[0] as Button;
                        if (addButton != null)
                        {
                            addButton.Visible = false;                   
                        }
                        Button removeButton = buttonPanel.Controls[1] as Button;
                        if (removeButton != null)
                        {
                            removeButton.Visible = false;
                        }
                    }
                }
            }
            return collectionForm;
        }

        protected override void ValidateEditValue(CollectionForm collectionForm)
        {
            if (okayButtons.ContainsKey(collectionForm))
            {
                Button okayButton = okayButtons[collectionForm];
                GridCellCollection items = collectionForm.EditValue as GridCellCollection;
                bool ok = true;
                for (int i = 0; i < items.Count; i++)
                {
                    if (String.IsNullOrEmpty(items[i].Name))
                    {
                        ok = false;
                        break;
                    }
	                CalculationHelper tmp = new CalculationHelper();
					if (!tmp.CheckFormula(items[i].FormulaForCalculating, (collectionForm.EditValue as GridCellCollection).Row.Grid))
					{
						ok = false;
						break;
					}
                }
                okayButton.Enabled = ok;
            }
        }

        protected override void OnClosing(System.ComponentModel.Design.CollectionEditor.CollectionForm frm)
        {
            GridCellCollection items = collectionForm.EditValue as GridCellCollection;
            for (int i = 0; i < items.Count; i++)
            {
                CalculationHelper tmp = new CalculationHelper();
                if (!tmp.CheckFormula(items[i].FormulaForCalculating, (collectionForm.EditValue as GridCellCollection).Row.Grid))
                {
                    items[i].FormulaForCalculating = "";
                }
            }
        }
    }

    public class GridRowEditor : GridCollectionEditor 
    {
        public GridRowEditor(Type type)
            : base(type)
        {

        }

        protected override Type CreateCollectionItemType()
        {
            return typeof(GridRow);
        }

        protected override object[] GetItems(object editValue)
        {
            foreach (var row in (editValue as GridRowCollection))
            {
                foreach (var cell in row.Cells)
                {
                    cell.NameChanged -= cell_NameChanged;
                    cell.NameChanged += cell_NameChanged;
                }
            }
            return base.GetItems(editValue);
        }

        

        protected override object CreateInstance(Type itemType)
        {
            GridRow gr = new GridRow();
            //хз почему имена одинаковые получаются без этого
            gr.Name = String.Format("Row_{0}", BaseControl.new_element_index.ToString());
            gr.Caption = String.Format("Row_{0}", BaseControl.new_element_index.ToString());
            Grid grid = (collectionForm.EditValue as GridRowCollection).Grid;
            gr.Root = grid.Root;
            gr.Grid = grid;
            int col = grid.Columns.Count;
            for (int i = 0; i < col; i++)
            {
                GridCellItem cell = new GridCellItem() { Column = grid.Columns[i] };
                cell.Root = grid.Root;
                cell.Row = gr;
                //хз почему имена одинаковые получаются без этого
                cell.Name = String.Format("Cell_{0}" , BaseControl.new_element_index.ToString());
                cell.Name = String.Format("Cell_{0}", BaseControl.new_element_index.ToString());
                cell.NameChanged += new EventHandler(cell_NameChanged);
                gr.Cells.Add(cell);
            }
            return gr;
        }

        void cell_NameChanged(object sender, EventArgs e)
        {
            ValidateEditValue(collectionForm);
        }

        protected override void ValidateEditValue(CollectionForm collectionForm)
        {
            if (okayButtons.ContainsKey(collectionForm))
            {
                Button okayButton = okayButtons[collectionForm];
                GridRowCollection items = collectionForm.EditValue as GridRowCollection;
                bool ok = true;
                for (int i = 0; i < items.Count; i++)
                {
                    if (String.IsNullOrEmpty(items[i].Name))
                    {
                        ok = false;
                        break;
                    }
                    foreach (GridCellItem cell in items[i].Cells)
                    {
                        if (String.IsNullOrEmpty(cell.Name))
                        {
                            ok = false;
                            break;
                        }
                    }
                    if (ok == false)
                        break;
                }
                okayButton.Enabled = ok;

				(collectionForm.EditValue as GridRowCollection).Grid.HeightGrid = items.Count * 21 + 25;
            }
        }
        
    }

#endregion

    
}
