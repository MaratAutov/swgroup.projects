﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using aetp.Common.ControlModel.Validator;
using System.ComponentModel;
using aetp.Common.Mediator;
using System.Collections.Specialized;
using aetp.Common.View;
using DevExpress.XtraTreeList.Nodes;


namespace aetp.Common.ControlModel
{   
    public class Report : Page
    {
        #region Properties
        //для идентификации отчета в статических коллекциях
        string _guid = null;        

        public string guid
        {
            get
            {
                if (_guid == null)
                    _guid = Guid.NewGuid().ToString();
                return _guid;
            }
            set
            {
                _guid = Guid.NewGuid().ToString();
            }
        }

        [DataMember]
        [DisplayName("Устаревшая форма")]
        public bool IsOlder { get; set; }

        // для определение режима у котором используется отчет конструктор / редактор
        [Browsable(false)]
        public ViewType RunMode { get; set; }

        [DataMember]
        [Browsable(false)]
        public override bool IsMandatoryToFill { get; set; }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Report_{0}";
            }
        }
               

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "Report {0}";
            }
        }
        [DataMember]
        [Browsable(false)]
        public override BaseControl ListControl { get; set; }
        [DataMember]
        [Browsable(false)]
        public override bool IsVisible   { get; set;}
        [DataMember]
        [Browsable(false)]
        public override TypeControl type { get; set; }
        [DataMember]
        [Browsable(false)]
        public override Check_State IsChecked { get; set; }
        [DataMember]
        [Browsable(false)]
        public override bool Visible_Page  { get; set; }             


        ReportValidator _validator;
        [DataMember]
        [Browsable(false)]
        public ReportValidator validator 
        {
            get
            {
                if (_validator == null)
                    _validator = new ReportValidator(this);
                return _validator;
            }
        }

        CellValidator _cellvalidator;
        [DataMember]
        [Browsable(false)]
        public CellValidator cellvalidator
        {
            get
            {
                if (_cellvalidator == null)
                    _cellvalidator = new CellValidator(this);
                return _cellvalidator;
            }
        }

        private bool _attachFile;
        [DataMember]
        [DisplayName("Прикрепить файлы")]
        [Category("Файлы")]
        public bool AttachFile
        {
            get
            {
                return _attachFile;
            }
            set
            {
                _attachFile = value;
                
            }
        }


        private bool _attachFileIsMandatory;
        [DataMember]
        [DisplayName("Обязательно прикрепление файлов")]
        [Category("Файлы")]
        public bool AttachFileIsMandatory
        {
            get
            {
                return _attachFileIsMandatory;
            }
            set
            {
                _attachFileIsMandatory = value;               
            }
        }
        

        PrintingForm _printingForm = new PrintingForm();
        [DataMember]
        [DisplayName("Печатная форма")]
        [Category("Печать")]
        public PrintingForm PrintingForm
        {
            get
            {
                 _printingForm.report = this;
                return _printingForm;
               
            }
            set
            {
                _printingForm = value;
            }
        }

        List<FileList> _file = new List<FileList> { };
        [DataMember]
        [Browsable(false)]
        [DisplayName ("файлы")]
        public List<FileList> File
        {
            get
            {
                return _file;
            }
            set
            {
                _file = value;
            }
        }      
        


        private StringCollection _items = new StringCollection();
        [DisplayName("Отслеживаемое значение")]
        [Browsable(false)]
        [Category("Привязка")]
        [DataMember]
        [Editor(
            "System.Windows.Forms.Design.StringCollectionEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a",
            "System.Drawing.Design.UITypeEditor,System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
          )]
        public override StringCollection Items
        {
            get;
            set;
        }

        [DataMember]
        [Browsable(false)]
        public ReportMetaData MetaData { get; set; }

        EditorViewSerializerConfig _serialaizerConfig;
        [DisplayName("Настройка сериализации")]
        [Category("Сериализация")]
        [DataMember]
        public EditorViewSerializerConfig SerialaizerConfig
        {
            get
            {
                if (_serialaizerConfig == null)
                    _serialaizerConfig = new EditorViewSerializerConfig();
                return _serialaizerConfig;
            }
            set
            {
                _serialaizerConfig = value;
            }
        }

        #endregion

        #region event

        public delegate void EventHandlerReporChanged(object sender, EventHandlerReporChangedArgs args);
        public event EventHandlerReporChanged Changed;
        protected void RaiseChanged()
        {
            if (Changed != null)
                Changed(this, new EventHandlerReporChangedArgs());
        }
                
        #endregion

        #region Constructors

        public Report()
        {
            new_element_index = 1;
            MetaData = new ReportMetaData();
            Children = new List<BaseControl>();
        }
        
        #endregion

        #region function implementation
       
        public void InitSouceMetaControlHandler()
        {
            InitSouceMetaControlHandler(this);
        }

        public void InitSouceMetaControlHandler(Page page)
        {
            ReportUtils.DoAction(page.Children, x =>
            {
                if (x is MetaConrol)
                {
                    var meta = x as MetaConrol;
                    if (meta.ListControl != null)
                    {
                        List<MetaConrol> metas = null;
                        if (meta.ListControl.Tag == null) metas = new List<MetaConrol>();
                        else metas = meta.ListControl.Tag as List<MetaConrol>;
                        metas.Add(meta);
                        meta.ListControl.Tag = metas;
                        meta.ListControl.PropertyChanged += new PropertyChangedEventHandler(ListControl_PropertyChanged);
                    }
                }
            });
        }

        void ListControl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var bc = sender as BaseControl;
            if (bc.Tag is List<MetaConrol>)
            {
                var metas = bc.Tag as List<MetaConrol>;
                if (bc is SimpleEdit && (bc as SimpleEdit).DataType == SimpleDataType.Bool)
                {
                    SetVisibleMetaControl(metas, bc as SimpleEdit);
                }
                else if (bc is aetp.Common.ControlModel.ComboBox)
                {
                    SetVisibleMetaControl(metas, bc as ComboBox);
                    //AddHandlerComboBox_Changed(ctrl as aetp.Common.ControlModel.ComboBox);
                }
            }
        }

        void SetVisibleMetaControl(List<MetaConrol> metas, SimpleEdit se)
        {
            foreach (var metaControl in metas)
            {
                if (se.EditValue != null)
                {
                    if (se.EditValue.ToString().Trim() == metaControl.IsChecked.ToString().Trim())
                    {
                        metaControl.Visible = true;
                        //SetVisibleTrueChildrenMetaControl(metaControl);
                    }
                    else if ((metaControl.IsChecked.ToString().Trim() == "None") && (se.EditValue.ToString().Trim() != "False") && (se.EditValue.ToString().Trim() != "True"))
                    {
                        metaControl.Visible = true;
                        //SetVisibleTrueChildrenMetaControl(metaControl);
                    }
                    else
                    {
                        metaControl.Visible = false;
                        //SetVisibleFalseChildrenMetaControl(metaControl);
                    }
                }
                else if (se.EditValue == null)
                {
                    if (metaControl.IsChecked.ToString().Trim() == "None")
                    {
                        metaControl.Visible = true;
                        //SetVisibleTrueChildrenMetaControl(metaControl);
                    }
                    else
                    {
                        metaControl.Visible = false;
                        //SetVisibleFalseChildrenMetaControl(metaControl);
                    }
                }
            }
            //SetVisibleMetaControlPage(metaControl, metaControl.Visible);
        }

        private void SetVisibleMetaControl(List<MetaConrol> metas, ComboBox se)
        {
            foreach (var metaControl in metas)
            {
                if (se.EditValue != null)
                {
                    if (metaControl.ItemsType == ItemsTypeMode.IsSet)
                    {
                        bool visible = se != null &&
                            se.EditValue != DBNull.Value;
                        SetMetaControlVisible(metaControl, visible);
                    }
                    else
                    {
                        if (metaControl.Items != null)
                            SetMetaControlVisible(metaControl, metaControl.Items.Contains(ReportUtils.GetComboBoxValue(se.EditValue).Code.Trim()));
                    }
                }
                else
                {
                    SetMetaControlVisible(metaControl, false);
                }
            }
        }

        void SetMetaControlVisible(aetp.Common.ControlModel.MetaConrol metaControl, bool visible)
        {
            if (visible)
            {
                metaControl.Visible = true;
                //pc.Visible = metaControl.Visible;
                //SetVisibleTrueChildrenMetaControl(metaControl);
            }
            else
            {
                metaControl.Visible = false;
                ReportUtils.DoAction(metaControl.Children, x => { if (x.IsCleaningOnUnvisible) if (x is SimpleEdit) x.EditValue = (x as SimpleEdit).DefaultValue; else x.EditValue = null; });
                //pc.Visible = metaControl.Visible;
                //SetVisibleFalseChildrenMetaControl(metaControl);
            }
            //SetVisibleMetaControlPage(metaControl, metaControl.Visible);
        }

        //private void SetVisibleFalseChildrenMetaControl(MetaConrol meta)
        //{
        //    FlowLayoutPanel pc = new FlowLayoutPanel();
        //    foreach (aetp.Common.ControlModel.BaseControl control in meta.Children)
        //    {
        //        if (control is aetp.Common.ControlModel.ComboBox)
        //        {
        //            foreach (aetp.Common.ControlModel.BaseControl ctrl in basecontrols)
        //            {
        //                if ((ctrl is MetaConrol) && ((ctrl as MetaConrol).ListControl != null) && ((ctrl as MetaConrol).ListControl.Name != null) && ((ctrl as MetaConrol).ListControl == control))
        //                {
        //                    foreach (FlowLayoutPanel pn in panels)
        //                    {
        //                        if (pn.Name == ctrl.Name)
        //                        {
        //                            pc = pn;
        //                            break;
        //                        }
        //                    }
        //                    pc.Visible = false;
        //                }
        //            }
        //        }
        //        if (control is aetp.Common.ControlModel.SimpleEdit)
        //        {
        //            if ((control as aetp.Common.ControlModel.SimpleEdit).DataType == SimpleDataType.Bool)
        //            {
        //                foreach (aetp.Common.ControlModel.BaseControl ctrl in basecontrols)
        //                {
        //                    if ((ctrl is MetaConrol) && ((ctrl as MetaConrol).ListControl != null) && ((ctrl as MetaConrol).ListControl.Name != null) && ((ctrl as MetaConrol).ListControl == control))
        //                    {
        //                        foreach (FlowLayoutPanel pn in panels)
        //                        {
        //                            if (pn.Name == ctrl.Name)
        //                            {
        //                                pc = pn;
        //                                break;
        //                            }
        //                        }
        //                        pc.Visible = false;
        //                    }
        //                }
        //            }
        //        }
        //        if (control is MetaConrol)
        //        {
        //            SetVisibleFalseChildrenMetaControl(control as MetaConrol);
        //        }
        //    }
        //}


        //private void SetVisibleTrueChildrenMetaControl(MetaConrol meta)
        //{
        //    FlowLayoutPanel pc = new FlowLayoutPanel();
        //    foreach (aetp.Common.ControlModel.BaseControl control in meta.Children)
        //    {
        //        if (control is aetp.Common.ControlModel.ComboBox)
        //        {
        //            ComboBoxValueChanged(control as aetp.Common.ControlModel.ComboBox);
        //        }
        //        if (control is aetp.Common.ControlModel.SimpleEdit)
        //        {
        //            Check_Change(control as aetp.Common.ControlModel.SimpleEdit);
        //        }
        //        if (control is MetaConrol)
        //        {
        //            if ((control as MetaConrol).Visible)
        //                SetVisibleTrueChildrenMetaControl(control as MetaConrol);
        //        }
        //    }
        //}


        //private void SetVisibleMetaControlPage(MetaConrol meta, bool pr)
        //{
        //    foreach (aetp.Common.ControlModel.BaseControl ctrl2 in meta.Children)
        //    {
        //        if (ctrl2 is GridAddedPage)
        //        {
        //            foreach (GridPage page in (ctrl2 as GridAddedPage).Children)
        //            {
        //                if ((page as aetp.Common.ControlModel.BaseControl).tlItem != null)
        //                {
        //                    TreeListNode node = (page as aetp.Common.ControlModel.BaseControl).tlItem;
        //                    node.Visible = pr;
        //                }
        //            }
        //        }
        //    }

        //} 
        #endregion
        public override object Clone()
        {
            Report report = (Report)this.MemberwiseClone();
            
            report.tlItem = null;
            report.Children = new List<BaseControl>();
            if (Children.Count > 0)
            {
                ChildrenClone(report, Children, this);
            }

            return report;
        }
        

        protected virtual void ChildrenClone(BaseControl parent, List<BaseControl> children,  BaseControl root)
        {
            foreach (BaseControl control in children)
            {
                BaseControl newControl = (BaseControl)control.Clone();
                newControl.tlItem = null;
                parent.AddChildren(newControl, root);
                control.Children = new List<BaseControl>();
                if (control.Children.Count > 0)
                {
                    
                    ChildrenClone(newControl, control.Children, root);
                }
            }
        }
    }

    public class EventHandlerReporChangedArgs
    {
        BaseControl control { get; set; }
    }

    [DataContract]
    [Browsable (false)]
    public class FileCollection 
    {
        [DataMember]
        List<string> list;

        IList List
        {
            get
            {
                return list;
            }
        }
        public FileCollection(List<string> range)
        {
            this.list = range;
        }

        public FileCollection()
        {
            list = new List<string>();
        }
        
        public void Add(string value)
        {
            list.Add(value as string);
        }

        public void Clear()
        {
            list.Clear();
        }
    }

    public class FileList
    {
        private string name = string.Empty;
        private string data = string.Empty;
        private string tag = string.Empty;

        public FileList(string name, string data,string tag)
        {
            this.name = name;
            this.data = data;
            this.tag = tag;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string Data
        {
            get
            {
                return this.data;
            }
            set
            {
                this.data = value;
            }
        }

        public string Tag
        {
            get
            {
                return this.tag;
            }
            set
            {
                this.tag = value;
            }
        }

    }
}

