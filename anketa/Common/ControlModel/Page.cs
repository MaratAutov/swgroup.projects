﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;
using aetp.Common.ControlModel.Attributes;
using aetp.Common.ControlModel.TypeConverters;
using System.Runtime.Serialization;
using System.Collections.Specialized;
using aetp.Common.View;

namespace aetp.Common.ControlModel
{
    [TypeConverter(typeof(LinkPageConverter))]
    [DataContract]
    public class Page : BaseControl, ICloneable, aetp.Common.ControlModel.IListConrol
    {
        [Browsable(false)]
        [DataMember]
        public string ParentPrefix { get; set; }


        public bool _designerSave;
        [Browsable(false)]
        [DataMember]
        public bool DesignerSave
        {
            get
            {
                return _designerSave;
            }
            set
            {
                _designerSave = value;
            }
        }

        public bool _isMandatoryElementExist;
        [Browsable(false)]
        [DataMember]
        public bool IsMandatoryElementExist
        {
            get
            {
                return _isMandatoryElementExist;
            }
            set
            {
                _isMandatoryElementExist = value;
            }
        }

        //bool _isShownPanel = true;
        //[Browsable(false)]
        //public bool IsShownPanel
        //{
        //    get
        //    {
        //        return _isShownPanel;
        //    }
        //    set
        //    {
        //        _isShownPanel = value;
        //    }
        //}


        public bool _editorSave;
        [Browsable(false)]
        [DataMember]
        public bool EditorSave
        {
            get
            {
                return _editorSave;
            }
            set
            {
                _editorSave = value;
            }
        }

        public event EventHandler Report_Change;
        protected void RaiseReportChange()
        {
            if (Report_Change != null)
                Report_Change(this, null);
        }

        public bool _reportChange;
        [Browsable(false)]
        [DataMember]
        public bool ReportChange
        {
            get
            {
                return _reportChange;
            }
            set
            {
                _reportChange = value;
                RaiseReportChange();
            }
        }

        [Browsable(false)]
        [DataMember]
        public int Index { get; set; }

        public override bool CanContains(Type t)
        {
            return true;
        }
                        
        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "Page_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "Page {0}";
            }
        }

        public Page()
        {
            Visible_Page = true;
            DesignerSave = false;
            EditorSave = false;
            ReportChange = false;
            //RaiseReportChange();          
        }


        public BaseControl _element;
        
        [DisplayName("Имя управл.контрола")]
        [PropertyOrder(20)]
        [TypeConverter(typeof(ListControlConverter))]
        [Category("Привязка")]
        [DataMember(Order=20)]
        public virtual BaseControl ListControl
        {
            get
            {
                return _element;
            }
            set
            {
                _element = value;
            }
        }

        [DisplayName("Свойство")]
        [PropertyOrder(40)]
        [TypeConverter(typeof(CheckedTypeConverter))]
        [Category("Привязка")]
        [DataMember]
        public virtual bool IsVisible
        {
            get;
            set;
        }


        TypeControl _type;
        [DisplayName("Тип управл.контрола")]
        [PropertyOrder(10)]
        [TypeConverter(typeof(EnumTypeConverter))]
        [Category("Привязка")]
        [DataMember(Order=10)]
        public virtual TypeControl type
        {
            get
            {
                return _type;
            }
            set
            {
                if (value == _type)
                    return;
                ListControl = null;
                _type = value;
            }
        }

        [DynamicPropertyFilter("type", "check")]
        [PropertyOrder(30)]
        [DisplayName("Отслеживаемое значение")]
        [Category("Привязка")]
        [TypeConverter(typeof(EnumTypeConverter))]
        [DataMember]
        public virtual Check_State IsChecked
        {
            get;
            set;
        }


        private StringCollection _items = new StringCollection();
        [DynamicPropertyFilter("type", "combo")]        
        [DisplayName("Отслеживаемое значение")]
        [Category("Привязка")]
        [DataMember]
        [Editor(
            "System.Windows.Forms.Design.StringCollectionEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a",
            "System.Drawing.Design.UITypeEditor,System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
          )]
        public virtual StringCollection Items
        {
            get { return _items; }
            set { _items = value; }
        }

        [DisplayName("Видимость страницы")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [Category("Внешний вид")]
        [DataMember]
        public virtual bool Visible_Page { get; set; }

        bool? _visible = null;
        [Browsable(false)]
        [DisplayName("Видна ли страница в дереве страниц")]
        public virtual bool IsShownPanel
        {
            get
            {
                if (_visible == null)
                    return IsMandatoryToFill;
                else
                    return (bool)_visible;
            }
            set
            {
                if (_visible != value)
                {
                    _visible = value;
                }
            }
        }

        public override object Clone()
        {
            Page newPage = (Page)this.MemberwiseClone();
            newPage.Children = new List<BaseControl>();            
            newPage.Items = new StringCollection();
            foreach (string str in this.Items)
            {
                newPage.Items.Add(str);
            }
            foreach (var child in this.Children)
            {
                BaseControl childClone = (BaseControl)child.Clone(newPage);
                childClone.Parent = newPage;
                newPage.Children.Add(childClone);
            }
            CloneListControl(newPage);
            return newPage;
        }
        
        public void CloneListControl(IListConrol newControl)
        {
            if (this.ListControl != null)
            {
                newControl.ListControl = ReportUtils.GetControlByNameUp(newControl as BaseControl, newControl.ListControl.Name, (newControl as BaseControl).Root);
            }
            ReportUtils.DoAction((newControl as BaseControl).Children, (bs) => { if (bs is IListConrol) (bs as IListConrol).CloneListControl(bs as IListConrol); });
        }

        public bool IsCreatable()
        {
            return !IsShownPanel && !IsMandatoryToFill;
        }

        public bool IsClearable()
        {
            return IsShownPanel && !IsMandatoryToFill;
        }

        public void InitialDefaultValues()
        {
            ReportUtils.DoAction(this.Children, x =>
            {
                x.PropertyChanged += new PropertyChangedEventHandler(x_PropertyChanged);
                if (x is SimpleEdit)
                {
                    var editor = x as SimpleEdit;
                    if (editor.DefaultValue != null)
                    {
                        if (editor.DefaultValue is aetp.Common.ControlModel.UITypeEditors.OrganizationDataItem)
                        {
                            editor.EditValue = DefaultValueControl.GetDefaultValue(editor.DefaultValue.ToString());
                        }
                        else
                        {
                            x.EditValue = editor.DefaultValue;
                        }
                    }
                }
                else if (x is GridCellItem)
                {
                    var editor = x as GridCellItem;
                    if (editor.DefaultValue != null)
                    {
                        if (editor.DefaultValue is aetp.Common.ControlModel.UITypeEditors.OrganizationDataItem)
                        {
                            editor.EditValue = DefaultValueControl.GetDefaultValue(editor.DefaultValue.ToString());
                        }
                        else
                        {
                            x.EditValue = editor.DefaultValue;
                        }
                    }
                }
                else if (x is ComboBox)
                {
                    var editor = x as ComboBox;
                    if (editor.DefaultValue != null)
                    {
                        var item = (from DictionaryListItem i in editor.Source.Items where i.Code == editor.DefaultValue.ToString() select i).FirstOrDefault();
                        x.EditValue = item;
                    }
                }
                else if (x is MemoEdit)
                {
                    var editor = x as MemoEdit;
                    if (editor.DefaultValue != null)
                    {
                        if (editor.DefaultValue is aetp.Common.ControlModel.UITypeEditors.OrganizationDataItem)
                        {
                            editor.EditValue = DefaultValueControl.GetDefaultValue(editor.DefaultValue.ToString());
                        }
                        else
                        {
                            x.EditValue = editor.DefaultValue;
                        }
                    }
                } 
            });
        }

        void x_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "EditValue")
            {
                this.ReportChange = true;
            }
        }
    }   

    public enum VisiblePage
    {
        [Description("Да")]
        Visible = 0,
        [Description("Нет")]
        Invisible = 1
    }
}
