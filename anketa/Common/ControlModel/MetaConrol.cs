﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using aetp.Common.ControlModel.TypeConverters;
using System.Reflection.Emit;
using System.Reflection;
using aetp.Common.Validator;
using aetp.Common.ControlModel.Attributes;
using System.Drawing.Design;
using aetp.Common.ControlModel.UITypeEditors;
using System.Runtime.Serialization;
using System.Collections.Specialized;

namespace aetp.Common.ControlModel
{
    [DataContract]
    [TypeConverter(typeof(PropertySorter))]
    public class MetaConrol: BaseControl, ICloneable, aetp.Common.ControlModel.IListConrol
    {
        [DataMember]
        [Browsable(false)]
        public override bool IsMandatoryToFill { get; set; }

        public override bool CanContains(Type t)
        {
            return (t == typeof(ComboBox) || t == typeof(MetaGrid) || t == typeof(SimpleEdit) || t == typeof(Grid) || t == typeof(GridAddedPage) || t == typeof(GridAddedRow) || t == typeof(MetaConrol) || t == typeof(SimpleRichEdit) || t == typeof(SimpleLabel));
        }        

        public BaseControl _element;        

        [DisplayName("Имя управл.контрола")]
        [PropertyOrder(20)]
        [TypeConverter(typeof(ListControlConverter))]
        [Category("Привязка")]
        [DataMember(Order=20)]
        public BaseControl ListControl 
        {   
            get
            {
                return _element;                    
            }
            set
            {
                _element = value;
            }
        }
        //используется в редакторе для проверки видимости метаконтрола
        bool _visible;
        [DataMember]
        [Browsable(false)]
        public bool Visible 
        {
            get
            {
                return _visible;
            }
            set
            {
                if (_visible != value)
                {
                    _visible = value;
                    NotifyPropertyChanged("Visible");
                }
            }
        }


        //панель для метаэлемента        
        [Browsable(false)]
        public System.Windows.Forms.FlowLayoutPanel Panel { get; set; }

        [DataMember]
        [DisplayName("Свойство")]
        [PropertyOrder(40)]
        [TypeConverter(typeof(EnumTypeConverter))]
        [Category("Привязка")]
        public PropertiesElement Properties
        {
            get;
            set;
        }
        
        TypeControl _type;
        [DataMember(Order=10)]
        [DisplayName("Тип управл.контрола")]
        [PropertyOrder(10)]
        [TypeConverter(typeof(EnumTypeConverter))]
        [Category("Привязка")]
        public TypeControl type
        {
            get
            {
                return _type;
            }
            set
            {
                if (value == _type)
                    return;
                ListControl = null;
                _type = value;
            }
        }



        [DataMember]
        [DynamicPropertyFilter("type", "check")]
        [PropertyOrder(30)]
        [DisplayName("Отслеживаемое значение")]
        [Category("Привязка")]        
        [TypeConverter(typeof(EnumTypeConverter))]
        public Check_State IsChecked
        {
            get;
            set;
        }

        ItemsTypeMode _itemsType = ItemsTypeMode.Values;
        [DataMember]
        [DynamicPropertyFilter("type", "combo")]
        [PropertyOrder(30)]
        [DisplayName("Значение")]
        [Category("Привязка")]
        [TypeConverter(typeof(EnumTypeConverter))]
        public ItemsTypeMode ItemsType
        {
            get
            {
                return _itemsType;
            }
            set
            {
                _itemsType = value;
            }
        }
                
        private StringCollection _items = new StringCollection();
        [DataMember]
        [DynamicPropertyFilter("ItemsType", "Values")]
        
        [PropertyOrder(30)]
        [DisplayName("Отслеживаемое значение")]
        [Category("Привязка")]
        [Editor(
        "System.Windows.Forms.Design.StringCollectionEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a",
        "System.Drawing.Design.UITypeEditor,System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
      )]
        public StringCollection Items
        {
            get { return _items; }
            set { _items = value; }
        }

        [Browsable(false)]
        protected override string TemplateName
        {
            get
            {
                return "MetaControl_{0}";
            }
        }

        [Browsable(false)]
        protected override string TemplateCaption
        {
            get
            {
                return "MetaControl {0}";
            }
        }

        

        public override object Clone()
        {
            MetaConrol newMetaControl = (MetaConrol)this.MemberwiseClone();
            newMetaControl.Children = new List<BaseControl>();
            newMetaControl.Items = new StringCollection();
            foreach (string str in this.Items)
            {
                newMetaControl.Items.Add(str);
            }
            foreach (var child in this.Children)
            {
                BaseControl childClone = (BaseControl)child.Clone(newMetaControl);
                childClone.Parent = newMetaControl;
                newMetaControl.Children.Add(childClone);
            }
            CloneListControl(newMetaControl as IListConrol);
            return newMetaControl;
        }

        public void CloneListControl(IListConrol newControl)
        {
            if (this.ListControl != null)
            {
                newControl.ListControl = ReportUtils.GetControlByNameUp(newControl as BaseControl, newControl.ListControl.Name, (newControl as BaseControl).Root);
                var oldMetas = newControl.ListControl.Tag as List<MetaConrol>;
                var newMetas = ReportUtils.GenerateNewList(oldMetas);
                if (!newMetas.Contains(newControl)) newMetas.Add(newControl as MetaConrol);
                newControl.ListControl.Tag = newMetas;
            }
            ReportUtils.DoAction((newControl as BaseControl).Children, (bs) => { if (bs is IListConrol) (bs as IListConrol).CloneListControl(bs as IListConrol); });
        }

        

        public MetaConrol()
        {
            
        }

        
    }
    
    public enum ItemsTypeMode
    {
        [Description("Значение")]
        Values = 0,
        [Description("Установлено")]
        IsSet = 1,
    }

    public enum PropertiesElement
    {
        [Description("Visible")]
        visible = 0
    }

    public enum TypeControl
    {
        [Description("ComboBox")]
        combo = 0,
        [Description("CheckBox")]
        check
    }

    public enum Check_State
    {
        [Description("Включено")]
        True = 0,
        [Description("Выключено")]
        False,
        [Description("Не определено")]
        None
    }
    
}
