﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using aetp.Common.View;
using System.Windows.Forms;
using aetp.Utils;
using aetp.Common.Print;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Localization;
using DevExpress.XtraPrintingLinks;
using aetp.Common.Control;
using System.Drawing;
using aetp.Common.Mediator;
using System.Data;
using DevExpress.XtraGrid.Views.Grid;
using System.Text.RegularExpressions;
using NCalc;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid;
using System.Diagnostics;

namespace aetp.Common
{
    public class Printing
    {
        EditorMediator mediator;
        Report Report
        {
            get
            {
                return mediator.Report;
            }
        }
        EditorView View
        {
            get
            {
                return mediator.View as EditorView;
            }
        }

        string footer = "[Страница # из # страниц]";
        public Printing(EditorMediator mediator)
        {
            this.mediator = mediator;
            PreviewLocalizer.Active = new RussianPrintLocalizer();
        }

        List<string> list_meta;
        void FindHiddenMetaControl(BaseControl ctrl)
        {
            if (ctrl is MetaConrol)
                if ((ctrl as MetaConrol).Visible == false)
                    list_meta.Add(ctrl.Name);
        }

        private void AddMainPageHeaderFooter(CompositeLink MainPage, string reportName, int paragraph,float size)
        {
            MainPage.Links.Add(new TextLink(new String('\n', paragraph), "Times New Roman"));
            TextLink link_header = new TextLink(reportName, "Times New Roman");
            link_header.FontStyle = FontStyle.Bold;
            link_header.emSize = size;
            link_header.HAlignment = StringAlignment.Center;
            MainPage.Links.Add(link_header);
            MainPage.Links.Add(new TextLink(new String('\n', paragraph), "Times New Roman"));
        }


        private void PrintSetting(List<ControlModel.Page> PagePanels, ExportEnum export_option, string filename, bool AddHeaderFooter)
        {
            string reportName = Report.Caption.ToUpper();

            PrintingSystem ps = new PrintingSystem();
            CompositeLink MainPage = new CompositeLink(ps);
            MainPage.Landscape = false;
            MainPage.PaperKind = System.Drawing.Printing.PaperKind.A4;

            List<BaseControl> collection = new List<BaseControl>();
            List<BaseControl> panel_collection = new List<BaseControl>();
            list_meta = new List<string>();

            PageHeaderFooter phf = MainPage.PageHeaderFooter as PageHeaderFooter;

            phf.Header.Content.Clear();
            phf.Header.Content.Add(reportName);

            if (AddHeaderFooter)
            {
                AddMainPageHeaderFooter(MainPage, reportName, 5, 16);
            }

            foreach (ControlModel.Page page in PagePanels)
            {
                collection.Clear();
                list_meta.Clear();

                if (page.IsShownPanel)
                {
                    ReportUtils.DoAction(page.Children, FindHiddenMetaControl);

                    collection.AddRange(this.GetAllControls(page.Children, list_meta));

                    if (page.Parent != null)
                    {
                        AddMainPageHeaderFooter(MainPage, page.Caption, 1, 12);
                    }

                    foreach (BaseControl control in collection)
                    {
                        if (control is BaseGrid)
                        {
                            string Text = string.Empty;
                            if ((control is BaseGrid) && (!(control as BaseGrid).HideCaption))
                            {
                                Text = (control as BaseGrid).Caption;
                            }

                            if (Text.Trim().Length != 0)
                            {
                                LinkBase lb_caption = GetLinkHeaderTable(Text);
                                if (lb_caption != null)
                                {
                                    ps.Links.Add(lb_caption);
                                    MainPage.Links.Add(lb_caption);
                                    MainPage.Links.Add(new TextLink("\n", "Times New Roman"));
                                }
                            }
                        }

                        List<LinkBase> lbs = GetLink(control, 11, PrintingFont.TNR);
                        foreach (var lb in lbs)
                        {
                            if (lb != null)
                            {
                                ps.Links.Add(lb);
                                MainPage.Links.Add(lb);
                            }
                        }
                    }

                    DevExpress.XtraPrinting.Link NewPage = new DevExpress.XtraPrinting.Link();
                    MainPage.Links.Add(new TextLink("\n", "Times New Roman"));
                    MainPage.Links.Add(NewPage);
                }
            }

            if (AddHeaderFooter)
                MainPage.Links.Add(new SignLink());

            phf.Footer.Content.Clear();
            phf.Footer.Content.Add(footer);
            phf.Footer.LineAlignment = BrickAlignment.Far;
            MainPage.CreateDocument();

            switch (export_option)
            {
                case ExportEnum.Preview: ps.PreviewRibbonFormEx.ShowDialog(); break;
                case ExportEnum.Pdf:
                    {
                        ps.ExportToPdf(filename);
                        OpenFileExport(filename);
                        break;
                    }
                case ExportEnum.Rtf:
                    {
                        ps.ExportToRtf(filename);
                        OpenFileExport(filename);
                        break;
                    }
                case ExportEnum.Excel:
                    {
                        ps.ExportOptions.Xls.TextExportMode = TextExportMode.Text;
                        ps.ExportToXls(filename);
                        OpenFileExport(filename);
                        break;
                    }
            }
        }

        public static LinkBase GetLinkHeaderTable(string caption)
        {
            return new TextLink(string.Format("{0}", caption), "Times New Roman");
        }

        public static string GetFont(PrintingFont font)
        {
            string font_family = string.Empty;
            if (font == PrintingFont.Courier)
                font_family = "Courier New";
            else if (font == PrintingFont.Arial)
            {
                font_family = "Arial";
            }
            else font_family = "Times New Roman";
            return font_family;
        }

        //печать элемента Фиксированная Таблица и Таблица С добавлением строк
        public static LinkBase PrintGrid(DataTable dt, Font font, bool hide_header, aetp.Common.ControlModel.BaseGrid gc)
        {
            PrintableComponentLink link = new PrintableComponentLink();
            GridControl grid = GetGrid(dt, font, hide_header);
            GridView gv = grid.MainView as GridView;
            Grid fixgrid = null;
            GridAddedRow gridAddedRow = null;
            GridAddedPage gridAddedPage = null;
            gv.ColumnPanelRowHeight = gc.HeightHeaderGrid;
            if (gc is Grid)
            {
                fixgrid = gc as Grid;
            }
            else if (gc is GridAddedRow)
            {
                gridAddedRow = gc as GridAddedRow;
            }
            else
            {
                gridAddedPage = gc as GridAddedPage;    
            }
            for (int i = 0; i < gv.Columns.Count; i++)
            {
                if (fixgrid != null)
                {
                    gv.Columns[i].Caption = fixgrid.Columns[i].Caption;
                    gv.Columns[i].Width = fixgrid.Columns[i].WidthColumn;
                }
                else if (gridAddedRow != null)
                {
                    gv.Columns[i].Caption = gridAddedRow.Columns[i].Caption;
                    gv.Columns[i].Width = gridAddedRow.Columns[i].WidthColumn;
                }
                else if (gridAddedPage != null)
                {
                    gv.Columns[i].Caption = gridAddedPage.Columns[i].Caption;
                    gv.Columns[i].Width = gridAddedPage.Columns[i].WidthColumn;
                }
            }
            
            link.Component = grid;
            return link;
        }

        static GridControl GetGrid(DataTable dt, Font font, bool hide_header)
        {
            DevExpress.XtraGrid.GridControl grid = new DevExpress.XtraGrid.GridControl();
            DevExpress.XtraGrid.Views.Grid.GridView view = new DevExpress.XtraGrid.Views.Grid.GridView();

            grid.Name = "grid";
            grid.MainView = view;
            grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { view });
            grid.BindingContext = new BindingContext();

            DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit memo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            memo.Appearance.Options.UseTextOptions = true;
            memo.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            memo.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            memo.Name = "repo1";
            grid.RepositoryItems.Add(memo);

            view.OptionsView.ColumnAutoWidth = false;
            view.OptionsView.RowAutoHeight = true;

            view.Name = "view";

            view.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            view.Appearance.Row.Font = font;

            view.OptionsView.ShowColumnHeaders = !hide_header;
            view.OptionsPrint.PrintHeader = !hide_header;
            view.Appearance.HeaderPanel.Font = font;
            view.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            view.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.Transparent;
            view.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.Transparent;
            view.AppearancePrint.HeaderPanel.BackColor = Color.Black;
            view.AppearancePrint.HeaderPanel.BorderColor = Color.White;
            view.AppearancePrint.HeaderPanel.ForeColor = Color.White;
            view.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            view.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            view.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            //view.ColumnPanelRowHeight = gv.ColumnPanelRowHeight + 10;
            view.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            view.OptionsView.ColumnAutoWidth = false;

            int count = 0;
            foreach (DataColumn dc in dt.Columns)
            {
                DevExpress.XtraGrid.Columns.GridColumn col = new DevExpress.XtraGrid.Columns.GridColumn();
                col.Name = dc.Caption;
                col.FieldName = dc.Caption;
                col.ColumnEdit = memo;

                view.Columns.Add(col);
                view.Columns[count].Visible = true;
                view.Columns[count].VisibleIndex = count;
                count++;
            }

            grid.Width = 1200;
            view.GridControl = grid;
            grid.ForceInitialize();

            grid.DataSource = dt;
            return grid;
        }

        public static string FormatDoubleForPrint(string value)
        {
            string temp = value;

            if (!String.IsNullOrEmpty(value))
            {
                int length = value.Length - 3;
                temp = value;

                if (value.Contains(","))
                {
                    length = value.IndexOf(',') - 3;
                }

                while (length > -1)
                {
                    temp = temp.Insert(length, " ");
                    length -= 3;
                }                
            }
            return temp;
        }

        

        public static List<LinkBase> GetLink(BaseControl control, float text_size, PrintingFont font_item, bool calcOptimal = true)
        {
            Font font = new Font(GetFont(font_item), text_size, FontStyle.Regular);
            List<LinkBase> result = new List<LinkBase>();
            if (control is Grid)
            {
                Grid grid = control as Grid;
                if (grid.Data != null)
                {
                    result.Add(new GridLink(grid, font, calcOptimal));                    
                }
                return result;
            }
            else if (control is GridAddedRow)
            {
                GridAddedRow grid = control as GridAddedRow;
                if (grid.Data != null)
                result.Add(new GridAddedRowLink(grid, font, calcOptimal));
                return result;
            }
            else if (control is GridAddedPage)
            {                
                GridAddedPage grid = control as GridAddedPage;
                if (grid.Data != null)
                result.Add(new GridAddedPageLink(grid, font, calcOptimal));
                return result;
            }
            else if (control is MetaGrid) 
            {
                foreach (var table in (control as MetaGrid).Tables)
                {
                    result.AddRange(GetLink(table, text_size, font_item, false));
                }
                return result;
            }
            else
            {
                string Caption = string.Empty;
                string Text = string.Empty;

                Caption = (control as BaseControl).Caption;

                if ((control is SimpleEdit) && (control as SimpleEdit).EditValue != null && (control as SimpleEdit).EditValue != DBNull.Value)
                {                    
                    if ((control as SimpleEdit).DataType == SimpleDataType.Bool)
                    {
                        bool value = (bool)(control as SimpleEdit).EditValue;
                        if (value == true)
                            Text = "Да";
                        else
                            Text = "Нет";                        
                    }
                    else if ((control as SimpleEdit).DataType == SimpleDataType.Double)
                    {                        
                        Text = (control as BaseControl).EditValue.ToString().Replace(".", ",");
                        Text = FormatDoubleForPrint(Text);                       
                    }
                    else if ((control as SimpleEdit).DataType == SimpleDataType.DateTime)
                    {                        
                        Text = Convert.ToDateTime((control as SimpleEdit).EditValue).ToShortDateString();                       
                    }
                    else if ((control as SimpleEdit).DataType == SimpleDataType.Integer)
                    {
                        Text = control.EditValue.ToString();
                        Text = FormatDoubleForPrint(Text);                        
                    }
                    else if (control.EditValue != null)
                    {
                        Text = control.EditValue.ToString();
                    }
                }
                else if (control.EditValue != null)
                {
                    Text = control.EditValue.ToString();
                }
                LinkBase link;
                if (control is SimpleLabel)
                {
                    link = new TextLink(string.Format("{0}", Caption), GetFont(font_item));
                }
                else
                {
                    if (Text.Trim().Length == 0)
                        Text = "";
                    if (Caption.Trim() == String.Empty)
                        link = new TextLink(string.Format("<b>{0}</b>", Text), GetFont(font_item));
                    else
                        link = new TextLink(string.Format("{0}: {1}", Caption, Text.Trim()), GetFont(font_item));
                }
                result.Add(link);
                return result;
            }
           
        }

        private void NewPage_CreateDetailArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            e.Graph.PrintingSystem.InsertPageBreak(0);            
        }

        private List<BaseControl> GetAllControls(List<BaseControl> c, List<string> list)
        {
            List<BaseControl> collection = new List<BaseControl>();
            foreach (BaseControl control in c)
            {
                bool exist = false;
                foreach (string s in list)
                {
                    if (control.Name == s)
                    {
                        exist = true; break;
                    }
                }
                if (!exist)
                {
                    if (!(control is ControlModel.Page || control is MetaConrol))
                    {
                        collection.Add(control);
                    }
                    if (control is MetaConrol)
                    {
                        collection.AddRange(this.GetAllControls(control.Children, list));
                    }
                }
            }
            return collection;
        }

        public class PrintElementContext
        {
            public BaseControl[] ctrls;
            public PrintItem Option;
            public string[] Values;
            public bool IsNoBorder;
            public string TableHeader;
            public PrintTableCollection table_collection;
            public List<PrintElementContext> list_table;
            public List<PrintElementContext> list_subtable;
            public GridAddedPage ctrl_collection;

            public PrintElementContext(PrintItem option)
            {
                Option = option;

                if (option.ItemType == PrintItemType.Table)
                {
                    list_table = new List<PrintElementContext> { };
                    list_subtable = new List<PrintElementContext> { };
                    IsNoBorder = option.IsNoBorder;
                }
                else if ((option.ItemType == PrintItemType.Template) || (option.ItemType == PrintItemType.Element))
                {
                    if (option.Values != null)
                    {
                        Values = new string[option.Values.Length];
                        ctrls = new BaseControl[option.Values.Length];
                    }
                    else
                    {
                        ctrls = new BaseControl[1];
                    }
                }
                else if (option.ItemType == PrintItemType.CollectionItem)
                {
                    ctrl_collection = new GridAddedPage();
                    list_table = new List<PrintElementContext> { };
                }
                else
                {
                    table_collection = new PrintTableCollection();
                }
            }                     

            public static string GetValueBaseControl(BaseControl ctr)
            {
                string _value = "";
                // проверка того что элемент находится на скрытом метаконтроле или скрытом листе
                if ((ctr != null && ctr.Parent is MetaConrol && (ctr.Parent as MetaConrol).Visible == false) ||
                    (ctr != null && ctr.ParentPage != null &&
                        (ctr.ParentPage.tlItem == null || ctr.ParentPage.tlItem.Visible == false)))
                {
                    _value = "";
                }
                else
                {
                    if (ctr is SimpleEdit)
                    {
                        if ((ctr as SimpleEdit).DataType == SimpleDataType.Bool)
                        {
                            if ((ctr as SimpleEdit).EditValue != null && (ctr as SimpleEdit).EditValue != DBNull.Value)
                            {
                                bool value = (bool)(ctr as SimpleEdit).EditValue;
                                if (value == true)
                                    _value = "Да";
                                else
                                    _value = "Нет";
                            }
                        }
                        else if ((ctr as SimpleEdit).DataType == SimpleDataType.DateTime)
                        {
                            if ((ctr as SimpleEdit).EditValue != null && (ctr as SimpleEdit).EditValue != DBNull.Value)
                            {
                                _value = Convert.ToDateTime((ctr as SimpleEdit).EditValue).ToShortDateString();
                            }
                        }
                        else if (ctr.EditValue != null)
                        {
                            _value = ctr.EditValue.ToString();
                        }
                    }
                    else if (ctr.EditValue != null)
                    {
                        _value = ctr.EditValue.ToString();
                    }
                }
                
                return _value;
            }

            public static bool ControlInHideMetaControl(BaseControl ctrl)
            {
                if ((ctrl != null && ctrl.Parent is MetaConrol && (ctrl.Parent as

    MetaConrol).Visible == false) ||
                                (ctrl != null && ctrl.ParentPage != null &&
                                    (ctrl.ParentPage.tlItem == null || ctrl.ParentPage.tlItem.Visible

    == false)))
                    return true;
                else return false;
            }

            public List<LinkBase> GetLink(GridAddedPage grid, int index_page, EditorView View)
            {
                List<LinkBase> result = new List<LinkBase>();
                try
                {
                    if (Option.ItemType == PrintItemType.Element)
                    {                        
                        if (grid != null) 
                        {
                            bool exist = false;

                            List<BaseControl> list = new List<BaseControl> { };
                            list = GetChildrenGridAddedPage(grid as GridAddedPage, index_page);
                            foreach (BaseControl ctr in list)
                            {
                                if ((!exist) && (ctr.Name == Option.ElementName.Trim('$') + "$" + index_page))
                                {                                   
                                        ctrls[0] = ctr;
                                        //xuc = null;//View.GetControl(ctr.ParentPage.Name, ctr.Name, ctr.ParentPage);
                                        exist = true;                                                                       
                                }
                            } 
                        };

                        if (ControlInHideMetaControl(ctrls[0]))
                            return new List<LinkBase>();

                        List<LinkBase> link_elements = Printing.GetLink(ctrls[0], Option.TextSize, Option.Font);
                        foreach (var link_element in link_elements)
                        {
                            if (link_element is TextLink)
                            {
                                (link_element as TextLink).HAlignment = Option.Alignment;
                                (link_element as TextLink).emSize = Option.TextSize;
                            }
                        }
                        return link_elements;                        
                        
                    }
                    else
                    {
                        TextLink link;

                        if (grid == null)
                        {
                            Option.TextTemplate = Option.TextTemplate.Replace("\\n", Environment.NewLine);
                            if (Values != null)
                            {
                                for (int i = 0; i < Values.Length; i++)
                                {
                                    Values[i] = Values[i].Trim();
                                    if (Values[i].ToString().Length == 0)
                                        Values[i] = "";
                                }

                                link = new TextLink(string.Format(Option.TextTemplate, Values), GetFont(Option.Font));
                            }
                            else
                                link = new TextLink(Option.TextTemplate, GetFont(Option.Font));
                            
                        }
                        else
                        {
                            if (Values != null)
                            {
                                string[] ValueText = new string[Values.Length];

                                for (int i = 0; i < Values.Length; i++)
                                {
                                    if (Values[i].StartsWith("$"))
                                    {
                                        string s = string.Empty;

                                        if (index_page != 0)
                                        {
                                            s = Values[i].Trim('$') + "$" + index_page.ToString();
                                        }
                                        else
                                        {
                                            s = Values[i].Trim('$');
                                        }

                                        bool exist = false;
                                        List<BaseControl> list = new List<BaseControl> { };
                                        list = GetChildrenGridAddedPage(grid as GridAddedPage, index_page);
                                        foreach (BaseControl ctr in list)
                                        {
                                            if (!exist)
                                            {
                                                if (ctr.Name == s)
                                                {
                                                    ValueText[i] = GetValueBaseControl(ctr);
                                                    exist = true;
                                                }
                                                else
                                                {
                                                    ValueText[i] = "";
                                                }                                                
                                            }
                                        }
                                    }
                                }
                                link = new TextLink(string.Format(Option.TextTemplate, ValueText), GetFont(Option.Font));
                            }
                            else
                            {
                                link = new TextLink(Option.TextTemplate, GetFont(Option.Font));
                            }
                        }

                        link.HAlignment = Option.Alignment;
                        link.emSize = Option.TextSize;
                        result.Add(link);
                        return result;
                    }                                         
                }
                catch
                {
                    return new List<LinkBase>();
                }
                
            }

            public static string GetTextElement(BaseControl bs)
            {
               
                    string Text = string.Empty;

                    if (ControlInHideMetaControl(bs))
                        return "";

                    Text = bs.Text;

                    return Text;                             
            }

                       

            public LinkBase GetLinkEmptyTableAddedRow(PrintTableCollectionItem item, float text_size, PrintingFont font_item)
            {
                LinkBase result = new LinkBase();
                Font font = new Font(GetFont(font_item), item.Rows[0].Cells[0].TextSize, FontStyle.Regular);
                result = new EmptyGridLinkTemplate(item,font);
                return result;
            }

            public static List<BaseControl> GetChildrenGridAddedPage(GridAddedPage grid,int index_page)
            {
                List<BaseControl> list = new List<BaseControl> { };

                foreach (BaseControl ctr in grid.Children[index_page].Children)
                {
                    list.Add(ctr);
                    if ((ctr is aetp.Common.ControlModel.Page) || (ctr is MetaConrol))
                    {
                        foreach (BaseControl ctr_page in ctr.Children)
                        {
                            list.Add(ctr_page);
                        }
                    }
                }
                return list;
            }            

            public LinkBase GetLinkTableCollectionGridAddedPage(PrintTableCollectionItem item, int index_page,int index_parent_page)
            {
                System.Data.DataTable dt = new System.Data.DataTable("newTable");
                if (item.Columns != null)
                {
                    int i = 0;
                    foreach (PrintTableCollectionItemColumn col in item.Columns)
                    {
                        DataColumn dst = new DataColumn("dt_col" + i.ToString(), typeof(string));
                        dt.Columns.Add(dst);
                        i++;
                    }
                }

                List<BaseControl> list = new List<BaseControl> { };
                list = GetChildrenGridAddedPage(item.ctrl as GridAddedPage, index_page);

                if (item.Rows != null)
                {
                    foreach (PrintTableCollectionItemRow row in item.Rows)
                    {
                        bool visible = true;

                        if (row.IsInVisible)
                        {
                            if(ControlInHideMetaControl(row.VisibilityElement))
                                visible = false;                            
                        }

                        if (visible)
                        {
                            DataRow dr = dt.NewRow();
                            int i = 0;                           
                            foreach (DataColumn col in dt.Columns)
                            {
                                if (i < item.Columns.Count)
                                { 
                                            if (row.Cells[i].IsTemplate)
                                            {
                                                string[] TempValuesTemplate = new string[row.Cells[i].ValuesTemplate.Length];

                                                for (int k = 0; k < row.Cells[i].ValuesTemplate.Length; k++)
                                                {
                                                    TempValuesTemplate[k] = "";
                                                }

                                                for (int value_index = 0; value_index < row.Cells[i].ValuesTemplate.Length; value_index++)
                                                {
                                                    if (row.Cells[i].ValuesTemplate[value_index].Contains("$page_index"))
                                                    {
                                                        TempValuesTemplate[value_index] = row.Cells[i].ValuesTemplate[value_index].Replace("$page_index", index_page.ToString());
                                                    }
                                                    else if (row.Cells[i].ValuesTemplate[value_index].StartsWith("$"))
                                                    {
                                                        TempValuesTemplate[value_index] = GetRowValue(index_parent_page, index_page, row.Cells[i].ValuesTemplate[value_index], list);
                                                    }
                                                    else
                                                    {
                                                        TempValuesTemplate[value_index] = row.Cells[i].ValuesTemplate[value_index];
                                                    }
                                                }

                                                dr[col] = string.Format(row.Cells[i].TextTemplate, TempValuesTemplate).Trim(',');
                                            }
                                            else
                                            {
                                                if (row.Cells[i].Value.Contains("$page_index"))
                                                {
                                                    dr[col] = row.Cells[i].Value.Replace("$page_index", index_page.ToString());
                                                }
                                                else if (row.Cells[i].Value.StartsWith("$"))
                                                { 
                                                    dr[col] = GetRowValue(index_parent_page, index_page, row.Cells[i].Value, list);                                                    
                                                }
                                                else
                                                {
                                                    dr[col] = row.Cells[i].Value;
                                                }
                                            }
                                                                              
                                    
                                }
                                i++;
                            }

                            dt.Rows.Add(dr);
                        }
                    }
                }

                LinkBase result = new LinkBase();
                result = new TableCollectionLinkTemplate(item, dt);
                return result;
            }

            public LinkBase GetLinkTableCollection(PrintTableCollectionItem item,GridAddedPage grid,int index_page)
            {
                List<BaseControl> list = new List<BaseControl> { };
                if (grid != null)
                {                    
                    list = GetChildrenGridAddedPage(grid, index_page);
                }

                System.Data.DataTable dt = new System.Data.DataTable("newTable");
                if (item.Columns != null)
                {
                    int i = 0;
                    foreach (PrintTableCollectionItemColumn col in item.Columns)
                    {
                        DataColumn dst = new DataColumn("dt_col" + i.ToString(), typeof(string));
                        dt.Columns.Add(dst);
                        i++;
                    }
                }

                if (item.Rows != null)
                {
                    foreach (PrintTableCollectionItemRow row in item.Rows)
                    {
                        bool visible = true;

                        if (row.IsInVisible)
                        {
                            if (ControlInHideMetaControl(row.VisibilityElement))
                                visible = false;
                        }

                        if (visible)
                        {
                            DataRow dr = dt.NewRow();                            
                            int i = 0;
                            foreach (DataColumn col in dt.Columns)
                            {
                                if (i < item.Columns.Count)
                                {
                                    
                                        if (row.Cells[i].IsTemplate)
                                        {
                                            string[] TempValuesTemplate = new string[row.Cells[i].ValuesTemplate.Length];

                                            for (int k = 0; k < row.Cells[i].ValuesTemplate.Length; k++)
                                            {
                                                TempValuesTemplate[k] = "";
                                            }

                                            for (int value_index = 0; value_index < row.Cells[i].ValuesTemplate.Length; value_index++)
                                            {
                                                if (row.Cells[i].ValuesTemplate[value_index].StartsWith("$"))
                                                {
                                                    if (grid == null)
                                                    {
                                                        if (row.Cells[i].ctrl[value_index] != null)
                                                            TempValuesTemplate[value_index] = GetTextElement(row.Cells[i].ctrl[value_index]);
                                                        else
                                                        {
                                                            TempValuesTemplate[value_index] = "";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        TempValuesTemplate[value_index] = GetRowValue(0, index_page, row.Cells[i].ValuesTemplate[value_index], list);
                                                    }
                                                }
                                            }

                                            string temp = row.Cells[i].TextTemplate.Replace("\\n", Environment.NewLine);

                                            dr[col] = string.Format(temp, TempValuesTemplate);
                                        }
                                        else
                                        {
                                            if ((row.Cells[i].Value.StartsWith("$")) && (!row.Cells[i].Value.Contains(':')))
                                            {
                                                if (grid == null)
                                                {
                                                    if (row.Cells[i].ctrl[0] != null)
                                                        dr[col] = GetTextElement(row.Cells[i].ctrl[0]);
                                                    else dr[col] = "";
                                                }
                                                else
                                                {
                                                    dr[col] = GetRowValue(0, index_page, row.Cells[i].Value, list);
                                                }
                                            }
                                            else if ((row.Cells[i].Value.StartsWith("$")) && (row.Cells[i].Value.Contains(':')))
                                            {
                                                if (grid == null)
                                                {
                                                    dr[col] = row.ValuesCells[i];
                                                }                                               
                                            }
                                            else
                                            {
                                                string temp = row.Cells[i].Value.Replace("\\n", Environment.NewLine);
                                                dr[col] = temp;
                                            }
                                        }
                                                                       
                                }
                                i++;
                            }

                            dt.Rows.Add(dr);
                        }
                    }
                }

                LinkBase result = new LinkBase();               
                result = new TableCollectionLinkTemplate(item,dt);
                return result;
            }

            public LinkBase GetLinkTableAddedRow(PrintTableCollectionItem item)
            { 
                LinkBase result = new LinkBase();                
                GridAddedRow gridrow = item.ctrl as GridAddedRow;                
                result = new GridAddedRowLinkTemplate(gridrow as GridAddedRow, item);
                return result;
            }

            public LinkBase GetLinkTable(PrintElementContext collection,GridAddedPage parent_grid,int index_page)
            {
                System.Data.DataTable dt = new System.Data.DataTable("newTable");
                DataColumn[] dt_column_collection = new System.Data.DataColumn[2];

                dt_column_collection[0] = new DataColumn("dt_name", typeof(string));
                dt_column_collection[1] = new DataColumn("dt_value", typeof(string));                

                dt.Columns.AddRange(dt_column_collection);

                foreach (PrintElementContext item in collection.list_table)
                {
                    string Caption = string.Empty;
                    string Text = string.Empty;

                    if (parent_grid == null)
                    {
                        if (item.Option.ItemType == PrintItemType.Element)
                        {
                            // проверка того что элемент находится на скрытом метаконтроле или скрытом листе
                            if ((item.ctrls[0] != null && item.ctrls[0].Parent is MetaConrol && (item.ctrls[0].Parent as MetaConrol).Visible == false) ||
                                (item.ctrls[0] != null && item.ctrls[0].ParentPage != null &&
                                    (item.ctrls[0].ParentPage.tlItem == null || item.ctrls[0].ParentPage.tlItem.Visible == false)))
                                continue;
                            if (item.ctrls[0] != null)
                            {
                                Caption = item.ctrls[0].Caption;
                                Text = GetTextElement(item.ctrls[0]);
                            }
                            if (String.IsNullOrEmpty(Text.Trim()))
                                Text = "";
                            dt.Rows.Add(Caption, Text);

                        }
                        else if (item.Option.ItemType == PrintItemType.Template)
                        {
                            string content;
                            item.Option.TextTemplate = item.Option.TextTemplate.Replace("\\n", Environment.NewLine);
                            if (item.Values != null)
                            {
                                for (int i = 0; i < item.Values.Length; i++)
                                {
                                    if (item.Values[i].ToString().Trim().Length == 0)
                                        item.Values[i] = "";
                                }

                                content = string.Format(item.Option.TextTemplate, item.Values);
                            }
                            else
                                content = item.Option.TextTemplate;
                            dt.Rows.Add(item.TableHeader, content);


                        }
                    }
                    else
                    {
                        if (item.Option.ItemType == PrintItemType.Template)
                        {
                            string content;
                            item.Option.TextTemplate = item.Option.TextTemplate.Replace("\\n", Environment.NewLine);

                            if (item.Values != null)
                            {
                                string[] ValuesTableCollection = new string[item.Values.Length];

                                for (int i = 0; i < item.Values.Length; i++)
                                {
                                    if (item.Option.Values[i].StartsWith("$"))
                                    {
                                        string s = string.Empty;
                                        s = item.Option.Values[i].Trim('$') + "$" + index_page.ToString();

                                        List<BaseControl> list = new List<BaseControl> { };
                                        list = GetChildrenGridAddedPage(parent_grid as GridAddedPage, index_page);

                                        foreach (BaseControl ctr in list)
                                        {
                                            if (ctr.Name == s)
                                            {
                                                ValuesTableCollection[i] = GetValueBaseControl(ctr);
                                            }
                                        }
                                    }
                                    if (item.Values[i].ToString().Trim().Length == 0)
                                        ValuesTableCollection[i] = "";
                                }

                                content = string.Format(item.Option.TextTemplate, ValuesTableCollection);
                            }
                            else

                                content = item.Option.TextTemplate;
                            dt.Rows.Add(item.TableHeader, content);
                        }
                    }

                }

                LinkBase result = new LinkBase();                
                result = new TableLinkTemplate(collection,dt);
                return result;
            }

            public string GetRowValue(int index_parent_page, int index_page, string cell_value, List<BaseControl> list)
            {
                string _value = "";
                string s = string.Empty;
                if (index_parent_page == 0)
                {
                    s = cell_value.Trim('$') + "$" + index_page.ToString();
                }
                else
                    s = cell_value.Trim('$') + "$" + index_parent_page.ToString() + "$" + index_page.ToString();


                foreach (BaseControl ctr in list)
                {
                    if (ctr.Name == s)
                    {
                        if (ControlInHideMetaControl(ctr))                        
                            _value = "";
                        _value = GetValueBaseControl(ctr);
                        
                    }                    
                }

                return _value;
            }
        }

        

        List<PrintElementContext> printContext;
        public void PrintOnSettings()
        {
            PreparePrint();
            PrintOnSettings(printContext, ExportEnum.Preview, "");
        }

        public void ExportOnSettings(ExportEnum export_option, string filter)
        {
            PreparePrint();
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = string.Format("(*.{0})|*.{0}", filter);

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    PrintOnSettings(printContext, export_option, sfd.FileName);
                }
                catch (Exception ex)
                {
                    ErrorHelper.Show(ex);
                }
            }
        }


        void AddPrintElementContextTable(PrintElementContext item)
        {
            for (int i = 0; i < item.Option.PrintItems.Count; i++)
            {
                List<PrintElementContext> list_pc = new List<PrintElementContext> { };
                PrintItem print_item = new PrintItem();

                print_item.Alignment = item.Option.PrintItems[i].Alignment;
                print_item.VAlignment = item.Option.PrintItems[i].VAlignment;
                print_item.ElementName = item.Option.PrintItems[i].ElementName;
                print_item.TextTemplate = item.Option.PrintItems[i].TextTemplate;
                print_item.Values = item.Option.PrintItems[i].Values;
                print_item.TextSize = item.Option.PrintItems[i].TextSize;
                print_item.IsItalic = item.Option.PrintItems[i].IsItalic;
                print_item.IsBackGround = item.Option.PrintItems[i].IsBackGround;
                if (item.Option.PrintItems[i].TableItemType == PrintTableItemType.Element)
                {
                    print_item.ItemType = PrintItemType.Element;
                }
                else if (item.Option.PrintItems[i].TableItemType == PrintTableItemType.Template)
                {
                    print_item.ItemType = PrintItemType.Template;
                }

                PrintElementContext element = new PrintElementContext(print_item);
                element.TableHeader = item.Option.PrintItems[i].TemplateHeader;
                element.list_subtable = list_pc;
                item.list_table.Add(element);
            }
        }


        void PreparePrint()
        {
            if (printContext != null) printContext.Clear();
            if ((printContext == null) || (printContext.Count == 0))
            {
                printContext = new List<PrintElementContext>();
                foreach (var item in Report.PrintingForm.PrintItems)
                {
                    printContext.Add(new PrintElementContext(item));
                }

                foreach (PrintElementContext item in printContext)
                {
                    if ((item.Option.ItemType == PrintItemType.Template) && (item.Values != null))
                    {
                        for (int i = 0; i < item.Values.Length; i++)
                        {
                            if (item.Option.Values[i].StartsWith("#"))
                            {
                                if (item.Option.Values[i] == "#version")
                                {
                                    item.Values[i] = this.GetType().Assembly.GetName().Version.ToString();
                                }
                                else
                                {
                                    foreach (OrganizationDataConfig.OrganizationDataInfo info_item in AppGlobal.Instance.OrganizationConfig.ListOrganizationDataInfo)
                                    {
                                        if ("#" + info_item.Name == item.Option.Values[i].ToString())
                                            item.Values[i] = info_item.Data;
                                    }
                                }
                            }
                        }
                    }
                    else  if (item.Option.ItemType == PrintItemType.Table)
                    {
                        AddPrintElementContextTable(item);
                    }
                    else if (item.Option.ItemType == PrintItemType.TableCollection)
                    {
                        item.table_collection = item.Option.PrintTableCollectionItems;
                    }
                    else if (item.Option.ItemType == PrintItemType.CollectionItem)
                    {
                        foreach (PrintItem printitem in item.Option.CollectionItems)
                        {
                            PrintElementContext element = new PrintElementContext(printitem);                            

                            if (element.Option.ItemType == PrintItemType.Table)
                            {
                                AddPrintElementContextTable(element);
                            }
                            else if (element.Option.ItemType == PrintItemType.TableCollection)
                            {
                                element.table_collection = printitem.PrintTableCollectionItems;
                            }

                            item.list_table.Add(element);
                        }
                    }
                }
                List<BaseControl> report_children = new List<BaseControl> { };
                GetAllChildrenReport(Report.Children, report_children);
                //ReportUtils.DoAction(Report.Children, ActionPrepare);
                ActionPrepareReport(report_children);
            }
            else
                ReCalcPrintContext();

        }

        void GetAllChildrenReport(List<BaseControl> report_children,List<BaseControl> list)
        {
            foreach (var bs in report_children)
            {
                list.Add(bs);
                if (bs is aetp.Common.ControlModel.Page || bs is GridPage || bs is MetaConrol || bs is GridAddedPage)
                    GetAllChildrenReport(bs.Children, list);
                if (bs is MetaGrid)
                    GetAllChildrenReport((bs as MetaGrid).Tables.ToList<BaseControl>(), list);
                if (bs is Grid)
                {
                    GetAllChildrenReport((bs as Grid).Columns.ToList<BaseControl>(), list);
                    GetAllChildrenReport((bs as Grid).Rows.ToList<BaseControl>(), list);
                    foreach (var row in (bs as Grid).Rows)
                        GetAllChildrenReport(row.Cells.ToList<BaseControl>(), list);
                }
                if (bs is GridAddedRow)
                {
                    GetAllChildrenReport((bs as GridAddedRow).Columns.ToList<BaseControl>(), list);
                }
            }            
        }

        void GetAllChildrenGrid(Grid grid, List<BaseControl> list)
        {
            list.Clear(); 
            foreach(aetp.Common.ControlModel.GridRow rows in grid.Rows)
            {                
                list.AddRange(rows.Cells);
            }
        }

        void SetPageHeaderFooter(CompositeLink MainPage, string reportName)
        {
            MainPage.PaperKind = System.Drawing.Printing.PaperKind.A4;
            PageHeaderFooter phf1 = MainPage.PageHeaderFooter as PageHeaderFooter;
            phf1.Footer.Content.Clear();
            phf1.Footer.Content.Add(footer);
            phf1.Footer.LineAlignment = BrickAlignment.Far;
            if (Report.PrintingForm.ShowHeader)
            {
                phf1.Header.Content.Clear();
                phf1.Header.Content.Add(reportName);
            }
        }

        void PrintOnSettings(List<PrintElementContext> printContext, ExportEnum export_option, string filename)
        {
            string reportName = Report.Caption.ToUpper();
            PrintingSystem ps = new PrintingSystem();

            if (export_option == ExportEnum.Excel)
            {
                CompositeLink MainPage = new CompositeLink(ps);
                MainPage.Landscape = false;
                MainPage.PaperKind = System.Drawing.Printing.PaperKind.A4;
                PageHeaderFooter phf = MainPage.PageHeaderFooter as PageHeaderFooter;
                if (Report.PrintingForm.ShowHeader)
                {
                    phf.Header.Content.Clear();
                    phf.Header.Content.Add(reportName);
                }

                MainPage.Links.Add(new TextLink(new String('\n', 5), "Times New Roman"));
                TextLink link_header = new TextLink(reportName, GetFont(Report.PrintingForm.Font));
                link_header.FontStyle = FontStyle.Bold;
                link_header.emSize = 16;
                link_header.HAlignment = StringAlignment.Center;
                MainPage.Links.Add(link_header);
                MainPage.Links.Add(new TextLink(new String('\n', 5), "Times New Roman"));
                
                foreach (var item in printContext)
                {
                    bool visible = false;

                    if (item.Option.IsInVisible)
                    {
                        if (item.Option.VisibilityElementName != null)
                            GetVisiblePage(Report.Children, item.Option.VisibilityElementName, ref visible);
                    }
                    else visible = true;

                    if (visible)
                    {
                        AddPageElement(item, MainPage, null, 0);
                    }
                }
                MainPage.CreateDocument();
            }
            else
            {

                CompositeLink MainPage = new CompositeLink(ps);
                MainPage.Landscape = false;
                MainPage.PaperKind = System.Drawing.Printing.PaperKind.A4;
                MainPage.CreateDocument(false);
                if (Report.PrintingForm.ShowTitle)
                {
                    MainPage.Links.Add(new TextLink(new String('\n', 5), "Times New Roman"));
                    TextLink link_header = new TextLink(reportName, GetFont(Report.PrintingForm.Font));
                    link_header.FontStyle = FontStyle.Bold;
                    link_header.emSize = 16;
                    link_header.HAlignment = StringAlignment.Center;
                    MainPage.Links.Add(link_header);
                    MainPage.Links.Add(new TextLink(new String('\n', 5), "Times New Roman"));
                }


                bool landscape = printContext[0].Option.IsLandscape;
                List<CompositeLink> list = new List<CompositeLink> { };
                CompositeLink MainPage1 = new CompositeLink(new PrintingSystem());
                MainPage1.Landscape = landscape;
                SetPageHeaderFooter(MainPage1, reportName);
                CompositeLink CurrentPage = MainPage1;

                PrintItem option = new PrintItem();
                option.ItemType = PrintItemType.Template;
                option.TextTemplate = "end";
                option.IsLandscape = !(printContext[printContext.Count - 1].Option.IsLandscape);
                PrintElementContext item_end = new PrintElementContext(option);
                printContext.Add(item_end);

                
                foreach (var item in printContext)
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    bool visible = false;

                    if (item.Option.IsInVisible)
                    {
                        if (item.Option.VisibilityElementName != null)
                            GetVisiblePage(Report.Children, item.Option.VisibilityElementName, ref visible);
                    }
                    else visible = true;

                    if (visible)
                    {
                        if (item.Option.IsLandscape != landscape)
                        {
                            CurrentPage.CreateDocument(false);
                            list.Add(CurrentPage);
                            CompositeLink MainPage3 = new CompositeLink(new PrintingSystem());
                            MainPage3.Landscape = item.Option.IsLandscape;
                            SetPageHeaderFooter(MainPage3, reportName);
                            AddPageElement(item, MainPage3, null, 0);
                            CurrentPage = MainPage3;
                        }
                        else
                        {
                            AddPageElement(item, CurrentPage, null, 0);
                        }

                        landscape = item.Option.IsLandscape;
                    }
                    sw.Stop();
                    Console.WriteLine("Object: {0}\nTime: {1}", item.Option, sw.ElapsedMilliseconds);
                }

                list.Add(CurrentPage);
                SetPageHeaderFooter(MainPage, reportName);
                foreach (CompositeLink link in list)
                {
                    MainPage.PrintingSystem.Pages.AddRange(link.PrintingSystem.Pages);
                }
                MainPage.PrintingSystem.ContinuousPageNumbering = true;
            }


            switch (export_option)
            {
                case ExportEnum.Preview: ps.PreviewRibbonFormEx.ShowDialog();  break;
                case ExportEnum.Pdf:
                    {
                        ps.ExportToPdf(filename);
                        OpenFileExport(filename);
                        break;
                    }
                case ExportEnum.Rtf:
                    {
                        ps.ExportToRtf(filename);
                        OpenFileExport(filename);
                        break;
                    }
                case ExportEnum.Excel:
                    {
                        ps.ExportOptions.Xls.TextExportMode = TextExportMode.Text;
                        ps.ExportToXls(filename);
                        OpenFileExport(filename);                        
                        break;
                    }
            }          
        }

        void GetVisiblePage(List<BaseControl> children,string page_name,ref bool visible)
        {
            foreach (BaseControl ctrl in children)
            {
                if (ctrl is aetp.Common.ControlModel.Page)
                {
                    if (ctrl.Name == page_name)
                    {
                        if (ctrl.tlItem != null)
                        {
                            visible = ctrl.tlItem.Visible && (ctrl as aetp.Common.ControlModel.Page).IsShownPanel;
                        }                        
                        return;
                    }

                    GetVisiblePage(ctrl.Children, page_name,ref visible);
                }
            }
        }

        void AddPageElementIsTableAddedRowWithPage(PrintElementContext item, CompositeLink MainPage, GridAddedPage grid, int index_page, PrintTableCollectionItem item_table)
        {
            if (!String.IsNullOrEmpty(item_table.TableNameAddedRow) && grid != null)
            {
                BaseControl bs = GetControlFromGridAddedPage(grid as GridAddedPage, index_page, item_table.TableNameAddedRow);
                if (bs != null)
                {
                    item_table.ctrl = bs;
                }     
            }

            AddPageElementIsTableAddedRow(item, MainPage, item_table);
        }

        void AddPageElementIsTable(PrintElementContext item, CompositeLink MainPage, GridAddedPage grid, int index_page)
        {
            LinkBase lb_table = null; 
            lb_table = item.GetLinkTable(item,grid,index_page);
            if (lb_table != null)
            {
                MainPage.Links.Add(lb_table);
            }                      
        }

        private BaseControl GetControlFromGridAddedPage(GridAddedPage grid,int index_page,string table_name)
        {
            BaseControl bs = null;
            foreach (BaseControl ctr in grid.Children[index_page].Children)
            {
                if (ctr.Name == table_name + "$" + index_page.ToString())
                {
                    bs = ctr;
                }
                if (ctr is aetp.Common.ControlModel.Page)
                {
                    foreach (BaseControl control in (ctr as aetp.Common.ControlModel.Page).Children)
                    {
                        if (control.Name == table_name + "$" + index_page.ToString())
                        {
                            bs = control;
                        }
                    }
                }
            }
            return bs;
        }

        void AddPageElementIsTableAddedRow(PrintElementContext item, CompositeLink MainPage, PrintTableCollectionItem item_table)
        {
            LinkBase lb_table = null;
            if ((item_table.TableNameAddedRow != null) && (item_table.ctrl != null) && (item_table.ctrl is GridAddedRow) && (item_table.ctrl as GridAddedRow).Data != null && (item_table.ctrl as GridAddedRow).Data.Rows.Count > 0)
            {
                lb_table = item.GetLinkTableAddedRow(item_table);
                if (lb_table != null)
                {
                    MainPage.Links.Add(lb_table);
                }
            }
            else
            {
                lb_table = item.GetLinkEmptyTableAddedRow(item_table, item.Option.TextSize, item.Option.Font);
                if (lb_table != null)
                {
                    MainPage.Links.Add(lb_table);
                }
            }
        }      


        void AddPageElementIsTableAddedPage(PrintTableCollectionItem item_table, CompositeLink MainPage, GridAddedPage grid, int index_page, PrintElementContext item)
        {
            if (!String.IsNullOrEmpty(item_table.TableName) && (grid != null))
            {
                BaseControl bs = GetControlFromGridAddedPage(grid as GridAddedPage, index_page, item_table.TableName);
                if (bs != null)
                {
                    item_table.ctrl = bs;
                }
            }

            LinkBase lb_table = null;
            if ((item_table.TableName != null) && (item_table.ctrl != null) && (item_table.ctrl is GridAddedPage) && (item_table.ctrl as GridAddedPage).Children.Count > 1)
            {
                for (int i = 1; i < (item_table.ctrl as GridAddedPage).Children.Count; i++)
                {
                    lb_table = item.GetLinkTableCollectionGridAddedPage(item_table, i, index_page);
                    if (lb_table != null)
                    {
                        MainPage.Links.Add(lb_table);
                    }
                }
            }
            else
            {
                lb_table = item.GetLinkEmptyTableAddedRow(item_table, item.Option.TextSize, item.Option.Font);
                if (lb_table != null)
                {
                    MainPage.Links.Add(lb_table);
                }
            }
        }


        void AddPageElement(PrintElementContext item,CompositeLink MainPage, GridAddedPage grid,int index_page)
        {
            if (item.Option.ItemType == PrintItemType.Table)
            {
                if (item.Option.PrintItems.Count > 0)
                {
                    AddPageElementIsTable(item, MainPage, grid, index_page);                   

                    TextLink link_empty = new TextLink(new String('\n', 1), GetFont(item.Option.Font), 7);
                    MainPage.Links.Add(link_empty);
                }
            }
            else if (item.Option.ItemType == PrintItemType.TableCollection)
            {
                if (item.Option.PrintTableCollectionItems != null)
                {
                    foreach (PrintTableCollectionItem item_table in item.Option.PrintTableCollectionItems)
                    {
                        if (item_table.IsTableAddedPage)
                        {
                            AddPageElementIsTableAddedPage(item_table, MainPage, grid, index_page, item);
                        }
                        else if (item_table.IsTableAddedRow)
                        {
                            if (grid != null)
                                AddPageElementIsTableAddedRowWithPage(item, MainPage, grid, index_page, item_table);
                            else AddPageElementIsTableAddedRow(item, MainPage, item_table);
                        }
                        else
                        {
                            LinkBase lb_table = item.GetLinkTableCollection(item_table, grid, index_page);
                            if (lb_table != null)
                            {
                                MainPage.Links.Add(lb_table);
                            }
                        }
                    }

                    TextLink link_empty = new TextLink(new String('\n', 1), GetFont(item.Option.Font), 7);

                    MainPage.Links.Add(link_empty);
                }

            }
            else if (item.Option.ItemType == PrintItemType.CollectionItem)
            {
                if ((item.Option.CollectionItems.Count > 0) && (!String.IsNullOrEmpty(item.Option.TableName)))
                {
                    int index = 0;
                    if ((item.ctrl_collection as GridAddedPage).Children.Count > 1) index = 1;
                    for (int count = index; count < (item.ctrl_collection as GridAddedPage).Children.Count; count++)
                    {
                        foreach (var collection_item in item.Option.CollectionItems)
                        {
                            PrintElementContext context_item = new PrintElementContext(collection_item);
                            if (collection_item.ItemType == PrintItemType.Template)
                            {
                                context_item.Values = collection_item.Values;
                            }
                            else if (collection_item.ItemType == PrintItemType.Table)
                            {
                                for (int i = 0; i < collection_item.PrintItems.Count; i++)
                                {
                                    List<PrintElementContext> list_pc = new List<PrintElementContext> { };
                                    PrintItem print_item = new PrintItem();

                                    print_item.Alignment = collection_item.PrintItems[i].Alignment;
                                    print_item.ElementName = collection_item.PrintItems[i].ElementName;
                                    print_item.TextTemplate = collection_item.PrintItems[i].TextTemplate;
                                    print_item.Values = collection_item.PrintItems[i].Values;
                                    print_item.TextSize = collection_item.PrintItems[i].TextSize;
                                    print_item.IsItalic = collection_item.PrintItems[i].IsItalic;
                                    print_item.IsBackGround = collection_item.PrintItems[i].IsBackGround;
                                    if (collection_item.PrintItems[i].TableItemType == PrintTableItemType.Element)
                                    {
                                        print_item.ItemType = PrintItemType.Element;
                                    }
                                    else if (collection_item.PrintItems[i].TableItemType == PrintTableItemType.Template)
                                    {
                                        print_item.ItemType = PrintItemType.Template;
                                    }

                                    PrintElementContext el = new PrintElementContext(print_item);
                                    el.TableHeader = collection_item.PrintItems[i].TemplateHeader;
                                    el.list_subtable = list_pc;
                                    el.Values = print_item.Values;
                                    context_item.list_table.Add(el);
                                }
                            }

                            AddPageElement(context_item, MainPage, (item.ctrl_collection as GridAddedPage), count);
                        }

                        TextLink link_empty = new TextLink(new String('\n', 1), GetFont(item.Option.Font), 7);

                        MainPage.Links.Add(link_empty);
                    }
                }
            }
            else
            {
                if (item.Option.TextTemplate == "\\r\\n")
                {
                    DevExpress.XtraPrinting.Link NewPage = new DevExpress.XtraPrinting.Link();

                    NewPage.CreateDetailArea += new CreateAreaEventHandler(NewPage_CreateDetailArea);
                    MainPage.Links.Add(NewPage);
                }
                else
                {

                    List<LinkBase> lbs = item.GetLink(grid, index_page, View);
                    foreach (var lb in lbs)
                    {
                        if (lb != null)
                        {
                            lb.CustomPaperSize = new Size(500, 500);
                            MainPage.Links.Add(lb);
                            TextLink link_empty = new TextLink(new String('\n', 1), GetFont(item.Option.Font), 7);

                            MainPage.Links.Add(link_empty);
                        }
                    }
                }
            }                           
        }        

        void InitializeTableCollection(PrintTableCollection collection, List<BaseControl> list)
        {
           
                BaseControl grid = null;
                List<BaseControl> children_grid = new List<BaseControl> { };

                foreach (PrintTableCollectionItem item in collection)
                {
                    if (item.IsTableAddedPage)
                    {
                        BaseControl bc = list.Where(x => (x.Name == item.TableName)).SingleOrDefault();
                        if ((bc != null))
                            item.ctrl = bc;

                    }
                    else if (item.IsTableAddedRow)
                    {
                        BaseControl bc = list.Where(x => (x.Name == item.TableNameAddedRow)).SingleOrDefault();
                        if ((bc != null))
                            item.ctrl = bc;
                    }
                    else
                    {
                        foreach (PrintTableCollectionItemRow row in item.Rows)
                        {
                            if (row.ValuesCells == null)
                            {
                                row.ValuesCells = new string[row.Cells.Count];
                            }

                            if ((row.IsInVisible) && (row.VisibilityElementName != null))
                            {
                                BaseControl bc = list.Where(x => (x.Name == row.VisibilityElementName)).SingleOrDefault();
                                if ((bc != null))
                                    row.VisibilityElement = bc;
                            }

                            if (row.Cells.Count != 0)
                            {
                                for (int i = 0; i < row.Cells.Count; i++)
                                {
                                    int length = 1;

                                    if (row.Cells[i].IsTemplate)
                                    {
                                        length = row.Cells[i].ValuesTemplate.Length;
                                    }

                                    if (row.Cells[i].ctrl == null)
                                    {
                                        row.Cells[i].ctrl = new BaseControl[length];
                                    }

                                    if (row.Cells[i].xuc == null)
                                    {
                                        row.Cells[i].xuc = new System.Windows.Forms.Control[length];
                                    }

                                    if (row.Cells[i].IsTemplate)
                                    {
                                        if ((row.Cells[i].ValuesTemplate.Length > 0) && (!String.IsNullOrEmpty(row.Cells[i].TextTemplate)))
                                        {
                                            for (int value = 0; value < row.Cells[i].ValuesTemplate.Length; value++)
                                            {
                                                BaseControl bc = list.Where(x => ("$" + x.Name == row.Cells[i].ValuesTemplate[value])).SingleOrDefault();
                                                if ((bc != null))
                                                    row.Cells[i].ctrl[value] = bc;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        BaseControl bc = list.Where(x => ("$" + x.Name == row.Cells[i].Value)).SingleOrDefault();
                                        if ((bc != null))
                                        {
                                            row.Cells[i].ctrl[0] = bc;
                                        }
                                        else if ((row.Cells[i].Value.StartsWith("$")) && (row.Cells[i].Value.Contains(':')))
                                        {
                                            string[] cell = row.Cells[i].Value.Split(':');
                                            if ((grid != null) && children_grid.Count > 0 && (cell[1] == grid.Name))
                                            {
                                                BaseControl ctrl = children_grid.Where(x => ("$" + x.Name == cell[0] && x is GridCellItem)).SingleOrDefault();
                                                if ((ctrl != null))
                                                {
                                                    row.ValuesCells[i] = GetFormattedValue(ctrl);
                                                }
                                            }
                                            else
                                            {
                                                BaseControl ctrl = list.Where(x => ("$" + x.Name == cell[0] && x is GridCellItem && (x as GridCellItem).Column.grid.Name == cell[1])).SingleOrDefault();
                                                if ((ctrl != null))
                                                {
                                                    row.ValuesCells[i] = GetFormattedValue(ctrl);
                                                    grid = list.Where(x => (x.Name == (ctrl as GridCellItem).Column.grid.Name)).SingleOrDefault();
                                                    GetAllChildrenGrid(grid as Grid, children_grid);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            
            

            }


        void ActionPrepareReport(List<BaseControl> list)
        {
            foreach (PrintElementContext context in printContext)
            {
                bool visible = false;
                if (context.Option.IsInVisible)
                {
                    if (context.Option.VisibilityElementName != null)
                        GetVisiblePage(Report.Children, context.Option.VisibilityElementName, ref visible);
                }
                else visible = true;

                if (visible)
                {
                    if (context.Option.ItemType == PrintItemType.Element)
                    {
                        BaseControl bc = list.Where(x => (x.Name == context.Option.ElementName)).SingleOrDefault();
                        if ((bc != null))
                            context.ctrls[0] = bc;
                    }
                    else
                        if ((context.Option.ItemType == PrintItemType.Template) && (context.Option.Values != null))
                        {
                            for (int i = 0; i < context.Option.Values.Length; i++)
                            {
                                BaseControl bc = list.Where(x => (x.Name == context.Option.Values[i])).SingleOrDefault();
                                if ((bc != null))
                                {
                                    context.ctrls[i] = bc;
                                    context.Values[i] = GetFormattedValue(bc);
                                }
                            }
                        }
                        else if (context.Option.ItemType == PrintItemType.Table)
                        {
                            foreach (PrintElementContext item_table in context.list_table)
                            {
                                if (item_table.Option.ItemType == PrintItemType.Element)
                                {
                                    BaseControl bc = list.Where(x => (x.Name == item_table.Option.ElementName)).SingleOrDefault();
                                    if (bc != null)
                                    {
                                        item_table.ctrls[0] = bc;
                                    }
                                }
                                else if ((item_table.Option.ItemType == PrintItemType.Template) && (item_table.Option.Values != null))
                                {
                                    for (int i = 0; i < item_table.Option.Values.Length; i++)
                                    {
                                        BaseControl bc = list.Where(x => (x.Name == item_table.Option.Values[i])).SingleOrDefault();
                                        if (bc != null)
                                        {
                                            item_table.ctrls[i] = bc;
                                            item_table.Values[i] = GetFormattedValue(bc);
                                        }
                                    }
                                }
                            }
                        }
                        else if (context.Option.ItemType == PrintItemType.CollectionItem)
                        {
                            if (!String.IsNullOrEmpty(context.Option.TableName))
                            {
                                BaseControl bc = list.Where(x => (x.Name == context.Option.TableName)).SingleOrDefault();
                                if (bc != null)
                                {
                                    context.ctrl_collection = bc as GridAddedPage;
                                }
                                foreach (PrintElementContext item in context.list_table)
                                {
                                    if (item.Option.ItemType == PrintItemType.TableCollection)
                                    {
                                        InitializeTableCollection(item.table_collection, list);
                                    }
                                }
                            }
                        }
                        else if (context.Option.ItemType == PrintItemType.TableCollection)
                        {
                            InitializeTableCollection(context.table_collection, list);
                        }
                }
            }
        }
        

        string GetFormattedValue(BaseControl ctrl)
        {            
            string formattedValue = ctrl.EditValue == null ? "" : ctrl.EditValue.ToString();
            if ((ctrl != null && ctrl.Parent is MetaConrol && (ctrl.Parent as MetaConrol).Visible == false) ||
                (ctrl != null && ctrl.ParentPage != null && (ctrl.ParentPage.tlItem == null || ctrl.ParentPage.tlItem.Visible == false)))
                return "";
            if ((ctrl is SimpleEdit) && (ctrl as SimpleEdit).DataType == SimpleDataType.DateTime)
            {
                if (ctrl.EditValue != null)
                {
                    formattedValue = Convert.ToDateTime(ctrl.EditValue).ToString("dd.MM.yyyy");
                    formattedValue = formattedValue.Length > 10 ? formattedValue.Substring(0, 10) : formattedValue;
                }
                else formattedValue = "";
            }
            if ((ctrl is SimpleEdit) && (ctrl as SimpleEdit).DataType == SimpleDataType.Bool)
            {
                if (formattedValue == "True")
                    formattedValue = "Да";
                else if (formattedValue == "False")
                    formattedValue = "Нет";
            }
            if ((ctrl is SimpleEdit) && (ctrl as SimpleEdit).DataType == SimpleDataType.Double)
            {
                formattedValue = formattedValue.Replace(".", ",");
            }
            if (ctrl is GridCellItem)
            {
                var gridcell = ctrl as GridCellItem;
                if ((gridcell.FormulaForCalculating != null) && (gridcell.FormulaForCalculating.ToString().Trim() != "") && (gridcell.EditValue != null) && gridcell.EditValue.ToString() != "NaN" && gridcell.EditValue.ToString() != string.Empty)
                {
                    decimal result = 0;
                    Decimal.TryParse(new Expression(gridcell.EditValue.ToString().Replace(',', '.')).Evaluate().ToString(), out result);
                    formattedValue = result.ToString();
                }
                if (gridcell.Type == GridCellType.Double)
                {
                    if (formattedValue.StartsWith(".") || formattedValue.StartsWith(","))
                    {
                        formattedValue = "0" + formattedValue;
                    }
                    formattedValue = formattedValue.TrimEnd('.', ',');
                    formattedValue = formattedValue.Replace('.', ',');
                }
            }
            return formattedValue;
        }

        void ReCalcPrintContext()
        {
            foreach (var item in printContext)
            {
                if (item.Values != null)
                    for (int i = 0; i < item.Values.Length; i++)
                    {
                        if (item.ctrls[i] != null)
                        {
                            item.Values[i] = GetFormattedValue(item.ctrls[i]);
                        }
                    }
            }
        }

        public void Print()
        {
            if (Report != null)
            {
                aetp.Common.Print.PrintOptionsEnum PrintOptions = aetp.Common.Print.PrintDialog.PrintOptions(Report.PrintingForm);
                if (PrintOptions == PrintOptionsEnum.None)
                    return;

                if ((Report.PrintingForm.PrintingType == PrintingType.Template) && (PrintOptions == PrintOptionsEnum.All))
                {
                    PrintOnSettings();                   
                }
                else
                    Print(PrintOptions);
            }
        }

        public void ExportXtddToExcel()
        {
            ExportXTDDtoExcelDialog export = new ExportXTDDtoExcelDialog();
            export.ShowDialog();
        }
        
        void Print(PrintOptionsEnum PrintOptions)
        {
            List<ControlModel.Page> PagePanels = new List<ControlModel.Page>();
            if (PrintOptions == PrintOptionsEnum.All)
            {
                GetPagePanelsAll(PagePanels);
            }
            else
            {
                GetPagePanelsSelectedPage(PagePanels);
            }
            PrintSetting(PagePanels, ExportEnum.Preview, "", PrintOptions == PrintOptionsEnum.All);
        }

        void GetPagePanelsAll(List<ControlModel.Page> PagePanels)
        {
            ReportUtils.DoAction(Report.Children, x =>
            {
                if (x is ControlModel.Page)
                {
                    var p = x as ControlModel.Page;
                    if (p.IsShownPanel)
                    {
                        PagePanels.Add(p);
                    }
                }
            });
        }


        void GetPagePanelsSelectedPage(List<ControlModel.Page> PagePanels)
        {
            var view = View as EditorView;
            PagePanels.Add(view.CurrentPage);
        }

        public void Export(ExportEnum export_option, string filter)
        {
            if (Report != null)
            {
                aetp.Common.Print.ExportOptionsEnum Options = aetp.Common.Print.ExportDialog.ExportOptions(Report.PrintingForm);
                if (Options == ExportOptionsEnum.None)
                    return;
                if ((Report.PrintingForm.PrintingType == PrintingType.Template) && (Options == ExportOptionsEnum.All) )
                {
                    ExportOnSettings(export_option, filter);                   
                }
                else
                    Export(Options, export_option, filter);
            }
        }

        void Export(ExportOptionsEnum Options, ExportEnum export_option, string filter)
        {
            List<ControlModel.Page> PagePanels;
            PagePanels = new List<ControlModel.Page>();
            if (Options == ExportOptionsEnum.All)
            {
                GetPagePanelsAll(PagePanels);
            }
            else
            {
                GetPagePanelsSelectedPage(PagePanels);
            }
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = string.Format("(*.{0})|*.{0}", filter);

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    PrintSetting(PagePanels, export_option, sfd.FileName, Options == ExportOptionsEnum.All);
                }
                catch (Exception ex)
                {
                    ErrorHelper.Show(ex);
                }
            }
        }

        private void OpenFileExport(string filename)
        {
            if (MessageBox.Show("Вы хотите открыть этот файл?", "Экспорт", MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = filename;
                proc.StartInfo.UseShellExecute = true;
                proc.Start();
            }
        }        
    }

    public enum ExportEnum
    {
        Preview = 0,
        Excel = 1,
        Pdf = 2,
        Rtf
    }
}
