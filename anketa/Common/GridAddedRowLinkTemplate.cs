﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting;
using aetp.Common.ControlModel;
using System.Data;
using System.Windows;

namespace aetp.Common.Control
{
    public class GridAddedRowLinkTemplate : DevExpress.XtraPrinting.Link
    {
        GridAddedRow grid;
        DataTable dt;
        Font[] font;
        float[] columnwidth;
        BrickStringFormat[] brickSFormat;
        Color[] backgroundColor;        
        PrintTableCollectionItem item;

        public GridAddedRowLinkTemplate(GridAddedRow grid, PrintTableCollectionItem item)
        {
            this.grid = grid;
            dt = PrepareDataTableFromGridAddedRow(grid);
            this.item = item;            
        }

        protected override void CreateReportHeader(BrickGraphics graph)
        {
            base.CreateReportHeader(graph);            
        }


        public static string GetFont(PrintingFont font)
        {
            string font_family = string.Empty;
            if (font == PrintingFont.Courier)
                font_family = "Courier New";
            else if (font == PrintingFont.Arial)
            {
                font_family = "Arial";
            }
            else font_family = "Times New Roman";
            return font_family;
        }

        protected override void CreateDetail(BrickGraphics graph)
        {
            columnwidth = new float[item.Columns.Count];
            brickSFormat = new BrickStringFormat[item.Columns.Count];
            backgroundColor = new Color[item.Columns.Count];
            font = new Font[item.Columns.Count];

            float sum_width = 0;
            for (int j = 0, i = item.Columns.Count - 1; i > -1; i--, j++)
            {
                //выравнивание
                StringFormat sf = new StringFormat();
                switch (item.Rows[0].Cells[i].VAlignment)
                {
                    case aetp.Common.ControlModel.VerticalAlignment.Bottom: sf.LineAlignment  = StringAlignment.Far; break;
                    case aetp.Common.ControlModel.VerticalAlignment.Center: sf.LineAlignment = StringAlignment.Center; break;
                    case aetp.Common.ControlModel.VerticalAlignment.Top: sf.LineAlignment = StringAlignment.Near; break;
                }
                sf.Alignment = item.Rows[0].Cells[i].Alignment;               
                brickSFormat[j] = new BrickStringFormat(sf);

                //заливка
                backgroundColor[i] = (item.Rows[0].Cells[i].IsBackGround) ? Color.LightGray : Color.Transparent;

                //шрифт
                FontStyle style = FontStyle.Regular;                
                if (item.Rows[0].Cells[i].IsItalic && item.Rows[0].Cells[i].IsBold) style = FontStyle.Italic | FontStyle.Bold;
                else if (!item.Rows[0].Cells[i].IsItalic && item.Rows[0].Cells[i].IsBold) style = FontStyle.Bold;
                else if (item.Rows[0].Cells[i].IsItalic && !item.Rows[0].Cells[i].IsBold) style = FontStyle.Italic;
                font[i] = new Font(GetFont(item.Rows[0].Cells[i].Font), item.Rows[0].Cells[i].TextSize,style);
                //ширина столбца
                columnwidth[j] = item.Columns[i].Width;
                sum_width += item.Columns[i].Width;
            }

            float k = (graph.ClientPageSize.Width -1 )/ sum_width;

            for (int i = 0; i < columnwidth.Length; i++)
            {
                columnwidth[i] = columnwidth[i] * k;                
            }        
            
            
            graph.BorderColor = Color.Black;
            graph.BorderWidth = 1;
            float y = 0;
            float max_height = 0;
            float width = (graph.ClientPageSize.Width - 1) / grid.Columns.Count;
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                float x = 0;
                graph.Font = font[0];
                max_height = GridLink.getMaxRowHeight(graph, dt.Rows[r], columnwidth);
                for (int c = 0; c < dt.Columns.Count; c++)
                {

                    graph.StringFormat = brickSFormat[c];
                    graph.BackColor = backgroundColor[c];
                    graph.Font = font[c];
                    
                    graph.DrawString(dt.Rows[r][c].ToString(), Color.Black,
                        new RectangleF(x, y, columnwidth[c], 
                        max_height), 
                        BorderSide.All);
                    x += columnwidth[c];
                }
                y += max_height;
            }
        }

        /// <summary>
        /// Приведение данных в таблицах с добавлением строк к виду необходимому для печати/экспорта
        /// Например поменять разделительв дробных числах
        /// </summary>
        /// <param name="grid">GridAddedRow</param>
        /// <param name="dt">DataTable</param>
        /// <returns></returns>
        static DataTable PrepareDataTableFromGridAddedRow(GridAddedRow grid)
        {
            DataTable newdt = new System.Data.DataTable();
            for (int r = 0; r < grid.Columns.Count; r++)
            {
                DataColumn col = new System.Data.DataColumn();
                col.ColumnName = grid.Columns[r].Name;
                newdt.Columns.Add(col);
            }
            if (grid.Data == null)
                return newdt;
            for (int r = 0; r < grid.Data.Rows.Count; r++)
            {
                DataRow dr = newdt.NewRow();
                for (int c = 0; c < grid.Columns.Count; c++)
                {
                    try
                    {
                        if (grid.Columns[c].Type == GridCellType.Label)
                        {
                            grid.Data.Rows[r][c] = grid.Columns[c].TextLabel;
                        }
                        if (grid.Data.Rows[r][c] != null && !String.IsNullOrEmpty(grid.Data.Rows[r][c].ToString()))
                        {
                            if (grid.Columns[c].Type == GridCellType.Double)
                            {
                                dr[c] = grid.Data.Rows[r][c].ToString().Replace(".", ",");
                                dr[c] = Printing.FormatDoubleForPrint(dr[c].ToString());
                            }
                            else if (grid.Columns[c].Type == GridCellType.Date)
                            {
                                dr[c] = Convert.ToDateTime(grid.Data.Rows[r][c].ToString()).ToShortDateString();
                            }
                            else if (grid.Columns[c].Type == GridCellType.Check)
                            {
                                bool value = (bool)Convert.ToBoolean(grid.Data.Rows[r][c].ToString());
                                if (value == true)
                                    dr[c] = "Да";
                                else
                                    dr[c] = "Нет";

                            }
                            else if (grid.Columns[c].Type == GridCellType.List)
                            {
                                var item = ReportUtils.GetDictionaryItemDescription(grid.Columns[c].Source, grid.Data.Rows[r][c].ToString());
                                if (item != null)
                                    dr[c] = item.ToString();
                            }
                            else if (grid.Columns[c].Type == GridCellType.Integer)
                            {
                                dr[c] = grid.Data.Rows[r][c];
                                dr[c] = Printing.FormatDoubleForPrint(dr[c].ToString());
                            }
                            else
                            {
                                dr[c] = grid.Data.Rows[r][c];
                            }
                        }
                        else if (grid.Columns[c].Type == GridCellType.Date)
                        {
                            DateTime date;
                            if (grid.Data.Rows[r][c] != null && DateTime.TryParse(grid.Data.Rows[r][c].ToString(), out date))
                            {
                                dr[c] = date.ToString("dd.MM.yyyy");
                            }
                        }
                        else if (grid.Columns[c].Type == GridCellType.List)
                        {
                            var item = ReportUtils.GetDictionaryItemDescription(grid.Columns[c].Source, grid.Data.Rows[r][c].ToString());
                            if (item != null)
                                dr[c] = item.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "Значение" + grid.Data.Rows[r][c].ToString());
                    }
                }
                newdt.Rows.Add(dr);
            }
            return newdt;
        }
    }
}
