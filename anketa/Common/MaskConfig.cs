﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace aetp.Common
{
    public class MaskConfig
    {
        const string ConfigFileName = "MaskConfig.xml";        

        public struct MaskInfo  
        {
            public string Name;
            public string Mask;
            public string Description;
            public string ErrorMessage;
        }

        public List<MaskInfo> ListMaskInfo {get; set; }

        class MaskSearcher
        {
            private string name;
            private string mask;
            private string description;
            private string error_message;

            public MaskSearcher(string name,string mask, string description,string error_message)
            {
                this.name = name;
                this.mask = mask;
                this.description = description;
                this.error_message = error_message;
            }

            public string Name
            {
                get { return name; }
                set { name = value; }
            }

            public string Mask
            {
                get { return mask; }
                set { mask = value; }
            }

            public string Description
            {
                get { return description; }
                set { description = value; }
            }

            public string ErrorMessage
            {
                get { return error_message; }
                set { error_message = value; }
            }


            public bool Condition(MaskConfig.MaskInfo mask_info)
            {
                return mask_info.Mask == mask;
            }
        }

        public MaskConfig()
        {
           
        }
                
        public string GetConfigFilePath()
        {
            return Path.Combine(aetp.Utils.Config.GetCommonAppPath(), ConfigFileName);
        }

        public void Save()
        {
            FileStream fs = new FileStream(GetConfigFilePath(), FileMode.Create);
            XmlSerializer serializer = new XmlSerializer(typeof(MaskConfig));
            serializer.Serialize(fs, this);
            fs.Close();
        }

        public void Load()
        {
            string fileName = GetConfigFilePath();
            if (!File.Exists(fileName))
            {
                Save();
            }
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            XmlSerializer serializer = new XmlSerializer(typeof(MaskConfig));
            MaskConfig cfg = (MaskConfig)serializer.Deserialize(fs);
            fs.Close();

            ListMaskInfo = cfg.ListMaskInfo;
        }

        public void AddMaskData(string name,string mask, string description,string error_message)
        {
            MaskConfig.MaskInfo item = new MaskConfig.MaskInfo();
            item.Name = name;
            item.Mask = mask;
            item.Description = description;
            item.ErrorMessage = error_message;

            AppGlobal.Instance.MaskConfig.ListMaskInfo.Remove(AppGlobal.Instance.MaskConfig.ListMaskInfo.Find(new MaskSearcher(name,mask,description,error_message).Condition));
            AppGlobal.Instance.MaskConfig.ListMaskInfo.Add(item);
            AppGlobal.Instance.MaskConfig.Save();            
        }
    }
}
