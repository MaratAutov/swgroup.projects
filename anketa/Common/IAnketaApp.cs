﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace aetp.Common
{
    public interface IAnketaApp
    {
        void Run();
        Form GetMainForm();
    }
}
