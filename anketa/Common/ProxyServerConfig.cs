﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;

namespace aetp.Common
{
    public class ProxyServerConfig
    {
        public enum ProxyTypes
        {
            None = 0,
            Default,
            Http
        }

        const string ConfigFileName = "ProxyServerConfig.xml";
        
        public ProxyTypes ProxyType;
        public string HttpUrl = "";
        public int HttpPort = 8080;
        public bool NotUsingProxyForLocalPort = true;

        public ProxyServerConfig()
        {
            
        }
                
        public string GetConfigFilePath()
        {
            return Path.Combine(aetp.Utils.Config.GetAppPath(), ConfigFileName);
        }

        public void Save()
        {
            FileStream fs = new FileStream(GetConfigFilePath(), FileMode.Create);
            XmlSerializer serializer = new XmlSerializer(typeof(ProxyServerConfig));
            serializer.Serialize(fs, this);
            fs.Close();
        }

        public void Load()
        {
            string fileName = GetConfigFilePath();
            if (!File.Exists(fileName))
            {
                Save();
            }
            FileStream fs = new FileStream(fileName, FileMode.Open);
            XmlSerializer serializer = new XmlSerializer(typeof(ProxyServerConfig));
            ProxyServerConfig cfg = (ProxyServerConfig)serializer.Deserialize(fs);
            fs.Close();
            ProxyType = cfg.ProxyType;
            HttpUrl = cfg.HttpUrl;
            HttpPort = cfg.HttpPort;
            NotUsingProxyForLocalPort = cfg.NotUsingProxyForLocalPort;            
        }

    }
}
