﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;

namespace aetp.Common.Event
{
    public class AddControlEventArgs
    {
        public Type type { get; set; }
    }

    public class AddControlEditEventArgs
    {
        public Type type { get; set; }
        public SimpleDataType dt { get; set; }
    }

    public class RemoveControlEventArgs
    {
        public BaseControl Control { get; set; }        
    }
}
