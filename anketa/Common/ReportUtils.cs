﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.IO;
using System.Xml;
using aetp.Common.Serializer;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using System.Collections;

namespace aetp.Common
{
    public static class ReportUtils
    {
        public static List<BaseControl> GetAllPges(Report report)
        {
            List<BaseControl> pages = new List<BaseControl>();
            GetControls(report.Children, ref pages, new List<Type>() { typeof(Page) });
            return pages;
        }

        public static List<BaseControl> GetAllControls(Report report, List<Type> types)
        {
            List<BaseControl> pages = new List<BaseControl>();
            GetControls(report.Children, ref pages, types);
            return pages;
        }

#region GetControlByName

        
        public static BaseControl GetControlByName(Report report, string name)
        {
            if (report.Name == name)
                return report;
            return GetControlByName(report.Children, name);
        }

        static BaseControl GetControlByName(List<BaseControl> controls, string name)
        {
            BaseControl res = null;
            foreach (var bs in controls)
            {
                if (bs.Name == name)
                    return bs;
                
                if (bs is Page || bs is MetaConrol)
                {
                    res = GetControlByName(bs.Children, name);
                }
                if (res != null)
                    return res;
                if (bs is GridAddedPage)
                {
                    if (bs.Children.Count > 0 && bs.Children[0] is GridPage)
                        res = GetControlByName(bs.Children[0].Children, name);
                }
                if (res != null)
                    return res;
            }
            return res;
        }

        public static BaseControl GetControlByNameUp(BaseControl control, string name, BaseControl upLimit)
        {
            BaseControl parent = control.Parent;
            BaseControl current = control;
            do
            {
                BaseControl[] child = new BaseControl[parent.Children.Count]; 
                parent.Children.CopyTo(child);
                List<BaseControl> children = child.ToList();
                children.Remove(current);
                BaseControl getControlByName = GetControlByName(children, name);
                if (getControlByName == null)
                {
                    current = parent;
                    parent = parent.Parent;
                }
                else
                    return getControlByName;
            } while (parent != upLimit);
            return null;
        }

#endregion

        static void GetControls(List<BaseControl> controls, ref List<BaseControl> pages, List<Type> types)
        {
            foreach (var bs in controls)
            {
                var type = (from t in types where t == bs.GetType() select t).FirstOrDefault();
                if (type != null)
                    pages.Add(bs);
                if (bs is Page || bs is MetaConrol)
                {
                    GetControls(bs.Children, ref pages, types);
                }
                if (bs is GridAddedPage)
                {
                    if (bs.Children.Count > 0 && bs.Children[0] is GridPage)
                        GetControls(bs.Children[0].Children, ref pages, types);
                }
                
            }
        }

        public static void DoAction(List<BaseControl> ctrls, Action<BaseControl> action)
        {
            
                foreach (var bs in ctrls)
                {
                    action(bs);
                    if (bs is Page || bs is GridPage || bs is MetaConrol || bs is GridAddedPage)
                        DoAction(bs.Children, action);
                    if (bs is MetaGrid)
                        DoAction((bs as MetaGrid).Tables.ToList<BaseControl>(), action);
                    if (bs is Grid)
                    {
                        DoAction((bs as Grid).Columns.ToList<BaseControl>(), action);
                        DoAction((bs as Grid).Rows.ToList<BaseControl>(), action);
                        foreach (var row in (bs as Grid).Rows)
                            DoAction(row.Cells.ToList<BaseControl>(), action);
                    }
                    if (bs is GridAddedRow)
                    {
                        DoAction((bs as GridAddedRow).Columns.ToList<BaseControl>(), action);
                    }
                }
            
           
        }

        public static void PageDoAction(List<BaseControl> ctrls, Action<BaseControl> action)
        {
            foreach (var bs in ctrls)
            {
                action(bs);
                if (bs is MetaConrol)
                    DoAction(bs.Children, action);
                if (bs is MetaGrid)
                    DoAction((bs as MetaGrid).Tables.ToList<BaseControl>(), action);
                if (bs is Grid)
                {
                    DoAction((bs as Grid).Columns.ToList<BaseControl>(), action);
                    DoAction((bs as Grid).Rows.ToList<BaseControl>(), action);
                    foreach (var row in (bs as Grid).Rows)
                        DoAction(row.Cells.ToList<BaseControl>(), action);
                }
                if (bs is GridAddedRow)
                {
                    DoAction((bs as GridAddedRow).Columns.ToList<BaseControl>(), action);
                }
            }
        }

        public static void DoAction(BaseControl parent, List<BaseControl> ctrls, Action<BaseControl, BaseControl> action)
        {
            foreach (var bs in ctrls)
            {
                action(parent, bs);
                if (bs is Page || bs is GridPage || bs is MetaConrol || bs is GridAddedPage)
                    DoAction(bs, bs.Children, action);
                if (bs is MetaGrid)
                    DoAction(bs, (bs as MetaGrid).Tables.ToList<BaseControl>(), action);
                if (bs is Grid)
                {
                    DoAction(bs, (bs as Grid).Columns.ToList<BaseControl>(), action);
                    DoAction(bs, (bs as Grid).Rows.ToList<BaseControl>(), action);
                    foreach (var row in (bs as Grid).Rows)
                        DoAction(row, row.Cells.ToList<BaseControl>(), action);
                }
                if (bs is GridAddedRow)
                {
                    DoAction(bs, (bs as GridAddedRow).Columns.ToList<BaseControl>(), action);
                }
            }
        }

        public static IList<T> GenerateNewList<T>(IList<T> oldList)
        {
            IList<T> newList = new List<T>();
            foreach (var item in oldList)
            {
                newList.Add(item);
            }
            return newList;
        }

        public static void RefreshModelsList()
        {
            Cursor.Current = Cursors.WaitCursor;
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.RunWorkerAsync();
            autoEvent.WaitOne();
            Cursor.Current = Cursors.Default;
        }
        static AutoResetEvent autoEvent = new AutoResetEvent(false);
        static void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            AppGlobal.Instance.Config.ListModelInfo.Clear();
            foreach (string filepath in Directory.GetFiles(AppGlobal.Instance.Config.ModelsPath, "*.aff"))
            {
                try
                {
                    Report report;
                   
                    using (XmlTextReader reportReader = new XmlTextReader(filepath))
                    {
                        report = new FormSerializer().Deserialize(reportReader, false);
                    }

                    if (report != null)
                        AppGlobal.Instance.Config.AddModel(report.Name, filepath, report.SerialaizerConfig.appVersion, report.Caption, report.SerializingTag, !report.IsOlder);
                    
                }
                catch (Exception ex)
                {
                    //игнорируем поврежденные, некорректные и старые модели
                    Console.WriteLine(ex);
                }
                AppGlobal.Instance.Config.Save();
            }
            autoEvent.Set();
        }

        public static DictionaryListItem GetComboBoxValue(object EditValue)
        {
            if (EditValue is aetpComboBoxItem)
            {
                var value = EditValue as aetpComboBoxItem;
                return new DictionaryListItem() { Active = true, Code = value.ID, Description = value.Name };
            }
            else if (EditValue is DictionaryListItem)
            {
                return EditValue as DictionaryListItem;
            }
            return new DictionaryListItem() { Active = true, Code = "", Description = "" };
        }

        public static void NormalizeGrid(BaseControl x)
        {
            if (x is Grid)
            {
                Grid grid = x as Grid;
                foreach (GridRow row in grid.Rows)
                {
                    row.Grid = grid;
                    foreach (GridCellItem cell in row.Cells)
                    {
                        cell.Grid = grid;
                    }
                }
                foreach (GridColumn column in grid.Columns)
                {
                    column.grid = grid;
                }
            }
        }

        public static void NormalizeGrid(List<BaseControl> children)
        {
            ReportUtils.DoAction(children, (x) =>
            {
                NormalizeGrid(x);
            });
        }

        public static DictionaryListItem GetDictionaryItem(DictionaryList list, string name)
        {
            return list.Items.Where(x => x.Description == name).SingleOrDefault();
        }

        public static DictionaryListItem GetDictionaryItemDescription(DictionaryList list, string code)
        {
            return list.Items.Where(x => x.Code == code).SingleOrDefault();
        }

        public static void RenameChildren(List<aetp.Common.ControlModel.BaseControl> children, string postfix, string parentPostfix, int index)
        {
            foreach (var item in children)
            {
                if (item is Page)
                {
                    Page p = item as Page;
                    p.ParentPrefix += parentPostfix;
                    p.Index = index;
                    RenameChildren(item.Children, postfix, parentPostfix, index);
                }
                if (item is MetaConrol)
                {
                    RenameChildrenMetaControl(item.Children, postfix, parentPostfix, index);
                }
                item.Name += postfix;
            }
        }

        static void RenameChildrenMetaControl(List<aetp.Common.ControlModel.BaseControl> children, string postfix, string parentPostfix, int index)
        {
            foreach (var item in children)
            {
                if (item is MetaConrol)
                {
                    RenameChildrenMetaControl(item.Children, postfix, parentPostfix, index);
                }
                item.Name += postfix;
            }
        }

        public static bool IsNameInColumn(GridAddedPage grid, string name)
        {
            var a = (from c in grid.Columns where c.FieldName == Utils.Utils.GetFieldName(name) select c).FirstOrDefault();
            return a != null;
        }

        public static List<BaseControl> GetChildren(BaseControl bc)
        {
            List<BaseControl> list = new List<BaseControl>();
            DoAction(bc.Children, x => list.Add(x));
            return list;
        }
    }
}
