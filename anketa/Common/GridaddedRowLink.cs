﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting;
using aetp.Common.ControlModel;
using System.Data;
using System.Windows;

namespace aetp.Common.Control
{
    public class GridAddedRowLink : DevExpress.XtraPrinting.Link
    {
        GridAddedRow grid;
        DataTable dt;
        Font font;
        float[] columnwidth;
        bool calcOptimal;

        public GridAddedRowLink(GridAddedRow grid, Font font, bool calcOptimal)
        {
            this.grid = grid;
            dt = PrepareDataTableFromGridAddedRow(this.grid);
            this.font = font;
            this.calcOptimal = calcOptimal;
        }

        protected override void CreateReportHeader(BrickGraphics graph)
        {
            base.CreateReportHeader(graph);
            columnwidth = GridLink.getColumnWidth(graph, dt, calcOptimal);
            if (grid.HideHeader) return;
            graph.BackColor = Color.Gray;
            graph.Font = font;
            graph.BorderColor = Color.Black;
            graph.BorderWidth = 1;
            float x = 0;
            float max_height = 0;
            int i = 0;
            foreach (var col in grid.Columns)
            {
                SizeF textSize = graph.MeasureString(col.TextLabel, (int)columnwidth[i]);
                if (textSize.Height > max_height)
                {
                    max_height = textSize.Height;
                }
                i++;
            }

            for (int c = 0; c < grid.Columns.Count; c++)
            {
                string caption = grid.Columns[c].Caption;
                graph.DrawString(caption, Color.Black,
                    new RectangleF(x, 0, columnwidth[c], max_height),
                    BorderSide.All);
                x += columnwidth[c];
            }
        }

        protected override void CreateDetail(BrickGraphics graph)
        {
            graph.BackColor = Color.Transparent;
            graph.Font = font;
            graph.BorderColor = Color.Black;
            graph.BorderWidth = 1;
            float y = 0;
            float max_height = 0;
            float width = (graph.ClientPageSize.Width - 1) / grid.Columns.Count;
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                float x = 0;
                max_height = GridLink.getMaxRowHeight(graph, dt.Rows[r], columnwidth);
                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    graph.DrawString(dt.Rows[r][c].ToString(), Color.Black,
                        new RectangleF(x, y, columnwidth[c], 
                        max_height), 
                        BorderSide.All);
                    x += columnwidth[c];
                }
                y += max_height;
            }
        }

        /// <summary>
        /// Приведение данных в таблицах с добавлением строк к виду необходимому для печати/экспорта
        /// Например поменять разделительв дробных числах
        /// </summary>
        /// <param name="grid">GridAddedRow</param>
        /// <param name="dt">DataTable</param>
        /// <returns></returns>
        static DataTable PrepareDataTableFromGridAddedRow(GridAddedRow grid)
        {

            DataTable newdt = new System.Data.DataTable();
            for (int r = 0; r < grid.Columns.Count; r++)
            {
                DataColumn col = new System.Data.DataColumn();
                col.ColumnName = grid.Columns[r].Name;
                newdt.Columns.Add(col);
            }
            if (grid.Data == null)
                return newdt;
            for (int r = 0; r < grid.Data.Rows.Count; r++)
            {
                DataRow dr = newdt.NewRow();
                for (int c = 0; c < grid.Columns.Count; c++)
                {
                    try
                    {
                        if (grid.Columns[c].Type == GridCellType.Label)
                        {
                            grid.Data.Rows[r][c] = grid.Columns[c].TextLabel;
                        }
                        if (grid.Data.Rows[r][c] != null && !String.IsNullOrEmpty(grid.Data.Rows[r][c].ToString()))
                        {
                            if (grid.Columns[c].Type == GridCellType.Double)
                            {
                                dr[c] = grid.Data.Rows[r][c].ToString().Replace(".", ",");
                                dr[c] = Printing.FormatDoubleForPrint(dr[c].ToString());
                            }
                            else if (grid.Columns[c].Type == GridCellType.Date)
                            {
                                dr[c] = Convert.ToDateTime(grid.Data.Rows[r][c].ToString()).ToShortDateString();
                            }
                            else if (grid.Columns[c].Type == GridCellType.Check)
                            {
                                bool value = (bool)Convert.ToBoolean(grid.Data.Rows[r][c].ToString());
                                if (value == true)
                                    dr[c] = "Да";
                                else
                                    dr[c] = "Нет";

                            }
                            else if (grid.Columns[c].Type == GridCellType.List)
                            {
                                var item = ReportUtils.GetDictionaryItemDescription(grid.Columns[c].Source, grid.Data.Rows[r][c].ToString());
                                if (item != null)
                                    dr[c] = item.ToString();
                            }
                            else if (grid.Columns[c].Type == GridCellType.Integer)
                            {
                                dr[c] = grid.Data.Rows[r][c];
                                dr[c] = Printing.FormatDoubleForPrint(dr[c].ToString());
                            }
                            else
                            {
                                dr[c] = grid.Data.Rows[r][c];
                            }
                        }
                        else if (grid.Columns[c].Type == GridCellType.Date)
                        {
                            DateTime date;
                            if (grid.Data.Rows[r][c] != null && DateTime.TryParse(grid.Data.Rows[r][c].ToString(), out date))
                            {
                                dr[c] = date.ToString("dd.MM.yyyy");
                            }
                        }
                        else if (grid.Columns[c].Type == GridCellType.List)
                        {
                            var item = ReportUtils.GetDictionaryItemDescription(grid.Columns[c].Source, grid.Data.Rows[r][c].ToString());
                            if (item != null)
                                dr[c] = item.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "Значение" + grid.Data.Rows[r][c].ToString());
                    }
                }
                newdt.Rows.Add(dr);
            }
            return newdt;
        }
    }
}
