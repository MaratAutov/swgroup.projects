﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting;

namespace aetp.Common.Control
{
    public class SignLink : Link
    {
        public SignLink()
            : base()
        {           
            foreach (OrganizationDataConfig.OrganizationDataInfo item in AppGlobal.Instance.OrganizationConfig.ListOrganizationDataInfo)
                        {
                             if (item.Name == "ФИОРуководителя")
                                 fio = item.Data;
                             if (item.Name == "Должность")
                                 director_sign = item.Data;
                        }            
        }

        float start_y = 100.0f;
        string director_sign = string.Empty;
        string fio = string.Empty;
        string mp = "Дата                                     М.П.";

        public override void CreateDocument(PrintingSystem ps)
        {
            base.CreateDocument(ps);
        }

        protected override void CreateDetail(BrickGraphics graph)
        {
            base.CreateDetail(graph);

            graph.StringFormat = new BrickStringFormat(StringAlignment.Near);
            graph.BackColor = Color.Transparent;
            graph.BorderWidth = 0;
            Font font = new Font("Times New Roman",11, FontStyle.Italic | FontStyle.Bold);
            graph.Font = font;
            SizeF textSize1 = graph.MeasureString(director_sign, (int)graph.ClientPageSize.Width);
            RectangleF rect1 = new RectangleF(0, start_y, textSize1.Width, textSize1.Height);
            graph.DrawString(director_sign, rect1);

            graph.StringFormat = new BrickStringFormat(StringAlignment.Near);
            graph.BackColor = Color.Transparent;
            graph.BorderWidth = 0;
            graph.Font = new Font("Times New Roman", 11, FontStyle.Regular);
            SizeF textSize2 = graph.MeasureString(fio, (int)graph.ClientPageSize.Width);
            RectangleF rect2 = new RectangleF(textSize1.Width, start_y, textSize2.Width, textSize2.Height);

            graph.DrawString(fio, rect2);

            float max_y = textSize1.Height > textSize2.Height ? textSize1.Height : textSize2.Height;
           
            graph.Font = SystemFonts.DefaultFont;
            graph.Font = new Font("Times New Roman",11,FontStyle.Regular);
            SizeF textSize3 = graph.MeasureString(mp, (int)graph.ClientPageSize.Width);
            RectangleF rect3 = new RectangleF(0, start_y + max_y + 35, textSize3.Width, textSize3.Height);
            graph.DrawString(mp, rect3);
        }
    }
}
