﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraTreeList;
using DevExpress.XtraBars.Ribbon;
using aetp.Common.Mediator;
using aetp.Common.ControlModel;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using System.Collections;

namespace aetp.Common.View
{
    public class BaseView : RibbonForm, ITreeListWindow
    {
        public virtual Report Report { get; set; }
        public virtual TreeList tlControls { get; set; }
                
        public TreeListNode GetFocusedTreeListNode()
        {
            return tlControls.FocusedNode;
        }

        public BaseControl GetFocusedControl()
        {
            TreeListNode node = GetFocusedTreeListNode();
            if (node != null)
                return (BaseControl)node.Tag;
            else
                return null;
        }


    }
}
