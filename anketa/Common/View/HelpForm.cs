﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace aetp.Common.View
{
    public partial class HelpForm : DevExpress.XtraEditors.XtraForm
    {
        public HelpForm()
        {
            InitializeComponent();
            //wb.IsWebBrowserContextMenuEnabled = false;
        }

        private void AboutProgram_Load(object sender, EventArgs e)
        {
            try
            {
                rtbHelp.Text = File.ReadAllText("INSTR_ALL.rtf");
                //wb.DocumentText = File.ReadAllText("help.htm");
            }
            catch
            {
                rtbHelp.Text = "Не удалось загрузить файл помощи...";
                //wb.DocumentText = "Не удалось загрузить файл помощи...";
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
