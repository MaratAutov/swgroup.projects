﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraTreeList;
using aetp.Common.ControlModel;
using aetp.Common.Mediator;
using System.Collections;

namespace aetp.Common.View
{
    public interface ITreeListWindow
    {
        TreeList tlControls { get; set; }
        BaseControl GetFocusedControl();
        Report Report { get; set; }
    }
}
