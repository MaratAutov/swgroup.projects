﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using aetp.Common.ControlModel;
using DevExpress.XtraTreeList;
using aetp.Common.Validator;
using System.Windows.Forms;
using aetp.Common.Control;
using System.Data;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraRichEdit;
using System.Drawing;

namespace aetp.Common.View
{
    public class EditorModel
    {
        IEditorWindow window;
        public PageManager pageManager;

        

        public EditorModel(IEditorWindow window)
        {
            this.window = window;
            pageManager = new PageManager(window);
        }

        public List<aetp.Common.ControlModel.BaseControl> basecontrols = new List<ControlModel.BaseControl>();
        public Page CurrentPage;
        
        Report _report;
        public Report Report
        {
            get
            {
                return _report;
            }
            set
            {
                _report = value;
                GetChildren(_report);
            }
        }

        public void ZeroFill()
        {
            if (CurrentPage != null)
            {
                ReportUtils.PageDoAction(CurrentPage.Children, new Action<aetp.Common.ControlModel.BaseControl>(ZeroFill));
            }
        }

        private void ZeroFill(aetp.Common.ControlModel.BaseControl bc)
        {
            int _value = 0;
            if (bc is Grid)
            {
                Grid model = bc as Grid;
                DataTable dt = model.Data;
                
                for (int r = 0; r < dt.Rows.Count; r++)
                {
                    GridCellCollection cells = model.Rows[r].Cells;
                    for (int c = 0; c < dt.Columns.Count; c++)
                    {
                        GridCellItem se = cells[c];
                        if ((model.Rows[r].Cells[c].Type == GridCellType.Double ||
                            model.Rows[r].Cells[c].Type == GridCellType.Integer) &&
                            (dt.Rows[r][c] == DBNull.Value || dt.Rows[r][c] == null))
                        {
                            dt.Rows[r][c] = _value;
                            se.EditValue = _value;
                        }
                    }
                }
            }
            if (bc is GridAddedRow)
            {
                GridAddedRow model = bc as GridAddedRow;
                DataTable dt = model.Data;
                
                for (int r = 0; r < dt.Rows.Count; r++)
                {                    
                    for (int c = 0; c < dt.Columns.Count; c++)
                    {
                        if ((model.Columns[c].Type == GridCellType.Double ||
                            model.Columns[c].Type == GridCellType.Integer) &&
                            (dt.Rows[r][c] == DBNull.Value || dt.Rows[r][c] == null || dt.Rows[r][c].Equals(0)))
                        {
                            dt.Rows[r][c] = _value;
                            (model.gc.Views[0] as DevExpress.XtraGrid.Views.Grid.GridView).SetRowCellValue(r, (model.gc.Views[0] as DevExpress.XtraGrid.Views.Grid.GridView).Columns[c], _value);
                        }
                    }
                }
                dt.AcceptChanges();
            }
        }

        public void GetChildren(Page p)
        {
            if (p == null)
                return;
            foreach (aetp.Common.ControlModel.BaseControl ctrl in p.Children)
            {
                if (ctrl is Page) GetChildren(ctrl as Page);
                if (ctrl is MetaConrol) GetChildrenMetaControl(ctrl as MetaConrol);              
                AddControlToList(ctrl);
            }
        }

        private void AddControlToList(aetp.Common.ControlModel.BaseControl control)
        {
            bool exist = false;
            foreach (aetp.Common.ControlModel.BaseControl ctrl in basecontrols)
            {
                if (ctrl.Name == control.Name)
                    exist = true;
            }
            if (!exist)
                basecontrols.Add(control);
        }

        public void GetChildrenMetaControl(MetaConrol p)
        {
            foreach (aetp.Common.ControlModel.BaseControl ctrl in p.Children)
            {
                if (ctrl is Page) GetChildren(ctrl as Page);
                if (ctrl is MetaConrol) 
                    GetChildrenMetaControl(ctrl as MetaConrol);               
                AddControlToList(ctrl);
            }
        }

        public bool IsLoadPage(Page page)
        {
            return page.IsShownPanel || page.IsMandatoryToFill;
        }
                
        public void ReInit()
        {
            if (Report != null)
                ValidationCollection.ReInit(Report.guid);
            pageManager.ReInit();
            basecontrols.Clear();
        }

        public void ShowFirstPage()
        {
            if (Report != null)
            {
                CurrentPage = Report;
                pageManager.ShowPage(Report);
            }
        }

        public ScrollableControl GetPage(Page page)
        {
            return pageManager.GetPanel(page.Name);
        }

        public void ShowPage(Page page, bool create = false)
        {
            CurrentPage = page;
            pageManager.ShowPage(page, create);
            SectionStoreButtonEnabled();
        }

        void SectionStoreButtonEnabled()
        {
            if (CurrentPage != null && !(CurrentPage is Report))
            {
                window.SaveSectionButton.Enabled = true;
                window.LoadSectionButton.Enabled = true;
            }
            else
            {
                window.SaveSectionButton.Enabled = false;
                window.LoadSectionButton.Enabled = false;
            }
        }
        
        void EditMainForm_Navigate(object sender, aetp.Common.Control.NavigateEventArgs args)
        {
            ShowPage(args.newPage);
        }

        void aetp_ControlEditValue_Changed(object sender, EventArgs e)
        {            
            Report.ReportChange = true;
            RaiseEditValueChanged(e);
        }

        public event EventHandler EditValueChanged;
        protected void RaiseEditValueChanged(EventArgs e)
        {
            if (EditValueChanged != null)
                EditValueChanged(this, e);
        }
        
        public List<Page> list_page = new List<Page> { };

        public void GetChildrenPage(aetp.Common.ControlModel.BaseControl parent)
        {
            foreach (aetp.Common.ControlModel.BaseControl control in parent.Children)
            {
                if (control is Page)
                {
                    list_page.Add(control as Page);
                    GetChildrenPage(control);
                }
            }
        }

        public void ClearCurrentPage()
        {
            ReportUtils.DoAction(CurrentPage.Children, x =>
                {
                    x.ClearProperty();
                    if (x is Page)
                    {
                        (x as Page).IsShownPanel = false;
                    }
                });
            CurrentPage.IsShownPanel = false;
            pageManager.DisposePage(CurrentPage);
        }
                
    }

    public class ValidatingEventArgs
    {
        public Page Page { get; set; }
        public bool IsRecurently { get; set; }
    }
    public delegate void ValidatingEventHandler(object sender, ValidatingEventArgs e);

    public enum ChangeType
    {
        Add,
        Remove
    }

    public class PageViewInfoCollectionChangeEventArgs
    {
        public PageViewInfo page;
        public ChangeType type;
    }

    public delegate void PageViewInfoCollectionChangeEventHandler(object sender, PageViewInfoCollectionChangeEventArgs e);

    public class PageViewInfo
    {
        public ScrollableControl panel { get; set; }
        public Page page { get; set; }
    }
}
