﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using aetp.Common.Control;
using System.Data;

namespace aetp.Common.View
{
    public class PageControlBinding
    {
        public static void BindingGrid(aetpGrid ctrl, Grid model)
        {
            if (!model.Data.ExtendedProperties.ContainsKey("model"))
                model.Data.ExtendedProperties.Add("model", model);
            ctrl.gc.DataSource = model.Data;
            model.Data.RowChanged += new DataRowChangeEventHandler(Data_RowChanged);
            model.Data.ColumnChanged += new DataColumnChangeEventHandler(Data_ColumnChanged);
        }

        static void Data_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            var model = (sender as DataTable).ExtendedProperties["model"] as Grid;
            DataColumnChangeWrapper wrapper = new DataColumnChangeWrapper(e, model);
            foreach (int col in wrapper.ColumnsIndex)
            {
                model.Rows[wrapper.RowIndex].Cells[col].EditValue = wrapper.Row[col];
            }
        }

        static void Data_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            var model = (sender as DataTable).ExtendedProperties["model"] as Grid;
            DataRowChangeWrapper wrapper = new DataRowChangeWrapper(e, model);
            foreach (int col in wrapper.ColumnsIndex)
            {
                model.Rows[wrapper.RowIndex].Cells[col].EditValue = wrapper.Row[col];
            }
        }

        public static void BindingGridAddedRow(aetpGridAddedRow ctrl, GridAddedRow model)
        {
            ctrl.DataSource = model.Data;
            ctrl.GridChanged += new aetpGridAddedRow.AddedGridChangedHandler(ctrl_GridChanged);
        }

        static void ctrl_GridChanged(object sender, DataAddedRowChangeEventArgs e)
        {
            var grid = sender as aetpGridAddedRow;
            GridAddedRow model = grid.model;
            model.Data = grid.dt;
        }

        public static void BindGridAddedPage(aetpGridAddedPage ctrl, GridAddedPage model)
        {
            ctrl.gc.DataSource = model.Data;
        }

        public static void BindingMetaGrid(aetpMetaGrid grid, MetaGrid model)
        {
            for (int i = 0; i < model.Tables.Count; i++)
            {
                if (model.Tables[i] is Grid)
                {
                    BindingGrid(grid.Grids[i] as aetpGrid, model.Tables[i] as Grid);
                }
                else
                {
                    BindingGridAddedRow(grid.Grids[i] as aetpGridAddedRow, model.Tables[i] as GridAddedRow);
                }
            }
        }
    }

    public class DataRowChangeWrapper
    {
        public int RowIndex { get; private set; }
        public DataRowAction Action { get; private set; }
        public List<int> ColumnsIndex { get; private set; }
        public DataRow Row { get; private set; }

        public DataRowChangeWrapper(DataRowChangeEventArgs e, Grid grid)
        {
            RowIndex = e.Row.Table.Rows.IndexOf(e.Row);
            Action = e.Action;
            Row = e.Row;
            ColumnsIndex = new List<int>();
            if (Action == DataRowAction.Change)
            {
                for (int c = 0; c < grid.Columns.Count; c++)
                {
                    if (!Equal(grid.Rows[RowIndex].Cells[c].EditValue, e.Row[c]))
                    {
                        ColumnsIndex.Add(c);
                    }
                }
            }
        }

        public bool Equal(object obj1, object obj2)
        {
            if ((obj1 == null || obj1 == DBNull.Value) &&
                 obj2 == null || obj2 == DBNull.Value)
            {
                return true;
            }
            else
            {
                return Equals(obj1, obj2);
            }
        }
    }

    public class DataColumnChangeWrapper
    {
        public int RowIndex { get; private set; }
        public List<int> ColumnsIndex { get; private set; }
        public DataRow Row { get; private set; }

        public DataColumnChangeWrapper(DataColumnChangeEventArgs e, Grid grid)
        {
            RowIndex = e.Row.Table.Rows.IndexOf(e.Row);
            Row = e.Row;
            ColumnsIndex = new List<int>();
            for (int c = 0; c < grid.Columns.Count; c++)
            {
                if (!Equal(grid.Rows[RowIndex].Cells[c].EditValue, e.Row[c]))
                {
                    ColumnsIndex.Add(c);
                }
            }
        }

        public bool Equal(object obj1, object obj2)
        {
            if ((obj1 == null || obj1 == DBNull.Value) &&
                 obj2 == null || obj2 == DBNull.Value)
            {
                return true;
            }
            else
            {
                return Equals(obj1, obj2);
            }
        }
    }
}
