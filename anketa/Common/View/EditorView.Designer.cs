﻿namespace aetp.Common.View
{
    partial class EditorView
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorView));
            this.tlMain = new DevExpress.XtraTreeList.TreeList();
            this.colCaption = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panel_file = new DevExpress.XtraEditors.PanelControl();            
            this.contentPanel = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnValidatePage = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblPageCaption = new DevExpress.XtraEditors.LabelControl();
            this.btnClearPage = new DevExpress.XtraEditors.SimpleButton();
            this.btnCreatePage = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadSection = new DevExpress.XtraEditors.SimpleButton();
            this.btnSaveSection = new DevExpress.XtraEditors.SimpleButton();
            this.btnZeroFill = new DevExpress.XtraEditors.SimpleButton();
            this.btn_file = new DevExpress.XtraEditors.DropDownButton();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bbiSaveAs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLoadFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAboutProgram = new DevExpress.XtraBars.BarButtonItem();
			//this.bbiHelp = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSavePacket = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendPacket = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPrintPacket = new DevExpress.XtraBars.BarButtonItem();
            this.bbiConvertPacket = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDocumentsSubmitterOpen = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreateNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiValidate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOpenFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddSign = new DevExpress.XtraBars.BarButtonItem();
            this.btn_export = new DevExpress.XtraBars.BarSubItem();
            this.bci_export_pdf = new DevExpress.XtraBars.BarCheckItem();
            this.bci_export_rtf = new DevExpress.XtraBars.BarCheckItem();
            this.bci_export_excel = new DevExpress.XtraBars.BarCheckItem();
            this.bbiExportXtddExcel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiImportModels = new DevExpress.XtraBars.BarButtonItem();
            this.rpAction = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgAction = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiAttachFile = new DevExpress.XtraBars.BarButtonItem();
            this.rpgError = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgOrganization = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiOrganizationData = new DevExpress.XtraBars.BarButtonItem();
            this.rpgSendPacket = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgPrint = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgExport = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgConvert = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgAboutProgram = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiNetworkSettings = new DevExpress.XtraBars.BarButtonItem();
            this.popup_container = new DevExpress.XtraBars.PopupControlContainer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.tlMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel_file)).BeginInit();
            this.panel_file.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popup_container)).BeginInit();
            this.SuspendLayout();
            // 
            // tlMain
            // 
            this.tlMain.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colCaption});
            this.tlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlMain.Location = new System.Drawing.Point(0, 0);
            this.tlMain.Name = "tlMain";
            this.tlMain.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.tlMain.Size = new System.Drawing.Size(250, 557);
            this.tlMain.StateImageList = this.imageList1;
            this.tlMain.TabIndex = 8;
            // 
            // colCaption
            // 
            this.colCaption.Caption = "Наименование";
            this.colCaption.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colCaption.FieldName = "Caption";
            this.colCaption.MinWidth = 33;
            this.colCaption.Name = "colCaption";
            this.colCaption.Visible = true;
            this.colCaption.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "page_white.png");
            this.imageList1.Images.SetKeyName(1, "text_editor.png");
            this.imageList1.Images.SetKeyName(2, "grid.png");
            this.imageList1.Images.SetKeyName(3, "ui_combo_box_blue.png");
            this.imageList1.Images.SetKeyName(4, "stock_form_combobox.png");
            this.imageList1.Images.SetKeyName(5, "hyperlink_blue (1).png");
            this.imageList1.Images.SetKeyName(6, "accessories_dictionary (1).png");
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 144);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.tlMain);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1136, 557);
            this.splitContainerControl1.SplitterPosition = 250;
            this.splitContainerControl1.TabIndex = 9;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(100, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.panel_file);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.contentPanel);
            this.splitContainerControl2.Panel2.Controls.Add(this.panelControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(500, 557);
            this.splitContainerControl2.SplitterPosition = 1;
            this.splitContainerControl2.TabIndex = 60;
            this.splitContainerControl2.Text = "splitContainerControl2";
            this.splitContainerControl2.Horizontal = false;
            // 
            // panel_file
            // 
            //this.panel_file.Controls.Add(this.flow_panel);
            this.panel_file.Dock = System.Windows.Forms.DockStyle.Fill;            
            this.panel_file.Location = new System.Drawing.Point(0, 0);
            this.panel_file.Name = "panel_file";
            this.panel_file.Size = new System.Drawing.Size(0, 0);
            this.panel_file.TabIndex = 1;
            this.panel_file.Visible = false;
            
            // 
            // contentPanel
            // 
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(0, 65);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(0, 0);
            this.contentPanel.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnValidatePage);
            this.panelControl1.Controls.Add(this.pictureBox1);
            this.panelControl1.Controls.Add(this.lblPageCaption);
            this.panelControl1.Controls.Add(this.btnClearPage);
            this.panelControl1.Controls.Add(this.btnCreatePage);
            this.panelControl1.Controls.Add(this.btnLoadSection);
            this.panelControl1.Controls.Add(this.btnSaveSection);
            this.panelControl1.Controls.Add(this.btnZeroFill);
            this.panelControl1.Controls.Add(this.btn_file);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(0, 65);
            this.panelControl1.TabIndex = 1;
            // 
            // btnValidatePage
            // 
            this.btnValidatePage.Location = new System.Drawing.Point(236, 36);
            this.btnValidatePage.Name = "btnValidatePage";
            this.btnValidatePage.Size = new System.Drawing.Size(75, 23);
            this.btnValidatePage.TabIndex = 4;
            this.btnValidatePage.Text = "Проверить";
            this.btnValidatePage.Click += new System.EventHandler(this.btnValidatePage_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::aetp.Common.Properties.Resources.info_box_blue;
            this.pictureBox1.Location = new System.Drawing.Point(20, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(33, 32);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // lblPageCaption
            // 
            this.lblPageCaption.Location = new System.Drawing.Point(74, 7);
            this.lblPageCaption.Name = "lblPageCaption";
            this.lblPageCaption.Size = new System.Drawing.Size(0, 13);
            this.lblPageCaption.TabIndex = 2;
            // 
            // btnClearPage
            // 
            this.btnClearPage.Enabled = false;
            this.btnClearPage.Location = new System.Drawing.Point(155, 36);
            this.btnClearPage.Name = "btnClearPage";
            this.btnClearPage.Size = new System.Drawing.Size(75, 23);
            this.btnClearPage.TabIndex = 1;
            this.btnClearPage.Text = "Очистить";
            this.btnClearPage.Click += new System.EventHandler(this.btnClearPage_Click_1);
            // 
            // btnCreatePage
            // 
            this.btnCreatePage.Enabled = false;
            this.btnCreatePage.Location = new System.Drawing.Point(74, 36);
            this.btnCreatePage.Name = "btnCreatePage";
            this.btnCreatePage.Size = new System.Drawing.Size(75, 23);
            this.btnCreatePage.TabIndex = 0;
            this.btnCreatePage.Text = "Создать";
            this.btnCreatePage.Click += new System.EventHandler(this.btnCreatePage_Click_1);
            // 
            // btnLoadSection
            // 
            this.btnLoadSection.Enabled = false;
            this.btnLoadSection.Location = new System.Drawing.Point(400, 36);
            this.btnLoadSection.Name = "btnLoadSection";
            this.btnLoadSection.Size = new System.Drawing.Size(75, 23);
            this.btnLoadSection.TabIndex = 0;
            this.btnLoadSection.Text = "Загрузить";
            this.btnLoadSection.Click += new System.EventHandler(this.btnLoadSection_Click);
            // 
            // btnSaveSection
            // 
            this.btnSaveSection.Enabled = false;
            this.btnSaveSection.Location = new System.Drawing.Point(480, 36);
            this.btnSaveSection.Name = "btnSaveSection";
            this.btnSaveSection.Size = new System.Drawing.Size(75, 23);
            this.btnSaveSection.TabIndex = 0;
            this.btnSaveSection.Text = "Сохранить";
            this.btnSaveSection.Click += new System.EventHandler(this.btnSaveSection_Click);
            // 
            // btnZeroFill
            // 
            this.btnZeroFill.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnZeroFill.Location = new System.Drawing.Point(700, 36);
            this.btnZeroFill.Name = "btnZeroFill";
            this.btnZeroFill.Size = new System.Drawing.Size(150, 23);
            this.btnZeroFill.TabIndex = 3;
            this.btnZeroFill.Text = "Заполнить таблицы";
            this.btnZeroFill.ToolTip = "Заполняет пустые числовые ячейки, значением нуля";
            this.btnZeroFill.ToolTipTitle = "Заполнить таблицы";
            this.btnZeroFill.Click += new System.EventHandler(this.btnZeroFill_Click);
            // 
            // btn_file
            // 
            this.btn_file.DropDownControl = this.popup_container;
            this.btn_file.Image = global::aetp.Common.Properties.Resources.attach20;
            this.btn_file.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_file.Location = new System.Drawing.Point(317, 36);
            this.btn_file.MenuManager = this.ribbon.Manager;
            this.btn_file.Name = "btn_file";
            this.btn_file.Size = new System.Drawing.Size(35, 23);
            this.btn_file.TabIndex = 2;
            this.btn_file.Visible = false;
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonText = null;
            this.ribbon.ApplicationIcon = global::aetp.Common.Properties.Resources.anketa;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.ExpandCollapseItem.Name = "";
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.bbiSaveAs,
            this.bbiSave,
            this.bbiLoadFile,
            this.bbiAboutProgram,
            //this.bbiHelp,
            this.bbiSavePacket,
            this.bbiSendPacket,
            this.bbiPrintPacket,
            this.bbiConvertPacket,
            this.bbiDocumentsSubmitterOpen,
            this.bbiCreateNew,
            this.bbiValidate,
            this.bbiOpenFile,
            this.bbiAddSign,
            this.btn_export,
            this.bci_export_pdf,
            this.bci_export_rtf,
            this.bci_export_excel,
            this.bbiExportXtddExcel,
            this.bbiImportModels});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 19;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpAction});
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1136, 144);
            this.ribbon.Toolbar.ItemLinks.Add(this.bbiCreateNew);
            this.ribbon.Toolbar.ItemLinks.Add(this.bbiSaveAs);
            this.ribbon.Toolbar.ItemLinks.Add(this.bbiLoadFile);
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            this.ribbon.ShowCustomizationMenu += new DevExpress.XtraBars.Ribbon.RibbonCustomizationMenuEventHandler(this.ribbon_ShowCustomizationMenu);
            // 
            // bbiSaveAs
            // 
            this.bbiSaveAs.Caption = "Сохранить отчёт как...";
            this.bbiSaveAs.Glyph = global::aetp.Common.Properties.Resources._32_сохранить;
            this.bbiSaveAs.Id = 4;
            this.bbiSaveAs.Name = "bbiSaveAs";
            this.bbiSaveAs.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSaveAs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveAs_ItemClick);
            // 
            // bbiHelp
            // 
            /*
            this.bbiHelp.Caption = "Помощь";
            this.bbiHelp.Glyph = global::aetp.Common.Properties.Resources.about_program;
            this.bbiHelp.Id = 32;
            this.bbiHelp.Name = "bbiHelp";
            this.bbiHelp.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiHelp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiHelp_ItemClick);
             */
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить отчёт";
            this.bbiSave.Enabled = false;
            this.bbiSave.Glyph = global::aetp.Common.Properties.Resources._32_сохранить;
            this.bbiSave.Id = 21;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // bbiLoadFile
            // 
            this.bbiLoadFile.Caption = "Открыть...";
            this.bbiLoadFile.Glyph = global::aetp.Common.Properties.Resources._32_открыть;
            this.bbiLoadFile.Id = 5;
            this.bbiLoadFile.Name = "bbiLoadFile";
            this.bbiLoadFile.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiLoadFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadFile_ItemClick);
            // 
            // bbiAboutProgram
            // 
            this.bbiAboutProgram.Caption = "Новое в программе";
            this.bbiAboutProgram.Glyph = global::aetp.Common.Properties.Resources.about_program;
            this.bbiAboutProgram.Id = 32;
            this.bbiAboutProgram.Name = "bbiAboutProgram";
            this.bbiAboutProgram.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAboutProgram.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAboutProgram_ItemClick);
            // 
            // bbiSavePacket
            // 
            this.bbiSavePacket.Caption = "Сформировать пакет для отправки";
            this.bbiSavePacket.Glyph = global::aetp.Common.Properties.Resources.save_packet;
            this.bbiSavePacket.Id = 6;
            this.bbiSavePacket.Name = "bbiSavePacket";
            this.bbiSavePacket.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSavePacket.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSavePacket_ItemClick);
            // 
            // bbiSendPacket
            // 
            this.bbiSendPacket.Caption = "Отправка пакета";
            this.bbiSendPacket.Glyph = global::aetp.Common.Properties.Resources.email_send;
            this.bbiSendPacket.Id = 7;
            this.bbiSendPacket.Name = "bbiSendPacket";
            this.bbiSendPacket.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSendPacket.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendPacket_ItemClick);
            // 
            // bbiPrintPacket
            // 
            this.bbiPrintPacket.Caption = "Печать";
            this.bbiPrintPacket.Glyph = global::aetp.Common.Properties.Resources.printer_3;
            this.bbiPrintPacket.Id = 10;
            this.bbiPrintPacket.Name = "bbiPrintPacket";
            this.bbiPrintPacket.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiPrintPacket.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrintPacket_ItemClick);
            // 
            // bbiConvertPacket
            // 
            this.bbiConvertPacket.Caption = "Конвертировать";
            this.bbiConvertPacket.Glyph = global::aetp.Common.Properties.Resources.convert;
            this.bbiConvertPacket.Id = 42;
            this.bbiConvertPacket.Name = "bbiConvertPacket";
            this.bbiConvertPacket.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiConvertPacket.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiConvertPacket_ItemClick);
            // 
            // bbiDocumentsSubmitterOpen
            // 
            this.bbiDocumentsSubmitterOpen.Caption = "Окно отправки пакетов";
            this.bbiDocumentsSubmitterOpen.Enabled = false;
            this.bbiDocumentsSubmitterOpen.Glyph = global::aetp.Common.Properties.Resources.gnome_mail_send_receive;
            this.bbiDocumentsSubmitterOpen.Id = 8;
            this.bbiDocumentsSubmitterOpen.Name = "bbiDocumentsSubmitterOpen";
            this.bbiDocumentsSubmitterOpen.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiDocumentsSubmitterOpen.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiDocumentsSubmitterOpen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDocumentsSubmitterOpen_ItemClick);
            // 
            // bbiCreateNew
            // 
            this.bbiCreateNew.Caption = "Создать";
            this.bbiCreateNew.Glyph = global::aetp.Common.Properties.Resources.new_in_programm;
            this.bbiCreateNew.Id = 9;
            this.bbiCreateNew.Name = "bbiCreateNew";
            this.bbiCreateNew.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiCreateNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreateNew_ItemClick);
            // 
            // bbiValidate
            // 
            this.bbiValidate.Caption = "Проверить ошибки";
            this.bbiValidate.Glyph = global::aetp.Common.Properties.Resources.warning;
            this.bbiValidate.Id = 12;
            this.bbiValidate.Name = "bbiValidate";
            this.bbiValidate.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiValidate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiValidate_ItemClick);
            // 
            // bbiOpenFile
            // 
            this.bbiOpenFile.Caption = "Открыть";
            this.bbiOpenFile.Id = 13;
            this.bbiOpenFile.LargeGlyph = global::aetp.Common.Properties.Resources._32_открыть;
            this.bbiOpenFile.Name = "bbiOpenFile";
            this.bbiOpenFile.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiOpenFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadFile_ItemClick);
            // 
            // bbiAddSign
            // 
            this.bbiAddSign.Caption = "Добавить подпись";
            this.bbiAddSign.Glyph = global::aetp.Common.Properties.Resources.add_sign;
            this.bbiAddSign.Id = 14;
            this.bbiAddSign.Name = "bbiAddSign";
            this.bbiAddSign.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddSign.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddSign_ItemClick);
            // 
            // btn_export
            // 
            this.btn_export.Caption = "Экспорт";
            this.btn_export.Id = 55;
            this.btn_export.LargeGlyph = global::aetp.Common.Properties.Resources._32_export;
            this.btn_export.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bci_export_pdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.bci_export_rtf),
            new DevExpress.XtraBars.LinkPersistInfo(this.bci_export_excel)});
            this.btn_export.Name = "btn_export";
            this.btn_export.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bci_export_pdf
            // 
            this.bci_export_pdf.Caption = "PDF файл";
            this.bci_export_pdf.Glyph = global::aetp.Common.Properties.Resources._32_pdf;
            this.bci_export_pdf.Id = 16;
            this.bci_export_pdf.Name = "bci_export_pdf";
            this.bci_export_pdf.Tag = 3;
            this.bci_export_pdf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bciExportToPdf_ItemClick);
            // 
            // bci_export_rtf
            // 
            this.bci_export_rtf.Caption = "RTF файл";
            this.bci_export_rtf.Glyph = global::aetp.Common.Properties.Resources._32_rtf;
            this.bci_export_rtf.Id = 17;
            this.bci_export_rtf.Name = "bci_export_rtf";
            this.bci_export_rtf.Tag = 2;
            this.bci_export_rtf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bciExportToRtf_ItemClick);
            // 
            // bci_export_excel
            // 
            this.bci_export_excel.Caption = "EXCEL файл";
            this.bci_export_excel.Glyph = global::aetp.Common.Properties.Resources._32_excel;
            this.bci_export_excel.Id = 18;
            this.bci_export_excel.Name = "bci_export_excel";
            this.bci_export_excel.Tag = 4;
            this.bci_export_excel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bciExportToExcel_ItemClick);
            // 
            // bbiExportXtddExcel
            // 
            this.bbiExportXtddExcel.Caption = "Экспорт XTDD в Excel";
            this.bbiExportXtddExcel.Glyph = global::aetp.Common.Properties.Resources.exportXTDDtoExcel;
            this.bbiExportXtddExcel.Id = 40;
            this.bbiExportXtddExcel.Name = "bbiExportXtddExcel";
            this.bbiExportXtddExcel.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiExportXtddExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportXtddExcel_ItemClick);
            // 
            // bbiImportModels
            // 
            this.bbiImportModels.Caption = "Обновить список моделей";
            this.bbiImportModels.Id = 15;
            this.bbiImportModels.LargeGlyph = global::aetp.Common.Properties.Resources.refresh_model;
            this.bbiImportModels.Name = "bbiImportModels";
            this.bbiImportModels.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiImportModels.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiImportModels_ItemClick);
            // 
            // rpAction
            // 
            this.rpAction.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgAction,
            this.rpgError,
            this.rpgOrganization,
            this.rpgSendPacket,
            this.rpgPrint,
            this.rpgExport,
            this.rpgConvert,
            this.rpgAboutProgram});
            this.rpAction.Name = "rpAction";
            this.rpAction.Text = "Главная";
            // 
            // rpgAction
            // 
            this.rpgAction.ItemLinks.Add(this.bbiCreateNew);
            this.rpgAction.ItemLinks.Add(this.bbiSaveAs);
            this.rpgAction.ItemLinks.Add(this.bbiSave);
            this.rpgAction.ItemLinks.Add(this.bbiOpenFile);
            this.rpgAction.ItemLinks.Add(this.bbiImportModels);
            this.rpgAction.ItemLinks.Add(this.bbiAttachFile);
            this.rpgAction.Name = "rpgAction";
            this.rpgAction.Text = "Файл";
            // 
            // bbiAttachFile
            // 
            this.bbiAttachFile.Caption = "Прикрепить файл";
            this.bbiAttachFile.Id = 31;
            this.bbiAttachFile.LargeGlyph = global::aetp.Common.Properties.Resources.attach;
            this.bbiAttachFile.Name = "bbiAttachFile";
            this.bbiAttachFile.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAttachFile.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiAttachFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAttachFile_ItemClick);
            // 
            // rpgError
            // 
            this.rpgError.ItemLinks.Add(this.bbiValidate);
            this.rpgError.Name = "rpgError";
            // 
            // rpgOrganization
            // 
            this.rpgOrganization.ItemLinks.Add(this.bbiOrganizationData);
            this.rpgOrganization.Name = "rpgOrganization";
            this.rpgOrganization.Text = "Реквизиты";
            this.rpgOrganization.Visible = false;
            // 
            // bbiOrganizationData
            // 
            this.bbiOrganizationData.Caption = "Реквизиты организации";
            this.bbiOrganizationData.Glyph = global::aetp.Common.Properties.Resources.organization;
            this.bbiOrganizationData.Id = 20;
            this.bbiOrganizationData.Name = "bbiOrganizationData";
            this.bbiOrganizationData.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiOrganizationData.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiOrganizationData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiOrganizationData_ItemClick);
            // 
            // rpgSendPacket
            // 
            this.rpgSendPacket.ItemLinks.Add(this.bbiSavePacket);
            this.rpgSendPacket.ItemLinks.Add(this.bbiAddSign);
            this.rpgSendPacket.ItemLinks.Add(this.bbiSendPacket);
            this.rpgSendPacket.ItemLinks.Add(this.bbiDocumentsSubmitterOpen);
            this.rpgSendPacket.Name = "rpgSendPacket";
            this.rpgSendPacket.Text = "Отправка пакета";
            // 
            // rpgPrint
            // 
            this.rpgPrint.ItemLinks.Add(this.bbiPrintPacket);
            this.rpgPrint.Name = "rpgPrint";
            // 
            // rpgExport
            // 
            this.rpgExport.ItemLinks.Add(this.btn_export);
            this.rpgExport.ItemLinks.Add(this.bbiExportXtddExcel);
            this.rpgExport.Name = "rpgExport";
            // 
            // rpgConvert
            // 
            this.rpgConvert.ItemLinks.Add(this.bbiConvertPacket);
            this.rpgConvert.Name = "rpgConvert";
            // 
            // rpgAboutProgram
            // 
            this.rpgAboutProgram.ItemLinks.Add(this.bbiAboutProgram);
            this.rpgAboutProgram.ItemLinks.Add(this.bbiNetworkSettings);
			//this.rpgAboutProgram.ItemLinks.Add(this.bbiHelp);
            this.rpgAboutProgram.Name = "rpgAboutProgram";
            // 
            // bbiNetworkSettings
            // 
            this.bbiNetworkSettings.Caption = "Настройка соединения";
            this.bbiNetworkSettings.Glyph = global::aetp.Common.Properties.Resources.network;
            this.bbiNetworkSettings.Id = 33;
            this.bbiNetworkSettings.Name = "bbiNetworkSettings";
            this.bbiNetworkSettings.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiNetworkSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNetworkSettings_ItemClick);
            // 
            // popup_container
            // 
            this.popup_container.Appearance.BackColor = System.Drawing.Color.White;
            this.popup_container.Appearance.Options.UseBackColor = true;
            this.popup_container.AutoSize = true;
            this.popup_container.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.popup_container.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.popup_container.CloseOnLostFocus = false;
            this.popup_container.CloseOnOuterMouseClick = false;
            this.popup_container.Location = new System.Drawing.Point(0, 0);
            this.popup_container.Name = "popup_container";
            this.popup_container.Size = new System.Drawing.Size(148, 5);
            this.popup_container.TabIndex = 0;
            this.popup_container.Visible = false;
            // 
            // EditorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1136, 701);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditorView";
            this.Ribbon = this.ribbon;
            this.Text = "Анкета.Редактор";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditorView_FormClosing);
            this.Load += new System.EventHandler(this.EditorView_Load);
            this.Shown += new System.EventHandler(this.EditorView_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.tlMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel_file)).EndInit();
            this.panel_file.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popup_container)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraTreeList.TreeList tlMain;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCaption;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpAction;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgAction;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgError;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgOrganization;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSendPacket;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgPrint;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgConvert;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgAboutProgram;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgExport;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAs;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiAboutProgram;
		//private DevExpress.XtraBars.BarButtonItem bbiHelp;
        private DevExpress.XtraBars.BarButtonItem bbiNetworkSettings;
        private DevExpress.XtraBars.BarButtonItem bbiLoadFile;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarButtonItem bbiSavePacket;
        private DevExpress.XtraBars.BarButtonItem bbiPrintPacket;
        private DevExpress.XtraBars.BarButtonItem bbiConvertPacket;
        private DevExpress.XtraBars.BarButtonItem bbiExportXtddExcel;
        private DevExpress.XtraBars.BarButtonItem bbiSendPacket;
        private DevExpress.XtraBars.BarButtonItem bbiDocumentsSubmitterOpen;
        private DevExpress.XtraBars.BarButtonItem bbiCreateNew;
        private DevExpress.XtraEditors.XtraScrollableControl contentPanel;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panel_file;
        private DevExpress.XtraEditors.SimpleButton btnClearPage;
        private DevExpress.XtraEditors.SimpleButton btnCreatePage;
        private DevExpress.XtraEditors.SimpleButton btnSaveSection;
        private DevExpress.XtraEditors.SimpleButton btnLoadSection;
        private DevExpress.XtraEditors.SimpleButton btnZeroFill;
        private DevExpress.XtraEditors.LabelControl lblPageCaption;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraBars.BarButtonItem bbiValidate;
        private DevExpress.XtraEditors.SimpleButton btnValidatePage;
        private DevExpress.XtraBars.BarButtonItem bbiOpenFile;
        private DevExpress.XtraBars.BarButtonItem bbiAddSign;
        private DevExpress.XtraBars.BarButtonItem bbiImportModels;
        private DevExpress.XtraBars.BarButtonItem bbiOrganizationData;
        private DevExpress.XtraBars.BarButtonItem bbiAttachFile;
        private DevExpress.XtraEditors.DropDownButton btn_file;
        private DevExpress.XtraBars.PopupControlContainer popup_container;        

        private DevExpress.XtraBars.BarSubItem btn_export;
        private DevExpress.XtraBars.BarCheckItem bci_export_rtf;
        private DevExpress.XtraBars.BarCheckItem bci_export_pdf;
        private DevExpress.XtraBars.BarCheckItem bci_export_excel;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
      


    }
}

