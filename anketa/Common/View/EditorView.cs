﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using aetp.Common.ControlModel;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraTreeList.Nodes.Operations;
using DevExpress.XtraTreeList.Nodes;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Windows;
using DevExpress.XtraTreeList;
using aetp.Common.Mediator;
using DevExpress.XtraEditors;
using aetp.Common.Control;
using DevExpress.XtraGrid;
using System.Diagnostics;
using aetp.Common.Validator;
using System.Reflection;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using aetp.Utils;

namespace aetp.Common.View
{
    public partial class EditorView : BaseView, IEditorWindow
    {
        EditorModel model;
        public event PageViewInfoCollectionChangeEventHandler PageVisibleChanged;
        protected void RaisePageVisibleChanged(PageViewInfoCollectionChangeEventArgs e)
        {
            if (PageVisibleChanged != null)
                PageVisibleChanged(this, e);
        }
        
        public string title
        {
            get
            {
                return "Программа-анкета подготовки электронных документов версии 2.16.3 ";
            }
        }

        public override TreeList tlControls
        {
            get
            {
                return tlMain;
            }
            set
            {
                tlMain = value;
            }
        }

        public string PageCaption
        {
            get
            {
                return lblPageCaption.Text;
            }
            set
            {
                lblPageCaption.Text = value;
            }
        }

        public string Title
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        public SimpleButton CreatePageButton
        {
            get
            {
                return btnCreatePage;
            }
        }

        public DevExpress.XtraBars.BarButtonItem AttachFileButton
        {
            get
            {
                return bbiAttachFile;
            }
            set
            {
                bbiAttachFile = value;
            }
        }      

        

        public DevExpress.XtraEditors.PanelControl PanelFile
        {
            get
            {
                return panel_file;
            }
            set
            {
                panel_file = value;
            }
        }

        public DevExpress.XtraEditors.SplitContainerControl splitterControl
        {
            get
            {
                return splitContainerControl2;
            }
            set
            {
                splitContainerControl2 = value;
            }
        } 
        

        public DevExpress.XtraBars.BarButtonItem SaveButton
        {
            get
            {
                return bbiSave;
            }
            set
            {
                bbiSave = value;
            }
        }

        public SimpleButton ClearPageButton
        {
            get
            {
                return btnClearPage;
            }
        }

        public SimpleButton SaveSectionButton
        {
            get
            {
                return btnSaveSection;
            }
        }

        public SimpleButton LoadSectionButton
        {
            get
            {
                return btnLoadSection;
            }
        }

        public XtraScrollableControl ContentPanel
        {
            get
            {
                return contentPanel;
            }
        }

        public override Report Report
        {
            get
            {
                return model.Report;
            }
            set
            {
                model.Report = value;
            }
        }

        public bool IsLoadPage(Page page)
        {
            return model.IsLoadPage(page);
        }

        public void ReInit()
        {
            model.ReInit();
            btnSaveSection.Enabled = false;
            btnLoadSection.Enabled = false;
        }

        public void ShowFirstPage()
        {
            model.ShowFirstPage();
        }

        public ScrollableControl GetPage(Page page)
        {
            return model.GetPage(page);
        }

	    public Page CurrentPage
	    {
		    get { return model.CurrentPage; }
	    }

        //public System.Windows.Forms.Control GetControl(string pageName, string controlName, Page page = null)
        //{
        //    return model.GetControl(pageName, controlName, page);
        //}

        #region event

        public event EventHandler EditValueChanged;
        protected void RaiseEditValueChanged(EventArgs e)
        {
            if (EditValueChanged != null)
                EditValueChanged(this, e);
        }

        public event EventHandler AddSign;
        protected void RaiseAddSign()
        {
            if (AddSign != null)
                AddSign(this, new EventArgs());
        }

        public event CloseWindowEventHandler CloseWindow;
        protected void RaiseCloseWindow()
        {
            if (CloseWindow != null)
                CloseWindow(this, new CloseWindowEventArgs() { report = Report });
        }
                
        public event EventHandler CreateNew;
        protected void RaiseCreateNew()
        {
            if (CreateNew != null)
                CreateNew(this, null);
        }

        public event EventHandler Save;
        protected void RaiseSave()
        {
            if (Save != null)
                Save(this, null);
        }

        public event EventHandler SaveAs;
        protected void RaiseSaveAs()
        {
            if (SaveAs != null)
                SaveAs(this, null);
        }

        public event EventHandler ExportToPdf;
        protected void RaiseExportToPdf()
        {
            if (ExportToPdf != null)
                ExportToPdf(this, null);
        }


        public event EventHandler ExportToExcel;
        protected void RaiseExportToExcel()
        {
            if (ExportToExcel != null)
                ExportToExcel(this, null);
        }

        public event EventHandler ExportToRtf;
        protected void RaiseExportToRtf()
        {
            if (ExportToRtf != null)
                ExportToRtf(this, null);
        }

        public event EventHandler AboutProgram;
        protected void RaiseAboutProgram()
        {
            if (AboutProgram != null)
                AboutProgram(this, null);
        }

        /*
        public event EventHandler Help;
        protected void RaiseHelp()
        {
            if (Help != null)
                Help(this, null);
        }
        */

        public event EventHandler NetworkSettings;
        protected void RaiseNetworkSettings()
        {
            if (NetworkSettings != null)
                NetworkSettings(this, null);
        }

        public event EventHandler OrganizationData;
        protected void RaiseOrganizationData()
        {
            if (OrganizationData != null)
                OrganizationData(this, null);
        }


        public event EventHandler SavePacketAs;
        protected void RaiseSavePacketAs()
        {
            if (SavePacketAs != null)
                SavePacketAs(this, null);
        }

        public event EventHandler SendPacket;
        protected void RaiseSendPacket()
        {
            if (SendPacket != null)
                SendPacket(this, null);
        }

		public event EventHandler PrintPacket;
		protected void RaisePrintPacket()
		{
			if (PrintPacket != null)
				PrintPacket(this, null);
		}

        public event EventHandler ConvertPacket;
        protected void RaiseConvertPacket()
        {
            if (ConvertPacket != null)
                ConvertPacket(this, null);
        }

        public event EventHandler ExportXtddExcel;
		protected void RaiseExportXtddExcel()
		{
			if (ExportXtddExcel != null)
				ExportXtddExcel(this, null);
		}

        public event EventHandler SaveSection;
        protected void RaiseSaveSection()
        {
            if (SaveSection != null)
                SaveSection(model, null);
        }

        public event EventHandler LoadSection;
        protected void RaiseLoadSection()
        {
            if (LoadSection != null)
                LoadSection(model, null);
        }

        public new event EventHandler Load;
        protected void RaiseLoad()
        {
            if (Load != null)
                Load(this, null);
        }

        public event EventHandler LoadFile;
        protected void RaiseLoadFile()
        {
            SaveOpenFile();
            if (LoadFile != null)
                LoadFile(this, null);
        }
        
        public event EventHandler OpenModels;
        protected void RaiseOpenModels()
        {
            if (OpenModels != null)
                OpenModels(this, null);
        }       

        public event EventHandler DocumentsSubmitterOpen;
        protected void RaiseDocumentsSubmitterOpen()
        {
            if (DocumentsSubmitterOpen != null)
                DocumentsSubmitterOpen(this, null);
        }

        public new event ValidatingEventHandler Validating;
        protected void RaiseValidating(Page page, bool isRecurently)
        {
            if (Validating != null)
                Validating(this, new ValidatingEventArgs() { Page = page, IsRecurently = isRecurently });
        }

        public event EventHandler AttachFile;
        protected void RaiseAttachFile()
        {
            if (AttachFile != null)
                AttachFile(this, null);
        }
        #endregion

        public void model_EditValueChanged(object sender, EventArgs e)
        {
            RaiseTreeListChanged(e);
        }

        void btnLoadSection_Click(object sender, System.EventArgs e)
        {
            RaiseLoadSection();
        }


        void btnSaveSection_Click(object sender, System.EventArgs e)
        {
            RaiseSaveSection();
        }

        public event EventHandler TreeListChanged;
        protected void RaiseTreeListChanged(EventArgs e)
        {
            if (TreeListChanged != null)
                TreeListChanged(this, e);
        }

        public void SetTitle()
        {
            this.Title = title;
        }

        public void SetTitleNetworkError()
        {
            this.Title = title + " - Ошибка обновления отчетных форм. Попробуйте настроить соединение";
        }

        public EditorView()
        {
            InitializeComponent();
            model = new EditorModel(this);
            model.EditValueChanged += new EventHandler(model_EditValueChanged);
            contentPanel.AutoScrollPosition = new Point(0, 0);
            SetTitle();
            if (!EditorApp.UseUpdating)
            {
                bbiImportModels.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                bbiNetworkSettings.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            if (!FileAssociation.IsAssociated(".xtdd"))
            {
                try
                {
                    string ico_path = System.Windows.Forms.Application.ExecutablePath.Remove(System.Windows.Forms.Application.ExecutablePath.LastIndexOf('\\'));
                    ico_path = Path.Combine(ico_path, "anketa.ico");
                    FileAssociation.Associate(".xtdd", "Editor.exe", "xtdd File", ico_path, System.Windows.Forms.Application.ExecutablePath);
                }
                catch (Exception ex)
                {
                    ErrorHelper.Show(ex);
                }
            }           
        }
        
        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            aetp.Common.ControlModel.BaseControl control = GetFocusedControl();
            if (control is Page)
            {
                model.ShowPage(control as Page);
            }
        }

        private void bciExportToRtf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseExportToRtf();
        }

        private void bciExportToPdf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseExportToPdf();
        }

        private void bciExportToExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseExportToExcel();
        }

        private void bbiSaveAs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseSaveAs();
        }

        private void bbiAboutProgram_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAboutProgram();
        }

        /*
        private void bbiHelp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseHelp();
        }
        */

        private void bbiNetworkSettings_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseNetworkSettings();
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseSave();
        }

        private void bbiOrganizationData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseOrganizationData();
        }

        private void bbiLoadFile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {                       
            RaiseLoadFile();
        }        

        private void bbiSavePacket_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseSavePacketAs();
        }

        private void bbiSendPacket_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseSendPacket();
        }

		private void bbiPrintPacket_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			RaisePrintPacket();
		}

        private void bbiConvertPacket_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseConvertPacket();
        }

        private void bbiExportXtddExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
            RaiseExportXtddExcel();
		}

        
        private void bbiDocumentsSubmitterOpen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseDocumentsSubmitterOpen();
        }

        private void bbiAttachFile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAttachFile();
        }

        private void bbiCreateNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveOpenFile();
            RaiseCreateNew();
        }

        private void SaveOpenFile()
        {
            if ((Report != null) && (Report.ReportChange))
            {
                if (System.Windows.Forms.MessageBox.Show(string.Format("Сохранить изменения в отчёте {0}?", Report.Caption), "Анкета.Редактор", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    RaiseSave();
                }
            }
        }

        private void btnCreatePage_Click_1(object sender, EventArgs e)
        {
            btnCreatePage.Enabled = false;
            model.CurrentPage.InitialDefaultValues();
            model.ShowPage(model.CurrentPage, true);
            if (Report != null)
            {
                Report.ReportChange = true;
            }
            PageViewInfo pvi = new PageViewInfo();
            pvi.page = model.CurrentPage;
            RaisePageVisibleChanged(new PageViewInfoCollectionChangeEventArgs() { page = pvi, type = ChangeType.Add });
            btnClearPage.Enabled = true;
        }

        private void btnZeroFill_Click(object sender, EventArgs e)
        {
            model.ZeroFill();
        }

        private void btnClearPage_Click_1(object sender, EventArgs e)
        {
            btnClearPage.Enabled = false;
            model.ClearCurrentPage();
            PageViewInfo pvi = new PageViewInfo();
            pvi.page = model.CurrentPage;
            RaisePageVisibleChanged(new PageViewInfoCollectionChangeEventArgs() { page = pvi, type = ChangeType.Remove });
            btnCreatePage.Enabled = true;
            if (Report != null)
            {
                Report.ReportChange = true;
            }
        }

        private void bbiValidate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseValidating(null, true);
        }

        private void btnValidatePage_Click(object sender, EventArgs e)
        {
            RaiseValidating(model.CurrentPage, false);
        }

        private void bbiAddSign_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddSign();
        }

        private void bbiImportModels_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseOpenModels();
        }

        private void EditorView_FormClosing(object sender, FormClosingEventArgs e)
        {
             SaveOpenFile();
             string path = Path.Combine(aetp.Utils.Config.GetAppPath(), "temp");
             if (Directory.Exists(path))
                 Directory.Delete(path, true);
        }

        private void EditorView_Load(object sender, EventArgs e)
        {
           
                
        }

        void EditorView_Shown(object sender, System.EventArgs e)
        {
            try
            {
                if (!EditorApp.UseUpdating) ReportUtils.RefreshModelsList();
                else AnketFormUpdaterFromWeb.CheckUpdates();
            }
            catch
            {
                SetTitleNetworkError();
            }
        }

        void ribbon_ShowCustomizationMenu(object sender, DevExpress.XtraBars.Ribbon.RibbonCustomizationMenuEventArgs e)
        {
            e.ShowCustomizationMenu = false;
        }
    }

}
