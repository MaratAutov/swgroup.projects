﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraTreeList;
using aetp.Common.ControlModel;
using aetp.Common.Mediator;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;


namespace aetp.Common.View
{
    public interface IEditorWindow
    {
        event EventHandler TreeListChanged;
        event EventHandler CreateNew;
        event EventHandler Save;
        event EventHandler SaveAs;
        event EventHandler ExportToRtf;
        event EventHandler ExportToPdf;
        event EventHandler ExportToExcel;
        event EventHandler AboutProgram;
        //event EventHandler Help;
        event EventHandler NetworkSettings;
        event EventHandler SavePacketAs;
        event EventHandler Load;
        event EventHandler LoadFile;
        event EventHandler OpenModels;
        event EventHandler SendPacket;
        event EventHandler DocumentsSubmitterOpen;
		event EventHandler PrintPacket;
        event EventHandler ExportXtddExcel;
        event EventHandler AddSign;
        event EventHandler AttachFile;
        event EventHandler OrganizationData;
        event EventHandler EditValueChanged;
        event EventHandler SaveSection;
        event EventHandler ConvertPacket;
        event EventHandler LoadSection;
        event PageViewInfoCollectionChangeEventHandler PageVisibleChanged;
        event CloseWindowEventHandler CloseWindow;
        event ValidatingEventHandler Validating;
        TreeListNode GetFocusedTreeListNode();
        string PageCaption { get; set; }


        SimpleButton SaveSectionButton { get; }
        SimpleButton LoadSectionButton { get; }
        SimpleButton CreatePageButton { get; }
        DevExpress.XtraEditors.PanelControl PanelFile { get; set; }
        DevExpress.XtraEditors.SplitContainerControl splitterControl { get; set; }
       
        DevExpress.XtraBars.BarButtonItem AttachFileButton { get; set; }
        SimpleButton ClearPageButton { get; }
        DevExpress.XtraBars.BarButtonItem SaveButton { get; set; }
        XtraScrollableControl ContentPanel { get; }
        string Title { get; set; }
        ScrollableControl GetPage(Page page);
        //System.Windows.Forms.Control GetControl(string pageName, string controlName, Page page = null);
        bool IsLoadPage(Page page);
        void ReInit();
        void ShowFirstPage();
        string title { get; }
    }

    public class CloseWindowEventArgs
    {
        public Report report { get; set; }
    }
    public delegate void CloseWindowEventHandler(object sender, CloseWindowEventArgs e);
}
