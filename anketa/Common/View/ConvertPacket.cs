﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using aetp.Common.ControlModel;
using aetp.Common.Mediator;
using System.Xml;
using aetp.Utils;

namespace aetp.Common.View
{
    public partial class ConvertPacket : Form
    {
        private Report _report;
        public Report Report
        {
            get
            {
                return _report;
            }
            set
            {
                _report = value;               
            }
        }

        public ConvertPacket()
        {
            InitializeComponent();
        }

        private void PathFile_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Anket File Form (*.xtdd)|*.xtdd";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                PathFile.Text = ofd.FileName;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (PathFile.Text.Trim().Length > 0)
            {
                if (File.Exists(PathFile.Text))
                {
                    Report = null;
                    GC.Collect();
                    EditorViewSerializerConvert serializer = new EditorViewSerializerConvert();
                    Report = serializer.DeserializeConvert(PathFile.Text);
                    if (Report != null)
                    {
                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.Filter = "(*.xtdd)|*.xtdd";

                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            try
                            {
                                File.WriteAllText(sfd.FileName, SerializeReport(Report));
                                this.Close();
                            }
                            catch (Exception ex)
                            {
                                ErrorHelper.Show(ex);
                            }
                        }                        
                    }
                }
                else
                {
                    MessageBox.Show(string.Format("Файл {0} не существует", PathFile.Text));
                }
            }
        }


        string SerializeReport(aetp.Common.ControlModel.Page page)
        {
            if (page != null)
            {
                EditorViewSerializerConvert serializer = new EditorViewSerializerConvert();
                MemoryStream memoryStream = new MemoryStream();
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = Report.SerialaizerConfig.GetEncoding();
                settings.Indent = true;
                settings.OmitXmlDeclaration = false;
                XmlWriter xmlTextWriter = XmlWriter.Create(memoryStream, settings);
                serializer.Serialize(xmlTextWriter, page, Report.SerialaizerConfig);
                return Encoding.UTF8.GetString(memoryStream.ToArray());
            }
            else return string.Empty;
        }

        private void ConvertPacket_Load(object sender, EventArgs e)
        {
            PathFile.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
        }
    }
}
