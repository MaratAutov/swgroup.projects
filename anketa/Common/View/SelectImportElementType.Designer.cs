﻿namespace aetp.Common.View
{
    partial class SelectImportElementType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectImportElementType));
            this.rbDictionary = new System.Windows.Forms.RadioButton();
            this.rbPage = new System.Windows.Forms.RadioButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.rbPrintForm = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // rbDictionary
            // 
            this.rbDictionary.AutoSize = true;
            this.rbDictionary.Checked = true;
            this.rbDictionary.Location = new System.Drawing.Point(47, 13);
            this.rbDictionary.Name = "rbDictionary";
            this.rbDictionary.Size = new System.Drawing.Size(68, 17);
            this.rbDictionary.TabIndex = 0;
            this.rbDictionary.TabStop = true;
            this.rbDictionary.Text = "Словари";
            this.rbDictionary.UseVisualStyleBackColor = true;
            // 
            // rbPage
            // 
            this.rbPage.AutoSize = true;
            this.rbPage.Location = new System.Drawing.Point(47, 45);
            this.rbPage.Name = "rbPage";
            this.rbPage.Size = new System.Drawing.Size(70, 17);
            this.rbPage.TabIndex = 1;
            this.rbPage.Text = "Разделы";
            this.rbPage.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(266, 107);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // rbPrintForm
            // 
            this.rbPrintForm.AutoSize = true;
            this.rbPrintForm.Location = new System.Drawing.Point(47, 77);
            this.rbPrintForm.Name = "rbPrintForm";
            this.rbPrintForm.Size = new System.Drawing.Size(110, 17);
            this.rbPrintForm.TabIndex = 3;
            this.rbPrintForm.Text = "Печатная форма";
            this.rbPrintForm.UseVisualStyleBackColor = true;
            // 
            // SelectImportElementType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 142);
            this.Controls.Add(this.rbPrintForm);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.rbPage);
            this.Controls.Add(this.rbDictionary);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SelectImportElementType";
            this.Text = "Выбор типа элементов для импорта";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbDictionary;
        private System.Windows.Forms.RadioButton rbPage;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.RadioButton rbPrintForm;
    }
}