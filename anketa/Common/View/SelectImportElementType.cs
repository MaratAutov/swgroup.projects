﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace aetp.Common.View
{
    public partial class SelectImportElementType : Form
    {
        public ImportElementType elementType
        {
            get
            {
                if (rbDictionary.Checked)
                    return ImportElementType.dictionary;
                else if (rbPage.Checked)
                    return ImportElementType.page;
                else return ImportElementType.printForm;
            }
            set
            {
                if (value == ImportElementType.dictionary)
                    rbDictionary.Checked = true;
                else if (value == ImportElementType.page)
                    rbPage.Checked = true;
                else rbPrintForm.Checked = true;
            }
        }


        public SelectImportElementType()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    public enum ImportElementType
    {
        dictionary = 0,
        page,
        printForm
    }
}
