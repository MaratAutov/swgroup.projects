﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace aetp.Common.View.Splash
{
    public partial class Splash : Form
    {
        Form _Parent;
        BackgroundWorker worker;
        public Splash(Form parent, ViewType type)
        {
            InitializeComponent();
            if (type == ViewType.Editor)
                BackgroundImage = Properties.Resources.ФСФР_start_4;
            else
                BackgroundImage = Properties.Resources.ФСФР_start_5;
            worker = new BackgroundWorker();
            this.worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.worker_DoWork);
            worker.RunWorkerAsync();
            _Parent = parent;
            _Parent.FormClosing += new FormClosingEventHandler(_Parent_FormClosing);
        }

        void _Parent_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }
        void SafeHide()
        {
            if (this.InvokeRequired) this.Invoke(new Action(() => this.Hide()));
            else this.Hide();
        }
        
        void SafeShow()
        {
            if (this.InvokeRequired) this.Invoke(new Action(() => _Parent.Show()));
            else _Parent.Show();
        }
        
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(2500);
            
            this.SafeHide();
            this.SafeShow(); 
        }     
    }
}
