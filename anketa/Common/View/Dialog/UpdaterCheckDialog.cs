﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace aetp.Common.View.Dialog
{
    public partial class UpdaterCheckDialog : XtraForm
    {
        string _version;
        public string Version 
        {
            get
            {
                return _version;
            }
            set
            {
                _version = value;
                lblText.Text = String.Format("Вышел пакет обновлений отчетных форм версии {0}. Хотите провести обновление?", _version);
            }
        }

        public bool IsNotRemindeMe
        {
            get
            {
                return ceNotReminde.Checked;
            }
            set
            {
                ceNotReminde.Checked = value;
            }
        }

        public UpdaterCheckDialog()
        {
            InitializeComponent();
        }

        public UpdaterCheckDialog(string version)
        {
            InitializeComponent();
            Version = version;
        }

        void btnOk_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

    }
}
