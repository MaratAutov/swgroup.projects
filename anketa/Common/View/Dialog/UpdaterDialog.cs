﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace aetp.Common.View.Dialog
{
    public partial class UpdaterDialog : XtraForm
    {
        ModelConfigCompareResult _compareData;
        public ModelConfigCompareResult CompareData
        {
            get
            {
                return _compareData;
            }
            set
            {
                _compareData = value;
                refreshData();
            }
        }

        void refreshData()
        {
            if (CompareData.OnlyServer.Count > 0)
            {
                DataTable dtOnlyServer = new DataTable();
                dtOnlyServer.Columns.Add("Name");
                dtOnlyServer.Columns.Add("Version");
                dtOnlyServer.Columns.Add("Updating", typeof(bool));
                dtOnlyServer.Columns.Add("Object", typeof(ModelInfo));

                foreach (var item in CompareData.OnlyServer)
                {
                    DataRow dr = dtOnlyServer.NewRow();
                    dr["Name"] = item.Name;
                    dr["Version"] = item.Version;
                    dr["Updating"] = true;
                    dr["Object"] = new ModelInfo(item);
                    dtOnlyServer.Rows.Add(dr);
                }
                gcOnlyServer.DataSource = dtOnlyServer;
            }
            else
                tpOnlyServer.PageVisible = false;

            if (CompareData.Changes.Count > 0)
            {
                DataTable dtChanges = new DataTable();
                dtChanges.Columns.Add("Name");
                dtChanges.Columns.Add("Version");
                dtChanges.Columns.Add("Replacing", typeof(bool));
                dtChanges.Columns.Add("Object", typeof(ModelConfigChangePair));

                foreach (var item in CompareData.Changes)
                {
                    DataRow dr = dtChanges.NewRow();
                    dr["Name"] = item.Local.Name;
                    dr["Version"] = item.Local.Version;
                    dr["Replacing"] = true;
                    dr["Object"] = item;
                    dtChanges.Rows.Add(dr);
                }
                gcChanges.DataSource = dtChanges;
            }
            else
                tpChanges.PageVisible = false;

            if (CompareData.OnlyLocal.Count > 0)
            {
                DataTable dtOnlyLocal = new DataTable();
                dtOnlyLocal.Columns.Add("Name");
                dtOnlyLocal.Columns.Add("Version");
                dtOnlyLocal.Columns.Add("Deleting", typeof(bool));
                dtOnlyLocal.Columns.Add("Object", typeof(ModelInfo));

                foreach (var item in CompareData.OnlyLocal)
                {
                    DataRow dr = dtOnlyLocal.NewRow();
                    dr["Name"] = item.Name;
                    dr["Version"] = item.Version;
                    dr["Deleting"] = false;
                    dr["Object"] = new ModelInfo(item);
                    dtOnlyLocal.Rows.Add(dr);
                }
                gcOnlyLocal.DataSource = dtOnlyLocal;
            }
            else
                tpOnlyLocal.PageVisible = false;
        }

        public List<UpdateItemWrapper> ItemsForUpdate
        {
            get
            {
                List<UpdateItemWrapper> items = new List<UpdateItemWrapper>();
                if (gcOnlyServer.DataSource is DataTable)
                {
                    foreach (DataRow row in (gcOnlyServer.DataSource as DataTable).Rows)
                    {
                        if ((bool)row["Updating"])
                        {
                            items.Add(new UpdateItemWrapper(row["Object"], UpdateAction.Update));
                        }
                    }
                }

                if (gcChanges.DataSource is DataTable)
                {
                    foreach (DataRow row in (gcChanges.DataSource as DataTable).Rows)
                    {
                        if ((bool)row["Replacing"])
                        {
                            items.Add(new UpdateItemWrapper(row["Object"], UpdateAction.Replace));
                        }
                    }
                }

                if (gcOnlyLocal.DataSource is DataTable)
                {
                    foreach (DataRow row in (gcOnlyLocal.DataSource as DataTable).Rows)
                    {
                        if ((bool)row["Deleting"])
                        {
                            items.Add(new UpdateItemWrapper(row["Object"], UpdateAction.Delete));
                        }
                    }
                }

                return items;
            }
        }

        

        public UpdaterDialog()
        {
            InitializeComponent();
        }

        public UpdaterDialog(ModelConfigCompareResult compareData)
        {
            InitializeComponent();
            this.CompareData = compareData;
        }
    }

    
}
