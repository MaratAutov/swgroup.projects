﻿namespace aetp.Common.View.Dialog
{
    partial class ProxyServerDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProxyServerDialog));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.rbDefault = new System.Windows.Forms.RadioButton();
            this.rbManual = new System.Windows.Forms.RadioButton();
            this.panelManualConfig = new DevExpress.XtraEditors.PanelControl();
            this.ceNotUseProxyForLocal = new DevExpress.XtraEditors.CheckEdit();
            this.teHttpUrl = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.sePort = new DevExpress.XtraEditors.SpinEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelManualConfig)).BeginInit();
            this.panelManualConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceNotUseProxyForLocal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teHttpUrl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sePort.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnOk);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 178);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(621, 45);
            this.panelControl1.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(541, 10);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.Click += new System.EventHandler(btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(460, 10);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(btnOk_Click);
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Checked = true;
            this.rbNone.Location = new System.Drawing.Point(22, 12);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(83, 17);
            this.rbNone.TabIndex = 1;
            this.rbNone.TabStop = true;
            this.rbNone.Text = "Без прокси";
            this.rbNone.UseVisualStyleBackColor = true;
            this.rbNone.CheckedChanged += new System.EventHandler(rb_CheckedChanged);
            // 
            // rbDefault
            // 
            this.rbDefault.AutoSize = true;
            this.rbDefault.Location = new System.Drawing.Point(22, 35);
            this.rbDefault.Name = "rbDefault";
            this.rbDefault.Size = new System.Drawing.Size(253, 17);
            this.rbDefault.TabIndex = 2;
            this.rbDefault.Text = "Использовать системные настройки прокси";
            this.rbDefault.UseVisualStyleBackColor = true;
            this.rbDefault.CheckedChanged += new System.EventHandler(rb_CheckedChanged);
            // 
            // rbManual
            // 
            this.rbManual.AutoSize = true;
            this.rbManual.Location = new System.Drawing.Point(22, 58);
            this.rbManual.Name = "rbManual";
            this.rbManual.Size = new System.Drawing.Size(253, 17);
            this.rbManual.TabIndex = 3;
            this.rbManual.Text = "Ручная настройка";
            this.rbManual.UseVisualStyleBackColor = true;
            this.rbManual.CheckedChanged += new System.EventHandler(rb_CheckedChanged);
            // 
            // panelControl2
            // 
            this.panelManualConfig.Controls.Add(this.sePort);
            this.panelManualConfig.Controls.Add(this.labelControl2);
            this.panelManualConfig.Controls.Add(this.labelControl1);
            this.panelManualConfig.Controls.Add(this.teHttpUrl);
            this.panelManualConfig.Enabled = false;
            this.panelManualConfig.Location = new System.Drawing.Point(41, 81);
            this.panelManualConfig.Name = "panelControl2";
            this.panelManualConfig.Size = new System.Drawing.Size(569, 52);
            this.panelManualConfig.TabIndex = 4;
            // 
            // ceUseProxyForLocal
            // 
            this.ceNotUseProxyForLocal.Location = new System.Drawing.Point(22, 145);
            this.ceNotUseProxyForLocal.Name = "ceUseProxyForLocal";
            this.ceNotUseProxyForLocal.Properties.AutoWidth = true;
            this.ceNotUseProxyForLocal.Properties.Caption = "Не использовать настройки прокси сервера для локальных адресов";
            this.ceNotUseProxyForLocal.Size = new System.Drawing.Size(358, 19);
            this.ceNotUseProxyForLocal.TabIndex = 5;
            // 
            // textEdit1
            // 
            this.teHttpUrl.Location = new System.Drawing.Point(78, 15);
            this.teHttpUrl.Name = "textEdit1";
            this.teHttpUrl.Size = new System.Drawing.Size(343, 20);
            this.teHttpUrl.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(67, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "HTTP прокси:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(453, 18);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(29, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Порт:";
            // 
            // spinEdit1
            // 
            this.sePort.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sePort.Location = new System.Drawing.Point(488, 15);
            this.sePort.Name = "spinEdit1";
            this.sePort.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.sePort.Size = new System.Drawing.Size(66, 20);
            this.sePort.TabIndex = 2;
            this.sePort.Properties.IsFloatValue = true;
            this.sePort.Properties.Mask.EditMask = "N00";
            this.sePort.Properties.MinValue = 0;
            this.sePort.Properties.MaxValue = 65535;
            // 
            // ProxyServerDialog
            // 
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 223);
            this.Controls.Add(this.ceNotUseProxyForLocal);
            this.Controls.Add(this.panelManualConfig);
            this.Controls.Add(this.rbManual);
            this.Controls.Add(this.rbDefault);
            this.Controls.Add(this.rbNone);
            this.Controls.Add(this.panelControl1);
            this.Name = "ProxyServerDialog";
            this.Text = "Настройки параметров подключения";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelManualConfig)).EndInit();
            this.panelManualConfig.ResumeLayout(false);
            this.panelManualConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceNotUseProxyForLocal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teHttpUrl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sePort.Properties)).EndInit();
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.RadioButton rbDefault;
        private System.Windows.Forms.RadioButton rbManual;
        private DevExpress.XtraEditors.PanelControl panelManualConfig;
        private DevExpress.XtraEditors.CheckEdit ceNotUseProxyForLocal;
        private DevExpress.XtraEditors.SpinEdit sePort;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit teHttpUrl;
    }
}