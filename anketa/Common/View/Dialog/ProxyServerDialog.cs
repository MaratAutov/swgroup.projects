﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using System.Xml;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;


namespace aetp.Common.View.Dialog
{
    public partial class ProxyServerDialog : XtraForm
    {
        ProxyServerConfig.ProxyTypes _proxyType;
        private ProxyServerConfig.ProxyTypes ProxyType
        {
            get
            {
                return _proxyType;
            }
            set
            {
                _proxyType = value;
                if (value == ProxyServerConfig.ProxyTypes.Default)
                {
                    rbDefault.Checked = true;
                }
                else if (value == ProxyServerConfig.ProxyTypes.Http)
                {
                    rbManual.Checked = true;
                    panelManualConfig.Enabled = true;
                }
                else
                {
                    rbNone.Checked = true;
                }

                if (value != ProxyServerConfig.ProxyTypes.Http)
                {
                    panelManualConfig.Enabled = false;
                }
            }
        }

        
        bool NotUseProxyForLocal
        {
            get
            {
                return ceNotUseProxyForLocal.Checked;
            }
            set
            {
                ceNotUseProxyForLocal.Checked = value;
            }
        }

        string HttpUrl
        {
            get
            {
                return teHttpUrl.Text;
            }
            set
            {
                teHttpUrl.Text = value;
            }
        }

        int HttpPort
        {
            get
            {
                return (int)sePort.Value;
            }
            set
            {
                sePort.Value = value;
            }
        }

        public ProxyServerDialog()
        {
            InitializeComponent();
            this.ProxyType = AppGlobal.Instance.ProxyServerConfig.ProxyType;
            this.NotUseProxyForLocal = AppGlobal.Instance.ProxyServerConfig.NotUsingProxyForLocalPort;
            this.HttpUrl = AppGlobal.Instance.ProxyServerConfig.HttpUrl;
            this.HttpPort = AppGlobal.Instance.ProxyServerConfig.HttpPort;
        }

        void btnOk_Click(object sender, System.EventArgs e)
        {
            AppGlobal.Instance.ProxyServerConfig.ProxyType = this.ProxyType;
            AppGlobal.Instance.ProxyServerConfig.NotUsingProxyForLocalPort = this.NotUseProxyForLocal;
            AppGlobal.Instance.ProxyServerConfig.HttpUrl = this.HttpUrl;
            AppGlobal.Instance.ProxyServerConfig.HttpPort = this.HttpPort;
            AppGlobal.Instance.ProxyServerConfig.Save();
            
            try
            {
                AnketFormUpdaterFromWeb.GetServerVersion();
                EditorApp.MainView.SetTitle();
                this.Close();
            }
            catch
            {
                EditorApp.MainView.SetTitleNetworkError();
                if (MessageBox.Show("Не удалось установить соединение с сервером обновлений. Продолжить работу с приложением без возможности обновления?",
                    "Соединение не установлено", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes) this.Close();
            }
        }

        void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        void rb_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbNone.Checked)
                this.ProxyType = ProxyServerConfig.ProxyTypes.None;
            else if (rbDefault.Checked)
                this.ProxyType = ProxyServerConfig.ProxyTypes.Default;
            else
                this.ProxyType = ProxyServerConfig.ProxyTypes.Http;
        }
    }
}
