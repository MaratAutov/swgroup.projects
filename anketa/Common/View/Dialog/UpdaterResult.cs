﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace aetp.Common.View.Dialog
{
    public partial class UpdaterResult : XtraForm
    {
        bool IsFullInfo = true;
        List<UpdateItemWrapper> _updateItems;
        public List<UpdateItemWrapper> UpdateItems
        {
            get
            {
                return _updateItems;
            }
            set
            {
                _updateItems = value;
                int total_updates_count = _updateItems.Count;
                int updated_count = _updateItems.Where(x => x.IsUpdating == true).Count();
                int error_count = total_updates_count - updated_count;

                meText.Text = String.Format("Успешно обновлено {0} из {1} шаблонов", total_updates_count, updated_count);

                DataTable dt = new DataTable();
                dt.Columns.Add("serverName");
                dt.Columns.Add("localName");
                dt.Columns.Add("result");
                dt.Columns.Add("exception");

                foreach (var item in _updateItems)
                {
                    DataRow dr = dt.NewRow();
                    switch( item.Action)
                    {
                        case UpdateAction.Update:
                            ModelInfo mi1 = item.UpdateItem as ModelInfo;
                            dr["serverName"] = String.Format("{0} (версия {1})", mi1.Name, mi1.Version);
                            break;
                        case UpdateAction.Delete:
                            ModelInfo mi2 = item.UpdateItem as ModelInfo;
                            dr["localName"] = String.Format("{0} (версия {1})", mi2.Name, mi2.Version);
                            break;
                        case UpdateAction.Replace:
                            ModelConfigChangePair pair = item.UpdateItem as ModelConfigChangePair;
                            dr["localName"] = String.Format("{0} (версия {1})", pair.Local.Name, pair.Local.Version);
                            dr["serverName"] = String.Format("{0} (версия {1})", pair.Server.Name, pair.Server.Version);
                            break;
                    }
                    if (item.IsUpdating)
                        dr["result"] = "Обновлено";
                    else
                    {
                        dr["result"] = "Ошибка";
                        dr["exception"] = item.ex.ToString();
                    }
                    dt.Rows.Add(dr);    
                }
                gcInfo.DataSource = dt;
            }
        }

        public UpdaterResult()
        {
            InitializeComponent();
            SetHeight();
        }

        public UpdaterResult(List<UpdateItemWrapper> updateItems)
        {
            InitializeComponent();
            SetHeight();
            UpdateItems = updateItems;
        }

        void UpdaterResult_Load(object sender, System.EventArgs e)
        {
            SetHeight();
        }

        void btnOk_Click(object sender, System.EventArgs e)
        {
            this.Close();    
        }

        void UpdaterResult_Resize(object sender, System.EventArgs e)
        {
                            
        }

        void SetHeight()
        {
            if (IsFullInfo)
            {
                this.MinimumSize = new Size(100, 50);
                this.MaximumSize = new Size(600, 500);
                this.Height = 230;
                btnFullInfo.Text = "Кратко";
            }
            else
            {
                int height = meText.Height + panelControl1.Height + 40;
                this.MaximumSize = this.MinimumSize = new Size(this.Width, height);
                this.Height = height;
                btnFullInfo.Text = "Подробно";
            }
            IsFullInfo = !IsFullInfo;
        }

        void btnFullInfo_Click(object sender, System.EventArgs e)
        {
            SetHeight();
        }
    }
}
