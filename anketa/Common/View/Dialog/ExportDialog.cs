﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace aetp.Common.View.Dialog
{
    public enum ExportOptionsEnum
    {
        All = 1,
        Selected
    }

    public partial class ExportDialog : Form
    {
        public ExportDialog()
        {
            InitializeComponent();
        }

        public ExportOptionsEnum GetExportOptions
        {
            get
            {
                if (rbSelected.Checked)
                {
                    return ExportOptionsEnum.Selected;
                }

                return ExportOptionsEnum.All;
            }
        }

        public static ExportOptionsEnum ExportOptions()
        {
            using (ExportDialog frm = new ExportDialog())
            {
                frm.ShowDialog();

                return frm.GetExportOptions;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.OK;
        }
    }
}
