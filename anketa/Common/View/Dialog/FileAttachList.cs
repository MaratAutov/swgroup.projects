﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using aetp.Common.ControlModel;
using System.IO;
using aetp.Utils;


namespace aetp.Common.View.Dialog
{
    public partial class FileAttachList : Form
    {
        Report report;
        public FileAttachList(Report report)
        {
            InitializeComponent();
            this.report = report;
        }

        private void FileAttachList_Load(object sender, EventArgs e)
        {
            RefreshList();
        }

        public void RefreshList()
        {
            lvModels.Items.Clear();
            if (report.File.Count > 0)
            {
                foreach (FileList item in report.File)
                {
                    ListViewItem lvi = new ListViewItem(new string[] { "" });
                    lvi.SubItems[0].Text = item.Name;
                    lvModels.Items.Add(lvi);
                }
            }
        }       


        private void btn_add_Click(object sender, EventArgs e)
        {            
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Прикрепить файл";
            ofd.Filter = "All files (*.*)|*.*";            

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string file_name = ofd.FileName.Remove(0, ofd.FileName.LastIndexOf("\\") + 1);
                bool exist = false;
                for (int i = 0; i < lvModels.Items.Count; i++)
                {
                    if (lvModels.Items[i].Text == file_name)
                    {
                        exist = true;
                        MessageBox.Show("Файл с таким именем уже добавлен");                        
                    }                        
                }

                if (!exist)
                {
                    if (!string.IsNullOrEmpty(ofd.FileName))
                    {
                        FileStream fs = new FileStream(ofd.FileName,
                                                       FileMode.Open,
                                                       FileAccess.Read);
                        byte[] filebytes = new byte[fs.Length];
                        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
                        string encodedData =
                            Convert.ToBase64String(filebytes,
                                                   Base64FormattingOptions.InsertLineBreaks);

                        report.File.Add(new FileList(file_name, encodedData, "File"));
                        RefreshList();
                        report.ReportChange = true;
                    }
                }
            } 
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {            
            if (lvModels.SelectedItems.Count == 1)
            {
                report.File.RemoveAt(lvModels.SelectedItems[0].Index);
                report.ReportChange = true;
            }
            RefreshList();
        }

        private void btn_view_Click(object sender, EventArgs e)
        {
            if (lvModels.SelectedItems.Count == 1)
            {
                if (report.File[lvModels.SelectedItems[0].Index].Data != null)
                {                    
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.Filter = "All files (*.*)|*.*";
                    sfd.FileName = report.File[lvModels.SelectedItems[0].Index].Name.ToString();
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            byte[] filebytes = Convert.FromBase64String(report.File[lvModels.SelectedItems[0].Index].Data.ToString());
                            FileStream fs = new FileStream(sfd.FileName,
                                                   FileMode.Create,
                                                   FileAccess.Write,
                                                   FileShare.None);
                            fs.Write(filebytes, 0, filebytes.Length);
                            fs.Close();
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.Show(ex);
                        }
                    }
                }
            }
        }       
    }
}
