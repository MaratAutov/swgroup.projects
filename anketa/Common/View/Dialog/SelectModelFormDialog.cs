﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using System.Xml;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;


namespace aetp.Common.View.Dialog
{
    public partial class SelectModelFormDialog : XtraForm
    {
        public SelectModelFormDialog()
        {
            InitializeComponent();
        }

        public string SelectedModelFullPath
        {
            get
            {
                if (treeList.FocusedNode != null)
                    return treeList.FocusedNode.GetValue(3).ToString();
                else
                    return null;
            }            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if ((treeList.FocusedNode != null) && (treeList.FocusedNode.GetValue(3).ToString().Trim() != ""))
            {
                this.Close();
            }                
            else
            {
                DialogResult = System.Windows.Forms.DialogResult.None;
                MessageBox.Show("Выберите вид отчета");
            }
        }        

        void RefreshModelsDirectoryList()
        {
            string path = Path.Combine(aetp.Utils.Config.GetCommonAppPath(), "ModelDirectoryConfig.xml");
            if (File.Exists(path))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(File.ReadAllText(path));
                XmlNodeList root = doc.GetElementsByTagName("ModelsDirectoryList");

                treeList.BeginUnboundLoad();

                foreach (XmlNode x in root)
                {
                    foreach (XmlNode xc in x.ChildNodes)
                    {
                        TreeListNode parentForRootNodes = null;
                        GetChildrenNode(parentForRootNodes, xc);
                    }
                }

                treeList.EndUnboundLoad();
            }
            else MessageBox.Show("Отсутствует конфигурационный файл ModelDirectoryConfig.xml");
        }

        private bool FindModelConfig(ModelConfig.ModelInfo item)
        {
            bool exist = false;
            string path = Path.Combine(aetp.Utils.Config.GetCommonAppPath(), "ModelDirectoryConfig.xml");
            if (File.Exists(path))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(File.ReadAllText(path));
                XmlNodeList root = doc.GetElementsByTagName("Report");

                foreach (XmlNode x in root)
                {
                    if (x.Attributes["name"].Value.ToString() == item.Name)
                    {
                        exist = true;
                    }
                }                
            }
            return exist;

        }

        private void GetChildrenNode(TreeListNode node,XmlNode xml_node)
        {
            if (xml_node.Name == "Report")
            {
                foreach (ModelConfig.ModelInfo item in AppGlobal.Instance.Config.ListModelInfo)
                {
                    if (item.Visible && item.Name == xml_node.Attributes["name"].Value.ToString())
                    {
                        if (File.Exists(item.Path))
                        {
                            TreeListNode childNode = treeList.AppendNode(
                    new object[] { item.Name, item.Version, item.Description, item.Path },
                    node);
                        }
                    }
                }
            }
            else if (xml_node.Name == "Dir")
            {
                TreeListNode childNode = treeList.AppendNode(
                   new object[] { xml_node.Attributes["name"].Value.ToString(), " ", " "," " },
                   node);
                if (xml_node.Attributes["model"] != null)
                {
                    foreach (ModelConfig.ModelInfo item in AppGlobal.Instance.Config.ListModelInfo)
                    {
                        if (!item.Visible) continue;
                        if (!FindModelConfig(item))
                        {
                            if (File.Exists(item.Path))
                            {
                                TreeListNode modelNode = treeList.AppendNode(
                        new object[] { item.Name, item.Version, item.Description, item.Path },
                        childNode);
                            }
                        }
                    }
                }
                if (xml_node.ChildNodes.Count > 0)
                {
                    foreach (XmlNode child_node in xml_node.ChildNodes)
                    {
                        GetChildrenNode(childNode, child_node);
                    }
                }
            }
        }
   

        private void CreateColumns(TreeList tl)
        {
            tl.BeginUpdate();
            tl.Columns.Add();
            tl.Columns[0].Caption = "Наименование";
            tl.Columns[0].VisibleIndex = 0;
            tl.Columns[0].OptionsColumn.AllowEdit = false;
            tl.Columns.Add();
            tl.Columns[1].Caption = "Версия";
            tl.Columns[1].VisibleIndex = 1;
            tl.Columns[1].OptionsColumn.AllowEdit = false;
            tl.Columns.Add();
            tl.Columns[2].Caption = "Описание";
            tl.Columns[2].VisibleIndex = 2;
            tl.Columns[2].OptionsColumn.AllowEdit = false;
            tl.Columns.Add();
            tl.Columns[3].Caption = "Путь";
            tl.Columns[3].VisibleIndex = -1;
            tl.Columns[3].OptionsColumn.AllowEdit = false;
            tl.EndUpdate();
        }       

        private void SelectModelForm_Load(object sender, EventArgs e)
        {
            btnOk.Enabled = false;
            treeList.FocusedNodeChanged += new FocusedNodeChangedEventHandler(treeList_FocusedNodeChanged);
            treeList.DoubleClick += new EventHandler(treeList_DoubleClick);
            CreateColumns(treeList);
            RefreshModelsDirectoryList();
        }

        private void treeList_DoubleClick(object sender, EventArgs e)
        {
            TreeList tree = sender as TreeList;
            TreeListHitInfo hi = tree.CalcHitInfo(tree.PointToClient(System.Windows.Forms.Control.MousePosition));
            if (hi.Node != null)
            {
                if (hi.Node.GetValue(3).ToString().Trim() != "")
                {
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
            }
        }

        private void treeList_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if ((treeList.FocusedNode != null) && (treeList.FocusedNode.GetValue(3).ToString().Trim() != ""))
            {
                btnOk.Enabled = true;
            }
            else
            {
                btnOk.Enabled = false;
            }
        }
    }
}
