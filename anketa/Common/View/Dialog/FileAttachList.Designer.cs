﻿namespace aetp.Common.View.Dialog
{
    partial class FileAttachList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileAttachList));
            this.lvModels = new System.Windows.Forms.ListView();
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_add = new DevExpress.XtraEditors.SimpleButton();
            this.btn_delete = new DevExpress.XtraEditors.SimpleButton();
            this.btn_view = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // lvModels
            // 
            this.lvModels.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName});
            this.lvModels.Dock = System.Windows.Forms.DockStyle.Left;
            this.lvModels.Location = new System.Drawing.Point(0, 0);
            this.lvModels.MultiSelect = false;
            this.lvModels.Name = "lvModels";
            this.lvModels.Size = new System.Drawing.Size(300, 258);
            this.lvModels.TabIndex = 2;
            this.lvModels.UseCompatibleStateImageBehavior = false;
            this.lvModels.View = System.Windows.Forms.View.Details;
            // 
            // colName
            // 
            this.colName.Text = "Наименование";
            this.colName.Width = 293;
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(306, 5);
            this.btn_add.Name = "btn_add";
            this.btn_add.TabIndex = 3;
            this.btn_add.Text = "Добавить";
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(306, 34);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.TabIndex = 4;
            this.btn_delete.Text = "Удалить";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_view
            // 
            this.btn_view.Location = new System.Drawing.Point(306, 63);
            this.btn_view.Name = "btn_view";
            this.btn_view.TabIndex = 5;
            this.btn_view.Text = "Выгрузить";
            this.btn_view.Click += new System.EventHandler(this.btn_view_Click);
            // 
            // FileAttachList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 258);
            this.Controls.Add(this.btn_view);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.lvModels);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(405, 296);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(405, 296);
            this.Name = "FileAttachList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Прикреплённые файлы";
            this.Load += new System.EventHandler(this.FileAttachList_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvModels;
        private System.Windows.Forms.ColumnHeader colName;
        private DevExpress.XtraEditors.SimpleButton btn_add;
        private DevExpress.XtraEditors.SimpleButton btn_delete;
        private DevExpress.XtraEditors.SimpleButton btn_view;
    }
}