﻿namespace aetp.Common.View.Dialog
{
    partial class UpdaterResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gcInfo = new DevExpress.XtraGrid.GridControl();
            this.gv = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.serverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.localName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.result = new DevExpress.XtraGrid.Columns.GridColumn();
            this.exception = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnFullInfo = new DevExpress.XtraEditors.SimpleButton();
            this.meText = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meText.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(438, 9);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(btnOk_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnFullInfo);
            this.panelControl1.Controls.Add(this.btnOk);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 183);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(525, 44);
            this.panelControl1.TabIndex = 2;
            // 
            // gcInfo
            // 
            this.gcInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcInfo.Location = new System.Drawing.Point(0, 50);
            this.gcInfo.MainView = this.gv;
            this.gcInfo.Name = "gcInfo";
            this.gcInfo.Size = new System.Drawing.Size(525, 133);
            this.gcInfo.TabIndex = 1;
            this.gcInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv});
            // 
            // gv
            // 
            this.gv.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.serverName,
            this.localName,
            this.result,
            this.exception});
            this.gv.GridControl = this.gcInfo;
            this.gv.Name = "gv";
            this.gv.OptionsView.ShowGroupPanel = false;
            this.gv.OptionsView.ShowIndicator = false;
            this.gv.OptionsBehavior.Editable = false;
            // 
            // serverName
            // 
            this.serverName.Caption = "Имя на сервере";
            this.serverName.FieldName = "serverName";
            this.serverName.Name = "serverName";
            this.serverName.Visible = true;
            this.serverName.VisibleIndex = 0;
            // 
            // localName
            // 
            this.localName.Caption = "Имя локальное";
            this.localName.FieldNameSortGroup = "localName";
            this.localName.Name = "localName";
            this.localName.Visible = true;
            this.localName.VisibleIndex = 1;
            // 
            // result
            // 
            this.result.Caption = "Результат";
            this.result.FieldName = "result";
            this.result.Name = "result";
            this.result.Visible = true;
            this.result.VisibleIndex = 2;
            // 
            // exception
            // 
            this.exception.Caption = "Ошибка";
            this.exception.FieldName = "exception";
            this.exception.Name = "exception";
            this.exception.Visible = true;
            this.exception.VisibleIndex = 3;
            // 
            // btnFullInfo
            // 
            this.btnFullInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFullInfo.Location = new System.Drawing.Point(357, 9);
            this.btnFullInfo.Name = "btnFullInfo";
            this.btnFullInfo.Size = new System.Drawing.Size(75, 23);
            this.btnFullInfo.TabIndex = 1;
            this.btnFullInfo.Text = "Подробно";
            this.btnFullInfo.Click += new System.EventHandler(btnFullInfo_Click);
            // 
            // meText
            // 
            this.meText.Dock = System.Windows.Forms.DockStyle.Top;
            this.meText.Location = new System.Drawing.Point(0, 0);
            this.meText.Name = "meText";
            this.meText.Properties.ReadOnly = true;
            this.meText.Size = new System.Drawing.Size(525, 50);
            this.meText.TabIndex = 3;
            this.meText.Width = 400;
            this.meText.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.meText.Properties.Appearance.Options.UseTextOptions = true;
            // 
            // UpdaterResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 230);
            this.Controls.Add(this.gcInfo);
            this.Controls.Add(this.meText);
            this.Controls.Add(this.panelControl1);
            this.Resize += new System.EventHandler(UpdaterResult_Resize);
            this.Load += new System.EventHandler(UpdaterResult_Load);
            this.Name = "UpdaterResult";
            this.Text = "Результат обновления";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meText.Properties)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraGrid.GridControl gcInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gv;
        private DevExpress.XtraGrid.Columns.GridColumn serverName;
        private DevExpress.XtraGrid.Columns.GridColumn localName;
        private DevExpress.XtraGrid.Columns.GridColumn result;
        private DevExpress.XtraGrid.Columns.GridColumn exception;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnFullInfo;
        private DevExpress.XtraEditors.MemoEdit meText;
    }
}