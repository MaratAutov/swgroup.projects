﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using aetp.Common.Control;

namespace aetp.Common.View
{
    public class DependantControl
    {
        // Класс данных о завиимых элементах и элементах источниках
        public class DependantItem
        {
            public List<aetp.Common.ControlModel.BaseControl> DependantControls { get; set; }
            public List<BaseControl> SourceControls { get; set; }
           
            public DependantItem()
            {
                DependantControls = new List<BaseControl>();
            }

            public DependantItem(List<BaseControl> source)
            {
                SourceControls = source;
                DependantControls = new List<BaseControl>();
            }
        }
        
        List<BaseControl> GetSourceControls(string name)
        {
            List<BaseControl> ctrls = new List<BaseControl>();
            ReportUtils.DoAction(report.Children, x => {
                if (Utils.Utils.GetFieldName(x.Name) == name)
                    ctrls.Add(x);
            });
            return ctrls;
        }

        // ключи источники данных список значений зависимые элементы и элементы источника
        static Dictionary<string, DependantItem> Source2Controls = new Dictionary<string, DependantItem>();
        static Report report;

        public void Init(Report rep)
        {
            report = rep;
            Source2Controls.Clear();
            ReportUtils.DoAction(report.Children,
                (bs) =>
                {
                    if (bs is SimpleEdit && !String.IsNullOrEmpty((bs as SimpleEdit).SourceControlName))
                        Add(bs as SimpleEdit);
                    else if (bs is ComboBox && !String.IsNullOrEmpty((bs as ComboBox).SourceControlName))
                        Add(bs as ComboBox);
                });
        }

        #region Add

        public void AddRange(BaseControl parent)
        {
            ReportUtils.DoAction(parent.Children, (bs) => { if (bs is SimpleEdit && !String.IsNullOrEmpty((bs as SimpleEdit).SourceControlName)) { Add(bs as SimpleEdit); } });
        }

        bool Add(SimpleEdit edit)
        {
            if (!Source2Controls.ContainsKey(edit.SourceControlName) || !Source2Controls[edit.SourceControlName].DependantControls.Contains(edit))
            {
                if (!Source2Controls.ContainsKey(edit.SourceControlName))
                {
                    var sources = GetSourceControls(edit.SourceControlName);
                    foreach (var ctrl in sources)
                    {
                        ctrl.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(se_PropertyChanged);
                    }
                    var depItem = new DependantItem(sources);
                    Source2Controls.Add(edit.SourceControlName, depItem);
                }
                Source2Controls[edit.SourceControlName].DependantControls.Add(edit);
                return true;
            }
            else
                return false;
        }

        bool Add(ComboBox edit)
        {
            if (!Source2Controls.ContainsKey(edit.SourceControlName) || !Source2Controls[edit.SourceControlName].DependantControls.Contains(edit))
            {
                
                if (!Source2Controls.ContainsKey(edit.SourceControlName))
                {
                    var sources = GetSourceControls(edit.SourceControlName);
                    foreach (var ctrl in sources)
                    {
                        ctrl.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(se_PropertyChanged);
                    }
                    Source2Controls.Add(edit.SourceControlName, new DependantItem(sources));
                }
                Source2Controls[edit.SourceControlName].DependantControls.Add(edit);
                return true;
            }
            else
                return false;
        }

        #endregion

        #region Remove

        public void RemoveRange(BaseControl parent)
        {
            ReportUtils.DoAction(parent.Children, (bs) => { if (bs is SimpleEdit && !String.IsNullOrEmpty((bs as SimpleEdit).SourceControlName)) { Remove(bs as SimpleEdit); } });
        }

        bool Remove(ComboBox edit)
        {
            if (Source2Controls.ContainsKey(edit.SourceControlName) && Source2Controls[edit.SourceControlName].DependantControls.Contains(edit))
            {
                Source2Controls[edit.SourceControlName].DependantControls.Remove(edit);
                return true;
            }
            else
                return false;
        }

        bool Remove(SimpleEdit edit)
        {
            if (Source2Controls.ContainsKey(edit.SourceControlName) && Source2Controls[edit.SourceControlName].DependantControls.Contains(edit))
            {
                Source2Controls[edit.SourceControlName].DependantControls.Remove(edit);
                return true;
            }
            else
                return false;
        }

        #endregion

        public void AddEditValueHandler(aetp.Common.ControlModel.BaseControl se)
        {
            if (Source2Controls.ContainsKey(Utils.Utils.GetFieldName(se.Name)))
            {
                se.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(se_PropertyChanged);
            }
        }

        void se_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "EditValue")
            {
                BaseControl bc = sender as BaseControl;
                // если страницу удалили, и элементы вследствии чего начинают обнуляться
                if (!bc.ParentPage.IsShownPanel && bc.EditValue == null)
                {
                    return;
                }
                if (Source2Controls.ContainsKey(Utils.Utils.GetFieldName(bc.Name)))
                {
                    foreach (var item in Source2Controls[Utils.Utils.GetFieldName(bc.Name)].DependantControls)
                    {
                        if (String.IsNullOrEmpty(Utils.Utils.GetNamePostfix(bc.Name)) || Utils.Utils.GetNamePostfix(item.Name) == Utils.Utils.GetNamePostfix(bc.Name))
                        {
                            if ((bc is SimpleEdit) && (bc as SimpleEdit).DataType == SimpleDataType.DateTime)
                            {
                                if (bc.EditValue != null)
                                    item.EditValue = Convert.ToDateTime(bc.EditValue);
                                else item.EditValue = bc.EditValue;
                            }
                            else if ((bc is SimpleEdit) && (bc as SimpleEdit).DataType == SimpleDataType.Bool)
                            {
                                if (bc.EditValue.ToString() == "1")
                                    item.EditValue = null;
                                else item.EditValue = bc.EditValue;
                            }
                            else
                            {
                                item.EditValue = bc.EditValue;
                            }
                        }
                    }
                }
            }
        }

        public void GetEditValue(string source_name,BaseControl edit)
        {
            if (Source2Controls.ContainsKey(source_name))
            {
                foreach (var item in Source2Controls[Utils.Utils.GetFieldName(source_name)].SourceControls)
                {
                    if (Utils.Utils.GetNamePostfix(item.Name) == Utils.Utils.GetNamePostfix(edit.Name))
                    {
                        object value = item.EditValue;
                        if (edit is SimpleEdit)
                        {
                            if ((edit as SimpleEdit).DataType == SimpleDataType.DateTime)
                            {
                                if (value != null)
                                    edit.EditValue = Convert.ToDateTime(value);
                                else edit.EditValue = value;
                            }
                            else if ((edit as SimpleEdit).DataType == SimpleDataType.Bool)
                            {
                                if (value != null && value.ToString() == "1")
                                    edit.EditValue = null;
                                else edit.EditValue = value;
                            }
                            else
                            {
                                edit.EditValue = value;
                            }
                        }
                        
                    }
                }
            }
        }

    }
}
