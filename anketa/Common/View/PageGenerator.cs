﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.Windows.Forms;
using System.Drawing;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors;
using aetp.Common.Validator;
using aetp.Common.Control;
using System.Data;
using System.ComponentModel;

namespace aetp.Common.View
{
    public class PageGenerator
    {
        int top = 1;
        int top_panel = 1;
        IEditorWindow window;
        DependantControl dependantControls = new DependantControl();


        public PageGenerator(IEditorWindow window)
        {
            this.window = window;
        }

        public ScrollableControl GeneratePage(Page page)
        {
            ScrollableControl panel = new ScrollableControl();
            panel.AutoScroll = true;
            panel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            panel.Tag = page.Tag;
            panel.Dock = DockStyle.Fill;
            ShowElement(panel, page, page.tlItem);
            return panel;           
        }

        public class GridScrollablePanel : FlowLayoutPanel
        {
            protected override Point ScrollToControl(System.Windows.Forms.Control activeControl)
            {
                if (activeControl is DevExpress.XtraGrid.GridControl)
                    return this.AutoScrollPosition;
                return base.ScrollToControl(activeControl);
            }
        }

        void ShowElement(ScrollableControl panel, Page p, TreeListNode tlItem)
        {
            GridScrollablePanel panel_flow = new GridScrollablePanel();
            panel_flow.Height = panel.Height;
            panel_flow.Width = panel.Width;
            panel_flow.Dock = DockStyle.Fill;
            panel_flow.FlowDirection = FlowDirection.LeftToRight;
            panel_flow.AutoScroll = true;

            panel_flow.HorizontalScroll.Visible = false;
            panel_flow.WrapContents = true;
            panel_flow.Top = 1;
            panel.Controls.Add(panel_flow);

            if (tlItem == null)
                tlItem = window.GetFocusedTreeListNode();

            List<aetp.Common.ControlModel.BaseControl> controls = p.Children;

            top = 1;
            top_panel = 1;

            foreach (aetp.Common.ControlModel.BaseControl ctrl in controls)
            {
                XtraUserControl editor = null;
                ctrl.Parent = p;

                if (ctrl is MetaConrol)
                {
                    Panel_MetaControl(ctrl as MetaConrol, panel_flow, panel, p, editor);

                }
                else
                    Element(ctrl, editor, p, panel, 2, null, panel_flow);
            }

        }

        void Panel_MetaControl(MetaConrol ctrl, ScrollableControl parent, ScrollableControl panel, Page p, XtraUserControl editor)
        {
            List<aetp.Common.ControlModel.BaseControl> controls2 = ctrl.Children;
            GridScrollablePanel pc = new GridScrollablePanel();
            pc.Name = ctrl.Name;
            pc.AutoSize = true;
            pc.AutoSizeMode = AutoSizeMode.GrowOnly;
            pc.WrapContents = true;
            pc.Height = 2;
            pc.Width = panel.Width;
            pc.Top = top;
            pc.FlowDirection = FlowDirection.LeftToRight;
            pc.HorizontalScroll.Visible = false;
            parent.Controls.Add(pc);
            pc.Visible = (ctrl as MetaConrol).Visible;
            ctrl.PropertyChanged += new PropertyChangedEventHandler(MetaControl_PropertyChanged);
            ctrl.Panel = pc;

            foreach (aetp.Common.ControlModel.BaseControl ctrl2 in controls2)
            {
                if (ctrl2 is MetaConrol)
                {
                    Panel_MetaControl(ctrl2 as MetaConrol, pc, panel, p, editor);
                    top = top + pc.Height + 1;
                }
                else
                {
                    Element(ctrl2, editor, p, panel, 1, pc, parent);
                }
            }

            (parent as FlowLayoutPanel).SetFlowBreak(pc, true);
        }

        void MetaControl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Visible")
            {
                MetaConrol meta = sender as MetaConrol;
                meta.Panel.Visible = meta.Visible;
            }
        }
        
        void Element(aetp.Common.ControlModel.BaseControl ctrl, XtraUserControl editor, Page p, ScrollableControl panel, int priznak, ScrollableControl pc, ScrollableControl _flowpanel)
        {
            if (ctrl is SimpleEdit)
            {
                ShowSimpleEdit(ctrl as SimpleEdit, ref editor);
            }
            else if (ctrl is SimpleLabel)
            {
                ShowLabel(ctrl as SimpleLabel, ref editor);
            }
            else if (ctrl is aetp.Common.ControlModel.MemoEdit)
            {
                ShowMemoEdit(ctrl as aetp.Common.ControlModel.MemoEdit, ref editor);
            }
            else if (ctrl is Grid)
            {
                Grid grid = ctrl as Grid;
                aetpGrid gc = new aetpGrid();
                gc.Configure(grid);
                editor = gc as XtraUserControl;
                editor.Width = window.ContentPanel.Width;
                editor.Height = gc.gc.Height;

                editor.Height = gc.HeightGrid;
                var editor_grid = editor as aetpGrid;
                if (gc.lblCaption.Height > editor.Height) editor.Height = gc.lblCaption.Height;
                editor_grid.gc.Width = grid.WidthGrid;
                editor_grid.Width = grid.WidthGrid + grid.WidthCaption + grid.Interval + 100;
                PageControlBinding.BindingGrid(gc, grid);
            }
            else if (ctrl is GridAddedRow)
            {
                GridAddedRow grid = ctrl as GridAddedRow;
                aetpGridAddedRow gc = new aetpGridAddedRow();
                gc.Configure(grid);
                editor = gc as XtraUserControl;
                editor.Width = window.ContentPanel.Width;

                int height;

                if (gc.lblCaption.Height > gc.gc.Height) height = gc.lblCaption.Height;
                else height = gc.gc.Height;
                editor.Height = height + 23;
                gc.Width = grid.WidthGrid;
                var editor_grid = editor as aetpGridAddedRow;
                (editor_grid).gc.Width = grid.WidthGrid;

                editor_grid.panel.Width = grid.WidthGrid + 2;
                editor_grid.panel.Height = gc.HeightGrid - 30;
                editor_grid.panel.Top = gc.gc.Top - 1;

                editor_grid.Width = grid.WidthGrid + grid.WidthCaption + grid.Interval + 100;
                
                if (editor_grid.IsMandatoryToFill)
                {
                    editor_grid.panel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
                    editor_grid.panel.Appearance.BorderColor = System.Drawing.Color.Red;
                }
                else
                {
                    editor_grid.panel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
                }
                PageControlBinding.BindingGridAddedRow(editor_grid, grid);
            }
            else if (ctrl is GridAddedPage)
            {
                GridAddedPage grid = ctrl as GridAddedPage;
                aetpGridAddedPage gc = new aetpGridAddedPage();
                gc.Configure(grid);
                editor = gc as XtraUserControl;
                editor.Width = window.ContentPanel.Width;
                editor.Height = grid.HeightGrid;
                gc.gc.Width = grid.WidthGrid;
                gc.gc.Height = grid.HeightGrid;
                grid.PageChanged += new aetpGridAddedPageChangeEventHandler(gc_PageChanged);
                PageControlBinding.BindGridAddedPage(gc, grid);
            }
            else if (ctrl is MetaGrid)
            {
                MetaGrid grid = ctrl as MetaGrid;
                aetpMetaGrid gc = new aetpMetaGrid();
                gc.Configure(grid);
                editor = gc as XtraUserControl;
                editor.Width = window.ContentPanel.Width;
                PageControlBinding.BindingMetaGrid(gc, grid);
            }
            else if (ctrl is aetp.Common.ControlModel.ComboBox)
            {
                aetp.Common.ControlModel.ComboBox combo = ctrl as aetp.Common.ControlModel.ComboBox;
                aetpComboBoxEdit comboEdit = new aetpComboBoxEdit();
                comboEdit.Name = combo.Name;
                comboEdit.Caption = combo.Caption;
                comboEdit.Items = combo.Source;
                editor = comboEdit;
                var editor_baseEdit = editor as aetpBaseEdit;
                editor_baseEdit.WidthCaption = combo.WidthCaption;
                editor_baseEdit.Interval = combo.Interval;
                editor_baseEdit.Editor.Width = combo.ComboWidth;

                ComboEditorValidation.AddValidation(combo, editor, window as ContainerControl);
                editor.Width = window.ContentPanel.Width;
            }
            else if (ctrl is SimpleRichEdit)
            {
                SimpleRichEdit rich = ctrl as SimpleRichEdit;
                aetpRichEdit richEdit = new aetpRichEdit();
                richEdit.Name = rich.Name;
                editor = richEdit;
                var editor_rich = editor as aetpRichEdit;

                editor_rich.WidthCaption = rich.WidthCaption;
                editor_rich.Interval = rich.Interval;
                editor_rich.rich.Width = rich.WidthControl;
                editor_rich.rich.Height = rich.HeigthControl;
                editor_rich.rich.Text = null;
                editor_rich.Caption = rich.Caption;
                RichEditorValidation.AddValidation(rich, editor, window as ContainerControl);
                editor_rich.Width = window.ContentPanel.Width;
                editor_rich.Height = rich.HeigthControl + 2;
                editor_rich.IsMandatoryToFill = rich.IsMandatoryToFill;
                if (rich.IsMandatoryToFill)
                {
                    editor_rich.errorProvider.SetIconAlignment(editor_rich.rich, ErrorIconAlignment.MiddleLeft);
                    editor_rich.errorProvider.SetIconPadding(editor_rich.rich, 1);
                }

            }
            if (editor != null)
            {
                if (priznak == 1)
                {
                    pc.Controls.Add(editor);
                    (pc as FlowLayoutPanel).SetFlowBreak(editor, true);
                    top_panel = top_panel + editor.Height + 1;
                    top = top + editor.Height + 1;
                    pc.Width = panel.Width;

                }
                else if (priznak == 2)
                {
                    editor.Top = top;
                    _flowpanel.Controls.Add(editor);
                    (_flowpanel as FlowLayoutPanel).SetFlowBreak(editor, true);
                    top = top + editor.Height + 1;
                    top_panel = top;
                }
            }

        }

        void gc_PageChanged(object sender, aetpGridAddedPageChange e)
        {
            if (e.changeType == ChangeType.Add)
            {
                dependantControls.AddRange(e.page);
            }
            else
            {
                dependantControls.RemoveRange(e.page);
            }
        }

        void ShowSimpleEdit(SimpleEdit se, ref XtraUserControl editor)
        {
            switch (se.DataType)
            {
                case SimpleDataType.String:
                    editor = new aetpTextEdit();
                    (editor as aetpTextEdit).MaxLength = se.MaxLength;
                    break;
                case SimpleDataType.DateTime:
                    editor = new aetpDateEdit();
                    (editor as aetpDateEdit).DateEditMask = "dd.MM.yyyy";
                    break;
                case SimpleDataType.Integer:
                case SimpleDataType.Double:
                    editor = new aetpNumericEdit(se);
                    break;
                case SimpleDataType.Bool:
                    editor = new aetpCheckEdit();
                    break;
            }

            SimpleEditorValidation.AddValidation(se, editor, window as ContainerControl);
            (editor as aetpBaseEdit).Caption = se.Caption;
            editor.Name = se.Name;
            editor.Width = window.ContentPanel.Width;
            var editor_baseEdit = editor as aetpBaseEdit;
            editor_baseEdit.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;

            editor_baseEdit.Editor.Width = se.WidthControl;
            editor_baseEdit.Editor.Height = se.HeigthControl;

            editor_baseEdit.WidthCaption = se.WidthCaption;
            editor_baseEdit.Interval = se.Interval;

        }

        void ShowMemoEdit(aetp.Common.ControlModel.MemoEdit se, ref XtraUserControl editor)
        {
            editor = new aetpMemoEdit();

            MemoEditorValidation.AddValidation(se, editor, window as ContainerControl);
            (editor as aetpMemoEdit).Caption = se.Caption;
            editor.Name = se.Name;
            editor.Width = window.ContentPanel.Width;

            var editor_memo = editor as aetpMemoEdit;
            editor_memo.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;

            editor_memo.Editor.Width = se.WidthControl;
            editor_memo.Editor.Height = se.HeigthControl;

            editor_memo.WidthCaption = se.WidthCaption;
            editor_memo.Interval = se.Interval;
        }

        void ShowLabel(SimpleLabel se, ref XtraUserControl editor)
        {
            editor = new aetpLabel();
            var editor_label = editor as aetpLabel;
            editor_label.Caption = se.Caption;
            editor_label.Name = se.Name;
            editor_label.LabelFont = se.Font.Font;
            editor_label.Width = window.ContentPanel.Width;
            editor_label.WidthCaption = se.WidthCaption;
            editor_label.Height = editor_label.HeightCaption + 20;
        }
    }
}
