﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace aetp.Common.View
{
    public partial class BaseToolWindow : DockContent
    {

        public new event EventHandler Closing;
        protected void RaiseClosing()
        {
            if (Closing != null)
                Closing(this, null);
        }

        public BaseToolWindow()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
            RaiseClosing();
        }
    }
}
