﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.Windows.Forms;

namespace aetp.Common.View
{
    public class PageManager
    {
        static SortedDictionary<string, PageViewInfo> _PagePanels = new SortedDictionary<string, PageViewInfo>();
        PageGenerator generator;
        IEditorWindow window;
        const byte maxPagesCount = 7;

        public PageManager(IEditorWindow window)
        {
            this.window = window;
            this.generator = new PageGenerator(window);
        }

        public ScrollableControl GetPanel(string name)
        {
            if (_PagePanels.ContainsKey(name))
                return _PagePanels[name].panel;
            else return null;
        }

        public void RefreshPage(Page page)
        {
            DisposePage(page);
            ShowPage(page, false);
        }

        public void ShowPage(Page page, bool create = false)
        {
            HideAllPanels();
            if (create)
            {
                window.CreatePageButton.Enabled = false;
                window.ClearPageButton.Enabled = true;
            }
            else
            {
                window.CreatePageButton.Enabled = page.IsCreatable();
                window.ClearPageButton.Enabled = page.IsClearable();
            }
            if (_PagePanels.ContainsKey(page.Name))
            {
                GetPanel(page.Name).Visible = true;
                page.IsShownPanel = true;
            }
            else
            {
                if (_PagePanels.Count >= maxPagesCount)
                {
                    DisposeOldPages();
                }
                
                if (!page.IsShownPanel && !create)
                    return;
                var panel = generator.GeneratePage(page);
                AddPanel(page.Name, page, panel);
                window.ContentPanel.Controls.Add(panel);
                page.IsShownPanel = true;
            }
        }

        void AddPanel(string name, Page page, ScrollableControl panel)
        {
            PageViewInfo pageViewInfo = new PageViewInfo() { page = page, panel = panel };
            _PagePanels.Add(name, pageViewInfo);
        }

        public void ReInit()
        {
            foreach (var page in _PagePanels)
            {
                page.Value.panel.Dispose();
            }
            _PagePanels.Clear();
        }

        public void DisposePage(Page page)
        {
            if (_PagePanels.ContainsKey(page.Name))
            {
                var itemForDelete = _PagePanels.Where(x => x.Key == page.Name).FirstOrDefault();
                window.ContentPanel.Controls.Remove(itemForDelete.Value.panel);
                _PagePanels.Remove(itemForDelete.Key);
                itemForDelete.Value.panel.Dispose();
            }
        }

        void DisposeOldPages()
        {
            byte pageForRemoveCount = (byte)(_PagePanels.Count - maxPagesCount + 1);
            for (int i = 0; i < pageForRemoveCount; i++)
            {
                var itemForDelete = _PagePanels.First();
                window.ContentPanel.Controls.Remove(itemForDelete.Value.panel);
                _PagePanels.Remove(itemForDelete.Key);
                itemForDelete.Value.panel.Dispose();
            }
        }

        void HideAllPanels()
        {
            foreach (var panelInfo in _PagePanels.Values)
            {
                panelInfo.panel.Visible = false;
            }
        }
    }
}
