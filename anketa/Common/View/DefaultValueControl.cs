﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using aetp.Common.Control;

namespace aetp.Common.View
{
    public class DefaultValueControl
    {
        public static string GetDefaultValue(string default_value)
        {
            string result = string.Empty;

            foreach (OrganizationDataConfig.OrganizationDataInfo item in AppGlobal.Instance.OrganizationConfig.ListOrganizationDataInfo)
            {
                if (item.Name == default_value)
                    result = item.Data;
            }            
            return result;
        }
    }
}
