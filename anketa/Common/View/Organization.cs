﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using DevExpress.XtraEditors.DXErrorProvider;

namespace aetp.Common.View
{
    public partial class Organization : Form
    {
        public Organization()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Regex regex_inn = new Regex(@"^(\d{10})$");
            Regex regex_ogrn = new Regex(@"^\d{13}$");
            if (regex_inn.IsMatch(txt_inn.Text.ToString()))
            {
                if (regex_ogrn.IsMatch(txt_ogrn.Text.ToString()))
                {
                    AppGlobal.Instance.OrganizationConfig.AddOrganizationData("ПолноеНаименование", txt_name.Text.Trim());
                    AppGlobal.Instance.OrganizationConfig.AddOrganizationData("КраткоеНаименование", txt_sname.Text.Trim());
                    AppGlobal.Instance.OrganizationConfig.AddOrganizationData("ИНН", txt_inn.Text.Trim());
                    AppGlobal.Instance.OrganizationConfig.AddOrganizationData("ОГРН", txt_ogrn.Text.Trim());
                    AppGlobal.Instance.OrganizationConfig.AddOrganizationData("ПризнакОрганизации", chk_kredit.Checked.ToString());
                    AppGlobal.Instance.OrganizationConfig.AddOrganizationData("АдресМестаНахождения", txt_adress.Text.Trim());
                    AppGlobal.Instance.OrganizationConfig.AddOrganizationData("КонтактныйТелефон", txt_phone.Text.Trim());
                    AppGlobal.Instance.OrganizationConfig.AddOrganizationData("АдресЭП", txt_mail.Text.Trim());
                    AppGlobal.Instance.OrganizationConfig.AddOrganizationData("ФИОРуководителя", txt_fio.Text.Trim());
                    AppGlobal.Instance.OrganizationConfig.AddOrganizationData("Должность", txt_dolj.Text.Trim());
                    this.Close();
                }
                else MessageBox.Show("Неправильно заполнено поле ОГРН"); 
            }
            else
            {
                MessageBox.Show("Неправильно заполнено поле ИНН");               
            }
        }

        private void Organization_Load(object sender, EventArgs e)
        {
            string fileName = AppGlobal.Instance.OrganizationConfig.GetConfigFilePath();
            if (File.Exists(fileName))
            {
                foreach (OrganizationDataConfig.OrganizationDataInfo item in AppGlobal.Instance.OrganizationConfig.ListOrganizationDataInfo)
                {
                    if (item.Name == "ПолноеНаименование")
                        txt_name.Text = item.Data;
                    if (item.Name == "КраткоеНаименование")
                        txt_sname.Text = item.Data;
                    if (item.Name == "ИНН")
                        txt_inn.Text = item.Data;
                    if (item.Name == "ОГРН")
                        txt_ogrn.Text = item.Data;
                    if (item.Name == "ПризнакОрганизации")
                        chk_kredit.Checked = Convert.ToBoolean(item.Data);
                    if (item.Name == "АдресМестаНахождения")
                        txt_adress.Text = item.Data;
                    if (item.Name == "КонтактныйТелефон")
                        txt_phone.Text = item.Data;
                    if (item.Name == "АдресЭП")
                        txt_mail.Text = item.Data;
                    if (item.Name == "ФИОРуководителя")
                        txt_fio.Text = item.Data;
                    if (item.Name == "Должность")
                        txt_dolj.Text = item.Data;
                }
            }            
        }        
    }
}
