﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace aetp.Common.View
{
    public partial class AboutProgram : DevExpress.XtraEditors.XtraForm
    {
        public AboutProgram()
        {
            InitializeComponent();
            wb.IsWebBrowserContextMenuEnabled = false;
        }

        private void AboutProgram_Load(object sender, EventArgs e)
        {
            try
            {
                wb.DocumentText = File.ReadAllText("releasenotes.htm");
            }
            catch
            {
                wb.DocumentText = "Не удалось загрузить список изменений...";
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
