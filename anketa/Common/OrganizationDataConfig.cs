﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace aetp.Common
{
    public class OrganizationDataConfig
    {
        const string ConfigFileName = "OrganizationDataConfig.xml";        

        public struct OrganizationDataInfo  
        {
            public string Name;
            public string Data;
        }

        public List<OrganizationDataInfo> ListOrganizationDataInfo {get; set; }

        class OrganizationDataInfoSearcher
        {
            private string name;
            private string data;

            public OrganizationDataInfoSearcher(string name, string data)
            {
                this.name = name;
                this.data = data;
            }

            public string Name
            {
                get { return name; }
                set { name = value; }
            }

            public string Data
            {
                get { return data; }
                set { data = value; }
            }

            public bool Condition(OrganizationDataConfig.OrganizationDataInfo organization_info)
            {
                return organization_info.Name == name;
            }
        }

        public OrganizationDataConfig()
        {
           
        }
                
        public string GetConfigFilePath()
        {
            return Path.Combine(aetp.Utils.Config.GetAppPath(), ConfigFileName);
        }

        public void Save()
        {
            FileStream fs = new FileStream(GetConfigFilePath(), FileMode.Create);
            XmlSerializer serializer = new XmlSerializer(typeof(OrganizationDataConfig));
            serializer.Serialize(fs, this);
            fs.Close();
        }

        public void Load()
        {
            string fileName = GetConfigFilePath();
            if (!File.Exists(fileName))
            {
                Save();
            }
            FileStream fs = new FileStream(fileName, FileMode.Open);
            XmlSerializer serializer = new XmlSerializer(typeof(OrganizationDataConfig));
            OrganizationDataConfig cfg = (OrganizationDataConfig)serializer.Deserialize(fs);
            fs.Close();

            ListOrganizationDataInfo = cfg.ListOrganizationDataInfo;
        }

        public void AddOrganizationData(string name, string data)
        {
            OrganizationDataConfig.OrganizationDataInfo item = new OrganizationDataConfig.OrganizationDataInfo();
            item.Name = name;
            item.Data = data;

            AppGlobal.Instance.OrganizationConfig.ListOrganizationDataInfo.Remove(AppGlobal.Instance.OrganizationConfig.ListOrganizationDataInfo.Find(new OrganizationDataInfoSearcher(name, data).Condition));
            AppGlobal.Instance.OrganizationConfig.ListOrganizationDataInfo.Add(item);
            AppGlobal.Instance.OrganizationConfig.Save();            
        }
    }
}
