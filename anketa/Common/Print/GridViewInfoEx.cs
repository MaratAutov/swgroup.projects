﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace aetp.Common.Print
{
	class GridViewInfoEx : GridViewInfo
	{
		public GridViewInfoEx(DevExpress.XtraGrid.Views.Grid.GridView gridView)
			: base(gridView) { }

		public bool GetIsPrinting()
		{
			return IsPrinting;
		}
	}
}
