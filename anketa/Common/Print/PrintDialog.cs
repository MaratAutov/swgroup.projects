﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using aetp.Common.ControlModel;

namespace aetp.Common.Print
{
	public enum PrintOptionsEnum
	{
        None = 0,
		All = 1,
		Selected
	}

	public partial class PrintDialog : Form
	{
		public PrintDialog()
		{
			InitializeComponent();
		}

		public PrintOptionsEnum GetPrintOptions
		{
			get
			{
                if (rbSelected.Checked)
                    return PrintOptionsEnum.Selected;               

				return PrintOptionsEnum.All;
			}
		}

        public static PrintOptionsEnum PrintOptions(PrintingForm printing)
		{
			using (PrintDialog frm = new PrintDialog())
			{                
                if (frm.ShowDialog() == DialogResult.Cancel)
                    return PrintOptionsEnum.None;

				return frm.GetPrintOptions;
			}
		}

		private void btnPrint_Click(object sender, EventArgs e)
		{
			this.Close();
			this.DialogResult = DialogResult.OK;
		}        
	}
}
