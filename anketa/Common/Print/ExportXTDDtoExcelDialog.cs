﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace aetp.Common.Print
{
    public partial class ExportXTDDtoExcelDialog : Form
    {
        public ExportXTDDtoExcelDialog()
        {
            InitializeComponent();
        }

        string[] xtddFileNames = null;
        private void XTDDPath_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            OpenFileDialog fbd = new OpenFileDialog();
            fbd.Multiselect = true;
            fbd.Filter = string.Format("(*.{0})|*.{0}", "xtdd");
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                xtddFileNames = fbd.FileNames;
                XTDDPath.Text = string.Join(", ", fbd.FileNames);
            }
        }

        private void ExcelPath_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            SaveFileDialog fbd = new SaveFileDialog();
            fbd.Filter = string.Format("(*.{0})|*.{0}", "xls");
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ExcelPath.Text = fbd.FileName;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            bool packetConvert = xtddFileNames.Length > 1;
            List<ConvertResult> errorResult = new List<ConvertResult>();
            foreach (string xtddFileName in xtddFileNames)
            {
                string xlsfileName = "";
                if (packetConvert)
                {
                    xlsfileName = Path.Combine(Path.GetDirectoryName(xtddFileName), Path.GetFileNameWithoutExtension(xtddFileName)) + ".xls";
                }
                else
                {
                    xlsfileName = ExcelPath.Text;
                }
                var result = Convert(xtddFileName, xlsfileName);
                if (!result.Successfully)
                {
                    errorResult.Add(result);
                }
            }
            Cursor.Current = Cursors.Default;
            if (!packetConvert)
            {
                OpenFile(ExcelPath.Text);
            }
            else
            {
                ShowResult(errorResult, xtddFileNames.Length);
            }
        }

        void ShowResult(List<ConvertResult> errors, int totalCount)
        {
            if (errors.Count == 0)
            {
                MessageBox.Show("Все файлы были успешно конвертированы");
            }
            else
            {
                string err = errors.Select(x => x.Error).Aggregate((x, y) => x + Environment.NewLine + y);
                string suberr = err.Substring(0, 255);
                if (err.Length > 255)
                {
                    suberr += Environment.NewLine + "...";
                }
                MessageBox.Show(String.Format("Всего конвертировано {0} файлов из {1}{2}{3}", 
                    totalCount - errors.Count, totalCount, Environment.NewLine, suberr));
            }
        }

        ConvertResult Convert(string xtddFile, string xlsFile)
        {
            ProcessStartInfo cmd = new ProcessStartInfo();
            cmd.WindowStyle = ProcessWindowStyle.Hidden;
            cmd.FileName = "Xtdd2Xls.exe";
            cmd.Arguments = String.Format("\"{0}\" \"{1}\"", xtddFile, xlsFile);
            cmd.RedirectStandardOutput = true;
            cmd.UseShellExecute = false;
            cmd.CreateNoWindow = true;
            cmd.StandardOutputEncoding = Encoding.UTF8;
            Process p = Process.Start(cmd);
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            if (output.EndsWith("Successfuly exported\r\n"))
            {
                return new ConvertResult();
            }
            else
            {
                return new ConvertResult(String.Format("Ошибка конвертирования \"{0}\": {1}", xtddFile, output));
            }
        }

        void OpenFile(string fileName)
        {
            if (MessageBox.Show(String.Format("Файл успешно экспортирован. Желаете открыть файл {0}", fileName),
                "Сконвертировано", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                Process p = new Process();
                p.StartInfo.FileName = fileName;
                try
                {
                    p.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("Ошибка открытия файла {0}\n{1}", fileName, ex.Message));
                }
            }
        }

        private void XTDDPath_EditValueChanged(object sender, EventArgs e)
        {
            ValidateOK();
            ValidateExcelPath();
        }

        public void ValidateExcelPath()
        {
            if (xtddFileNames == null || xtddFileNames.Length == 1)
            {
                ExcelPath.Enabled = true;
            }
            else
            {
                ExcelPath.Enabled = false;
            }
        }

        public void ValidateOK()
        {
            if (xtddFileNames != null && xtddFileNames.Length > 1)
            {
                btnOk.Enabled = true;
            }
            else if (xtddFileNames == null)
            {
                btnOk.Enabled = false;
            }
            else
            {
                btnOk.Enabled = (xtddFileNames.Length == 1 && ExcelPath.Text.Trim().Length > 0 && File.Exists(XTDDPath.Text.Trim())) ? true : false;
            }
        }

        private void ExcelPath_EditValueChanged(object sender, EventArgs e)
        {
            ValidateOK();
        }       
    }

    public class ConvertResult
    {
        public bool Successfully;
        public string Error;

        public ConvertResult()
	    {
            Successfully = true;
	    }

        public ConvertResult(string error)
	    {
            Successfully = false;
            Error = error;
	    }
    }
}
