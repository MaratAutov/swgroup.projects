﻿namespace aetp.Common.Print
{
    partial class ExportXTDDtoExcelDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.XTDDPath = new DevExpress.XtraEditors.ButtonEdit();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.ExcelPath = new DevExpress.XtraEditors.ButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.XTDDPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExcelPath.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(55, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Файл XTDD";
            // 
            // XTDDPath
            // 
            this.XTDDPath.EditValue = "";
            this.XTDDPath.Location = new System.Drawing.Point(158, 12);
            this.XTDDPath.Name = "XTDDPath";
            this.XTDDPath.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.XTDDPath.Size = new System.Drawing.Size(497, 20);
            this.XTDDPath.TabIndex = 2;
            this.XTDDPath.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.XTDDPath_ButtonClick);
            this.XTDDPath.EditValueChanged += new System.EventHandler(this.XTDDPath_EditValueChanged);
            this.XTDDPath.Properties.ReadOnly = true;
            // 
            // btnOk
            // 
            this.btnOk.Enabled = false;
            this.btnOk.Location = new System.Drawing.Point(158, 82);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 51);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Файл Excel";
            // 
            // ExcelPath
            // 
            this.ExcelPath.EditValue = "";
            this.ExcelPath.Location = new System.Drawing.Point(158, 48);
            this.ExcelPath.Name = "ExcelPath";
            this.ExcelPath.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ExcelPath.Size = new System.Drawing.Size(497, 20);
            this.ExcelPath.TabIndex = 6;
            this.ExcelPath.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ExcelPath_ButtonClick);
            this.ExcelPath.EditValueChanged += new System.EventHandler(this.ExcelPath_EditValueChanged);
            this.ExcelPath.Properties.ReadOnly = true;
            // 
            // ExportXTDDtoExcelDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 122);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.ExcelPath);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.XTDDPath);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(691, 160);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(691, 160);
            this.Name = "ExportXTDDtoExcelDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Экспорт XTDD в Excel";
            ((System.ComponentModel.ISupportInitialize)(this.XTDDPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExcelPath.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ButtonEdit XTDDPath;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ButtonEdit ExcelPath;
    }
}