﻿using aetp.DocumentsSubmitter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using aetp.Common.ControlModel;
using System.Xml;
using aetp.Common.Serializer;
using System.Text;

namespace UnitTests
{
    
    
    /// <summary>
    ///Это класс теста для ConfigTest, в котором должны
    ///находиться все модульные тесты ConfigTest
    ///</summary>
    [TestClass()]
    public class DesignerSaveLoad
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Тест для Save
        ///</summary>
        [TestMethod()]
        public void SaveTest()
        {
            string path = Path.GetTempFileName();
            //create report in temp file
            Report report = new Report();
            report.RunMode = aetp.Common.ViewType.Constructor;
            Page page = new Page();
            report.AddChildren(page, report);
            page.AddChildren(new SimpleEdit(), report);
            Page page1 = new Page();
            Grid grid1 = new Grid();
            grid1.Columns.Add(new GridColumn());
            grid1.Columns.Add(new GridColumn());
            grid1.Rows.Add(new GridRow());
            grid1.Rows.Add(new GridRow());
            
            GridAddedRow grid2 = new GridAddedRow();
            page1.AddChildren(grid2, report);
            grid2.Columns.Add(new GridAddedRowColumn());

            GridAddedPage grid3 = new GridAddedPage();
            page1.AddChildren(grid3, report);
            grid3.Columns.Add(new GridAddedPageColumn());
            grid3.Page = new Page();
            grid3.Children.Add(grid3.Page);
            grid3.Page.AddChildren(new SimpleEdit() { DataType = SimpleDataType.Double } , report);

            page1.AddChildren(grid1, report);
            
            report.AddChildren(page1, report);
            using (XmlTextWriter xmlTextWriter = new XmlTextWriter(path, Encoding.UTF8))
            {
                new FormSerializer().Serialize(xmlTextWriter, report);
            }

            //load report from temp file
            Report report1;
            Assert.IsTrue(File.Exists(path));
            using (XmlTextReader reader = new XmlTextReader(path))
            {
                report1 = new FormSerializer().Deserialize(reader, true);
            }
            Assert.IsNotNull(report1);
            Assert.AreEqual(report1.Children.Count, 2);
            Assert.AreEqual(report1.Children[0].Children[0].GetType(), typeof(SimpleEdit));
            Assert.AreEqual((report1.Children[1].Children[2] as Grid).Rows.Count, 2);
            Assert.AreEqual((report1.Children[1].Children[0] as GridAddedRow).Columns.Count, 1);
            Assert.AreEqual(((report1.Children[1].Children[1] as GridAddedPage).Page.Children[0] as SimpleEdit).DataType, SimpleDataType.Double );
        }

        
    }
}
