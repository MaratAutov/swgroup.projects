﻿using aetp.Common.ControlModel.UITypeEditors;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using aetp.Common.ControlModel;
using aetp.Common.Serializer;

namespace UnitTests
{
    
    
    /// <summary>
    ///Это класс теста для gridEditFormTest, в котором должны
    ///находиться все модульные тесты gridEditFormTest
    ///</summary>
    [TestClass()]
    public class gridEditFormTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        /// Проверка связи ссылки поля Column у ячейки с реальной колонкой из грида
        ///</summary>
        [TestMethod()]
        public void gridEditForm_ChangeColumnNameTest()
        {
            // создаем редктор грида и добавляем в грид 4 колонки и одну строчку
            gridEditForm_Accessor editor = new gridEditForm_Accessor();
            Report report = new Report();
            Grid grid = new Grid();
            report.AddChildren(grid, report);
            editor.EditValue = grid;
            for (int i = 0; i < 4; i++)
            {
                editor.btnColumnAdd_Click(null, null);
                editor.model.Columns[i].Name = "Col0" + (i+1).ToString();
            }

            editor.btnAddRow_Click(null, null);
            // меняем имя у колонки и проверяем равны ли имена колонки с ссылкой на колонку через ячейку
            Assert.AreEqual(editor.model.Columns[0], editor.model.Rows[0].Cells[0].Column);
            editor.model.Columns[0].Name += "1";
            Assert.AreEqual(editor.model.Columns[0], editor.model.Rows[0].Cells[0].Column);
            Assert.AreEqual(editor.model.Columns[0].Name, editor.model.Rows[0].Cells[0].Column.Name);
            
            // открываем грид в новом редакторе, опять меняем имя колонки и проверяем эквивалентность имен у колонки и у колонки на которую ссылается ячейка колонки
            gridEditForm_Accessor editor1 = new gridEditForm_Accessor();
            editor1.EditValue = editor.model;
            Assert.AreEqual(editor1.model.Columns[0], editor1.model.Rows[0].Cells[0].Column);
            editor1.model.Columns[0].Name += "1";
            Assert.AreEqual(editor1.model.Columns[0], editor1.model.Rows[0].Cells[0].Column);
            Assert.AreEqual(editor1.model.Columns[0].Name, editor1.model.Rows[0].Cells[0].Column.Name);

        }
    }
}
