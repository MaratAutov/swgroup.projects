﻿using aetp.Common.Mediator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml;
using aetp.Common.View;
using aetp.Common.ControlModel;
using System.Text;
using aetp.Common.Control;
using System.IO;

namespace UnitTests
{
    
    
    /// <summary>
    ///Это класс теста для EditorViewSerializerTest, в котором должны
    ///находиться все модульные тесты EditorViewSerializerTest
    ///</summary>
    [TestClass()]
    public class EditorViewSerializerTest
    {


        private TestContext testContextInstance;
        static Report report;
        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            report = new Report();
            report.Name = "report";
            report.SerializingTag = "report";
            //report.MetaData = new ReportMetaData();
            GridAddedRow grid = new GridAddedRow();
            grid.Root = report;
            grid.Name = "grid";
            grid.SerializingTag = "grid";
            grid.SerializingRowTag = "grid_1";
            grid.Columns.Add(new GridAddedRowColumn() { Name = "Col01", SerializingTag = "Col01", Type = GridCellType.Label });
            grid.Data = null;
            new aetpGridAddedRow().Configure(grid);
            grid.NewRow();
            report.AddChildren(grid, report);
            TreeListMediator tree = new TreeListMediator(new EditorMediator(new EditorView(), report, ""));
        }
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Тест для Serialize
        ///</summary>
        [TestMethod()]
        public void SerializeTest()
        {
            EditorViewSerializer target = new EditorViewSerializer(); // TODO: инициализация подходящего значения
            StringBuilder sb = new StringBuilder();
            XmlWriter writer = XmlWriter.Create(sb); // TODO: инициализация подходящего значения
            target.Serialize(writer, report);
            string str = sb.ToString();
            var reader = XmlReader.Create(new StringReader(str), new XmlReaderSettings() { IgnoreWhitespace = false });
            XmlHelper xml = new XmlHelper(reader);
            var node = xml.GetNode("*/*/*/Col01");
            Assert.AreEqual(" ", node.Value);
        }
                
    }
}
