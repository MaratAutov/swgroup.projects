﻿using aetp.DocumentsSubmitter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace UnitTests
{
    
    
    /// <summary>
    ///Это класс теста для ConfigTest, в котором должны
    ///находиться все модульные тесты ConfigTest
    ///</summary>
    [TestClass()]
    public class ConfigTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Тест для Save
        ///</summary>
        [TestMethod()]
        public void SaveTest()
        {
            Config target = new Config(); // TODO: инициализация подходящего значения
            target.Save();
            bool exists = File.Exists(target.GetConfigFilePath());
            Assert.IsTrue(exists);

        }

        /// <summary>
        ///Тест для Load
        ///</summary>
        [TestMethod()]
        public void LoadTest()
        {
            Config target = new Config(); // TODO: инициализация подходящего значения
            target.Load();
            Assert.AreEqual(target.Transport_FFMS_ReportsService, "http://lk.fcsm.ru:8080/ReportService.asmx");
        }
    }
}
