﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetpControls.Visual;
using aetpControls;
using DevExpress.XtraTreeList.Nodes;

namespace anketa
{
    public class aetpReportDeserializer
    {
        EditMainForm window;

        public EditMainForm Deserialize(Report report)
        {
            window = new EditMainForm();
            aetpControlTreeListWrapper tlWrapperRoot = new aetpControlTreeListWrapper(null, null);
            aetpControlTreeListWrapper tlWrapperReport = new aetpControlTreeListWrapper(tlWrapperRoot, report);
            createTreeList(tlWrapperReport, report.Children);
            window.tlMain.DataSource = tlWrapperRoot;
            window.report = report;
            window.tlMain.ExpandAll();
            return window;
        }

        private void createTreeList(aetpControlTreeListWrapper parentNode, List<aetpControl> children)
        {
            foreach (aetpControl control in children)
            {
                aetpControlTreeListWrapper node = new aetpControlTreeListWrapper(parentNode, control);
                if (control.Children.Count > 0)
                    createTreeList(node, control.Children);
            }
        }
    }
}
