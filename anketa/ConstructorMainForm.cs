﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using aetpControls;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraTreeList.Nodes.Operations;
using DevExpress.XtraTreeList.Nodes;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Windows.Markup;
using System.Windows;
using aetpControls.Visual;

namespace anketa
{
    public partial class ConstructorMainForm : RibbonForm
    {
        #region fields
        
        Report report = new Report();
        aetpControlTreeListWrapper tlWrapperRoot = new aetpControlTreeListWrapper(null, null);
        aetpControlTreeListWrapper tlWrapperReport;

        #endregion

        public ConstructorMainForm()
        {
            InitializeComponent();
            tlWrapperReport = new aetpControlTreeListWrapper(tlWrapperRoot, report);
        }

        #region Event Handlers

        private void bbiAddPage_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AddControl(new Page());
        }

        private void bbiAddSimpleEditor_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AddControl(new SimpleEdit());
        }

        private void tlControls_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (e.Node != null)
                propertyGrid.SelectedObject = GetFocusedControl();
        }

        private void ConstructorMainForm_Load(object sender, EventArgs e)
        {
            tlControls.DataSource = tlWrapperRoot;
        }

        private void propertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            tlControls.RefreshDataSource();
        }

        private void bbiGenerateCode_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //GenerateReportOperation operation = new GenerateReportOperation();
            //tlControls.NodesIterator.DoLocalOperation(operation, tlControls.Nodes);
            //Report report = operation.report;
            MemoryStream memoryStream = new MemoryStream();
            XmlSerializer xs = new XmlSerializer(typeof(Report));
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream,Encoding.Default);
            xs.Serialize(xmlTextWriter, report);
            string result = Encoding.Default.GetString(memoryStream.ToArray());
            meCode.Text = result;
        }

        private void bbiDeserialize_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Report));
            Report report;
            using (TextReader reader = new StringReader(meCode.Text))
            {
                report = (Report)xs.Deserialize(reader);
            }
            if (report == null) return;
            aetpReportDeserializer deser = new aetpReportDeserializer();
            EditMainForm form = deser.Deserialize(report);
            form.Show();
        }

        private void bbiRemoveItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RemoveFocusedControl();
        }

        #endregion

        #region private methods

        void RemoveFocusedControl()
        {
            aetpControlTreeListWrapper node = GetFocusedTreeListWrapper();
            node.objectCore.Remove();
            node.Remove();
            tlControls.RefreshDataSource();
        }

        aetpControlTreeListWrapper GetFocusedTreeListWrapper()
        {
            if (tlControls.FocusedNode != null)
                return tlControls.GetDataRecordByNode(tlControls.FocusedNode) as aetpControlTreeListWrapper;
            else
                return aetpControlTreeListWrapper.Null;
        }

        aetpControl GetFocusedControl()
        {
            return GetFocusedTreeListWrapper().objectCore;
        }

        aetpControlTreeListWrapper AddControl(aetpControl control)
        {
            aetpControlTreeListWrapper parentNode = tlWrapperReport;
            aetpControl parentControl = report;
            
            if (tlControls.FocusedNode != null)
            {
                parentNode = GetFocusedTreeListWrapper();
                parentControl = parentNode.objectCore;
            }
            aetpControlTreeListWrapper node = new aetpControlTreeListWrapper(parentNode, control);
            parentControl.AddChildren(control);
            control.Parent = parentControl;
            tlControls.RefreshDataSource();
            if (tlControls.FocusedNode == null)
                tlControls.FocusedNode = tlControls.Nodes[0];
            tlControls.FocusedNode.Expanded = true;
            return node;
        }

        #endregion

    }

    public class GenerateReportOperation : TreeListOperation
    {
        public Report report { get; set; }
        public GenerateReportOperation()
            : base()
        {
            report = new Report();
        }
        public override void Execute(TreeListNode node)
        {
            if (node.ParentNode == null)
                report.AddChildren((aetpControl)(node.TreeList.GetDataRecordByNode(node) as aetpControlTreeListWrapper).objectCore.Clone());
            else
                (node.ParentNode.TreeList.GetDataRecordByNode(node.ParentNode) as aetpControlTreeListWrapper).objectCore
                    .AddChildren((aetpControl)(node.TreeList.GetDataRecordByNode(node) as aetpControlTreeListWrapper).objectCore.Clone());
        }
        public override bool NeedsFullIteration { get { return true; } }
    }
}
