﻿namespace anketa
{
    partial class ConstructorMainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bbiAddPage = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddSimpleEditor = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGenerateCode = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeserialize = new DevExpress.XtraBars.BarButtonItem();
            this.rpMain = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.propertyGrid = new System.Windows.Forms.PropertyGrid();
            this.tlControls = new DevExpress.XtraTreeList.TreeList();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colCaption = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.meCode = new DevExpress.XtraEditors.MemoEdit();
            this.bbiRemoveItem = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlControls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonText = null;
            // 
            // 
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.ExpandCollapseItem.Name = "";
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.bbiAddPage,
            this.bbiAddSimpleEditor,
            this.bbiGenerateCode,
            this.bbiDeserialize,
            this.bbiRemoveItem});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 6;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpMain});
            this.ribbon.Size = new System.Drawing.Size(1136, 147);
            // 
            // bbiAddPage
            // 
            this.bbiAddPage.Caption = "addPage";
            this.bbiAddPage.Id = 1;
            this.bbiAddPage.Name = "bbiAddPage";
            this.bbiAddPage.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddPage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPage_ItemClick);
            // 
            // bbiAddSimpleEditor
            // 
            this.bbiAddSimpleEditor.Caption = "Add Simple Editor";
            this.bbiAddSimpleEditor.Id = 2;
            this.bbiAddSimpleEditor.Name = "bbiAddSimpleEditor";
            this.bbiAddSimpleEditor.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddSimpleEditor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddSimpleEditor_ItemClick);
            // 
            // bbiGenerateCode
            // 
            this.bbiGenerateCode.Caption = "Generate Code";
            this.bbiGenerateCode.Id = 3;
            this.bbiGenerateCode.Name = "bbiGenerateCode";
            this.bbiGenerateCode.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiGenerateCode.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGenerateCode_ItemClick);
            // 
            // bbiDeserialize
            // 
            this.bbiDeserialize.Caption = "Deserialize";
            this.bbiDeserialize.Id = 4;
            this.bbiDeserialize.Name = "bbiDeserialize";
            this.bbiDeserialize.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiDeserialize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeserialize_ItemClick);
            // 
            // rpMain
            // 
            this.rpMain.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.rpMain.Name = "rpMain";
            this.rpMain.Text = "Главная";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiAddPage);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiAddSimpleEditor);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiRemoveItem);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiGenerateCode);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiDeserialize);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Дейсвия";
            // 
            // propertyGrid
            // 
            this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Right;
            this.propertyGrid.Location = new System.Drawing.Point(868, 147);
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.Size = new System.Drawing.Size(268, 365);
            this.propertyGrid.TabIndex = 6;
            this.propertyGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid_PropertyValueChanged);
            // 
            // tlControls
            // 
            this.tlControls.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName,
            this.colCaption});
            this.tlControls.Dock = System.Windows.Forms.DockStyle.Left;
            this.tlControls.Location = new System.Drawing.Point(0, 147);
            this.tlControls.Name = "tlControls";
            this.tlControls.Size = new System.Drawing.Size(265, 365);
            this.tlControls.TabIndex = 7;
            this.tlControls.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.tlControls_FocusedNodeChanged);
            // 
            // colName
            // 
            this.colName.Caption = "Имя";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colCaption
            // 
            this.colCaption.Caption = "Заголовок";
            this.colCaption.FieldName = "Caption";
            this.colCaption.Name = "colCaption";
            this.colCaption.Visible = true;
            this.colCaption.VisibleIndex = 1;
            // 
            // meCode
            // 
            this.meCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meCode.Location = new System.Drawing.Point(265, 147);
            this.meCode.MenuManager = this.ribbon;
            this.meCode.Name = "meCode";
            this.meCode.Size = new System.Drawing.Size(603, 365);
            this.meCode.TabIndex = 9;
            // 
            // bbiRemoveItem
            // 
            this.bbiRemoveItem.Caption = "Remove Item";
            this.bbiRemoveItem.Id = 5;
            this.bbiRemoveItem.Name = "bbiRemoveItem";
            this.bbiRemoveItem.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRemoveItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRemoveItem_ItemClick);
            // 
            // ConstructorMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1136, 512);
            this.Controls.Add(this.meCode);
            this.Controls.Add(this.tlControls);
            this.Controls.Add(this.propertyGrid);
            this.Controls.Add(this.ribbon);
            this.Name = "ConstructorMainForm";
            this.Ribbon = this.ribbon;
            this.Text = "Конструктор";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ConstructorMainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlControls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpMain;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private System.Windows.Forms.PropertyGrid propertyGrid;
        private DevExpress.XtraTreeList.TreeList tlControls;
        private DevExpress.XtraBars.BarButtonItem bbiAddPage;
        private DevExpress.XtraBars.BarButtonItem bbiAddSimpleEditor;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCaption;
        private DevExpress.XtraBars.BarButtonItem bbiGenerateCode;
        private DevExpress.XtraEditors.MemoEdit meCode;
        private DevExpress.XtraBars.BarButtonItem bbiDeserialize;
        private DevExpress.XtraBars.BarButtonItem bbiRemoveItem;

    }
}

