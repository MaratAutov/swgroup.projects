﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using aetp.Common;
using System.IO;
using aetp.Common.ControlModel;
using System.Xml;
using aetp.Common.Serializer;
using System.Xml.Serialization;

namespace UpdaterGenerator
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnOpenDirectory_Click(object sender, EventArgs e)
        {
            string startupPath = Application.StartupPath;
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                dialog.Description = "Open a folder which contains the xml output";
                dialog.ShowNewFolderButton = false;
                dialog.RootFolder = Environment.SpecialFolder.MyComputer;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    tePath.Text = dialog.SelectedPath;
                }
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            ModelConfig config = new ModelConfig();
            List<ModelConfig.ModelInfo> modellist = new List<ModelConfig.ModelInfo>();
            foreach (string filepath in Directory.GetFiles(tePath.Text, "*.aff"))
            {
                try
                {
                    Report report;

                    using (XmlTextReader reportReader = new XmlTextReader(filepath))
                    {
                        report = new FormSerializer().Deserialize(reportReader, true);
                    }
                    if (report != null)
                    {
                        ModelConfig.ModelInfo item = new ModelConfig.ModelInfo();
                        item.Name = report.Name;
                        item.Path = Path.GetFileName(filepath);
                        item.Version = report.SerialaizerConfig.appVersion;
                        item.Description = report.SerialaizerConfig.appDescription;
                        item.Tag = report.SerializingTag;
                        item.GenerateHash(filepath);
                        modellist.Add(item);
                    }

                }
                catch
                {
                    //игнорируем поврежденные, некорректные и старые модели
                }
            }
            string filename = Path.Combine(tePath.Text, "ModelConfig.xml");
            config.ModelsPath = Path.GetDirectoryName(filename);
            config.ListModelInfo = modellist;
            FileStream fs = new FileStream(filename, FileMode.Create);
            XmlSerializer serializer = new XmlSerializer(typeof(ModelConfig));
            serializer.Serialize(fs, config);
            fs.Close();

            File.WriteAllText(Path.Combine(tePath.Text, "version.txt"), teVersion.Text);
        }

        
    }
}
