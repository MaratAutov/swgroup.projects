﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
//using aetp.DocumentsSubmitter.FFMS;
using System.Windows.Forms;

namespace aetp.DocumentsSubmitter
{
    public class Uploader
    {
        public string SrcFileName { get; set; }
        public string DstNotificationFileName { get; set; }
        public string ReportName { get; set; }
        public Exception Error { get; set; }
        public string Status
        {
            get;
            private set;
        }

        public UploadSessionController sessionController { get; set; }
                
        public void Execute()
        {
            try 
            {
                using (var cl = new ReportService.ReportServiceSoapClient("ReportServiceSoap", DSGlobal.Instance.Config.Transport_FFMS_ReportsService)) 
                {
                    sessionController.SendingState = "Авторизация";
                    string retval = cl.LogIn(DSGlobal.Instance.Config.Login, Cripto.Decrypt_ic(DSGlobal.Instance.Config.Password));
                    
                    if (String.IsNullOrEmpty(retval)) 
                    {
                        sessionController.SendingState = "Отправка документа";
                        var buffer = File.ReadAllBytes(SrcFileName);
                        // SrcFileName должно иметь расширение .zip
                        string fileName = SrcFileName;
                        if (Path.GetExtension(SrcFileName) == ".tmp")
                            fileName = ReportName + ".zip";

                        string res = cl.Submit(fileName, buffer);
                        int num = 0;
                        if (!int.TryParse(res, out num)) 
                        {
                            sessionController.SendingState = "Отправка документа - Отмена";
                            Status = res;
                            return;
                        }
                    
                        var notif = cl.Notification(num);
                    
                        sessionController.SendingState = "Отправка документа - Успешно";
                        Status = notif.Status;

                        File.WriteAllBytes(
                            DstNotificationFileName,
                            notif.Notification
                        );
                    }
                    else 
                    {
                        MessageBox.Show(retval);
                    }
                }
            } 
            catch (Exception exc) 
            {
                throw new Exception("Ошибка подключения к указанному сервису \n" + exc.Message);
            }

        }
    }
}
