﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Security.Permissions;
using System.Net;
using System.IO;
using aetp.Utils;
using System.Threading;

namespace aetp.DocumentsSubmitter.Views
{
    public partial class lkList : XtraUserControl
    {
        public lkList()
        {
            InitializeComponent();
            InitYearsList();
            pagination1.PageCount = 10;
            pagination1.PageChanged += new PaginationEventHandler(pagination1_PageChanged);
        }

        void InitYearsList()
        {
            cbeYears.SelectedIndexChanged -= cbeYears_SelectedIndexChanged;
            int currYear = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                cbeYears.Properties.Items.Add(currYear - i);
            }
            cbeYears.SelectedItem = currYear;
            cbeYears.SelectedIndexChanged += cbeYears_SelectedIndexChanged;
        }

        int CurrentYear
        {
            get
            {
                return (int)cbeYears.SelectedItem;
            }
        }

        void pagination1_PageChanged(object sender, PaginationEventArgs e)
        {
            RefreshList();
        }

        /// <summary>
        /// Асинхронный метод получения данных
        /// </summary>
        ReportService.PackageList dataList;
        string retval;
        void refreshAction()
        {
            Thread.Sleep(5000);
            try
            {
                using (var cl = new ReportService.ReportServiceSoapClient("ReportServiceSoap", DSGlobal.Instance.Config.Transport_FFMS_ReportsService))
                {
                    retval = cl.LogIn(
                        DSGlobal.Instance.Config.Login,
                        Cripto.Decrypt_ic(DSGlobal.Instance.Config.Password)
                    );

                    if (String.IsNullOrEmpty(retval))
                    {
                        dataList = cl.GetOutgoing(
                            CurrentYear,
                            pagination1.CurrentPage,
                            pagination1.PageCount,
                            null,
                            null,
                            null
                        );

                        output();
                    }
                    else
                    {
                        dataList = null;
                        output();
                    }
                }
            }
            catch
            {
                dataList = null;
                retval = "Проверьте настройки подключения";
                output();
            }
        }

        /// <summary>
        /// Потокобезопасный вывод результатов
        /// </summary>
        void output()
        {
            if (this.InvokeRequired)
            {
                Action outputdelegate = output;
                this.BeginInvoke(outputdelegate);
            }
            else
            {
                if (dataList == null)
                {
                    pagination1.TotalPage = 1;
                    pagination1.TotalCount = 0;
                    gcList.DataSource = null;
                    MessageBox.Show(retval);
                }
                else
                {
                    pagination1.TotalPage = (int)(dataList.TotalCount / pagination1.PageCount) + 1;
                    pagination1.TotalCount = dataList.TotalCount;
                    gcList.DataSource = ListToDataTable(dataList.Packages);
                }
                pagination1.RefreshButton();
                this.Cursor = Cursors.Default;
            }
        }

        public void RefreshList()
        {
            this.Cursor = Cursors.WaitCursor;
            Action refreshdelegate = refreshAction;
            refreshdelegate.BeginInvoke(null, null);
            
        }

        public void RefreshList(int page)
        {
            pagination1.CurrentPage = page;
            RefreshList();
        }

        DataTable ListToDataTable(ReportService.PackageFullInfo[] info)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(string));
            dt.Columns.Add("CreateDate", typeof(string));
            dt.Columns.Add("PackageName", typeof(string));
            dt.Columns.Add("OutcomingNumber", typeof(string));
            dt.Columns.Add("OutcomingDate", typeof(string));
            dt.Columns.Add("IncomingNumber", typeof(string));
            dt.Columns.Add("IncomingDate", typeof(string));
            dt.Columns.Add("KiasStatus", typeof(string));
            dt.Columns.Add("InitialStatus", typeof(string));

            foreach (var n in info)
            {
                DataRow row = dt.NewRow();
                row["ID"] = n.ID;
                row["CreateDate"] = n.UploadTime;
                row["PackageName"] = n.Name;
                row["OutcomingNumber"] = n.IncomingNumber;
                row["OutcomingDate"] = n.IncomingDate;
                row["IncomingNumber"] = n.SedNumber;
                row["IncomingDate"] = n.SedDate;
                row["KiasStatus"] = n.CurrentStatusInfo;
                row["InitialStatus"] = n.InitialStatusInfo;
                dt.Rows.Add(row);
            }
            return dt;
        }

        private void linkKiasStatus_Click(object sender, EventArgs e)
        {
            titlePacket title = getFocusedNotificationTitle();
            Download(title.ID, title.Name + ".уведомление", true);
        }
        
        private void linkPacket_Click(object sender, EventArgs e)
        {
            titlePacket title = getFocusedNotificationTitle();
            Download(title.ID, title.Name);
        }

        titlePacket getFocusedNotificationTitle()
        {
            DataRow dr = gv.GetFocusedDataRow();
            if (dr != null)
            {
                return new titlePacket()
                {
                    ID = dr["ID"].ToString(),
                    Name = dr["PackageName"].ToString()
                };
            }
            else return null;
        }

        void Download(string postfix, string filename, bool notification=false)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "ZIP архив (*.zip)|*.zip";
            sfd.FileName = filename;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (var cl = new ReportService.ReportServiceSoapClient("ReportServiceSoap", DSGlobal.Instance.Config.Transport_FFMS_ReportsService))
                    {
                        byte[] data = null;
                        if (notification) {
                            data = cl.Notification(int.Parse(postfix)).Notification;
                        } else {
                            data = cl.Download(int.Parse(postfix));
                        }
                        File.WriteAllBytes(sfd.FileName, data);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private class titlePacket
        {
            public string ID;
            public string Name;
        }

        private void cbeYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            pagination1.CurrentPage = 1;
            RefreshList();
        }
    }
}
