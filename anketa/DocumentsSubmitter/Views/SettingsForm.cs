﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using aetp.Utils;

namespace aetp.DocumentsSubmitter.Views
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                DSGlobal.Instance.Config.Transport_FFMS_ReportsService = "http://lk.fcsm.ru:8080/ReportService.asmx";
                DSGlobal.Instance.Config.Login = teLogin.Text;
                DSGlobal.Instance.Config.Password = Cripto.Encrypt_ic(tePassword.Text.Trim());
                DSGlobal.Instance.Config.Save();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHelper.Show(ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            teServiceAddress.Text = DSGlobal.Instance.Config.Transport_FFMS_ReportsService;
            teLogin.Text = DSGlobal.Instance.Config.Login;

            if (!String.IsNullOrEmpty(DSGlobal.Instance.Config.Password))

                tePassword.Text = Cripto.Decrypt_ic(DSGlobal.Instance.Config.Password);
            else
                tePassword.Text = DSGlobal.Instance.Config.Password;
            
        }
    }
}
