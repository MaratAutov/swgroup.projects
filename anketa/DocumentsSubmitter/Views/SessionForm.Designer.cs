﻿namespace aetp.DocumentsSubmitter.Views
{
    partial class SessionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionForm));
            this.sessionList1 = new aetp.DocumentsSubmitter.Views.SessionList();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bbiLoad = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClear = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReSend = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSettings = new DevExpress.XtraBars.BarButtonItem();
            this.rpMain = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgAction = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgSettings = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpLkList = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgActionLk = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgSettings1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.tcMain = new DevExpress.XtraTab.XtraTabControl();
            this.tpUpload = new DevExpress.XtraTab.XtraTabPage();
            this.tpList = new DevExpress.XtraTab.XtraTabPage();
            this.lkList1 = new aetp.DocumentsSubmitter.Views.lkList();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcMain)).BeginInit();
            this.tcMain.SuspendLayout();
            this.tpUpload.SuspendLayout();
            this.tpList.SuspendLayout();
            this.SuspendLayout();
            // 
            // sessionList1
            // 
            this.sessionList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sessionList1.HasErrors = false;
            this.sessionList1.HasInProcess = false;
            this.sessionList1.HasUnsaved = false;
            this.sessionList1.Location = new System.Drawing.Point(0, 0);
            this.sessionList1.Name = "sessionList1";
            this.sessionList1.Size = new System.Drawing.Size(780, 302);
            this.sessionList1.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonText = null;
            this.ribbonControl1.ApplicationIcon = global::aetp.DocumentsSubmitter.Properties.Resources.anketa;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiLoad,
            this.bbiSave,
            this.bbiClear,
            this.bbiReSend,
            this.bbiRefresh,
            this.bbiSettings});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 8;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpMain,
            this.rpLkList});
            this.ribbonControl1.Size = new System.Drawing.Size(786, 144);
            // 
            // bbiLoad
            // 
            this.bbiLoad.Caption = "Загрузить";
            this.bbiLoad.Glyph = global::aetp.DocumentsSubmitter.Properties.Resources._32_открыть;
            this.bbiLoad.Id = 1;
            this.bbiLoad.Name = "bbiLoad";
            this.bbiLoad.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiLoad.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoad_ItemClick);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить уведомления";
            this.bbiSave.Glyph = global::aetp.DocumentsSubmitter.Properties.Resources._32_сохранить;
            this.bbiSave.Id = 2;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // bbiClear
            // 
            this.bbiClear.Caption = "Очистить";
            this.bbiClear.Glyph = global::aetp.DocumentsSubmitter.Properties.Resources._32_удалить;
            this.bbiClear.Id = 3;
            this.bbiClear.Name = "bbiClear";
            this.bbiClear.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClear_ItemClick);
            // 
            // bbiReSend
            // 
            this.bbiReSend.Caption = "Повторить передачу";
            this.bbiReSend.Glyph = global::aetp.DocumentsSubmitter.Properties.Resources.sign_out;
            this.bbiReSend.Id = 4;
            this.bbiReSend.Name = "bbiReSend";
            this.bbiReSend.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiReSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReSend_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Glyph = global::aetp.DocumentsSubmitter.Properties.Resources.gnome_view_refresh;
            this.bbiRefresh.Id = 5;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiSettings
            // 
            this.bbiSettings.Caption = "Настройки";
            this.bbiSettings.Glyph = global::aetp.DocumentsSubmitter.Properties.Resources.settings;
            this.bbiSettings.Id = 7;
            this.bbiSettings.Name = "bbiSettings";
            this.bbiSettings.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSettings_ItemClick);
            // 
            // rpMain
            // 
            this.rpMain.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgAction,
            this.rpgSettings});
            this.rpMain.Name = "rpMain";
            this.rpMain.Text = "Главная";
            // 
            // rpgAction
            // 
            this.rpgAction.ItemLinks.Add(this.bbiLoad);
            this.rpgAction.ItemLinks.Add(this.bbiSave);
            this.rpgAction.ItemLinks.Add(this.bbiClear);
            this.rpgAction.ItemLinks.Add(this.bbiReSend);
            this.rpgAction.Name = "rpgAction";
            this.rpgAction.Text = "Действия";
            // 
            // rpgSettings
            // 
            this.rpgSettings.ItemLinks.Add(this.bbiSettings);
            this.rpgSettings.Name = "rpgSettings";
            this.rpgSettings.Text = "Настройки";
            // 
            // rpLkList
            // 
            this.rpLkList.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgActionLk,
            this.rpgSettings1});
            this.rpLkList.Name = "rpLkList";
            this.rpLkList.Text = "Главная";
            this.rpLkList.Visible = false;
            // 
            // rpgActionLk
            // 
            this.rpgActionLk.ItemLinks.Add(this.bbiRefresh);
            this.rpgActionLk.Name = "rpgActionLk";
            this.rpgActionLk.Text = "Действия";
            // 
            // rpgSettings1
            // 
            this.rpgSettings1.ItemLinks.Add(this.bbiSettings);
            this.rpgSettings1.Name = "rpgSettings1";
            this.rpgSettings1.Text = "Настройки";
            // 
            // tcMain
            // 
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Location = new System.Drawing.Point(0, 144);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedTabPage = this.tpUpload;
            this.tcMain.Size = new System.Drawing.Size(786, 330);
            this.tcMain.TabIndex = 2;
            this.tcMain.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpUpload,
            this.tpList});
            this.tcMain.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tcMain_SelectedPageChanged);
            // 
            // tpUpload
            // 
            this.tpUpload.Controls.Add(this.sessionList1);
            this.tpUpload.Name = "tpUpload";
            this.tpUpload.Size = new System.Drawing.Size(780, 302);
            this.tpUpload.Text = "Загрузка";
            // 
            // tpList
            // 
            this.tpList.Controls.Add(this.lkList1);
            this.tpList.Name = "tpList";
            this.tpList.Size = new System.Drawing.Size(780, 302);
            this.tpList.Text = "Личный кабинет";
            // 
            // lkList1
            // 
            this.lkList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lkList1.Location = new System.Drawing.Point(0, 0);
            this.lkList1.Name = "lkList1";
            this.lkList1.Size = new System.Drawing.Size(780, 302);
            this.lkList1.TabIndex = 0;
            // 
            // SessionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 474);
            this.Controls.Add(this.tcMain);
            this.Controls.Add(this.ribbonControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SessionForm";
            this.Ribbon = this.ribbonControl1;
            this.Text = "Отправка отчетности";
            this.Shown += new System.EventHandler(this.SessionForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcMain)).EndInit();
            this.tcMain.ResumeLayout(false);
            this.tpUpload.ResumeLayout(false);
            this.tpList.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private SessionList sessionList1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpMain;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgAction;
        private DevExpress.XtraBars.BarButtonItem bbiLoad;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiClear;
        private DevExpress.XtraBars.BarButtonItem bbiReSend;
        private DevExpress.XtraTab.XtraTabControl tcMain;
        private DevExpress.XtraTab.XtraTabPage tpUpload;
        private DevExpress.XtraTab.XtraTabPage tpList;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpLkList;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgActionLk;
        private lkList lkList1;
        private DevExpress.XtraBars.BarButtonItem bbiSettings;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSettings;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSettings1;
    }
}