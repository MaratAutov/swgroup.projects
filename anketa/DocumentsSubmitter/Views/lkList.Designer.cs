﻿namespace aetp.DocumentsSubmitter.Views
{
    partial class lkList
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcList = new DevExpress.XtraGrid.GridControl();
            this.gv = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCreateDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackageName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.linkPacket = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colOutcomingNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutcomingDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncomingNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncomingDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiasStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.linkKiasStatus = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colInitialStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.linkInitialStatus = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.pagination1 = new aetp.DocumentsSubmitter.Views.Pagination();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cbeYears = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkPacket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkKiasStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkInitialStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbeYears.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcList
            // 
            this.gcList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcList.Location = new System.Drawing.Point(0, 29);
            this.gcList.MainView = this.gv;
            this.gcList.Name = "gcList";
            this.gcList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.linkKiasStatus,
            this.linkInitialStatus,
            this.linkPacket});
            this.gcList.Size = new System.Drawing.Size(715, 256);
            this.gcList.TabIndex = 0;
            this.gcList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv});
            // 
            // gv
            // 
            this.gv.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCreateDate,
            this.colPackageName,
            this.colOutcomingNumber,
            this.colOutcomingDate,
            this.colIncomingNumber,
            this.colIncomingDate,
            this.colKiasStatus,
            this.colInitialStatus});
            this.gv.GridControl = this.gcList;
            this.gv.Name = "gv";
            this.gv.OptionsCustomization.AllowFilter = false;
            this.gv.OptionsCustomization.AllowSort = false;
            this.gv.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gv.OptionsView.ShowGroupPanel = false;
            // 
            // colCreateDate
            // 
            this.colCreateDate.Caption = "Дата и время приема документа";
            this.colCreateDate.FieldName = "CreateDate";
            this.colCreateDate.Name = "colCreateDate";
            this.colCreateDate.OptionsColumn.AllowEdit = false;
            this.colCreateDate.Visible = true;
            this.colCreateDate.VisibleIndex = 0;
            // 
            // colPackageName
            // 
            this.colPackageName.Caption = "Наименование файла пакета";
            this.colPackageName.ColumnEdit = this.linkPacket;
            this.colPackageName.FieldName = "PackageName";
            this.colPackageName.Name = "colPackageName";
            this.colPackageName.Visible = true;
            this.colPackageName.VisibleIndex = 1;
            // 
            // linkPacket
            // 
            this.linkPacket.AutoHeight = false;
            this.linkPacket.Name = "linkPacket";
            this.linkPacket.Click += new System.EventHandler(this.linkPacket_Click);
            // 
            // colOutcomingNumber
            // 
            this.colOutcomingNumber.Caption = "Исходящий номер участника";
            this.colOutcomingNumber.FieldName = "OutcomingNumber";
            this.colOutcomingNumber.Name = "colOutcomingNumber";
            this.colOutcomingNumber.OptionsColumn.AllowEdit = false;
            this.colOutcomingNumber.Visible = true;
            this.colOutcomingNumber.VisibleIndex = 2;
            // 
            // colOutcomingDate
            // 
            this.colOutcomingDate.Caption = "Дата создания документа";
            this.colOutcomingDate.FieldName = "OutcomingDate";
            this.colOutcomingDate.Name = "colOutcomingDate";
            this.colOutcomingDate.OptionsColumn.AllowEdit = false;
            this.colOutcomingDate.Visible = true;
            this.colOutcomingDate.VisibleIndex = 3;
            // 
            // colIncomingNumber
            // 
            this.colIncomingNumber.Caption = "Входящий номер ФСФР";
            this.colIncomingNumber.FieldName = "IncomingNumber";
            this.colIncomingNumber.Name = "colIncomingNumber";
            this.colIncomingNumber.OptionsColumn.AllowEdit = false;
            this.colIncomingNumber.Visible = true;
            this.colIncomingNumber.VisibleIndex = 4;
            // 
            // colIncomingDate
            // 
            this.colIncomingDate.Caption = "Дата регистрации ФСФР";
            this.colIncomingDate.FieldName = "IncomingDate";
            this.colIncomingDate.Name = "colIncomingDate";
            this.colIncomingDate.OptionsColumn.AllowEdit = false;
            this.colIncomingDate.Visible = true;
            this.colIncomingDate.VisibleIndex = 5;
            // 
            // colKiasStatus
            // 
            this.colKiasStatus.Caption = "Статус";
            this.colKiasStatus.ColumnEdit = this.linkKiasStatus;
            this.colKiasStatus.FieldName = "KiasStatus";
            this.colKiasStatus.Name = "colKiasStatus";
            this.colKiasStatus.Visible = true;
            this.colKiasStatus.VisibleIndex = 6;
            // 
            // linkKiasStatus
            // 
            this.linkKiasStatus.AutoHeight = false;
            this.linkKiasStatus.Name = "linkKiasStatus";
            this.linkKiasStatus.Click += new System.EventHandler(this.linkKiasStatus_Click);
            // 
            // colInitialStatus
            // 
            this.colInitialStatus.Caption = "Первое уведомление";
            this.colInitialStatus.FieldName = "InitialStatus";
            this.colInitialStatus.OptionsColumn.AllowEdit = false;
            this.colInitialStatus.Name = "colInitialStatus";
            this.colInitialStatus.Visible = true;
            this.colInitialStatus.VisibleIndex = 7;
            // 
            // linkInitialStatus
            // 
            this.linkInitialStatus.AutoHeight = false;
            this.linkInitialStatus.Name = "linkInitialStatus";
            // 
            // pagination1
            // 
            this.pagination1.CurrentPage = 1;
            this.pagination1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagination1.Location = new System.Drawing.Point(0, 285);
            this.pagination1.Name = "pagination1";
            this.pagination1.PageCount = 0;
            this.pagination1.Size = new System.Drawing.Size(715, 34);
            this.pagination1.TabIndex = 1;
            this.pagination1.TotalCount = 0;
            this.pagination1.TotalPage = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.cbeYears);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(715, 29);
            this.panelControl1.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(21, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(23, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Год:";
            // 
            // cbeYears
            // 
            this.cbeYears.Location = new System.Drawing.Point(99, 5);
            this.cbeYears.Name = "cbeYears";
            this.cbeYears.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeYears.Size = new System.Drawing.Size(100, 20);
            this.cbeYears.TabIndex = 0;
            this.cbeYears.SelectedIndexChanged += new System.EventHandler(this.cbeYears_SelectedIndexChanged);
            // 
            // lkList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcList);
            this.Controls.Add(this.pagination1);
            this.Controls.Add(this.panelControl1);
            this.Name = "lkList";
            this.Size = new System.Drawing.Size(715, 319);
            ((System.ComponentModel.ISupportInitialize)(this.gcList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkPacket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkKiasStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkInitialStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbeYears.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcList;
        private DevExpress.XtraGrid.Views.Grid.GridView gv;
        private DevExpress.XtraGrid.Columns.GridColumn colPackageName;
        private Pagination pagination1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOutcomingNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOutcomingDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit linkPacket;
        private DevExpress.XtraGrid.Columns.GridColumn colIncomingNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colIncomingDate;
        private DevExpress.XtraGrid.Columns.GridColumn colKiasStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit linkKiasStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colInitialStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit linkInitialStatus;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbeYears;
    }
}
