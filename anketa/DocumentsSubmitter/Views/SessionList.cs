﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace aetp.DocumentsSubmitter.Views
{
    public partial class SessionList : XtraUserControl
    {
        public bool HasUnsaved { get; set; }
        public bool HasInProcess { get; set; }
        public bool HasErrors { get; set; }
        public int SessionsListCount
        {
            get
            {
                return _sessionsList.Count;
            }
        }

        public event EventHandler StateUpdated;
        protected void RaiseStateUpdated()
        {
            if (StateUpdated != null)
                StateUpdated(this, null);
        }

        public List<UploadSessionController> Items
        {
            get
            {
                return _sessionsList;
            }
        }

        public SessionList()
		{
			InitializeComponent();
			UpdateState();
		}

		public void AddSession(UploadSessionController session)
		{
			listViewMain.Items.Add(session.Lvi);
			_sessionsList.Add(session);
			VisibleSessionsCount++;
			UpdateState();
		}
		List<UploadSessionController> _sessionsList = new List<UploadSessionController>();

		private int _visibleSessionsCount = 0;

		private int VisibleSessionsCount
		{
			get { return _visibleSessionsCount; }
			set
			{
				_visibleSessionsCount = value;
				//labelDragDropHint.Visible = _visibleSessionsCount == 0;
			}
		}

		#region DnD

		private void UploadForm_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				string[] filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));				
				{
					e.Effect = DragDropEffects.Copy;
					return;
				}
			}
			e.Effect = DragDropEffects.None;
		}

		private void UploadForm_DragDrop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				var filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
				foreach (var s in filePaths)
				{
					Action<string, string> @delegate = StartUpload;
					BeginInvoke(@delegate, s);
				}
			}
		}

		#endregion

		#region Ulpoad

		public void StartUpload(string obj, string reportName = null)
		{
            var uploader = new Uploader
            {
                DstNotificationFileName = Path.GetTempFileName(),
                ReportName = reportName ?? Path.GetFileNameWithoutExtension(obj),
                SrcFileName = obj
            };
			var sessionControl = new UploadSessionController
			{
                Uploader = uploader,
				UploadState = UploadSessionController.State.InProcess
			};
            uploader.sessionController = sessionControl;
			AddSession(sessionControl);
			ScheduleJob(sessionControl);
		}

		public void ScheduleJob(UploadSessionController uploadSessionController)
		{
			uploadSessionController.UploadState = UploadSessionController.State.InProcess;
			var w = new BackgroundWorker();
            w.DoWork += BackgroundUpload;
            w.RunWorkerCompleted += (sender, args) =>
			                        	{
			                        		uploadSessionController.HasUnsavedData = true;
			                        		uploadSessionController.UploadState = args.Cancelled
			                        		                             	? UploadSessionController.State.Failed
			                        		                             	: UploadSessionController.State.Succeeded;
			                        		UpdateState();
			                        	};
			w.WorkerReportsProgress = true;

			w.RunWorkerAsync(uploadSessionController);
			UpdateState();
		}
        
		private void BackgroundUpload(object sender, DoWorkEventArgs args)
		{
			BackgroundWorker w = (BackgroundWorker)sender;
			var sc = (UploadSessionController)args.Argument;
			try
			{
                sc.SessionList = this;
				sc.Uploader.Execute();
			}
			catch (Exception e)
			{
				w.ReportProgress(100, e.Message);
				args.Cancel = true;
				sc.Uploader.Error = e;
			}
		}

        public void RemoveSession(UploadSessionController c)
        {
            listViewMain.Items.Remove(c.Lvi);
            _sessionsList.Remove(c);
        }

		#endregion


		public void UpdateState()
		{
			bool hasUnsaved = false;
			bool hasInProcess = false;
			bool hasErrors = false;
			foreach (var c in _sessionsList)
			{
				hasUnsaved |= c.HasUnsavedData;
				hasInProcess |= c.UploadState == UploadSessionController.State.InProcess;
				hasErrors |= c.UploadState == UploadSessionController.State.Failed;
			}

            HasUnsaved = hasUnsaved;
            HasInProcess = hasInProcess;
            HasErrors = hasErrors;
			pictureBox1.Visible = toolStripStatusLabel1.Visible = hasInProcess;
            
            RaiseStateUpdated();
		}

		
    }
}
