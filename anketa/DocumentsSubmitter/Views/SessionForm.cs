﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Ribbon;
using aetp.ElectroSignature;

namespace aetp.DocumentsSubmitter.Views
{
    public partial class SessionForm : RibbonForm
    {
        
        byte[] report_body = null;
        public SessionForm()
        {
            InitializeComponent();
            UpdateControls();
            sessionList1.StateUpdated += new EventHandler(sessionList1_StateUpdated);
        }

        public SessionForm(byte[] report_body)
        {
            InitializeComponent();
            UpdateControls();
            this.report_body = report_body;
            sessionList1.StateUpdated += new EventHandler(sessionList1_StateUpdated);
        }

        void sessionList1_StateUpdated(object sender, EventArgs e)
        {
            UpdateControls();
        }
        
        void UpdateControls()
        {
            bbiSave.Enabled = sessionList1.HasUnsaved;
            bbiClear.Enabled = sessionList1.SessionsListCount > 0;
            bbiReSend.Enabled = sessionList1.HasErrors;
        }
                
        private void bbiLoad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            openFileDialog1.Multiselect = true;
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {

                foreach (var fileName in openFileDialog1.FileNames)
                {
                    sessionList1.StartUpload(fileName);
                }
            }
            
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (folderBrowserDialog.ShowDialog(this) == DialogResult.OK)
            {
                foreach (var c in sessionList1.Items.ToArray())
                {
                    if (c.HasUnsavedData)
                    {
                        c.Save(Path.Combine(folderBrowserDialog.SelectedPath,
                                            Path.GetFileNameWithoutExtension(c.Uploader.SrcFileName) + "_Уведомление.zip"));
                        sessionList1.RemoveSession(c);
                    }
                }
            }
            sessionList1.UpdateState();
        }

        private void bbiClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            foreach (var c in sessionList1.Items.ToArray())
            {
                if (c.UploadState != UploadSessionController.State.InProcess)
                {
                    sessionList1.RemoveSession(c);
                }
            }
            sessionList1.UpdateState();
        }

        private void bbiReSend_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            foreach (var c in sessionList1.Items)
            {
                if (c.UploadState == UploadSessionController.State.Failed)
                {
                    sessionList1.ScheduleJob(c);
                }
            }
        }        

        private void SessionForm_Shown(object sender, EventArgs e)
        {
            //если есть пакет на загрузку
            if (report_body != null)
            {
                ElectroSignatureEngine.ParentForm = this;
                PacketOptionsForm optionView = new PacketOptionsForm();
                if (optionView.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    ElectroSignatureEngine ecp = new ElectroSignatureEngine();
                    string temp = Path.GetTempFileName();
                    byte[] tempFile = ecp.Sign(optionView.EditValue.FileName, report_body);
                    if (tempFile != null)
                    {
                        File.WriteAllBytes(temp, tempFile);
                        sessionList1.StartUpload(temp, optionView.EditValue.FileName);
                    }
                    else
                    {
                        MessageBox.Show("Отправка пакета была отменена", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                sessionList1.UpdateState();
            }
        }

        private void tcMain_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page == tpUpload)
            {
                rpMain.Visible = true;
                rpLkList.Visible = false;
            }
            else
            {
                rpMain.Visible = false;
                rpLkList.Visible = true;
                lkList1.RefreshList();
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            lkList1.RefreshList(1);
        }

        private void bbiSettings_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SettingsForm settings = new SettingsForm();
            settings.ShowDialog(this);
        }
    }
}
