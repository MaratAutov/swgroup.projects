﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Text.RegularExpressions;

namespace aetp.DocumentsSubmitter.Views
{
    public partial class PacketOptionsForm : XtraForm
    {
        public PacketOption EditValue { get; set; }
        
        public PacketOptionsForm()
        {
            InitializeComponent();
            EditValue = new PacketOption();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
             if (!dxErrorProvider1.HasErrors)
                   this.Close();
            
        }

        private void teName_TextChanged(object sender, EventArgs e)
        {
            this.btnOk.Enabled = (bool)(teName.Text.Trim().Length > 0);
        }
                
        private void PacketOptionsForm_Load(object sender, EventArgs e)
        {
            this.teName.TextChanged += new EventHandler(teName_TextChanged);
            // Specify the type of records stored in the BindingSource.
            bindingSource1.DataSource = typeof(PacketOption);
            // Create an empty MyRecord and add it to the BindingSource.
            
            bindingSource1.Add(EditValue);
            // Bind the text editors to the MyRecord.FirstName and MyRecord.LastName properties.
            teName.DataBindings.Add(new Binding("EditValue", this.bindingSource1,
              "FileName", true, DataSourceUpdateMode.OnPropertyChanged));
            // Bind the DXErrorProvider to the data source.
            dxErrorProvider1.DataSource = bindingSource1;
            // Specify the container of controls (textEdit1 and textEdit2) 
            // which are monitored for errors.
            dxErrorProvider1.ContainerControl = this;
        }
    }

   
    public class PacketOption: IDXDataErrorInfo 
    {
        public string FileName { get; set; }

        public void GetPropertyError(string propertyName, ErrorInfo info)
        {
            if (propertyName == "FileName")
            {
                Regex rgx = new Regex(@"^[a-zA-Zа-яА-Я]{1}[a-zA-Zа-яА-Я0-9\-_]{2,}$", RegexOptions.IgnoreCase);
                if (String.IsNullOrEmpty(FileName) || !rgx.IsMatch(FileName))
                {
                    info.ErrorText = "Неверно заданное имя пакета";
                }
            }
        }
        
        // IDXDataErrorInfo.GetError method
        public void GetError(ErrorInfo info) { }  
    }
}
