﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace aetp.DocumentsSubmitter.Views
{
    public partial class Pagination : XtraUserControl
    {
        int _totalPage;
        public int TotalPage
        {
            get
            {
                return _totalPage;
            }
            set
            {
                _totalPage = value;
            }
        }
        
        int _totalCount;
        public int TotalCount 
        {
            get
            {
                return _totalCount;
            }
            set
            {
                _totalCount = value;

                lblTotalCount.Text = value.ToString();

                //if (PageCount == 0)
                //    TotalPage = 1;
                //else
                //    TotalPage = (int)(TotalCount / PageCount) + 1;
            }
        }
        public int PageCount { get; set; }
        
        public int CurrentPage 
        {
            get
            {
                return Convert.ToInt32(teCurrentPage.EditValue);
            }
            set
            {
                teCurrentPage.EditValueChanging -= this.teCurrentPage_EditValueChanging;
                teCurrentPage.EditValue = value;
                teCurrentPage.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.teCurrentPage_EditValueChanging);
            }
        }
        public event PaginationEventHandler PageChanged;
        protected void RaisePageChanged()
        {
            if (PageChanged != null)
                PageChanged(this, new PaginationEventArgs(CurrentPage));
        }

        public Pagination()
        {
            InitializeComponent();
            CurrentPage = 1;
            TotalCount = 0;
            btnFirstPage.Enabled = false;
            btnPrevPage.Enabled = false;
            btnNextPage.Enabled = false;
            btnLastPage.Enabled = false;            
        }

        private void btnFirstPage_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            RefreshButton();
            RaisePageChanged();
        }

        private void btnPrevPage_Click(object sender, EventArgs e)
        {
            CurrentPage--;
            RefreshButton();
            RaisePageChanged();
        }

        private void btnNextPage_Click(object sender, EventArgs e)
        {
            CurrentPage++;
            RefreshButton();
            RaisePageChanged();
        }

        private void btnLastPage_Click(object sender, EventArgs e)
        {
            CurrentPage = TotalPage;
            RefreshButton();
            RaisePageChanged();
        }

        public void RefreshButton()
        {
            if (CurrentPage == 1)
            {
                btnFirstPage.Enabled = false;
                btnPrevPage.Enabled = false;
            }
            else
            {
                btnFirstPage.Enabled = true;
                btnPrevPage.Enabled = true;
            }
            if (CurrentPage == TotalPage)
            {
                btnLastPage.Enabled = false;
                btnNextPage.Enabled = false;
            }
            else
            {
                btnLastPage.Enabled = true;
                btnNextPage.Enabled = true;
            }
            
        }

        private void teCurrentPage_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            int n;
            bool b = Int32.TryParse(e.NewValue.ToString(), out n);

            if (!b || n < 1 || n > TotalPage)
            {
                e.Cancel = true;
            }
            else
            {
                CurrentPage = n;
                RaisePageChanged();
            }
        }
    }

    public delegate void PaginationEventHandler(object sender, PaginationEventArgs e);

    public class PaginationEventArgs
    {
        public int newPageNumber { get; set; }

        public PaginationEventArgs(int newPageNumber)
        {
            this.newPageNumber = newPageNumber;
        }
    }
}
