﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Reflection;
using System.Security.Cryptography;

namespace aetp.DocumentsSubmitter
{
    class Cripto
    {
        private static string key = "MgNO0kvPDMXZInei";

        public static byte[] Encrypt_ic(byte[] data)
        {
            SymmetricAlgorithm sa = Rijndael.Create();
            ICryptoTransform ct = sa.CreateEncryptor(
            (new PasswordDeriveBytes(key, null)).GetBytes(16),
            new byte[16]);

            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);

            cs.Write(data, 0, data.Length);
            cs.FlushFinalBlock();
            return ms.ToArray();
        }

        public static string Encrypt_ic(string data)
        {
            return Convert.ToBase64String(Encrypt_ic(Encoding.UTF8.GetBytes(data)));
        }

        static public byte[] Decrypt_ic(byte[] data)
        {
            BinaryReader br = new BinaryReader(InternalDecrypt_ic(data));
            return br.ReadBytes((int)br.BaseStream.Length);
        }

        static public string Decrypt_ic(string data)
        {
            CryptoStream cs = InternalDecrypt_ic(Convert.FromBase64String(data));
            StreamReader sr = new StreamReader(cs);
            return sr.ReadToEnd();
        }

        static CryptoStream InternalDecrypt_ic(byte[] data)
        {
            SymmetricAlgorithm sa = Rijndael.Create();
            ICryptoTransform ct = sa.CreateDecryptor(
            (new PasswordDeriveBytes(key, null)).GetBytes(16),
            new byte[16]);

            MemoryStream ms = new MemoryStream(data);
            return new CryptoStream(ms, ct, CryptoStreamMode.Read);
        }
    }
}
