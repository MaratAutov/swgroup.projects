﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;

namespace aetp.DocumentsSubmitter
{
    public class Config
    {
        const string ConfigFileName = "DocumentsSubmitterConfig.xml";
        public string Transport_FFMS_ReportsService { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public Config()
        {
            // init default 
            Transport_FFMS_ReportsService = "http://lk.fcsm.ru:8080/ReportService.asmx";
            Login = string.Empty;
            Password = string.Empty;
        }

        public string GetConfigFilePath()
        {
            return Path.Combine(aetp.Utils.Config.GetAppPath(), ConfigFileName);
        }

        public void Save()
        {
            FileStream fs = new FileStream(GetConfigFilePath(), FileMode.Create);
            XmlSerializer serializer = new XmlSerializer(typeof(Config));
            serializer.Serialize(fs, this);
            fs.Close();
        }

        public void Load()
        {
            try
            {
                string fileName = GetConfigFilePath();
                if (!File.Exists(fileName))
                {
                    Save();
                }
                FileStream fs = new FileStream(fileName, FileMode.Open);
                XmlSerializer serializer = new XmlSerializer(typeof(Config));
                Config cfg = (Config)serializer.Deserialize(fs);
                fs.Close();
                this.Transport_FFMS_ReportsService = "http://lk.fcsm.ru:8080/ReportService.asmx";
                this.Login = cfg.Login;
                this.Password = cfg.Password;

                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
