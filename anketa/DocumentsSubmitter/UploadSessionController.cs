﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace aetp.DocumentsSubmitter
{
    public class UploadSessionController
    {
        private const int PROCESSING_STATUS_INDEX = 1;
        private const int DOCNAME_INDEX = 0;
        private const int TRANSMITION_STATUS_INDEX = 2;
        private const int SENDING_STATUS_INDEX = 3;
        private Uploader _uploader;

        public Control SessionList { get; set; }

        public Uploader Uploader
        {
            get { return _uploader; }
            set { _uploader = value; }
        }

        public enum State
        {
            InProcess,
            Succeeded,
            Failed
        }

        private State _state;

        public State UploadState
        {
            get { return _state; }
            set
            {
                _state = value;
                UpdateToState();
            }
        }

        private bool _hasUnsavedData;

        public bool HasUnsavedData
        {
            get { return _hasUnsavedData; }
            set
            {
                _hasUnsavedData = value;
            }
        }

        ListViewItem _lvi = new ListViewItem(new string[] { "", "", "", "" });

        public ListViewItem Lvi
        {
            get { return _lvi; }
        }

        private void UpdateToState()
        {
            DocumentName = _uploader.ReportName;
            switch (_state)
            {
                case State.Failed:
                    {
                        StatusText = //"Не удалось передать пакет. " +
                            (Uploader.Error != null ? Uploader.Error.Message : null);
                        TransmitionStatus = TransmitionStatus.Failure;
                        break;
                    }
                case State.InProcess:
                    {
                        StatusText = null;
                        TransmitionStatus = TransmitionStatus.Processing;
                        break;
                    }
                case State.Succeeded:
                    {
                        StatusText = _uploader.Status;
                        TransmitionStatus = TransmitionStatus.Success;
                        
                        if (StatusText.Contains("Ошибка")) {
                            TransmitionStatus = TransmitionStatus.SuccessWithServerWarning;
                        }
                        break;
                    }
            }
        }

        public void Save(string filename)
        {
            int counter = 0;
            string dir = Path.GetDirectoryName(filename);
            string fn = Path.GetFileNameWithoutExtension(filename);
            string ext = Path.GetExtension(filename);
            while (File.Exists(filename))
            {
                counter++;
                filename = Path.Combine(dir, fn + "_" + counter + ext);
            }

            File.Copy(_uploader.DstNotificationFileName, filename);

            HasUnsavedData = false;
            UpdateToState();
        }

        public string StatusText
        {
            get { return _lvi.SubItems[PROCESSING_STATUS_INDEX].Text; }
            set { SetToStatusText(value); }
        }

        Action<string> StringSetter;
        void SetToStatusText(string value)
        {
            if (SessionList == null)
                return;
            if (SessionList.InvokeRequired)
            {
                StringSetter = SetToStatusText;
                SessionList.Invoke(StringSetter, value);
            }
            else
                _lvi.SubItems[PROCESSING_STATUS_INDEX].Text = value;
        }

        public string DocumentName
        {
            get { return _lvi.SubItems[DOCNAME_INDEX].Text; }
            set { _lvi.SubItems[DOCNAME_INDEX].Text = value; }
        }

        public string SendingState
        {
            set 
            {
                SetSendingStatusDelegate del = new SetSendingStatusDelegate(SetSendingStatus);
                SessionList.BeginInvoke(del, value);
            }
        }

        delegate void SetSendingStatusDelegate(string value);

        private void SetSendingStatus(string value)
        {
            _lvi.SubItems[SENDING_STATUS_INDEX].Text = value; 
        }

        private TransmitionStatus _transmitionStatus;

        public TransmitionStatus TransmitionStatus
        {
            get { return _transmitionStatus; }
            set
            {
                _transmitionStatus = value;
                _lvi.SubItems[TRANSMITION_STATUS_INDEX].Text =
                    _transmitionStatusToString[value];
                _lvi.StateImageIndex = (int)value;
            }
        }

        #region mappings

        static UploadSessionController()
        {
            _transmitionStatusToString = new Dictionary<TransmitionStatus, string>
			                             	{
			                             		{TransmitionStatus.Success, "Передано успешно"},
			                             		{TransmitionStatus.Failure, "Ошибка"},
			                             		{TransmitionStatus.SuccessWithServerWarning, "Передано, но ФСФР вернул ошибку"},
			                             		{TransmitionStatus.Processing, "Передается..."}
			                             	};

        }

        private static IDictionary<TransmitionStatus, string> _transmitionStatusToString;
       

        #endregion

    }

    public enum TransmitionStatus
    {
        Success = 0,
        Failure = 1,
        SuccessWithServerWarning = 2,
        Processing = 4
    }
}
