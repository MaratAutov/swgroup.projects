﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.DocumentsSubmitter.Views;

namespace aetp.DocumentsSubmitter
{
    public class DocumentsSubmitterRunner
    {
        public static void Run()
        {
            SessionForm sf = new SessionForm();
            sf.ShowDialog();
        }

        public static void Run(byte[] packet_body)
        {
            SessionForm sf = new SessionForm();
            sf.ShowDialog();
        }
    }
}
