﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aetp.DocumentsSubmitter
{    
    public class DSGlobal
    {
        private DSGlobal() { }
        public Config Config { get; set; }
        
        static DSGlobal()
        {
            instance = new DSGlobal();
            instance.Config = new Config();
            instance.Config.Load();
        }
        private static DSGlobal instance;
        public static DSGlobal Instance
        {
            get
            {
                return instance;
            }
        }
    }
    
}
