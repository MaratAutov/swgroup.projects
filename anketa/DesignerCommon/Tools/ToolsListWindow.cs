﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using aetp.Common.ControlModel;
using aetp.Common.Tools.Tasks;

namespace aetp.Common.Tools
{
    public partial class ToolsListWindow : Form
    {
        Report report;

        public ToolsListWindow(Report report)
        {
            InitializeComponent();
            this.report = report;
            RefreshList();
        }

        void RefreshList()
        {
            IToolTask task1 = new RemoveElements();
            ListViewItem lvi1 = new ListViewItem(new string[] { task1.Name, task1.Description });
            lvi1.Tag = task1;
            lv.Items.Add(lvi1);

            IToolTask task2 = new SetGridProperty();
            ListViewItem lvi2 = new ListViewItem(new string[] { task2.Name, task2.Description });
            lvi2.Tag = task2;
            lv.Items.Add(lvi2);
            
            IToolTask task3 = new SetCorrectRootProperty();
            ListViewItem lvi3 = new ListViewItem(new string[] { task3.Name, task3.Description });
            lvi3.Tag = task3;
            lv.Items.Add(lvi3);

            IToolTask task4 = new SetGridColumnProperty();
            ListViewItem lvi4 = new ListViewItem(new string[] { task4.Name, task4.Description });
            lvi4.Tag = task4;
            lv.Items.Add(lvi4);

            IToolTask task5 = new SelectEqualCell();
            ListViewItem lvi5 = new ListViewItem(new string[] { task5.Name, task5.Description });
            lvi5.Tag = task5;
            lv.Items.Add(lvi5);

            IToolTask task6 = new SetGridRowProperty();
            ListViewItem lvi6 = new ListViewItem(new string[] { task6.Name, task6.Description });
            lvi6.Tag = task6;
            lv.Items.Add(lvi6);

            IToolTask task7 = new SetNewElementIndexProperty();
            ListViewItem lvi7 = new ListViewItem(new string[] { task7.Name, task7.Description });
            lvi7.Tag = task7;
            lv.Items.Add(lvi7);

            IToolTask task8 = new SetDictionaryItemsOnComboBox();
            ListViewItem lvi8 = new ListViewItem(new string[] { task8.Name, task8.Description });
            lvi8.Tag = task8;
            lv.Items.Add(lvi8);

            IToolTask task9 = new CorretValidationItem();
            ListViewItem lvi9 = new ListViewItem(new string[] { task9.Name, task9.Description });
            lvi9.Tag = task9;
            lv.Items.Add(lvi9);
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.ListView.SelectedListViewItemCollection lvis = lv.SelectedItems;
            if (lvis.Count > 0)
            {
                try
                {
                    (lvis[0].Tag as IToolTask).Execute(report);
                    MessageBox.Show("OK");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void lv_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
                pg.SelectedObject = lv.SelectedItems[0].Tag;
        }
    }

    public interface IToolTask
    {
        string Name { get; }
        string Description { get; }
        void Execute(Report report);
    }
}
