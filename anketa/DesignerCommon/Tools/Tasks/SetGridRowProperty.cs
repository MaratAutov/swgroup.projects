﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using aetp.Common.Validator;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.Validator;
using DevExpress.XtraTreeList.Nodes;
using aetp.Common.ControlModel.TypeConverters;
using DevExpress.XtraEditors.DXErrorProvider;
using aetp.Common.ControlModel;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace aetp.Common.Tools.Tasks
{
    public class SetGridRowProperty : IToolTask
    {
        public SetGridRowProperty()
        {
            
        }

        [Browsable(false)]
        public string Name
        {
            get
            {
                return "Set Grid Row property";
            }
        }

        [Browsable(false)]
        public string Description
        {
            get
            {
                return "Set Grid Row property $Y - row index pattern ";
            }
        }

        [DisplayName("Имя грида")]
        public virtual string GridName { get; set; }
               
        [DisplayName("Шаблон Имени строки")]
        public virtual String RowName { get; set; }

        [DisplayName("Обязательно для заполнения")]
        public virtual bool IsMadndatoryToFill { get; set; }

        [DisplayName("Количество строк")]
        public virtual int RowCount { get; set; }

        public void Execute(Report report)
        {
            if (report == null)
                throw new Exception("Отчет не может быть null");
            else
            {
                BaseControl bs = ReportUtils.GetControlByName(report, GridName);
                if (!(bs is Grid))
                    throw new Exception("Table not found");
                Operation(bs as Grid);
            }
        }

        void Operation(Grid grid)
        {
            
            for (int j = 0; j < RowCount; j++)
            {
                GridRow row = null;
                if (j < grid.Rows.Count)
                    row = grid.Rows[j];
                else
                {
                    row = new GridRow();
                    row.Grid = grid;
                    row.Root = grid.Root;
                    int col = grid.Columns.Count;
                    for (int i = 0; i < col; i++)
                    {
                        GridCellItem cell = new GridCellItem() { Column = grid.Columns[i] };
                        cell.Root = grid.Root;
                        cell.Row = row;
                        cell.Name = String.Format("Cell_{0}", BaseControl.new_element_index.ToString());
                        cell.Name = String.Format("Cell_{0}", BaseControl.new_element_index.ToString());
                        row.Cells.Add(cell);
                    }
                    grid.Rows.Add(row);
                }
                row.Name = RowName.Replace("$Y", (j + 1).ToString());
                row.IsMandatoryToFill = IsMadndatoryToFill;
                
            }
        }
    }

}
