﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using aetp.Common.Validator;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.Validator;
using DevExpress.XtraTreeList.Nodes;
using aetp.Common.ControlModel.TypeConverters;
using DevExpress.XtraEditors.DXErrorProvider;
using aetp.Common.ControlModel;
using System.Windows.Forms;


namespace aetp.Common.Tools.Tasks
{
    public class SetCorrectRootProperty : IToolTask
    {
        public string Name
        {
            get
            {
                return "Set correct root property";
            }
        }

        public string Description
        {
            get
            {
                return "Если импортировали разделы то советую прогнать эту тулзу, она выставит правильный Root у элементов, сейчас эта бага пофикшена и импорт работает корректно";
            }
        }

        public void Execute(Report report)
        {
            if (report == null)
                throw new Exception("Отчет не может быть null");
            else
                ReportUtils.DoAction(report.Children, (bs) => { bs.Root = report; });
        }

    }
}
