﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.Windows.Forms;
using System.ComponentModel;

namespace aetp.Common.Tools.Tasks
{
    public class SetDictionaryItemsOnComboBox : IToolTask
    {
        [Browsable(false)]
        public string Name
        {
            get
            {
                return "Исправляет комбо боксы уменьшает размер файла";
            }
        }

        [Browsable(false)]
        public string Description
        {
            get
            {
                return "При импорте у комбобоксов копируются словари в свойствах, что бы завязать их на глобальные словари выполните это ";
            }
        }

        Report report;

        public void Execute(Report report)
        {
            if (report == null)
                throw new Exception("Отчет не может быть null");
            this.report = report;
            ReportUtils.DoAction(report.Children, action);
            GC.Collect();
        }

        void action(BaseControl bs)
        {
            if (bs is aetp.Common.ControlModel.ComboBox)
            {
                aetp.Common.ControlModel.ComboBox combo = bs as aetp.Common.ControlModel.ComboBox;
                if (combo.Source != null)
                {
                    foreach (var d in report.MetaData.Dictionarys)
                    {
                        if (d.Name == combo.Source.Name)
                        {
                            combo.Source = d;
                            break;
                        }
                    }
                }
            }
        }

    }
}
