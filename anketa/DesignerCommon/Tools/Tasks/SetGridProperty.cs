﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using aetp.Common.Validator;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.Validator;
using DevExpress.XtraTreeList.Nodes;
using aetp.Common.ControlModel.TypeConverters;
using DevExpress.XtraEditors.DXErrorProvider;
using aetp.Common.ControlModel;
using System.Windows.Forms;


namespace aetp.Common.Tools.Tasks
{
    public class SetGridProperty : IToolTask
    {
        [Browsable(false)]
        public string Name
        {
            get
            {
                return "Set Grid property";
            }
        }

        [Browsable(false)]
        public string Description
        {
            get
            {
                return "set Grid property";
            }
        }

        [DisplayName("Расстояние между Подписью и таблицей")]
        public virtual int Interval { get; set; }
        
        [DisplayName("Ширина Подписи")]
        public virtual int WidthCaption { get; set; }

        [DisplayName("Ширина таблицы")]
        public virtual int WidthGrid { get; set; }
        
        [DisplayName("Высота таблицы")]
        public virtual int HeightGrid { get; set; }

        [DisplayName("Высота таблицы (в строках)")]
        public int HeightGridInRows { get; set; }

        //[DisplayName("Автоподбор высоты")]
        //public bool AutoHeight { get; set; }		

        public void Execute(Report report)
        {
            if (report == null)
                throw new Exception("Отчет не может быть null");
            else
                Operation(report, report.Children);
        }

        void Operation(BaseControl parent, List<BaseControl> ctrls)
        {
            for (int i = ctrls.Count -1; i >=0; i--)
            {
                if (!action(parent, ctrls[i]))
                {
                    if (ctrls[i] is Page || ctrls[i] is MetaConrol || ctrls[i] is GridAddedPage || ctrls[i] is GridPage)
                        Operation(ctrls[i], ctrls[i].Children);
                }
            }
        }

        bool action(BaseControl parent, BaseControl control)
        {
            if (control is BaseGrid)
            {
                BaseGrid g = control as BaseGrid;
                g.Interval = Interval;
                g.WidthCaption = WidthCaption;
                g.WidthGrid = WidthGrid;
                if (!(control is GridAddedRow))
                    g.HeightGrid = HeightGrid;
            }
            if (control is GridAddedRow)
            {
                (control as GridAddedRow).HeightGridInRows = HeightGridInRows;
            }
            return false;

        }

    }
}
