﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.Windows.Forms;
using System.ComponentModel;

namespace aetp.Common.Tools.Tasks
{
    public class RemoveElements : IToolTask
    {
        [Browsable(false)]
        public string Name
        {
            get
            {
                return "Remove Element";
            }
        }

        [Browsable(false)]
        public string Description
        {
            get
            {
                return "remove ListView";
            }
        }

        public void Execute(Report report)
        {
            if (report == null)
                throw new Exception("Отчет не может быть null");
            else
                Operation(report, report.Children);
        }

        void Operation(BaseControl parent, List<BaseControl> ctrls)
        {
            for (int i = ctrls.Count -1; i >=0; i--)
            {
                if (!action(parent, ctrls[i]))
                {
                    if (ctrls[i] is Page || ctrls[i] is MetaConrol)
                        Operation(ctrls[i], ctrls[i].Children);
                }
            }
        }

        bool action(BaseControl parent, BaseControl control)
        {
            if (control is DictionaryList)
            {
                parent.Children.Remove(control);
                return true;
            }
            return false;

        }

    }
}
