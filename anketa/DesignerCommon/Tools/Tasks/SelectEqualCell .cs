﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.Windows.Forms;
using System.ComponentModel;

namespace aetp.Common.Tools.Tasks
{
    public class SelectEqualCell : IToolTask
    {
        [Browsable(false)]
        public string Name
        {
            get
            {
                return "Select Equals Cells";
            }
        }

        [Browsable(false)]
        public string Description
        {
            get
            {
                return "Найти ячейки столбцы или строки с одинаковым именем во всем отчете";
            }
        }

        public void Execute(Report report)
        {          
            if (report == null)
                throw new Exception("Отчет не может быть null");
            else
            {
                report.cellvalidator.Operation(report); 
            }                
        }
    }    
}
