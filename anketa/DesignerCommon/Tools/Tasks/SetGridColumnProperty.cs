﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using aetp.Common.Validator;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.Validator;
using DevExpress.XtraTreeList.Nodes;
using aetp.Common.ControlModel.TypeConverters;
using DevExpress.XtraEditors.DXErrorProvider;
using aetp.Common.ControlModel;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace aetp.Common.Tools.Tasks
{
    public class SetGridColumnProperty : IToolTask
    {
        public SetGridColumnProperty()
        {
            Cells = new GridCellCollection();
        }

        [Browsable(false)]
        public string Name
        {
            get
            {
                return "Set Grid Column property";
            }
        }

        [Browsable(false)]
        public string Description
        {
            get
            {
                return "Set Grid Column property $X - column index pattern $Y - row index pattern ";
            }
        }

        [DisplayName("Имя грида")]
        public virtual string GridName { get; set; }
               
        [DisplayName("Ячейки")]
        public virtual GridCellCollection Cells { get; set; }

        [DisplayName("Изменять Имя")]
        public virtual bool IsChangeName { get; set; }
        [DisplayName("Изменять Тег")]
        public virtual bool IsChangeSerializtionTag { get; set; }
        [DisplayName("Изменять Caption")]
        public virtual bool IsChangeCaption { get; set; }
        [DisplayName("Изменять Обязательность для заполнения")]
        public virtual bool IsChangeMadndatoryToFill { get; set; }
        [DisplayName("Изменять тип")]
        public virtual bool IsChangeType { get; set; }

        public void Execute(Report report)
        {
            if (report == null)
                throw new Exception("Отчет не может быть null");
            else
            {
                BaseControl bs = ReportUtils.GetControlByName(report, GridName);
                if (!(bs is Grid))
                    throw new Exception("Table not found");
                Operation(bs as Grid);
            }
        }

        void Operation(Grid grid)
        {
            int r =0;
            foreach (var row in grid.Rows)
            {
                for (int i = 0; i < Cells.Count; i++)
                {
                    if (i >= row.Cells.Count)
                        break;

                    if (IsChangeName)
                    {
                        row.Cells[i].Name = Cells[i].Name.Replace("$Y", (r + 1).ToString()).Replace("$X", (i + 1).ToString());
                    }
                    if (IsChangeCaption)
                        row.Cells[i].Caption = Cells[i].Caption;
                    if (IsChangeSerializtionTag)
                        row.Cells[i].SerializingTag = Cells[i].SerializingTag;
                    if (IsChangeMadndatoryToFill)
                        row.Cells[i].IsMandatoryToFill = Cells[i].IsMandatoryToFill;
                    if (IsChangeType)
                        row.Cells[i].Type = Cells[i].Type;
                }
                r++;
            }
        }
    }

    public class GridCellCollection : Collection<GridCellItem>
    {
        protected override void  InsertItem(int index, GridCellItem item)
        {
 	         base.InsertItem(index, item);
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
        }

        protected override void SetItem(int index, GridCellItem item)
        {
            base.SetItem(index, item);
        }
    }

    
}
