﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.Windows.Forms;
using System.ComponentModel;
using aetp.Common.Validator;

namespace aetp.Common.Tools.Tasks
{
    public class CorretValidationItem : IToolTask
    {
        [Browsable(false)]
        public string Name
        {
            get
            {
                return "Исправляет количества элементов валидации";
            }
        }

        [Browsable(false)]
        public string Description
        {
            get
            {
                return "Исправляет количества элементов валидации";
            }
        }

        Report report;

        List<info> list;

        public void Execute(Report report)
        {
            if (report == null)
                throw new Exception("Отчет не может быть null");
            this.report = report;
            //showinfo();
            ReportUtils.DoAction(report.Children, action);
            var ser = new aetp.Common.Serializer.FormSerializer();
            //ValidationCollection.Clear();
            GC.Collect();
            //showinfo();
        }

        void showinfo()
        {
            list = new List<info>();
            ReportUtils.DoAction(report.Children, actioninfo);
            var total = list.OrderByDescending(x => x.EditorValidatorsCount).Take(15).ToList();
            string msg = "";
            foreach (var i in total)
            {
                msg += i.Name + ":" + i.EditorValidatorsCount.ToString() + Environment.NewLine;
            }
            MessageBox.Show(msg);
        }

        void action(BaseControl bs)
        {
            int i = 0;
            List<Type> types = new List<Type>();
            List<Validator.IValidationItem> validations = new List<Validator.IValidationItem>();
            while (i < bs.EditorValidators.Count)
            {
                if (!types.Contains(bs.EditorValidators[i].GetType()))
                {
                    types.Add(bs.EditorValidators[i].GetType());
                    validations.Add(bs.EditorValidators[i]);
                }
                i++;
            }
            bs.EditorValidators = validations;
        }

        void actioninfo(BaseControl bs)
        {
            list.Add(new info(bs.Name, bs.EditorValidators.Count, bs.EditorValidators));
        }

        class info
        {
            public string Name { get; set; }
            public int EditorValidatorsCount { get; set; }
            public List<Validator.IValidationItem> Validators { get; set; }

            public info(string name, int count, List<Validator.IValidationItem> validators)
            {
                Name = name;
                EditorValidatorsCount = count;
                Validators = validators;
            }
        }

    }
}
