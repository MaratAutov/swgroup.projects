﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using aetp.Common.Validator;
using System.Runtime.Serialization;
using aetp.Common.ControlModel.Validator;
using DevExpress.XtraTreeList.Nodes;
using aetp.Common.ControlModel.TypeConverters;
using DevExpress.XtraEditors.DXErrorProvider;
using aetp.Common.ControlModel;
using System.Windows.Forms;


namespace aetp.Common.Tools.Tasks
{
    public class SetNewElementIndexProperty : IToolTask
    {
        [Browsable(false)]
        public string Name
        {
            get
            {
                return "Set new Element Index property";
            }
        }

        [Browsable(false)]
        public string Description
        {
            get
            {
                return "Set new Element Index property";
            }
        }

        [DisplayName("Текущий индекс нового элеменда для добавления")]
        public virtual int Index
        {
            get;
            set;
        }
        
        public void Execute(Report report)
        {
            BaseControl.new_element_index = Index;
        }

    }
}
