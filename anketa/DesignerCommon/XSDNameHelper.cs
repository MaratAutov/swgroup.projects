﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.Text.RegularExpressions;
using System.IO;

namespace aetp.Common
{
    public class XSDNameHelper
    {
        #region private method

        static char[] badChars = { ',', '-', '.' };

        static string removeBadChars(string str)
        {
            str = str.Trim();
            if (str.Length > 0 && badChars.Contains(str[0]))
                str = str.Remove(0, 1);
            if (str.Length > 0 && badChars.Contains(str.Last()))
                str = str.Remove(str.Length - 1, 1);
            return str;
        }

        static string numPattern = "^[0-9]+$";
        
        static string GetFirstNonNumericRowCaption(GridRow row, int maxColumns = -1)
        {
            string caption = "";
            int index = 0;
            for (int j = 0; j < row.Cells.Count; j++)
            {
                if (index == j)
                    break;
                if (row.Cells[j].Type == GridCellType.Label)
                {
                    string str = row.Cells[j].Caption;
                    str = removeBadChars(str);

                    if (!String.IsNullOrEmpty(str))
                    {
                        Regex r = new Regex(numPattern);
                        if (!r.IsMatch(str))
                        {
                            caption = str;
                            break;
                        }
                    }
                }
                index++;
            }
            return caption;
        }

        static string CaptionsToString(IEnumerable<string> captions)
        {
            return String.Join("|", captions);
        }

        static CellCaptionFindResultContext FindCellCaption(GridCellItem cell)
        {
            var getResult = GetCellCaption(cell);
            CellFindMethod method = CellFindMethod.NotFind;
            List<string> captions = new List<string>();
            string columnCaption = GetColumnCaption(cell.Column);
            if (getResult.Method == CellFindMethod.NotFind)
            {
                int index = GetColumnIndex(cell.Column);
                Stack<string> rowCaptions = new Stack<string>();
                bool isFindedFirstCaption = false;
                for (int j = index - 1; j >= 0; j--)
                {
                    var finded = GetCellCaption(cell.Row.Cells[j]);
                    if (isFindedFirstCaption && finded.Method == CellFindMethod.NotFind) break;
                    if (finded.Method != CellFindMethod.NotFind && !String.IsNullOrEmpty(finded.Caption)
                        // русская и английкая х
                        && (finded.Caption != "X" || finded.Caption != "Х"))
                    {
                        isFindedFirstCaption = true;
                        rowCaptions.Push(finded.Caption);
                    }
                }
                while (rowCaptions.Count > 0)
                {
                    captions.Add(rowCaptions.Pop());
                }
                captions.Add(columnCaption);
            }
            else
            {
                captions.Add(columnCaption);
                captions.Add(getResult.Caption);
                method = CellFindMethod.CaptionAndColumn;
            }
            return new CellCaptionFindResultContext(CaptionsToString(captions), method);
        }

        static bool RowIsHeader(GridRow row)
        {
            bool isHeader = true;
            foreach (var cell in row.Cells)
            {
                if (cell.Type != GridCellType.Label)
                    isHeader = false;
            }
            return isHeader;
        }

        #region GetHeaderCellCaption

        /// <summary>
        /// Получение описания ячейки по заголовку взятому из строк верхних таблиц метагрида
        /// </summary>
        /// <param name="column">Колонка в которой находится ячейка</param>
        /// <param name="row">Строка с заголовками</param>
        /// <returns></returns>
        static string GetHeaderCellCaption(GridColumn column, GridRow row)
        {
            int index = GetColumnIndex(column);
            if (column.grid.Columns.Count == row.Grid.Columns.Count)
            {
                return GetCellCaption(row.Cells[index]).Caption;
            }
            else
            {
                int delta = 0;
                for (int i = 0; i <= index - 1; i++)
                {
                    delta += column.grid.Columns[i].WidthColumn;
                }

                int width = 0;
                int j = 0;
                while (width < delta)
                {
                    width += row.Grid.Columns[j].WidthColumn;
                    j++;
                }
                if (j > 0)
                    j -= 1;
                return GetCellCaption(row.Cells[j]).Caption;
            }
        }

        /// <summary>
        /// Получение описания ячейки по заголовку взятому из строк верхних таблиц метагрида
        /// </summary>
        /// <param name="column">Колонка в которой находится ячейка</param>
        /// <param name="row">Строка с заголовками</param>
        /// <returns></returns>
        static string GetHeaderCellCaption(GridAddedRowColumn column, GridRow row)
        {
            int index = GetColumnIndex(column);
            if (column.Grid.Columns.Count == row.Grid.Columns.Count)
            {
                return GetCellCaption(row.Cells[index]).Caption;
            }
            else
            {
                int delta = 0;
                for (int i = 0; i <= index - 1; i++)
                {
                    delta += column.Grid.Columns[i].WidthColumn;
                }

                int width = 0;
                int j = 0;
                while (width < delta)
                {
                    width += row.Grid.Columns[j].WidthColumn;
                    j++;
                }
                if (j > 0)
                    j -= 1;
                return GetCellCaption(row.Cells[j]).Caption;
            }
        }

        #endregion

        #region GetColumnCaption

        static string GetColumnCaption(GridAddedRowColumn column)
        {
            string caption = removeBadChars(column.Caption);
            if ((column.Caption.StartsWith("Element") || String.IsNullOrEmpty(column.Caption.Trim())) && column.Grid.metagrid != null)
            {
                int position = GetColumnIndex(column);
                List<string> captions = new List<string>();
                foreach (var table in column.Grid.metagrid.Tables)
                {
                    if (table is Grid)
                    {
                        var grid = table as Grid;
                        if (grid.Columns.Count == column.Grid.Columns.Count && !grid.Columns[position].Caption.StartsWith("Element"))
                        {
                            return grid.Columns[position].Caption;
                        }
                        else
                        {
                            bool issetNotHeader = false;
                            foreach (var row in grid.Rows)
                            {
                                bool isHeader = RowIsHeader(row);
                                if (isHeader)
                                {
                                    captions.Add(GetHeaderCellCaption(column, row));
                                }
                                else
                                {
                                    issetNotHeader = true;
                                    break;
                                }
                            }
                            if (issetNotHeader)
                                break;
                        }
                    }
                }
                return String.Join(" ", captions);
            }

            return caption;

        }

        static string GetColumnCaption(GridColumn column)
        {
            string caption = removeBadChars(column.Caption);
            if ((column.Caption.StartsWith("Element") || String.IsNullOrEmpty(column.Caption.Trim())) && column.grid.metagrid != null)
            {
                int position = GetColumnIndex(column);
                List<string> captions = new List<string>(); 
                foreach (var table in column.grid.metagrid.Tables)
                {
                    if (table is Grid)
                    {
                        var grid = table as Grid;
                        if (grid.Columns.Count == column.grid.Columns.Count && !grid.Columns[position].Caption.StartsWith("Element"))
                        {
                            return grid.Columns[position].Caption;
                        }
                        else
                        {
                            bool issetNotHeader = false;
                            foreach (var row in grid.Rows)
                            {
                                bool isHeader = RowIsHeader(row);
                                if (isHeader)
                                {
                                    captions.Add(GetHeaderCellCaption(column, row));
                                }
                                else
                                {
                                    issetNotHeader = true;
                                    break;
                                }
                            }
                            if (issetNotHeader)
                                break;
                        }
                    }
                }
                return String.Join(" ", captions);
            }

            return caption;
        }

        #endregion

        #region GetColumnIndex

        static int GetColumnIndex(GridColumn column)
        {
            int position = column.grid.Columns.IndexOf(column);
            // если прямая ссылка не работает ищем по имени
            if (position == -1)
            {
                for (int i = 0; i < column.grid.Columns.Count; i++)
                {
                    if (column.grid.Columns[i].Name == column.Name)
                    {
                        position = i;
                        break;
                    }
                }
            }
            return position;
        }

        static int GetColumnIndex(GridAddedRowColumn column)
        {
            int position = column.Grid.Columns.IndexOf(column);
            // если прямая ссылка не работает ищем по имени
            if (position == -1)
            {
                for (int i = 0; i < column.Grid.Columns.Count; i++)
                {
                    if (column.Grid.Columns[i].Name == column.Name)
                    {
                        position = i;
                        break;
                    }
                }
            }
            return position;
        }

        #endregion

        static int GetCellIndex(GridCellItem cell)
        {
            int position = cell.Row.Cells.IndexOf(cell);
            // если прямая ссылка не работает ищем по имени
            if (position == -1)
            {
                for (int i = 0; i < cell.Row.Cells.Count; i++)
                {
                    if (cell.Row.Cells[i].Name == cell.Name)
                    {
                        position = i;
                        break;
                    }
                }
            }
            return position;
        }

        static CellCaptionFindResultContext GetCellCaption(GridCellItem cell)
        {
            string caption = String.Empty;
            if (cell.Type == GridCellType.Label)
            {
                caption = removeBadChars(cell.Caption);
                if (!String.IsNullOrEmpty(caption))
                {
                    return new CellCaptionFindResultContext(cell.Caption, CellFindMethod.Lable);
                }
            }
            else if (cell.Type == GridCellType.String)
            {
                caption = removeBadChars(cell.Caption);
                if (!String.IsNullOrEmpty(caption))
                {
                    return new CellCaptionFindResultContext(cell.Mask.Description ?? "", CellFindMethod.MaskDescription);
                }
            }
            return new CellCaptionFindResultContext(String.Empty, CellFindMethod.NotFind);
        }

        static string GetNonStandartCaption(string caption)
        {
            if (caption.StartsWith("Table") || caption.StartsWith("element") || String.IsNullOrEmpty(caption.Trim()))
                return string.Empty;
            else
                return caption;
        }

        class CellCaptionFindResultContext
        {
            public string Caption { get; set; }
            public CellFindMethod Method { get; set; }

            public CellCaptionFindResultContext (string caption, CellFindMethod method)
	        {
                Caption = caption;
                Method = method;
	        }
        }

        enum CellFindMethod
        {
            NotFind = 0,
            Lable,
            MaskDescription,
            LeftCaption,
            CaptionAndColumn
        }

        #endregion

        #region public methods

        public static string GetRowCaption(GridRow row)
        {
            string caption = "";
            List<string> captions = new List<string>();
            foreach (var cell in row.Cells)
            {
                caption = GetCellCaption(cell).Caption;
                caption = removeBadChars(caption);
                    
                if (!String.IsNullOrEmpty(caption))
                    captions.Add(caption);
            }
            
            return CaptionsToString(captions);
        }
        
        public static string GetGridCellItemCaption(GridCellItem cell)
        {
            return FindCellCaption(cell).Caption;
        }

        public static string GetGridCaption(BaseGrid grid)
        {
            string caption = GetNonStandartCaption(grid.Caption);
            if (String.IsNullOrEmpty(caption))
            {
                if (grid is Grid && (grid as Grid).metagrid != null)
                {
                    caption = GetNonStandartCaption((grid as Grid).metagrid.Caption);
                }
                if (grid is GridAddedRow && (grid as GridAddedRow).metagrid != null)
                {
                    caption = GetNonStandartCaption((grid as GridAddedRow).metagrid.Caption);
                }
            }
            else
                caption = grid.Caption;
            
            return caption;
        }

        public static string GetGridAddedRowColumnCaption(GridAddedRowColumn column)
        {
            string caption = GetColumnCaption(column);
            return caption;
        }

        #endregion
    }
}
