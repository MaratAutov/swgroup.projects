﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using aetp.Common.ControlModel;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraTreeList.Nodes.Operations;
using DevExpress.XtraTreeList.Nodes;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Windows;
using DevExpress.XtraTreeList;
using aetp.Common.Mediator;
using WeifenLuo.WinFormsUI.Docking;
using aetp.Common.Event;
using aetp.Common.Tools;

namespace aetp.Common.View
{
    public partial class ConstructorView : BaseView
    {
        ConstructorToolWindowMediator toolWindowMeditator;

        #region BaseView implementation

        public override TreeList tlControls
        {
            get
            {
                return toolWindowMeditator.tlControls;
            }
        }
        
        #endregion

        public PropertyGrid PropertyGrid
        {
            get
            {
                return toolWindowMeditator.PropertyGrid;
            }
        }

        public string Title
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        public string CodeStream
        {
            get
            {
                return toolWindowMeditator.CodeStream;
            }
            set
            {
                toolWindowMeditator.CodeStream = value;
            }
        }

        public ConstructorView()
        {
            InitializeComponent();            

            this.Title = "Анкета.Конструктор " + this.GetType().Assembly.GetName().Version.ToString();
            

            toolWindowMeditator = new ConstructorToolWindowMediator(this);
            
#if DEBUG
            bbiTools.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
#endif
        }

        #region event

        public event EventHandler XSDGenerate;
        protected void RaiseXSDGenerate()
        {
            if (XSDGenerate != null)
                XSDGenerate(this, null);
        }

        public delegate void AddControlHandler(object sender, AddControlEventArgs args);
        public event AddControlHandler AddNewConrol;
        protected void RaiseAddNewControl(Type t)
        {
            if (AddNewConrol != null)
                AddNewConrol(this, new AddControlEventArgs() { type = t });
        }


        public delegate void AddControlEditHandler(object sender, AddControlEditEventArgs args,SimpleDataType dt);
        public event AddControlEditHandler AddNewEditConrol;
        protected void RaiseAddNewEdit(Type t,SimpleDataType dt)
        {
            if (AddNewEditConrol != null)
                AddNewEditConrol(this, new AddControlEditEventArgs() { type = t },dt);
        }

        
        public event AddListHandler AddNewDictionary;
        protected void RaiseAddNewDictionary(Type t)
        {
            if (AddNewDictionary != null)
                AddNewDictionary(this, new MetaDataListAddedEventArgs() { type = t });
        }

        public event EventHandler Import;
        protected void RaiseImport()
        {
            if (Import != null)
                Import(this, null);
        }        

        public event EventHandler Dublicate;
        protected void RaiseDublicate()
        {
            if (Dublicate != null)
                Dublicate(this, null);
        }

        public delegate void RemoveControlHandler(object sender, RemoveControlEventArgs args);
        public event RemoveControlHandler RemoveFocusedControl;
        protected void RaiseRemoveFocusedControl(BaseControl control)
        {
            if (RemoveFocusedControl != null)
                RemoveFocusedControl(this, new RemoveControlEventArgs() { Control = control });
        }

        public event EventHandler SaveAs;
        protected void RaiseSaveAs()
        {
            if (SaveAs != null)
                SaveAs(this, null);
        }

        public event EventHandler Save;
        protected void RaiseSave()
        {
            if (Save != null)
                Save(this, null);
        }

        public event EventHandler LoadFile;
        protected void RaiseLoadFile()
        {
            if (LoadFile != null)
            {                
                LoadFile(this, null);
            }
        }

        public event EventHandler RunEditor;
        protected void RaiseRunEditor()
        {
            if (RunEditor != null)
                RunEditor(this, null);
        }

        public event EventHandler OpenEditor;
        protected void RaiseOpenEditor()
        {
            if (OpenEditor != null)
                OpenEditor(this, null);
        }

        public event EventHandler New;
        protected void RaiseNew()
        {
            if (New != null)
                New(this, null);
        }

        public delegate void LoadHandler(object sender, LoadEventArgs args);
        public event LoadHandler LoadXml;
        protected void RaiseLoad(TextReader xml)
        {
            if (LoadXml != null)
                LoadXml(this, new LoadEventArgs() { XmlReader = xml });
        }
                
        #endregion

        #region Event Handlers

        private void bbiAddPage_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(Page));
        }

        private void bbiAddSimpleEditor_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(SimpleEdit));
        }

        private void bbiAddSimpleEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewEdit(typeof(SimpleEdit),SimpleDataType.String);
        }

        private void bbiAddRichEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(SimpleRichEdit));
        }

        private void bbiAddDateEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewEdit(typeof(SimpleEdit),SimpleDataType.DateTime);
        }

        private void bbiAddIntEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewEdit(typeof(SimpleEdit),SimpleDataType.Integer);
        }

        private void bbiAddFloatEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewEdit(typeof(SimpleEdit),SimpleDataType.Double);
        }

        private void bbiAddCheckEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewEdit(typeof(SimpleEdit),SimpleDataType.Bool);
        }

        private void bbiAddLabel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(SimpleLabel));
        }

        private void AddLink_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(Link));
        }

        private void btnAddGrid_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(Grid));
        }

        private void bbiAddGridAddedRow_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(GridAddedRow));
        }

		private void bbiAddMetaGrid_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			RaiseAddNewControl(typeof(MetaGrid));
		}

        private void bbiAddGridAddedPage_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(GridAddedPage));
        }

        private void btn_gridAddedPage_Click(object sender, EventArgs e)
        {
            RaiseAddNewControl(typeof(GridAddedPage));
        }

        private void bbiAddComboBox_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(aetp.Common.ControlModel.ComboBox));
        }

        private void bbiTools_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new ToolsListWindow(Report).ShowDialog();
        }

        private void bbiRemoveItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BaseControl getFocusedControl = GetFocusedControl();
            if (!(getFocusedControl is Report))
            {
                if (getFocusedControl != null && System.Windows.Forms.MessageBox.Show(
                    "Вы действительно хотите удалить элемент?", "Внимание", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation) == System.Windows.Forms.DialogResult.Yes)
                {
                    RaiseRemoveFocusedControl(getFocusedControl);
                    // aetp.Common.ControlModel.BaseControl.list_linkcontrol.Remove(getFocusedControl);
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Невозможно удалить корневой элемент");
            }
        }

        private void bbiLoad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseLoad(new StringReader(toolWindowMeditator.CodeStream));
        }

        private void bbiAddDictionary_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewDictionary(typeof(DictionaryList));
        }

        private void bbiImportDictionary_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseImport();
        }
        

        private void bbiSaveAs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseSaveAs();
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseSave();
        }

        private void bbiLoadFile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseLoadFile();
        }

        private void bbiRunEditor_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseRunEditor();
        }

        private void bbiDublicateSection_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseDublicate();
        }

        private void bbiOpenEditor_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseOpenEditor();
        }

        private void ConstructorView_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            toolWindowMeditator.Initial();
        }

        private void bbiNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Report == null || System.Windows.Forms.MessageBox.Show("Все не сохраненные данные будут потеряны. Вы действительно хотите создать новый отчет?", "Внимание!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)== System.Windows.Forms.DialogResult.Yes)
            {
                RaiseNew();
                //aetp.Common.ControlModel.BaseControl.list_linkcontrol.Clear();
                //aetp.Common.ControlModel.BaseControl.list_bascontrol.Clear();
            }
        }

        private void bbiAddMetaControl_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(MetaConrol));
        }

        private void bbiAddMemoEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAddNewControl(typeof(MemoEdit));
        }

        private void btnXSDGenerate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseXSDGenerate();
        }
        #endregion
        
    }
}
