﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using aetp.Common.ControlModel;
using System.IO;

namespace aetp.Common.View
{
    public partial class StructureView : BaseToolWindow
    {
        //указывает какое действие собирается сделать treelist при drag and drop
        int imageindex;

        public TreeList tlControls
        {
            get
            {
                return tlMain;
            }
        }

        public string Log
        {
            get
            {
                return log.Text;
            }
            set
            {
                log.Text = value;
            }
        }

        public StructureView()
        {
            InitializeComponent();
#if DEBUG
            log.Visible = true;
#endif
        }
        
        private BaseControl GetDragObject(IDataObject data)
        {
            return (data.GetData(typeof(TreeListNode)) as TreeListNode).Tag as BaseControl;
        }

        private void StructureView_Load(object sender, EventArgs e)
        {
            tlControls.OptionsBehavior.DragNodes = true;
        }

        private void tlMain_DragOver(object sender, DragEventArgs e)
        {
            TreeListHitInfo hi = tlControls.CalcHitInfo(tlControls.PointToClient(new Point(e.X, e.Y)));
            TreeListNode node = GetDragNode(e.Data);
            if (node == null)
            {
                e.Effect = DragDropEffects.None;
                // log
                log.Text += "node null"+Environment.NewLine;
                return;
            }
            BaseControl dragObj = GetDragObject(e.Data);
            if (!dragObj.CanDrags)
            {
                e.Effect = DragDropEffects.None;
                // log
                log.Text += "drag object can't drags (" + dragObj.GetType().ToString() + ")" + Environment.NewLine;
                return;
            }
            if (hi.Node == null)
            {
                e.Effect = DragDropEffects.None;
                // log
                log.Text += "node is null" + Environment.NewLine;
                return;
            }
            if ((hi.Node.Tag as BaseControl).Parent is GridAddedPage)
            {
                // log
                log.Text += "parent is grid added page" + Environment.NewLine;
                e.Effect = DragDropEffects.None;
                return;
            }
            if (((hi.Node.Tag as BaseControl).CanContains(dragObj.GetType()) ||
                imageindex == 1 || imageindex == 2))
            {
                // log
                log.Text += string.Format("!!!!can contains imageindex={0}; node name='{1}';type='{2}'" + Environment.NewLine, imageindex, (hi.Node.Tag as BaseControl).Name, dragObj.GetType().ToString());
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                log.Text += string.Format("can not contains imageindex={0}; node name='{1}'" + Environment.NewLine, imageindex, (hi.Node.Tag as BaseControl).Name);
                e.Effect = DragDropEffects.None;
            }
           
        }

        private void SetDefaultCursor()
        {
            Cursor = Cursors.Default;
        }

        private TreeListNode GetDragNode(IDataObject data)
        {
            return data.GetData(typeof(TreeListNode)) as TreeListNode;
        }
                
        private void tlMain_DragDrop(object sender, DragEventArgs e)
        {
            
        }
                
        private void tlMain_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column == colName)
            {
                (e.Node.Tag as BaseControl).Name = (string)e.Value;
            }
            else if (e.Column == colCaption)
            {
                (e.Node.Tag as BaseControl).Caption = (string)e.Value;
            }
        }

        private void tlMain_AfterDragNode(object sender, NodeEventArgs e)
        {
            BaseControl control = e.Node.Tag as BaseControl;
            BaseControl parent = e.Node.ParentNode.Tag as BaseControl;
            int index = e.Node.ParentNode.Nodes.IndexOf(e.Node);
            (control).Move(parent, index);
        }
                       
        private void tlMain_CalcNodeDragImageIndex(object sender, CalcNodeDragImageIndexEventArgs e)
        {
            imageindex = e.ImageIndex;
            
        }
    }
}
