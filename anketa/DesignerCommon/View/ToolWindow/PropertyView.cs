﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using  System.Reflection;

namespace aetp.Common.View
{
    public partial class PropertyView : BaseToolWindow
    {
        public PropertyGrid PropertyGrid
        {
            get
            {
                return propertyGrid;
            }
        }       


        public PropertyView()
        {
            InitializeComponent();            
        }
        

        private void propertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            PropertyGridRefresh();            
        }

        private void propertyGrid_SelectedObjectsChanged(object sender, EventArgs e)
        {
            PropertyGridRefresh();
        }

        private void PropertyGridRefresh()
        {
            if (propertyGrid.SelectedObject != null)
            {
                PropertyDescriptor descriptor = TypeDescriptor.GetProperties(propertyGrid.SelectedObject.GetType())["EditValue"];

                BrowsableAttribute attribute = (BrowsableAttribute)
                                              descriptor.Attributes[typeof(BrowsableAttribute)];
                FieldInfo fieldToChange = attribute.GetType().GetField("browsable",
                                                 System.Reflection.BindingFlags.NonPublic |
                                                 System.Reflection.BindingFlags.Instance);
                fieldToChange.SetValue(attribute, false);
            }

            propertyGrid.Refresh();
        }
    }
}
