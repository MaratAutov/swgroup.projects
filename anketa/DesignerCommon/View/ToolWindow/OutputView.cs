﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTreeList;

namespace aetp.Common.View
{
    public partial class OutputView : BaseToolWindow
    {
        public string Output
        {
            get
            {
                return meCode.Text;
            }
            set
            {
                meCode.Text = value;
            }
        }

        public OutputView()
        {
            InitializeComponent();
        }
    }
}
