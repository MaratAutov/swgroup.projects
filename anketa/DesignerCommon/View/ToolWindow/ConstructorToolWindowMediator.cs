﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraTreeList;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using aetp.Common.View;
using aetp.Common.View.ToolWindow;

namespace aetp.Common.View
{
    public class ConstructorToolWindowMediator
    {
        Dictionary<ToolWindowType, BaseToolWindow> ToolWindows = new Dictionary<ToolWindowType, BaseToolWindow>();
        ConstructorView constructor;

        public TreeList tlControls
        {
            get
            {
                return (ToolWindows[ToolWindowType.structure] as StructureView).tlControls;
            }
        }

        public PropertyGrid PropertyGrid
        {
            get
            {
                return (ToolWindows[ToolWindowType.property] as PropertyView).PropertyGrid;
            }
        }
                
        public string CodeStream
        {
            get
            {
                return (ToolWindows[ToolWindowType.output] as OutputView).Output;
            }
            set
            {
                (ToolWindows[ToolWindowType.output] as OutputView).Output = value;
            }
        }

        public ConstructorToolWindowMediator(ConstructorView constructor)
        {
            this.constructor = constructor;

            StructureView structure = new StructureView();
            structure.MdiParent = constructor;
            ToolWindows.Add(ToolWindowType.structure, structure);
            structure.Closing += new EventHandler(structure_Closing);

            PropertyView property = new PropertyView();
            property.MdiParent = constructor;            
            ToolWindows.Add(ToolWindowType.property, property);
            property.Closing += new EventHandler(property_Closing);

            OutputView output = new OutputView();
            output.MdiParent = constructor;
            ToolWindows.Add(ToolWindowType.output, output);
            output.Closing += new EventHandler(output_Closing);

            constructor.bbiPropertyWindow.DownChanged += new DevExpress.XtraBars.ItemClickEventHandler(bbiPropertyWindow_DownChanged);
            constructor.bbiStructureWindow.DownChanged += new DevExpress.XtraBars.ItemClickEventHandler(bbiStructureWindow_DownChanged);
            constructor.bbiOutputWindow.DownChanged += new DevExpress.XtraBars.ItemClickEventHandler(bbiOutputWindow_DownChanged);
        }

        void bbiOutputWindow_DownChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (constructor.bbiOutputWindow.Down)
                (ToolWindows[ToolWindowType.output]).Show(constructor.dockPanel1, DockState.DockBottom);
            else
                (ToolWindows[ToolWindowType.output]).Close();
        }

        void bbiStructureWindow_DownChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (constructor.bbiStructureWindow.Down)
                (ToolWindows[ToolWindowType.structure]).Show(constructor.dockPanel1, DockState.DockLeft);
            else
                (ToolWindows[ToolWindowType.structure]).Close();
        }

        void bbiPropertyWindow_DownChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (constructor.bbiPropertyWindow.Down)
                (ToolWindows[ToolWindowType.property]).Show(constructor.dockPanel1, DockState.DockRight);
            else
                (ToolWindows[ToolWindowType.property]).Close();
        }

        public void Initial()
        {
            (ToolWindows[ToolWindowType.structure]).Show(constructor.dockPanel1, DockState.DockLeft);
            (ToolWindows[ToolWindowType.property]).Show(constructor.dockPanel1, DockState.DockRight);
            (ToolWindows[ToolWindowType.output]).Show(constructor.dockPanel1, DockState.DockBottom);
        }

        void output_Closing(object sender, EventArgs e)
        {
            constructor.bbiOutputWindow.Down = false;
        }

        void property_Closing(object sender, EventArgs e)
        {
            constructor.bbiPropertyWindow.Down = false;
        }

        void structure_Closing(object sender, EventArgs e)
        {
            constructor.bbiStructureWindow.Down = false;
        }
    }
}
