﻿namespace aetp.Common.View
{
    partial class StructureView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StructureView));
            this.tlMain = new DevExpress.XtraTreeList.TreeList();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colCaption = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.ilElement = new System.Windows.Forms.ImageList(this.components);
            this.log = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.tlMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.log.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tlMain
            // 
            this.tlMain.AllowDrop = true;
            this.tlMain.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName,
            this.colCaption});
            this.tlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlMain.Location = new System.Drawing.Point(0, 0);
            this.tlMain.Name = "tlMain";
            this.tlMain.Size = new System.Drawing.Size(369, 286);
            this.tlMain.StateImageList = this.ilElement;
            this.tlMain.TabIndex = 8;
            this.tlMain.AfterDragNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tlMain_AfterDragNode);
            this.tlMain.CalcNodeDragImageIndex += new DevExpress.XtraTreeList.CalcNodeDragImageIndexEventHandler(this.tlMain_CalcNodeDragImageIndex);
            this.tlMain.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.tlMain_CellValueChanged);
            this.tlMain.DragDrop += new System.Windows.Forms.DragEventHandler(this.tlMain_DragDrop);
            this.tlMain.DragOver += new System.Windows.Forms.DragEventHandler(this.tlMain_DragOver);
            // 
            // colName
            // 
            this.colName.Caption = "Имя";
            this.colName.FieldName = "Name";
            this.colName.MinWidth = 49;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowSort = false;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colCaption
            // 
            this.colCaption.Caption = "Заголовок";
            this.colCaption.FieldName = "Caption";
            this.colCaption.Name = "colCaption";
            this.colCaption.OptionsColumn.AllowSort = false;
            this.colCaption.Visible = true;
            this.colCaption.VisibleIndex = 1;
            // 
            // ilElement
            // 
            this.ilElement.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilElement.ImageStream")));
            this.ilElement.TransparentColor = System.Drawing.Color.Transparent;
            this.ilElement.Images.SetKeyName(0, "16_лист.png");
            this.ilElement.Images.SetKeyName(1, "16_элемент-редактирования.png");
            this.ilElement.Images.SetKeyName(2, "16-выпадающий-список.png");
            this.ilElement.Images.SetKeyName(3, "16_переход-на-лист.png");
            this.ilElement.Images.SetKeyName(4, "16_словарь.png");
            this.ilElement.Images.SetKeyName(5, "16_добавить-таблицу.png");
            this.ilElement.Images.SetKeyName(6, "16_таблица-с-добавлением-строк.png");
            this.ilElement.Images.SetKeyName(7, "16_таблица-с-добавлением-листов.png");
            this.ilElement.Images.SetKeyName(8, "16_мета_контрол.png");
            this.ilElement.Images.SetKeyName(9, "16_дата.png");
            this.ilElement.Images.SetKeyName(10, "16_дробное-число.png");
            this.ilElement.Images.SetKeyName(11, "16_текстовое-поле.png");
            this.ilElement.Images.SetKeyName(12, "16_целое-число.png");
            this.ilElement.Images.SetKeyName(13, "16_чекбокс.png");
            this.ilElement.Images.SetKeyName(14, "16_подпись.png");
            // 
            // log
            // 
            this.log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.log.Location = new System.Drawing.Point(0, 286);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(369, 271);
            this.log.TabIndex = 9;
            this.log.Visible = false;
            // 
            // StructureView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 557);
            this.Controls.Add(this.tlMain);
            this.Controls.Add(this.log);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "StructureView";
            this.Text = "Структура документа";
            this.Load += new System.EventHandler(this.StructureView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tlMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.log.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList tlMain;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCaption;
        private System.Windows.Forms.ImageList ilElement;
        private DevExpress.XtraEditors.MemoEdit log;
    }
}