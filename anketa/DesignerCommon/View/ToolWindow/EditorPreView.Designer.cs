﻿namespace aetp.Common.View
{
    partial class EditorPreView
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorPreView));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bbiSaveAs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOrganizationData = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLoadFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAttachFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiValidate = new DevExpress.XtraBars.BarButtonItem();
            this.rpAction = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgAction = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tlMain = new DevExpress.XtraTreeList.TreeList();
            this.colCaption = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.contentPanel = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnValidatePage = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblPageCaption = new DevExpress.XtraEditors.LabelControl();
            this.btnClearPage = new DevExpress.XtraEditors.SimpleButton();
            this.btnCreatePage = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tlMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "page_white.png");
            this.imageList1.Images.SetKeyName(1, "text_editor.png");
            this.imageList1.Images.SetKeyName(2, "grid.png");
            this.imageList1.Images.SetKeyName(3, "ui_combo_box_blue.png");
            this.imageList1.Images.SetKeyName(4, "stock_form_combobox.png");
            this.imageList1.Images.SetKeyName(5, "hyperlink_blue (1).png");
            this.imageList1.Images.SetKeyName(6, "accessories_dictionary (1).png");
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonText = null;
            this.ribbon.ApplicationIcon = global::aetp.Common.Properties.Resources.anketa;
            // 
            // 
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.ExpandCollapseItem.Name = "";
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.bbiSaveAs,
            this.bbiRefresh,
            this.bbiSave,
            this.bbiOrganizationData,
            this.bbiLoadFile,
            this.bbiValidate});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 13;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpAction});
            this.ribbon.Size = new System.Drawing.Size(1136, 145);
            // 
            // bbiSaveAs
            // 
            this.bbiSaveAs.Caption = "Сохранить Как...";
            this.bbiSaveAs.Glyph = global::aetp.Common.Properties.Resources._32_сохранить;
            this.bbiSaveAs.Id = 4;
            this.bbiSaveAs.Name = "bbiSaveAs";
            this.bbiSaveAs.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSaveAs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveAs_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Glyph = global::aetp.Common.Properties.Resources.refresh_model;
            this.bbiRefresh.Id = 15;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Glyph = global::aetp.Common.Properties.Resources._32_сохранить;
            this.bbiSave.Id = 40;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;

            // 
            // bbiOrganizationData
            // 
            this.bbiOrganizationData.Caption = "Реквизиты организации";
            this.bbiOrganizationData.Glyph = global::aetp.Common.Properties.Resources._32_сохранить;
            this.bbiOrganizationData.Id = 20;
            this.bbiOrganizationData.Name = "bbiOrganizationData";
            this.bbiOrganizationData.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiOrganizationData.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiLoadFile
            // 
            this.bbiLoadFile.Caption = "Открыть...";
            this.bbiLoadFile.Glyph = global::aetp.Common.Properties.Resources._32_открыть;
            this.bbiLoadFile.Id = 5;
            this.bbiLoadFile.Name = "bbiLoadFile";
            this.bbiLoadFile.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiLoadFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadFile_ItemClick);
            // 
            // bbiValidate
            // 
            this.bbiValidate.Caption = "Проверить ошибки";
            this.bbiValidate.Id = 12;
            this.bbiValidate.LargeGlyph = global::aetp.Common.Properties.Resources.warning;
            this.bbiValidate.Name = "bbiValidate";
            this.bbiValidate.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiValidate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiValidate_ItemClick);
            // 
            // rpAction
            // 
            this.rpAction.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgAction});
            this.rpAction.Name = "rpAction";
            this.rpAction.Text = "Главная";
            // 
            // rpgAction
            // 
            this.rpgAction.ItemLinks.Add(this.bbiSaveAs);
            this.rpgAction.ItemLinks.Add(this.bbiSave);
            this.rpgAction.ItemLinks.Add(this.bbiValidate);
            this.rpgAction.ItemLinks.Add(this.bbiRefresh);
            this.rpgAction.ItemLinks.Add(this.bbiOrganizationData);
            this.rpgAction.Name = "rpgAction";
            this.rpgAction.Text = "Action";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 145);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.tlMain);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.contentPanel);
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1136, 556);
            this.splitContainerControl1.SplitterPosition = 234;
            this.splitContainerControl1.TabIndex = 15;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // tlMain
            // 
            this.tlMain.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colCaption});
            this.tlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlMain.Location = new System.Drawing.Point(0, 0);
            this.tlMain.Name = "tlMain";
            this.tlMain.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.tlMain.Size = new System.Drawing.Size(234, 556);
            this.tlMain.StateImageList = this.imageList1;
            this.tlMain.TabIndex = 9;
            // 
            // colCaption
            // 
            this.colCaption.Caption = "Наименование";
            this.colCaption.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colCaption.FieldName = "Caption";
            this.colCaption.MinWidth = 33;
            this.colCaption.Name = "colCaption";
            this.colCaption.Visible = true;
            this.colCaption.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // contentPanel
            // 
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(0, 65);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(897, 491);
            this.contentPanel.TabIndex = 15;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnValidatePage);
            this.panelControl1.Controls.Add(this.pictureBox1);
            this.panelControl1.Controls.Add(this.lblPageCaption);
            this.panelControl1.Controls.Add(this.btnClearPage);
            this.panelControl1.Controls.Add(this.btnCreatePage);
            //this.panelControl1.Controls.Add(this.bbiAttachFile);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(897, 65);
            this.panelControl1.TabIndex = 12;
            // 
            // btnValidatePage
            // 
            this.btnValidatePage.Location = new System.Drawing.Point(236, 37);
            this.btnValidatePage.Name = "btnValidatePage";
            this.btnValidatePage.Size = new System.Drawing.Size(75, 23);
            this.btnValidatePage.TabIndex = 4;
            this.btnValidatePage.Text = "Проверить";
            this.btnValidatePage.Click += new System.EventHandler(this.btnValidatePage_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(20, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(33, 32);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // lblPageCaption
            // 
            this.lblPageCaption.Location = new System.Drawing.Point(74, 7);
            this.lblPageCaption.Name = "lblPageCaption";
            this.lblPageCaption.Size = new System.Drawing.Size(0, 13);
            this.lblPageCaption.TabIndex = 2;
            // 
            // btnClearPage
            // 
            this.btnClearPage.Enabled = false;
            this.btnClearPage.Location = new System.Drawing.Point(155, 36);
            this.btnClearPage.Name = "btnClearPage";
            this.btnClearPage.Size = new System.Drawing.Size(75, 23);
            this.btnClearPage.TabIndex = 1;
            this.btnClearPage.Text = "Очистить";
            this.btnClearPage.Click += new System.EventHandler(this.btnClearPage_Click_1);
            // 
            // btnCreatePage
            // 
            this.btnCreatePage.Enabled = false;
            this.btnCreatePage.Location = new System.Drawing.Point(74, 36);
            this.btnCreatePage.Name = "btnCreatePage";
            this.btnCreatePage.Size = new System.Drawing.Size(75, 23);
            this.btnCreatePage.TabIndex = 0;
            this.btnCreatePage.Text = "Создать";
            this.btnCreatePage.Click += new System.EventHandler(this.btnCreatePage_Click_1);
            // 
            // bbiAttachFile
            // 
            this.bbiAttachFile.Caption = "Прикрепить файл";
            this.bbiAttachFile.Id = 31;
            this.bbiAttachFile.LargeGlyph = global::aetp.Common.Properties.Resources.attach;
            this.bbiAttachFile.Name = "bbiAttachFile";
            this.bbiAttachFile.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAttachFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAttachFile_ItemClick);
            // 
            // EditorPreView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1136, 701);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.ribbon);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditorPreView";
            this.Text = "";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EditorPreView_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tlMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpAction;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgAction;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAs;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiOrganizationData;
        private DevExpress.XtraBars.BarButtonItem bbiLoadFile;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarButtonItem bbiValidate;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        public DevExpress.XtraTreeList.TreeList tlMain;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCaption;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.XtraScrollableControl contentPanel;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnValidatePage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.LabelControl lblPageCaption;
        private DevExpress.XtraEditors.SimpleButton btnClearPage;
        private DevExpress.XtraEditors.SimpleButton btnCreatePage;
        private DevExpress.XtraBars.BarButtonItem bbiAttachFile;
    }
}

