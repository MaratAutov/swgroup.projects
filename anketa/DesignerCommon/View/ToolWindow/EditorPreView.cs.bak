﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using aetp.Common.ControlModel;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraTreeList.Nodes.Operations;
using DevExpress.XtraTreeList.Nodes;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Windows;
using DevExpress.XtraTreeList;
using aetp.Common.Mediator;
using DevExpress.XtraEditors;
using aetp.Common.Control;
using DevExpress.XtraGrid;
using System.Diagnostics;
using aetp.Common.Validator;
using System.Reflection;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Collections.ObjectModel;

namespace aetp.Common.View
{
    public partial class EditorPreView : ContentWindow, IEditorWindow, ITreeListWindow
    {
        EditorModel model;

        public event PageViewInfoCollectionChangeEventHandler PageVisibleChanged;
        protected void RaisePageVisibleChanged(PageViewInfoCollectionChangeEventArgs e)
        {
            if (PageVisibleChanged != null)
                PageVisibleChanged(this, e);
        }

        public string title
        {
            get
            {
                return "Программа-анкета подготовки электронных документов версии 2.16.3";
            }
        }

        public TreeListNode GetFocusedTreeListNode()
        {
            return tlControls.FocusedNode;
        }

        public aetp.Common.ControlModel.BaseControl GetFocusedControl()
        {
            TreeListNode node = GetFocusedTreeListNode();
            if (node != null)
                return (aetp.Common.ControlModel.BaseControl)node.Tag;
            else
                return null;
        }

        public TreeList tlControls
        {
            get
            {
                return tlMain;
            }
            set
            {
                tlMain = value;
            }
        }

        public string FileName
        {
            get;
            set;
        }

        public DevExpress.XtraBars.BarButtonItem AttachFileButton
        {
            get
            {
                return bbiAttachFile;
            }
            set
            {
                bbiAttachFile = value;
            }
        }

        public string Title
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        public DevExpress.XtraEditors.PanelControl PanelFile
        {
            get;
            set;
        } 

        public DevExpress.XtraBars.BarButtonItem SaveButton
        {
            get
            {
                return bbiSave;
            }
            set
            {
                bbiSave = value;
            }
        }

        public DevExpress.XtraEditors.SplitContainerControl splitterControl
        {
            get;
            set;

        } 

        public string PageCaption
        {
            get
            {
                return lblPageCaption.Text;
            }
            set
            {
                lblPageCaption.Text = value;
            }
        }

        public SimpleButton CreatePageButton
        {
            get
            {
                return btnCreatePage;
            }
        }

        public SimpleButton ClearPageButton
        {
            get
            {
                return btnClearPage;
            }
        }

        SimpleButton _sectionLoadButton = new SimpleButton();
        public SimpleButton LoadSectionButton
        {
            get
            {
                return _sectionLoadButton;
            }
        }

        SimpleButton _sectionSaveButton = new SimpleButton();
        public SimpleButton SaveSectionButton
        {
            get
            {
                return _sectionSaveButton;
            }
        }

        public XtraScrollableControl ContentPanel
        {
            get
            {
                return contentPanel;
            }
        }

        public Report Report
        {
            get
            {
                return model.Report;
            }
            set
            {
                model.Report = value;
            }
        }

        public bool IsLoadPage(Page page)
        {
            return model.IsLoadPage(page);
        }

        public void ReInit()
        {
            model.ReInit();
        }

        public void ShowFirstPage()
        {
            model.ShowFirstPage();
        }

        public ScrollableControl GetPage(Page page)
        {
            return model.GetPage(page);
        }
                
        #region event      
        

        public event EventHandler AddSign;
        protected void RaiseAddSign(EventArgs e)
        {
            if (AddSign != null)
                AddSign(this, e);
        }

        public event EventHandler ConvertPacket;
        protected void RaiseConvertPacket()
        {
            if (ConvertPacket != null)
                ConvertPacket(this, null);
        }

        public event EventHandler OpenModels;
        protected void RaiseOpenModels(EventArgs e)
        {
            if (OpenModels != null)
                OpenModels(this, e);
        }

        public event EventHandler TreeListChanged;
        protected void RaiseTreeListChanged(EventArgs e)
        {
            if (TreeListChanged != null)
                TreeListChanged(this, e);
        }

        public event EventHandler AboutProgram;
        protected void RaiseAboutProgram()
        {
            if (AboutProgram != null)
                AboutProgram(this, null);
        }

        public event EventHandler NetworkSettings;
        protected void RaiseNetworkSettings()
        {
            if (NetworkSettings != null)
                NetworkSettings(this, null);
        }

        public event EventHandler ExportToPdf;
        protected void RaiseExportToPdf()
        {
            if (ExportToPdf != null)
                ExportToPdf(this, null);
        }

        public event EventHandler ExportXtddExcel;
        protected void RaiseExportXtddExcel()
        {
            if (ExportXtddExcel != null)
                ExportXtddExcel(this, null);
        }

        public event EventHandler ExportToExcel;
        protected void RaiseExportToExcel()
        {
            if (ExportToExcel != null)
                ExportToExcel(this, null);
        }

        public event EventHandler ExportToRtf;
        protected void RaiseExportToRtf()
        {
            if (ExportToRtf != null)
                ExportToRtf(this, null);
        }

        public event EventHandler EditValueChanged;
        protected void RaiseEditValueChanged(EventArgs e)
        {
            if (EditValueChanged != null)
                EditValueChanged(this, e);
        }

        public event CloseWindowEventHandler CloseWindow;
        protected void RaiseCloseWindow()
        {
            if (CloseWindow != null)
                CloseWindow(this, new CloseWindowEventArgs() { report = Report });
        }

        public event EventHandler CreateNew;
        protected void RaiseCreateNew()
        {
            if (CreateNew != null)
                CreateNew(this, null);
        }

        public event EventHandler LoadSection;
        protected void RaiseLoadSection()
        {
            if (LoadSection != null)
                LoadSection(this, null);
        }

        public event EventHandler SaveSection;
        protected void RaiseSaveSection()
        {
            if (SaveSection != null)
                SaveSection(this, null);
        }

        public event EventHandler Save;
        protected void RaiseSave()
        {
            if (Save != null)
                Save(this, null);
        }


        public event EventHandler SaveAs;
        protected void RaiseSaveAs()
        {
            if (SaveAs != null)
                SaveAs(this, null);
        }

        public event EventHandler OrganizationData;
        protected void RaiseOrganizationData()
        {
            if (OrganizationData != null)
                OrganizationData(this, null);
        }

        public event EventHandler SavePacketAs;
        protected void RaiseSavePacketAs()
        {
            if (SavePacketAs != null)
                SavePacketAs(this, null);
        }

        public event EventHandler SendPacket;
        protected void RaiseSendPacket()
        {
            if (SendPacket != null)
                SendPacket(this, null);
        }

		public event EventHandler PrintPacket;
		protected void RaisePrintPacket()
		{
			if (PrintPacket != null)
				PrintPacket(this, null);
		}

        public new event EventHandler Load;
        protected void RaiseLoad()
        {
            if (Load != null)
                Load(this, null);
        }

        public event EventHandler RefreshPage;
        protected void RaiseRefresh()
        {
            if (RefreshPage != null)
                RefreshPage(this, null);
        }

        public event EventHandler LoadFile;
        protected void RaiseLoadFile()
        {
            if (LoadFile != null)
                LoadFile(this, null);
        }

        public event EventHandler DocumentsSubmitterOpen;
        protected void RaiseDocumentsSubmitterOpen()
        {
            if (DocumentsSubmitterOpen != null)
                DocumentsSubmitterOpen(this, null);
        }

        public new event ValidatingEventHandler Validating;
        protected void RaiseValidating(Page page, bool isRecurently)
        {
            if (Validating != null)
                Validating(this, new ValidatingEventArgs() { Page = page, IsRecurently = isRecurently });
        }
        public event EventHandler AttachFile;
        protected void RaiseAttachFile()
        {
            if (AttachFile != null)
                AttachFile(this, null);
        }

        #endregion

        public EditorPreView()
        {
            InitializeComponent();
            model = new EditorModel(this);
            contentPanel.AutoScrollPosition = new Point(0, 0);
        }
     
        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            aetp.Common.ControlModel.BaseControl control = GetFocusedControl();
            if (control is Page)
            {
                model.ShowPage(control as Page);
            }
        }
                        
        private void bbiSaveAs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseSaveAs();
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseRefresh();
        }

        private void bbiLoadFile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
                       
            RaiseLoadFile();
        }

        private void bbiAttachFile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseAttachFile();
        }

        private void btnCreatePage_Click_1(object sender, EventArgs e)
        {
            btnCreatePage.Enabled = false;
            model.CurrentPage.InitialDefaultValues();
            model.ShowPage(model.CurrentPage, true);
            PageViewInfo pvi = new PageViewInfo();
            pvi.page = model.CurrentPage;
            RaisePageVisibleChanged(new PageViewInfoCollectionChangeEventArgs() { page = pvi, type = ChangeType.Add });
            btnClearPage.Enabled = true;
        }

        private void btnClearPage_Click_1(object sender, EventArgs e)
        {
            btnClearPage.Enabled = false;
            model.ClearCurrentPage();
            PageViewInfo pvi = new PageViewInfo();
            pvi.page = model.CurrentPage;
            RaisePageVisibleChanged(new PageViewInfoCollectionChangeEventArgs() { page = pvi, type = ChangeType.Remove });
            btnCreatePage.Enabled = true;
        }

        private void bbiValidate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RaiseValidating(null, true);
        }

        private void btnValidatePage_Click(object sender, EventArgs e)
        {
            RaiseValidating(model.CurrentPage, false);
        }

        private void EditorPreView_FormClosed(object sender, FormClosedEventArgs e)
        {
            RaiseCloseWindow();
        }

    }
    
    
}
