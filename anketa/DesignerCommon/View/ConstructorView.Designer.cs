﻿namespace aetp.Common.View
{
    partial class ConstructorView
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConstructorView));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bbiAddPage = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddSimpleEditor = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRemoveItem = new DevExpress.XtraBars.BarButtonItem();
            this.btnAddGrid = new DevExpress.XtraBars.BarButtonItem();
            this.AddLink = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddComboBox = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddGridAddedRow = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddGridAddedPage = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddMetaGrid = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddDictionary = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLoadFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRunEditor = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDublicateSection = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTools = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPropertyWindow = new DevExpress.XtraBars.BarButtonItem();
            this.bbiStructureWindow = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOutputWindow = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOpenEditor = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddMetaControl = new DevExpress.XtraBars.BarButtonItem();
            this.btnXSDGenerate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiImport = new DevExpress.XtraBars.BarButtonItem();            
            this.rpMain = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgFile = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpg_action = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpg_xsd = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpWindow = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgWindow = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dockPanel1 = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.btn_table = new DevExpress.XtraBars.BarSubItem();
            this.bci_table = new DevExpress.XtraBars.BarCheckItem();
            this.bci_tableAddRow = new DevExpress.XtraBars.BarCheckItem();
            this.bci_tableAddPage = new DevExpress.XtraBars.BarCheckItem();
            this.bci_Metatable = new DevExpress.XtraBars.BarCheckItem();

            this.btn_edit = new DevExpress.XtraBars.BarSubItem();
            this.bci_simpleEdit = new DevExpress.XtraBars.BarCheckItem();
            this.bci_RichEdit = new DevExpress.XtraBars.BarCheckItem();
            this.bci_dateEdit = new DevExpress.XtraBars.BarCheckItem();
            this.bci_IntEdit = new DevExpress.XtraBars.BarCheckItem();
            this.bci_FloatEdit = new DevExpress.XtraBars.BarCheckItem();
            this.bci_CheckEdit = new DevExpress.XtraBars.BarCheckItem();
            this.bci_Label = new DevExpress.XtraBars.BarCheckItem();
            this.bci_MetaControl = new DevExpress.XtraBars.BarCheckItem();
            this.bci_MemoEdit = new DevExpress.XtraBars.BarCheckItem();

            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            this.SuspendLayout();          


            this.bci_table.Caption = "Фиксированная таблица";
            this.bci_table.Name = "bci_table";
            this.bci_table.Tag = 1;
            this.bci_table.Glyph = global::aetp.Common.Properties.Resources._32_добавить_таблицу;
            this.bci_table.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddGrid_ItemClick);

            this.bci_tableAddRow.Caption = "Таблица с добавлением строк";
            this.bci_tableAddRow.Name = "bci_tableAddRow";
            this.bci_tableAddRow.Tag = 2;
            this.bci_tableAddRow.Glyph = global::aetp.Common.Properties.Resources._32_таблица_с_добавлением_строк;
            this.bci_tableAddRow.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddGridAddedRow_ItemClick);

            this.bci_tableAddPage.Caption = "Таблица с добавлением листов";
            this.bci_tableAddPage.Name = "bci_tableAddPage";
            this.bci_tableAddPage.Tag = 3;
            this.bci_tableAddPage.Glyph = global::aetp.Common.Properties.Resources._32_таблица_с_добавлением_листов;
            this.bci_tableAddPage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddGridAddedPage_ItemClick);

            this.bci_Metatable.Caption = "МетаТаблица";
            this.bci_Metatable.Name = "bci_Metatable";
            this.bci_Metatable.Tag = 3;
            this.bci_Metatable.Glyph = global::aetp.Common.Properties.Resources.table_star;
            this.bci_Metatable.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddMetaGrid_ItemClick);
            

            this.btn_table.Caption = "Таблица";
            this.btn_table.Id = 55;
            this.btn_table.LargeGlyph = global::aetp.Common.Properties.Resources._32_добавить_таблицу;
            this.btn_table.Name = "btn_table";
            this.btn_table.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.btn_table.AddItem(bci_table);
            this.btn_table.AddItem(bci_tableAddRow);
            this.btn_table.AddItem(bci_tableAddPage);            
            this.btn_table.AddItem(bci_Metatable);
            


            this.bci_simpleEdit.Caption = "Текстовое поле";
            this.bci_simpleEdit.Name = "bci_simpleEdit";
            this.bci_simpleEdit.Tag = 1;
            this.bci_simpleEdit.Glyph = global::aetp.Common.Properties.Resources._32_текстовое_поле;
            this.bci_simpleEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddSimpleEdit_ItemClick);

            this.bci_RichEdit.Caption = "Поле с форматированием";
            this.bci_RichEdit.Name = "bci_RichEdit";
            this.bci_RichEdit.Tag = 8;
            this.bci_RichEdit.Glyph = global::aetp.Common.Properties.Resources._32_текстовое_поле;
            this.bci_RichEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddRichEdit_ItemClick);

            this.bci_dateEdit.Caption = "Дата";
            this.bci_dateEdit.Name = "bci_dateEdit";
            this.bci_dateEdit.Tag = 2;
            this.bci_dateEdit.Glyph = global::aetp.Common.Properties.Resources._32_дата;
            this.bci_dateEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddDateEdit_ItemClick);

            this.bci_IntEdit.Caption = "Целое число";
            this.bci_IntEdit.Name = "bci_IntEdit";
            this.bci_IntEdit.Tag = 3;
            this.bci_IntEdit.Glyph = global::aetp.Common.Properties.Resources._32_целое_число;
            this.bci_IntEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddIntEdit_ItemClick);

            this.bci_FloatEdit.Caption = "Число с плавающей точкой";
            this.bci_FloatEdit.Name = "bci_FloatEdit";
            this.bci_FloatEdit.Tag = 4;
            this.bci_FloatEdit.Glyph = global::aetp.Common.Properties.Resources._32_дробное_число;
            this.bci_FloatEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddFloatEdit_ItemClick);

            this.bci_CheckEdit.Caption = "Чекбокс";
            this.bci_CheckEdit.Name = "bci_CheckEdit";
            this.bci_CheckEdit.Tag = 5;
            this.bci_CheckEdit.Glyph = global::aetp.Common.Properties.Resources._32_чекбокс;
            this.bci_CheckEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddCheckEdit_ItemClick);

            this.bci_Label.Caption = "Метка";
            this.bci_Label.Name = "bci_Label";
            this.bci_Label.Tag = 8;
            this.bci_Label.Glyph = global::aetp.Common.Properties.Resources._32_label;
            this.bci_Label.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddLabel_ItemClick);

            this.bci_MetaControl.Caption = "Мета Элемент";
            this.bci_MetaControl.Name = "bci_MetaControl";
            this.bci_MetaControl.Tag = 6;
            this.bci_MetaControl.Glyph = global::aetp.Common.Properties.Resources.document_star;
            this.bci_MetaControl.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddMetaControl_ItemClick);

            this.bci_MemoEdit.Caption = "Многострочное текстовое поле";
            this.bci_MemoEdit.Name = "bci_MemoEdit";
            this.bci_MemoEdit.Tag = 7;
            this.bci_MemoEdit.Glyph = global::aetp.Common.Properties.Resources._32_текстовое_поле;
            this.bci_MemoEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddMemoEdit_ItemClick);
            this.bci_MemoEdit.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;


            this.btn_edit.Caption = "Элемент редактирования";
            this.btn_edit.Id = 60;
            this.btn_edit.LargeGlyph = global::aetp.Common.Properties.Resources._32_элемент_редактирования;
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.btn_edit.AddItem(bci_Label);
            this.btn_edit.AddItem(bci_simpleEdit);
            this.btn_edit.AddItem(bci_MemoEdit);
            this.btn_edit.AddItem(bci_RichEdit);
            this.btn_edit.AddItem(bci_dateEdit);
            this.btn_edit.AddItem(bci_IntEdit);
            this.btn_edit.AddItem(bci_FloatEdit);
            this.btn_edit.AddItem(bci_CheckEdit);
            this.btn_edit.AddItem(bci_MetaControl);
            

            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonText = null;
            this.ribbon.ApplicationIcon = global::aetp.Common.Properties.Resources.anketa;
            // 
            // 
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.ExpandCollapseItem.Name = "";
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.bbiAddPage,
            this.bci_Metatable,
            this.bci_table,
            this.bci_tableAddPage,
            this.bci_tableAddRow,
            this.btn_edit,
            this.bci_dateEdit,
            this.bci_FloatEdit,
            this.bci_CheckEdit,
            this.bci_Label,
            this.bci_IntEdit,
            this.bci_MetaControl,
            this.bci_MemoEdit,
            this.bci_simpleEdit,
            this.bci_RichEdit,
            this.bbiAddSimpleEditor,
            this.bbiRemoveItem,
            this.btnAddGrid,
            this.btn_table,
            this.AddLink,
            this.bbiAddComboBox,
            this.bbiAddGridAddedRow,
            this.bbiAddGridAddedPage,
            this.bbiAddMetaGrid,
            this.bbiAddDictionary,
            this.bbiSaveAs,
            this.bbiSave,
            this.bbiLoadFile,
            this.bbiRunEditor,
            this.bbiDublicateSection,
            this.bbiTools,
            this.bbiPropertyWindow,
            this.bbiStructureWindow,
            this.bbiOutputWindow,
            this.bbiNew,
            this.bbiOpenEditor,
            this.bbiAddMetaControl,
            this.btnXSDGenerate,
            this.bbiImport});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 29;
            this.ribbon.Name = "ribbon";
            
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpMain,
            this.rpWindow});
            this.ribbon.Toolbar.ItemLinks.Add(this.bbiNew);
            this.ribbon.Toolbar.ItemLinks.Add(this.bbiSaveAs);
            this.ribbon.Toolbar.ItemLinks.Add(this.bbiLoadFile);
            this.ribbon.Size = new System.Drawing.Size(1505, 144);
            // 
            // bbiAddPage
            // 
            this.bbiAddPage.Caption = "Лист";
            this.bbiAddPage.Glyph = global::aetp.Common.Properties.Resources._32_лист;
            this.bbiAddPage.Id = 1;
            this.bbiAddPage.Name = "bbiAddPage";
            this.bbiAddPage.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddPage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPage_ItemClick);
            // 
            // bbiAddSimpleEditor
            // 
            this.bbiAddSimpleEditor.Caption = "Элемент редактирования";
            this.bbiAddSimpleEditor.Glyph = global::aetp.Common.Properties.Resources._32_элемент_редактирования;
            this.bbiAddSimpleEditor.Id = 2;
            this.bbiAddSimpleEditor.Name = "bbiAddSimpleEditor";
            this.bbiAddSimpleEditor.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddSimpleEditor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddSimpleEditor_ItemClick);
            this.bbiAddSimpleEditor.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiRemoveItem
            // 
            this.bbiRemoveItem.Caption = "Удалить";
            this.bbiRemoveItem.Glyph = global::aetp.Common.Properties.Resources._32_удалить;
            this.bbiRemoveItem.Id = 5;
            this.bbiRemoveItem.Name = "bbiRemoveItem";
            this.bbiRemoveItem.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRemoveItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRemoveItem_ItemClick);
            // 
            // btnAddGrid
            // 
            this.btnAddGrid.Caption = "Добавить фиксированную Таблицу";
            this.btnAddGrid.Glyph = global::aetp.Common.Properties.Resources._32_добавить_таблицу;
            this.btnAddGrid.Id = 6;
            this.btnAddGrid.Name = "btnAddGrid";
            this.btnAddGrid.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.btnAddGrid.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddGrid_ItemClick);
            this.btnAddGrid.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // AddLink
            // 
            this.AddLink.Caption = "Переход на лист";
            this.AddLink.Glyph = global::aetp.Common.Properties.Resources._32_переход_на_лист;
            this.AddLink.Id = 8;
            this.AddLink.Name = "AddLink";
            this.AddLink.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.AddLink.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AddLink_ItemClick);
            // 
            // bbiAddComboBox
            // 
            this.bbiAddComboBox.Caption = "Выпадающий список";
            this.bbiAddComboBox.Glyph = global::aetp.Common.Properties.Resources._32_выпадающий_список;
            this.bbiAddComboBox.Id = 9;
            this.bbiAddComboBox.Name = "bbiAddComboBox";
            this.bbiAddComboBox.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddComboBox.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddComboBox_ItemClick);
            // 
            // bbiAddGridAddedRow
            // 
            this.bbiAddGridAddedRow.Caption = "Таблица с добавлением строк";
            this.bbiAddGridAddedRow.Glyph = global::aetp.Common.Properties.Resources._32_таблица_с_добавлением_строк;
            this.bbiAddGridAddedRow.Id = 10;
            this.bbiAddGridAddedRow.Name = "bbiAddGridAddedRow";
            this.bbiAddGridAddedRow.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddGridAddedRow.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddGridAddedRow_ItemClick);
            this.bbiAddGridAddedRow.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiAddGridAddedPage
            // 
            this.bbiAddGridAddedPage.Caption = "Таблица с добавлением листов";
            this.bbiAddGridAddedPage.Glyph = global::aetp.Common.Properties.Resources._32_таблица_с_добавлением_листов;
            this.bbiAddGridAddedPage.Id = 11;
            this.bbiAddGridAddedPage.Name = "bbiAddGridAddedPage";
            this.bbiAddGridAddedPage.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddGridAddedPage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddGridAddedPage_ItemClick);
            this.bbiAddGridAddedPage.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiAddMetaGrid
            // 
            this.bbiAddMetaGrid.Caption = "Мета Таблица";
            this.bbiAddMetaGrid.Glyph = global::aetp.Common.Properties.Resources.table_star;
            this.bbiAddMetaGrid.Id = 10;
            this.bbiAddMetaGrid.Name = "bbiAddMetaGrid";
            this.bbiAddMetaGrid.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddMetaGrid.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddMetaGrid_ItemClick);
            this.bbiAddMetaGrid.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            
            // 
            // bbiAddDictionary
            // 
            this.bbiAddDictionary.Caption = "Словарь";
            this.bbiAddDictionary.Glyph = global::aetp.Common.Properties.Resources._32_словарь;
            this.bbiAddDictionary.Id = 12;
            this.bbiAddDictionary.Name = "bbiAddDictionary";
            this.bbiAddDictionary.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddDictionary.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddDictionary_ItemClick);
            // 
            // bbiSaveAs
            // 
            this.bbiSaveAs.Caption = "Сохранить Как...";
            this.bbiSaveAs.Glyph = global::aetp.Common.Properties.Resources._16_сохранить;
            this.bbiSaveAs.Id = 14;
            this.bbiSaveAs.LargeGlyph = global::aetp.Common.Properties.Resources._32_сохранить;
            this.bbiSaveAs.Name = "bbiSaveAs";
            this.bbiSaveAs.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSaveAs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveAs_ItemClick);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Enabled = false;
            this.bbiSave.Glyph = global::aetp.Common.Properties.Resources._16_сохранить;
            this.bbiSave.Id = 30;
            this.bbiSave.LargeGlyph = global::aetp.Common.Properties.Resources._32_сохранить;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);            
            // 
            // bbiLoadFile
            // 
            this.bbiLoadFile.Caption = "Открыть";
            this.bbiLoadFile.Glyph = global::aetp.Common.Properties.Resources._16_открыть;
            this.bbiLoadFile.Id = 15;
            this.bbiLoadFile.LargeGlyph = global::aetp.Common.Properties.Resources._32_открыть;
            this.bbiLoadFile.Name = "bbiLoadFile";
            this.bbiLoadFile.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiLoadFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadFile_ItemClick);
            // 
            // bbiRunEditor
            // 
            this.bbiRunEditor.Caption = "Запустить Редактор";
            this.bbiRunEditor.Glyph = global::aetp.Common.Properties.Resources._32_запустить_редактор;
            this.bbiRunEditor.Id = 16;
            this.bbiRunEditor.Name = "bbiRunEditor";
            this.bbiRunEditor.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRunEditor.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiRunEditor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRunEditor_ItemClick);
            // 
            // bbiDublicateSection
            // 
            this.bbiDublicateSection.Caption = "Клонировать элемент";
            this.bbiDublicateSection.Glyph = global::aetp.Common.Properties.Resources._32_клонировать_элемент;
            this.bbiDublicateSection.Id = 17;
            this.bbiDublicateSection.Name = "bbiDublicateSection";
            this.bbiDublicateSection.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiDublicateSection.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            this.bbiDublicateSection.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDublicateSection_ItemClick);
            // 
            // bbiTools
            // 
            this.bbiTools.Caption = "Инструменты";
            this.bbiTools.Glyph = global::aetp.Common.Properties.Resources.tools;
            this.bbiTools.Id = 18;
            this.bbiTools.Name = "bbiTools";
            this.bbiTools.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiTools.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiTools.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTools_ItemClick);
            // 
            // bbiPropertyWindow
            // 
            this.bbiPropertyWindow.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiPropertyWindow.Caption = "Свойства";
            this.bbiPropertyWindow.Down = true;
            this.bbiPropertyWindow.Glyph = global::aetp.Common.Properties.Resources.document_properties;
            this.bbiPropertyWindow.Id = 19;
            this.bbiPropertyWindow.Name = "bbiPropertyWindow";
            this.bbiPropertyWindow.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bbiStructureWindow
            // 
            this.bbiStructureWindow.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiStructureWindow.Caption = "Структура";
            this.bbiStructureWindow.Down = true;
            this.bbiStructureWindow.Glyph = global::aetp.Common.Properties.Resources.activity_window;
            this.bbiStructureWindow.Id = 20;
            this.bbiStructureWindow.Name = "bbiStructureWindow";
            this.bbiStructureWindow.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bbiOutputWindow
            // 
            this.bbiOutputWindow.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiOutputWindow.Caption = "Вывод";
            this.bbiOutputWindow.Down = true;
            this.bbiOutputWindow.Glyph = global::aetp.Common.Properties.Resources.sign_out;
            this.bbiOutputWindow.Id = 21;
            this.bbiOutputWindow.Name = "bbiOutputWindow";
            this.bbiOutputWindow.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bbiNew
            // 
            this.bbiNew.Caption = "Создать";
            this.bbiNew.Glyph = global::aetp.Common.Properties.Resources._16_создать;
            this.bbiNew.Id = 22;
            this.bbiNew.LargeGlyph = global::aetp.Common.Properties.Resources._32_создать;
            this.bbiNew.Name = "bbiNew";
            this.bbiNew.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNew_ItemClick);
            // 
            // bbiOpenEditor
            // 
            this.bbiOpenEditor.Caption = "Предварительный просмотр";
            this.bbiOpenEditor.Glyph = global::aetp.Common.Properties.Resources.preview;
            this.bbiOpenEditor.Id = 23;
            this.bbiOpenEditor.Name = "bbiOpenEditor";
            this.bbiOpenEditor.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiOpenEditor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiOpenEditor_ItemClick);
            // 
            // bbiAddMetaControl
            // 
            this.bbiAddMetaControl.Caption = "Мета Элемент";
            this.bbiAddMetaControl.Glyph = global::aetp.Common.Properties.Resources.document_star;
            this.bbiAddMetaControl.Id = 25;
            this.bbiAddMetaControl.Name = "bbiAddMetaControl";
            this.bbiAddMetaControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAddMetaControl.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddMetaControl_ItemClick);
            this.bbiAddMetaControl.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btnXSDGenerate
            // 
            this.btnXSDGenerate.Caption = "Генерировать XSD";
            this.btnXSDGenerate.Glyph = global::aetp.Common.Properties.Resources.xsd;
            this.btnXSDGenerate.Id = 27;
            this.btnXSDGenerate.Name = "btnXSDGenerate";
            this.btnXSDGenerate.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.btnXSDGenerate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXSDGenerate_ItemClick);
            // 
            // bbiImportDictionary
            // 
            this.bbiImport.Caption = "Импорт элементов";
            this.bbiImport.Id = 28;
            this.bbiImport.LargeGlyph = global::aetp.Common.Properties.Resources.document_import;
            this.bbiImport.Name = "bbiImportDictionary";
            this.bbiImport.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiImport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiImportDictionary_ItemClick);

            // 
            // rpMain
            // 
            this.rpMain.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {            
            this.rpgFile,
            this.ribbonPageGroup1,
            this.rpg_action,
            this.rpg_xsd});
            this.rpMain.Name = "rpMain";
            this.rpMain.Text = "Главная";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiAddPage);            
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiAddSimpleEditor);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnAddGrid);
            this.ribbonPageGroup1.ItemLinks.Add(this.btn_table);
            this.ribbonPageGroup1.ItemLinks.Add(this.btn_edit);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiAddGridAddedRow);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiAddGridAddedPage);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiAddMetaGrid);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiAddMetaControl);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiAddComboBox);           
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiAddDictionary);
            this.ribbonPageGroup1.ItemLinks.Add(this.AddLink);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Редактирование";
            // 
            // rpgFile
            // 
            this.rpgFile.ItemLinks.Add(this.bbiNew);
            this.rpgFile.ItemLinks.Add(this.bbiSaveAs);
            this.rpgFile.ItemLinks.Add(this.bbiSave);
            this.rpgFile.ItemLinks.Add(this.bbiLoadFile);
            this.rpgFile.Name = "rpgFile";
            this.rpgFile.Text = "Файл";

            // 
            // rpg_action
            //
            this.rpg_action.ItemLinks.Add(this.bbiImport);
            this.rpg_action.ItemLinks.Add(this.bbiDublicateSection);
            this.rpg_action.ItemLinks.Add(this.bbiRemoveItem);
            this.rpg_action.Name = "rpg_action";
            this.rpg_action.Text = "";
            // 
            // rpg_xsd
            //             
            this.rpg_xsd.ItemLinks.Add(this.bbiOpenEditor);
            this.rpg_xsd.ItemLinks.Add(this.bbiRunEditor);
            this.rpg_xsd.ItemLinks.Add(this.bbiTools);
            this.rpg_xsd.ItemLinks.Add(this.btnXSDGenerate);
            this.rpg_xsd.Name = "rpg_xsd";
            this.rpg_xsd.Text = "";

            // 
            // rpWindow
            // 
            this.rpWindow.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgWindow});
            this.rpWindow.Name = "rpWindow";
            this.rpWindow.Text = "Окно";
            // 
            // rpgWindow
            // 
            this.rpgWindow.ItemLinks.Add(this.bbiStructureWindow);
            this.rpgWindow.ItemLinks.Add(this.bbiPropertyWindow);
            this.rpgWindow.ItemLinks.Add(this.bbiOutputWindow);
            this.rpgWindow.Name = "rpgWindow";
            this.rpgWindow.Text = "Панели";
            // 
            // dockPanel1
            // 
            this.dockPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel1.DockBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.dockPanel1.DockBottomPortion = 200D;
            this.dockPanel1.DockLeftPortion = 300D;
            this.dockPanel1.DockRightPortion = 300D;
            this.dockPanel1.DockTopPortion = 200D;
            this.dockPanel1.Location = new System.Drawing.Point(0, 144);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Size = new System.Drawing.Size(1505, 539);
            dockPanelGradient1.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient1.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.SystemColors.Control;
            tabGradient1.StartColor = System.Drawing.SystemColors.Control;
            tabGradient1.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            autoHideStripSkin1.TabGradient = tabGradient1;
            autoHideStripSkin1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.SystemColors.Control;
            dockPanelGradient2.StartColor = System.Drawing.SystemColors.Control;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            dockPaneStripSkin1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.SystemColors.Control;
            tabGradient5.StartColor = System.Drawing.SystemColors.Control;
            tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.SystemColors.InactiveCaption;
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.TextColor = System.Drawing.SystemColors.InactiveCaptionText;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.Transparent;
            tabGradient7.StartColor = System.Drawing.Color.Transparent;
            tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            this.dockPanel1.Skin = dockPanelSkin1;
            this.dockPanel1.TabIndex = 17;
            // 
            // ConstructorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1505, 683);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "ConstructorView";
            this.Ribbon = this.ribbon;
            //this.Text = "Анкета. Конструктор";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ConstructorView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        public DevExpress.XtraBars.Ribbon.RibbonPage rpMain;
        public DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        public DevExpress.XtraBars.Ribbon.RibbonPageGroup rpg_action;
        public DevExpress.XtraBars.Ribbon.RibbonPageGroup rpg_xsd;
        public DevExpress.XtraBars.BarButtonItem bbiAddPage;
        public DevExpress.XtraBars.BarButtonItem bbiAddSimpleEditor;
        public DevExpress.XtraBars.BarButtonItem bbiRemoveItem;
        public DevExpress.XtraBars.BarButtonItem btnAddGrid;
        public DevExpress.XtraBars.BarButtonItem AddLink;
        public DevExpress.XtraBars.BarButtonItem bbiAddComboBox;
        public DevExpress.XtraBars.BarButtonItem bbiAddGridAddedRow;
        public DevExpress.XtraBars.BarButtonItem bbiAddGridAddedPage;
		public DevExpress.XtraBars.BarButtonItem bbiAddMetaGrid;
        public DevExpress.XtraBars.BarButtonItem bbiAddDictionary;
        public DevExpress.XtraBars.BarButtonItem bbiSaveAs;
          public DevExpress.XtraBars.BarButtonItem bbiSave;
        public DevExpress.XtraBars.BarButtonItem bbiTools;
        public DevExpress.XtraBars.BarButtonItem bbiLoadFile;
        public DevExpress.XtraBars.BarButtonItem bbiRunEditor;
        public DevExpress.XtraBars.BarButtonItem bbiDublicateSection;
        public DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgFile;
        public WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel1;
        public DevExpress.XtraBars.Ribbon.RibbonPage rpWindow;
        public DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgWindow;
        public DevExpress.XtraBars.BarButtonItem bbiNew;
        public DevExpress.XtraBars.BarButtonItem bbiOpenEditor;
        public DevExpress.XtraBars.BarButtonItem bbiAddMetaControl;
        public DevExpress.XtraBars.BarButtonItem btnXSDGenerate;
        public DevExpress.XtraBars.BarButtonItem bbiImport;
        public DevExpress.XtraBars.BarButtonItem bbiPropertyWindow;
        public DevExpress.XtraBars.BarButtonItem bbiStructureWindow;
        public DevExpress.XtraBars.BarButtonItem bbiOutputWindow;
        public System.Windows.Forms.Button btn_grid;
        public System.Windows.Forms.Button btn_gridAddedRow;
        public System.Windows.Forms.Button btn_gridAddedPage;
        public System.Windows.Forms.Button btn_Metagrid;
        public DevExpress.XtraBars.BarSubItem btn_table;
        public DevExpress.XtraBars.BarCheckItem bci_table;
        public DevExpress.XtraBars.BarCheckItem bci_tableAddRow;
        public DevExpress.XtraBars.BarCheckItem bci_tableAddPage;
        public DevExpress.XtraBars.BarCheckItem bci_Metatable;
        public DevExpress.XtraBars.BarSubItem btn_edit;
        public DevExpress.XtraBars.BarCheckItem bci_simpleEdit;
         public DevExpress.XtraBars.BarCheckItem bci_RichEdit;
        public DevExpress.XtraBars.BarCheckItem bci_dateEdit;
        public DevExpress.XtraBars.BarCheckItem bci_IntEdit;
        public DevExpress.XtraBars.BarCheckItem bci_FloatEdit;
        public DevExpress.XtraBars.BarCheckItem bci_CheckEdit; 
        public DevExpress.XtraBars.BarCheckItem bci_Label;
        public DevExpress.XtraBars.BarCheckItem bci_MetaControl;
        public DevExpress.XtraBars.BarCheckItem bci_MemoEdit;
        
    }
}

