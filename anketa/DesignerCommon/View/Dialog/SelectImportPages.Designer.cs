﻿namespace aetp.Common.View.Dialog
{
    partial class SelectImportPages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DictionaryList = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.panelSave = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.panelRename = new DevExpress.XtraEditors.PanelControl();
            this.btnRename = new DevExpress.XtraEditors.SimpleButton();
            this.NameDictionary = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DictionaryList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelSave)).BeginInit();
            this.panelSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelRename)).BeginInit();
            this.panelRename.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NameDictionary.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DictionaryList
            // 
            this.DictionaryList.CheckOnClick = true;
            this.DictionaryList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DictionaryList.Location = new System.Drawing.Point(0, 0);
            this.DictionaryList.Name = "DictionaryList";
            this.DictionaryList.Size = new System.Drawing.Size(314, 231);
            this.DictionaryList.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.DictionaryList.TabIndex = 0;
            // 
            // panelSave
            // 
            this.panelSave.Controls.Add(this.btnCancel);
            this.panelSave.Controls.Add(this.btnOk);
            this.panelSave.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSave.Location = new System.Drawing.Point(0, 231);
            this.panelSave.Name = "panelSave";
            this.panelSave.Size = new System.Drawing.Size(314, 50);
            this.panelSave.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(234, 15);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(153, 15);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // panelRename
            // 
            this.panelRename.Controls.Add(this.btnRename);
            this.panelRename.Controls.Add(this.NameDictionary);
            this.panelRename.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelRename.Location = new System.Drawing.Point(0, 181);
            this.panelRename.Name = "panelRename";
            this.panelRename.Size = new System.Drawing.Size(314, 50);
            this.panelRename.TabIndex = 2;
            this.panelRename.Visible = false;
            // 
            // btnRename
            // 
            this.btnRename.Location = new System.Drawing.Point(227, 14);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(75, 23);
            this.btnRename.TabIndex = 3;
            this.btnRename.Text = "ОК";
            // 
            // NameDictionary
            // 
            this.NameDictionary.Location = new System.Drawing.Point(12, 17);
            this.NameDictionary.Name = "NameDictionary";
            this.NameDictionary.Size = new System.Drawing.Size(195, 20);
            this.NameDictionary.TabIndex = 2;
            // 
            // SelectImportPages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 281);
            this.Controls.Add(this.panelRename);
            this.Controls.Add(this.DictionaryList);
            this.Controls.Add(this.panelSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectImportPages";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Выберите разделы для импорта";
            ((System.ComponentModel.ISupportInitialize)(this.DictionaryList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelSave)).EndInit();
            this.panelSave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelRename)).EndInit();
            this.panelRename.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NameDictionary.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckedListBoxControl DictionaryList;
        private DevExpress.XtraEditors.PanelControl panelSave;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.PanelControl panelRename;
        private DevExpress.XtraEditors.SimpleButton btnRename;
        private DevExpress.XtraEditors.TextEdit NameDictionary;
    }
}