﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.ControlModel;
using DevExpress.XtraEditors.Controls;

namespace aetp.Common.View.Dialog
{
    public partial class SelectImportDicitonary : DevExpress.XtraEditors.XtraForm
    {
        public Report Report;
        private Report DestReport;
        private CheckedListBoxItem selectItem;
        object[] cancelitems;
        int n_item = 0;

        public SelectImportDicitonary(Report _DestReport, Report _Report)
        {
            DestReport = _DestReport;
            Report = _Report;
            InitializeComponent();
            for (int i = 0; i < Report.MetaData.Dictionarys.Count; i++)
            {
                DictionaryList.Items.Add(Report.MetaData.Dictionarys[i]);
            }
        }

        private void RefrechCheckItem()
        {
            for (int i = 0; i < n_item; i++)
            {
                if (cancelitems[i] != null)
                    DictionaryList.Items[cancelitems[i]].InvertCheckState();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            //проверка уникальности переносимых словарей
            cancelitems = new object[DictionaryList.CheckedItems.Count];
            n_item = 0;
            foreach (CheckedListBoxItem ctrl in DictionaryList.CheckedItems)
            {
                string DictName = ((DictionaryList)ctrl.Value).Name;
                if (DestReport.MetaData.GetDictionaryByName(DictName) != null)
                {
                    if (MessageBox.Show(string.Format("Cловарь \"{0}\" уже есть в списке. Переименовать и добавить его?", DictName), "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        NameDictionary.EditValue = ctrl.Description;
                        
                        DictionaryList.Enabled = false;
                        panelRename.Visible = true;
                        panelSave.Visible = false;
                        selectItem = ctrl;
                        NameDictionary.EditValue = DictName;
                        RefrechCheckItem();
                        return;
                    }
                    else
                    {
                        cancelitems[n_item] = (DictionaryList)ctrl.Value;
                        n_item++;
                    }
                }
            }
            //удаляем из выделенных отмененые для переноса
            RefrechCheckItem();
            //переносим словари
            for (int i = 0; i < DictionaryList.Items.Count; i++)
            {
                if (DictionaryList.Items[i].CheckState == CheckState.Unchecked)
                    Report.MetaData.RemoveList((DictionaryList)DictionaryList.Items[i].Value);
            }
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRename_Click(object sender, EventArgs e)
        {
            selectItem.Description = NameDictionary.Text;
            ((DictionaryList)selectItem.Value).Name = NameDictionary.Text;
            DictionaryList.Enabled = true;
            panelRename.Visible = false;
            panelSave.Visible = true;
        }
    }
}