﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common.ControlModel;
using DevExpress.XtraEditors.Controls;

namespace aetp.Common.View.Dialog
{
    public partial class SelectImportPages : DevExpress.XtraEditors.XtraForm
    {
        private Report Report;
        private Report DestReport;
        public List<aetp.Common.ControlModel.BaseControl> transferList;
        List<aetp.Common.ControlModel.BaseControl> pages;
        
        public SelectImportPages(Report _DestReport, Report _Report)
        {
            DestReport = _DestReport;
            Report = _Report;
            InitializeComponent();
            List<Type> types = new List<Type>() { typeof(Page), typeof(Grid), typeof(GridAddedPage), typeof(GridAddedRow), typeof(MetaGrid) };
            pages = ReportUtils.GetAllControls(Report, types);
            for (int i = 0; i < pages.Count; i++)
            {
                DictionaryList.Items.Add(pages[i]);
            }
        }
        
        private void btnOk_Click(object sender, EventArgs e)
        {
            //переносим словари
            transferList = new List<aetp.Common.ControlModel.BaseControl>();
            for (int i = 0; i < DictionaryList.Items.Count; i++)
            {
                if (DictionaryList.Items[i].CheckState == CheckState.Checked)
                    transferList.Add(DictionaryList.Items[i].Value as aetp.Common.ControlModel.BaseControl);
            }
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}