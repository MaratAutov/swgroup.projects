﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.Mediator;
using aetp.Common.ControlModel;
using System.Xml;
using aetp.Common.Validator;
using System.IO;

namespace aetp.Common
{
    public class XSDGenerator
    {
        EditorViewSerializerConfig config;
        Report report;
        const string xs = "http://www.w3.org/2001/XMLSchema";

        #region flags
        List<string> GeneratedTypeName = new List<string>();

        void ResetFlags()
        {
            GeneratedTypeName = new List<string>();
        }
        #endregion

        public XSDGenerator(Report report)
        {
            this.config = report.SerialaizerConfig;
            this.report = report;
        }

        Dictionary<string, string> names;

        public void Generate(XmlWriter writer)
        {
            ResetFlags();
            Dictionary<string, Page> complexes = new Dictionary<string, Page>();
            Dictionary<string, BaseControl> simple = new Dictionary<string, BaseControl>();
            Dictionary<string, BaseGrid> grids = new Dictionary<string, BaseGrid>();

            //if (report.AttachFile)
            //    complexes.Add(page.SerializingTag, page);

            GetTypes(report, ref complexes, ref simple, ref grids);
            names = GetSimpleTypeNames(simple);
            
            // header
            writer.WriteStartElement("xs", "schema", xs);
            writer.WriteAttributeString("xmlns", "xserializer", null, config.namespace_xserializer);
            writer.WriteAttributeString("xmlns", "xs", null, xs);
            writer.WriteAttributeString("xmlns", "av", null, config.namespace_av);
            writer.WriteAttributeString("targetNamespace", config.namespace_av);
            writer.WriteAttributeString("elementFormDefault", "qualified");
            writer.WriteAttributeString("attributeFormDefault", "unqualified");
            writer.WriteComment("========== Глобальные элементы (документы) ==========");

            //head element
            writer.WriteStartElement("xs", "element", xs);
            writer.WriteAttributeString("name", report.SerializingTag);
            writer.WriteAttributeString("type", "av:" + PageTypeName(report));
            //annotation
            GenerateAnnotationComplexClass(writer, report);
            
            //end element
            writer.WriteEndElement();

            //complex type
            writer.WriteComment("========== Комплексные типы ==========");
            if (report.AttachFile)
                GenerateXsdFile(writer);
            RecGenerateTypes(writer, complexes, grids);
            
            // simple type
            writer.WriteComment("========== Простые типы ==========");
            GenerateStandartTypes(writer, simple);
            
            //end schema
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
        }

        bool GenerateTypeName(string name)
        {
            if (!GeneratedTypeName.Contains(name))
            {
                GeneratedTypeName.Add(name);
                return true;
            }
            else
                return false;
        }

        void GenerateXsdFile(XmlWriter writer)
        {
            writer.WriteStartElement("xs", "complexType", xs);
            writer.WriteAttributeString("name", "Files");
            writer.WriteStartElement("xs", "annotation", xs);
            writer.WriteStartElement("xs", "appinfo", xs);
            writer.WriteElementString("description", "Files");
            writer.WriteElementString("type", "ComplexClass");
            writer.WriteStartElement("baseTypes");
            writer.WriteElementString("baseType", "IncomingPackage");
            writer.WriteElementString("baseType", "DatabaseObject");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteElementString("xs", "documentation", xs, "Files");
            writer.WriteEndElement();
            writer.WriteStartElement("xs", "sequence", xs);
            writer.WriteStartElement("xs", "element", xs);
            writer.WriteAttributeString("name", "File");
            writer.WriteAttributeString("minOccurs", "0");
            writer.WriteAttributeString("maxOccurs", "unbounded");
            writer.WriteStartElement("xs", "complexType", xs);
            writer.WriteStartElement("xs", "simpleContent", xs);
            writer.WriteStartElement("xs", "extension", xs);
            writer.WriteAttributeString("base", "xs:string");
            writer.WriteStartElement("attribute", xs);
            writer.WriteAttributeString("name", "Name");
            writer.WriteAttributeString("type", "xs:string");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        string PageTypeName(Page page)
        {
            return page.Name + "_" + page.SerializingTag;
        }

        void RecGenerateTypes(XmlWriter writer, Dictionary<string, Page> complexes, Dictionary<string, BaseGrid> grids)
        {
            if (GenerateTypeName("ДаНет"))
                GenerateVocabularyType(writer, "ДаНет", "Да/Нет");
            foreach (var item in report.MetaData.Dictionarys)
            {
                if (GenerateTypeName(item.Name))
                    GenerateVocabularyType(writer, item.Name, item.Caption);
            }
            foreach (Page page in complexes.Values)
            {
                if (GenerateTypeName(PageTypeName(page)))
                    GenerateComplexType(writer, page);
            }
            foreach (BaseGrid grid in grids.Values)
            {
                if (grid is GridAddedRow)
                    GenerateComplexGridAddedRowType(writer, grid as GridAddedRow);
                if (grid is Grid)
                    GenerateComplexGridType(writer, grid as Grid);
                if (grid is GridAddedPage)
                    GenerateComplexGridAddedPageType(writer, grid as GridAddedPage);
                if (grid is MetaGrid)
                    GenerateComplexMetaGridType(writer, grid as MetaGrid);
            }
        }

        void GenerateComplexTypeHeader(XmlWriter writer, string name, string type, string documentation = null)
        {
            writer.WriteStartElement("xs", "complexType", xs);
            writer.WriteAttributeString("name", name);
            writer.WriteStartElement("xs", "annotation", xs);
            writer.WriteStartElement("xs", "appinfo", xs);
            writer.WriteElementString("description", null);
            if (!String.IsNullOrEmpty(type))
                writer.WriteElementString("type", "Report");
            writer.WriteEndElement();
            writer.WriteElementString("xs", "documentation", xs, documentation);
            writer.WriteEndElement();
        }

        void GenerateComplexMetaGridType(XmlWriter writer, MetaGrid grid)
        {
            if (GenerateTypeName(grid.SerializingTag))
            {
                GenerateComplexTypeHeader(writer, grid.SerializingTag, "Report", grid.Caption);
                writer.WriteStartElement("xs", "sequence", xs);

                foreach (var g in grid.Tables)
                {
                    if (g is Grid)
                        GenerateRows(writer, g as Grid);
                    else if (g is GridAddedRow)
                    {
                        GridAddedRow gar = g as GridAddedRow;
                        GenerateElement(writer, gar.SerializingRowTag, "av:" + gar.SerializingRowTag, gar.IsMandatoryToFill ? "1" : "0", null, "unbounded");
                    }
                }

                writer.WriteEndElement();
                writer.WriteEndElement();
            }
            foreach (var g in grid.Tables)
            {
                if (g is Grid)
                    GenerateRowType(writer, g as Grid);
                else if (g is GridAddedRow)
                    GenerateAddedRowType(writer, g as GridAddedRow);
            }
        }

        void GenerateComplexGridAddedPageType(XmlWriter writer, GridAddedPage grid)
        {
            if (!GenerateTypeName(grid.SerializingTag))
                return;
            GenerateComplexTypeHeader(writer, grid.SerializingTag, "Report", grid.Caption);
            writer.WriteStartElement("xs", "sequence", xs);
            if (grid.Children.Count > 0)
            {
                BaseControl bs = grid.Children[0];
                string name = bs.SerializingTag;
                string minOccurs = "0";
                string description = bs.Caption;
                string type = "av:" + PageTypeName(bs as Page);
                GenerateElement(writer, name, type, minOccurs, description, "unbounded");
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        void GenerateComplexGridType(XmlWriter writer, Grid grid)
        {
            if (GenerateTypeName(grid.SerializingTag))
            {
                GenerateComplexTypeHeader(writer, grid.SerializingTag, "Report", XSDNameHelper.GetGridCaption(grid));
                GenerateRows(writer, grid);
                writer.WriteEndElement();
            }
            GenerateRowType(writer, grid);
        }

        void GenerateRows(XmlWriter writer, Grid grid)
        {
            writer.WriteStartElement("xs", "sequence", xs);
            foreach (GridRow row in grid.Rows)
            {
                string name = row.SerializingTag;
                string minOccurs = "0";
                string description = XSDNameHelper.GetRowCaption(row);
                string type = "av:" + getUniqueName(row);
                GenerateElement(writer, name, type, minOccurs, description, null);
            }
            writer.WriteEndElement();
        }

        

        void GenerateRowType(XmlWriter writer, Grid grid)
        {
            foreach (GridRow row in grid.Rows)
            {
                if (!GenerateTypeName(getUniqueName(row)))
                    continue;
                GenerateComplexTypeHeader(writer, getUniqueName(row), null);
                writer.WriteStartElement("xs", "sequence", xs);
                foreach (GridCellItem cell in row.Cells)
                {
                    string name = cell.SerializingTag;
                    string minOccurs = cell.IsMandatoryToFill ? "1" : "0";
                    string description = XSDNameHelper.GetGridCellItemCaption(cell);
                    string type = GetTypeString(cell.Type, cell, cell.boolSerializationType);
                    GenerateElement(writer, name, type, minOccurs, description, null);
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }

        private void GenerateComplexGridAddedRowType(XmlWriter writer, GridAddedRow grid)
        {
            if (GenerateTypeName(grid.SerializingTag))
            {
                GenerateComplexTypeHeader(writer, grid.SerializingTag, "Report");
                writer.WriteStartElement("xs", "sequence", xs);
                GenerateElement(writer, grid.SerializingRowTag, "av:" + grid.SerializingRowTag, grid.IsMandatoryToFill ? "1": "0", null, "unbounded");
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
            GenerateAddedRowType(writer, grid);
        }

        void GenerateAddedRowType(XmlWriter writer, GridAddedRow grid)
        {
            if (!GenerateTypeName(grid.SerializingRowTag))
                return;
            writer.WriteStartElement("xs", "complexType", xs);
            writer.WriteAttributeString("name", grid.SerializingRowTag);
            writer.WriteStartElement("xs", "annotation", xs);
            writer.WriteStartElement("xs", "appinfo", xs);
            writer.WriteElementString("description", XSDNameHelper.GetGridCaption(grid));
            writer.WriteElementString("type", "Report");
            writer.WriteEndElement();
            writer.WriteElementString("xs", "documentation", xs, null);
            writer.WriteEndElement();
            writer.WriteStartElement("xs", "sequence", xs);

            foreach (var col in grid.Columns)
            {
                string name = col.SerializingTag;
                string minOccurs = col.IsMandatoryToFill ? "1" : "0";
                string description = XSDNameHelper.GetGridAddedRowColumnCaption(col);

                string type = GetTypeString(col.Type, col,col.boolSerializationType);
                GenerateElement(writer, name, type, minOccurs, description, null);
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        string GetRegexMaskName(string mask)
        {
            string name = "";
            if (!String.IsNullOrEmpty(mask))
            {
                string[] tmp =
                    mask.Split('(', ')', '*', '/', '+', '-', ':', ' ',',','.','"');
                for (int i = 0; i < tmp.Length; i++)
                {
                    if (tmp[i].Length != 0)
                    {
                        tmp[i] = (string)(tmp[i].Remove(1, tmp[i].Length - 1)).ToUpper() + tmp[i].Remove(0, 1);
                        name += tmp[i];
                    }
                }
            }
            return name;
        }


        Dictionary<string, string> GetSimpleTypeNames(Dictionary<string, BaseControl> simple)
        {
            Dictionary<string, string> names = new Dictionary<string, string>();
            foreach (KeyValuePair<string, BaseControl> kvp in simple)
            {                
                MaskItem mi = GetMask(kvp.Value);
                string name = string.Empty;

                if (mi.MaskType != "Пользовательский")
                {
                    name = GetRegexMaskName(mi.MaskType);
                }
                else
                {
                    if (!String.IsNullOrEmpty(mi.Description))
                        name = GetRegexMaskName("_" + mi.Description);
                    else
                    {
                        name = GetRegexMaskName("_" + mi.ErrorMsg);
                    }
                    //name = "name" + Guid.NewGuid().ToString();
                }               
                names.Add(kvp.Key, name);
            }
            return names;
        }

        const string split_holder = "_";
        public string getUniqueName(BaseControl ctrl)
        {
            string value = ctrl.SerializingTag;
            if (ctrl is GridRow)
                value = (ctrl as GridRow).Grid.SerializingTag + split_holder + value;
            if (ctrl.Parent != null)
                value = ctrl.Parent.SerializingTag + split_holder + value;
            return value;
        }

        void GetTypes(Page page, ref Dictionary<string, Page> complexes, ref Dictionary<string, BaseControl> simple, ref Dictionary<string, BaseGrid> grids)
        {

            if (!complexes.ContainsKey(PageTypeName(page)))
                complexes.Add(PageTypeName(page), page);
            GetTypes(page.Children, ref complexes, ref simple, ref grids);
        }

        void GetTypes(List<BaseControl> controls, ref Dictionary<string, Page> complexes, ref Dictionary<string, BaseControl> simple, ref Dictionary<string, BaseGrid> grids)
        {
            foreach (var bs in controls)
            {
                if (bs is Page)
                    GetTypes(bs as Page, ref complexes, ref simple, ref grids);
                if ((bs is SimpleEdit) && !String.IsNullOrEmpty((bs as SimpleEdit).Mask.Mask) && !simple.ContainsKey((bs as SimpleEdit).Mask.Mask + "#" + (bs as SimpleEdit).Mask.Description))
                    simple.Add((bs as SimpleEdit).Mask.Mask + "#" + (bs as SimpleEdit).Mask.Description, bs);
                if (bs is MetaConrol)
                {
                    GetTypes((bs as MetaConrol).Children, ref complexes, ref simple, ref grids);
                }
                if (bs is BaseGrid)
                {
                    BaseGrid grid = bs as BaseGrid;
                    if (!grids.ContainsKey(grid.SerializingTag))
                        grids.Add(grid.SerializingTag, grid);
                    if (grid is GridAddedRow)
                    {
                        GridAddedRow gar = grid as GridAddedRow;
                        foreach (var col in gar.Columns)
                        {
                            string mask = GetMask(col).Mask;
                            if (!String.IsNullOrEmpty(mask) && !simple.ContainsKey(mask + "#" + GetMask(col).Description))
                                simple.Add(mask + "#" + GetMask(col).Description, col);
                        }
                    }
                    if (grid is Grid)
                    {
                        Grid g = grid as Grid;
                        foreach (var row in g.Rows)
                        {
                            foreach (var cell in row.Cells)
                            {
                                string mask = GetMask(cell).Mask;
                                if (!String.IsNullOrEmpty(mask) && !simple.ContainsKey(mask + "#" + GetMask(cell).Description))
                                    simple.Add(mask + "#" + GetMask(cell).Description, cell);
                            }
                        }
                    }
                    if (grid is GridAddedPage)
                    {
                        if (grid.Children.Count > 0 && grid.Children[0] is Page)
                            GetTypes(grid.Children[0] as Page, ref complexes, ref simple, ref grids);
                    }
                }
            }
        }

        void GenerateComplexType(XmlWriter writer, Page page)
        {
            writer.WriteStartElement("xs", "complexType", xs);
            writer.WriteAttributeString("name", PageTypeName(page));
            GenerateAnnotationComplexClass(writer, page);
            GenerateSequenceComplexClass(writer, page);
            if (page is Report)
                GenerateReportAttributes(writer, page as Report);
            writer.WriteEndElement();
        }

        void GenerateReportAttributes(XmlWriter writer, Report report)
        {
            writer.WriteStartElement("xs", "attribute", xs);
            writer.WriteAttributeString("name", "externalId");
            writer.WriteAttributeString("type", "av:Guid");
            writer.WriteEndElement();
            writer.WriteStartElement("xs", "attribute", xs);
            writer.WriteAttributeString("name", "internalId");
            writer.WriteAttributeString("type", "av:Guid");
            writer.WriteEndElement();
            writer.WriteStartElement("xs", "attribute", xs);
            writer.WriteAttributeString("name", "appVersion");
            writer.WriteAttributeString("type", "av:String");
            writer.WriteAttributeString("use", "required");
            writer.WriteAttributeString("fixed", report.SerialaizerConfig.appVersion);
            writer.WriteEndElement();
            writer.WriteStartElement("xs", "attribute", xs);
            writer.WriteAttributeString("name", "appDescription");
            writer.WriteAttributeString("type", "av:String");
            writer.WriteEndElement();
        }

        //annotation
        void GenerateAnnotationComplexClass(XmlWriter writer, Page page)
        {
            writer.WriteStartElement("xs", "annotation", xs);
            writer.WriteStartElement("xs", "appinfo", xs);
            writer.WriteElementString("description", page.Caption);
            writer.WriteElementString("type", "ComplexClass");
            writer.WriteStartElement("baseTypes");
            writer.WriteElementString("baseType", "IncomingPackage");
            writer.WriteElementString("baseType", "DatabaseObject");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteElementString("xs", "documentation", xs, page.Caption);
            writer.WriteEndElement();
        }

        //sequence
        void GenerateSequenceComplexClass(XmlWriter writer, Page page)
        {
            if (page.Children.Count == 0)
                return;
            writer.WriteStartElement("xs", "sequence", xs);
            if (page is Report)
            {
                if ((page as Report).AttachFile)
                {
                    writer.WriteStartElement("xs", "element", xs);
                    writer.WriteAttributeString("name", "Files");
                    writer.WriteAttributeString("type", "av:Files");
                    writer.WriteAttributeString("minOccurs", "0");

                    writer.WriteStartElement("xs", "annotation", xs);
                    writer.WriteStartElement("xs", "appinfo", xs);
                    writer.WriteElementString("description", "Files");
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
            }
            foreach (BaseControl bs in page.Children)
            {
                if (bs is Page)
                    GeneratePageElement(writer, bs as Page);
                if (bs is MetaConrol)
                {
                    GenerateSequenceComplexClassMetaControl(writer, bs as MetaConrol);
                }
                if (bs is SimpleEdit)
                    GenerateSimpleElement(writer, bs as SimpleEdit);
                if (bs is ComboBox)
                    GenerateComboBoxElement(writer, bs as ComboBox);
                if (bs is BaseGrid)
                    GenerateGridElement(writer, bs as BaseGrid);
            }
            writer.WriteEndElement();
        }

        void GenerateSequenceComplexClassMetaControl(XmlWriter writer, MetaConrol control)
        {
            if (control.Children.Count == 0)
                return;
            //writer.WriteStartElement("xs", "sequence", xs);
            foreach (BaseControl bs in control.Children)
            {
                if (bs is Page)
                    GeneratePageElement(writer, bs as Page);
                if (bs is MetaConrol)
                {
                    GenerateSequenceComplexClassMetaControl(writer, bs as MetaConrol);
                }
                if (bs is SimpleEdit)
                    GenerateSimpleElement(writer, bs as SimpleEdit);
                if (bs is ComboBox)
                    GenerateComboBoxElement(writer, bs as ComboBox);
                if (bs is BaseGrid)
                    GenerateGridElement(writer, bs as BaseGrid);
            }
            //writer.WriteEndElement();
        }

        private void GeneratePageElement(XmlWriter writer, Page page)
        {
            string name = page.SerializingTag;
            string minOccurs = string.Empty;
            if ((page.IsMandatoryToFill) && page.Visible_Page)
            {
                minOccurs = "1";
            }
            else
            {
                minOccurs = "0";
            }

            string description = page.Caption;
            string type = "av:" + PageTypeName(page);
            GenerateElement(writer, name, type, minOccurs, description, null);
        }

        void GenerateGridElement(XmlWriter writer, BaseGrid grid)
        {
            string name = grid.SerializingTag;
            string minOccurs = "0";
            string description = grid.Caption;
            string type = "av:" + grid.SerializingTag;
            GenerateElement(writer, name, type, minOccurs, description, null);
        }

        
        void GenerateComboBoxElement(XmlWriter writer, ComboBox cb)
        {
            string name = cb.SerializingTag;
            string minOccurs = string.Empty;
            if ((cb.IsMandatoryToFill) && !(cb.Parent is MetaConrol))
            {
                minOccurs = "1";
            }
            else
            {
                minOccurs = "0";
            }

            string description = cb.Caption;
            string type = cb.Source != null ? "av:" + cb.Source.Name : null;
            GenerateElement(writer, name, type, minOccurs, description, null);
        }

        string GetTypeString(SimpleDataType type, BaseControl control,BoolSerializtionType bool_type)
        {
            string typestr = null;
            switch (type)
            {
                case SimpleDataType.Bool:
                    if (bool_type == BoolSerializtionType.yesno)
                    typestr = "av:ДаНет";
                    else if (bool_type == BoolSerializtionType.boolean)
                        typestr = "av:Boolean";
                    break;
                case SimpleDataType.String:
                    string mask = GetMask(control).Mask;
                    if (String.IsNullOrEmpty(mask))
                        typestr = "av:String";
                    else
                        typestr = "av:" + names[mask + "#" + GetMask(control).Description];
                    break;
                case SimpleDataType.DateTime:
                    typestr = "av:Date";
                    break;
                case SimpleDataType.Integer:
                    typestr = "av:Long";
                    break;
                case SimpleDataType.Double:
                    typestr = "av:Double";
                    break;
            }
            return typestr;
        }

        string GetTypeString(GridCellType type, BaseControl control, BoolSerializtionType bool_type)
        {
            string typestr = null;
            switch (type)
            {
                case GridCellType.Check:
                   if (bool_type == BoolSerializtionType.yesno)
                    typestr = "av:ДаНет";
                    else if (bool_type == BoolSerializtionType.boolean)
                        typestr = "av:Boolean";
                    break;
                case GridCellType.String:
                    string mask = GetMask(control).Mask;
                    if (String.IsNullOrEmpty(mask))
                        typestr = "av:String";
                    else
                        typestr = "av:" + names[mask + "#" + GetMask(control).Description];
                    break;
                case GridCellType.Date:
                    typestr = "av:Date";
                    break;
                case GridCellType.Integer:
                    typestr = "av:Long";
                    break;
                case GridCellType.Double:
                    typestr = "av:Double";
                    break;
                case GridCellType.Label:
                    typestr = "av:String";
                    break;
                case GridCellType.List:
                    DictionaryList dl = GetDictionaryList(control);
                    if (dl != null)
                        typestr = "av:" + dl.Name;
                    break;
            }
            return typestr;
        }

        DictionaryList GetDictionaryList(BaseControl bs)
        {
            if (bs is GridCellItem)
                return (bs as GridCellItem).Source;
            else if (bs is GridAddedRowColumn)
                return (bs as GridAddedRowColumn).Source;
            else if (bs is ComboBox)
                return (bs as ComboBox).Source;
            return null;
        }

        MaskItem GetMask(BaseControl control)
        {
            if (control is SimpleEdit)
                return (control as SimpleEdit).Mask;
            if (control is GridAddedRowColumn)
                return (control as GridAddedRowColumn).Mask;
            if (control is GridCellItem)
                return (control as GridCellItem).Mask;
            return null;
        }

        void GenerateSimpleElement(XmlWriter writer, SimpleEdit se)
        {
            string name = se.SerializingTag;
            string type = GetTypeString(se.DataType, se, se.boolSerializationType);

            string minOccurs = string.Empty;
            if ((se.IsMandatoryToFill) && !(se.Parent is MetaConrol))
            {
                minOccurs = "1";
            }
            else
            {
                minOccurs = "0";
            }
            string description = se.Caption;
            
            GenerateElement(writer, name, type, minOccurs, description, null);
        }

        void GenerateElement(XmlWriter writer, string name, string type, string minOccurs, string description, string maxOccurs)
        {
            writer.WriteStartElement("xs", "element", xs);
            writer.WriteAttributeString("name", name);
            writer.WriteAttributeString("type", type);
            writer.WriteAttributeString("minOccurs", minOccurs);
            if (!String.IsNullOrEmpty(maxOccurs))
                writer.WriteAttributeString("maxOccurs", maxOccurs);
            writer.WriteStartElement("xs", "annotation", xs);
            writer.WriteStartElement("xs", "appinfo", xs);
            writer.WriteElementString("description", description);
            writer.WriteEndElement();

            writer.WriteElementString("xs","documentation",xs, description);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        void GenerateStandartTypes(XmlWriter writer, Dictionary<string, BaseControl> simple)
        {
            GenerateSimpleType(writer, "Boolean", "xs:boolean", null, null);
            GenerateSimpleType(writer, "Date", "xs:date", null, null);
            GenerateSimpleType(writer, "Guid", "xs:string", null, @"(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}");
            GenerateSimpleType(writer, "Integer", "xs:int", null, null);
            GenerateSimpleType(writer, "Long", "xs:long", null, null);
            GenerateSimpleType(writer, "Double", "xs:double", null, null);
            GenerateSimpleType(writer, "String", "xs:string", null, null);

            foreach (KeyValuePair<string, BaseControl> kvp in simple)
            {                
                MaskItem mask = GetMask(kvp.Value);
                string name = names[kvp.Key];
                string description = mask.Description;
                GenerateSimpleType(writer, name, "xs:string", description, mask.ToXSDPattern());
            }

            foreach (DictionaryList dict in report.MetaData.Dictionarys)
            {
                GenerateEnumerationType(writer, dict);
            }

            // Да/нет
            DictionaryList yesno = new DictionaryList();
            yesno.Name = "ДаНет";
            yesno.Caption = "Да/Нет";
            yesno.Add(new DictionaryListItem() { Code = "Да", Description = "Да" });
            yesno.Add(new DictionaryListItem() { Code = "Нет", Description = "Нет" });
            GenerateEnumerationType(writer, yesno);
        }

        void GenerateVocabularyType(XmlWriter writer, string name, string description)
        {
            writer.WriteStartElement("xs", "complexType", xs);
            writer.WriteAttributeString("name", name);
            writer.WriteStartElement("xs", "annotation", xs);
            writer.WriteStartElement("xs", "appinfo", xs);
            writer.WriteElementString("description", description);
            writer.WriteElementString("type", "Vocabulary");
            writer.WriteStartElement("baseTypes");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteElementString("xs", "documentation", xs, description);
            writer.WriteEndElement();

            writer.WriteStartElement("xs", "sequence", xs);
            GenerateElement(writer, "Код", "av:" + name + "Код", "1", "Код", null);
            GenerateElement(writer, "Описание", "av:String", "0", "Описание", null);
            GenerateElement(writer, "Активно", "av:Boolean", "0", "Активно", null);
            writer.WriteEndElement();
            
            writer.WriteEndElement();
        }

        void GenerateEnumerationType(XmlWriter writer, DictionaryList dict)
        {
            writer.WriteStartElement("xs", "simpleType", xs);
            writer.WriteAttributeString("name", dict.Name + "Код");
            writer.WriteStartElement("xs", "annotation", xs);
            writer.WriteStartElement("xs", "appinfo", xs);
            writer.WriteElementString("description", dict.Caption);
            writer.WriteEndElement();
            writer.WriteElementString("xs", "documentation", xs, dict.Caption);
            writer.WriteEndElement();
            writer.WriteStartElement("xs", "restriction", xs);
            writer.WriteAttributeString("base", "xs:string");

            foreach (var item in dict.Items)
            {
                writer.WriteStartElement("xs", "enumeration", xs);
                writer.WriteAttributeString("value", item.Code);
                writer.WriteStartElement("xs", "annotation", xs);
                writer.WriteStartElement("xs", "appinfo", xs);
                writer.WriteElementString("description", item.Description);
                writer.WriteEndElement();
                writer.WriteElementString("xs", "documentation", xs, item.Description);
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        void GenerateSimpleType(XmlWriter writer, string name, string restriction, string description, string pattern)
        {
            writer.WriteStartElement("xs", "simpleType", xs);
            writer.WriteAttributeString("name", name);
            writer.WriteStartElement("xs", "annotation", xs);
            writer.WriteStartElement("xs", "appinfo", xs);
            writer.WriteStartElement("description");
            if (description != null)
                writer.WriteString(description);
            writer.WriteEndElement();
            writer.WriteElementString("type", "SimpleClass");
            writer.WriteEndElement();
            writer.WriteStartElement("xs", "documentation", xs);
            if (description != null)
                writer.WriteString(description);
            writer.WriteEndElement();
            writer.WriteEndElement();
            if (restriction != null)
            {
                writer.WriteStartElement("xs", "restriction", xs);
                writer.WriteAttributeString("base", restriction);
                if (pattern != null)
                {
                    writer.WriteStartElement("xs", "pattern", xs);
                    writer.WriteAttributeString("value", pattern);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }
    }
}
