﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.Mediator;
using aetp.Common.View;
using System.Threading;
using System.Windows.Forms;
using aetp.Common.View.Splash;

namespace aetp.Common
{
    public class ConstructorApp : IAnketaApp
    {
        public void Run()
        {
            GetMainForm().Show();
        }

        public Form GetMainForm()
        {
            ConstructorView view = new ConstructorView();
            ConstructorMediator mediator = new ConstructorMediator(view,"");
            Splash splash = new Splash(view, ViewType.Constructor);
            return splash;
        }

        public Form GetMainForm(string filename)
        {
            ConstructorView view = new ConstructorView();
            ConstructorMediator mediator = new ConstructorMediator(view,filename);
            Splash splash = new Splash(view, ViewType.Constructor);
            return splash;
        }
    }
}
