﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.View;
using aetp.Common.ControlModel;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Windows.Forms;
using aetp.Common.Serializer;
using aetp.Common.Event;
using aetp.Common.Validator;
using aetp.Utils;
using aetp.Common.View.Dialog;
using System.Text.RegularExpressions;
using aetp.Common;

namespace aetp.Common.Mediator
{
    public class ConstructorMediator : BaseMediator
    {
        bool IsNotSave = false;
        DependantControl dependantControls = new DependantControl();
        
        #region Event handler

        protected override void tlControls_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (e.Node != null)
                (View as ConstructorView).PropertyGrid.SelectedObject = View.GetFocusedControl();
        }

        #region Add Control

        void PropertyGrid_PropertyValueChanged(object s, System.Windows.Forms.PropertyValueChangedEventArgs e)
        {
            ((s as PropertyGrid).SelectedObject as BaseControl).tlItem.SetValue("Name", ((s as PropertyGrid).SelectedObject as BaseControl).Name);
            ((s as PropertyGrid).SelectedObject as BaseControl).tlItem.SetValue("Caption", ((s as PropertyGrid).SelectedObject as BaseControl).Caption);
            Constructor_Title();
            (View as ConstructorView).bbiSave.Enabled = true;
        }

        void tlControls_CellValueChanged(object s, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
        {
            Constructor_Title();
            (View as ConstructorView).bbiSave.Enabled = true;          
        }

        void ConstructorMediator_AddNewConrol(object sender, AddControlEventArgs args)
        {
            if (Report != null)
            {
                BaseControl parentControl = Report;
                Type t = args.type;

                if (View.tlControls.FocusedNode != null)
                {
                    parentControl = View.GetFocusedControl();
                }
                if (!parentControl.CanContains(t))
                {
                    parentControl = parentControl.Parent;
                }
                if (parentControl != null && parentControl.CanContains(t))
                {
                    BaseControl control = (BaseControl)Activator.CreateInstance(t);
                    parentControl.AddChildren(control, Report);
                    PostProcessingControlAdd(control);
                    (View as ConstructorView).bbiSave.Enabled = true;
                }
                else
                {
                    System.Windows.MessageBox.Show("Данная операция невозможна");
                }
            }
        }


        void ConstructorMediator_AddNewEditConrol(object sender, AddControlEditEventArgs args, SimpleDataType dt)
        {
            if (Report != null)
            {
                BaseControl parentControl = Report;
                Type t = args.type;

                if (View.tlControls.FocusedNode != null)
                {
                    parentControl = View.GetFocusedControl();
                }
                if (!parentControl.CanContains(t))
                {
                    parentControl = parentControl.Parent;
                }
                if (parentControl != null && parentControl.CanContains(t))
                {
                    SimpleEdit control = (SimpleEdit)Activator.CreateInstance(t);
                    control.DataType = dt;
                    parentControl.AddChildren(control, Report);
                    PostProcessingControlAdd(control);
                    (View as ConstructorView).bbiSave.Enabled = true;
                }
                else
                {
                    System.Windows.MessageBox.Show("Данная операция невозможна");
                }
            }
        }
                
        public override void PostProcessingControlAdd(BaseControl control)
        {
            if (control is Link)
            {
                foreach (BaseControl c in control.Parent.Children)
                {
                    if (c is Page)
                        (control as Link).LinkPages.Add(c as Page);
                }
            }
            if (control is aetp.Common.ControlModel.ComboBox)
            {
                aetp.Common.ControlModel.ComboBox cb = control as aetp.Common.ControlModel.ComboBox;
                cb.Items = new DictionaryCollection(Report.MetaData.Dictionarys);
            }
            
            if (control is Page)
                UpdateLinkControls(control.Parent);
        }

        #endregion

        void ConstructorMediator_RemoveFocusedControl(object sender, RemoveControlEventArgs args)
        {
            BaseControl control = View.GetFocusedControl();
            
            if (control != null)
            {
                control.Remove();
                (View as ConstructorView).bbiSave.Enabled = true;
                if (control is Page)
                    UpdateLinkControls(control.Parent);
            }
        }

        void UpdateLinkControls(BaseControl page)
        {
            if (Report != null)
            {
                LinkPageCollection pages = new LinkPageCollection();
                if (page != null)
                {
                    foreach (BaseControl control in page.Children)
                    {
                        if (control is Page)
                            pages.Add(control as Page);
                    }
                    foreach (BaseControl control in page.Children)
                    {
                        if (control is Link)
                            (control as Link).LinkPages = pages;
                    }
                }
            }
        }

        #endregion

        #region Constructors

        public ConstructorMediator(BaseView view,string filename) : base(view)
        {
            ConstructorView v = View as ConstructorView;
            v.New += new EventHandler(v_New);
            v.Save += new EventHandler(v_SaveAll);
            v.AddNewEditConrol += new ConstructorView.AddControlEditHandler(ConstructorMediator_AddNewEditConrol);
            v.AddNewConrol += new ConstructorView.AddControlHandler(ConstructorMediator_AddNewConrol);
            v.RemoveFocusedControl += new ConstructorView.RemoveControlHandler(ConstructorMediator_RemoveFocusedControl);
            v.AddNewDictionary += new AddListHandler(v_AddNewDictionary);
            v.Import += new EventHandler(v_Import);
            v.LoadXml += new ConstructorView.LoadHandler(v_Load);
            v.PropertyGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(PropertyGrid_PropertyValueChanged);
            v.tlControls.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(tlControls_CellValueChanged);
            v.SaveAs += new EventHandler(v_SaveAs);
            v.LoadFile += new EventHandler(v_LoadFile);
            v.RunEditor += new EventHandler(v_RunEditor);
            v.OpenEditor += new EventHandler(v_OpenEditor);
            v.XSDGenerate += new EventHandler(v_XSDGenerate);
            v.Dublicate += new EventHandler(v_Dublicate);

            if (filename.Length > 0)
            {
                Report report;

                using (XmlTextReader reader = new XmlTextReader(filename))
                {
                    report = new FormSerializer().Deserialize(reader, true);
                }
                if (report == null) return;
                report.RunMode = ViewType.Constructor;
                this.Report = report;
                View.Report = report;
                Constructor_Title();
                (View as ConstructorView).bbiSave.Enabled = false;
                Report_FileName = filename;
                this.Report.DesignerSave = true;
            }
            if (!FileAssociation.IsAssociated(".aff"))
            {
                try
                {
                    string ico_path = System.Windows.Forms.Application.ExecutablePath.Remove(System.Windows.Forms.Application.ExecutablePath.LastIndexOf('\\'));
                    ico_path = Path.Combine(ico_path, "anketa.ico");
                    FileAssociation.Associate(".aff", "Designer.exe", "aff File", ico_path, System.Windows.Forms.Application.ExecutablePath);
                }
                catch (Exception ex)
                {
                    ErrorHelper.Show(ex);
                }
            }
        }

        #endregion

        void v_Dublicate(object sender, EventArgs e)
        {
            if (View.tlControls.FocusedNode != null && View.tlControls.FocusedNode.Tag is BaseControl)
            {
                if (!(View.tlControls.FocusedNode.Tag is Report))
                {
                    BaseControl bs = (View.tlControls.FocusedNode.Tag as BaseControl).Clone() as BaseControl;
                    if ((bs is Page || bs is BaseGrid))
                    {
                        if (bs != null)
                            Report.AddChildren(bs, Report);
                    }
                    else
                        MessageBox.Show("Нельзя клонировать данный элемент");
                }
                else
                    MessageBox.Show("Нельзя клонировать данный элемент");
            }
        }

        void v_UpdateTitle(object sender, EventArgs e)
        {
            Constructor_Title();
        }

        void v_XSDGenerate(object sender, EventArgs e)
        {
            if (Report != null)
            {
                MemoryStream stream = new MemoryStream();
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.OmitXmlDeclaration = false;
                XmlWriter xmlTextWriter = XmlWriter.Create(stream, settings);
                new XSDGenerator(Report).Generate(xmlTextWriter);
                string str = Encoding.UTF8.GetString(stream.ToArray());
                (View as ConstructorView).CodeStream = str;
            }
        }

        public string _report_filename;
        public string Report_FileName
        {
            get
            {
                return _report_filename;
            }
            set
            {
                _report_filename = value;
            }
        }


        void v_New(object sender, EventArgs e)
        {
            DialogResult result = DialogResult.OK;
            if (IsNotSave)
            {
                string str_warning = String.Format("Отчет '{0}' не был сохранен, хотите сохранить его?", Report.Name);
                result = MessageBox.Show(str_warning, "Предупреждение", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.OK)
                {
                    v_SaveAs(this, null);
                }
            }
            if (result != DialogResult.Cancel)
            {
                this.Report = new Report();
                this.Report.RunMode = ViewType.Constructor;
                View.Report = Report;
                Constructor_Title();
                (View as ConstructorView).bbiSave.Enabled = true;
                Report_FileName = "";
            }
        }

        void v_RunEditor(object sender, EventArgs e)
        {
            FormSerializer serializer = new FormSerializer();
            //clone
            string str_serializable = serializer.Serialize(Report);
            Report report = serializer.Deserialize(new StringReader(str_serializable));
            report.InitSouceMetaControlHandler();
            dependantControls.Init(report);
            report.InitialDefaultValues();
            report.ReportChange = false;
            report.RunMode = ViewType.Editor;
            new EditorApp(report).Run();
        }

        void v_OpenEditor(object sender, EventArgs e)
        {
            Refresh_Page();
        }

        void view_RefreshPage(object sender, EventArgs e)
        {
            (sender as EditorPreView).Close();
            Refresh_Page();
            
        }

        void Constructor_Title()
        {
            (View as ConstructorView).Title = "Анкета.Конструктор " + this.GetType().Assembly.GetName().Version.ToString() + " - " + Report.Caption;
        }

        void Refresh_Page()
        {
            if (Report != null)
            {
                FormSerializer serializer = new FormSerializer();
                string str_serializable = serializer.Serialize(Report);
                Report report = serializer.Deserialize(new StringReader(str_serializable));
                if (report != null)
                    report.RunMode = ViewType.Editor;
                report.InitSouceMetaControlHandler();
                dependantControls.Init(report);
                report.InitialDefaultValues();
                EditorPreView view = new EditorPreView();
                EditorMediator mediator = new EditorMediator(view, report,"");
                view.Report = report;
                view.CloseWindow += new CloseWindowEventHandler(view_CloseWindow);
                view.Show((View as ConstructorView).dockPanel1);
                view.ShowFirstPage();
                report.ReportChange = false;
                view.RefreshPage += new EventHandler(view_RefreshPage);
            }
            else
            {
                MessageBox.Show("Невозможно открыть пустой отчёт");
            }
        }
        void view_CloseWindow(object sender, CloseWindowEventArgs e)
        {
            if (e.report != null)
                ValidationCollection.RemoveWindow(e.report.guid);
        }

        void v_LoadFile(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Anket File Form (*.aff)|*.aff";
            if (ofd.ShowDialog() == DialogResult.OK)
            {                
                Report report;

                using (XmlTextReader reader = new XmlTextReader(ofd.FileName))
                {
                    report = new FormSerializer().Deserialize(reader, true);
                }
                if (report == null) return;
                report.RunMode = ViewType.Constructor;
                this.Report = report;
                View.Report = report;
                Constructor_Title();
                (View as ConstructorView).bbiSave.Enabled = false;
                Report_FileName = ofd.FileName;
                this.Report.DesignerSave = true;
            }
        }

        bool IsError = false;
        private void  ValidateName(BaseControl parentControl)
        {
            List<BaseControl> report_controls = new List<BaseControl>();
            GetChildren(Report, report_controls);
            foreach (BaseControl control in parentControl.Children)
            {
                EqName(control, report_controls, parentControl);

                if ((control.Children.Count > 0) && (!IsError))
                    ValidateName(control);
                if (control is MetaGrid && !IsError)
                    ValidateName(control);
            }
            if (parentControl is MetaGrid)
            {
                foreach (BaseControl control in (parentControl as MetaGrid).Tables)
                {
                    EqName(control, report_controls, parentControl);

                    if ((control.Children.Count > 0) && !IsError)
                        ValidateName(control);
                    if (control is MetaGrid && (!IsError))
                        ValidateName(control);
                }
            }
        }

        void EqName(BaseControl control, List<BaseControl> report_controls, BaseControl parentControl)
        {
            if (!IsError)
            {
                if (String.IsNullOrEmpty(control.Name))
                {
                    IsError = true;
                    MessageBox.Show(String.Format("Раздел: {0};\nЭлемент {1};\nОшибка: {2};", parentControl, control.Name, "Не всем элементам присвоены имена"));
                }
            }
            if (!IsError)
            {
                Regex rgx = new Regex(@"^[a-zA-Zа-яА-Я]{1}[a-zA-Zа-яА-Я0-9_]*$", RegexOptions.IgnoreCase);
                if (!rgx.IsMatch(control.Name))
                {
                    IsError = true;
                    MessageBox.Show(String.Format("Раздел: {0};\nЭлемент {1};\nОшибка: {2};", parentControl, control.Name, "Невозможно присвоить данное значение!\nИмя должно начинаться с буквы\nи состоять из букв, цифр, сиволов -,_"));

                }
            }

            for (int i = 0; i < report_controls.Count(); i++)
            {
                if (!IsError)
                {
                    if (report_controls[i] == control)
                        continue;
                    if (report_controls[i].Name == control.Name)
                    {
                        IsError = true;
                        MessageBox.Show(String.Format("Раздел: {0};\nЭлемент {1};\nОшибка: {2};", parentControl, control.Name, "Элемент с данным именем уже существует!"));
                        
                        return;
                    }
                }
            }
        }

        public void GetChildren(Page p, List<BaseControl> report_controls)
        {
            if (p == null)
                return;
            foreach (aetp.Common.ControlModel.BaseControl ctrl in p.Children)
            {
                if (ctrl is Page) GetChildren(ctrl as Page,report_controls);
                if (ctrl is MetaConrol) GetChildrenMetaControl(ctrl as MetaConrol, report_controls);
                if (ctrl is MetaGrid)
                {
                    foreach (BaseGrid grid in (ctrl as MetaGrid).Tables)
                    {
                        report_controls.Add(grid);
                    }
                }
                report_controls.Add(ctrl);
            }            
        }

        public void GetChildrenMetaControl(MetaConrol p, List<BaseControl> report_controls)
        {
            foreach (aetp.Common.ControlModel.BaseControl ctrl in p.Children)
            {
                if (ctrl is Page) GetChildren(ctrl as Page, report_controls);
                if (ctrl is MetaConrol)
                    GetChildrenMetaControl(ctrl as MetaConrol, report_controls);
                if (ctrl is MetaGrid)
                {
                    foreach (BaseGrid grid in (ctrl as MetaGrid).Tables)
                    {
                        report_controls.Add(grid);
                    }
                }
                report_controls.Add(ctrl);
            }
        }

        void v_SaveAs(object sender, EventArgs e)
        {
            IsError = false;
            if (Report != null)
            {
                try
                {
                    Report.validator.Validate();
                    ValidateName(Report);
                    // найти одинаковые ячейки, строки и колонки в таблицах
                    string equals = Report.cellvalidator.Operation(Report);
                    if (equals.Length > 1)
                    {
                        IsError = true;
                        MessageBox.Show(equals, "Найдены совпадающие имена ячеек");
                    }

                    if (!IsError)
                    {
                        if (!Report.Name.ToString().Contains(' '))
                        {
                            SaveFileDialog sfd = new SaveFileDialog();
                            sfd.InitialDirectory = AppGlobal.Instance.Config.ModelsPath;
                            sfd.FileName = Report.Name;
                            sfd.Filter = "Anket File Form (*.aff)|*.aff";
                            if (sfd.ShowDialog() == DialogResult.OK)
                            {
                                XmlTextWriter xmlTextWriter = new XmlTextWriter(sfd.FileName, Encoding.UTF8);
                                new FormSerializer().Serialize(xmlTextWriter, Report);                               
                                Report.DesignerSave = true;
                                (View as ConstructorView).bbiSave.Enabled = false;
                                Report_FileName = sfd.FileName;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Имя отчёта не должно содержать пробелы.");
                        }
                    }                    
                }
                catch (Exception ex)
                {
                    ErrorHelper.Show(ex);
                }
            }
            else
            {
                MessageBox.Show("Невозможно сохранить пустой отчёт");
            }
        }


        void v_SaveAll(object sender, EventArgs e)
        {
            IsError = false;
            if (Report != null)
            {
                try
                {
                    Report.validator.Validate();
                    ValidateName(Report);
                    // найти одинаковые ячейки, строки и колонки в таблицах
                    string equals = Report.cellvalidator.Operation(Report);
                    if (equals.Length > 1)
                    {
                        IsError = true;
                        MessageBox.Show(equals, "Найдены совпадающие имена ячеек");
                    }

                    if (!IsError)
                    {                        
                        if (Report.DesignerSave)
                        {
                            string path = string.Empty;                            

                            if (Report_FileName.Length > 0)
                            {
                                path = Report_FileName;
                            }
                            else
                            {
                                path = Path.Combine(AppGlobal.Instance.Config.ModelsPath, Report.Name + ".aff");
                            }
                            XmlTextWriter xmlTextWriter = new XmlTextWriter(path, Encoding.UTF8);
                            new FormSerializer().Serialize(xmlTextWriter, Report);                          
                            (View as ConstructorView).bbiSave.Enabled = false;
                            Report.DesignerSave = true;
                        }
                        else
                        {
                            SaveFileDialog sfd = new SaveFileDialog();
                            sfd.InitialDirectory = AppGlobal.Instance.Config.ModelsPath;
                            sfd.FileName = Report.Name;
                            sfd.Filter = "Anket File Form (*.aff)|*.aff";
                            if (sfd.ShowDialog() == DialogResult.OK)
                            {
                               
                                    XmlTextWriter xmlTextWriter = new XmlTextWriter(sfd.FileName, Encoding.UTF8);
                                    new FormSerializer().Serialize(xmlTextWriter, Report);                                  
                                    Report.DesignerSave = true;
                                    (View as ConstructorView).bbiSave.Enabled = false;
                                    Report_FileName = sfd.FileName;
                                
                            }
                        }                      
                    }
                }
                catch (Exception ex)
                {
                    ErrorHelper.Show(ex);
                }
            }
            else
            {
                MessageBox.Show("Невозможно сохранить пустой отчёт");
            }
        }
        
        
        void v_AddNewDictionary(object sender, MetaDataListAddedEventArgs e)
        {
            if (Report != null && Report.CanContains(e.type))
            {
                DictionaryList list = (DictionaryList)Activator.CreateInstance(e.type);
                
                list.Parent = Report;
                Report.MetaData.AddList(list);
            }
        }

        void v_Import(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Anket File Form (*.aff)|*.aff";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                SelectImportElementType select = new SelectImportElementType();
                if (select.ShowDialog() != DialogResult.OK)
                    return;
                Report report;

                using (XmlTextReader reader = new XmlTextReader(ofd.FileName))
                {
                    report = new FormSerializer().Deserialize(reader, true);
                }
                if (select.elementType == ImportElementType.dictionary)
                    ImportDictionary(report);
                else if (select.elementType == ImportElementType.page)
                    ImportPage(report);
                else ImportPrintForm(report);
                (View as ConstructorView).bbiSave.Enabled = true;
            }
        }       

        void ImportPage(Report report)
        {
            if (report != null && Report != null)
            {
                SelectImportPages importDictionary = new SelectImportPages(Report, report);
                if (importDictionary.ShowDialog() == DialogResult.OK)
                {
                    foreach (BaseControl page in importDictionary.transferList)
                    {
                        Page new_page = new Page();
                        new_page.Caption = page.Caption;
                        new_page.Children = page.Children;
                        new_page.EditorValidators = page.EditorValidators;
                        new_page.EditValue = page.EditValue;
                        new_page.Index = (page as Page).Index;
                        new_page.IsChecked = (page as Page).IsChecked;
                        new_page.IsCleaningOnUnvisible = page.IsCleaningOnUnvisible;
                        new_page.IsMandatoryElementExist = (page as Page).IsMandatoryElementExist;
                        new_page.IsMandatoryToFill = page.IsMandatoryToFill;
                        new_page.IsVisible = (page as Page).IsVisible;
                        new_page.Items = (page as Page).Items;

                        new_page.ListControl = (page as Page).ListControl;
                        new_page.Name = page.Name;
                        new_page.PrintingAlignment = page.PrintingAlignment;
                        new_page.ReportChange = (page as Page).ReportChange;
                        new_page.SerializingTag = page.SerializingTag;
                        new_page.Tag = page.Tag;
                        new_page.Visible_Page = (page as Page).Visible_Page;

                        ReportUtils.DoAction(new_page, new_page.Children, importAction);
                        Report.AddChildren(new_page, Report);
                    }
                }
                (View as ConstructorView).bbiSave.Enabled = true;
            }
            else if (report == null)
            {
                MessageBox.Show("Импортируемый отчет поврежден или пустой");
            }
            else if (Report == null)
            {
                MessageBox.Show("Сначала необходимо создать отчет");
            }
        }

        void ImportPrintForm(Report report)
        {
            if (report != null && Report != null)
            {
                Report.PrintingForm = report.PrintingForm;
                (View as ConstructorView).bbiSave.Enabled = true;
            }
            else if (report == null)
            {
                MessageBox.Show("Импортируемый отчет поврежден или пустой");
            }
            else if (Report == null)
            {
                MessageBox.Show("Сначала необходимо создать отчет");
            }
        }

        void importAction(BaseControl parent, BaseControl bs)
        {
            bs.Root = Report;
            bs.Parent = parent;
            if (bs is aetp.Common.ControlModel.ComboBox)
            {
                aetp.Common.ControlModel.ComboBox combo = bs as aetp.Common.ControlModel.ComboBox;
                if (combo.Source != null)
                {
                    bool is_finded = false;
                    foreach (var d in Report.MetaData.Dictionarys)
                    {
                        if (d.Name == combo.Source.Name)
                        {
                            combo.Source = d;
                            is_finded = true;
                            break;
                        }
                    }
                    if (!is_finded)
                        combo.Source = null;
                }
            }
        }

        void ImportDictionary(Report report)
        {            
            if (report != null && Report != null)
            {
                SelectImportDicitonary importDictionary = new SelectImportDicitonary(Report, report);
                if (importDictionary.ShowDialog() == DialogResult.OK)
                {
                    report = importDictionary.Report;
                    foreach (DictionaryList dict in report.MetaData.Dictionarys)
                    {
                        DictionaryList list = new DictionaryList();
                        
                        list.Parent = Report;
                        list.Items = dict.Items;
                        list.Name = dict.Name;
                        list.Tag = dict.Tag;
                        list.SerializingTag = dict.SerializingTag;
                        list.Caption = dict.Caption;

                        Report.MetaData.AddList(list);
                    }
                }
                (View as ConstructorView).bbiSave.Enabled = true;
            }
            else if (report == null)
            {
                MessageBox.Show("Импортируемый отчет поврежден или пустой");
            }
            else if (Report == null)
            {
                MessageBox.Show("Сначала необходимо создать отчет");
            }
        }

        void v_Load(object sender, LoadEventArgs args)
        {            
            this.Report = new FormSerializer().Deserialize(args.XmlReader);
        }
                
        void v_Save(object sender, EventArgs e)
        {            
            try
            {
                (View as ConstructorView).CodeStream = new FormSerializer().Serialize(Report);
            }
            catch (Exception ex)
            {
                ErrorHelper.Show(ex);
            }
        }

    }
}
