﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;

namespace aetp.Common
{
   
    public static class MainFactory
    {
        public static IAnketaApp GetApp(ViewType type, params object[] Params)
        {
            switch (type)
            {
                case ViewType.Constructor:
                    AppGlobal.Instance.RunMode = ViewType.Constructor;
                    return new ConstructorApp();
                case ViewType.Editor:
                    AppGlobal.Instance.RunMode = ViewType.Editor;
                    return new EditorApp(Params == null ? null : Params[0] as Report);
                default: throw new Exception("Данный тип формы не поддерживается приложением");
            }
        }
    }
}

