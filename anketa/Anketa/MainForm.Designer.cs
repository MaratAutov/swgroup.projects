﻿namespace aetp.Anketa
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnShowConstructor = new DevExpress.XtraEditors.SimpleButton();
            this.btnShowEditor = new DevExpress.XtraEditors.SimpleButton();
            this.meCode = new DevExpress.XtraEditors.MemoEdit();
            this.btnShowQueryConstructor = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadEditor = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.meCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnShowConstructor
            // 
            this.btnShowConstructor.Location = new System.Drawing.Point(34, 12);
            this.btnShowConstructor.Name = "btnShowConstructor";
            this.btnShowConstructor.Size = new System.Drawing.Size(307, 38);
            this.btnShowConstructor.TabIndex = 0;
            this.btnShowConstructor.Text = "Запустить Конструктор";
            this.btnShowConstructor.Click += new System.EventHandler(this.btnShowConstructor_Click);
            // 
            // btnShowEditor
            // 
            this.btnShowEditor.Location = new System.Drawing.Point(34, 100);
            this.btnShowEditor.Name = "btnShowEditor";
            this.btnShowEditor.Size = new System.Drawing.Size(307, 38);
            this.btnShowEditor.TabIndex = 1;
            this.btnShowEditor.Text = "Show Editor";
            this.btnShowEditor.Visible = false;
            this.btnShowEditor.Click += new System.EventHandler(this.btnShowEditor_Click);
            // 
            // meCode
            // 
            this.meCode.Location = new System.Drawing.Point(34, 144);
            this.meCode.Name = "meCode";
            this.meCode.Size = new System.Drawing.Size(307, 156);
            this.meCode.TabIndex = 2;
            this.meCode.Visible = false;
            // 
            // btnShowQueryConstructor
            // 
            this.btnShowQueryConstructor.Location = new System.Drawing.Point(34, 306);
            this.btnShowQueryConstructor.Name = "btnShowQueryConstructor";
            this.btnShowQueryConstructor.Size = new System.Drawing.Size(307, 38);
            this.btnShowQueryConstructor.TabIndex = 3;
            this.btnShowQueryConstructor.Text = "Show Query Constructor";
            this.btnShowQueryConstructor.Visible = false;
            this.btnShowQueryConstructor.Click += new System.EventHandler(this.btnShowQueryConstructor_Click);
            // 
            // btnLoadEditor
            // 
            this.btnLoadEditor.Location = new System.Drawing.Point(34, 56);
            this.btnLoadEditor.Name = "btnLoadEditor";
            this.btnLoadEditor.Size = new System.Drawing.Size(307, 38);
            this.btnLoadEditor.TabIndex = 4;
            this.btnLoadEditor.Text = "Запустить Редактор";
            this.btnLoadEditor.Click += new System.EventHandler(this.btnLoadEditor_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 355);
            this.Controls.Add(this.btnLoadEditor);
            this.Controls.Add(this.btnShowQueryConstructor);
            this.Controls.Add(this.meCode);
            this.Controls.Add(this.btnShowEditor);
            this.Controls.Add(this.btnShowConstructor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.meCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnShowConstructor;
        private DevExpress.XtraEditors.SimpleButton btnShowEditor;
        private DevExpress.XtraEditors.MemoEdit meCode;
        private DevExpress.XtraEditors.SimpleButton btnShowQueryConstructor;
        private DevExpress.XtraEditors.SimpleButton btnLoadEditor;
    }
}

