﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using aetp.Common;
using System.Xml.Serialization;
using aetp.Common.ControlModel;
using System.IO;
using aetp.Common.View;
using aetp.Common.Mediator;
using aetp.Common.Serializer;
using System.Xml;

namespace aetp.Anketa
{
    public partial class MainForm : XtraForm
    {
        public MainForm()
        {
            InitializeComponent();
            
#if DEBUG
            this.Size = new Size(this.Size.Width, 400);
            btnShowEditor.Visible = true;
            btnShowQueryConstructor.Visible = true;
            meCode.Visible = true;
#else
            this.Size = new Size(this.Size.Width, 140);
#endif

        }

        private void btnShowConstructor_Click(object sender, EventArgs e)
        {
            MainFactory.GetApp(ViewType.Constructor).Run();
        }

        private void btnShowEditor_Click(object sender, EventArgs e)
        {
            try
            {
                EditorRunner.RunFromString(meCode.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }           
        }

        private void btnShowQueryConstructor_Click(object sender, EventArgs e)
        {
            ValidatorConstructorView view = new ValidatorConstructorView();
            ValidatorConstructorMediator mediator = new ValidatorConstructorMediator(view);
            view.Show();
        }

        private void btnLoadEditor_Click(object sender, EventArgs e)
        {
            EditorRunner.Run();
            //OpenFileDialog ofd = new OpenFileDialog();
            //ofd.Filter = "Anket File Form (*.aff)|*.aff";
            //if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    try
            //    {
            //        EditorRunner.RunFromFile(ofd.FileName);
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.ToString());
            //    }
            //}
        }
    }
}
