﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using aetp.Common;

namespace Designer
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length > 0)
            {
                Application.Run(new ConstructorApp().GetMainForm(args[0]));
            }
            else
            {
                Application.Run(new ConstructorApp().GetMainForm());
            }            
        }
    }
}
