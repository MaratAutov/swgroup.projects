﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aetp.Utils
{
    public static class ErrorHelper
    {
        public static void Show(Exception ex)
        {
            new ErrorForm(ex).ShowDialog();
        }
    }
}
