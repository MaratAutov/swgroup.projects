﻿namespace aetp.Utils
{
    partial class ErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorForm));
            this.teMessage = new DevExpress.XtraEditors.TextEdit();
            this.meStackTrace = new DevExpress.XtraEditors.MemoEdit();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.teMessage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meStackTrace.Properties)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // teMessage
            // 
            this.teMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teMessage.Location = new System.Drawing.Point(13, 13);
            this.teMessage.Name = "teMessage";
            this.teMessage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.teMessage.Properties.Appearance.Options.UseBackColor = true;
            this.teMessage.Properties.ReadOnly = true;
            this.teMessage.Size = new System.Drawing.Size(390, 20);
            this.teMessage.TabIndex = 0;
            // 
            // meStackTrace
            // 
            this.meStackTrace.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.meStackTrace.Location = new System.Drawing.Point(13, 39);
            this.meStackTrace.Name = "meStackTrace";
            this.meStackTrace.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.meStackTrace.Properties.Appearance.Options.UseBackColor = true;
            this.meStackTrace.Properties.ReadOnly = true;
            this.meStackTrace.Size = new System.Drawing.Size(390, 172);
            this.meStackTrace.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(173, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "OK";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.10854F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.89146F));
            this.tableLayoutPanel1.Controls.Add(this.btnClose, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 217);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(415, 31);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // ErrorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 248);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.meStackTrace);
            this.Controls.Add(this.teMessage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ErrorForm";
            this.Text = "Ошибка";
            ((System.ComponentModel.ISupportInitialize)(this.teMessage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meStackTrace.Properties)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit teMessage;
        private DevExpress.XtraEditors.MemoEdit meStackTrace;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}