﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;

namespace aetp.Utils
{
    public partial class ErrorForm : XtraForm
    {
        public ErrorForm()
        {
            InitializeComponent();
        }

        public ErrorForm(Exception ex)
        {
            InitializeComponent();
            if (ex != null)
            {
                teMessage.Text = ex.Message;
                meStackTrace.Text = ex.StackTrace;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
