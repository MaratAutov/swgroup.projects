﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace aetp.Utils
{
    public static class Utils
    {
        public static string GetFieldName(string name)
        {
            int index = name.IndexOf('$');
            if (index != -1)
                return name.Substring(0, name.IndexOf('$'));
            else
                return name;
        }

        public static string GetNamePostfix(string name)
        {
            int index = name.IndexOf('$');
            if (index != -1)
                return name.Substring(index);
            else
                return null;
        }

        /// <summary>
        /// получение имени колонки из связки Таблица$Колонка
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetColumnName(string name)
        {
            int index = name.IndexOf('$');
            if (index != -1)
                return name.Substring(index + 1);
            else
                return name;
        }

        // проверка на то входит ли имя в текущее дерево объектов
        public static bool InTree(string nameTop, string nameDown)
        {
            string postfix1 = GetNamePostfix(nameTop);
            string postfix2 = GetNamePostfix(nameDown);
            if (postfix2 == null && postfix1 != null)
                return false;
            else if (postfix2 == null && postfix1 == null)
                return true;

            return postfix2.StartsWith(postfix1);
        }

        public static void ControlsDoAction(Control ctrl, Action<Control> action)
        {
            foreach (Control c in ctrl.Controls)
            {
                action(c);
                if (c.HasChildren)
                    ControlsDoAction(c, action);
            }
        }

    }
}
