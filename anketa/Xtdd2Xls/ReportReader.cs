﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using System.Xml;
using aetp.Common;
using System.Xml.XPath;

namespace Xtdd2Xls
{
    /// <summary>
    /// Заполнение EditValue контролов из xtdd по xpath
    /// </summary>
    public class ReportReader
    {
        Report report;
        XtddFile xml;

        public ReportReader(Report report)
        {
            this.report = report;   
        }

        public void Read(XtddFile xml)
        {
            this.xml = xml;
            ReportUtils.DoAction(report.Children, FillEditValue);
        }

        void FillEditValue(BaseControl bc)
        {
            if (bc is Grid)
            {
                var info = bc.Tag as GridMetaInfo;
                var grid = bc as Grid;
                if (info.Type == ControlType.GridAddedPage)
                    FillGridAddedPage(grid);
                else
                    FillGrid(grid);
            }
            else if (bc is SimpleEdit)
            {
                bc.EditValue = xml.GetValue(bc.Tag.ToString());
            }
        }

        void FillGridAddedPage(Grid grid)
        {
            var iterator = xml.GetNodesIterator((grid.Tag as GridMetaInfo).XPath);
            foreach (XPathNavigator node in iterator)
            {
                var row = new GridRow();
                row.Grid = grid;
                
                foreach (var col in grid.Columns)
                {
                    ColumnMetaInfo cmi = col.Tag as ColumnMetaInfo;
                    var cell = new GridCellItem();
                    cell.Type = cmi.Type;
                    row.Cells.Add(cell);
                    cell.EditValue = xml.GetValue(cmi.XPath, node);
                }
                grid.Rows.Add(row);
            }
        }

        void FillGrid(Grid grid)
        {
            var node = xml.GetNode((grid.Tag as GridMetaInfo).XPath);
            {
                for (int r = 0; r < grid.Rows.Count; r++)
                {
                    // фиксированные строки
                    if (grid.Rows[r].Tag == null)
                    {
                        for (int c = 0; c < grid.Columns.Count; c++)
                        {
                            if (grid.Rows[r].Cells[c].Type == GridCellType.String)
                            {
                                grid.Rows[r].Cells[c].EditValue = xml.GetValue(grid.Rows[r].Cells[c].Tag.ToString(), node);
                                
                            }
                            else if (grid.Rows[r].Cells[c].Type == GridCellType.Double)
                            {
                                grid.Rows[r].Cells[c].EditValue = xml.GetValue(grid.Rows[r].Cells[c].Tag.ToString(), node);

                            }
                        }
                    }
                    // мульти строки    
                    else
                    {
                        var iterator = xml.GetNodesIterator(grid.Rows[r].Tag.ToString(), node);
                        GridRow typeRow = grid.Rows[r];
                        grid.Rows.RemoveAt(r);
                        if (r!=0) r--;
                        if (iterator != null && iterator.Count != 0)
                        {
                            foreach (XPathNavigator itemRow in iterator)
                            {
                                var row = new GridRow();
                                row.Grid = grid;
                                for (int c = 0; c < grid.Columns.Count; c++)
                                {
                                    var cell = new GridCellItem();
                                    cell.Type = typeRow.Cells[c].Type;
                                    cell.EditValue = xml.GetValue(grid.Columns[c].Tag.ToString(), itemRow);
                                    row.Cells.Add(cell);
                                    
                                }
                                grid.Rows.Insert(r, row);
                                r++;
                            }
                        }
                    }
                }
            }
        }
    }
}
