﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common.ControlModel;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using aetp.Common.Control;
using aetp.Common;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System.Data;
using DevExpress.XtraEditors;
using System.Drawing;
using DevExpress.XtraPrinting.Localization;

namespace Xtdd2Xls
{
    /// <summary>
    /// Класс подготовки печатной формы по имеющемуся Report
    /// </summary>
    public class Printing
    {
        Report report;
        PrintingSystem ps;
        CompositeLink MainPage;
        string footer = "[Страница # из # страниц]";

        public Printing(Report report)
        {
            this.report = report;
            PreviewLocalizer.Active = new RussianPrintLocalizer();
        }

        void CreateDocument()
        {
            ps = new PrintingSystem();
            MainPage = new CompositeLink(ps);
            MainPage.Landscape = false;
            MainPage.PaperKind = System.Drawing.Printing.PaperKind.A4;
            AddHeader();
            DoAction(report.Children, AddLink);
            MainPage.Links.Add(new SignLink());
            MainPage.CreateDocument();
        }

        public void ExportToXls(string fileName)
        {
            CreateDocument();
            ps.ExportToXls(fileName);
        }

        public void Print()
        {
            CreateDocument();
            ps.PreviewRibbonFormEx.ShowDialog();
        }

        void AddHeader()
        {
            string reportName = report.Caption.ToUpper();
            PageHeaderFooter phf = MainPage.PageHeaderFooter as PageHeaderFooter;
            phf.Header.Content.Clear();
            phf.Header.Content.Add(reportName);
            AddMainPageHeaderFooter(MainPage, reportName, 5, 16);
            phf.Footer.Content.Clear();
            phf.Footer.Content.Add(footer);
            phf.Footer.LineAlignment = BrickAlignment.Far;
        }

        private void AddMainPageHeaderFooter(CompositeLink MainPage, string reportName, int paragraph, float size)
        {
            MainPage.Links.Add(new TextLink(new String('\n', paragraph), "Times New Roman"));
            TextLink link_header = new TextLink(reportName, "Times New Roman");
            link_header.FontStyle = FontStyle.Bold;
            link_header.emSize = size;
            link_header.HAlignment = StringAlignment.Center;
            MainPage.Links.Add(link_header);
            MainPage.Links.Add(new TextLink(new String('\n', paragraph), "Times New Roman"));
        }

        void DoAction(List<aetp.Common.ControlModel.BaseControl> ctrls, Action<aetp.Common.ControlModel.BaseControl> action)
        {
            foreach (var bs in ctrls)
            {
                action(bs);
                if (bs is aetp.Common.ControlModel.Page)
                    DoAction(bs.Children, action);
            }
        }

        void AddLink(aetp.Common.ControlModel.BaseControl bc)
        {
            if (bc is Grid)
            {
                Grid grid = bc as Grid;
                if (grid.Rows.Count == 0)
                    return;
                Font font = new Font("Times New Roman", 12, FontStyle.Regular);
                AddMainPageHeaderFooter(MainPage, bc.Caption, 0, 12);
                MainPage.Links.Add(aetp.Common.Printing.PrintGrid(GetDataTable(grid), font, false, grid));
            }
            else if (bc is aetp.Common.ControlModel.Page)
            {
                AddMainPageHeaderFooter(MainPage, bc.Caption, 1, 14);
            }
            else
            {
                MainPage.Links.Add(new TextLink(string.Format("{0}: {1}", bc.Caption, bc.EditValue), "Times New Roman"));
            }    
        }

        aetpGrid GetGrid(Grid grid)
        {
            aetpGrid gc = new aetpGrid();
            gc.Configure(grid);
            return gc;
        }

        DataTable GetDataTable(Grid grid)
        {
            DataTable dt = new DataTable();
            for (int c = 0; c < grid.Columns.Count; c++)
            {
                dt.Columns.Add(new DataColumn(grid.Columns[c].Name));
            }

            for (int r = 0; r < grid.Rows.Count; r++)
            {
                DataRow dr = dt.NewRow();
                for (int c = 0; c < grid.Columns.Count; c++)
                {
                    if (grid.Rows[r].Cells[c].Type == GridCellType.Double && grid.Rows[r].Cells[c].EditValue != null)
                    {
                        dr[c] = grid.Rows[r].Cells[c].EditValue.ToString().Replace(".", ",");
                    }
                    else
                    {
                        dr[c] = grid.Rows[r].Cells[c].EditValue;
                    }
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

    }
}
