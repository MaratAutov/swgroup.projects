﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using aetp.Common;

namespace Xtdd2Xls
{
    class Program
    {

        static void Main(string[] args)
        {            
            try
            {
                Console.OutputEncoding = System.Text.Encoding.UTF8;
                if (args.Length < 1 || args.Length > 2)
                    throw new ArgumentOutOfRangeException("Неправильное количество параметров");
                    
                XtddFile xtdd = new XtddFile(args[0]);
                Console.WriteLine("Model: {0}; RootName: {1}; Version: {2}", xtdd.XsdModel, xtdd.XsdRootName, xtdd.Version);
                var schema = XsdHelper.GetSchema(xtdd);
                ReportGenerator rg = new ReportGenerator(schema);
                var report = rg.Generate();
                ReportReader reader = new ReportReader(report);
                reader.Read(xtdd);
                var printer = new Printing(report);
                if (args.Length == 2)
                    printer.ExportToXls(args[1]);
                else
                    printer.Print();
                Console.WriteLine("Successfuly exported");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Xtdd2Xls raise exception: {0}", ex);
            }
        }
    }
}
