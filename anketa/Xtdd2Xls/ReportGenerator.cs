﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using aetp.Common.ControlModel;

namespace Xtdd2Xls
{
    public class ReportGenerator
    {
        XmlSchema schema;
        Report report;
        public ReportGenerator(XmlSchema schema)
        {
            this.schema = schema;
        }

        public Report Generate()
        {
            report = null;
            XmlSchemaElement element = schema.Elements.Values.OfType<XmlSchemaElement>().First();
            iterateOverElement(new ElementInfo(schema, element, ""), report);
            return report;
        }

        public void iterateOverElement(ElementInfo elementInfo, BaseControl parent)
        {
            BaseControl control = addControl(elementInfo, parent);
            
            XmlSchemaSequence sequence = elementInfo.Sequence;
            if (sequence == null || elementInfo.Type != ControlType.Page || control == null)
                return;

            // Iterate over each XmlSchemaElement in the Items collection.
            foreach (XmlSchemaElement childElement in sequence.Items)
            {
                iterateOverElement(new ElementInfo(schema, childElement, elementInfo.XPath), control);
            }
        }

        bool ValidElement(ElementInfo elementInfo)
        {
            if (elementInfo.Element.Name == "SourceLink")
            {
                return false;
            }
            return true;
        }

        BaseControl addControl(ElementInfo elementInfo, BaseControl parent)
        {
            if (!ValidElement(elementInfo))
            {
                return null;
            }
            BaseControl control = parent;
            if (report == null)
            {
                report = GetReport(elementInfo);
                control = report;
                parent = report;
            }
            else if (elementInfo.Type == ControlType.Simple)
            {
                control = GetSimpleEdit(elementInfo);
            }
            else if (elementInfo.Sequence != null)
            {
                if (elementInfo.Type == ControlType.GridAddedPage)
                {
                    control = GetGridAddedPage(elementInfo);
                }
                else if (elementInfo.Type == ControlType.ComboBoxOrCheckBox)
                {
                    control = GetSimpleEdit(elementInfo);
                    control.Tag = XmlElementHelper.GetXPath(elementInfo.XPath, "Описание");
                }
                else if (elementInfo.Type == ControlType.Grid)
                {
                    control = GetGrid(elementInfo);
                }
                else
                {
                    control = GetPage(elementInfo);
                }
            }
            try
            {
                control.Name = elementInfo.FullName;
            }
            catch
            {
                // имя не на что не влияет но оно должно быть уникальным
                control.Name = "a" + Guid.NewGuid().ToString().Replace("-", "_");
            }
            if (control.Tag == null) control.Tag = elementInfo.XPath;
            control.Caption = elementInfo.Documentation;
            if (!(control is Report)) parent.AddChildren(control, report);
            return control;
        }

        public Report GetReport(ElementInfo elementInfo)
        {
            Report report = new Report();
            return report;
        }

        SimpleDataType GetType(ElementInfo elementInfo)
        {
            switch (elementInfo.TypeName)
            {
                case "Double":
                    return SimpleDataType.Double;
                default:
                    return SimpleDataType.String;
            }
        }

        GridCellType GetCellType(ElementInfo elementInfo)
        {
            if (elementInfo.IsFixedValue)
                return GridCellType.Label;
            switch (elementInfo.TypeName)
            {
                case "Double":
                    return GridCellType.Double;
                default:
                    return GridCellType.String;
            }
        }

        public BaseControl GetSimpleEdit(ElementInfo elementInfo)
        {
            SimpleEdit control = new SimpleEdit();
            control.DataType = GetType(elementInfo);
            return control;
        }

        public Page GetPage(ElementInfo elementInfo)
        {
            Page page = new Page();
            return page;
        }

        void ProcessCellElement(GridRow row, ElementInfo elementInfo, ElementInfo parentElementInfo)
        {
            var cell = new GridCellItem();
            if (elementInfo.Sequence == null)
            {
                if (parentElementInfo.IsMultiple)
                {
                    cell.Tag = elementInfo.Name;
                }
                else
                {
                    cell.Tag = elementInfo.XPath;
                }
                if (elementInfo.IsFixedValue)
                {
                    cell.EditValue = elementInfo.FixedValue;
                }
                cell.Type = GetCellType(elementInfo);
                
                row.Cells.Add(cell);
            }
            else
            {
                ProcessRowElement(row.Grid, elementInfo);
            }
        }

        void ProcessRowElement(Grid grid, ElementInfo elementInfo)
        {
            var row = new GridRow();
            row.Grid = grid;
            grid.Rows.Add(row);
            if (elementInfo.IsMultiple)
            {
                row.Tag = elementInfo.XPath;
            }
            else
            {
                row.Tag = null;
            }
            foreach (XmlSchemaElement col in elementInfo.Sequence.Items)
            {
                var colInfo = new ElementInfo(schema, col, elementInfo.XPath);
                ProcessCellElement(row, colInfo, elementInfo);
            }
        }

        public Grid GetGrid(ElementInfo elementInfo)
        {
            Grid grid = new Grid();
            grid.Root = report;
            var childrenElement = elementInfo.Sequence.Items[0] as XmlSchemaElement;
            var childrenInfo = new ElementInfo(schema, childrenElement, elementInfo.XPath);
            grid.Tag = new GridMetaInfo(elementInfo.XPath, ControlType.Grid);

            foreach (XmlSchemaElement item in childrenInfo.Sequence.Items)
            {
                var itemInfo = new ElementInfo(schema, item, null);
                if (!itemInfo.IsMultiple && itemInfo.Sequence == null)
                {
                    GridColumn col = new GridColumn();
                    col.Caption = itemInfo.Documentation;
                    col.Tag = itemInfo.XPath;
                    col.Name = itemInfo.FullName;
                    grid.Columns.Add(col);
                }
            }
            foreach (XmlSchemaElement item in elementInfo.Sequence.Items)
            {
                var itemInfo = new ElementInfo(schema, item, "");
                ProcessRowElement(grid, itemInfo);
            }

            return grid;
        }

        public Grid GetGridAddedPage(ElementInfo elementInfo)
        {
            Grid grid = new Grid();
            grid.Root = report;
            var childrenElement = elementInfo.Sequence.Items[0] as XmlSchemaElement;
            var childrenInfo = new ElementInfo(schema, childrenElement, elementInfo.XPath);
            grid.Tag = new GridMetaInfo(childrenInfo.XPath, ControlType.GridAddedPage);
            
            foreach (XmlSchemaElement item in childrenInfo.Sequence.Items)
            {
                GridColumn col = new GridColumn();
                var itemInfo = new ElementInfo(schema, item, null);
                col.Caption = itemInfo.Documentation;
                if (itemInfo.Type == ControlType.ComboBoxOrCheckBox)
                    col.Tag = new ColumnMetaInfo(XmlElementHelper.GetXPath(itemInfo.XPath, "Описание"), GetCellType(itemInfo));
                else
                    col.Tag = new ColumnMetaInfo(itemInfo.XPath, GetCellType(itemInfo));
                col.Name = itemInfo.FullName;
                grid.Columns.Add(col);
            }
            return grid;
        }
    }

    public class GridMetaInfo
    {
        public string XPath;
        public ControlType Type;

        public GridMetaInfo(string xpath, ControlType type)
        {
            XPath = xpath;
            Type = type;
        }
    }

    public class ColumnMetaInfo
    {
        public string XPath;
        public GridCellType Type;

        public ColumnMetaInfo(string xpath, GridCellType type)
        {
            XPath = xpath;
            Type = type;
        }
    }
}
