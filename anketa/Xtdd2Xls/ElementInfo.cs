﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using System.Xml;

namespace Xtdd2Xls
{
    public enum ControlType
    {
        Page = 0,
        ComboBoxOrCheckBox,
        Simple,
        GridAddedPage,
        Grid
    }
    /// <summary>
    /// Wrapper для получения информации из XmlSchemaElement
    /// </summary>
    public class ElementInfo
    {
        XmlSchema schema;
        XmlSchemaElement element;
        XmlSchemaSequence sequence;
        string fullName;
        string xpath;
        
        public XmlSchemaSequence Sequence
        {
            get
            {
                return sequence;
            }
        }

        public XmlSchemaElement Element
        {
            get
            {
                return element;
            }
        }

        public XmlSchema Schema
        {
            get
            {
                return schema;
            }
        }

        public string FullName
        {
            get
            {
                return fullName;
            }
        }

        public string XPath
        {
            get
            {
                return xpath;
            }
        }

        public string TypeName
        {
            get
            {
                return element.SchemaTypeName.Name;
            }
        }

        public string Documentation { get; set; }
        public ControlType Type { get; set; }
        public string Name 
        {
            get
            {
                return element.Name;
            }
        }
        public bool IsRequired
        {
            get
            {
                return element.MinOccurs > 0;
            }
        }
        public bool IsMultiple
        {
            get
            {
                return element.MaxOccursString == "unbounded";
            }
        }

        public bool IsFixedValue
        {
            get
            {
                return element.FixedValue != null;
            }
        }

        public string FixedValue
        {
            get
            {
                return element.FixedValue;
            }
        }

        public ElementInfo(XmlSchema schema, XmlSchemaElement element, string xpath)
        {
            this.schema = schema;
            this.element = element;
                        
            this.xpath = XmlElementHelper.GetXPath(xpath, element);

            this.fullName = XmlElementHelper.GetFullName(this.xpath);
            
            
            FillCommonProperty();
            if (element.ElementSchemaType is XmlSchemaSimpleType)
            {
                Type = ControlType.Simple;
            }
            else if (element.ElementSchemaType is XmlSchemaComplexType)
            {
                FillComplexType();
            }
        }

        void FillComplexType()
        {
            if (isComboBoxOrCheckBox())
            {
                Type = ControlType.ComboBoxOrCheckBox;
                return;
            }
            if (isGridAddedPage())
            {
                Type = ControlType.GridAddedPage;
                return;
            }
            if (isGrid())
            {
                Type = ControlType.Grid;
                return;
            }
            Type = ControlType.Page;
        }

        string[] comboBoxTags = { "Код", "Описание", "Активно" };
        bool isComboBoxOrCheckBox()
        {
            if (sequence == null || sequence.Items.Count != 8)
                return false;

            if (sequence.Items.OfType<XmlSchemaElement>()
                    .Where(x => comboBoxTags.Contains(x.Name)).Count() == comboBoxTags.Length)
                return true;
            else
                return false;
        }

        bool isGridAddedPage()
        {
            if (sequence != null && sequence.Items.Count == 1)
            {
                var childrenElement = sequence.Items[0] as XmlSchemaElement;
                var childrenInfo = new ElementInfo(schema, childrenElement, xpath);
                if (childrenInfo.IsMultiple)
                    return true;
            }
            return false;
        }

        void FillCommonProperty()
        {
            Documentation = XmlElementHelper.GetElementDocumentation(element).Split('|').First();
            sequence = XmlElementHelper.GetElementSequence(element);
        }

        bool isGrid()
        {
            if (sequence != null && sequence.Items.Count > 1)
            {
                string first_name = (sequence.Items[0] as XmlSchemaElement).Name;
                string[] parts = first_name.Split('_');
                string name = "";
                for (int i = 0; i < parts.Length - 1; i ++)
                {
                    string.Join("", name, parts[i]);
                }
                string last = parts.Last();
                int n;
                bool isNumeric = int.TryParse(last, out n);
                if (!isNumeric)
                    return false;
                if (sequence.Items.OfType<XmlSchemaElement>()
                    .Where(x => x.Name.StartsWith(name)).Count() == sequence.Items.OfType<XmlSchemaElement>().Count())
                    return true;
            }
            return false;
        }

    }
}
