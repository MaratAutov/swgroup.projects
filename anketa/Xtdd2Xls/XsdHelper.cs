﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Schema;
using System.Xml;

namespace Xtdd2Xls
{
    public class XsdHelper
    {
        public static XmlSchema GetSchema(XtddFile file)
        {
            const string xsdDirectory = "XSD";
            string appPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string xsdFileName = string.Join("_", file.XsdModel, file.XsdRootName) + ".xsd";
            string xsdPath = Path.Combine(appPath, xsdDirectory, file.Version, file.XsdModel, xsdFileName);
            if (!File.Exists(xsdPath))
            {
                throw new InvalidOperationException("Не найдена xsd схема");
            }
            XmlSchemaSet ss = new XmlSchemaSet();
            XmlTextReader reader = new XmlTextReader(xsdPath);
            XmlSchema schema = XmlSchema.Read(reader, ValidationCallback);
            ss.Add(schema);
            ss.Compile();
            return schema;
        }

        static void ValidationCallback(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                Console.Write("WARNING: ");
            else if (args.Severity == XmlSeverityType.Error)
                Console.Write("ERROR: ");

            Console.WriteLine(args.Message);
        }
    }
}
