﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using System.Xml;

namespace Xtdd2Xls
{
    public static class XmlElementHelper
    {
        public static string GetElementDocumentation(XmlSchemaElement element)
        {
            if (element.Annotation == null)
                return "";
            foreach (var annot in element.Annotation.Items)
            {
                if (annot is XmlSchemaDocumentation)
                    if ((annot as XmlSchemaDocumentation).Markup.Length > 0)
                    {
                        XmlNode description = (annot as XmlSchemaDocumentation).Markup.Where(x => x.ParentNode.LocalName == "documentation").FirstOrDefault();
                        if (description != null)
                            return description.Value.Replace("&quot;", "\"");
                    }
            }
            return "";
        }

        public static XmlSchemaSequence GetElementSequence(XmlSchemaElement element)
        {
            XmlSchemaComplexType complexType = element.ElementSchemaType as XmlSchemaComplexType;
            if (complexType != null)
            {
                XmlSchemaSequence sequence = complexType.ContentTypeParticle as XmlSchemaSequence;
                return sequence;
            }
            return null;
        }

        public static string GetXPath(string xpath, XmlSchemaElement element)
        {
            return GetXPath(xpath, element.Name);
        }

        public static string GetXPath(string xpath, string name)
        {
            return string.Join("/", xpath, name);
        }

        public static string GetFullName(string xpath)
        {
            if (xpath.Length > 0) return xpath.Remove(0, 1).Replace('/', '_');
            else return xpath;
        }
    }
}
