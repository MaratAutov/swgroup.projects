﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace Xtdd2Xls
{
    /// <summary>
    /// Абстракция над файлом xtdd представляет доступ к основным свойствам файла
    /// </summary>
    public class XtddFile
    {
        public XmlDocument XmlDoc { get; private set; }
        public XPathNavigator Navigator { get; private set; }
        public string XsdModel { get; private set; }
        public string XsdRootName { get; private set; }
        public string Version { get; private set; }
        public string FileName { get; private set; }

        private XmlNamespaceManager nsManager;
        private string nsLocal;

        public XtddFile(string fileName)
        {
            try
            {
                FileName = fileName;
                XmlDoc = new XmlDocument();
                XmlDoc.Load(fileName);
                Navigator = XmlDoc.CreateNavigator();
                XPathDocument doc = new XPathDocument(fileName);
                XmlNode node = XmlDoc.DocumentElement;
                string tgtNamespace = node.NamespaceURI;
                XsdModel = tgtNamespace.Split('/').Last();
                XsdRootName = node.LocalName;
                Version = node.Attributes["appVersion"].Value;
                GetNameSpace();
            }
            catch
            {
                throw new InvalidOperationException("Файл не может быть конвертирован");
            }
        }

        void GetNameSpace()
        {
            Navigator.MoveToFirstChild();
            nsManager = new XmlNamespaceManager(Navigator.NameTable);
            while (String.IsNullOrEmpty(nsLocal))
            {
                    
                if (!string.IsNullOrEmpty(Navigator.Prefix))
                {
                    nsManager.AddNamespace(
                        Navigator.Prefix, Navigator.NamespaceURI
                    );
                    nsLocal = Navigator.Prefix;
                }
                else if (Navigator.NamespaceURI != null)
                {
                    nsLocal = "av";
                    nsManager.AddNamespace(
                        nsLocal, Navigator.NamespaceURI
                    );
                }
                if (!Navigator.MoveToChild(XPathNodeType.All))
                    break;
            }
            Navigator.MoveToRoot();
        }

        private string XPathString(string s, string ns)
        {
            string[] p = s.Split('/');
            if (p.Length > 1) return string.Join("/" + ns + ":", p);
            else return string.Join("", ns, ":", p[0]);
        }

        public XPathNodeIterator GetNodesIterator(string xpath, XPathNavigator navigator = null)
        {
            xpath = XPathString(xpath, nsLocal);
            if (navigator == null)
            {
                navigator = Navigator;
            }
            else
            {
                xpath = xpath.TrimStart('/');
            }
            return navigator.Select(xpath, nsManager);
        }

        public XPathNavigator GetNode(string xpath, XPathNavigator navigator = null)
        {
            xpath = XPathString(xpath, nsLocal);
            if (navigator == null)
            {
                navigator = Navigator;
            }
            else
            {
                xpath = xpath.TrimStart('/');
            }
            return navigator.SelectSingleNode(xpath, nsManager);
        }

        public string GetValue(string xpath, XPathNavigator navigator = null)
        {
            var e = GetNode(xpath, navigator);
            if (e == null)
                return null;
            else
                return e.Value.Trim();
        }
    }
}
