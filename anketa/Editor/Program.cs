﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using aetp.Common;

namespace Editor
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            argsParse(args);
        }

        private static void argsParse(string[] args)
        {
            string fileName = null;
            
            if (args.Length > 0 && !args[0].StartsWith("-"))
                fileName = args[0];
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-refresh_models")
                {
                    ReportUtils.RefreshModelsList();                    
                }
            }
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ru-RU");
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ru-RU");
            
            if (fileName == null)
                Application.Run(new EditorApp(null).GetMainForm());
            else
                Application.Run(new EditorApp(null).GetMainForm(fileName));
        }
    }
}
