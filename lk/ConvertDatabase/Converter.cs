﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Account.DAO;

namespace ConvertDatabase
{
    public static class Converter
    {
        public static int Convert(OldPackageModel packageDetail, IList<MailDetail> mails)
        {
            ReportInfo rep = new ReportInfo();
            rep.INN = packageDetail.inn;
            rep.OGRN = packageDetail.ogrn;
            rep.Address = packageDetail.adress;
            rep.Version = packageDetail.appversion;
            rep.MiddleName = packageDetail.lastname;
            rep.OrgFullName = packageDetail.fullname;
            rep.IncomingDate = packageDetail.incomingdate;
            rep.IncomingNumber = packageDetail.incomingnumber;
            rep.Model = packageDetail.modelname;
            
            int orgtypecode;
            if (int.TryParse(packageDetail.orgtypecode, out orgtypecode))
                rep.OrgTypeCode = orgtypecode;
            else
                rep.OrgTypeCode = null;
            
            rep.OrgTypeName = packageDetail.orgtypename;
            rep.Quartal = packageDetail.quarter;
            int doctypecode;
            if (int.TryParse(packageDetail.doctypecode, out doctypecode))
                rep.DocTypeCode = doctypecode;
            else
                rep.DocTypeCode = null;

            rep.Date = packageDetail.reportday;
            rep.Month = packageDetail.reportmonth;
            rep.DocTypeName = packageDetail.doctypename;
            rep.Year = packageDetail.reportyear;
            rep.Type = packageDetail.rootname;
            rep.OrgShortName = packageDetail.shortname;
            rep.SedNumber = packageDetail.входящийНомерПрисвоенныйПакету;
            rep.SedDate = packageDetail.датаПрисвоенияВходящегоНомера;
            rep.FirstName = packageDetail.firstname;
            rep.LastName = packageDetail.middlename;
            rep.DocumentUrl = ConvertUrl(packageDetail.documenturi);
            rep.DocumentSignUrl = ConvertUrl(packageDetail.documentsignuri);
            rep.NotificationUrl = ConvertUrl(packageDetail.notificationuri);
            rep.NotificationSignUrl = ConvertUrl(packageDetail.notificationsignuri);
            rep.ProcessedDocumentUrl = ConvertUrl(packageDetail.urlprocesseddocument);
            rep.RoName = packageDetail.краткоеНаименованиеРО;
            rep.RoType = packageDetail.regioncode;

            Package package = new Package();
            package.UploadTime = packageDetail.creationdate ?? DateTime.MinValue;
            package.SignatureType = packageDetail.signtype;
            package.IsImported = true;

            List<MailMessage> messages = new List<MailMessage>();
            foreach (var mail in mails)
            {
                messages.Add(Convert(mail));
            }

            Status initialStatus, currentStatus;
            ConvertStatus(packageDetail.актуальныйСтатус, packageDetail, out initialStatus, out currentStatus);
            PackageService.Create(package, rep, messages, initialStatus, currentStatus);
            return package.ID;
        }

        private static string[] signNotOkStatus = { "PackageCorrupted", "SignatureError" };
        public static void ConvertStatus(string actulaStatus, OldPackageModel detail, out Status initialStatus, out Status currentStatus)
        {
            initialStatus = new Status();
            currentStatus = new Status();
            PrimaryStatus status = StatusService.GetPrimaryStatusByOldStatus(actulaStatus);
            currentStatus.PrimaryCode = (int)status;
            currentStatus.Exception = detail.exceptionstring;
            // забиваем на реальное время выставления статуса и проставляем дату создания
            currentStatus.AssignmentTime = detail.creationdate ?? DateTime.MinValue;
            initialStatus.AssignmentTime = detail.creationdate ?? DateTime.MinValue;
            // приведение статусов на глаз так как сейчас все статусы старого СПО и нового перепутаны
            currentStatus.Info = detail.nstatus;
            if (signNotOkStatus.Contains(actulaStatus))
                initialStatus.PrimaryCode = (int)PrimaryStatus.SIGNATURE_ERROR;
            else if (string.IsNullOrEmpty(actulaStatus))
                initialStatus.PrimaryCode = (int)PrimaryStatus.NotFound;
            else
                initialStatus.PrimaryCode = (int)PrimaryStatus.Verified;
        }

        public static string ConvertUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
                return url;
            else
                return url.Replace("/", "\\").Replace("fs:\\", "fs:\\old\\");
        }

        public static MailMessage Convert(MailDetail mail)
        {
            MailMessage result = new MailMessage();
            result.Url = ConvertUrl(mail.url);
            result.MailFrom = mail.frommail;
            result.MailTo = mail.tomail;
            result.Subject = mail.subject;
            result.CreateAt = mail.sentdate ?? DateTime.MinValue;
            if (mail.server == "Report")
            {
                result.Info = "From RO";
            }
            if (mail.server == "SED" && (mail.status == "SED" || mail.status == "Error"))
            {
                result.Info = "From SED";
            }
            else
            {
                result.Info = "To SED";
            }
            return result;
        }
    }
}
