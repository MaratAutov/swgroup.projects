﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.DataAnnotations;

namespace ConvertDatabase.Models
{
    [Alias("mailattach")]
    public class mailattach
    {
        public string id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
    }
}
