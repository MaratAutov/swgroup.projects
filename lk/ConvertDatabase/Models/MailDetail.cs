﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConvertDatabase
{
    public class MailDetail
    {
        public string body { get; set; }
        public string frommail { get; set; }
        public int imapid { get; set; }
        public bool isprocessed { get; set; }
        public bool isprocessing { get; set; }
        public bool issended { get; set; }
        public string messageid { get; set; }
        public string reportpackageid { get; set; }
        public DateTime? sentdate { get; set; }
        public string server { get; set; }
        public string status { get; set; }
        public string subject { get; set; }
        public string tomail { get; set; }

        public string name { get; set; }
        public string url { get; set; }
    }
}
