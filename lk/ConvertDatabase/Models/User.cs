﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConvertDatabase.Models
{
    public class User
    {
        public int ID { get; set; }
        public string INN { get; set; }
        public string OGRN { get; set; }
    }
}
