﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.DataAnnotations;

namespace ConvertDatabase.Models
{
    public class missionwebdavt
    {
        [References(typeof(imapmessagecard))]
        public string mesid { get; set; }
        [References(typeof(mailattach))]
        public string attachid { get; set; }
    }
}
