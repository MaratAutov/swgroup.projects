﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConvertDatabase
{
    public class OldPackageModel
    {
        public string reportid { get; set; }
        public string inn { get; set; }
        public string ogrn { get; set; }
        public string adress { get; set; }
        public string appversion { get; set; }
        public string fullname { get; set; }
        public string shortname { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string middlename { get; set; }
        public string signtype { get; set; }
        public string urldocument { get; set; }
        public string urldocumentsign { get; set; }
        public string urlnotification { get; set; }
        // лежит не верное значение указывающее на document sign
        public string urlnotificationsign { get; set; }
        public string urlprocesseddocument { get; set; }

        public string documenturi { get; set; }
        public string documentsignuri { get; set; }
        public string notificationuri { get; set; }
        public string notificationsignuri { get; set; }
        
        public string актуальныйСтатус { get; set; }
        public DateTime? incomingdate { get; set; }
        public string incomingnumber { get; set; }
        public string modelname { get; set; }
        public string orgtypecode { get; set; }
        public string orgtypename { get; set; }
        public string doctypecode { get; set; }
        public string doctypename { get; set; }
        public string reportyear { get; set; }
        public string quarter { get; set; }
        public string reportmonth { get; set; }
        public DateTime? reportday { get; set; }
        public string rootname { get; set; }
        public string входящийНомерПрисвоенныйПакету { get; set; }
        public DateTime? датаПрисвоенияВходящегоНомера { get; set; }
        public DateTime? creationdate { get; set; }
        public string occurederrors { get; set; }
        public string exceptionstring { get; set; }
        public string nstatus { get; set; }
        public string regioncode { get; set; }
        public string краткоеНаименованиеРО { get; set; }

        public string portalid { get; set; }
        
        public bool? IsImported { get; set; }

        public override string ToString()
        {
            return String.Format(@"reportid = {1}{0}
                                inn = {2}{0}
         ogrn = {3}{0}
         adress = {4}{0}
         appversion = {5}{0}
         fullname = {6}{0}
         shortname = {7}{0}
         firstname = {8}{0}
         lastname = {9}{0}
         middlename = {10}{0}
         signtype = {11}{0}
         urldocument = {12}{0}
         urldocumentsign = {13}{0}
         urlnotification = {14}{0}
         urlnotificationsign = {15}{0}
         urlprocesseddocument = {16}{0}

         documenturi = {17}{0}
         documentsignuri = {18}{0}
         notificationuri = {19}{0}
         notificationsignuri = {20}{0}
        
         актуальныйСтатус = {21}{0}
        incomingdate = {22}{0}
         incomingnumber = {23}{0}
         modelname = {24}{0}
         orgtypecode = {25}{0}
         orgtypename = {26}{0}
         doctypecode = {27}{0}
         doctypename = {28}{0}
         reportyear = {29}{0}
         quarter = {30}{0}
         repormonth = {31}{0}
        reportday = {32}{0}
         rootname = {33}{0}
         входящийНомерПрисвоенныйПакету = {34}{0}
        датаПрисвоенияВходящегоНомера = {35}{0}
        creationdate = {36}{0}
         occurederrors = {37}{0}
         exceptionstring = {38}{0}
         nstatus = {39}{0}
         regioncode = {40}{0}
         краткоеНаименованиеРО = {41}{0}", Environment.NewLine,
                   reportid,
         inn,
         ogrn,
         adress,
         appversion,
         fullname,
         shortname,
         firstname,
         lastname,
         middlename,
         signtype,
         urldocument,
         urldocumentsign,
         urlnotification,
         urlnotificationsign,
         urlprocesseddocument,

         documenturi,
         documentsignuri,
         notificationuri,
         notificationsignuri,

         актуальныйСтатус,
        incomingdate,
         incomingnumber,
         modelname,
         orgtypecode,
         orgtypename,
         doctypecode,
         doctypename,
         reportyear,
         quarter,
         reportmonth,
        reportday,
         rootname,
         входящийНомерПрисвоенныйПакету,
        датаПрисвоенияВходящегоНомера,
        creationdate,
         occurederrors,
         exceptionstring,
         nstatus,
         regioncode,
         краткоеНаименованиеРО,

         portalid,

        IsImported);
        }
    }
}
