﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConvertDatabase.Models
{
    public class reportpackage
    {
        public string id { get; set; }
        public bool? IsImported { get; set; }
        public int IsUpdated { get; set; }
    }
}
