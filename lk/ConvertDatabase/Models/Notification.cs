﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConvertDatabase.Models
{
    public class Notification
    {
        public string ID { get; set; }
        public string PACKAGE_NAME { get; set; }
        public int? USER_ID { get; set; }
        public string KIAS_ID { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string INCOMING_NUMBER { get; set; }
    }
}
