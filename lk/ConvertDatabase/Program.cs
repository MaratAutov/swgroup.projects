﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using Account.Tools;
using Account.DAO;
using ConvertDatabase.Repository;
using System.Threading;

namespace ConvertDatabase
{
    class Program
    {
        static Logger log = NLogger.GetLog("ConvertDatabase", "log.txt");
        
        static void Main(string[] args)
        {
            //Test("2c90817142c715c40142eacd784a5684");
            //TaskUpdateOwner();
            //UpdateMonth();
            //TaskUpdateOwnerByKiasID();
            //GetStatistics();
            //ThreadUpdateOwner();
            //TaskUpdateOwnerByKiasID();
            //UpdateUploadTime();
            TaskUpdateOwnerBySedNumber();
            //GetStatistics();
            Console.WriteLine("The end");
            Console.ReadKey();
        }
        
        static int batchSize = 1000;

        public class param
        {
            public string txt;
        }

        static void ThreadUpdateOwner()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateOwnereProc), new param { txt = "Thread 1" });
            ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateOwnereProc), new param { txt = "Thread 2" });
            ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateOwnereProc), new param { txt = "Thread 3" });
            ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateOwnereProc), new param { txt = "Thread 4" });
        }

        static void UpdateOwnereProc(object p)
        {
            for (int i = 0; i < 20; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine((p as param).txt + " " + i.ToString());
            }

        }

        static void UpdateUploadTime()
        {
            try
            {
                log.Debug("start task update upload time {0}", DateTime.Now);
                int returnCount = batchSize;
                int iterCount = 0;
                int totalCount = 0;
                int errorCount = 0;
                while (returnCount == batchSize)
                {
                    IList<Package> packages = null;
                    try
                    {
                        packages = OldPackageRepository.GetImportedPackages(batchSize, iterCount * batchSize);
                    }
                    catch (Exception ex)
                    {
                        string error = String.Format("Error get package list :: {0}", ex);
                        Console.WriteLine(error);
                        log.Error(error);
                        returnCount = batchSize;
                        continue;
                    }
                    foreach (var package in packages)
                    {
                        try
                        {
                            var notification = PortalRepository.GetNotification(package.PortalID, package.notifytimeid);
                            if (notification != null && notification.CREATE_DATE != null &&
                                notification.CREATE_DATE != package.UploadTime)
                            {
                                DateTime old = package.UploadTime;
                                package.UploadTime = notification.CREATE_DATE ?? DateTime.MinValue;
                                PackageService.Update(package);
                                string info = String.Format("updated date, packageid: {0}; old: {1}; new: {2}", package.ID, old, package.UploadTime);
                                log.Debug(info);
                                Console.WriteLine(info);
                            }
                            else
                            {
                                errorCount++;
                                string error = String.Format("not finded notification for id: {0}, portalid: {1}", package.ID, package.PortalID);
                                log.Debug(error);
                                Console.WriteLine(error);
                            }
                        }
                        catch (Exception ex)
                        {
                            errorCount++;
                            Console.WriteLine(ex);
                            log.Error(String.Format("erorr reprocessing id: {0}; exception {1}", package.ID, ex));
                        }
                    }

                    iterCount++;
                    if (returnCount != batchSize)
                        totalCount = iterCount * batchSize + packages.Count;
                    returnCount = packages.Count;
                }
                Console.WriteLine("Total count {0}; error count {1}", totalCount, errorCount);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        static void GetStatistics()
        {
            int eq_count = 0, not_eq_count = 0, bigest_count = 0;
            Console.WriteLine(UserIDs.Count);
            foreach (var kvp in UserIDs)
            {
                try
                {
                    int lk_count = PortalRepository.GetNotificationCount(kvp.Key);
                    int acc_count = PackageRepository.GetPackageCount(kvp.Value);
                    if (lk_count == acc_count)
                        eq_count++;
                    else if (acc_count > lk_count)
                        bigest_count++;
                    else
                        not_eq_count++;
                    string result = String.Format("OwnerID: {0,7}; portal: {1,6}; account: {2,6}", kvp.Value, lk_count, acc_count);
                    Console.WriteLine(result);
                    log.Debug(result);
                }
                catch (Exception ex)
                {
                    log.Error("Error get statistic for userid: {0}; ex: {1}", kvp.Value, ex);
                }
            }
            string total_result = String.Format("Eq: {0,8}; Not eq: {1,8}; Bigest: {2,8};",
                eq_count, not_eq_count, bigest_count);
            Console.WriteLine(total_result);
            log.Debug(total_result);
        }

        static void Test(string id)
        {
            var p = OldPackageRepository.GetPackage(id);
            Console.Write(p);
        }

        static void UpdateMonth()
        {
            log.Debug("start task update owner at {0}", DateTime.Now);
                List<OldPackageModel> oldPackagesTotal = new List<OldPackageModel>();
                int returnCount = batchSize;
                int iterCount = 0;
                int totalCount = 0;
                int errorCount = 0;
                while (returnCount == batchSize)
                {
                    IList<Package> packages = null;
                    try
                    {
                        packages = OldPackageRepository.GetPackagesForUpdateMonth(batchSize, iterCount * batchSize);
                    }
                    catch (Exception ex)
                    {
                        string error = String.Format("Error get package list :: {0}", ex);
                        Console.WriteLine(error);
                        log.Error(error);
                        returnCount = batchSize;
                        continue;
                    }

                    foreach (var package in packages)
                    {
                        try
                        {
                            if (String.IsNullOrEmpty(package.PortalID))
                                continue;
                            var old = OldPackageRepository.GetPackageByPortalID(package.PortalID);
                            ReportInfo ri = PackageService.GetReportInfo(package.ReportID);
                            if (old != null && !String.IsNullOrEmpty(old.reportmonth) && ri != null && ri.Month != old.reportmonth)
                            {
                                string month = ri.Month;
                                ri.Month = old.reportmonth;
                                PackageService.UpdateReportInfo(ri);
                                string info = String.Format("updated package id: {0}; month from: {1}; month to {2}", package.ID, month, ri.Month);
                                log.Debug(info);
                                Console.WriteLine(info);
                            }
                            else
                            {
                                errorCount++;
                                string error = String.Format("not finded package for id: {0}, old.reportmonth: {1}, ri.Month: {2} ", package.ID, 
                                    old == null ? "" : old.reportmonth, 
                                    ri ==null ? "" : ri.Month);
                                log.Debug(error);
                                Console.WriteLine(error);
                            }
                        }
                        catch (Exception ex)
                        {
                            errorCount++;
                            Console.WriteLine(ex);
                            log.Error(String.Format("erorr reprocessing id: {0}; exception {1}", package.ID, ex));
                        }
                    }

                    iterCount++;
                    if (returnCount != batchSize)
                        totalCount = iterCount * batchSize + packages.Count;
                    returnCount = packages.Count;

                }
        }

        static void TaskUpdateOwner()
        {
            try
            {
                log.Debug("start task update owner at {0}", DateTime.Now);
                List<OldPackageModel> oldPackagesTotal = new List<OldPackageModel>();
                int returnCount = batchSize;
                int iterCount = 0;
                int totalCount = 0;
                int errorCount = 0;
                while (returnCount == batchSize)
                {
                    IList<Package> packages = null;
                    try
                    {
                        packages = OldPackageRepository.GetPackagesForUpdateOwner(batchSize, iterCount * batchSize);
                    }
                    catch (Exception ex)
                    {
                        string error = String.Format("Error get package list :: {0}", ex);
                        Console.WriteLine(error);
                        log.Error(error);
                        returnCount = batchSize;
                        continue;
                    }
                    foreach (var package in packages)
                    {
                        try
                        {
                            var notification = PortalRepository.GetNotification(package.PortalID);
                            if (notification != null && notification.USER_ID != null)
                            {
                                if (!UserIDs.ContainsKey((int)notification.USER_ID))
                                    throw new Exception(String.Format("not finding user for notification: {0}; user_id: {1}", notification.ID, notification.USER_ID));
                                package.OwnerID = UserIDs[(int)notification.USER_ID];
                                if (String.IsNullOrEmpty(package.Name))
                                {
                                    package.Name = notification.PACKAGE_NAME;
                                }
                                PackageService.Update(package);
                                string info = String.Format("updated package id: {0}; ownerid: {1}", package.ID, package.OwnerID);
                                log.Debug(info);
                                Console.WriteLine(info);
                            }
                            else
                            {
                                errorCount++;
                                string error = String.Format("not finded notification for id: {0}, portalid: {1}", package.ID, package.PortalID);
                                log.Debug(error);
                                Console.WriteLine(error);
                            }
                        }
                        catch (Exception ex)
                        {
                            errorCount++;
                            Console.WriteLine(ex);
                            log.Error(String.Format("erorr reprocessing id: {0}; exception {1}", package.ID, ex));
                        }
                    }

                    iterCount++;
                    if (returnCount != batchSize)
                        totalCount = iterCount * batchSize + packages.Count;
                    returnCount = packages.Count;
                }
                Console.WriteLine("Total count {0}; error count {1}", totalCount, errorCount);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        static void TaskUpdateOwnerBySedNumber()
        {
            try
            {
                log.Debug("start task update owner at {0}", DateTime.Now);
                List<OldPackageModel> oldPackagesTotal = new List<OldPackageModel>();
                int returnCount = batchSize;
                int iterCount = 0;
                int totalCount = 0;
                int errorCount = 0;
                while (returnCount == batchSize)
                {
                    IList<Package> packages = null;
                    try
                    {
                        packages = OldPackageRepository.GetPackagesForUpdateOwnerBySedNumber(batchSize, iterCount * batchSize);
                    }
                    catch (Exception ex)
                    {
                        string error = String.Format("Error get package list :: {0}", ex);
                        Console.WriteLine(error);
                        log.Error(error);
                        returnCount = batchSize;
                        continue;
                    }
                    foreach (var package in packages)
                    {
                        try
                        {
                            ReportInfo ri = Account.DAO.PackageService.GetReportInfo(package.ReportID);
                            if (String.IsNullOrEmpty(ri.SedNumber))
                                continue;

                            var notification = PortalRepository.GetNotificationBySedNumber(ri.SedNumber);
                            if (notification != null && notification.USER_ID != null)
                            {
                                if (!UserIDs.ContainsKey((int)notification.USER_ID))
                                    throw new Exception(String.Format("not finding user for sed number: {0}; user_id: {1}", ri.SedNumber, notification.USER_ID));
                                package.OwnerID = UserIDs[(int)notification.USER_ID];
                                if (String.IsNullOrEmpty(package.Name))
                                {
                                    package.Name = notification.PACKAGE_NAME;
                                }
                                PackageService.Update(package);
                                string info = String.Format("updated package id: {0}; ownerid: {1}", package.ID, package.OwnerID);
                                log.Debug(info);
                                Console.WriteLine(info);
                            }
                            else
                            {
                                errorCount++;
                                string error = String.Format("not finded notification for id: {0}, sednumber: {1}", package.ID, ri.SedNumber);
                                log.Debug(error);
                                Console.WriteLine(error);
                            }
                        }
                        catch (Exception ex)
                        {
                            errorCount++;
                            Console.WriteLine(ex);
                            log.Error(String.Format("erorr reprocessing id: {0}; exception {1}", package.ID, ex));
                        }
                    }

                    iterCount++;
                    if (returnCount != batchSize)
                        totalCount = iterCount * batchSize + packages.Count;
                    returnCount = packages.Count;
                }
                Console.WriteLine("Total count {0}; error count {1}", totalCount, errorCount);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        static void TaskUpdateOwnerByKiasID()
        {
            try
            {
                log.Debug("start task update owner at {0}", DateTime.Now);
                List<OldPackageModel> oldPackagesTotal = new List<OldPackageModel>();
                int returnCount = batchSize;
                int iterCount = 0;
                int totalCount = 0;
                int errorCount = 0;
                while (returnCount == batchSize)
                {
                    IList<Package> packages = null;
                    try
                    {
                        packages = OldPackageRepository.GetPackagesForUpdateOwnerByKiasID(batchSize, iterCount * batchSize);
                    }
                    catch (Exception ex)
                    {
                        string error = String.Format("Error get package list :: {0}", ex);
                        Console.WriteLine(error);
                        log.Error(error);
                        returnCount = batchSize;
                        continue;
                    }
                    foreach (var package in packages)
                    {
                        try
                        {
                            var notification = PortalRepository.GetNotificationByKiasID(package.notifytimeid);
                            if (notification != null && notification.USER_ID != null)
                            {
                                if (!UserIDs.ContainsKey((int)notification.USER_ID))
                                    throw new Exception(String.Format("not finding user for notification: {0}; user_id: {1}", notification.ID, notification.USER_ID));
                                package.OwnerID = UserIDs[(int)notification.USER_ID];
                                if (String.IsNullOrEmpty(package.Name))
                                {
                                    package.Name = notification.PACKAGE_NAME;
                                }
                                PackageService.Update(package);
                                string info = String.Format("updated package id: {0}; ownerid: {1}", package.ID, package.OwnerID);
                                log.Debug(info);
                                Console.WriteLine(info);
                            }
                            else
                            {
                                errorCount++;
                                string error = String.Format("not finded notification for id: {0}, portalid: {1}", package.ID, package.PortalID);
                                log.Debug(error);
                                Console.WriteLine(error);
                            }
                        }
                        catch (Exception ex)
                        {
                            errorCount++;
                            Console.WriteLine(ex);
                            log.Error(String.Format("erorr reprocessing id: {0}; exception {1}", package.ID, ex));
                        }
                    }

                    iterCount++;
                    if (returnCount != batchSize)
                        totalCount = iterCount * batchSize + packages.Count;
                    returnCount = packages.Count;
                }
                Console.WriteLine("Total count {0}; error count {1}", totalCount, errorCount);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        static Dictionary<int, int> _userIDs = null;
        static Dictionary<int, int> UserIDs
        {
            get
            {
                if (_userIDs == null)
                {
                    Dictionary<int, int> _ids = new Dictionary<int, int>();
                    var portalUsers = PortalRepository.GetAllUsers();
                    var spoUsers = UserService.GetAll();
                    foreach (var oldUsers in portalUsers)
                    {
                        var spo = spoUsers.Where(x => (x.INN == oldUsers.INN && x.OGRN == oldUsers.OGRN)).FirstOrDefault();
                        if (spo != null)
                        {
                            _ids.Add(oldUsers.ID, spo.ID);
                        }
                        else
                        {
                            log.Error("[ID: {2}, INN: {0}, OGRN: {1}] not in new db", oldUsers.INN, oldUsers.OGRN, oldUsers.ID);
                        }
                    }
                    _userIDs = _ids;
                }
                return _userIDs;
            }

        }

        static void TaskUpdateIDs()
        {
            log.Debug("start task update ids at {0}", DateTime.Now);
            List<OldPackageModel> oldPackagesTotal = new List<OldPackageModel>();
            int returnCount = batchSize;
            int iterCount = 0;
            int totalCount = 0;
            int errorCount = 0;
            while (returnCount == batchSize)
            {
                IList<OldPackageModel> packages = null;
                try
                {
                    packages = OldPackageRepository.GetPackagesForUpdate(batchSize, iterCount * batchSize);
                }
                catch (Exception ex)
                {
                    string error = String.Format("Error get package list :: {0}", ex);
                    Console.WriteLine(error);
                    log.Error(error);
                    returnCount = batchSize;
                    continue;
                }
                foreach (var package in packages)
                {
                    try
                    {
                        string cdocurl = Converter.ConvertUrl(package.documenturi);
                        //string cprocurl = Converter.ConvertUrl(package.urlprocesseddocument);
                        if (String.IsNullOrEmpty(cdocurl))
                            continue;

                        var newpackage = PackageRepository.GetPackage(cdocurl);
                        if (newpackage != null)
                        {
                            newpackage.PortalID = package.portalid;
                            newpackage.KiasID = package.reportid;
                            PackageService.Update(newpackage);
                            log.Debug("updated package id: {0}; portalid: {1}; oldid: {2}", newpackage.ID, package.portalid, package.reportid);
                            Console.WriteLine("Updated id {0}", newpackage.ID);
                            OldPackageRepository.SetUpdated(package.reportid);
                        }
                        else
                        {
                            errorCount++;
                            log.Debug("not finded package for id: {0}", package.reportid);
                            Console.WriteLine("not finded id {0}", package.reportid);
                        }
                    }
                    catch (Exception ex)
                    {
                        errorCount++;
                        Console.WriteLine(ex);
                        log.Error(String.Format("erorr reprocessing id: {0}; exception {1}", package.reportid, ex));
                    }
                }

                iterCount++;
                if (returnCount != batchSize)
                    totalCount = iterCount * batchSize + packages.Count;
                returnCount = packages.Count;
            }
            Console.WriteLine("Total count {0}; error count {1}", totalCount, errorCount);
        }

        
        /// <summary>
        /// Метод для повторной обработки тех пакетов которые не прошли переливку на самом деле
        /// </summary>
        static void TaskGetError()
        {
            log.Debug("start at {0}", DateTime.Now);
            List<OldPackageModel> oldPackagesTotal = new List<OldPackageModel>();
            int returnCount = batchSize;
            int iterCount = 0;
            int totalCount = 0;
            int errorCount = 0;
            while (returnCount == batchSize)
            {
                IList<OldPackageModel> packages = null;
                try
                {
                    packages = OldPackageRepository.GetPackages(batchSize, iterCount * batchSize);
                    //log.Debug("Get {0} packages", packages.Count);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    log.Error(ex);
                }
                if (packages == null)
                    continue;
                foreach (var package in packages)
                {
                    try
                    {
                        string cdocurl = Converter.ConvertUrl(package.documenturi);
                        string cprocurl = Converter.ConvertUrl(package.urlprocesseddocument);
                        if (String.IsNullOrEmpty(cdocurl) && String.IsNullOrEmpty(cprocurl))
                            continue;

                        if (!PackageRepository.PackageExists(cdocurl, cprocurl))
                        {
                            string str = String.Format("Finded id: {0}; docUri: {1}; procUri: {2}", package.reportid, cdocurl , cprocurl);
                            log.Debug(str);
                            Console.WriteLine(package.reportid);
                            var mails = MailRepository.GetMail(package.reportid);
                            int id = Converter.Convert(package, mails);
                            Console.WriteLine(id);
                            log.Debug(id);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        log.Error(String.Format("erorr reprocessing id: {0}; exception {1}", package.reportid, ex));
                    }
                }

                iterCount++;
                if (returnCount != batchSize)
                    totalCount = iterCount * batchSize + packages.Count;
                returnCount = packages.Count;
            }
        }

        static void TaskExecute()
        {
            log.Debug("start at {0}", DateTime.Now);
            List<OldPackageModel> oldPackagesTotal = new List<OldPackageModel>();
            int returnCount = batchSize;
            int iterCount = 0;
            int totalCount = 0;
            int errorCount = 0;
            while (returnCount == batchSize)
            {
                IList<OldPackageModel> packages = null;
                try
                {
                    packages = OldPackageRepository.GetPackages(batchSize);
                    log.Debug("Get {0} packages", packages.Count);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    log.Error(ex);
                }
                if (packages == null)
                    continue;
                foreach (var package in packages)
                {
                    try
                    {
                        var mails = MailRepository.GetMail(package.reportid);
                        int id = Converter.Convert(package, mails);
                        Console.WriteLine(id);
                        log.Debug(id);
                    }
                    catch (Exception ex)
                    {
                        errorCount++;
                        Console.WriteLine(ex);
                        log.Error(ex);
                    }
                    OldPackageRepository.SetImported(package.reportid);
                }
                iterCount ++;
                if (returnCount != batchSize)
                    totalCount = iterCount * batchSize + packages.Count;
                returnCount = packages.Count;
            }
            string result = String.Format("Total count: {0}; Error count: {1}", totalCount, errorCount);
            Console.WriteLine(result);
            log.Error(result);
            Console.ReadLine();

        }
    }
}
