﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.PostgreSQL;
using System.Configuration;
using System.Data;

namespace ConvertDatabase
{
    public static class ORM
    {
        private static OrmLiteConnectionFactory _factoryMailSource { get; set; }
        private static OrmLiteConnectionFactory _factorySource { get; set; }
        private static OrmLiteConnectionFactory _factoryDestination { get; set; }
        private static OrmLiteConnectionFactory _factoryPortal { get; set; }
        private static bool IsInitedSource;
        private static bool IsInitedMailSource;
        private static bool IsInitedDestination;
        private static bool IsInitedPortal;

        public static IDbConnection dbSource
        {
            get
            {
                if (!IsInitedSource || _factorySource == null)
                    _factorySource = Initialize(ref IsInitedSource, "ConnectionSource");

                return _factorySource.OpenDbConnection();
            }
        }

        public static IDbConnection dbMailSource
        {
            get
            {
                if (!IsInitedMailSource || _factoryMailSource == null)
                    _factoryMailSource = Initialize(ref IsInitedMailSource, "ConnectionMailSource");

                return _factoryMailSource.OpenDbConnection();
            }
        }

        public static IDbConnection dbDestination
        {
            get
            {
                if (!IsInitedDestination || _factoryDestination == null)
                    _factoryDestination = Initialize(ref IsInitedDestination, "ConnectionDestination");

                return _factoryDestination.OpenDbConnection();
            }
        }

        public static IDbConnection dbPortal
        {
            get
            {
                if (!IsInitedPortal || _factoryPortal == null)
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionPortal"].ToString();
                    if (string.IsNullOrEmpty(ConnectionString))
                        throw new ConfigurationErrorsException("ORM Connection string was not set");

                    _factoryPortal = new OrmLiteConnectionFactory(
                        ConnectionString,
                        SqlServerDialect.Provider
                    );     
                    IsInitedPortal = true;
                }
                return _factoryPortal.OpenDbConnection();
            }
        }

        private static OrmLiteConnectionFactory Initialize(ref bool IsInited, string connectionName)
        {
            IsInited = true;
            string ConnectionString = ConfigurationManager.ConnectionStrings[connectionName].ToString();

            if (string.IsNullOrEmpty(ConnectionString))
                throw new ConfigurationErrorsException("ORM Connection string was not set");

            return new OrmLiteConnectionFactory(
                ConnectionString,
                PostgreSqlDialect.Provider
            );
        }
    }
}
