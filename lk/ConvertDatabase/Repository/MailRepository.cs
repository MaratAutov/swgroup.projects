﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.PostgreSQL;

namespace ConvertDatabase
{
    public static class MailRepository
    {
        public static IList<MailDetail> GetMail(string packageid)
        {
            using (var db = ORM.dbMailSource)
            {
                return db.Select<MailDetail>(String.Format("select * from imapmessagecard m " +
	                "inner join missionwebdavt w on m.id = w.mesid " +
	                "inner join mailattach a on w.attachid = a.id " +
                    "where reportpackageid = '{0}'", packageid));
            }
        }
    }
}
