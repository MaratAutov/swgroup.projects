﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.OrmLite;
using ConvertDatabase.Models;

namespace ConvertDatabase.Repository
{
    public class PortalRepository
    {
        public static List<User> GetAllUsers()
        {
            using (var db = ORM.dbPortal)
            {
                return db.Select<User>();
            }
        }

        public static Notification GetNotification(string id)
        {
            using (var db = ORM.dbPortal)
            {
                return db.Select<Notification>(w => w.Where(x => x.ID == id)).FirstOrDefault();
            }
        }

        public static Notification GetNotification(string portalid, string kiasid)
        {
            using (var db = ORM.dbPortal)
            {
                return db.Select<Notification>(w => w.Where(x => x.ID == portalid || x.KIAS_ID == kiasid)).FirstOrDefault();
            }
        }

        public static Notification GetNotificationByKiasID(string id)
        {
            using (var db = ORM.dbPortal)
            {
                return db.Select<Notification>(w => w.Where(x => x.KIAS_ID == id)).FirstOrDefault();
            }
        }

        public static Notification GetNotificationBySedNumber(string number)
        {
            using (var db = ORM.dbPortal)
            {
                return db.Select<Notification>(w => w.Where(x => x.INCOMING_NUMBER == number)).FirstOrDefault();
            }
        }

        public static int GetNotificationCount(int userid)
        {
            using (var db = ORM.dbPortal)
            {
                return db.Select<Notification>(w => w.Where(x => x.USER_ID == userid)).Count();
            }
        }
    }
}
