﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.PostgreSQL;

using ConvertDatabase.Models;
using Account.DAO;

namespace ConvertDatabase
{
    public static class OldPackageRepository
    {
        public static List<OldPackageModel> GetPackages(int limit)
        {
            using (var db = ORM.dbSource)
            {
                return db.Select<OldPackageModel>("select report.region as regioncode, p.status as nstatus, *, \"report\".\"id\" as reportid from \"reportpackage\" report " +				
	                "left join \"поручениеобработкиотчетности\" p on report.missionid = p.id "	+
	                "left join \"информацияопакете\" info on p.packageinfoid = info.ID "+
                    "left join \"уведомление\" sed on sed.id = p.notifysedid " +
                    "where report.\"IsImported\" ISNULL " +
                    String.Format("limit {0}", limit));

            }
        }

        public static OldPackageModel GetPackageByPortalID(string portalID)
        {
            using (var db = ORM.dbSource)
            {
                return db.Select<OldPackageModel>("select report.region as regioncode, p.status as nstatus, *, \"report\".\"id\" as reportid from \"reportpackage\" report " +				
	                "left join \"поручениеобработкиотчетности\" p on report.missionid = p.id "	+
	                "left join \"информацияопакете\" info on p.packageinfoid = info.ID "+
                    "left join \"уведомление\" sed on sed.id = p.notifysedid " +
                    "where reportmonth is not null and " +
                    String.Format("report.portalid='{0}' limit 1", portalID)).SingleOrDefault();
            }
        }

        public static OldPackageModel GetPackage(string id)
        {
            using (var db = ORM.dbSource)
            {
                return db.Select<OldPackageModel>("select report.region as regioncode, p.status as nstatus, *, \"report\".\"id\" as reportid from \"reportpackage\" report " +
                    "left join \"поручениеобработкиотчетности\" p on report.missionid = p.id " +
                    "left join \"информацияопакете\" info on p.packageinfoid = info.ID " +
                    "left join \"уведомление\" sed on sed.id = p.notifysedid " +
                    String.Format("where report.id = '{0}'", id)).SingleOrDefault();

            }
        }

        public static List<OldPackageModel> GetPackages(int limit, int offset)
        {
            using (var db = ORM.dbSource)
            {
                return db.Select<OldPackageModel>("select report.region as regioncode, p.status as nstatus, *, \"report\".\"id\" as reportid from \"reportpackage\" report " +
                    "left join \"поручениеобработкиотчетности\" p on report.missionid = p.id " +
                    "left join \"информацияопакете\" info on p.packageinfoid = info.ID " +
                    "left join \"уведомление\" sed on sed.id = p.notifysedid " +
                    String.Format("limit {0} offset {1}", limit , offset));

            }
        }

        public static List<OldPackageModel> GetPackagesForUpdate(int limit, int offset)
        {
            using (var db = ORM.dbSource)
            {
                return db.Select<OldPackageModel>("select report.region as regioncode, p.status as nstatus, *, \"report\".\"id\" as reportid from \"reportpackage\" report " +
                    "left join \"поручениеобработкиотчетности\" p on report.missionid = p.id " +
                    "left join \"информацияопакете\" info on p.packageinfoid = info.ID " +
                    "left join \"уведомление\" sed on sed.id = p.notifysedid " +
                    "where report.\"IsUpdated\" is null " +
                    String.Format("limit {0} offset {1}", limit, offset));
            }
        }

        public static List<Package> GetPackagesForUpdateMonth(int limit, int offset)
        {
            using (var db = ORM.dbDestination)
            {
                return db.Select<Package>("select p.* from \"Package\" p " +
                    " inner join \"ReportInfo\" ri on p.\"ReportID\" = ri.\"ID\" " +
                 " where p.\"PortalID\" is not null and ri.\"Month\" is null "+
                    String.Format("limit {0} offset {1}", limit, offset));
            }
        }

        public static List<Package> GetPackagesForUpdateOwner(int limit, int offset)
        {
            using (var db = ORM.dbDestination)
            {
                return db.Select<Package>("select * from \"Package\" p where p.\"PortalID\" is not null and p.\"PortalID\" <> '' and p.\"OwnerID\" = 0 order by \"ID\" " +
                    String.Format("limit {0} offset {1}", limit, offset));
            }
        }

        public static List<Package> GetImportedPackages(int limit, int offset)
        {
            using (var db = ORM.dbDestination)
            {
                return db.Select<Package>("select * from \"Package\" p where p.\"IsImported\" is true order by \"ID\" " +
                    String.Format("limit {0} offset {1}", limit, offset));
            }
        }

        public static List<Package> GetPackagesForUpdateOwnerByKiasID(int limit, int offset)
        {
            using (var db = ORM.dbDestination)
            {
                return db.Select<Package>("select * from \"Package\" p where \"IsImported\" is true and p.\"notifytimeid\" is not null and p.\"notifytimeid\" <> '' and p.\"OwnerID\" = 0 " +
                    String.Format("limit {0} offset {1}", limit, offset));
            }
        }

        public static List<Package> GetPackagesForUpdateOwnerBySedNumber(int limit, int offset)
        {
            using (var db = ORM.dbDestination)
            {
                return db.Select<Package>("select * from \"Package\" p " +
                    " inner join \"ReportInfo\" ri on p.\"ReportID\" = ri.\"ID\" " + 
                    " where \"IsImported\" is true and ri.\"SedNumber\" is not null and ri.\"SedNumber\" <> '' and p.\"OwnerID\" = 0 " +
                    String.Format("limit {0} offset {1}", limit, offset));
            }
        }

        public static void SetImported(string reportid)
        {
            using (var db = ORM.dbSource)
            {
                reportpackage report = db.Select<reportpackage>(String.Format("id = '{0}'", reportid)).SingleOrDefault();
                report.IsImported = true;
                db.Update(report);
            }
        }

        public static void SetUpdated(string reportid)
        {
            using (var db = ORM.dbSource)
            {
                reportpackage report = db.Select<reportpackage>(String.Format("id = '{0}'", reportid)).SingleOrDefault();
                report.IsUpdated = 1;
                db.Update(report);
            }
        }
    }
}
