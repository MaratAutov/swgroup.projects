﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.PostgreSQL;
using Account.DAO;

namespace ConvertDatabase
{
    class PackageRepository
    {
        class Exists
        {
            public string ID { get; set; }
        }

        public static bool PackageExists(string documentUrl, string processedDocumentUrl)
        {
            using (var db = ORM.dbDestination)
            {
                var r = db.Select<Exists>("select p.\"ID\" " +
                    "from \"Package\" p " +
                    "inner join \"ReportInfo\" info on p.\"ReportID\" = info.\"ID\" " +
                    String.Format("where p.\"IsImported\" IS True AND (info.\"DocumentUrl\" = '{0}' OR info.\"ProcessedDocumentUrl\" = '{1}') limit 1; ",documentUrl, processedDocumentUrl))
                    .SingleOrDefault();

                if (r != null)
                    return true;
                else
                    return false;
            }
        }

        public static Package GetPackage(string documentUrl)
        {
            using (var db = ORM.dbDestination)
            {
                return db.Select<Package>("select p.* " +
                    "from \"Package\" p " +
                    "inner join \"ReportInfo\" info on p.\"ReportID\" = info.\"ID\" " +
                    String.Format("where p.\"IsImported\" IS True AND info.\"DocumentUrl\" = '{0}' limit 1; ", documentUrl))
                    .SingleOrDefault();
            }
        }

        public static int GetPackageCount(int ownerid)
        {
            using (var db = ORM.dbDestination)
            {
                return db.Select<Package>(w => w.Where(x => x.OwnerID == ownerid && x.IsImported == true)).Count();
            }
        }
        
    }
}
