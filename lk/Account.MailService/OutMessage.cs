﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Account.MailService
{
    [Serializable]
    public class AttachmentFile
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public byte[] Data { get; set; }
    }

    [Serializable]
    public class OutMessage
    {
        public int PackageID { get; set; }
        public string EmlUrl { get; set; }

        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }
        public AttachmentFile[] Attachments { get; set; }
    }
}