﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceCore;
using ActiveUp.Net.Mail;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using Account.DAO;

namespace Account.MailService
{
    public class NoneFetcher : ICoreFetcher
    {
        MailService service;

        public NoneFetcher() { }

        public NoneFetcher(MailService service)
        {
            this.service = service;
        }

        public void FetchMail(int id, ServiceCoreInfo info)
        {

        }

        public bool FetchAndEnqueueMail(FetchMessage msgCount, ServiceCoreInfo info)
        {
            return true;
        }
    }
}