﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceCore;
using ActiveUp.Net.Mail;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using Account.DAO;
using Account.Tools;

namespace Account.MailService
{
    public class LotusFetcher : ICoreFetcher
    {
        MailService service;

        private LotusFetcher() { }

        public LotusFetcher(MailService service)
        {
            this.service = service;
        }

        Imap4Client fetchClient
        {
            get
            {
                if (MailService.fetchClient == null || !MailService.fetchClient.IsConnected)
                {
                    MailService.Connect(MailService.imapСfg, out MailService.fetchClient);
                    MailService.log.Debug("FetchClient initialized");
                }
                return MailService.fetchClient;
            }
        }

        Mailbox fetchInbox
        {
            get
            {
                MailService.log.Debug(
                        "Selecting FecthInbox with name: {0}",
                        MailService.imapСfg.Mailbox
                    );
                return MailService.fetchClient.SelectMailbox(MailService.imapСfg.Mailbox);
            }
        }

        public bool FetchAndEnqueueMail(FetchMessage msg, ServiceCoreInfo info)
        {
            MailService.log.Debug(
                "Started [FetchAndEnqueueMail]"
            );

            if (msg.IsMessage)
            {
                foreach (var id in msg.IDs)
                {
                    FetchMail(id, info);
                }
            }
            else
            {
                var unseenMessages = fetchInbox.Search("Unseen");
                MailService.log.Debug(
                    "Started [FetchAndEnqueueMail], Unseen count {0}", unseenMessages.Count()
                );
                foreach (var id in unseenMessages)
                {
                    //log.Debug("Fetching Mail with ID = {0}", id);
                    FetchMail(id, info);
                }
            }
            return true;
        }

        public void FetchMail(int id, ServiceCoreInfo info)
        {
            try
            {
                ++info.Processed;
                
                Message msg = fetchInbox.Fetch.MessageObject(id);
                MailService.log.Debug("Fetching Mail with ID = {0}, Subject = {1}", id, msg.Subject);
                if (msg.Subject.StartsWith("RO"))
                {
                    return;
                }
                // Sed stub configuring in Web.config auto_Answer
                if (ConfigurationManager.AppSettings["auto_Answer"] == "True" && msg.LeafMimeParts.Count == MailService.GetMimePartsCountToStub())
                {
                    Guid guid = Guid.NewGuid();
                    AttachmentFile[] atts = new AttachmentFile[1] {
                            new AttachmentFile {
                                Name = "response.xml",
                                Type = "application/xml",
                                Data = Encoding.UTF8.GetBytes(string.Format(
                                    @"<?xml version=""1.0"" encoding=""utf-8""?>
<wf:RegDocPass xmlns:wf=""http://docflow.services.fsfr.it.ru/"">       
    <num>вх{3}/13</num>
    <date>{0}</date>
    <time>{2}</time>    
    <docSystemId>{1}</docSystemId>
</wf:RegDocPass>
", DateTime.Now.ToShortDateString(), guid, DateTime.Now.ToShortTimeString(), new Random(DateTime.Now.Millisecond).Next(1,10000)))
                            }
                        };
                    string subj = msg.HeaderFields["subject"];
                    int pos = subj.IndexOf(':');
                    if (pos != -1)
                        subj = subj.Substring(0, pos);
                    MailService.log.Debug("Enqueue message from SED with subject {0}", subj);
                    MailService.coreSender.Enqueue(new OutMessage
                    {
                        From = MailService.imapСfg.Login,
                        To = MailService.imapСfg.Login,
                        Subject = subj + "/" + DateTime.Now.ToString(),
                        Attachments = atts
                    });
                    ++info.Succeeded;
                }
                int neededPartsCount = MailService.GetMimePartsCountFromSED();
                if (msg.LeafMimeParts.Count != neededPartsCount)
                {
                    MailService.log.Debug("Subject = {0}, Mime parts count = {1}, its not equal {2}", msg.Subject, msg.LeafMimeParts.Count, neededPartsCount);
                    return;
                }

#warning msg.Attachments not work but LeafMimeParts is unpredictable
                string subject = msg.HeaderFields["subject"];

                Match m = MailService.SedResponseSubject.Match(subject);
                if (m.Success)
                {
                    MailService.log.Debug("Subject = {0}, matched on PackageID = {1}", msg.Subject, m.Groups["ID"].Value);
                    int packageID = int.Parse(m.Groups["ID"].Value);

                    MailStoreInfo msi = MailStoreHelper.GetFileToStore();
                    msg.StoreToFile(msi.FileName);
                    MailService.log.Debug("Send to updating report with packageID = {0}, mailGuid = {1}", packageID, msi.guid);
                    Account.DAO.PackageService.UpdateMailMessage(
                        packageID,
                        msi.RelativeFileName,
                        msg.Subject,
                        msg.From.Email,
                        string.Join(";", msg.To),
                        "From SED"
                    );

                }

                MimePart response = msg.LeafMimeParts[0];
                MailService.log.Debug(
                    "Sed response: {0}",
                    msg.LeafMimeParts[0].TextContent
                );
                MailService.log.Debug("Posting response to SED Service");
                using (var cl = new SPOService.SPOServiceSoapClient())
                {
                    cl.ProcessSedResponse(new SPOService.SedResponse
                    {
                        Subject = subject,
                        XmlData = response.BinaryContent
                    });
                }
                string recentFolder = ConfigurationManager.AppSettings["RecentFolder"];
                if (!String.IsNullOrEmpty(recentFolder))
                    fetchInbox.MoveMessage(id, recentFolder);
                ++info.Succeeded;
            }
            catch (Exception exc)
            {
                ++info.Errors;
                MailService.log.ErrorException(
                    "Exception raised during fetched mail processing:",
                    exc
                );
            }
        }
    }
}