﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceCore;

namespace Account.MailService
{
    public interface ICoreFetcher
    {
        bool FetchAndEnqueueMail(FetchMessage msg, ServiceCoreInfo info);
        void FetchMail(int id, ServiceCoreInfo info);
    }
}