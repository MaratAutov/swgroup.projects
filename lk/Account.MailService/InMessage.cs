﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Account.MailService
{
    public class InMessage
    {
        private string Subject { get; set; }
        private string XmlResponce { get; set; }

        public InMessage(string subj, string xml)
        {
            Subject = subj;
            XmlResponce = xml;
        }
    }
}