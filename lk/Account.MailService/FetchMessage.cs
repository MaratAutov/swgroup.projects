﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Account.MailService
{
    public class FetchMessage
    {
        public int Count;
        public List<int> IDs
        {
            get;
            private set;
        }
        public bool IsMessage = false;

        public FetchMessage() { }

        public static FetchMessage GetInstance(int id)
        {
            FetchMessage message = new FetchMessage();
            message.IDs = new List<int>();
            message.IDs.Add(id);
            message.IsMessage = true;
            message.Count = 1;
            return message;
        }

        public FetchMessage(int count) 
        {
              Count = count;      
        }

        public override string ToString()
        {
            string ids = "{";
            if (IDs != null)
                ids += string.Join(", ", IDs);
            ids += "}";
            return String.Format("FetchMessage count: {0}; idenifiers: {1}", Count, ids);
        }
    }
}