﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Account.MailService
{
    public class MailHostConfig
    {
        private string hostType { get; set; } 

        public MailHostConfig(string type)
        {
            hostType = type;
        }

        private string GetFromSettings(string key, bool canBeNull=false)
        {
            string typedKey = string.Format("{0}_{1}", hostType, key);
            string value = ConfigurationManager.AppSettings[typedKey].Trim();
            if (string.IsNullOrEmpty(value) && !canBeNull)
            {
                string msg = "Configuration variable {0} not found or is empty";
                throw new ConfigurationErrorsException(
                    string.Format(msg, typedKey)
                );
            }

            return value;
        }

        public string Host
        {
            get
            {
                return GetFromSettings("host");
            }
        }

        public int Port
        {
            get
            {
                return int.Parse(GetFromSettings("port"));
            }
        }

        public string Login
        {
            get
            {
                return GetFromSettings("login");
            }
        }

        public string Password
        {
            get
            {
                return GetFromSettings("pass");
            }
        }

        public bool UseSSL
        {
            get
            {
                return Convert.ToBoolean(GetFromSettings("useSSL"));
            }
        }

        public bool UseAutorization
        {
            get
            {
                return Convert.ToBoolean(GetFromSettings("useAutorization"));
            }
        }

        public string Mailbox
        {
            get
            {
                return GetFromSettings("mailbox", true);
            }
        }
    }
}