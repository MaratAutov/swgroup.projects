﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Mail;
using System.Configuration;

using ActiveUp.Net.Mail;
using NLog;


using Account.Tools;
using ServiceCore;
using System.ComponentModel;


namespace Account.MailService
{
    public enum smtp_type
    {
        SED = 0,
        gmail
    }
    
    /// <summary>
    /// Summary description for MailService
    /// </summary>
    [WebService(Namespace = "http://aetp.ru/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MailService : System.Web.Services.WebService
    {
        internal static string archiveRoot = Path.GetFullPath(
            ConfigurationManager.AppSettings["ArchiveDir"]
        );
        static smtp_type? _smtp_type = null;
        internal static smtp_type Smtp_type
        {
            get
            {
                if (_smtp_type == null)
                {
                    string str_value = ConfigurationManager.AppSettings["smtp_type"];
                    switch (str_value)
                    {
                        case "SED":
                            _smtp_type = smtp_type.SED;
                            break;
                        case "gmail":
                            _smtp_type = smtp_type.gmail;
                            break;
                        default:
                            _smtp_type = smtp_type.SED;
                            break;
                    }
                }
                return (smtp_type)_smtp_type;
            }    
        }

        internal static int GetMimePartsCountFromSED()
        {
            switch (Smtp_type)
            {
                case smtp_type.SED: return 1;
                case smtp_type.gmail: return 2;
                default: throw new Exception("Not properly set smtp type");
            }
        }

        internal static int GetMimePartsCountToStub()
        {
            switch (Smtp_type)
            {
                case smtp_type.SED: return 2;
                case smtp_type.gmail: return 3;
                default: throw new Exception("Not properly set smtp type");
            }
        }

        /// <summary>
        /// Регулярное выражение для разбора темы письма из СЭД:
        /// [ID Пакета]:[Входящий Номер]/[Номер ФСФР]
        /// </summary>
        internal static Regex SedResponseSubject = new Regex(
            "(?<ID>[^/]+)/(?<sedDate>.*)"
        );

        /// <summary>
        /// [Type] Reviewer: bundle "[код_региона]-[цифра_цифра]-&lt;[номер_СЭД]>" ([дата_в_формате_дд.мм.гггг])
        /// </summary>
        internal static Regex RoRequestSubject = new Regex(
            "(?<type>SMCL|XTDL)\\s+Reviewer:\\s+bundle\\s+\\\"[-0-9]{1,}-[0-9]{1,}-<(?<sedNum>.*)>\\\"\\s+\\((?<sedDate>[-0-3][-0-9]\\.(01|02|03|04|05|06|07|08|09|10|11|12)\\.[-0-9]{4})\\)"
        );

        private static string ServiceName = "Почтовый сервис";
        internal static NLog.Logger log;
        ICoreFetcher fetcher;
        public delegate void NewMessageReceived(object obj, NewMessageReceivedEventArgs e);

        private static ServiceCore<int> coreRoFetcher;
        internal static Imap4Client listenRoClient;
        internal static Imap4Client fetchRoClient;
        
        private static ServiceCore<FetchMessage> coreFetcher;
        //private static SPOService.SPOServiceSoapClient sedClient;
        internal static Imap4Client listenClient;
        internal static Imap4Client fetchClient;
        
        internal static MailHostConfig imapСfg;
        internal static MailHostConfig imapRoСfg;

        static ServiceCore<OutMessage> _coreSender;
        internal static ServiceCore<OutMessage> coreSender
        {
            get
            {
                if (_coreSender == null)
                {
                    coreSender = ServiceCore<OutMessage>.GetInstance(
                        "Сервис отправки запроса в СЭД",
                        log
                    );
                    coreSender.InitEvents(SendMail);
                }
                return _coreSender;
            }
            set
            {
                _coreSender = value;
            }
        }
        //internal static LumiSoft.Net.SMTP.Client.SMTP_Client sendClient;
        internal static MailHostConfig smtpCfg;

        private static bool isInited;
        private static bool isIniting;

        public MailService()
        {
            
            if (isInited || isIniting)
                return;

            isIniting = true;
                        
#warning При переключении СЭД поменять не забыть SpoService
            fetcher = new LotusFetcher(this);
            
            if (log == null) {
                var logFile = ConfigurationManager.AppSettings["LogFile"];
                log = NLogger.GetLog(ServiceName, logFile);
                log.Debug("MailMan Core instance initialization");
            }

            if (!Directory.Exists(archiveRoot)) {
                try {
                    Directory.CreateDirectory(archiveRoot);
                } catch (Exception exc) {
                    log.ErrorException(
                        "Failed to create email archive target directory",
                        exc                        
                    );
                }
            }

            log.Debug("Loading MailHost configurations");
            imapСfg = new MailHostConfig("imap");
            imapRoСfg = new MailHostConfig("imap_ro");
            smtpCfg = new MailHostConfig("smtp");            

            Connect(imapСfg, out fetchClient);
            Connect(imapСfg, out listenClient);
            Connect(imapRoСfg, out listenRoClient);

            //sendClient = new LumiSoft.Net.SMTP.Client.SMTP_Client();
            
            coreFetcher = ServiceCore<FetchMessage>.GetInstance(
                "Сервис получения ответа СЭД",
                log
            );
            coreFetcher.InitEvents(FetchAndEnqueueMail);

            coreRoFetcher = ServiceCore<int>.GetInstance(
                "Сервис получения запроса РО СЭД",
                log
            );
            coreRoFetcher.InitEvents(FetchAndEnqueueRoMail);
            
            startListener();
            startRoListener();

            isIniting = false;
            isInited = true;
        }

        void startListener()
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += new DoWorkEventHandler(StartListen);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            bw.RunWorkerAsync(new StartListenParams() { client = listenClient, callback = OnMessageReceived, config = imapСfg });
        }

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            startListener();
        }

        void startRoListener()
        {
            BackgroundWorker bwRo = new BackgroundWorker();
            bwRo.DoWork += new DoWorkEventHandler(StartListen);
            bwRo.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwRo_RunWorkerCompleted);
            bwRo.RunWorkerAsync(new StartListenParams() { client = listenRoClient, callback = OnRoMessageReceived, config = imapRoСfg });
        }

        void bwRo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            startRoListener();
        }

        ~MailService()
        {
            try {
                if (listenClient != null) {
                    listenClient.Close();
                }
                if (listenRoClient != null) {
                    listenRoClient.Close();
                }
            } catch (Exception exc) {
                log.ErrorException(
                    "Failed to shutdown gracefully",
                    exc
                );
            }
        }

        #region MailService Web methods
        [WebMethod]
        public void EnqueueForSend(OutMessage msg)
        {
            log.Debug("Started [EnqueueForSend] send to {0} subject {1}", msg.To ?? "", msg.Subject ?? "");
            coreSender.Enqueue(msg);
        }
        /// <summary>
        /// для почтовых ящиков которые поддерживают полнотекстовый поиск писем
        /// </summary>
        /// <param name="mail">Почтовый ящик</param>
        /// <param name="subject">тема для поиска</param>
        /// <returns></returns>
        IEnumerable<Message> GetMessageFromSearch(Mailbox mail, string subject)
        {
            return mail.SearchParse(String.Format("SUBJECT {0}", subject)).Cast<Message>();
        }
        /// <summary>
        /// Почтовый сервер Lotus notes  не поддерживает полнотектовый поиск писем, делаем перебор
        /// </summary>
        /// <param name="mail">Почтовый ящик</param>
        /// <param name="subject">тема для поиска</param>
        /// <returns></returns>
        IEnumerable<Message> GetMessageFromBruteforce(Mailbox mail, string subject)
        {
            int[] ids = mail.Search("ALL");
            foreach (int id in ids)
            {
                Message msg = mail.Fetch.MessageObject(id);
                if (msg.Subject.StartsWith(subject))
                    yield return msg;
            }
        }

        [WebMethod]
        public void EnqueueForFetch(string SubjectQuery)
        {
            log.Debug("Started [EnqueueForFetch]");
            if (fetchClient == null || !fetchClient.IsConnected)
            {
                Connect(imapСfg, out fetchClient);
            }
            var fetchInbox = fetchClient.SelectMailbox(imapСfg.Mailbox);
            log.Debug("Total messages count = {0}", fetchInbox.MessageCount);
            IEnumerable<Message> fetchInboxSearchParse = GetMessageFromBruteforce(fetchInbox, SubjectQuery).OrderByDescending(x => x.Date);
            
            if (fetchInboxSearchParse == null)
            {
                MailService.log.Debug("Not found message with subject {0}", SubjectQuery);
                return;
            }
            MailService.log.Debug("Found {0} messages with subject: {1}", fetchInboxSearchParse.Count(), SubjectQuery);
            foreach (Message msg in fetchInboxSearchParse)
            {
                log.Debug("Fetching msg with subject: {0}; id = {1}", msg.Subject,  msg.IndexOnServer);
                coreFetcher.Enqueue(FetchMessage.GetInstance(msg.Id));
            }    
        }

        [WebMethod]
        public List<ServiceCoreInfo> Status()
        {
            log.Debug("Started [Status]");            
            while (isIniting);

            List<ServiceCoreInfo> result = new List<ServiceCoreInfo>() {
                coreSender.GetInfo(),
                coreFetcher.GetInfo()
            };
            return result;
        }

        [WebMethod]
        public void Shutdown(bool clearFetchQueue, bool clearSendQueue)
        {            
            if (!isInited || isIniting)
                return;

            log.Debug("Started [Shutdown]");

            if (listenClient != null) {
                listenClient.StopIdle();
                listenClient.Close();
            }

            if( fetchClient != null ) {
                coreFetcher.Shutdown(clearFetchQueue);
                fetchClient.Close();
            }

            coreSender.Shutdown(clearSendQueue);
        }

        [WebMethod]
        public void ResetStats()
        {
            coreFetcher.ResetInfo();
            coreSender.ResetInfo();
        }
        #endregion

        #region coreSender routine
        
        
        private static bool SendMail(OutMessage msg, ServiceCoreInfo info)
        {
            log.Debug("Started [SendMail]");
            var new_sendClient = new LumiSoft.Net.SMTP.Client.SMTP_Client();
            //connect to server every time
            try
            {
                log.Debug("Host: {0} Port: {1} SSL: {2}", smtpCfg.Host, smtpCfg.Port, smtpCfg.UseSSL);
                new_sendClient.Connect(smtpCfg.Host, smtpCfg.Port, smtpCfg.UseSSL);
                new_sendClient.EhloHelo(smtpCfg.Host);
                log.Debug(
                    "SendClient conected with: Login={0} | EnableSSL={1}",
                    smtpCfg.Login,
                    smtpCfg.UseSSL
                );
            }
            catch (Exception ex)
            {
                log.Error(
                    "Connecting error with message: {0}",
                    ex
                );
            }
            

            if (smtpCfg.UseAutorization)
            {
                if (!new_sendClient.IsAuthenticated)
                {
                    new_sendClient.Authenticate(smtpCfg.Login, smtpCfg.Password);
                }
            }

            ++info.Processed;
            try {
                MailMessage mail = null;

                log.Debug("Generating MailMessage; from={0}; to={1}; subject={2}", msg.From, msg.To, msg.Subject);
                mail = new MailMessage(msg.From, msg.To);
                mail.Subject = msg.Subject;
                mail.IsBodyHtml = msg.IsHtml;
                mail.Body = msg.Body;
                    

                if (msg.Attachments != null)
                foreach (AttachmentFile f in msg.Attachments) {
                    // this stream should be closed after sedClient.Send
                    string name = f.Name;
                    MemoryStream stream = new MemoryStream(f.Data);
                    System.Net.Mail.Attachment request = 
                        new System.Net.Mail.Attachment(
                            stream, name, f.Type
                        );
                    request.ContentDisposition.FileName = name;
                    mail.Attachments.Add(request);
                }
                
                log.Debug("Sending MailMessage in Thread");
                new_sendClient.MailFrom(msg.From, (long)-1);
                new_sendClient.RcptTo(msg.To);
                new_sendClient.SendMessage(mail.GetStream());
                              
                if (msg.PackageID != 0) {
                    var mailStore = MailStoreHelper.GetFileToStore();
                    mail.Save(mailStore.FileName);
                    
                    Account.DAO.PackageService.UpdateMailMessage(
                        msg.PackageID, 
                        mailStore.RelativeFileName, 
                        msg.Subject, 
                        msg.From, 
                        msg.To, 
                        "To SED"
                    );
                }                
            
                ++info.Succeeded;
                return true;
            } catch (Exception exc) {
                log.Error(
                    "Failed to send message subject = {0}; Exception:\n{1}",
                    msg.Subject,
                    exc
                );
                ++info.Errors;
                return false;
            }
        }
        #endregion

        #region coreFetcher routines
        private bool FetchAndEnqueueMail(FetchMessage msg, ServiceCoreInfo info)
        {
            return fetcher.FetchAndEnqueueMail(msg, info);
        }

        #endregion

        #region coreRoFetcher routines
        private bool FetchAndEnqueueRoMail(int msgCount, ServiceCoreInfo info)
        {
            log.Debug(
                "Started [FetchAndEnqueueRoMail] with msgCount = {0}",
                msgCount
            );
            if (fetchRoClient == null) {
                Connect(imapRoСfg, out fetchRoClient);
                log.Debug("FetchRoClient initialized");
            }

            log.Debug(
                "Selecting FecthInbox with name: {0}",
                imapRoСfg.Mailbox
            );
            var fetchInbox = fetchRoClient.SelectMailbox(imapRoСfg.Mailbox);
            foreach (var id in fetchInbox.Search("Unseen")) {
                log.Debug("Fetching RoMail with ID = {0}", id);
                try {
                    ++info.Processed;
                    Message msg = fetchInbox.Fetch.MessageObject(id);
                    
                    string subject = msg.HeaderFields["subject"];

                    Match m = RoRequestSubject.Match(subject);
                    if (!m.Success) {
                        continue;
                    }

                    MimePart file = null;
                    foreach (MimePart mime in msg.LeafMimeParts) {
                        if (!string.IsNullOrEmpty(mime.Filename)) {
                            file = mime;
                        }
                    }

                    if (file == null)
                        continue;

                    var mailStore = MailStoreHelper.GetFileToStore();
                    msg.StoreToFile(mailStore.FileName);

                    log.Debug("Posting response to SED Service");
                    using (var cl = new SPOService.SPOServiceSoapClient())
                    {
                        string res = cl.UploadPackage(
                            file.Filename,
                            file.BinaryContent,
                            -1,
                            DateTime.Now,
                            true,
                            subject
                        );

                        Account.DAO.PackageService.UpdateMailMessage(
                            int.Parse(res),
                            mailStore.RelativeFileName,
                            msg.Subject,
                            msg.From.Email,
                            string.Join(";", msg.To),
                            "From RO"
                        );
                    }
                    string recentFolder = ConfigurationManager.AppSettings["RecentFolder"];
                    if (!String.IsNullOrEmpty(recentFolder))
                        fetchInbox.MoveMessage(id, recentFolder);
                    ++info.Succeeded;
                } catch (Exception exc) {
                    ++info.Errors;
                    log.DebugException(
                        "Exception raised during ro fetched mail processing:",
                        exc
                    );                    
                }
            }
            return true;
        }
        #endregion

        #region Listen routines
        static Object syncConnect = new Object();
        public class StartListenParams
        {
            public MailHostConfig config;
            public Imap4Client client;
            public NewMessageReceived callback;
        }

        static void StartListen(object sender, DoWorkEventArgs e)
        {
            
            var param = e.Argument as StartListenParams;
            Imap4Client client = param.client;
            NewMessageReceived callback = param.callback;
            log.Debug("Starting Listen Thread Host: {0}", param.config.Host);
            lock (syncConnect)
            {
                if (client == null || !client.IsConnected)
                {
                    try
                    {
                        Connect(param.config, out client);
                    }
                    catch (Exception ex)
                    {
                        log.DebugException(String.Format("Error connecting to imap server {0} ", param.config.Host), ex);
                        return;
                    }
                }
            }

            if (client.ServerCapabilities.Contains("IDLE"))
            {
                client.NewMessageReceived += new NewMessageReceivedEventHandler(callback);
                client.SelectMailbox(param.config.Mailbox).Subscribe();

                try
                {
                    log.Debug("Starting IMAP IDLE mode");
                    client.StartIdle();
                }
                catch (Exception exc)
                {
                    log.Error(
                        "Exception in Listen Thread{0}{1}",
                        Environment.NewLine,
                        exc
                    );

                    try
                    {
                        client.Close();
                    }
                    catch (Exception)
                    {
                        // pass
                    }
                    finally
                    {
                        client = null;
                    }

                    StartListen(sender, e);
                }
            }
            else
            {
                while (true)
                {
                    
                    Thread.Sleep(30000);
                    log.Debug("Starting checking new messages on {0}", param.config.Host);
                    try
                    {
                        callback(
                            null,
                            new NewMessageReceivedEventArgs(1)
                        );
                    }
                    catch (Exception ex)
                    {
                        log.DebugException("Error while recieving new message: ", ex);
                    }
                }
            }
        }

        private static void OnMessageReceived(object sender, NewMessageReceivedEventArgs e)
        {
            log.Debug("Started [OnMessageRecieved]");
            coreFetcher.Enqueue(new FetchMessage(e.MessageCount));
        }

        private static void OnRoMessageReceived(object sender, NewMessageReceivedEventArgs e)
        {
            log.Debug("Started [OnMessageRecieved]");
            coreRoFetcher.Enqueue(e.MessageCount);
        }
        #endregion

        #region Helper methods
        internal static bool Connect(MailHostConfig cfg, out Imap4Client client)
        {
            
            log.Debug(
                "Establishing connection to {0}:{1}",
                cfg.Host, cfg.Port
            );
            
            client = new Imap4Client();
            try
            {
                if (cfg.UseSSL)
                    client.ConnectSsl(cfg.Host, cfg.Port);
                else
                    client.Connect(cfg.Host, cfg.Port);
                client.Login(cfg.Login, cfg.Password);

                return client.IsConnected;
            }
            catch (Exception ex)
            {
                
                log.DebugException(
                    "Exception while establishing coonection to " + cfg.Host, ex);
                return false;
            }
        }
        #endregion
    }
}