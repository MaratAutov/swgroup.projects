﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Account.MailService
{
    public class StatusInfo
    {
        public bool IsListenerReady;
        public bool IsListenerConnected;
        public bool IsFetcherConnected;
        public bool IsProcessorActive;
    }
}