﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using Quartz.Impl;
using System.Threading;
using System.Net;
using System.Security.Cryptography;
using System.Configuration;


namespace Account.WorkMonitoring
{
    public class SeldonFSFRMonitoringWorkJob : IJob
    {
        public static string Error { get; set; }      

        public void Execute(IJobExecutionContext context)
        {
            Console.WriteLine("Мониторинг работоспособности сервисов:");
            GetStatusLK();
            GetStatusMailServise();
            GetStatusSPO();
            GetStatusReportServise(); 
        }

        public static void SendMail(string error, string title)
        {
            Console.WriteLine("Отправляем письмо...");
            string[] mails = ConfigurationManager.AppSettings["NotificationEmail"].Split(',');
            try
            {
                foreach (var mail in mails)
                {

                    using (var cl = new MailService.MailServiceSoapClient())
                    {
                        cl.EnqueueForSend(new MailService.OutMessage()
                        {
                            From = "fsfr.support@fcsm.ru",
                            To = mail,
                            Subject = title,
                            Body = error,
                            IsHtml = true
                        });
                    }
                    Console.WriteLine("{0} OK", mail);
                }
                Console.WriteLine("Готово.");
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }

        public static void GetStatusLK()
        {
            Console.WriteLine("Проверка работоспособности личного кабинета...");
            try
            {
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(ConfigurationManager.AppSettings["AccountURL"]));
                myHttpWebRequest.Proxy = null;
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
                    Error = "";
                myHttpWebResponse.Close();

                Console.WriteLine("Статус:Активен");
            }
            catch (WebException e)
            {
                Error = string.Format("\r\nWebException Raised. The following error occured : {0}; message: {1}", e.Status, e.Message);

                Console.WriteLine(Error);
                SendMail(Error, "Проверка работоспособности личного кабинета");
            }
            catch (Exception e)
            {
                Error = string.Format("\nThe following Exception was raised : {0}", e.Message);

                Console.WriteLine(Error);
                SendMail(Error, "Проверка работоспособности личного кабинета");
            }
        }

        public static void GetStatusSPO()
        {
            Console.WriteLine("Проверка работоспособности SPOService...");
            try
            {
                using (SPOService.SPOServiceSoapClient cl = new SPOService.SPOServiceSoapClient())
                {
                    if (!cl.Status().IsActive)
                    {
                        SendMail(cl.Status().Errors.ToString(), "Проверка работоспособности SPOService");
                    }
                    else
                    {
                        Console.WriteLine("Статус:Активен");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                SendMail(Error, "Проверка работоспособности SPOService");
            }
        }

        public static void GetStatusReportServise()
        {
            Console.WriteLine("Проверка работоспособности ReportService...");
            try
            {
                using (ReportService.ReportServiceSoapClient cl = new ReportService.ReportServiceSoapClient())
                {
                    
                    if (!cl.Status().IsActive)
                    {
                        SendMail(cl.Status().Errors.ToString(), "Проверка работоспособности ReportService");
                    }
                    else
                    {
                        Console.WriteLine("Статус:Активен");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                SendMail(Error, "Проверка работоспособности ReportService");
            }
        }


        public static void GetStatusMailServise()
        {
            Console.WriteLine("Проверка работоспособности MailService...");
            try
            {
                using (MailService.MailServiceSoapClient cl = new MailService.MailServiceSoapClient())
                    foreach (var s in cl.Status())
                    {
                        if (!s.IsActive)
                        {
                            SendMail(s.Errors.ToString(), "Проверка работоспособности MailService");
                        }
                        else
                        {
                            Console.WriteLine("Статус:Активен");
                        }
                    }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                SendMail(Error, "Проверка работоспособности MailService");
            }
        }
    }
}
