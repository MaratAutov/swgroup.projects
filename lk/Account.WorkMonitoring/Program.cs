﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Account;
using System.Web;
using System.Net;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;

namespace Account.WorkMonitoring
{
    class Program
    {        
        public static string Error { get; set; }       

        static void Main(string[] args)
        {
            
            ISchedulerFactory schedFact = new StdSchedulerFactory();
            IScheduler sched = schedFact.GetScheduler();
            sched.Start();
            JobDetailImpl jobDetail = new JobDetailImpl("FSFRAccountWorkMonitoring", null, typeof(SeldonFSFRMonitoringWorkJob));
            SimpleTriggerImpl trigger = new SimpleTriggerImpl("AccountWorkMonitoring", "FSFR",-1, TimeSpan.FromSeconds(Convert.ToInt32(ConfigurationManager.AppSettings["SyncTimeout"])));
        
            sched.ScheduleJob(jobDetail, trigger);                
        }       

    }
}
