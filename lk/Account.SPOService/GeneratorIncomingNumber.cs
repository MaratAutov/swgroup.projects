﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Account.DAO;

namespace SPO
{
    public interface IGeneratorIncomingNumber
    {
        string Generate(int packageID);
    }

    public class GeneratorIncomingNumberSimple : IGeneratorIncomingNumber
    {
        public string Generate(int packageID)
        {
            return new Random(DateTime.Now.Millisecond).Next(1, 10000).ToString();
        }
    }

    public class PostgresFunctionGenerator : IGeneratorIncomingNumber
    {
        public string Generate(int packageID)
        {
            return FunctionsService.GetIncomingNumber(packageID);
        }
    }
}