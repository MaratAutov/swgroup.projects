﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Account.DAO;

namespace SPO
{
    public class SEDCardInfoException : Exception
    {
        string message;
        string code;
        public override string Message
        {
            get
            {
                return message;
            }
        }

        public string Code
        {
            get
            {
                return code;
            }
        }

        public SEDCardInfoException(string message, string code)
        {
            this.message = message;
            this.code = code;
        }
    }

    public class SEDCardInfoValidator : CustomValidator
    {
        public int PackageID { get; set; }

        public SEDCardInfoValidator(int packageID)
        {
            PackageID = packageID;
        }

        List<CustomValidator.ValidationItem> _items;
        protected override List<CustomValidator.ValidationItem> Items
        {
            get 
            {
                if (_items == null)
                {
                    _items = new List<ValidationItem>();
                    /*
#warning возможно стоит придумать правила проверки кодов
#if DEBUG
                    _items.Add(new ValidationItem("docTypeCode", "Не определен тип отчета", ValidateDocTypeCode));
                    _items.Add(new ValidationItem("orgTypeCode", "Не определен тип организации", ValidateOrgTypeCode));
#endif
                     */
                    _items.Add(new ValidationItem("RegNumber", "Документ с таким исходящим номером уже существует.", ValidateRegNumber));
                    _items.Add(new ValidationItem("RegDate", "Неверно задана дата регистрации", ValidateRegDate));
                }
                return _items;
            }
        }

        bool ValidateDocTypeCode(string value)
        {
            return true;
        }

        bool ValidateOrgTypeCode(string value)
        {
            return true;
        }

        bool ValidateRegNumber(string value)
        {
            var package = PackageService.GetFullInfo(PackageID);
            if (PackageService.IssetDublicateOutgoing(PackageID, CardInfo["INN"], CardInfo["OGRN"], CardInfo["RegNumber"], DateTime.Now.Year.ToString()))
            {
                throw new SEDCardInfoException("Документ с таким исходящим номером уже существует.", "502");
            }
            SPOService.log.Info("Reg number " + value + " is valid");
            return true;
        }

        bool ValidateRegDate(string value)
        {
            DateTime dt;
            if (!DateTime.TryParse(value, out  dt))
            {
                throw new SEDCardInfoException("Неверно задана дата регистрации", "502");
            }
            else
            {
                if (dt.Year < 2000 || dt.Year > 2100)
                    throw new SEDCardInfoException("Неверно задан год регистрации", "502");
            }
            return true;
        }

        public SEDCardInfoValidator(Dictionary<string, string> cardInfo)
        {
            CardInfo = cardInfo;
        }

        protected override void Validation()
        {

            foreach (var item in Items)
            {
                if (!item.IsValid(CardInfo[item.FieldName]))
                    Errors.Add(item.ErrorMsg);
            }
            if (Errors.Count > 0)
                throw new SEDCardInfoException(Error, "502");
        }

        public override string Error
        {
            get { return String.Format("{{ERROR_FIELDS_EMPTY}} Не заданы необходимые поля: {0}", String.Join(";", Errors)); }
        }
    }
}