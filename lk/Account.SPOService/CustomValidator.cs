﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPO
{
    public abstract class CustomValidator
    {
        public List<string> Errors;
        protected Dictionary<string, string> CardInfo;
        protected abstract List<ValidationItem> Items { get; }

        protected delegate bool ValidationFunc(string value);

        protected bool ValidateOnStringIsNullOrEmpty(string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        protected bool ValidateOnZero(string value)
        {
            return value != "0";
        }

        protected bool ValidateOnNotFound(string value)
        {
            return value != "<не известно>";
        }

        protected abstract void Validation();
        public void Validate()
        {
            Errors = new List<string>();
            if (Items == null)
                return;
            if (CardInfo == null)
                throw new ArgumentNullException("Значимая инофрмация из отчета не может быть прочитана");
            Validation();
        }
        public void Validate(Dictionary<string, string> cardInfo)
        {
            CardInfo = cardInfo;
            Validate();
        }
        public abstract string Error { get; }

        protected class ValidationItem
        {
            public string FieldName;
            public string ErrorMsg;
            public ValidationFunc IsValid;

            public ValidationItem(string fieldName, string errorMsg, ValidationFunc isValid)
	        {
                FieldName = fieldName;
                ErrorMsg = errorMsg;
                IsValid = isValid;
	        }

        }

    }
}
