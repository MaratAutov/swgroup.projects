﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;

using ICSharpCode.SharpZipLib.Zip;

using Account.DAO;
using Account.DAO.SED;
using Account.Tools;
using Account.Tools.MailService;


namespace SPO
{
    public class LetterGenerator
    {
        private XmlHelper xml;
        private InteropPackage Package;

        private static Dictionary<string, List<ModelInfo>> Configurations;
        private static Dictionary<string, DocMap> DocMapping;
        private static Dictionary<string, OrgMap> OrgMapping;
        private static string SEDRequestTemplate;

        public string RequestTemplatePath { get; set; }

        public LetterGenerator(InteropPackage package)
        {
            Package = package;
            
            LoadConfigurations(true);

            xml = new XmlHelper(package.XmlData);
        }


        public Dictionary<string, string> CreateInfo()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (var mapping in Configurations["common"])
            {
                if (mapping.ModelVersion != Package.Version)
                    continue;
                string value = mapping.Value;
                if (!mapping.IsStatic)
                    value =  xml.GetValue(value);
                if (!result.ContainsKey(mapping.Attr) || (result.ContainsKey(mapping.Attr) && String.IsNullOrEmpty(result[mapping.Attr])))
                {
                    result[mapping.Attr] = value;
                }
            }

            string firstNamePath = null, middleNamePath = null;
            
            if(Configurations.ContainsKey(Package.Model))
            {
                foreach (var mapping in Configurations[Package.Model])
                {
                    // берем правила только для пакетов интересующей нас версии
                    if (mapping.ModelVersion != Package.Version)
                        continue;
                    if (mapping.Attr == "SignerFirstName")
                        firstNamePath = mapping.Value;
                    if (mapping.Attr == "SignerMiddleName")
                        middleNamePath = mapping.Value;

                    string value = mapping.Value;
                    if (!mapping.IsStatic)
                    {
                        value = xml.GetValue(value);
                    }

                    if (!result.ContainsKey(mapping.Attr) || (result.ContainsKey(mapping.Attr) && String.IsNullOrEmpty(result[mapping.Attr])))
                    {
                        result[mapping.Attr] = value;
                    }
                }
            }
            //если в кофиге не задано определение для имени и отчества то они находятся в поле фамилии
            if (firstNamePath == String.Empty && middleNamePath == String.Empty)
            {
                string fio = result["SignerLastName"];
                if (!string.IsNullOrEmpty(fio))
                {
                    string[] words = fio.Split(' ', '.');
                    if (words.Length >= 3)
                    {
                        result["SignerFirstName"] = words[1];
                        result["SignerMiddleName"] = words[2];
                        result["SignerLastName"] = words[0];
                    }
                }
            }

            if( !result.ContainsKey("docTypeCode") && !result.ContainsKey("docTypeName"))
            {
                if (!DocMapping.ContainsKey(Package.Type)) {
                    result["docTypeCode"] = "0";
                    result["docTypeName"] = "<не известно>";
                } else {
                    result["docTypeCode"] = DocMapping[Package.Type].Code;
                    result["docTypeName"] = DocMapping[Package.Type].Name;
                }
            }
            
            // приведение кода к int так было в таром СПО
            result["docTypeCode"] = Int32.Parse(result["docTypeCode"]).ToString();

            if (result.ContainsKey("orgCode") && String.IsNullOrEmpty(result["orgCode"]))
                throw new Exception("Не удалось определить вид организации");

            if (!result.ContainsKey("orgTypeCode") && !result.ContainsKey("orgTypeName"))
            {
                if (!OrgMapping.ContainsKey(result["orgCode"])) {
                    result["orgTypeCode"] = "0";
                    result["orgTypeName"] = "<не известно>";
                } else {
                    result["orgTypeCode"] = OrgMapping[result["orgCode"]].Code;
                    result["orgTypeName"] = OrgMapping[result["orgCode"]].Name;
                }
            }

            


            if (ConfigurationManager.AppSettings["TestMode"] == "True")
            {
                if (result.ContainsKey("RegNumber")) result["RegNumber"] = "Test____" + result["RegNumber"];
            }
            return result;
        }
        
        public static void LoadConfigurations(bool reset=true)
        {
            if (reset)
            {
                Configurations = new Dictionary<string, List<ModelInfo>>();
                DocMapping = new Dictionary<string, DocMap>();
                OrgMapping = new Dictionary<string, OrgMap>();
            }

            foreach(var cfg in SEDMappingService.GetConfigurations())
            {
                // common configuration elements has NULL in db Model field
                if (cfg.Model == null )
                    cfg.Model = "common";

                if( !Configurations.ContainsKey(cfg.Model) )
                    Configurations.Add(cfg.Model, new List<ModelInfo>());

                Configurations[cfg.Model].Add(cfg);
            }

            foreach (var cfg in SEDMappingService.GetDocMapping())
                DocMapping[cfg.Type] = cfg;

            foreach (var cfg in SEDMappingService.GetOrgMapping())
                OrgMapping[cfg.Type] = cfg;
        }

        private byte[] CreateRequest(Dictionary<string, string> info)
        {
            if (SEDRequestTemplate == null )
            {
                string tplName = null;
                if (HostingEnvironment.ApplicationPhysicalPath == null)
                    tplName = RequestTemplatePath;
                else
                    tplName = Path.Combine(
                        HostingEnvironment.ApplicationPhysicalPath,
                        @"App_Data\request_template.xml"
                    );

                using(StreamReader reader = new StreamReader(tplName))
                    SEDRequestTemplate = reader.ReadToEnd();
            }            
            
            string xmlBody = SEDRequestTemplate;
            foreach( var item in info )
            {
                string placeholder = string.Format("{{{0}}}", item.Key);
                xmlBody = xmlBody.Replace(placeholder, item.Value);
            }

            return Encoding.UTF8.GetBytes(xmlBody);
        }

        private byte[] CreateReport()
        {
            Stream outStream = new MemoryStream();
            using(ZipFile zip = ZipFile.Create(outStream))
            {
                zip.BeginUpdate();
                CustomStaticDataSource reportData = new CustomStaticDataSource();
                reportData.SetStream(new MemoryStream(Package.XmlData));

                zip.Add(reportData, "report.xtdd", CompressionMethod.Stored);
                
                zip.CommitUpdate();

                zip.TestArchive(true);
                
                zip.IsStreamOwner = false;
                zip.Close();

                outStream.Position = 0;
            }
            byte[] data = new byte[outStream.Length];
            outStream.Read(data, 0, (int)outStream.Length);
            return data;
        }

        public OutMessage ComposeMessage(Dictionary<string, string> info)
        {
            string subject = Package.Id.ToString();

            byte[] requestData = CreateRequest(info);
            byte[] reportData = CreateReport();

            OutMessage msg = new OutMessage()
            {
                Subject = subject,
                From = ConfigurationManager.AppSettings["from-sed-email"],
                To = ConfigurationManager.AppSettings["to-sed-email"]
            };
            msg.Attachments = new AttachmentFile[2]
            {
                new AttachmentFile()
                {
                    Data = requestData,
                    Name = "request.xml",
                    Type = "application/xml"
                },
                new AttachmentFile()
                {
                    Data = reportData,
                    Name = "document.zip",
                    Type = "application/zip"
                }
            };

            return msg;
        }
    }

    public class CustomStaticDataSource : IStaticDataSource
    {
        private Stream _stream;
        // Implement method from IStaticDataSource
        public Stream GetSource()
        {
            return _stream;
        }

        // Call this to provide the memorystream
        public void SetStream(Stream inputStream)
        {
            _stream = inputStream;
            _stream.Position = 0;
        }
    }
}