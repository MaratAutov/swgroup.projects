﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceCore;
using System.Text.RegularExpressions;
using Account.Tools;
using Account.DAO;
using System.Text;

namespace SPO
{
    public class ProcessPackageSelf : ProcessPackage2Lotus
    {
        IGeneratorIncomingNumber generatorSedNumber;

        public ProcessPackageSelf(SPOService service, IGeneratorIncomingNumber generatorSedNumber)
        {
            spoService = service;
            this.generatorSedNumber = generatorSedNumber;
        }
                
        public override bool processPackage(InteropPackage package, ServiceCoreInfo info)
        {
            SPOService.log.Debug(
                "Starting [processPackage] with package:{0}{1} (self)",
                Environment.NewLine,
                package
            );

            ++info.Processed;
            LetterGenerator gen = new LetterGenerator(package);
            Dictionary<string, string> cardInfo = null;
            try
            {
                cardInfo = gen.CreateInfo();
                
                RequiredCardInfoValidator validator = new RequiredCardInfoValidator(cardInfo);
                validator.Validate();
            }
            catch (RequiredCardInfoException ex)
            {
                SPOService.log.Debug(String.Format("Error validating package id {0} excption {1} (self)", package.Id, ex));
                UpdateStatusPackage.Build(
                    package.Id,
                    PrimaryStatus.REPORT_INVALID,
                    SecondaryStatus.ERROR_FIELDS_EMPTY,
                    info: ex.Message,
                    excInfo: ex.ToString()
                );
                
                ++info.Succeeded;
                return false;
            }
            catch (Exception ex)
            {
                SPOService.log.Debug(String.Format("Error validating package id {0} excption {1} (self)", package.Id, ex));
                UpdateStatusPackage.Build(
                    package.Id,
                    PrimaryStatus.REPORT_INVALID,
                    SecondaryStatus.ERROR_FIELDS_EMPTY,
                    info: "{ERROR_FIELDS_EMPTY}" + ex.Message,
                    excInfo: ex.ToString()
                );
                
                ++info.Succeeded;
                return false;
            }

            try
            {
                PackageService.UpdateReportInfo(package.Id, cardInfo);
            }
            catch (Exception ex)
            {
                SPOService.log.Error(ex);
                return false;
            }
            /* ama
            var mail = gen.ComposeMessage(cardInfo);
            mail.PackageID = package.Id;

            int send_attempts = 0;
            do
            {
                MailService.MailServiceSoapClient cl = null;
                try
                {
                    cl = new MailService.MailServiceSoapClient();
                    cl.EnqueueForSend(mail);
                    SPOService.log.Debug("Mail enquing for send in MailService :: {0}; attempts {1}", mail.PackageID, send_attempts + 1);
                    break;
                }
                catch (TimeoutException tex)
                {
                    SPOService.log.Error("Stop Sending, because timeoute exception: " + tex.ToString());
                    break;
                }
                catch (Exception ex)
                {
                    SPOService.log.Error(ex);
                }
                finally
                {
                    if (cl != null)
                        ((IDisposable)cl).Dispose();
                    send_attempts++;
                }
            } while (send_attempts < 10);
            */
            try
            {
                UpdateStatusPackage.Build
                    (
                    package.Id,
                    PrimaryStatus.SED_Awaiting
                );
                
                ++info.Succeeded;
            }
            catch (Exception exc)
            {
                SPOService.log.DebugException(
                    "Exception in [processPackage] while updating status",
                    exc
                );
                ++info.Failed;
                return false;
            }

            SEDCardInfoException sedEx = null;
            try
            {
                SEDCardInfoValidator validator = new SEDCardInfoValidator(cardInfo);
                validator.PackageID = package.Id;
                validator.Validate();
            }
            catch(SEDCardInfoException ex)
            {
                sedEx = ex;
                SPOService.log.Info("Error validate package # " + package.Id.ToString());
            }


            DateTime dtNow = DateTime.Now;
            string subject = String.Format("{0}/{1}", package.Id, dtNow.ToShortDateString());
            byte[] xmlData = null;
            Guid guid = Guid.NewGuid();
            if (sedEx == null)
            {
                xmlData = Encoding.UTF8.GetBytes(string.Format(
                                    @"<?xml version=""1.0"" encoding=""utf-8""?>
                                        <wf:RegDocPass xmlns:wf=""http://docflow.services.fsfr.it.ru/"">       
                                            <num>{3}</num>
                                            <date>{0}</date>
                                            <time>{2}</time>    
                                            <docSystemId>{1}</docSystemId>
                                        </wf:RegDocPass>
                                        ", DateTime.Now.ToShortDateString(), guid,
                                         DateTime.Now.ToShortTimeString(), generatorSedNumber.Generate(package.Id), package.Id));
            }
            else
            {
                xmlData = Encoding.UTF8.GetBytes(string.Format(
                                    @"<?xml version=""1.0"" encoding=""utf-8""?>
                                        <wf:RegDocPass xmlns:wf=""http://docflow.services.fsfr.it.ru/"">       
                                            <faultcode>{0}</faultcode>
                                            <faultstring>{1}</faultstring>
                                        </wf:RegDocPass>
                                        ", sedEx.Code, sedEx.Message));
            }
            
            spoService.ProcessSedResponse(new SedResponse() { Subject = subject, XmlData = xmlData });
            return true;
        }

        public override bool processSedResponse(SedResponse response, ServiceCoreInfo info)
        {
            SPOService.log.Debug(
                "Started [ProcessSedResponse] with:{0}{1}",
                Environment.NewLine,
                response
            );

            Match m = SedResponseSubject.Match(response.Subject);
            if (m != null)
            {
                try
                {
                    int packageID = int.Parse(m.Groups["ID"].Value);

                    XmlHelper xml = new XmlHelper(response.XmlData, false);

                    string faultCode = xml.GetValue("*/faultcode");
                    string faultStr = xml.GetValue("*/faultstring");

                    if (faultCode != null || faultStr != null)
                    {
                        SecondaryStatus sedError = GetSedError(faultCode, faultStr);
                        SPOService.log.Debug(
                            "SED returned Fault:{0}[{1}] {2}",
                            Environment.NewLine,
                            faultCode,
                            faultStr
                        );
                        //обновить статус если только пришла известная ошибка СЭД, иначе пусть висит
                        if (sedError != SecondaryStatus.ERROR_UNKNOWN)
                        {
                            UpdateStatusPackage.Build(packageID,
                                PrimaryStatus.Rejected,
                                sedError,
                                string.Format("{{{0}}} {1}", sedError, faultStr)
                            );
                        }
                        return false;
                    }

                    string incomingID = xml.GetValue("*/docSystemId");
                    string sedID = xml.GetValue("*/num");
                    string regDate = xml.GetValue("*/date");
                    string regTime = xml.GetValue("*/time");
                    SPOService.log.Debug(
                        "SED returned Pass:{0}num={1}{0}docSystemId={2}{0}regDate={3}",
                        Environment.NewLine,
                        sedID,
                        incomingID,
                        regDate
                    );

                    Package pkg = PackageService.GetPackage(packageID);
                    ReportInfo rep = PackageService.GetReportInfo(pkg.ReportID);
                    rep.SedNumber = sedID;
                    rep.SedDate = DateTime.Parse(regDate + " " + regTime);
                    PackageService.UpdateReportInfo(rep);
                    UpdateStatusPackage.Build(
                            pkg.ID, PrimaryStatus.SED_Registered
                        );
                    try
                    {
                        string dir = System.IO.Path.GetDirectoryName(MailStoreHelper.GetAbsoluteFileName(rep.ProcessedDocumentUrl));
                        string sedResponseFileName = System.IO.Path.Combine(dir, "sedResponse.xml");
                        System.IO.File.WriteAllBytes(sedResponseFileName, response.XmlData);
                    }
                    catch (Exception ex)
                    {
                        SPOService.log.Error("Error saving sed response document url: {0}; error: {1}", rep.ProcessedDocumentUrl, ex);
                    }
                    
                }
                catch (Exception ex)
                {
                    SPOService.log.Error(ex);
                }
            }
            else
            {
                SPOService.log.Debug(
                    "Failed matching Subject: {0}",
                    response.Subject
                );
                return false;
            }
            return true;
        }

    }
}