﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SPO
{
    public class RequiredCardInfoException : Exception
    {
        string message;
        public override string Message
        {
            get
            {
                return message;
            }
        }

        public RequiredCardInfoException(string message)
        {
            this.message = message;
        }
    }

    public class RequiredCardInfoValidator : CustomValidator
    {
        protected override List<CustomValidator.ValidationItem> Items
        {
            get 
            {
                var items = new List<ValidationItem>();
                items.Add(new ValidationItem("orgFullName", "Не задано поле \"Полное наименование организации\"", ValidateOnStringIsNullOrEmpty));
                items.Add(new ValidationItem("INN", "Не задано поле \"ИНН\"", ValidateOnStringIsNullOrEmpty));
                items.Add(new ValidationItem("OGRN", "Не задано поле \"ОГРН\"", ValidateOnStringIsNullOrEmpty));
                items.Add(new ValidationItem("docTypeCode", "Не определен тип отчета", ValidateOnZero));
                items.Add(new ValidationItem("docTypeName", "Не определено имя типа отчета", ValidateOnNotFound));
                items.Add(new ValidationItem("orgTypeCode", "Не определен тип организации", ValidateOnZero));
                items.Add(new ValidationItem("orgTypeName", "Не определено имя типа организации", ValidateOnNotFound));
                items.Add(new ValidationItem("Subject", "Не определено краткое описание", ValidateOnStringIsNullOrEmpty));
                items.Add(new ValidationItem("RegNumber", "Не определен номер исходящего документа", ValidateOnStringIsNullOrEmpty));
                items.Add(new ValidationItem("RegDate", "Не определена дата исходящего документа", ValidateOnStringIsNullOrEmpty));
                items.Add(new ValidationItem("SignerLastName", "Не определена фамилия подписавшего лица", ValidateOnStringIsNullOrEmpty));
                items.Add(new ValidationItem("SignerFirstName", "Не определено имя подписавшего лица", ValidateOnStringIsNullOrEmpty));
                items.Add(new ValidationItem("SignerMiddleName", "Не определено отчетсво подписавшего лица", ValidateOnStringIsNullOrEmpty));
                return items;
            }
        }

        public RequiredCardInfoValidator(Dictionary<string, string> cardInfo)
        {
            CardInfo = cardInfo;
        }

        protected override void Validation()
        {
            foreach (var item in Items)
            {
                if (!item.IsValid(CardInfo[item.FieldName]))
                    Errors.Add(item.ErrorMsg);
            }
            if (Errors.Count > 0)
            {
                throw new RequiredCardInfoException(Error);
            }
        }

        public override string Error
        {
            get { return String.Format("{{ERROR_FIELDS_EMPTY}} Не удалось прочитать необходимые поля: {0}", String.Join(";", Errors)); }
        }
    }
}