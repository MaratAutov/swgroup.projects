﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Net;
using System.Web.Security;
using System.Web.Script.Serialization;
using NLog;

using ServiceCore;
using Account.DAO;
using Account.Tools;
using Account.DAO.SED;
using Autofac;

namespace SPO
{
    

    /// <summary>
    /// Сервис для подготовки отчетностки к отправке в СЭД и регистрации в Реестре
    /// </summary>
    [WebService(Namespace = "http://localhost:37446/SPOService.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    //[System.Web.Script.Services.ScriptService]
    public class SPOService : System.Web.Services.WebService
    {
        #region Статические переменные
        public static Logger log;
        public readonly bool? IsRegisteredInReestr;
        public readonly bool? LinkingOnCertificate;
        public readonly bool? LinkingOnReportInfo;

        IProcessPackage processor;
        static Autofac.IContainer container { get; set; }

        /// <summary>
        /// Имя для создания экземпляра, логгера, отображения статистики
        /// </summary>
        private static string ServiceName = "СПО";
        
        /// <summary>
        /// [Type] Reviewer: bundle "[код_региона]-[цифра_цифра]-&lt;[номер_СЭД]>" ([дата_в_формате_дд.мм.гггг])
        /// </summary>
        private static Regex RoRequestSubject = new Regex(
            "(?<type>SMCL|XTDL)\\s+Reviewer:\\s+bundle\\s+\\\"[-0-9]{1,}-[0-9]{1,}-<(?<sedNum>.*)>\\\"\\s+\\((?<sedDate>[-0-3][-0-9]\\.(01|02|03|04|05|06|07|08|09|10|11|12)\\.[-0-9]{4})\\)"
        );

        /// <summary>
        /// Ядро сервиса асинхронно исполняющее времязатратные задачи
        /// </summary>
        private static ServiceCore<SPOTask> core;
        #endregion


        #region Инициализация сервиса

        /// <summary>
        /// Конфигурирование IoC если нужно подменить ответственные за обработку классы менять тут
        /// при отказе от СЭД вместо ProcessPackage2Lotus зарегистрировать ProcessPackageSelf
        /// </summary>
        void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ProcessPackage2Lotus>().As<IProcessPackage>();
            builder.RegisterType<PostgresFunctionGenerator>().As<IGeneratorIncomingNumber>();
            // ama
            //builder.RegisterType<ProcessPackageSelf>().As<IProcessPackage>();
            //builder.RegisterType<PostgresFunctionGenerator>().As<IGeneratorIncomingNumber>();
            container = builder.Build();
        }
        
        /// <summary>
        /// Конструктов вызывается каждый раз при вызове веб-метода,
        /// Иициализирует внутренние сервисы только при первом обращении.
        /// </summary>
        /// 
        public SPOService()
        {
            Configure();
            SEDMappingService.GetConfigurations();

            if (log == null)
            {
                var logFile = ConfigurationManager.AppSettings["LogFile"];
                log = NLogger.GetLog(ServiceName, logFile);
                log.Debug("SED Core instance initialization");
            }
            if (IsRegisteredInReestr == null)
                IsRegisteredInReestr = bool.Parse(ConfigurationManager.AppSettings["RegisterInReestr"]);
            if (LinkingOnCertificate == null)
                LinkingOnCertificate = bool.Parse(ConfigurationManager.AppSettings["LinkingOnCertificate"]);
            if (LinkingOnReportInfo == null)
                LinkingOnReportInfo = bool.Parse(ConfigurationManager.AppSettings["LinkingOnReportInfo"]);
#warning При переключении СЭД  не забыть поменять MailService
            //processor = new ProcessPackage2Lotus(this);
            processor = container.Resolve<IProcessPackage>(new NamedParameter("service", this));
            core = ServiceCore<SPOTask>.GetInstance(ServiceName, log);
            core.InitEvents( processTask );

            log.Debug("SPOService initialized");
        }
        #endregion
        
        #region Вспомогательные веб-методы
        /// <summary>
        /// Перезагружает данные из базы данных (OrgMap, DocMap, SedMapping)
        /// для генерации карточки СЭД по файлу отчетности
        /// </summary>
        [WebMethod]
        public void UpdateSEDMappings()
        {
            log.Debug("Updating LetterGenerator configuration");
            LetterGenerator.LoadConfigurations(true);
        }

        /// <summary>
        /// Выключение сервиса и очистка очереди задач
        /// </summary>
        [WebMethod]
        public void Shutdown()
        {
            log.Debug("Started [Shutdown]");
            core.Shutdown(true);
        }

        /// <summary>
        /// Получение статуса сервиса (для мониторинга)
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public ServiceCoreInfo Status()
        {
            log.Debug("Started [Status]");
            return core.GetInfo();
        }
        #endregion

        #region Веб-методы СПО
        [WebMethod]
        public string UploadPackage(string Name,
                                    byte[] Data,
                                    int OwnerID,
                                    DateTime UploadTime,
                                    bool IsRO,
                                    string SubjectRO=null)
        {
            string sedNum = null;
            DateTime? sedDate = null;
            if (IsRO) {
                Match m = RoRequestSubject.Match(SubjectRO);
                sedNum = m.Groups["sedNum"].Value;
                sedDate = Convert.ToDateTime(m.Groups["sedDate"].Value);
            }

            //var dbPackage = PackageService.GetOrCreate(
            //    Name,
            //    Data,
            //    OwnerID,
            //    UploadTime,
            //    IsRO,
            //    sedNum,
            //    sedDate
            //);

            var dbPackage = UpdateStatusPackage.Create(
                Name,
                Data,
                OwnerID,
                UploadTime,
                IsRO,
                sedNum,
                sedDate
            );
            
            if (!dbPackage.Existing)
            {
                ValidatePackage(new UploadedPackage()
                {
                    Package = dbPackage,
                    Name = Name,
                    Data = Data,
                    IsRO = IsRO,
                    OwnerID = OwnerID
                });
            }
            return dbPackage.ID.ToString();
        }

        [WebMethod]
        public string UploadDocument(string Name,
                                    byte[] Data,
                                    int OwnerID,
                                    DateTime UploadTime,            
            int receiver
            )
        {
            
            var dbDoc = DocumentService.GetOrCreate(
                Name,
                Data,
                OwnerID,
                UploadTime,
                receiver
            );

            return dbDoc.ID.ToString();
        }

        [WebMethod]
        public void ValidatePackage(UploadedPackage package)
        {
            core.Enqueue(new SPOTask() {
                Mode = TaskMode.VALIDATE_PACKAGE,
                Object = package
            });
        }

        /// <summary>
        /// Добавить в очередь СПО обработку пакета для отправки запроса в СЭД
        /// </summary>
        /// <param name="package"></param>
        [WebMethod]
        public void ProcessPackage(InteropPackage package)
        {
            log.Debug("Starting process package id = {0}", package.Id);
            core.Enqueue(new SPOTask() {
                Mode = TaskMode.PROCESS_PACKAGE,
                Object = package                    
            });
        }

        [WebMethod]
        public void ProcessSedResponse(SedResponse response)
        {
            core.Enqueue( new SPOTask() {
                Mode = TaskMode.PROCESS_SED_RESPONSE,
                Object = response
            });
        }

        /// <summary>
        /// Добавить в очередь СПО получение краткого наименования 
        /// Регионального Отделения для заданного пакета
        /// </summary>
        /// <param name="packInfo"></param>
        [WebMethod]
        public void AcquireRoName(PackageInfo packInfo)
        {
            core.Enqueue(new SPOTask()
            {
                Mode = TaskMode.GET_RO_NAME,
                Object = packInfo
            });
            
        }

        /// <summary>
        /// Добавить в очередь СПО регистрацию пакета в Реестре
        /// </summary>
        /// <param name="packInfo"></param>
        [WebMethod]
        public void RegisterInReestr(PackageInfo packInfo)
        {
            core.Enqueue(new SPOTask()
            {
                Mode = TaskMode.REGISTER_IN_REESTR,
                Object = packInfo
            });
        }

        /// <summary>
        /// Добавить в очередь СПО получение краткого наименования 
        /// Регионального Отделения для заданного пакета
        /// </summary>
        /// <param name="packInfo"></param>
        [WebMethod]
        public void ReAcquireRoName(int id)
        {
            core.Enqueue(new SPOTask()
            {
                Mode = TaskMode.GET_RO_NAME,
                Object = GetReportInfo(id)
            });

        }

        /// <summary>
        /// Добавить в очередь СПО регистрацию пакета в Реестре
        /// </summary>
        /// <param name="packInfo"></param>
        [WebMethod]
        public void ReRegisterInReestr(int id)
        {
            core.Enqueue(new SPOTask()
            {
                Mode = TaskMode.REGISTER_IN_REESTR,
                Object = GetReportInfo(id)
            });
        }
        #endregion

        #region Обработчики задач ядра СПО
        
        PackageInfo GetReportInfo(int id)
        {
            Account.DAO.Package pkg = PackageService.GetPackage(id);
            Account.DAO.ReportInfo rep = PackageService.GetReportInfo(pkg.ReportID);

            return new PackageInfo()
            {
                Package = pkg,
                Report = rep
            };
        }
        
        /// <summary>
        /// Ядро сервиса обработки СПО
        /// </summary>
        /// <param name="task"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        private bool processTask(SPOTask task, ServiceCoreInfo info)
        {
            info.Processed++;
            switch (task.Mode)
            {
                case TaskMode.VALIDATE_PACKAGE:
                    return validatePackage((UploadedPackage)task.Object, info);
                case TaskMode.PROCESS_PACKAGE:
                    return processPackage((InteropPackage)task.Object, info);
                case TaskMode.PROCESS_SED_RESPONSE:
                    return processSedResponse((SedResponse)task.Object, info);
                case TaskMode.GET_RO_NAME:
                    return acquireRoName((PackageInfo)task.Object, info);
                case TaskMode.REGISTER_IN_REESTR:
                    return registerInReestr((PackageInfo)task.Object, info);
                default:
                    return false;
            }
        }
    
        private bool validatePackage(UploadedPackage package, ServiceCoreInfo info)
        {
            using (var stream = new MemoryStream(package.Data))
            {
                Package dbPackage = package.Package;
                
                try {
                    
                    var packman = new PackageManager();
                    if (!packman.Open(package.Name, package.Data)) {
                        UpdateStatusPackage.Build(
                            dbPackage.ID,
                            PrimaryStatus.PACKAGE_ERROR,
                            packman.ErrorStatus,
                            string.Format("{{{0}}} {1}", packman.ErrorStatus, packman.ErrorMessage),
                            packman.ErrorException
                        );
                        return false;
                    }

                    string docStoreUrl = packman.Store(dbPackage.ID.ToString());
                    
                    //*
                    bool SignatureVerified = true;
                    var vr = packman.VerifySignature(package.OwnerID);
                    //vr.Status = Progresoft.Accounts.ElectroSignature.SignatureStatus.Valid;
                    if (vr.Status != Progresoft.Accounts.ElectroSignature.SignatureStatus.Valid) {
                        UpdateStatusPackage.Build(
                            dbPackage.ID,
                            PrimaryStatus.SIGNATURE_ERROR,
                            SecondaryStatus.UNSET,
                            packman.ErrorMessage,
                            packman.ErrorException,
                            IsUpdateInitialStatus: true
                        );
                        dbPackage = PackageService.GetPackage(dbPackage.ID);
                        SignatureVerified = false;
                    }
                    if (SignatureVerified)
                    {
                        UpdateStatusPackage.Build(
                            dbPackage.ID,
                            PrimaryStatus.Verified,
                            IsUpdateInitialStatus: true
                        );
                        dbPackage = PackageService.GetPackage(dbPackage.ID);
                    }
                    //Попробовать привязать к пользователю если его нет, по данным из сертификата
                    if (LinkingOnCertificate == true)
                    {
                        if (package.OwnerID <= 0)
                        {
                            UserAccount user = UserService.GetByInnOgrn(vr.INN, vr.OGRN);
                            if (user != null)
                            {
                                package.OwnerID = user.ID;
                                package.Package.OwnerID = user.ID;
                                PackageService.Update(package.Package);
                                SPOService.log.Debug("Package ID = {0}; Asset User ID = {1};", package.Package.ID, package.OwnerID);
                            }
                        }
                    }

                    //*/
                    dbPackage.SignatureType = packman.Signature;
                    PackageService.Update(dbPackage);

                    if (!packman.VerifyXtdd()) {
                        if (SignatureVerified)
                        {
                            UpdateStatusPackage.Build(
                                dbPackage.ID,
                                PrimaryStatus.REPORT_ERROR,
                                SecondaryStatus.ERROR_XSD_VALIDATION,
                                String.Format("{{{0}}} {1}", SecondaryStatus.ERROR_XSD_VALIDATION, packman.ErrorMessage),
                                packman.ErrorException
                            );
                        }
                        return false;
                    }

                    if (package.OwnerID <= 0 && LinkingOnReportInfo == true)
                    {
                        var p = new InteropPackage()
                        {
                            Id = dbPackage.ID,
                            XmlData = packman.ReportPackage.ReportBody,
                            ExternalId = packman.ExternalId,
                            Model = packman.Model,
                            Type = packman.Type,
                            Version = packman.Version
                        };
                        LetterGenerator gen = new LetterGenerator(p);
                        try
                        {
                            var cardInfo = gen.CreateInfo();
                            LinkOwnerOnReportData(p,
                                cardInfo);
                        }
                        catch (Exception ex)
                        {
                            log.DebugException(String.Format("Ошибка связывания пользователя по данным из отчета :: {0}", dbPackage.ID), ex);
                        }
                    }

                    if (!SignatureVerified)
                        return false;

                    //*
                    if (!packman.VerifyFIOSignature())
                    {
                        UpdateStatusPackage.Build(
                            dbPackage.ID,
                            PrimaryStatus.SIGNATURE_INVALID,
                            SecondaryStatus.UNSET,
                            packman.ErrorMessage,
                            packman.ErrorException,
                            IsUpdateInitialStatus: true
                        );
                        return false;
                    }

                    var dbInfo = dbPackage.Info;
                    dbInfo.ProcessedDocumentUrl = docStoreUrl;
                    dbInfo.Version = packman.Version;
                    dbInfo.Model = packman.Model;
                    dbInfo.Type = packman.Type;
                    dbInfo.ExternalID = packman.ExternalId;

                    PackageService.UpdateReportInfo(dbInfo);
                                        
                    if (!dbInfo.IsRoReport) {
                        ProcessPackage(new InteropPackage() {
                            Id = dbPackage.ID,
                            XmlData = packman.ReportPackage.ReportBody,
                            ExternalId = packman.ExternalId,
                            Model = packman.Model,
                            Type = packman.Type,
                            Version = packman.Version
                        });
                    } else {
                        UpdateStatusPackage.Build(
                            dbPackage.ID, PrimaryStatus.RO_Awaiting
                        );

                        AcquireRoName(new PackageInfo()
                        {
                            Package = dbPackage,
                            Report = dbPackage.Info
                        });
                    }

                    return true;
                } catch (Exception exc) {
                    log.ErrorException(
                        "Ошибка при валидации пакета",
                        exc
                    );
                    // При возникновении не декларированных ошибок не менять статус пакета, отсавлять только логи, нужно бы еще както информировать поддержку
                    
                }
                return false;
            }
        }
        
        //* Привязка к пользователю по данным из отчета
        public void LinkOwnerOnReportData(InteropPackage package, Dictionary<string, string> cardInfo)
        {
            try
            {
                var dbpackage = PackageService.GetPackage(package.Id);
                
                string inn = null;
                if (cardInfo.ContainsKey("INN"))
                    inn = cardInfo["INN"];
                string ogrn = null;
                if (cardInfo.ContainsKey("OGRN"))
                    ogrn = cardInfo["OGRN"];
                if (!String.IsNullOrEmpty(inn) && !String.IsNullOrEmpty(ogrn))
                {
                    var user = UserService.GetByInnOgrn(inn, ogrn);
                    if (user != null)
                    {
                        dbpackage.OwnerID = user.ID;
                        PackageService.Update(dbpackage);
                        SPOService.log.Debug("Package ID = {0}; Asset User ID = {1};", dbpackage.ID, dbpackage.OwnerID);
                    }
                }
            }
            catch (Exception ex)
            {
                SPOService.log.Error(ex);
            }
        }

        /// <summary>
        /// Обработка пакета для отправки в СЭД:
        /// Разбирает тело отчета и генерирует письмо для регистрации в СЭД
        /// Тема письма: [ID пакета]:[Входящий Номер]
        /// Приложения:
        /// - Файл отчетности (zip)
        /// - Карточка (xml)
        /// </summary>
        /// <param name="package">Пакет отправленный из ЛК на обработку в СЭД</param>
        /// <param name="info">Статистика сервиса</param>
        /// <returns>true в случае успеха, false если не удалось обновить статус</returns>
        private bool processPackage(InteropPackage package, ServiceCoreInfo info)
        {
            processor.processPackage(package, info);
            return true;
        }

        /// <summary>
        /// Обработка ответа из SED
        /// </summary>
        /// <param name="response"></param>
        public bool processSedResponse(SedResponse response, ServiceCoreInfo info)
        {
            processor.processSedResponse(response, info);
            return true;
        }

        /// <summary>
        /// Получение наименования Регионального Отделения по ИНН и ОГРН
        /// </summary>
        /// <param name="packInfo"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        private bool acquireRoName(PackageInfo packInfo, ServiceCoreInfo info)
        {
            //для АЛ не надо получать регион а сразу регистрировать 
            if (packInfo.Report.Model == "АЛ")
            {
                RegisterInReestr(packInfo);
                return true;
            }
            
            var report = packInfo.Report;
            var package = packInfo.Package;
            log.Debug("Get region name for package PackageId={0}; ReportId={1};", packInfo.Package.ID, packInfo.Report.ID);
            try
            {
                using (var cl = new PUService.PUService())
                {
                    try
                    {
                        log.Debug("Getting region name for package id: {0}; Model: {1}; INN: {2}; OGRN: {3};", package.ID, report.Model, report.INN, report.OGRN);
                        report.RoName = cl.getRO(report.INN, report.OGRN);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error getting region name package id: {0}; Model: {1}; INN: {2}; OGRN: {3}; Exception: {4}", package.ID, report.Model, report.INN, report.OGRN, ex);
                    }
                    if (!string.IsNullOrEmpty(report.RoName))
                    {
                        log.Debug("Region name success getting is equal '{2}' for package PackageId={0}; ReportId={1};", packInfo.Package.ID, packInfo.Report.ID, report.RoName);
                        PackageService.UpdateReportInfo(report);
                        UpdateStatusPackage.Build(
                            package.ID,
                            PrimaryStatus.RO_Recieved
                        );
                        RegisterInReestr(packInfo);
                    }
                    else
                    {
                        log.Error("Invalid Region name  for package PackageId={0}; ReportId={1};", packInfo.Package.ID, packInfo.Report.ID, report.RoName);
                        // Не меняем статус при ошибке только логи
                    }
                }
            }
            catch (Exception exc)
            {
                log.Error("Raised error while getting region name: {0}", exc);
                // Не меняем статус при ошибке только логи
            }
            return true;
        }

        /// <summary>
        /// Регистрация пакета в Реестре
        /// Передает пакет в сервисы Реестра (работающие в Lotus Notes)
        /// Для ПУРЦБ: PUService
        /// Для АЛ: ALService
        /// Для ЛКИ, УММ: URService
        /// </summary>
        /// <param name="packInfo"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        private bool registerInReestr(PackageInfo packInfo, ServiceCoreInfo info)
        {
            log.Debug("Start registering in reestr package PackageId={0}; ReportId={1};", packInfo.Package.ID, packInfo.Report.ID);
            var package = packInfo.Package;
            var report  = packInfo.Report;

            var blob = PackageService.GetBlob(package.BlobID);

            UpdateStatusPackage.Build(
                package.ID,
                PrimaryStatus.REESTR_Awaiting
            );

            IReportPackage rPack = ReportPackageManager.Load(
                package.Name + ".zip",
                blob.Body
            );
            string xmlReportZipped = rPack.GetGzippedBase64Body();
            string notification = string.Format(
                @"<?xml version=""1.0"" encoding=""UTF-8"" standalone=""yes""?>
                <ns2:Уведомление xmlns:ns2=""http://www.it.ru/Schemas/Avior/{0}"">
                    <КраткоеНаименованиеРегиональногоОтделенияФсфрРоссии>{1}</КраткоеНаименованиеРегиональногоОтделенияФсфрРоссии>
                    <ВходящийНомерПрисвоенныйПакету>{2}</ВходящийНомерПрисвоенныйПакету>
                    <ДатаПрисвоенияВходящегоНомера>{3}</ДатаПрисвоенияВходящегоНомера>
                </ns2:Уведомление>",
                report.Model,
                report.RoName,
                report.SedNumber,
                report.SedDate.Value.ToString("yyyy-MM-dd")
            );
            string xmlNotification = Convert.ToBase64String(
                Encoding.UTF8.GetBytes(notification)
            );

            string result = null;
            switch (report.Model)
            {
                case "ПУРЦБ":
                    using (var cl = new PUService.PUService())
                    {
                        try
                        {
                            result = cl.exportReport(
                                xmlReportZipped,
                                xmlNotification,
                                package.UploadTime.ToString("yyyy-MM-dd"),
                                "true"
                            );
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error registerin into reestr package id: {0}; Model: {1}; Exception: {2}", package.ID, report.Model, ex);
                        }
                    }
                    break;
                case "АЛ":
                    using (var cl = new ALService.ALService())
                    {
                        try
                        {
                            result = cl.exportAL(
                                xmlReportZipped,
                                report.SedDate.Value.ToString("yyyy-MM-dd"),
                                report.SedNumber,
                                "true"
                            );
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error registerin into reestr package id: {0}; Model: {1}; Exception: {2}", package.ID, report.Model, ex);
                        }
                    }
                    break;
                case "ЛКИ":
                case "УММ":
                    using (var cl = new UniversalService.URService())
                    {
                        try
                        {
                            result = cl.exportReport(
                                xmlReportZipped,
                                xmlNotification,
                                package.UploadTime.ToString("yyyy-MM-dd"),
                                "true",
                                report.OrgTypeCode.ToString(),
                                package.ID.ToString()
                            );
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error registerin into reestr package id: {0}; Model: {1}; Exception: {2}", package.ID, report.Model, ex);
                        }
                    }
                    break;
                default:
                    result = "success";
                    break;
            }

            if (result == "success")
            {
                log.Debug("Package PackageId={0}; ReportId={1}; was successfully registered in reestr", packInfo.Package.ID, packInfo.Report.ID);
                UpdateStatusPackage.Build(
                    package.ID,
                    PrimaryStatus.REESTR_Registered
                );
            }
            else
            {
                log.Debug("Reestr returned error PackageId={0}; ReportId={1}; Return value={2}", packInfo.Package.ID, packInfo.Report.ID, result);
                log.Debug(notification);
                // Не меняем статус при ошибке только логи
                return false;
            }
            return true;
        }
        #endregion
    }
}