﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Account.DAO;

namespace SPO
{
    public sealed class UploadedPackage
    {
        public Package Package  { get; set; }
        public byte[]  Data     { get; set; }
        public string  Name     { get; set; }
        public int     OwnerID  { get; set; }
        public bool    IsRO     { get; set; }
    }

    public sealed class InteropPackage
    {
        public int    Id         { get; set; }
        public byte[] XmlData    { get; set; }
        public string ExternalId { get; set; }
        public string Model      { get; set; }
        public string Type       { get; set; }
        public string Version    { get; set; }

        public override string ToString()
        {
            return String.Format("Id: {1}{0}ExternalId: {2}{0}Model: {3}{0}Type: {4}{0}Version{5}{0}", Environment.NewLine, Id, ExternalId, Model, Type, Version);
        }
    }

    public sealed class SedResponse
    {
        public string Subject { get; set; }
        public byte[] XmlData { get; set; }

        public override string ToString()
        {
            return String.Format("Subject: {0}"+Environment.NewLine+"Data:{1}", Subject, Encoding.UTF8.GetString(XmlData));
        }
    }

    public sealed class SPOTask
    {
        public object Object { get; set; }
        public TaskMode Mode { get; set; }

        public override string ToString()
        {
            return String.Format("SPOTask{0}Mode: {1}{0}Object: {2}", Environment.NewLine, Mode, Object);
        }
    }

    public sealed class PackageInfo
    {
        public Package    Package { get; set; }
        public ReportInfo Report  { get; set; }
    }

    public enum TaskMode
    {
        VALIDATE_PACKAGE = 1,
        PROCESS_PACKAGE,
        PROCESS_SED_RESPONSE,
        GET_RO_NAME,
        REGISTER_IN_REESTR
    }
}
