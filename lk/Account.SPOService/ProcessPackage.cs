﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceCore;
using System.Text.RegularExpressions;
using Account.Tools;
using Account.DAO;
using System.Net;
using System.IO;

namespace SPO
{
    public class ProcessPackage : IProcessPackage
    {
        public SPOService spoService { get; set; }

        public bool processPackage(InteropPackage package, ServiceCoreInfo info)
        {
            SPOService.log.Debug(
                "Starting [processPackage] with package:{0}{1}",
                Environment.NewLine,
                package
            );

            ++info.Processed;
            LetterGenerator gen = new LetterGenerator(package);
            var cardInfo = gen.CreateInfo();

            try
            {
                PackageService.UpdateReportInfo(package.Id, cardInfo);
            }
            catch (Exception ex)
            {
                SPOService.log.Error(ex);
                return false;
            }

            int send_attempts = 0;
            SEDService.documentRegistrationInfo regInfo = null;
            do
            {
                try
                {
                    
                    string response = HttpUtils.HttpUploadFile(System.Configuration.ConfigurationManager.AppSettings["UploadUrl"],
                                             package.XmlData, "param", "multipart/form-data;", new System.Collections.Specialized.NameValueCollection());
                    if (response != null && response.StartsWith("SUCCESS"))
                    {
                        // откинем слово SUCCESS в ответе
                        string fileId = response.Substring(8);
                        SEDService.DocumentRegistrationClient drc = new SEDService.DocumentRegistrationClient();
                        SEDService.documentInfo doc = new SEDService.documentInfo();
                        doc.correspondent = cardInfo["orgFullName"];
                        doc.correspondentInn = cardInfo["INN"];
                        doc.correspondentType = cardInfo["orgTypeCode"];
                        DateTime dt;
                        if (DateTime.TryParse(cardInfo["RegDate"], out dt))
                            doc.documentDate = dt;
                        doc.documentNumber = cardInfo["RegNumber"];
                        doc.documentType = cardInfo["docTypeCode"];
                        doc.header = cardInfo["docTypeName"];
                        doc.fileId = fileId;
                        doc.signerFirstName = cardInfo["SignerFirstName"];
                        doc.signerLastName = cardInfo["SignerLastName"];
                        doc.signerMiddleName = cardInfo["SignerMiddleName"];
                        
                        regInfo = drc.registerDocument(doc);
                        break;
                    }
                    
                }
                catch (Exception ex)
                {
                    SPOService.log.Error(ex);
                }
                finally
                {
                    send_attempts++;
                }
            } while (send_attempts < 10);
            //не получилось зааплодить выходим
            if (send_attempts >= 10)
            {
                SPOService.log.Error(
                    "Can't upload to the server");
                return false;
            }
            try
            {

                Package pkg = PackageService.GetPackage(package.Id);
                ReportInfo rep = PackageService.GetReportInfo(pkg.ReportID);
                rep.SedNumber = regInfo.registrationNumber;
                rep.SedDate = regInfo.registrationDate;
                PackageService.UpdateReportInfo(rep);

                //StatusService.UpdateStatusForPackage(
                //        pkg.ID, PrimaryStatus.SED_Registered
                //    );
                UpdateStatusPackage.Build(
                        pkg.ID, PrimaryStatus.SED_Registered
                    );
            }
            catch (Exception exc)
            {
                SPOService.log.DebugException(
                    "Exception in [processPackage] while updating status",
                    exc
                );
                ++info.Failed;
                return false;
            }

            /*
            using (WebClient client = new WebClient())
            {
                client.UploadData(
                    "NewSED_POST_Url",
                    "POST",
                    package.XmlData
                );
            }

            using (var newSED = new NewSEDReference.DocumentRegistrationImplService())
            {
                #warning fileID for NewSED not acquired (POST report)
                var res = newSED.registerDocument(new NewSEDReference.documentInfo() {
                    correspondent = cardInfo["orgFullName"],
                    correspondentType = cardInfo["orgTypeName"],
                    documentType = cardInfo["docTypeName"],
                    documentNumber = cardInfo["RegNumber"],
                    documentDate = Convert.ToDateTime(cardInfo["RegDate"]),
                    fileId = null,
                    header = cardInfo["Subject"],
                    signer = string.Join(" ", new string[] { 
                        cardInfo["SignerLastName"],
                        cardInfo["SignerFirstName"],
                        cardInfo["SignerMiddleName"]
                    })
                });

                Package pkg = PackageService.GetPackage(package.Id);
                ReportInfo rep = PackageService.GetReportInfo(pkg.ReportID);
                rep.SedNumber = res.registrationNumber;
                rep.SedDate = res.registrationDate;
                PackageService.UpdateReportInfo(rep);
            }
             * */
            return true;
        }

        public bool processSedResponse(SedResponse response, ServiceCoreInfo info)
        {
            SPOService.log.Debug(
                "Started [ProcessSedResponse] with:{0}{1}",
                Environment.NewLine,
                response
            );

            
            return true;
        }
    }
}