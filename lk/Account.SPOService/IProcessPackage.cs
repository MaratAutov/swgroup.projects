﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceCore;

namespace SPO
{
    public interface IProcessPackage
    {
        SPOService spoService { get; set; }
        bool processPackage(InteropPackage package, ServiceCoreInfo info);
        bool processSedResponse(SedResponse response, ServiceCoreInfo info);
    }
}
