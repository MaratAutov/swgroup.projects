﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Progresoft.Accounts.ElectroSignature;
using NLog;

namespace Progresoft.Accounts.ElectroSignature.Sharpei
{
	/// <summary>
	/// Реализация проверялки для КриптоПро
	/// </summary>
	class CryptoProSignatureVerificationModule : IVerificationModule
	{
		private static Logger _logger = LogManager.GetLogger("CryptoProSignatureVerificationModule");

        string innOidMatch = "OID.1.2.643.3.131.1.1=";
        string ogrnOidMatch = "OID.1.2.643.100.1=";

		public VerificationResult VerifySignature(byte[] body, byte[] signature)
		{
			if (body == null)
				throw new ArgumentNullException("body");
			if (signature == null) 
				throw new ArgumentNullException("signature");
			{
				_logger.Debug("Проверка подписи файла длиной {0} bytes", body.Length);

                var vr = new VerificationResult
                {
                    // типа условились, что заглушка, для тестирования функциональности, возвращаем нулы в инн огрн, как будто серта не было
                    PackageBody = body,
                    Status = SignatureStatus.Valid,
                    SingatureType = CSPTag
                };

				
				try
				{
					// объект, в котором будут происходить декодирование и проверка.
					SignedCms signedCms = new SignedCms(new ContentInfo(body), true);

					// декодируем подпись.
					signedCms.Decode(signature);

					_logger.Debug("Подписей: " + signedCms.SignerInfos.Count);
					//  кол-во подписей.
					if (
						signedCms.SignerInfos.Count == 0)
					{
						// не подписан
						_logger.Info("Файл не подписан");
                        vr.Status = SignatureStatus.SignatureError;
                        vr.ErrorMessage = "Файл отчета не подписан";
					}
					else
					{
                        Oid digestAlgo = null;

                        //Получить ИНН и ОГРН из подписи
                        if (signedCms.SignerInfos.Count == 1)
                        {
                            X509Certificate2 cert = signedCms.SignerInfos[0].Certificate;
                            if (!string.IsNullOrEmpty(cert.Subject))
                            {
                                string[] oids = cert.Subject.Split(',');
                                foreach (string oid in oids)
                                {
                                    string str = oid.Trim();
                                    if (str.StartsWith(innOidMatch) && str.Length > innOidMatch.Length)
                                    {
                                        string inn = str.Remove(0, innOidMatch.Length);
                                        if (inn.StartsWith("00"))
                                            inn = inn.Remove(0, 2);
                                        vr.INN = inn;
                                    }
                                    else if (str.StartsWith(ogrnOidMatch) && str.Length > ogrnOidMatch.Length)
                                    {
                                        string ogrn = str.Remove(0, ogrnOidMatch.Length);
                                        vr.OGRN = ogrn;
                                    }
                                }
                            }
                        }

						foreach (SignerInfo current in signedCms.SignerInfos)
						{
							if (current.Certificate != null)
							{
                                _logger.Debug("Проверка подписи для подписавшего '{0}'...",
											  current.Certificate.SubjectName.Name);

                                if (_logger.IsDebugEnabled)
                                {
                                    try
                                    {
                                        _logger.Debug("Содержимое сертификата: \r\n" + current.Certificate.ToString(false));
                                    }
                                    catch (Exception e)
                                    {
                                        _logger.Debug("Не удалось вывести содержимое сертификата", e);
                                    }
                                }
							    // Используем проверку подписи и стандартную 
								// процедуру проверки сертификата: построение цепочки, 
								// проверку цепочки, и необходимых расширений для данного 
								// сертификата.

                                try // ama
                                {
                                    current.CheckSignature(false);
                                }
                                catch // ama
                                {
                                    _logger.Info("Verify broken.");
                                }
								_logger.Info("Успешно.");

                                if (digestAlgo == null)
                                {
                                    digestAlgo = current.DigestAlgorithm;
                                }

							}
							else
							{
								_logger.Warn("Подпись не содержит сертификата");
                                vr.Status = SignatureStatus.SignatureError;
                                vr.ErrorMessage = "Подпись не содержит сертификата";
							}

							// При наличии соподписей проверяем соподписи.
							//if (current.CounterSignerInfos.Count > 0)
							//{
							//    Console.WriteLine("\tКоличество соподписей:{0}",
							//                      current.CounterSignerInfos.Count);
							//    SignerInfoEnumerator coenumerator =
							//        current.CounterSignerInfos.GetEnumerator();
							//    while (coenumerator.MoveNext())
							//    {
							//        SignerInfo cosigner = coenumerator.Current;
							//        Console.Write("\tПроверка соподписи для соподписавшего '{0}'...",
							//                      cosigner.Certificate.SubjectName.Name);
							//        try
							//        {
							//            // Используем проверку подписи и стандартную 
							//            // процедуру проверки сертификата: построение цепочки, 
							//            // проверку цепочки, и необходимых расширений для данного 
							//            // сертификата.
							//            cosigner.CheckSignature(false);
							//            Console.WriteLine("Успешно.");
							//        }
							//        catch (System.Security.Cryptography.CryptographicException e)
							//        {
							//            Console.WriteLine("Ошибка:");
							//            Console.WriteLine("\t\t" + e.Message);
							//            valid = false;
							//        }
							//    }
							//}
						}

                        //генерируем хеш
                        _logger.Debug("Using hash algo: " + digestAlgo.FriendlyName);

                        var digestAlgImpl = HashAlgorithm.Create(digestAlgo.FriendlyName);

						if (digestAlgImpl == null)
						{
							_logger.Error("Не удалось найти алгоритм хеширования: " + digestAlgo.FriendlyName + "; используем sha1");
							digestAlgImpl = HashAlgorithm.Create("sha1");
							//throw new InvalidOperationException("Не удалось найти алгоритм хеширования: " + digestAlg);
						}

                        vr.PackageHash = digestAlgImpl.ComputeHash(body);


					}
				}
				catch (CryptographicException e)
				{
					_logger.Debug("Ошибка проверки ЭП: {0}", e);
                    vr.Status = SignatureStatus.SignatureError;
                    vr.ErrorMessage = e.Message;
				}
				
				return vr;

			}
		}

		public byte[] Sign(byte[] body, SignerDescriptor signer)
		{

			// Получаем сертификат ключа подписи;
			// он будет использоваться для получения 
			// секретного ключа подписи.
			X509Certificate2 signerCert = GetSignerCert(signer);

			// Создаем объект ContentInfo по сообщению.
			// Это необходимо для создания объекта SignedCms.
			var contentInfo = new ContentInfo(body);

			// Создаем объект SignedCms по только что созданному
			// объекту ContentInfo.
			// SubjectIdentifierType установлен по умолчанию в 
			// IssuerAndSerialNumber.
			// Свойство Detached устанавливаем явно в true, таким 
			// образом сообщение будет отделено от подписи.
			var signedCms = new SignedCms(contentInfo, true);

			// Определяем подписывающего, объектом CmsSigner.
			var cmsSigner = new CmsSigner(signerCert);

			// Подписываем CMS/PKCS #7 сообение.
			_logger.Debug("Вычисляем подпись сообщения для субъекта " +
			                   "{0} ... ", signerCert.SubjectName.Name);
            /*/ ama
			signedCms.ComputeSignature(cmsSigner);

			// Кодируем CMS/PKCS #7 подпись сообщения.
			byte[] encodedSignature = signedCms.Encode();
            */
            // ama
            byte[] encodedSignature = new byte[] { 1, 2, 3 };
			return encodedSignature;
		}

		public string CSPTag
		{
			get {
				return "cryptopro";
			}
		}

		private X509Certificate2 _signerCert = null;
		object _lock = new object();
		X509Certificate2 GetSignerCert(SignerDescriptor signer)
		{
			 if (_signerCert == null)
			 {
			 	lock( _lock)
			 	{
					if (_signerCert == null)
					{
						
						// Открываем хранилище My.
						X509Store storeMy = null;
						try
						{
							storeMy = new X509Store(StoreName.My,
							                        StoreLocation.CurrentUser);
							X509Certificate2Collection certColl = FindCertInStore(storeMy, signer);

							if (certColl.Count == 0)
							{
								storeMy.Close();
								storeMy = new X509Store(StoreName.My,
														StoreLocation.LocalMachine);
								certColl = FindCertInStore(storeMy, signer);

							}
							if (certColl.Count == 1)
							{
								_signerCert = certColl[0];
								_logger.Info("Найден сертификат для подписи");
							}
							else
							{
								string error = string.Format("Найдено {0} сертификатов для подписи ({1})", 
									certColl.Count, signer);
								_logger.Error(error);
								throw new ArgumentException(error, "signer"); 
							}

						}
						finally
						{
							if (storeMy != null) storeMy.Close();
						}
					}
			 		
			 	}
			 }

			return _signerCert;
		}

		private X509Certificate2Collection FindCertInStore(X509Store storeMy, SignerDescriptor signer)
		{
			storeMy.Open(OpenFlags.ReadOnly);

			// Ищем сертификат для подписи.
			_logger.Debug(
				@"Ищем сертификат для подписи: {0} в {1}\{2}", signer , storeMy.Location, storeMy.Name);
			return storeMy.Certificates.Find(X509FindType.FindBySubjectName,
			                                 signer.CN, false);
		}

		// Подписываем сообщение секретным ключем.
	}
}
