﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using NLog;

namespace Progresoft.Accounts.ElectroSignature.InterPro
{
    public class InterProCmdLineVerificationModule : IVerificationModule
    {
        private static Regex failureReasonRegex = new Regex(@"Reason:\s+(?<reason>.*)");

        private static Logger _logger = LogManager.GetLogger("InterProCmdLineVerificationModule");

        private const int COMMANDLINE_TIMEOUT_MS = 60000;
        private const int SUCCESS_EXIT_CODE = 0;

        private static string _caPath = ConfigurationManager.AppSettings["CAPath"];
        public static string CAPath
        {
            get { return _caPath; }
            set { _caPath = value; }
        }

        private static string  _fVerifyLocation = _caPath + "\\fverify.exe";
        public static string FVerifyLocation
        {
            get { return _fVerifyLocation; }
            set { _fVerifyLocation = value; }
        }


        public VerificationResult VerifySignature(byte[] body, byte[] signature)
        {
            SignatureStatus status;
            string errorMsg = "";
            try
            {
                _logger.Debug("Current fir: " + Directory.GetCurrentDirectory());
                string dataFile = Path.GetTempFileName();
                string signFile = Path.GetTempFileName();
                string outfile = Path.GetTempFileName();

                try
                {
                    File.WriteAllBytes(dataFile, body);
                    File.WriteAllBytes(signFile, signature);

                    string arguments = String.Format(
                        "-form \"{0}\" -data \"{1}\" -nocrl -CApath \"{2}\" -out \"{3}\"",
                        signFile,
                        dataFile,
                        CAPath,
                        outfile
                        );
                    var psi = new ProcessStartInfo
                                  {
                                      FileName = FVerifyLocation,
                                      Arguments = arguments,

                                      CreateNoWindow = true,
                                      UseShellExecute = false

                                  };

                    _logger.Debug("Starting FVerify: '{0}' '{1}'", 
                                       psi.FileName,
                                       psi.Arguments);
                    Process process = Process.Start(psi);
                    if (process.WaitForExit(COMMANDLINE_TIMEOUT_MS))
                    {

                        status = process.ExitCode == SUCCESS_EXIT_CODE ? SignatureStatus.Valid : SignatureStatus.SignatureError;
                        _logger.Debug("FVerify exit code was {0} (returning {1})", process.ExitCode,
                                           status);
                        if (status != SignatureStatus.Valid)
                        {
                            string outTxt = File.ReadAllText(outfile);
                            _logger.Info("FVerify said:\r\n"  + outTxt);
                            var m = failureReasonRegex.Match(outTxt);
                            if (m != null) {
                                errorMsg = m.Groups["reason"].Value;
                            }
                        }
                    }
                    else
                    {
                        errorMsg = "Timeout running FVerify";
                        _logger.Warn(errorMsg);
                        status = SignatureStatus.SignatureError;
                    }
                }
                finally
                {
                    File.Delete(dataFile);
                    File.Delete(signFile);
                    File.Delete(outfile);
                }
            }
            catch (Exception e)
            {
                _logger.Error("Unhandled exception", e);
                status = SignatureStatus.SignatureError;
                errorMsg = e.Message;
            }
            var vr = new VerificationResult {
				PackageBody = body,
				Status = status,
                SingatureType = CSPTag,
                ErrorMessage = errorMsg
			};
		    return vr;
        }

        public byte[] Sign(byte[] body, SignerDescriptor signer)
        {
            throw new NotSupportedException();
        }

    	public string CSPTag
    	{
			get
			{
				return "interpro";
			}
    	}
    }
}
