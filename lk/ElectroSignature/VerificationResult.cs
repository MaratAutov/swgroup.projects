﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Progresoft.Accounts.ElectroSignature
{
	/// <summary>
	/// Результат проверки ЭЦП
	/// </summary>
	public class VerificationResult
	{
		public SignatureStatus Status { get; set; }
		/// <summary>
		/// ИНН из сертификата
		/// </summary>
		public string INN { get; set; }
		/// <summary>
		/// ОГРН из сертификата
		/// </summary>
		public string OGRN { get; set; }
		/// <summary>
		/// Сведения об отношениях, при осуществлении которых
		/// электронный документ будет иметь юридическое значение 
		/// </summary>
		public string OID { get; set; }
		/// <summary>
		/// Тело пакета (извлекается из подписаного файла)
		/// </summary>
		public byte[] PackageBody { get; set; }

        /// <summary>
        /// Хеш-код, вычисленный правильным алго
        /// </summary>
        public byte[] PackageHash { get; set; }

        public string ErrorMessage = null;

        public string SingatureType { get; set; }
	}
}
