﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;

namespace Progresoft.Accounts.ElectroSignature
{
	public class SignedPackage
	{
	    /// <summary>
		/// Тело пакета
		/// </summary>
		public byte[] PackageBody
		{
			get;
			private set;
		}

		/// <summary>
		/// Подпись в detached виде
		/// </summary>
		public byte[] SignatureBody
		{
			get;
			private set;
		}

	    /// <summary>
		/// Загрузить ПодписанныйПакет из массива байт
		/// </summary>
		public SignedPackage(byte[] packageStreamBytes)
		{
			
		}
	}
}
