﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Progresoft.Accounts.ElectroSignature
{
    /// <summary>
    /// Интерфейс, реализуемый любым модулем проверки ЭЦП
    /// </summary>
    public interface IVerificationModule
    {
		/// <summary>
		/// Проверить валидность детачнутой cms-подписи
		/// </summary>
		/// <param name="body">Тело, блоб</param>
		/// <param name="signature">Подпись, блоб</param>
		/// <returns></returns>
        VerificationResult VerifySignature(byte[] body, byte[] signature);
		/// <summary>
		/// Подписать блоб дефолтной подписью для данного СКЗИ
		/// </summary>
		/// <param name="body">тело, блоб</param>
		/// <returns>cms sign, blob</returns>
    	byte[] Sign(byte[] body, SignerDescriptor signer);

		/// <summary>
		/// Константа для идентификации метода:
		///  * Константа: cryptopro
		///  * Константа: interpro
		/// Для КИАСа.
		/// </summary>
		string CSPTag { get; }
    }

    public enum SignatureStatus
    {
        Valid = 0,
        CertificateExpired = 1,
        CertificateRevoked = 2,
        SignatureError = 3,
		PackageCirrupted = 4,
		NoTrust = 5,		
    }
}
