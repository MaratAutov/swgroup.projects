﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using NLog;
using Progresoft.Accounts.ElectroSignature.InterPro;
using Progresoft.Accounts.ElectroSignature.Sharpei;

namespace Progresoft.Accounts.ElectroSignature
{
    public class SignatureEngine
    {
        public static readonly SignatureEngine Instance = new SignatureEngine();

        private static Logger _logger = LogManager.GetLogger("SignatureEngine");

        private readonly IDictionary<string, IVerificationModule> _supportedCryptoz = new Dictionary<string, IVerificationModule>();
        private IVerificationModule _defaultSignatureModule;

        protected SignatureEngine()
        {
            var cryptoPro = new CryptoProSignatureVerificationModule();
            _supportedCryptoz.Add(".sig", cryptoPro);
            _supportedCryptoz.Add(".sgn", cryptoPro);
            _supportedCryptoz.Add(".sign", cryptoPro);

            _defaultSignatureModule = cryptoPro;

            var interPro = new InterProCmdLineVerificationModule();
            _supportedCryptoz.Add(".ipro_sig", interPro);
        }
      
        /// <summary>
        /// Check if signing method is supported by SignatureEngine
        /// </summary>
        /// <param name="ext">signature file extension</param>
        /// <returns>'true' if extension is registered as supported otherwise 'false'</returns>
        public bool Supports(string ext)
        {
            return _supportedCryptoz.ContainsKey(ext);
        }

        public VerificationResult Verify(byte[] body, byte[] sign, string signer)
        {
            IVerificationModule module = null;
            _supportedCryptoz.TryGetValue(signer, out module);

            if (module == null)
            {
                // bad
            }
            var res = module.VerifySignature(body, sign);
            res.SingatureType = module.CSPTag;
            return res;
        }

        /// <summary>
        /// Подписывает дефолтным алгоритмом дефолтным ключем
        /// </summary>
        /// <param name="?">Файл, который необходимо подписать</param>
        /// <returns>Файл <see ref=SignedPackage></see> с файлом и эцп</returns>
        public byte[] Sign(string name, byte[] body, SignerDescriptor signer)
        {
            // 0. упаковать!
            using (var m = new MemoryStream())
            {
                using (var s = new ZipOutputStream(m))
                {                
                    var bodyEntry = new ZipEntry(name);
                    s.PutNextEntry(bodyEntry);
                    s.Write(body, 0, body.Length);                 
                }
                body = m.GetBuffer();
                name = Path.ChangeExtension(name, "zip");
            }

            // 1. подписать

            byte[] sign = _defaultSignatureModule.Sign(body, signer);
                //new byte[] { 1, 2, 3 };
            // 2. упаковать
            using (var m = new MemoryStream())
            {
                using (var s = new ZipOutputStream(m))
                {
                    var bodyEntry = new ZipEntry(name);
                    s.PutNextEntry(bodyEntry);
                    s.Write(body, 0, body.Length);
                    var signEntry = new ZipEntry(name + ".sig");
                    s.PutNextEntry(signEntry);
                    s.Write(sign, 0, sign.Length);
                }
                return m.GetBuffer();
            }
        }


        private const string _signatureExt = ".sig";

        private static byte[] Decompress(ZipFile file, ZipEntry entry)
        {
            using (Stream s = file.GetInputStream(entry))
            using (BinaryReader br = new BinaryReader(s))
                return br.ReadBytes((int)entry.Size);
        }
    }

    /// <summary>
    /// Абстракция вокруг того, как мы идентифицируем подписанта в настройках.
    /// Зачем? А вдруг завтра нам будет недостаточно CN; а протягивается этот параметр через 18 методов
    /// </summary>
    public class SignerDescriptor
    {
        public string CN { get; set; }

        public override string ToString()
        {
            return "CN=" + CN;
        }
    }
}
