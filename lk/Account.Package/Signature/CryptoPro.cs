﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Account.Package.Signature
{
    public sealed class CryptoPro : ISignatureValidator
    {
        public static readonly string[] _extensions = new string[]
        {
            ".sig",
            ".sign",
            ".sgn"
        };

        public string[] SupportedExtensions
        {
            get
            {
                return _extensions;
            }
        }
    }
}
