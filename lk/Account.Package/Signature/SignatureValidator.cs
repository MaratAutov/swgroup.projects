﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Account.Package.Signature
{
    public static class SignatureValidator
    {
        private static Dictionary<string, ISignatureValidator> validators { get; set; }

        static SignatureValidator()
        {
            validators = new Dictionary<string, ISignatureValidator>();

            Register(new CryptoPro());
        }
        
        public static void Register(ISignatureValidator validator)
        {
            foreach(string ext in validator.SupportedExtensions)
                validators.Add(ext, validator);
        }

        public static void Unregister(ISignatureValidator validator)
        {
            foreach (string ext in validator.SupportedExtensions)
                validators.Remove(ext);
        }
    }
}
