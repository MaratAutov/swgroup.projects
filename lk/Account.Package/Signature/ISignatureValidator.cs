﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Account.Package.Signature
{
    public interface ISignatureValidator
    {
        string[] SupportedExtensions { get; }
    }
}
