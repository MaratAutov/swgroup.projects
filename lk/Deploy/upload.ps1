﻿import-module .\ftp.psm1
import-module .\ftpdeploysettings.psm1

[string] $username = "Account_deploy_user"
[string] $userpassword = "123456"
[string] $ftp = "ftp://fsfr/" 

[string] $InitialFolder="c:\Deploy\SPO\"

#param ([string] $path)

# path on ftp to deploy
#$path = "Account"
$path = $args[0]
$pathtosource = $args[1]

Set-FtpConnection $ftp $username $userpassword

#very slow deleting
#Remove-FromFtp "Account"

$InitialFolder += $pathtosource + "\"
Send-ToFtp $InitialFolder $path
#upload web.config
$configurlsource = [System.IO.Path]::Combine($InitialFolder , "Web.Test.config")
$configurldest = $ftp + "/" + $path + "/" + "Web.config"
Send-FileToFtp $configurlsource $configurldest