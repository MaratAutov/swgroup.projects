import psycopg2
import os, datetime, sys, codecs

path_sql_script = os.path.join(os.path.dirname(os.path.realpath(__file__)), "scripts")

def connect():
    try:
        conn = psycopg2.connect("dbname='Account' user='postgres' host='fsfr' password='12345'")
        conn.set_isolation_level(0)
        return conn
    except:
        print("I am unable to connect to the database")
        exit(1)

def reset_settings():
    conn = connect()
    cur = conn.cursor()
    cur.execute("""truncate table "Version";""")
    conn.commit()
    cur.close()
    conn.close()

def initial_connection_to_config_store():
    version = 'unknown'
    conn = connect()
    cur = conn.cursor()
    cur.execute("""SELECT * from "Version";""")
    rows = cur.fetchall()
    row_count = len(rows)
    if row_count > 1:
        print("Error! Settings rows more than one! Truncating table")
        reset_settings()
    
    if row_count == 0:
        print("Error! Settings row not exist. Creating settings row...")
        version = '0000'
        cur.execute("""insert into "Version"(number) values('%s');""" % version)
        conn.commit()
        print("Settings row created succesfully")
    else:
        version = rows[0][0]
    cur.close()
    conn.close()
    if version == "unknown": 
        print("Error! Vesion is set to Unknown")
    return version

def execute_script(full_file_path):
    conn = connect()
    cur = conn.cursor()
    try:
        procedures  = codecs.open(full_file_path,'r','utf-8').read()
        print(procedures);
        cur.execute(procedures)
        conn.commit()
    except psycopg2.ProgrammingError as e:
        print("Error in script %s" % full_file_path)
        print("EXCEPTION: procedures :%s" % str(e))
        exit(1)
    cur.close()
    conn.close()

def get_scripts(directory, version):
    sorted_file_list = []
    for name in os.listdir(directory):
        if name[-3:].lower() == "sql" and name[:4] > version:
            sorted_file_list.append(os.path.join(directory, name))
    sorted_file_list.sort()
    return sorted_file_list

def update_version(version):
    conn = connect()
    cur = conn.cursor()
    cur.execute("""update "Version" set number = '%s', update_at = '{%s}';""" % (version, datetime.datetime.now()))
    print("Version settings updated to %s" % version)
    conn.commit
    cur.close()
    conn.close()

def migrate():
    print("Start migrate")
    version = initial_connection_to_config_store()
    for file in get_scripts(path_sql_script, version):
        execute_script(file)
        update_version(file[-8:-4])
    print("Finish migrate")
    exit(0)

def migrate_to_version(v):
    p = ".\\scripts\\" + v + ".sql"
    print("Start migrate to %s" % p)
    execute_script(p)

#reset_settings()
if len(sys.argv) > 1:
    migrate_to_version(sys.argv[1])
else:
    migrate()

