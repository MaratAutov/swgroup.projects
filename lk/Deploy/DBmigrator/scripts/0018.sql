ALTER TABLE "UserAccount"
   ALTER COLUMN "Password" TYPE character varying(40);

ALTER TABLE "UserAccount"
   ALTER COLUMN "Phone" TYPE character varying(50);

DROP VIEW "PackageFullInfo";

ALTER TABLE "UserAccount"
   ALTER COLUMN "Login" TYPE character varying(320);

CREATE OR REPLACE VIEW "PackageFullInfo" AS 
 SELECT "Package"."Name", "Package"."OwnerID", u."Login" AS "Owner", 
    "Package"."UploadTime", "Package"."InitialStatusID", 
    "Package"."CurrentStatusID", "ReportInfo"."IncomingNumber", 
    "ReportInfo"."IncomingDate", "ReportInfo"."SedNumber", 
    "ReportInfo"."SedDate", "Package"."BlobID", "Package"."ID", 
    s1."PrimaryCode" AS "CurrentStatusCode", 
    s2."PrimaryCode" AS "InitialStatusCode", "Package"."ReportID", 
    "Package"."SignatureType", "ReportInfo"."INN", "ReportInfo"."OGRN", 
    "ReportInfo"."Model", "ReportInfo"."Type", "ReportInfo"."DocTypeName", 
    "ReportInfo"."OrgTypeName", "ReportInfo"."ExternalID", 
    "ReportInfo"."Version", "ReportInfo"."OrgFullName", 
    "ReportInfo"."OrgShortName", "ReportInfo"."Address", "ReportInfo"."Subject", 
    "ReportInfo"."FirstName", "ReportInfo"."LastName", 
    "ReportInfo"."MiddleName", "ReportInfo"."Year", "ReportInfo"."Quartal", 
    "ReportInfo"."Month", "ReportInfo"."Date", "ReportInfo"."OrgTypeCode", 
    "ReportInfo"."DocTypeCode", "ReportInfo"."RoName", 
    "ReportInfo"."IsRoReport", s1."Info" AS "CurrentStatusInfo", 
    s2."Info" AS "InitialStatusInfo", 
    s1."Exception" AS "CurrentStatusException", "ReportInfo"."RoType", 
    "Package"."IsImported"
   FROM "Package"
   JOIN "ReportInfo" ON "Package"."ReportID" = "ReportInfo"."ID"
   JOIN "Status" s1 ON "Package"."CurrentStatusID" = s1."ID"
   JOIN "Status" s2 ON "Package"."InitialStatusID" = s2."ID"
   LEFT JOIN "UserAccount" u ON "Package"."OwnerID" = u."ID";

UPDATE "UserAccount"
	SET 
		"Password" = '3DFA829D21D842A003D83F4CC59B78B51EF85310'
	where 
		"Login" = 'dev' or "Login" = 'test';

insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vladimir@partad.ru', '89EE05F18D85BF3A88A57179DFF6183C8F50EAB6', 
 '2010-01-27 11:01:03.000',True,False,NULL,'������, ������ �������� ����������','7710028130','1027739915400','4957896885', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'support@naufor.org', 'DCA207B2E7BEE86CFD740DB5F62DF7F5BC737DE6', 
 '2010-01-27 14:27:11.000',True,False,NULL,'����������������','7728167023','1027739383463','7877774', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'odk@odk.ru', 'A0D99DBDE270BF9A43C1802FC70DCFEC3CC08D89', 
 '2010-01-27 17:12:44.000',True,False,NULL,'��� ��� ������������������ �����������','7705110090','1027739157522','4959563070', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'eochertyanova@armd.ru', '62C451A619A34023EF09C286F00758C9587A38C8', 
 '2010-01-28 09:40:48.000',True,False,'���������� �','������','7704030460','1055800432951','495-797-60-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dmitry.sergeevich.ivanov@gmail.com', 'DDBACD10AB2C57C081AC4C161D23743B018F81EE', 
 '2010-01-31 12:50:38.000',False,False,NULL,'test','7704030460','1031621006042','113113113', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'accounts.fcsm@gmail.com', 'DDBACD10AB2C57C081AC4C161D23743B018F81EE', 
 '2010-01-31 13:26:05.000',True,False,NULL,'accounts.fcsm@gmail.com','1655070635','1027700262270','123321123', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@nfa.ru', '3713C69FF87380631DA39BB97B6515DE3E00AF7C', 
 '2010-02-02 15:44:15.000',True,False,'����� ������� ������������','���','7717088481','1037739452981','(495) 980-98-74', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'borodin@micex.com', 'DE23E210275990FCD936AA0FECE6ADAE3E2F2F14', 
 '2010-02-03 09:23:58.000',True,False,'������� ����� �����������','��� "�������� ����� ����"','7703507076','1037789012414','790-6713', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm@troika.ru', 'E221A7A5C9BB4620909AD32065C44F18AE6703D4', 
 '2010-02-03 12:14:19.000',True,False,'��������� ������ ����������','�������� ����������� �������� "�������������� �������� "������ ������"','7710048970','1027739007768','(495) 258-0500', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vv@reap.ru', '4F96615E347F53716376CED2586DCDD0A5A06813', 
 '2010-02-04 10:02:05.000',True,False,'�������� �������� ������������','��� ������ �-����','5407178808','1025403189481','8-383-218-76-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ircol@ircol.ru', '017CEEA4BE243FFE4987BB2823E4F7737A93C94F', 
 '2010-02-04 10:29:17.000',True,False,'��������� ������� ���������','��� �����','7728023430','1027739042396','+7(495)6329090', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nik@spcex.ru', '17A751A464657395107D19CDCB9F4C1C28481017', 
 '2010-02-05 13:16:00.000',True,False,'��������� ������� ����������','��� ����','7825331045','1037843013812','(812) 3243828', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'maril@ddom.ru', '00C021FCC21EAE993D400D1892EFA3C3B0328B50', 
 '2010-02-08 09:03:58.000',True,False,'�������� �����','��� "���������� ��� "����� �����"','6454014608','1026403340655','(8452) 274-555', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'grachev@kortacompany.ru', '712BF1BBC91C96AA0DDF8C2DA0C99D515F44504B', 
 '2010-02-08 10:13:13.000',True,False,'������ ����� ����������','��� "�����"','7841393970','1089847338965','(495) 967-86-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rost@rrost.ru', '6558911CF2AF548E82F484CFCB234B2CED3B131D', 
 '2010-02-08 10:31:20.000',True,False,'�������� �.�.','��� "����������� �.�.�.�."','7726030449','7726012517542','(495) 771-73-35', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aton-line@aton-line.ru', '7F2D80C744CFDED1D81ED789797ECABCB4D9CC6A', 
 '2010-02-08 10:44:23.000',True,False,'��������� ��������� ������������','��� "����"','7702015515','1027739583200','(495) 228-38-97 (3412)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ntryapichnikova@mgsec.ru', 'B3F8E4F294BABE7406FDDDA66441E1E07CD0B8CC', 
 '2010-02-08 10:56:12.000',True,False,'������������ ������� ����������','��� "�������������� �������� "�� ��� ����������"','7707520007','1047796413851','782-93-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'roganski@rts.ru', '600A907B40E857FD3B9265D5FF0BD6F6F8F69927', 
 '2010-02-08 11:24:29.000',True,False,'��������� ����� �������������','�������� ����������� �������� "�������� ����� ���"','7707284328','1027739235006','+7 (495) 705 90 31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Oksana.DOBROVOLSKAYA@raiffeisen.ru', 'FE20DE566DF5B1EA26400CA17D0BEEDB978A3EA6', 
 '2010-02-08 11:29:51.000',True,False,'������������� ������ ����������','��� "�� "���������� �������"','7702358512','1037702037680','745-52-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gp@ugrafinance.ru', 'FC7F99C70C8D49F0A095199B1DAC425F65BA3A7A', 
 '2010-02-08 11:34:44.000',True,False,'���������� ������ ���������','����������� �������� "����������" (�������� � ������������ ����������������)','7724567580','1067746244070','(495) 641-23-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mw_compliance@morganstanley.com', '5930DC3F6F39757911C161E0A6962F5140982414', 
 '2010-02-08 11:50:09.000',True,False,'������� ����� ����������','��� "������ ������ ����"','7750003929','1057711007023','287-21-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Anna.myagchenkova@rmg.ru', '7B5607BD6E128F31497AC439CE17E3DA471A5869', 
 '2010-02-08 12:42:50.000',True,False,'���� ���������� ����������','�������� ����������� �������� "���, ��� ��� ��� ����������"','7710008600','1037739424250','(495) 258-62-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'backoffice@rshb.ru', 'F860E51AA45097C2C81F56807C9E46EC1F202DF2', 
 '2010-02-08 13:05:48.000',True,False,'�������� ������ �������������','��� "��������������"','7725114488','1027700342890','363-02-94', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ermoshina@finbank.ru', '3BDDCA2180787B6D83E6F5EBDD8BBA601798D3CD', 
 '2010-02-08 13:26:21.000',True,False,'�������� ������� ������������','��� "������ ��������������" (���)','7713073043','1027739222246','(499) 900-88-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'afonkin@micex.com', 'B3EC2A6F7AE005673A80F98085F930911452072A', 
 '2010-02-08 13:44:23.000',True,False,'�������� ����� �������������','�������� ����������� �������� "���������� ����� ����-���"','7702077840','1027739387411','(495) 234-24-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'backoffice@rarussecurities.com', '3D3AE72CF8E902291B26BCB4DD9F7073159561B3', 
 '2010-02-08 13:50:43.000',True,False,'���������� ��������� ������������','��� "����� ����������"','7705879440','1097746048267','287-37-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rts@energ.ru', '2F07378D024BD8EAEA1DEC0FEE85C2DDA94C3CF9', 
 '2010-02-08 13:51:25.000',True,False,'��������� ������ ������������','�������� ����������� �������� "�������������� �������� "�������������"','7803053926','1027809205841','(812) 329-55-86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vdor@mosfud.ru', 'A2F458A848691CB84085469EC222F37E3F0E727A', 
 '2010-02-08 13:55:46.000',True,False,'������� ������� ����������','�� ���� ������ � ���','7707142066','1037739537560','(499) 238-24-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'otchet_FCSM@mgto.ru', 'E4A8073EF63D1E92AC69B227BEEBB84A23AA9C80', 
 '2010-02-08 14:04:45.000',True,False,'������ ������ ���������','��� �� "������������"','8620015121','1038603652361','(495) 925-80-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aivanov@veles-capital.ru', 'F1D3D1318CAC4D1F32390E5155D7D9C18B557875', 
 '2010-02-08 14:54:20.000',True,False,'������ ��������� ���������','��� "�� "����� �������"','7709303960','1027700098150','258 19 88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'NParshikova@rusfund.ru', 'C975396515ED998180515249ACC62CB408CF6FDE', 
 '2010-02-08 15:41:38.000',True,False,'��������� ������� ����������','��� "������� �����"','7744003215','1037744006981','+ 7 (495) 725-55-15 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rk@rk.utk.ru', 'A372ED5661D3FDCA0D805FE954A2990B8364A862', 
 '2010-02-09 04:49:58.000',True,False,'������ ������� ����������','��� "����������� - �������"','6659035711','1026602947414','(343) 349-56-06', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@am-aton.ru', '4F72806A093F926D4842B04E026455CA7C073952', 
 '2010-02-09 05:56:40.000',True,False,'�������� ������ ����������','��� "�� "����-����������"','7701253764','1027700027233','510-15-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@lgn.ru', '942021921400BF84C9ED8DF53AF6E96A318229EA', 
 '2010-02-09 06:08:50.000',True,False,'������� ������ ������������','����������� ������������ ���� "������" (�������� ����������� ��������)','7750005524','1097711000100','(495) 781-00-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Svetlana.Krylova@unicreditgroup.ru', '6C48110E85ED2A7DCECC977C876FC2710F17BC02', 
 '2010-02-09 06:14:04.000',True,False,'������� �������� ���������','��� ��������� ����','7710030411','1027739082106','(495) 723-71-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nuri@sovlink.ru', 'D4B629534A44EEA92D881965D9E4760616BDF761', 
 '2010-02-09 06:18:41.000',True,False,'������� �.�.','��� "�������"','7707121820','1027739102225','9671300', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@koin.ru', '3C549061BBA007B780FF448A84414E553B7DD5AC', 
 '2010-02-09 07:38:53.000',True,False,'������� ������� ���������','��� "�� "����"','7709341476','1037739668196','(495) 225-25-75', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk_stat@open.ru', 'CF4C1049CC681F819493F3E07ABF21B4710F041C', 
 '2010-02-09 08:22:01.000',True,False,'��������� ���� ���������� ','��� �� "��������"','7705394773','1027739072613','495 232 59 73', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'samokhinalv@lmsic.com', 'AC1D93FC453162B0BDC7F6D643F4ADEEECA8F9C6', 
 '2010-02-09 08:31:22.000',True,False,'�������� ������ ������������','�������� ����������� �������� "�������������� �������� "��������������"','7806027770','1027807976680','(812) 329-19-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@trinfico.ru', 'F1F2C650C898325DFDDD3E229E6612DC2E4B8758', 
 '2010-02-09 08:31:44.000',True,False,'������� ����� �������','�������� ����������� �������� "��������"','7724136129','1027700085126','(495) 725-2500', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@promptcap.ru', 'A05D2C3A61093DFFC022F2D650419A5E0659B1E1', 
 '2010-02-09 08:48:16.000',True,False,'������ ���� �������������','��� "�������������������"','7710579120','1057746575600','8 (495) 780-55-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tcapital@trinfico.ru', 'FC0780D864FE08BCA33BB200EF0AB5D42AAB398A', 
 '2010-02-09 08:49:06.000',True,False,'����������� ����� ����������','�������� ����������� �������� "�������� �������"','7704041398','1027700084620','(495) 725-2500', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'slava.isaev@baml.com', '4F962FFC59F9F3BDBACDCAD7F1601A05797D9A2A', 
 '2010-02-09 08:55:47.000',True,False,'����� �������� ���������','��� "������� ���� ����������"','7703622255','1077746005666','(495) 6626027', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sergey.sadovsky@dcc.ru', '540E6D2CED74D2433123BDD8547C08FFF961FB17', 
 '2010-02-09 08:57:28.000',True,False,'��������� ������ ����������','�������� ����������� �������� "�����������-����������� ��������" ','7710021150','1027739143497','7(495)9560999', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'boris.kuchin@db.com', '93AD327F1471063E564E45BBF08CD231FD9B8200', 
 '2010-02-09 09:00:44.000',True,False,'����� ����� ������������','��� "����� ����������"','7709035743','1027700094487','8(495)933-92-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'belka@rts.ru', 'D81CC61F0C5F73E0CFB4AE911B023093235E22AD', 
 '2010-02-09 09:21:42.000',True,False,'������� ���� ������������','�������� ����������� �������� "����������� ����� ���"','7712071068','1027700563890','+7(495)705-90-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sales@temp-invest.ru', '0E919ED0097D257B01E3C6DFEA7117DF06B9E17A', 
 '2010-02-09 09:29:37.000',True,False,'���������� ������ ���������','��� ��� "����-������"','6904025627','1026900566340','(4822)52-00-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'standart@fcstd.ru', 'D74CAB72CF655D9F454E608BAFFFE2509CD536ED', 
 '2010-02-09 10:27:47.000',True,False,'��������� ����� ����������  ','�������� ����������� �������� "���������� ���������� "��������"','7810249119','1027804868662','(812)610-04-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Elena.Kutkina@ubs.com', '2F7270C7526B03100B02EDFA8D4EDF2A796DE0C5', 
 '2010-02-09 11:10:04.000',True,False,'������� ����� ������������','��� "� �� �� ����������"','7711079811','1037739330012','(495) 648 2442', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'knyazeva@ccb.ru', 'E1644F5C124D0742CB6DACEBE4F13F5EF4EF00C5', 
 '2010-02-09 11:12:27.000',True,False,'������� ������� ����������','��� "������������" (���)','7707025725','1027739198387','956 86 27 (329)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@mparm.ru', 'F668E2A11078034DE92135D14C27DE30E23EBFBE', 
 '2010-02-09 11:12:31.000',True,False,'��������� ���� ������������','��� "���������� �������"','7721563919','5067746334080','(495) 363-96-19', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'malkovsky@infina.ru', '56983155591C7473E1CD72FB7A4CD095FF165012', 
 '2010-02-09 11:15:41.000',True,False,'������������ ��������','��� �� "������"','7722557227','1057748376124','8-495-780-43-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Alla.Berdina@ifdk.com', '9AE3B82137E35859B88B37EEC9DE680A922F0E44', 
 '2010-02-09 11:34:01.000',True,False,'������� ����','��� "�������������� �������� "��������"','814054090','1028601442210','(495) 411-52-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'OL-Russia-Com-FSFM-Bank@ubs.com', '671A28D799798E0E3B140D1A90754109DD38765A', 
 '2010-02-09 11:41:41.000',True,False,'������ ���� ����������','�������� � ������������ ���������������� "� �� �� ����"','7750003982','1067711001863','7 (495) 648-20-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@region.ru', '151B2F7AD321EF7C27D548B5B0C033A4E2DD8995', 
 '2010-02-09 12:10:42.000',True,False,'������� ������� �������������','�������� ����������� �������� "������������ �������� "������"','7708213619','1037708002144','495-777-29-64 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'boa@finpar.ru', 'C49D4564C5D30ABC4F8B92B65E04D4C3B432BC80', 
 '2010-02-09 13:32:21.000',True,False,'�������� ������','��� "���������� �������"','7714278597','1027714010411','(495) 940-05-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uralsibcapreports@uralsibcap.ru', '71556D331C55EF2F196DED71AE4E13D9C69B818E', 
 '2010-02-09 18:18:35.000',True,False,'������ ������� ����������','��� "������� �������"','7707194868','1027739000739','(495) 705-90-39', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uralsibwebreports@uralsibweb.ru', '24B55B6441A90B1D21B4B8368837C67A71656185', 
 '2010-02-09 18:23:45.000',True,False,'�������� ������� ����������','��� "������� �������-���������� ������"','7702526358','1047796391257','(495) 925-59-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivsas@tnk-bp.com', 'F529539E983747718A0571E9D8E9EED78090EF63', 
 '2010-02-09 18:38:41.000',True,False,'��� ����� ����������','��� "����-������"','7730125647','1027739462630','(495) 363-60-91', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Golikova_OV@surgutneftegas.ru', 'DE1653CB7786F2E55BF5446C29B9394FFCD2F979', 
 '2010-02-10 08:43:40.000',True,False,'�������� ����� ������������','��� "�����-����������"','8602102290','1028600581602','(3462)46-21-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gon@accord-invest.ru', 'BB592EA13AFA10E77B7161F41974C7DA946375AF', 
 '2010-02-10 09:27:44.000',True,False,'��������� �.�.','��� "������-������"','7744002483','1027739000398','(347)273-50-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'GKedruk@izhcombank.ru', '6F587ADFA65882E982CCB38586866A326E473C6B', 
 '2010-02-10 09:31:59.000',True,False,'������ ������ ����������','����������� ������������ ���� "���������" (�������� ����������� ��������)','1835047032','1021800000090','(3412)919-130', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uralocenka.ru', '5EE3D1426670C84296D16A51CEAD410C98309FA3', 
 '2010-02-10 10:34:38.000',True,False,'������� ��������� ���������','��� "����������"','6658175219','1036602688143','(343) 245-63-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Pavlova-SI@cher.metcombank.ru', '4A7A5D6D8938E707591439BF8B37632A74D112AB', 
 '2010-02-10 11:02:32.000',True,False,'������� �������� ��������','��� ����������','3528017287','1023500002404','8 (8202) 23-93-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Julia@libracapital.ru', 'AB64EE7AA4D9643B60737C229EE3646603BC9720', 
 '2010-02-10 11:19:07.000',True,False,'�������� ���� �������������','��� �������������� �������� "����� �������"','4823007256','1024840826933','74955807781', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@vetb.ru', '15033155A69625703CAE6FA5E5B6F6FA1321DEE8', 
 '2010-02-10 11:22:20.000',True,False,'������� ������ ����������','�� "��������-����������� ��������� ����" (���)','7744002028','1027739043628','+7(495)646-2797', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Svetlana.Balyasnikova@prof-invest.com', 'E785F825FC618A4F2613266A717E44AA346615EA', 
 '2010-02-10 11:48:01.000',True,False,'����������� �������� ������������','��� ��� "����������"','7615002703','1027601493622','(4852) 72-65-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ffmsreporting@rencap.com', '269247FE15A2341C22FE0AFF7A8336ED56CA718E', 
 '2010-02-10 12:12:56.000',True,False,'����� ����� �������������','�������� � ������������ ���������������� "��������� ������"','7710591880','1057747525791','+7(495)7255225', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'petrova_a@sovintel.ru', 'F37AA184E44B842CC790E389EE945B8C2563A258', 
 '2010-02-10 12:15:07.000',True,False,'���������� ���� ��������','��� "��-�� ����������"','7826002780','1027810260830','(812)234-09-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga@uukrt.ru', '951C6911009748A56BAD2EB1EAFE7F325B64A730', 
 '2010-02-10 12:54:08.000',True,False,'�������� ����� �������','�������� ����������� �������� "�������� ����������� �������� ���������� ���������"','7709424115','1037709044361','(843)2921590', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rsu3@yadex.ru', 'C5A43425F1FAAB97C1A8451E8512942D87E4DDFE', 
 '2010-02-10 13:15:01.000',True,False,'��������� ������� ������������','���"��������-������������ ���������� -3"','7713085313','1027739107758','4808100', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ekaterina.l.borzova@jpmorgan.com', 'BDD5E876D5D980B585E1BC9824BD0D616596ACEA', 
 '2010-02-10 14:02:07.000',True,False,'������� ��������� ����������','�� "��.�. ������ ���� ����������"','7710014949','1027739606245','(495)967 1889', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'afmg@mail.ru', '09F3CCD6870FD7BA202FA3BE45B3620364679F28', 
 '2010-02-10 14:25:59.000',True,False,'�������� �.�.','��� "���"','5612065074','1075658031866','(3532) 35-03-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a.galushkin@kf.ru', '0D9F9E1E21FDC65A0463A3DA039A448DA22D0BE4', 
 '2010-02-10 14:36:35.000',True,False,'�������� ������ ���������','��� ������ (���)','7825429040','1027809255858','78123261305', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nfa@newsymbol.ru', 'DF80E9491004A0E259ECFDEA17BC37586D19EE04', 
 '2010-02-10 14:52:33.000',True,False,'������ �.�.','���� ����� ������ (���)','7734028813','1027700412893','+7 (499) 946-4260', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'konovalova@solidinvest.ru', 'C651D746656F0292D1467916A555301A95E4BE2A', 
 '2010-02-10 16:40:32.000',True,False,'���������� �������� ������������','��� ��� "�����"','5008009854','1027739045839','(495) 228-70-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ShkuropatSA@pervobank.ru', '4A94D7B9574E94856844A243ACB8AAD253CB9A6E', 
 '2010-02-10 16:58:06.000',True,False,'�������� �������� �����������','��� "���������"','6316106558','1066300000260','(846) 278-78-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'l.hismatullina@afbank.ru', '20CB5F65918A6BBF9A08A09F95063C51573E1F57', 
 '2010-02-11 08:12:27.000',True,False,'������������ ����� ���������','��� "�� ����"','274061157','1020280000014','(347) 2-911-301', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm_user@gazbank.ru', '050FCD0BEF182C0D830E5808AB680CA17103F4C5', 
 '2010-02-11 09:48:44.000',True,False,'������� ����� �������','��� ��� "�������"','6314006156','1026300002244','(846)337-90-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alexander.trusov@wnog.org', '6565371D4DC381A5EE9E4E99043475790295E046', 
 '2010-02-11 10:17:41.000',True,False,'������ ��������� �������������','��� "����"','7802470236','1097847141116','313-91-92', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'z.garaev@uksun.ru', '92B648B3414BD9070520FBAF3CA2767A4D789946', 
 '2010-02-11 10:51:59.000',True,False,'������ ������� ���������','��� "����������� �������� "���"','7721607806','1077764541337','74952210724', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'back-office@gerfin.ru', '94C4893D531BB66CC29545E942E46C8FA6A73EE3', 
 '2010-02-11 11:18:12.000',True,False,'��������� �����','��� "������ ������"','7714609721','1057747540366','(495)748-05-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'maria.kirina@citi.com', 'F3785211E1302F9C0B35A56218F1B77629CFFBFA', 
 '2010-02-11 12:05:43.000',True,False,'������ ����� ����������','��� �� ��������','7710401987','1027700431296','+7 495 642 74 78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ron.in', 'CC489EEDBB3A3E5555EB708BE707F02B1640601D', 
 '2010-02-11 12:09:53.000',True,False,'�������� ��','��� �����','7718686491','1087746130823','+7 495 792-30-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr_bank@uralsib.ru', 'AED701C7ADCBDDC3FF6D0318E8B52ABDD2ACE163', 
 '2010-02-11 14:04:46.000',True,False,'�������� �.�.','��� "�������"','274062111','1020280000190','(495)-705-90-39', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'RENBRffmsreporting@rencap.com', '269247FE15A2341C22FE0AFF7A8336ED56CA718E', 
 '2010-02-11 14:42:01.000',True,False,'����������� ����� ���������','�������� � ������������ ���������������� "��������� ������" ','7709258228','1027739121981','(495) 258 7777', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'RECAZffmsreporting@rencap.com', '269247FE15A2341C22FE0AFF7A8336ED56CA718E', 
 '2010-02-11 14:43:52.000',True,False,'����������� ����� ��������','�������� ����������� �������� "��������� �������"','7708059484','1027739298190','(495) 258 7777', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buhpmi@yandex.ru', 'C808BC829313C1C44B926497D78B8E1E907C177E', 
 '2010-02-11 14:56:52.000',True,False,'�������� �.�.','��� "�������������"','7743551064','1057746236371','781-90-09', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vt@fm-c.ru', 'F1E1A0794A1DF38E3E96B8EEB1DCE0A183322C68', 
 '2010-02-11 15:46:37.000',True,False,'������� �������� ������������','�������� � ������������ ���������������� "�� �������"','7721620250','1087746551210','(495) 287-47-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.frolov@kf.ru', '79CC5F0AD23924C1DECA1C501A43C50C6E7CA693', 
 '2010-02-11 16:06:29.000',True,False,'������ �.�.','��� ������ �������������� ���� (���)','7831000637','1027800000062','(812) 326 - 1305 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'TVGluhova@bfgbank.ru', 'E077D4C1ED8387209DB21B6C74975736CF408C49', 
 '2010-02-11 16:22:50.000',True,False,'������� �.�.','�� ���� - ������ (���)','7730062041','1037739226128','+7(499)249-06-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rudyk@mos-broker.ru', '96EEC7A52C2A01E29A5CBFC32E45D399CDAE63BE', 
 '2010-02-11 16:37:40.000',True,False,'����� ����� ������������','��� "���-������"','7702668793','1087746377113','(495) 380-27-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'control@taif-invest.ru', '1861CAD4166F825D86C8D916BC4DDA1CA9C1039D', 
 '2010-02-11 18:24:23.000',True,False,'������� ������ �.','��� "����-������"','1655028721','1021602834516','(843) 236-33-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'knv@roperator.ru', 'D755600693BE223A531B59EF37E52CB69E360DF1', 
 '2010-02-11 19:38:56.000',True,False,'������ ������� ����������','�������� ����������� �������� "��������� ������������������ ����������� - ��������� ���������� �80 "��������������" ������ ���������"','2129029793','1022101275767','78352299099', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'y.chechel@mnpf-akvilon.ru', 'A30C6C9481A6873278418DD72F1D50869F18BA83', 
 '2010-02-12 10:27:42.000',True,False,'������ ��� ������������','���� "�������"','8901009532','1028900509923','8-499-402-89-91', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sharpov@globalcap.ru', '5985E957E1F88BF13F2899DE5E2D9143C69039EA', 
 '2010-02-12 11:20:26.000',True,False,'������ �������� ��������������','�������� ����������� �������� "�������������� �������� ������ �������"','7720580118','5077746446872','+7 (495) 959-15-05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@gidinvest.ru', 'CB687FDABA2198E9DE6FD284484887AA183854A3', 
 '2010-02-12 13:40:44.000',True,False,'������ ����� �����������','���"��� �� �� ������"','6455002676','1026403668741','(8452) 39-37-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'at@frsd.ru', 'F38B84B59065500C690859BCB0917817F5D799D0', 
 '2010-02-12 13:43:05.000',True,False,'������� ������ ������������','��� ������ ������������������ �����������','7710198911','1027700373678','7950859 +166', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dealer@komi.com', '326261361B1F3F405685F1E6FCC396D5482BE090', 
 '2010-02-12 15:56:00.000',True,False,'��������� �.�.','��� �������������� �������� "�����"','1101124861','1061101019110','8212-21-54-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bkv@accent.ru', '3891BD5CC5DC0E45E7376E58201638B37ECFBCEC', 
 '2010-02-12 16:14:23.000',True,False,'���������� ������� �������������','��� "��������-��������"','7802421944','1089847044980','(812) 327-43-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '9940180@mail.ru', 'F9CE50F317C8851700BA9927CC85AE94815DFC1F', 
 '2010-02-12 16:32:06.000',True,False,'���������� ���� ����������','��� "502 �����"','5031085677','1095031002098','89169940180', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'audit-kontakt@yandex.ru', 'E83AEC31BFDB0B986AE35B44A8771457A3DA8721', 
 '2010-02-12 17:37:47.000',True,False,'������ ���� �������','��� "�����-�������"','5258028112','1025202608364','88314102407', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Info@itks.ru', 'ECD29AA4CDC060B04DBC16FBBB3D30A8A67390B0', 
 '2010-02-12 17:58:11.000',True,False,'�������� ����� ����������','�������� ����������� �������� "�������� "�����������������"','7203122310','1027200823187','(3452) 28-02-05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sk@pskb.com', '89FFEF2C408C51E50163BFA1B943D8F1E21D7AD5', 
 '2010-02-15 02:36:08.000',True,False,'��������� �. �.','��� ��� �������� "�����������"','2539013067','1022500001061','(4232) 42-42-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'syromyatnaya@petro-plast.com', '30590F6C37860804F461BC3A4C84FE392DEF0D70', 
 '2010-02-15 11:01:17.000',True,False,'���������� ������ ����������','����������','7810213994','1037821036549','(812)- 327-43-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dbk@fritex.ru', '295AC1DBD3935F6E435C5B80E0727874AF811D9B', 
 '2010-02-15 11:26:30.000',True,False,'�������� ������� ��������','��� ��� "�������"','7606009999','1027600839815','8-4852-25-84-24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'raevskaya13@yandex.ru', 'C3D7C3016DE3B70E593EC2F21DF8C373FC9F8F87', 
 '2010-02-15 12:25:29.000',True,False,'�������� ������� ����������','��� "����" (���)','7702281122','1027739206692','(465)564-84-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ocb@sibank.ru', 'E20F163BDB30F18DE1033D30535B8DA2BA4A3914', 
 '2010-02-15 12:55:54.000',True,False,'������� ���� ���������','��� "�������������"','274061206','1020280000036','(347) 250-29-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Poluhinav@hq.nettrader.ru', 'F1EE9EE7AB1F71123D435DE30CB3584E1D54D163', 
 '2010-02-15 14:20:33.000',True,False,'�������� �������� ����������','��� "����������"','7705224891','1027700305742','495 589-10-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'l.ryndina@bfa.ru', '6E67BB7E950F5D38947F3BA6371E5FF2E90D9FC3', 
 '2010-02-15 17:00:10.000',True,False,'������� ��� ������������','��� "���� ���"','7831001408','1027800005199','458-54-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edo@nevuk.ru', '6C6F3C1409D5C7B0EBF676A3EBD3D9E14A123EDB', 
 '2010-02-15 17:22:23.000',True,False,'������ ���� ���������','�������� � ������������ ���������������� "������� ����������� ��������"','7707583543','1067746469757','(812) 320-53-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ashindauletova@alfabank.ru', 'C7DF62248DEFEB984DB0BEC014815A400243D0D5', 
 '2010-02-15 18:03:01.000',True,False,'������������ ���� ���������','�����-���� ���','7728168971','1027700067328','788-03-60-8558', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tatyana@ttr.ru', 'F7B8D180581F2EB07816BD9BEEC63B627379BC78', 
 '2010-02-15 18:44:21.000',True,False,'�������� �������','��� "������ �������"','7743008718','1027739159546','7690851', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tatyana@tatris.ru', '70FE238089ECFAC28D33C3F616FFE01C5C9B82C9', 
 '2010-02-15 18:58:36.000',True,False,'������� ��������','��� "�� ������"','7707069063','1027739129604','(495)7486868', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'avtobaza@bk.ru', '618A64AB47A36F70AD137D89F184B2D253A45E9A', 
 '2010-02-15 19:55:44.000',True,False,'������ ��������� ������������','��� ���','7723586284','5067746840695','495-799-1880', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'EPanasenko@lockobank.ru', '5F7F4D8D24E7B2EDE41D7C5B5558024D770C5FED', 
 '2010-02-16 09:32:04.000',True,False,'��������� ������� �����������','������������ ���� "����-����" (�������� ����������� ��������)','7750003943','1057711014195','7390729', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena_shendakova@troika.ru', '51D61D643D5932AD257964C02EFC8F1D13A9CA6E', 
 '2010-02-16 11:35:40.000',True,False,'��������� �.�.','��� "�������� ����"','7744002959','1027744007246','963 644 83 41', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oolga@spbex.ru', '1B295F29AC7674023378F837C2776F33B4574602', 
 '2010-02-16 11:58:46.000',True,False,'���������� ����� ����������','��� "�����-������������� �����"','7801268965','1097800000440','(812) 322-76-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@maritimebank.com', '014CFC40D74B5E11B1495655F273CE9BD430BF36', 
 '2010-02-16 13:11:34.000',True,False,'������������� ���� ����������','������� ���� (���)','7714060199','1027700568224','(495)777-11-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zolotova@unin.ru', 'BE91198C4583F0405147DBD4BCB48589DF0200AB', 
 '2010-02-16 14:37:03.000',True,False,'�������� ������ �������','��� "����������-������"','7727133824','1027739203062','499 243 03 55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'postbox@investbank.ru', '2AB5E8DB84C0258E177C0FA3F5ED88B23C5AA6A7', 
 '2010-02-16 14:53:26.000',True,False,'���������� ������� ������������','��� "����������" (���)','3900000866','1023900001070','(495) 411-6811 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ludmila@stinv.ru', 'E2C67D7369A26A6DD4FE8D176FD191AC9F6C6744', 
 '2010-02-16 15:29:01.000',True,False,'��������� ������� ����������','�������� � ������������ ���������������� "��������� ��������������� ����������"','5018110564','1065018034003','(495) 937-51-48', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lharkovskaya@ellcap.ru', 'B549DF3662068CEE5A123236E413E905F50F5C6F', 
 '2010-02-16 15:42:09.000',True,False,'����������� ������� ���������','��� "��������� �������"','7713635373','1077761767082','(495)739-29-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kolesnik@rts.ru', 'DE36B2394DE7A136D8A8F753F642B9EEB98860D1', 
 '2010-02-16 16:42:47.000',True,False,'����������� ������� ����������','�������������� ����������� �������� ����������� ����� ���','7712095220','1027700192651','(495)705-90-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'barmina@sopas.ru', '1F20A5F550585BC716F27ED80F6C15CABE1FBEFC', 
 '2010-02-16 16:48:15.000',True,False,'������� ���� �������','��� "�����"','7717094781','1027739313821','84957455455', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'xga@metib.ru', 'FF2A126623D349B3EE4BB9FDBF9FBCE6ECA48E65', 
 '2010-02-16 17:42:50.000',True,False,'������ ������� �����������','��� ��� ����������������','7709138570','1027700218666','7846951', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'POPOV@ROSBANK.RU', 'E9240CAC3AB86F7C6D698D950F26F422BFE48316', 
 '2010-02-16 19:22:35.000',True,False,'����� ����� ������������','��� ������� ���','7730060164','1027739460737','(495) 725-05-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'g.mironova@avbank.ru', 'FD4BECF2497FD0A53AD4F2EB3D567BA33621EC58', 
 '2010-02-17 09:02:25.000',True,False,'�������� �.�.','��� ���� ���','6320006108','1026300002200','(848 2) 40-72-04', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zaryankina@regnm.ru', 'D4D8C06ACE3BA2233A173FF58847127E15154EB1', 
 '2010-02-17 09:54:46.000',True,False,'��������� ����� �������������','��� "�� ������"','7708207809','1027708015576','(495)777-29-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'irinka205@yandex.ru', 'E701CE5180B94299DEB63027B02BE539B192BD54', 
 '2010-02-17 10:08:53.000',True,False,'����� ��������','��� "����"','7710519241','1037789015241','+7(499)795-7077', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'T.Guseva@rusinvestclub.ru', '2FCE5E070AC3D8E62208A253E8C32285C94B03EA', 
 '2010-02-17 11:48:01.000',True,False,'������ �.�.','��� �� "�������������"','5050045359','1035010221663','(495)411-62-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'k.serebryakov@intercapital.ru', '654E993F0B852953D707E3D76A4D8FC9365DB0D0', 
 '2010-02-17 12:52:53.000',True,False,'���������� ������ ����������','��� �� "������������-����"','7715000114','1027739299719','(495) 788-05-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'v.grozova@everest.ru', '6591959ADC4C14D545B992D3F2E5AE956AB92828', 
 '2010-02-17 14:43:23.000',True,False,'������� �������� ������������','�������� � ������������ ���������������� "����������� �������� "������� ����� ����������"','7707620354','5077746279463','(495) 798-55-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 't.bulakhova@everest.ru', '8714DD6A4AC9DCEA066AFB75A2B79BFF50DA6B1B', 
 '2010-02-17 14:47:55.000',True,False,'������� �������� ������������','�������� � ������������ ���������������� "�������������� �������� "������� ��������� ������" ','7707620322','5077746277604','(495) 798-55-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vlb@ellink.ru', '912C9794AF926FC6AC1484910A7DEBF5140AAD7F', 
 '2010-02-17 16:06:41.000',True,False,'�������� ��������� ����������','��� �� "������� ���� ����"','6025001470','1026000001774','8(81153)50957', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mazova@itb.ru', '005CCF8FFAE44A4C4A8FBF6F12474270D7D4F73D', 
 '2010-02-17 16:50:29.000',True,False,'������ �������� �����������','��� "��������������" (���)','7717002773','1027739543182','739-45-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@investdelo.ru', '0126D3CC704DE161C3893FC25CD66032DE8C4777', 
 '2010-02-17 16:56:28.000',True,False,'��������� �������� ��������','��� "�������������� ����"','7723605064','5077746297580','8(495)748-56-26', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'as@mostransbank.ru', '591ABC96C44BBEF8FD244AFD343A66FC6A13C97A', 
 '2010-02-17 17:13:32.000',True,False,'������ ����� ����������','��� "������������" ���','7718137822','1027739075407','8 (495) 797 55 42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'innaignatova@yandex.ru', '69902C5F95E6B3BF1DD592B89B89711721A44CC0', 
 '2010-02-17 17:16:00.000',True,False,'���� ��������','�������� � ������������ ���������������� "�������������� �������� "���������� �������"','7708633236','5077746450964','(495) 629-51-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jurde@vrk.ru', '9C7883FCEF75EE068A62375CC24AB9638631ACCD', 
 '2010-02-17 21:09:12.000',True,False,'��������� ����� �������������','�������� ����������� �������� "������� �������� ��������"','6661049239','1026605227923','8(343) 2830225', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'trust@tbhouse.biz', 'E47EF40BF88A56ABA068AACF92FE6C4958AD9292', 
 '2010-02-18 10:11:38.000',True,False,'������� ������� ������������','��� "������������� ����"','7707566361','1057748957265','(495)443-65-49', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'enkina@mmbank.ru', '62BFEA54626069CB1DF7F550ACC54C989737DDB4', 
 '2010-02-18 10:51:10.000',True,False,'������ ����� �����������','��� "���� ������"','7702000406','1027700159497','8(495)745-80-00 1172', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'askhab-ali@mail.ru', 'FB32EF6F65E3AACEB8EFD30F22012279ADC3D617', 
 '2010-02-18 12:20:16.000',True,False,'�������','��� "����� �������"','522013516','1040501302456','89634043170', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ovolkova@cig.ru', '4219099CC35DF45030BB4C0ABF6DD08FA8E472C8', 
 '2010-02-18 12:46:40.000',True,False,'������� ����� ��������','��� "��-��-��� ������"','7703632616','5077746704437','797-87-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ocb@vologdabank.ru', '5904A7B878D4FA8D6F3D90443AA961D00E8FB836', 
 '2010-02-18 13:07:31.000',True,False,'������� ����� �������','��� ���� "�����������"','3525030674','1023500000040','(8172) 72-33-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'EKirichenko@russ-invest.com', '3669A86B9E2E8AFE1FB28A726309EDB3E91E88E7', 
 '2010-02-18 15:15:28.000',True,False,'��������� ���������','��� "�� ����-������"','7704081545','1027739662796','363-93-80', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'osadchiy@iib.ru', '8001FF12DDB9EB696EE54AF6F118A0C0AF6A189A', 
 '2010-02-18 15:22:36.000',True,False,'������� ��������� ����������','��� "������������� ������������ ����"','7710409880','1027739543798','626-4446 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'funds@sovincom.ru', '4D69733882E318152CD2BF47AC7730EB5C0671D4', 
 '2010-02-18 17:25:15.000',True,False,'�������� ������� ����������','�� "��������" ���','7718053114','1027739055530','964-13-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '4437034@mail.ru', '970DFC5085DE4672E2B1904B506749B35CCD82E6', 
 '2010-02-19 04:23:07.000',True,False,'�������� ���� ������������','��� "���"','7731629005','1097746322255','79169380020', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'korneeva@ifgk.ru', '5B76E1B2D799365562007615CC99AD2A47983AEF', 
 '2010-02-19 14:47:29.000',True,False,'�������� �������� ��������','��� "�����������"','1326031292','1021300976817','8142 27-08-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'back-office@forshtadt.ru', '403B7162F97B4D89874B18FE7B16728F44980F70', 
 '2010-02-24 08:08:06.000',True,False,'��������� �.�.','��� "��������" (���)','5610032972','1025600000854','(3232) 980558', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@nkcbank.ru', '84123E46ED8CC871BF921F7C13A4CAC7855980B3', 
 '2010-02-24 11:04:41.000',True,False,'������ ������ �����������','��� ��� "������������ ����������� �����"','7750004023','1067711004481','7059666', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'repko-an@atomenergoprom.ru', 'F8D0B3F7CCEC431DF334FA7C93CA6C5F5654315D', 
 '2010-02-24 12:10:10.000',True,False,'����� ��������� �.','��� "��������������"','7706664260','1077758081664','(495) 969-29-39', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@profinvest.ru', '7D1EC2FE73FDD5E8B1CCA2D7530DCA71C5885468', 
 '2010-02-24 14:12:52.000',True,False,'������� ������ �������������','�������� ����������� �������� "����������������"','7452017122','1027403767346','(351) 247-73-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@str-cap.ru', '8DEDEBE2EF28C4545028B1F675956F965E7BA9B9', 
 '2010-02-24 14:18:47.000',True,False,'�������� ��������� �������������','��� "��������� �������"','7702673480','1087746601788','(495) 953-25-38 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@sobin.ru', '3FA35DED4EF9153D8441EA6F26ED8A663B2D0C73', 
 '2010-02-24 14:24:44.000',True,False,'��������� ������','��� "���������"','7722076611','1027739051009','(495)725-25-25 (5405)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'SmirnovAS@metcom.ru', 'FBC2A8608F2923AAD90E65DFE122FB1B21B8E145', 
 '2010-02-24 16:02:50.000',True,False,'������� ��������� ���������','��� "����������"','6612010782','1026600000195','(495)755-8094 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'slava@tatinc.ru', 'F9810A58B2F6D3210FDDBED91FB55A756247A9DA', 
 '2010-02-25 09:30:03.000',True,False,'�������� ��������','��� "������-����������"','1616010907','1021600814993','89172877070', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'august@fbaugust.ru', '906CA21875C9E78E75643B745D50A3DBA6126C69', 
 '2010-02-25 12:40:11.000',True,False,'��������� ������','�������� ����������� �������� "���������� ������ "������"','3445028704','1023403844100','(8442) 96 25 55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alexey_tatarchenko@troika.ru', '4ED26086692F15CEFE96C99A9175C054BF2FC232', 
 '2010-02-25 13:42:01.000',True,False,'���������� �������','��� ���� "������ ������"','7536002161','1027500001100','(495) 287-99-66', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sablina@kartixfc.ru', '49048BAED39C4B9F2FA2F740E38E30D00FAEEE1B', 
 '2010-02-25 14:07:57.000',True,False,'������ �. �.','��� "�� "�������"','7710526190','1047796031392','8-495-663-36-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'GHremuchkova@sviaz-bank.ru', '9B5C605885D5D32D64DB5723DB808EA8831CA4D8', 
 '2010-02-26 10:15:32.000',True,False,'���������� ����� ���������','��� ��� "�����-����"','7710301140','1027700159288','(495) 228-38-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena.sviridova@bnk.ru', 'F38A47BC95709EACE10D7AFAEBB0DD4CA378B0B2', 
 '2010-02-27 10:23:10.000',True,False,'��������� ����� �������������','��� ���������������','7722080343','1027739048204','7880880', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'marina@ricom.ru', '64F7AC53D22A3F19FE81EE7C5BE5E57B8F1037D6', 
 '2010-02-27 11:02:33.000',True,False,'�������� ������ �����������','��� "�������������� �������� "�����-�����"','7701033078','1027739075165','(499) 2415307', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Geoinvest.ltd@mail.ru', '3BCBD3F779B037318C7E26351B1849F8336CBF7C', 
 '2010-02-27 13:50:20.000',True,False,'������ ���� ����������','Geoinvest','7703655187','1087746121979','(499)9784411', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'markin@geprobank.ru', 'E2101B7F47724C641C3080A798D5504C1752ACD9', 
 '2010-02-27 14:06:52.000',True,False,'������ ������ ����������','��� "�����������������"','5003054042','1055000004751','666-32-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'karaseva@option.ru', 'AACAAB8B0C4AE4DECA7C359264E566C9F67494F4', 
 '2010-02-27 14:18:48.000',True,False,'�������� ������� ���������','��� "�������������-���������� �������� "������"','7719561946','1057747710790','(495) 645-87-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'strokanova@yugneftemash.ru', '5BB6D9B1A0D618AE306A2DFC64910E999B22C3C3', 
 '2010-02-27 15:10:09.000',True,False,'���������� ���� �������','��� "����������"','2310071742','1022301599968','(861)2344386', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Scherbakova@trust-uk.ru', '434959333D68F29E3362058669E35465CEB84D7B', 
 '2010-03-01 11:10:33.000',True,False,'��������� ��� �����������','��� "����� ���������� ��������"','1655159210','1081690035382','(843) 533-54-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tsenina@psbank.ru', '2C3FECE284805D75FD2FEABB11B055786F4358C5', 
 '2010-03-01 11:34:45.000',True,False,'��������� ����� ����������','��� "�������������"','7744000912','1027739019142','777-10-20 (70-2464)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'komissarova@finansec.ru', '13C5255E9BE5227BCF076766973ABF4233CA43F0', 
 '2010-03-01 13:00:46.000',True,False,'�������� �������','��� "�����������"','7733632860','1077763217157','8(495) 680 90 46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'darya.savchenko@gs.com', 'A80F6AD64A709455BB1F7115D8C72E79895D91CA', 
 '2010-03-01 15:01:04.000',True,False,'�������� ����� ����������','��� "������� ����"','7710619750','1067746339847','645 40 29', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bkrylov@smpbank.ru', '4E9DC67A20C39E56947750D4EBFB33FAD396BCCB', 
 '2010-03-01 15:16:19.000',True,False,'������ �.�.','��� "��� ����"','7750005482','1097711000078','+7(495)737-03-37 (1308)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ikstandart@ikstandart.ru', 'A852FE144C2D14A4B197656513BD863823D7DC1A', 
 '2010-03-01 15:21:12.000',True,False,'�������� ���� ����������','��� "�������������� �������� "��������"','7805060005','1027802732033','(812) 622-12-04', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'analitik@mail.kamchatka.ru', 'D9B187DAE6BB63D91018629CDE3A0B9AF3333E4E', 
 '2010-03-02 04:41:25.000',True,False,'������ ������� ����������','�������� � ������������ ���������������� "���������� �������� "�������� ������"','4101116760','1074101003393','8(4152)423513', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'popovanv@vtb24.ru', '012F669317A199EE93686FE74CA31720E5207A94', 
 '2010-03-02 11:02:22.000',True,False,'������ �.�.','��� 24 (���)','7710353606','1027739207462','9602424', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'v_kulakov@glenic.ru', 'E76157E4AEB10F0E9C78E64E5D9485EE461CA8CE', 
 '2010-03-02 14:02:58.000',True,False,'������� ������ ������������','��� "���������� ��� ������"','7703586173','1067746406452','(495) 253-25-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'L.Akhmetshina@tfb.ru', '8BEED676DDFD195F416A1609F0FE585E48700EAB', 
 '2010-03-02 14:06:08.000',True,False,'��������� �.�.','�������� � ������������ ���������������� "�������������� �������� "��� ������"','7722579502','1067746693574','(843) 291-98-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@bfa.ru', 'B844667E3B93D0FB23B3FD63A61551E595C17C7E', 
 '2010-03-02 14:19:52.000',True,False,'������ �������� ������������','��� "���������� ���������� ���������"','7810726001','1027804900287','(812)326-15-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bkof@dohod.ru', 'DC1DDD9D114DF99E96DDCDC64B5C2996ADF315EE', 
 '2010-03-02 15:14:02.000',True,False,'�������� �.�.','��� "�� "������"','7813067004','1027806881585','812 312-30-24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'migita.av@a-rnr.ru', 'AB8D7D25655EB852EFB6858721A5653CB58CC729', 
 '2010-03-02 16:10:57.000',True,False,'������ ������� ������������','��� "��������� ���"','7107039003','1027100964527','+7 (4742) 44-31-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ekaterina.g@original.su', '80C72857C7C51E5DF6CDDC06E2AB60DA61DA1251', 
 '2010-03-02 17:40:37.000',True,False,'�������� ��������� ������������','��� "��������"','7724728510','1097746768602','8-903-113-18-26', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dmitrieva@ittrade.ru', '0A195D9E13DB80F8CF65B4375C6717A524BEE1CC', 
 '2010-03-02 18:21:13.000',True,False,'��������� �.�.','��� "�� "�� �� ������"','7717116241','1027700010205','(495) 933-32-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ob-mu78@mail.ru', 'AAB5549CCD909D525539A88FCDAB5DF4D82BDE5E', 
 '2010-03-03 08:23:09.000',True,False,'�������� ����� ���������','��� "�� 78"','4501016680','1024500523585','83522-255289', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'b_office@moscowdebt.ru', 'C3D084621AA1017DF404BA21CF11ABF0691DE463', 
 '2010-03-03 11:21:40.000',True,False,'������� ����� ����������','��� "������-�"','7721046036','1027700053523','795-06-12 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tovm2009@gmail.com', '280603F305DB5CC7F69576471601967C9C69C48C', 
 '2010-03-03 12:18:45.000',True,False,'������� ����� ���������','�������� ����������� �������� "��� ��������"','7444022930','1027402051930','8-3519-205100', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ic-cc.ru', 'F829B8995A8FF58A079784A59F7A2B3122AAA730', 
 '2010-03-03 14:52:15.000',True,False,'���������� �����','��� "�� "����� �������"','7725639693','1087746714593','(495) 228-59-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kek@iftr.ru', 'CE1240052441756F042C89D3FF53A903DAC254BA', 
 '2010-03-03 15:18:13.000',True,False,'����������� ����� ��������������','��� �� "�������� �������"','7702158961','1027700065414','(495) 258-0721', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'drik@daevnet.ru', '1BE0EA90EE02D9B9F6F542F5A123FB1915A341A1', 
 '2010-03-03 15:21:10.000',True,False,'���� ����� �����������','�������� ����������� �������� "������������� ������������ �������������� �����"','5024028269','1035004453736','(495) 604-84-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Mikhailyk@sf�-finance.com', 'AAF2D97A2E3B1B4B4E58C3317A37AD5EE8ECBF96', 
 '2010-03-03 15:27:31.000',True,False,'�������� ������� �������������','��� "��������� ���������� ����������"','7729546577','1067746537847','(495) 933 51 25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@dcapital.ru', '1B4D3F96E66E9559CBCD6BC3D353FFDCE45ABA1F', 
 '2010-03-03 17:56:48.000',True,False,'������ ����� ������������','��� ���� "��������-�������"','7709345294','1027739067861','(495) 937-91-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bovda@lotus-capital.ru', '5F00BFF755F7FD43AB39FC47B0668B8D9799329E', 
 '2010-03-04 10:58:59.000',True,False,'����� �����','��� "�� "����� �������"','7710671655','5077746684450','(495) 580-53-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@applegreen.ru', '9D71B6ADE6A65CDE483C229B43EC25DDBBD22212', 
 '2010-03-04 11:00:55.000',True,False,'�������� ������� �������������','��� "�� "�������"','7722509512','1047796174975','(495) 641-31-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kontrol@rcnikoil.ru', 'A56223D8550FC26BA765F0586F42201B8C5CA460', 
 '2010-03-04 11:18:33.000',True,False,'��������� ����� ���������� ','��� "����������� ������"','7730081453','1027700060607','495-755-90-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Fedotova@gencapital.ru', 'CF57CB16FE39322E8E502063EE1A27C62D66F8D5', 
 '2010-03-04 15:39:50.000',True,False,'�������� ����� ��������','��� ����������� �������� "����������"','7704521404','1047796381445','+7 495 661 55 66', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'BobkinaEV@profrc.ru', 'B4AD66A30EBD3A9F03E95DC9EA6C9BA566E00FAF', 
 '2010-03-04 16:23:04.000',True,False,'������� ����� ����������','�������� ����������� �������� "���������������� ��������������� �����"','3821010220','1023802254574','(495) 229-38-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kolenkova@novikom.ru', '9DD810D3591866AA8A7A860531311909FF24BECB', 
 '2010-03-05 07:33:51.000',True,False,'��������� ����� ����������','��� ��� "�����������"','7706196340','1027739075891','(495) 974-71-87  ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alexei.ezhov@hsbc.com', '78750B2E95B21313376CC9E24404694305124CB0', 
 '2010-03-05 08:56:16.000',True,False,'���� ������� ����������','��� "���-��-��-�� ���� (��)"','7707115538','1027739139075','74957211517', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm@fb-troika.ru', '13A66C60B2DB0A464951DF826AB48BAB05B0BC4E', 
 '2010-03-05 09:13:36.000',True,False,'�������� ����� ����������','�������� ����������� �������� ����������� ������ ������� ������','7710165634','1027739007537','NULL', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nikitina@brok.ru', 'C65CD4B99D7A40D5FF05250B430E5010E226A1AC', 
 '2010-03-05 09:52:07.000',True,False,'�������� �.�.','��� "�� "�����������������" ','7825004143','1027809181069','325-96-96 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'irina.krush@ingbank.com', 'B7141B999EF850A4C693ABF4400FC7FC568271D9', 
 '2010-03-05 11:13:42.000',True,False,'���� ����� ���������','��� ���� (�������) ���','7712014310','1027739329375','(495)755-54-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Isafonov@gebank.ru', 'A36265C177BD49C0C3088E9FF4730714E7906E5C', 
 '2010-03-09 07:54:00.000',True,False,'������� ���� ��������','��� "�������������"','4026006420','1024000000210','8 (4842) 53-16-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@mixfinance.ru', '8B8CAD2F0E48D6263146D2338CA61C82B6C14D6A', 
 '2010-03-09 09:44:26.000',True,False,'�������� �������','��� "����������"','7705212310','1027739371010','74957485676', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'newreg@newreg.ru', '95EB04CC246AEEE012940BA51A10E12EFC806059', 
 '2010-03-09 12:03:24.000',True,False,'��������� ����� �����������','�������� ����������� �������� "����� �����������"','7719263354','1037719000384','(495) 964-22-56', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'SKurygina@prospect.com.ru', '52857F991FA58C26BEF4271E87372FD49EA8E5C1', 
 '2010-03-09 12:20:56.000',True,False,'�������� �������� �������������','�������� ����������� �������� "�������������� �������� "��������"','7711073471','1027739113687','8(495)937-33-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nikolay@romanenko.ru', '3CD7ADD9665A60A8DDE3247A1702CF09EC032FE6', 
 '2010-03-09 13:05:39.000',True,False,'��������� ������� �������������','��� "������ ����� ���"','1646027023','1101674000405','79033061039', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vefimov@rambler.ru', '08B4D0161A1B228F3E22F5464146C2E7D8678CA0', 
 '2010-03-09 13:07:11.000',True,False,'������� ����� �������������','��� "����������"','7728501735','1037789073816','921-43-39', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'adavidov@univer.ru', '22F8C5EC25B892BE775C60175900DF75998DAF98', 
 '2010-03-09 13:51:14.000',True,False,'������� ��������','��� "������ �������"','7704612010','5067746134760','(495) 792-55-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'eib@arz6.ru', '1D0B3AB040706C54DB5FE9A7F0EF02EC79FB8A6C', 
 '2010-03-10 06:25:52.000',True,False,'�������� �.�.','��� "���-6"','7701007504','1027700327885','(495)788-60-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kazn@bankmrb.ru', '31B6829CD8879AFFBF24BFAE606DD5AB0D1EE17A', 
 '2010-03-10 06:47:41.000',True,False,'������� ������ ����������','�� "���" (���)','2112001380','1027739435668','4959211994', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mtv@jvp.ru', '28280F57A78886155901011068901DE74B432699', 
 '2010-03-10 08:30:04.000',True,False,'�������� ������� ����������','��� "���� �� �� ������"','7730542672','1067746697292','(495) 510-53-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'medzhevskaya@mfcb.ru', '7D125218110DFF215152149864164E3181A24180', 
 '2010-03-10 13:38:17.000',True,False,'���������� �.�.','��� �� "�������"','8603001619','1028600002199','(495)-958-08-87(89)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'inbox@mikf.ru', 'A743DB8F8CA7DCE998878C8624B5B1ECF7B181D0', 
 '2010-03-10 13:51:06.000',True,False,'������ �.�.','��� "�������� ����� "���-�"','5029038831','1025003519310','(495) 708-42-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@depocent.ru', 'BEA92AC9355C94D33F6F3AB3DAAE87316EEE7465', 
 '2010-03-11 09:20:33.000',True,False,'����������� ���� �����������','��� "�������� ������������ �����"','1435081832','1021401046182','(4112)32-73-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'msa@apkbank.ru', '4B7D38ED5F11DC7A499D33A9E66CEFD20D3BC83C', 
 '2010-03-11 13:03:41.000',True,False,'��������� �������� �������������','��� �� "��������������"','5026014060','1095000004252','7392005', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'veks@tarkhanybank.ru', '431772706BAF6B8275C0859C49BE1DF2515DC35A', 
 '2010-03-11 15:19:11.000',True,False,'���������� ������� �����������','��� "���������� ���� "�������"','5834004442','1025800001050','(8412)56-07-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm@troika-am.ru', '13A66C60B2DB0A464951DF826AB48BAB05B0BC4E', 
 '2010-03-11 15:41:35.000',True,False,'������� ������� �������������','�������� ����������� �������� ������������ �������� ������� ������','7710183778','1027739007570','(495)258-05-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@mcd.ru', '7C91FB698ACFEA5E674C1A20CCDC522D9E3F06C5', 
 '2010-03-11 16:28:58.000',True,False,'������� ������� ����������','�������� ����������� �������� "����������� ���������� �����������"','7708047457','1027700095730','8 (495) 221-13-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'backoff@ifgit.com', 'E7AC62A92C5438FBD6EA85858A6167887A187769', 
 '2010-03-12 11:42:51.000',True,False,'��������� ������� ��������','�������� � ������������ ���������������� ������������� ���������� ������  "������-�����"','7715697872','1087746532532','8(495)638-57-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'djur@tavrich.ru', '94A7748C906E4982532C744959D7C7D9351EAFEC', 
 '2010-03-12 12:46:55.000',True,False,'�������� ������� ��������','���� "�����������" (���)','7831000108','1027800000315','(812) 329-55-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Chernysh@mfc.ru', '8E2DC7A6460305961F0FCDDDCA8E35ABE8488429', 
 '2010-03-12 14:14:56.000',True,False,'������ �������� ���������','��� "���������� �������� �����"','7729138539','1027739035796','(495)755 55 07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dealing@irdag.ru', 'B405C85F9D08372AFF9A12C01928EB780DF8B660', 
 '2010-03-12 14:23:56.000',True,False,'�������� �.�.','��� ��� "��������"','541015808','1020500000619','8(872)78-08-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gko@acropol.ru', 'FD3F2DD253F36D2391AA76351602DD1F8A6AB5B5', 
 '2010-03-12 17:40:21.000',True,False,'���� �.�.','�� "��������" ���','7750004168','1077711000091','(495) 253-65-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'staribor@mail.ru', '53502DC123D8CCB650F13CA9EE4816457001BA0D', 
 '2010-03-15 03:44:45.000',True,False,'��������� ��������� ���������','��� "��� �� "��������"','5504120698','1065504056100','8-3812-58-09-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Babkina@open.ru', '889BB33A80EFD089CF2D958F09D12C3F4C7421E7', 
 '2010-03-15 08:39:52.000',True,False,'������� ��������� ','��� ���� "��������"','7744003399','1037711013295','755-88-67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'plechko@ailine.ru', '8E695D792AB7F407F849B448980C1812D73F75E7', 
 '2010-03-15 14:11:26.000',True,False,'������� �������','��� "���� ����"','7701253651','1027739558956','777-88-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bizexp2004@mail.ru', '16DBEF24C3CA6695CCE1EBEDBB1A7F2A9E961439', 
 '2010-03-16 05:31:26.000',True,False,'������� ������� ���������','��� "������-�������"','2225039297','1022201759018','8 (3852) 668807', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vgrica@yandex.ru', 'CA5186416463D881F01B11396889D85524E71396', 
 '2010-03-16 08:38:10.000',True,False,'����� ��������� ������������','�������� ����������� �������� "��������� ����������"','3814014054','1083814001754','8 395 54 3 13 32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kotl@gr-invest.ru', '74AEE5E432517545D28DE1E7D28CD4D92F3A59C5', 
 '2010-03-16 09:11:34.000',True,False,'�������� �.�.','���"�������������� �������� "�������"','7703012130','1027739332400','(495)623-9902', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'safronova_MV@nomos.ru', '07E588F4D4A26C1004CDBA35D2F991112ED6CBEF', 
 '2010-03-16 09:31:06.000',True,False,'��������� ����� ������������','�����-���� (���)','7706092528','1027739019208','737-73-55 (4745)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@gutabank.ru', '25F50042C3C238C22BA6ACBC2DADBA1DF37EE1B3', 
 '2010-03-16 11:13:59.000',True,False,'�������� ������ ����������','��� "����-����"','6905011218','1026900005307','(4822) 49-48-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ifk_sheldi@rambler.ru', 'C6D13A8D549609B3F45E351EFCF24ABCB9DCAF7D', 
 '2010-03-16 12:00:03.000',True,False,'������� ����� ����������','�������� � ������������ ���������������� "�������������� �������� "�����"','7604077009','1057600603884','(4852) 30-36-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vako@mart.ru', '227AB75C7187FA687E1A28DDD9443ECB72BC503F', 
 '2010-03-16 16:30:57.000',True,False,'������� ���� �����������','��� �� ��������','6025001487','1026000002160','(81153)51656', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oofr@bspb.ru', '1F20A5F550585BC716F27ED80F6C15CABE1FBEFC', 
 '2010-03-16 17:00:28.000',True,False,'�������� ������� ������������','��� "���� "�����-���������"','7831000027','1027800000140','-1257', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'IvaninaA@khmb.ru', '35363F925C87050FD8BEE561A6A0DFBD0C355E10', 
 '2010-03-17 09:06:55.000',True,False,'������� ���� ����������','��� �����-���������� ����','8601000666','1028600001880','8(3467)390-717', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'llositckaya@oceanbank.ru', '67353CA41DB749042DD9417E56DB56AB3021C00E', 
 '2010-03-17 12:18:21.000',True,False,'�������� ������ ����������','����� ���� (���)','7744002356','1027739326933','(495) 981-55-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rafael@intraco.ru', '1BB5E10E69EB3E9117B6513C877D6CDF555040A9', 
 '2010-03-17 14:55:57.000',True,False,'����������� ������� ��������','��� "����������� �������"','5903027161','1025900763063','(342) 233-01-63(64)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ovchinnikova_n@region.ru', 'E0F97B9672A43AA6A3B1D58C9C789621D5442ED9', 
 '2010-03-17 15:47:55.000',True,False,'����������� ������� ����������','�������� ����������� �������� "������ ����� ����������" ','7730149408','1027739046895','777-29-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kuznetsova.natalia@nevcos.office.ru', 'F53F1DBFA0D1C5340DF9E762322D75C5F9030736', 
 '2010-03-18 08:31:10.000',True,False,'��������� ������� ����������','��� "������� ���������"','7811038047','1027806078893','(812)320-51-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Schetilova.GA@obibank.ru', '62B62C102840C26CC7A6CFA39EC93ADE8ADFEA39', 
 '2010-03-18 13:58:42.000',True,False,'�������� �.�.','��� �� "�������"','7708013592','1037739028678','(495) 933-33-23 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pelih_sf@sibgazbank.ru', '00A4C06CE2085B50F67AB57716E684E17562DE40', 
 '2010-03-18 14:36:50.000',True,False,'����� �������� ���������','��� ��� "�������������"','8602190219','1028600001968','(3462)502502', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@connectfinance.ru', '96989041791B0048B80659C3D60E5F4C75D343D4', 
 '2010-03-18 15:39:26.000',True,False,'��������� ������ ����������','�������� � ������������ ���������������� ����������� �������� "������� ������ ����������"','7716536003','1057748847331','+7 495 792-50-85', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@efcapital.ru', '86DFB9A1C633409015C28C1E481BE783B9C0D615', 
 '2010-03-18 15:47:23.000',True,False,'��������� ������ ����������','�������� ����������� �������� "����������� �������� "���������� �������"','7704629704','1067761451460','+7 495 792-50-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'o_timoshina@axi.ru', '23D6324233E8D81CE51C93994390D56EE0E0DDE1', 
 '2010-03-18 16:08:39.000',True,False,'�������� ������ ������������','��� "�������"','7717054595','1027739049766','(495) 780-97-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'as@blackfieldcapital.com', 'D656573AC1447C3E91A046C8EB4E4153F70BD2EE', 
 '2010-03-19 10:56:15.000',True,False,'��������� ��� �����������','��� �� "�������� �������"','7702714344','1097746524842','89169009800', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tsiglyuk@ibsp.ru', 'DE367AEE8E0408FE41EF16FA9D52F4D4EC89BE7A', 
 '2010-03-19 12:18:00.000',True,False,'������ �.�.','��� "����"','7831000210','1027800001547','542-09-41', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alex@fdom.ru', '923B447AA0A8D0C65266FF5890EAAFDC4D13BBF7', 
 '2010-03-19 12:35:58.000',True,False,'������� ������� ����������','��� "�������� ����"','5902114608','1025900512373','(342) 2383502', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ati@aticon-invest.ru', '8414D524A2C4D1F7D13D914ADA06D52E1F61449F', 
 '2010-03-22 09:59:49.000',True,False,'������� �.�.','��� �� "������"','6903023112','1026900529908','(4822) 760108', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'O.Semina@roseurobank.ru', 'CF646FFF7AAB90CEE48BBECDFF83D461C2E14CFF', 
 '2010-03-22 14:30:24.000',True,False,'������ ����� ����������','��� "�����������" (���)','7701219266','1027739326757','7771111-3238', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yk@mmbank.ru', '5DE9946D0D2DC21C163938E264E6552E1BA6D475', 
 '2010-03-22 14:43:26.000',True,False,'��������� �.�.','��� "�� ����� ������"','7710211626','1027739005700','(495) 925-80-00 (20-602)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zaira@sembank.ru', '39D74838F6E949D8B60F4883FDBEC8FA04742352', 
 '2010-03-22 15:02:36.000',True,False,'������� ����� ������������','��� �� "�������"','7730049675','1027739475378','(495) 995-95-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sitek_invest@mail.ru', '5B7E15476A5B24FC0DE0A444A2C84224EB2D01F5', 
 '2010-03-22 17:39:54.000',True,False,'�������� ������ ���������','��� "����� ������"','7718554350','1057747538067','-1371', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sandreva@ukrfc.ru', 'EB66360A14F2CBB5A29E9E7D41640FA45AAAED2D', 
 '2010-03-23 10:30:01.000',True,False,'�������� ������� ����������','��� �� "���-�������"','7444036805','1027402052347','(3519) 25-60-26', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yulia.kuzmina@gazprombank.ru', 'BC5321684BE851B816F61E1D427FD3829C96D472', 
 '2010-03-23 11:46:11.000',True,False,'�������� ���� ����������','��� "������� �����������"','7727223267','1027727002467','(495)332-77-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'binfo@maxwell.ru', '04992D16D675AF5A2E6E5DC7BCA6126673B814D0', 
 '2010-03-23 12:22:35.000',True,False,'�������� ��������� ����������','��� �� "�������� �������"','7736207053','1027700314597','(495) 782-88-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga.yakovleva@gbm.ru', 'EE4CDA83F2C8E15F2AAA54DC3EA64B266328EB38', 
 '2010-03-23 12:27:14.000',True,False,'�������� ����� ����������','�� "������� ����-������" (���)','7703120537','1027739041406','+7(495) 961-25-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'costa@ya.ru', 'C945D217A0118984248AE48B9A5A51E87E389D99', 
 '2010-03-23 13:52:05.000',True,False,'������ ���������� ��������','��� ����������������','6163074306','1056163028910','8632638305', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bobko@vitus.ru', '1075A9E804CEEB77D6E170A71FC2A74D7F82B3CB', 
 '2010-03-23 15:44:03.000',True,False,'����� ����� ����������','�������� � ������������ ���������������� "�����"','5906037545','1025901364686','(342) 218-40-39', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk@vitus.ru', '1075A9E804CEEB77D6E170A71FC2A74D7F82B3CB', 
 '2010-03-23 15:47:05.000',True,False,'������� ������ ������������','�������� � ������������ ���������������� "����������� �������� "�����"','5902131890','1025900509062','(342)218-40-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'diascon@mail.ru', 'ED85DB7132B83C449E8795C54A206780205DEA52', 
 '2010-03-24 06:22:27.000',True,False,'��������� ������� ����������','��� "���  "�������-������"','1435117704','1021401047458','(4112)482133', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'valentina.kudashkina@trust.ru', '99D5797848305E3AED32C977A12E92575FC5D0B1', 
 '2010-03-24 10:22:41.000',True,False,'��������� ��������� �����������','�� "�����" (���)','7831001567','1027800000480','(495) 926 96 09', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@leader-invest.ru', '6947D946FAB99A8891DDB6230887FBB8C7D558ED', 
 '2010-03-24 11:04:25.000',True,False,'���������� ������� ������������','��� "�����"','5018026672','1025002040250','(495) 661-45-35', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buu@etrust.ru', '13C2413C2F0C1CD66E44279E49B92E9DCCDA5839', 
 '2010-03-24 14:18:07.000',True,False,'����� ���� �������','�� "���������" (���)','7744000334','1027739154497','(495) 745-59-70 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@gardinvest.ru', '3E597E7022D55441D7A56DDAB5F0785BD51174CD', 
 '2010-03-25 10:22:35.000',True,False,'������ �.�.','�������� � ������������ ���������������� "�������������� �������� "��������� ����������"','7713287260','1037739045464','(495) 234-21-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'skudlaev@bmfn.com', '23CEE62CCEBFC4AB3740B653F394BF1E83EB901A', 
 '2010-03-25 11:21:42.000',True,False,'������ �.�.','��� "������ ������� �������� ���"','7723598635','1077746077529','(495) 640-1423', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'OKohanovskaia@corp.aoreestr.ru', 'EC055798131B34F808E8699979993C40E269DB39', 
 '2010-03-25 12:29:20.000',True,False,'����������� ����� ����������','��� ������','7704028206','1027700047275','(495) 617-01-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mbk@spartakbank.ru', 'A69D55951E660D9E68D0540937D1744B310BBEA6', 
 '2010-03-25 12:45:20.000',True,False,'�������� �����','��� "�������������"','7744002042','1027700051390','495-380-19-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm@bogatyri-am.ru', 'D9A32533E839E6C59A013B0845DC6FB3A80B1E84', 
 '2010-03-25 13:22:58.000',True,False,'������ ����� ������������','��� ������������ �������� ���������','7705470030','1027705016536','(495) 518-6858', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kodyaeva@bnkv.ru', '9272FA84E8C0F6224BA2CE8147728B7781C6FE36', 
 '2010-03-25 14:51:35.000',True,False,'������� �������� ����������','����������� ������������ ���� "���� �� ������� �������" (�������� ����������� ��������)','7708005552','1027739045025','(495) 777-10-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'boyko@evrazbank.ru', '209E1E032D2D216C75DDE4FAB59EACC6091C37F6', 
 '2010-03-25 15:32:39.000',True,False,'����� ����� �������������','��� "���������"','7704013560','1025000002015','(495)510-10-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'I.epifanova@rostbank.ru', 'ABEF3EB2F9188D595CD14C7949B787E3CFB032DD', 
 '2010-03-25 18:00:36.000',True,False,'������ ���� ������������','��� ���� "����"','7702131303','1027739178972','(495) 988-33-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'laptieva.ekaterina@altabank.ru', '4BB1594AA927765A3D1122F2B96F046A604EB52E', 
 '2010-03-26 15:58:40.000',True,False,'�������� ��������� �������������','��"�����-����"(���)','7730040030','1027739047181','(495)673-54-00(181)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'eldoc@yfc.ru', '718C14E578245A52D4619A9E7C5A09D22A2C738B', 
 '2010-03-29 06:31:33.000',True,False,'��������� ������� ���������','��� ��� "�������� �������� �����"','1435001668','1021401046160','8(4112)335700', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'back-office@finrise.ru', '4A0012E0C472C27ABFD355C56D3106874D096517', 
 '2010-03-29 11:47:20.000',True,False,'������� ����� ����������','��� "��� "�������������"','1901021632','1021900523941','(495) 543-45-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ess29@yandex.ru', '942BC4FA3CD3B0E702EB380D6BDA4713F3E27414', 
 '2010-03-29 13:19:47.000',True,False,'������ �������� ����������','��� "�����������"','5053056542','1095053000855','8(495)789-97-57', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'official@fincor.ru', 'C6F7E76CFCA53EFA6A19196F497E2F4D44075D63', 
 '2010-03-29 14:25:28.000',True,False,'������� ������� ����������','��� "���������-������������� ����������"','7702165310','1037843049320','(812)6999999', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'morozova@mcombank.ru', 'C5888EC61397C56583ED04AF6D2C49F66F1DCBB9', 
 '2010-03-29 14:47:19.000',True,False,'�������� ����� ������������','������������� ������������ ���� (���) ','2465029704','1027700053776','(495) 748-53-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@mytroika.ru', '13A66C60B2DB0A464951DF826AB48BAB05B0BC4E', 
 '2010-03-29 14:49:18.000',True,False,'������� ����� ����������','�������� ����������� �������� �3D�','7723173826','1027739258843','(495) 725-27-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo-bank@mail.ru', '6100B89F0B50DAD7B83F71DEEB811D278F1CC2F0', 
 '2010-03-29 15:44:54.000',True,False,'������� �������� ����������','����������� ������������ ���� "�������-����"','7704012291','1037700006684','8 (499) 255-70-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'burneiko@4dim.ru', '4152DDB05F7BD3CDD1C115E3DCE4EC47CFE3265F', 
 '2010-03-29 16:23:34.000',True,False,'�������� ������ ���������','��� ��� ��������� ���������','7801105343','1027804909241','(812)324-22-74 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@primebroker.ru', 'E420B6B88C6098A1ECA4596CCD601EDE69216514', 
 '2010-03-29 16:38:30.000',True,False,'��������� ���� ���������','��� "������� ���������� ���"','7724527732','1047796884651','8 (495) 650 09 40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lugovoy@vladimir.minbank.ru', '40ADCE67B28063586DD3EBA95DCB604D4412A9EC', 
 '2010-03-30 08:17:42.000',True,False,'������� ������� ����������','�������� ����������� �������� "���������-������"','3302021034','1023301289153','(4922) 423559', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rr@donpac.ru', '51AA16CA4042B89B0DB93BB4C28A307B80DBB4EE', 
 '2010-03-30 11:26:00.000',True,False,'������� ��������� ������������','��� "����-������������ �����������"','6166032022','1026104025716','(863) 252-68-74', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tatb9naolegovna@mail.ru', 'E9C6BD8B5C3C06570334458D7E5ED5D5B2986E65', 
 '2010-03-30 11:39:08.000',True,False,'��������� ������� ��������','�������� ����������� �������� "�������� - ������������ ���������� �� ������������ ������������ � ������� ������ � 4"','7839015794','1047836021342','(812) 575-45-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'valeria@azimut-capital.ru', '39AD10E8CCCE5F69D193EE3E800928B1CDBE09BC', 
 '2010-03-30 12:08:22.000',True,False,'�������� ������� �������������','�������� ����������� �������� "������-�������"','7702595626','1067746313920','(495) 745-95-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@olymp-capital.ru', '76CD5895EE36089360BCBEC3F6C1FFD91191ACD3', 
 '2010-03-30 15:59:58.000',True,False,'��������� �.�.','��� "�� "����� �������"','7721628789','1087746898227','(495) 982-35-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'L.Apalkova@voz.ru', '48CC121F11E75B98C07E197B66E9E3D60E64CDC3', 
 '2010-03-31 09:36:20.000',True,False,'��������� ������� ����������','���� "�����������" (���)','5000001042','1027700540680','(495) 620-19-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'u.zamula@makbank.ru', '7744A03FF6DEA248D46EC0D608EF7E73F680BE36', 
 '2010-03-31 11:08:26.000',True,False,'������ �.�. ','��" ���-����"(���)','1433001750','1027739534052','8(495)745-80-58', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'staruhina@priscocb.ru', '1931266E6AA0F5EB0DE0615E14467B31323DDFC3', 
 '2010-03-31 11:58:38.000',True,False,'���������� ������ �������������','�� "������ ������� ����", ���','7713029647','1027739700548','+7 (495) 775 6524', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ldd.ru', 'DEBF378A17986326CF036F2E74F0246314C42071', 
 '2010-03-31 13:01:40.000',True,False,'��� ������ �������','��� "���"','7723704185','1097746022220','(495) 623-96-30', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sereda@strat.ru', '55310E85E2147ADEDBADBF27E1C1038457002945', 
 '2010-03-31 14:31:39.000',True,False,'����� ���� ��������������','��� "���������" (���)','7727039934','1027739199355','(495) 380-14-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ifantidi@m2mbank.ru', '7BCD70BFC3CB683A12C5218DBD08D7F7B6B6D3E9', 
 '2010-03-31 15:16:07.000',True,False,'�������� �.�.','�2� ������� ����','7744001320','1027739049370','(495) 223-223-5', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'deposit@masterbank.ru', '5126E5EC9E783A4E6C3EC7DD9C881B8328F22062', 
 '2010-04-01 13:30:30.000',True,False,'�������� ������� ��������','������-���� (���)','7705420744','1027739049304','956-2211 (1848)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'backperesvet@bank-peresvet.ru', 'C73D4755031C2CEE47A4695A7ABAABFB975CCC10', 
 '2010-04-01 14:42:59.000',True,False,'������ ������� �������','��� "��������"(���)','7703074601','1027739250285','974-82-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'l_novikova@eltrast.ru', 'BE92C48D067712E2B0F2B4C05E5591358B71F7B1', 
 '2010-04-01 14:53:47.000',True,False,'�������� ������� ��������','��� �� "�����"','7812018149','1027809194984','(812) 380-30-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'system@mcd.ru', '720048FF1197E4FB32A095C5CC573C2BA3E4C52A', 
 '2010-04-01 14:57:23.000',True,False,'������ ����� ����������','�������� ����������� �������� "������� �������" (��� "������� �������")','7725221708','1037725008562','(495) 221-13-17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'melzak@sbank.ru', '1FF2F10B5E0B423179487E7057650411F5BF7E43', 
 '2010-04-01 15:41:48.000',True,False,'������� ����� ����������','�� ���� (���)','7723008300','1027739177091','+7 (495) 777 1121', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'emelyanenko.ee@intercomm.ru', '9FF5376C245AC51E2D1DF33B4FC6FD28F4709D35', 
 '2010-04-01 15:50:25.000',True,False,'����������� ��������� ����������','�� "������������" (���)','7704045650','1037700024581','+7(495) 380-22-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga.zharkova@bnpparibas.com', '3FE44211766D7196E3052B56C54EA5DA4B68D646', 
 '2010-04-01 16:03:20.000',True,False,'������� ����� �������������','�� "��� ������ ������" ���','6452010742','1027739664260','(8-495) 729-52-00 (12-277)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'saozara@skr.ru', '0CA22D6A85036B1E9BB87CDF87283104AE16C1E7', 
 '2010-04-02 04:53:20.000',True,False,'������� �������� ���������','���"������� ����"','2615001080','1022602825046','88654456144', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'l.titarenko@sdco.ru', '60BFC18C1177872BE3E744B382440BA85F787204', 
 '2010-04-02 05:09:00.000',True,False,'��������� ������� ��������','�������� ����������� �������� "��������-������������ ��������"','7725117665','1027739057840','(495)428-46-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'E-Trade2010@yandex.ru', 'DDC91100A07F96915872AFDC1C6408B215C63EF2', 
 '2010-04-02 08:54:43.000',True,False,'��������� �.�','��� "�������� ��������"','7731532846','1057748718785','(495) 213-32-38', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'general@cbrastro.ru', '64BEF8CB2558E164A7581E739F51EC70CF7A0CEE', 
 '2010-04-02 10:00:06.000',True,False,'������ �.�.','��� "��-������"','7723012723','1027739036654','913-51-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sinyukov@severgazbank.ru', 'A937834BCFF4B3C34B7CCDB58345182F109F95A0', 
 '2010-04-02 11:50:04.000',True,False,'������� ������� ����������','��� �� "������������"','3525023780','1023500000160','(8172) 57-36-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'filyanova@ns-bank.ru', '678104EBB6079E66714A152F82E6C19D27B3F306', 
 '2010-04-02 12:25:44.000',True,False,'�������� ������� ����������','��� �� ������������ ��������','7744002807','1027744002670','725-59-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ohromova@amibank.ru', '42F4ABBE51665F26877558015E9DC27F989DBFB5', 
 '2010-04-02 13:28:39.000',True,False,'������� ����� ����������','��� "���-����"','7744001419','1027739495420','(495) 981-44-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tsvetkova@mybank-group.ru', '53A6EFE27DD0317C7B5324BC6CECC956D73DD5DD', 
 '2010-04-05 07:06:24.000',True,False,'�������� ����� �������','��� "��� ����"','7714014756','1027739672300','-723', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'cmv@mab.ru', '41AB1020BC3F6F9DCA9E549AD2044480C5C51163', 
 '2010-04-05 08:35:48.000',True,False,'�������� ������ ������������','�������� ����������� �������� "������������� ����������� ����"','7703025925','1027739097165','(495) 967-86-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'otc_pf@aibank.ru', 'F99050258285AAA723F10A80BC286D0214F12E3B', 
 '2010-04-05 08:45:38.000',True,False,'�������� ����� ������������','��� �� "�����-������-����"','7717011200','1037739058609','(495) 780-54-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'donfao@donpac.ru', 'AC56594AE9146681BD491593B4C7E0E0A34604A9', 
 '2010-04-05 10:44:27.000',True,False,'������� ������ �����������','��� "����������� "������"','6163020692','1026103159653','(863) 269-88-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Ludmila.pinchuk@ru.natixis.com', '3228ECA7B92ED6678C1416FC25BD21BB0C3B0437', 
 '2010-04-05 10:55:22.000',True,False,'������ ������� �������������','�������� ���� (���)','7744001810','1037739058180','787-17-17 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm@russobank.com', 'C925454224A847ED036BAF996BF0CE5DFDAC0D5C', 
 '2010-04-05 18:44:42.000',True,False,'������� ����� �����������','��� ��� "���������"','7704099052','1027739081930','(499)241-1395', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'salnikova@siab.ru', 'DFFBDAA4CE5A32C3E678BD9F13B8170C86634E5B', 
 '2010-04-06 11:16:59.000',True,False,'���������� ����� ��������','��� "����"','2465037737','1022400003944','(812)3808130', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'eduard@draga.ru', 'FC270423CDE0EC207CBFA212B11BAEC6DDAD255F', 
 '2010-04-06 11:47:39.000',True,False,'������ ������ ���������','��� "�� - �����"','7704011964','1037739162240','(495) 7191514', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bmv@bpf.ru', '28106B25B7CC41CBB81C9C16EF104D6EF0929325', 
 '2010-04-06 11:54:17.000',True,False,'�������� ������ ����������','�� "���" (���)','7719038888','1027739042572','99-55-100', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ocb@mibank.ru', 'ECD2AF91F36ACD585EBDEDEE66138947C8FF550A', 
 '2010-04-06 12:17:34.000',True,False,'�������� ������� �������������','������������� �������������� ���� (�������� ����������� ��������)','7744000609','1027739030725','8-495-932-98-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sna@iftr.ru', '92AD71DA219C4D0A2561C7B950C2D9E9B3AEEB54', 
 '2010-04-06 12:50:03.000',True,False,'��������� ������� ����������','�������� ����������� �������� "���������� �������� "�������� �����"','7701254863','1027739032870','(495)755-87-48', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Yaroshevich_OA@servis-reestr.ru', '6EB85C7924C7CE3895F2A74D3F5E43277820AA0A', 
 '2010-04-06 13:57:53.000',True,False,'�������� ���� �������������','��� "������-������"','8605006147','1028601354055','(495)783-01-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.smirnova@sngb.ru', '8E66A5FDF9404BDAFA4E56337FA0B8B12188D546', 
 '2010-04-06 14:08:27.000',True,False,'�������� �.�.','��� "����"','8602190258','1028600001792','8-3462-39-87-44', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'broker@vsb.ru', '31431EE08ED02DB4D084A669992FA5EC74ECCCD2', 
 '2010-04-06 16:25:10.000',True,False,'��������� ����� ����������','������������ �������� ���������� ���� (�������� � ������������ ����������������)','6311013853','1026300001860','(846) 242-08-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rimessa@yandex.ru', '9EE70215BD974DFE0ED59E6440A029E1D160594D', 
 '2010-04-06 17:07:57.000',True,False,'�������� ������� ��������','��� "��  "���������"','3302000490','1023301285171','(4922) 32-09-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uom@nerungribank.ru', '7CEF635C26E67A4CB8E3EB23B60EDB103B4AF680', 
 '2010-04-07 06:31:47.000',True,False,'������� ����� ','��� "������������"','1434000020','1021400001292','89146936333', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kickbox72@mail.ru', '072C3FEA1E751410015BA57409673FB7C485D357', 
 '2010-04-07 06:55:04.000',True,False,'������� �������� ����������','���"������� ����"','3849006544','1093850029206','89025104448', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@nco.vologda.ru', '5F4264D8128955E28451BFFA0AEA92567D41FDDF', 
 '2010-04-07 10:07:45.000',True,False,'������� �.�.','��� "�������-������"','3525121018','1023500000138','NULL', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'reportinguk@kapital-am.ru', '7C2E0AEF7D0B2D847CE46E8A0B4BE0B8F8E250C9', 
 '2010-04-07 11:15:20.000',True,False,'������ ����� ����������','��� "����������� �������� "��������"','7714148894','1027739220134','7770170', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'back@investpalata.ru', '2941B39A6A128BC5567BA91A4C89800C8551456E', 
 '2010-04-07 11:49:48.000',True,False,'��������� ������� ����������','��� "�������������� ������"','3666007300','1023601563468','(4732) 55-99-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Zhiltsova_YA@solidar.ru', '301BAF632EE2F1FE61FE9ECDDA403EE312A09621', 
 '2010-04-07 12:22:12.000',True,False,'�������� ���� ���������','���� ����������� ������������ � ���������� ���������� "������������" (�������� ����������� ��������) ','7736188731','1027739165409','(495) 797-59-64 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@ncapital.ru', 'DE3DD448C9AE8B9005775DD0978D2E6593D439A7', 
 '2010-04-07 12:44:19.000',True,False,'�������� �.�.','��� "����-�������"','7714515544','1037739891958','(495) 544-41-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'grigory.gataulin@lcib.ru', '00D341B1A6F6A496F5B7F8070659E38517880EBE', 
 '2010-04-07 13:40:43.000',True,False,'�������� �������� ������������','��� "���� �������"','7704616014','5067746758525','(495)651-68-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vshakirov@dohod-invest.ru', 'AA0B43B2FDED3F9F3ABD20335CFD0F37AFBD4A3B', 
 '2010-04-07 14:12:28.000',True,False,'������� ����� ����������','��� �� �������� ����������','7702626754','1067760440098','79055352310', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@jugra.ru', '885DCC8B9E7B196CBC28E8F459F249F24FBF6245', 
 '2010-04-07 19:06:40.000',True,False,'��������� �������','��� ��� "����"','8605000586','1028600001770','495-959-58-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'razvitie@pm-invest.ru', '5BC4B7223A73114E09AA4EB056CB438D560120EF', 
 '2010-04-08 08:43:45.000',True,False,'���������� ������� ��������','�������� � ������������ ���������������� "�������������� �������� "��������"','5904208393','1095904007154','(342) 241-20-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'control_fd@aaanet.ru', '6179ADCAEAD338885F5F0CFB53B1CBC094B2F0B3', 
 '2010-04-08 09:04:01.000',True,False,'���������� ���� ����������','��� �������� ��� "�������������"','6166016623','1026104025078','(863)252-28-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'm.tvorogov@nipbank.ru', '2AF0FA1C8147CB755A78912E1E34774A9FD02FB8', 
 '2010-04-08 10:48:46.000',True,False,'�������� �.�.','�� "�����������������" (���)','7744001144','1027739043750','786-21-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'semesh@baltinvest.com', 'A7D3A312989AB751718274354CE8695989EF89AF', 
 '2010-04-08 11:24:23.000',True,False,'����� ������� ����������','��� "��������������"','7831001415','1027800001570','(812)326-14-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'echernova@sotsgorbank.ru', '9019C0987F41401170EEEC40372D691CA481095C', 
 '2010-04-08 11:37:09.000',True,False,'������� ����� ������������','��� �� ����������','5029058309','1025000000266','775-10-20 (3400)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ldlantsova@express-bank.ru', 'B319B54BE36826955C3CD5FDEA5A084F0074DCDB', 
 '2010-04-08 11:58:33.000',True,False,'������� ������ ����������','��� �� "���������"','2801015394','1022800000112','(495) 780-51-00 (5117)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stock@ergobank.ru', 'A29038B5B790C618FA09623834C458DD3978A64C', 
 '2010-04-08 12:10:03.000',True,False,'���� �.�.','��� �� "��������"','7705004247','1027739371956','(495) 783-43-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'merkatus@mail.ru', '78D0BE946A6FF445D4A15C9342859DA175502D61', 
 '2010-04-08 13:44:57.000',True,False,'�������� ������ �������','��� ��� "��������"','7325080876','1087325005063','8-9610421143', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tokuneva@starbank.ru', '897F32ED8EB441C3055AF32EFD1640032F8F39D3', 
 '2010-04-08 15:01:44.000',True,False,'��������� ������ ������������','��� ��������','8905007462','1028900000051','(495)-648-95-59', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'levykina@eurasia-invest.ru', '28BD49077837AE9C34212B626F95CB5D83ABB108', 
 '2010-04-08 15:30:26.000',True,False,'�������� ������� ����������','��� "��� "�������"','7703034951','1027739756098','(495) 950-49-58', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'larisa@finbridge.ru', '35A068E3BADFB209288373CECBE5D73C9A19A7D0', 
 '2010-04-08 17:45:34.000',True,False,'���������� ������ ������������','��� �������������� �������� �������� �����','7713236210','1027739000068','(495) 956-68-85', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Elena.Kosareva@denizbank.com', 'FEA4F88188275C1EAC52B1DB94E84114F4F9010F', 
 '2010-04-08 18:05:29.000',True,False,'�������� �. �.','��� "��������� ������"','7705205000','1027739453390','(495) 789-97-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gusarova@sbbg.ru', '0574EBA4E87A7C71E398EFDCAA41C720D46E1008', 
 '2010-04-08 19:01:45.000',True,False,'�������� ������� ������������','��� "���������� ����"','6732013898','1126700000558','(495) 785-55-55 2557', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'reportingua@kapital-am.ru', 'C2CA7337AF7D75D498DC9BE8A40D6DFFCAD904A3', 
 '2010-04-09 08:40:46.000',True,False,'�������� ������ ����������','��� "�������� ���������� ��������" ','7706681026','1087746129888','777-01-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jpad@yandex.ru', 'EEB3E6EE10FB5A95719BDABC9FC3AD679796652A', 
 '2010-04-09 12:40:36.000',True,False,'����� ����','�������� ����������� �������� "����������� �������� "�����"','5902350348','1025900507885','(342) 212-00-44', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'backoffice@mir-group.ru', '550662D8275ABEB48D6CC94D774671196AC12868', 
 '2010-04-09 13:21:34.000',True,False,'������ ������ �������������','��� "����������� �������� "�. �. �."','7806123025','1027804177170','74959213119', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'maslova@ispr.nsk.su', 'CDCE222F17A9CF1A09EC88DDD1BB5AF90FF65DF6', 
 '2010-04-09 13:27:42.000',True,False,'������� ���� �������������','��� �� "����������-������"','5406103334','1025402452360','383 211 90 70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dinvest@bk.ru', '5BB89D34DA3B7CA835E583ACFDC8E53DB7A89B1E', 
 '2010-04-09 14:32:43.000',True,False,'�������� ������� ����������','��� "������� ����������"','7703569763','1057748946155','542-0592', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Aleksandra.Dubrovskaya@interprombank.ru', '8B646CB469D0EB0587AEC15271670A0117AD8F43', 
 '2010-04-09 15:51:45.000',True,False,'���������� ���������� ������������','��� ��� "�������������"','7704132246','1027739033013','4952322050', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'deal@chelinvest.ru', '45FF75CC8830E7F8E35CE582F6241F2F67694FB2', 
 '2010-04-12 11:37:28.000',True,False,'������ ����� ������������','��� "���������������"','7421000200','1027400001650','+7 (351) 266-45-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Alexey.Youdakov@socgen.com', 'D700737AA74233B6313BB2F32BC5E470D7DF7944', 
 '2010-04-12 13:06:17.000',True,False,'������ ������� ����������','��� "����"','7703023935','1027739199256','(495) 720 6725', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@snorascm.ru', 'ACE0F84A9428E11788EE6F36050C9EBA69F8A2CC', 
 '2010-04-12 14:25:57.000',True,False,'�������� ��������� ������������','�������� ����������� �������� "������ ������� �������"','7715285621','1027739034542','411-68-11 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nearonova@alal.ru', '5C1987AB43D87660CAB76A6244484FC162791C05', 
 '2010-04-12 16:29:53.000',True,False,'��������� �������','��� �� "����� ������"','7704018984','1025000004787','499-252-00-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'brik@rubeg.com', 'DB56106FBAB78346F9FBB5C5667A974354C1BE5B', 
 '2010-04-12 16:55:59.000',True,False,'���� �.�.','��� "����� ���� ������"','7804076965','1027802497854',' 8 (812)337 55 55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'asablina@moskb.ru', 'A2B198ABFB81D698007062B53A23B30F34D3669F', 
 '2010-04-12 17:19:42.000',True,False,'������� ���� ������������','�� "��������������"','7744000711','1027739151109','363-22-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@transcapital.com', 'EDB2454F4984E42DA913BA804A7A204E47425E95', 
 '2010-04-12 17:59:53.000',True,False,'�������� ������� ','����������� ������������ ���� "����������������" (�������� ����������� ��������)','7709129705','1027739186970','495 797 32 00 +1357', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ann@vtb.ru', '603F3A9CF3E6FD0B94FE5BCACDF8B54C75A7094B', 
 '2010-04-13 12:49:19.000',True,False,'��������� ���� ��������','��� ���� ���','7702070139','1027739609391','775-54-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stech@asinvest.tomsk.ru', 'EFC5BC6BB1216A6C63F106BD8B1987231BB6187B', 
 '2010-04-13 12:57:59.000',True,False,'��������� �.�.','��� "�� ��-������"','7018042290','1027000862680','(3822) 42-06-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@rbsalliance.com', 'F84B9C5351E11CA33C2E80AF65AD6BBD3DA78F52', 
 '2010-04-13 13:32:10.000',True,False,'������� ������� �������������','��� "���"','7701569246','1047796908829','(499) 391-00-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alextezikov@gmail.com', '140C7DFA8131FE38629BA2A4ACFBD10461D5D155', 
 '2010-04-13 14:36:44.000',True,False,'������� ��������� �������������','��� "��������������"','1626000087','1021600000366','(843)517-22-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yivanovskaya@msk.binbank.ru', '24BA3E9E611DDD0D4EA5CC8DC45F9075816FE4DB', 
 '2010-04-13 14:49:07.000',True,False,'���������� ���� ����������','���"�������"','7731025412','1027700159442','755-50-60(27-87)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mafur@inbox.ru', 'BBFE8C600D7A4636CFC3B03B7044E1137FDFF6B9', 
 '2010-04-13 14:57:35.000',True,False,'��������� ����� ����������','�������� ����������� �������� �������������� �������� "������-3"','7726071798','1027739311555','89639918373', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elvira@fbid.ru', 'B206BFD595C904AC7862E59C35EC4BF941DFAEA8', 
 '2010-04-13 18:07:02.000',True,False,'��������� ������� ����������','�� �� ��������� � �������� (���)','7718098813','1027739278610','(495) 603-58-86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'german.mik@mail.ru', '7A07B2ECA9D450AE84A7E7DA16096FEB10C008B3', 
 '2010-04-13 20:17:37.000',True,False,'������ ������ �����������','��� "���"','7720003833','1027739460440','(495) 685-19-30', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'etv@rmb.ru', '293DA7A796B43885DC0858EC46F64CC947EA956B', 
 '2010-04-14 07:32:58.000',True,False,'���������� �������','�� "���" ���','7750004111','1077711000036','782-03-66 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bunakov_sa@soyuzny.ru', 'CE46F31621B2AE7CE2116A6DBC3B495FBBB6E3DC', 
 '2010-04-14 07:48:54.000',True,False,'������� ������','��� �� "�������"','7708072196','1027739051383','788-6699', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mack@xrus.ru', '717C14550E52118B54AC4EB1FE70A77E7BD74610', 
 '2010-04-14 09:11:42.000',True,False,'��������� ����� ����������','��� "�������� ���-�����"','7704239616','1027700122339','499-245-55-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lantsova@amcrus.com', 'FBA3DEFAC6F5E78064E20DDAC744FC6EF46A5617', 
 '2010-04-14 09:12:29.000',True,False,'������� �.�.','���"��� ������"','7722300990','1037722047065','(495)788-76-28', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'istok@milbank.ru', '8E566FF544876A16B1C587C12F82B689E94FC5D8', 
 '2010-04-14 09:13:38.000',True,False,'�������� ��������� �������������','��� �� "�������"','7731202936','1037739634206','786-45-09', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'natalia.akashkina@ca-cib.com', '510CB479855B8102DB9D532706C4B9537CFEC435', 
 '2010-04-14 09:31:31.000',True,False,'�������� ������� �������','��� "������ �������"','7831000612','1027800000953','(435) 564-83-44', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sup@vebrr.ru', 'A6A043E603A9522B49D410F9F3FB6CCE52636869', 
 '2010-04-14 10:11:50.000',True,False,'�������� ������� ������������','�� "�����" (���)','7744002691','1027739547549','980-7519', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'padas@ermak.ru', 'E6CF1C98EB4FFE67CE47C978165B30335E63587E', 
 '2010-04-14 10:12:57.000',True,False,'����� ����','��������  ����������� �������� "�������������� �������� "�����"','5902113957','1025900507159','(342) 212-00-44', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ruchyv@si-bank.ru', '0C8538F6041A11DD761C7B1003C28B0DDFD00148', 
 '2010-04-14 10:39:39.000',True,False,'���������� �.�.','�������������� (���)','7710020212','1037739314348','499-973-40-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@highklass.ru', 'CED2C6CDDE276B3265F9372001FB120D88B445AE', 
 '2010-04-14 11:17:31.000',True,False,'��������� �����','��� "��� �����"','7704011890','1037739445809','363-93-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'invest@monilit-ufa.ru', '7BCD70BFC3CB683A12C5218DBD08D7F7B6B6D3E9', 
 '2010-04-14 11:23:35.000',True,False,'�������� ������� ������������','��� "�������-������"','7707609488','1067760091300','+7 (347)291-22-67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'monolit@ufanet.ru', '7BCD70BFC3CB683A12C5218DBD08D7F7B6B6D3E9', 
 '2010-04-14 11:27:29.000',True,False,'�������� ������� ������������','��� "���������� "�������"','7724281447','1037724042212','+7(347)291-22-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'o.ilinskaya@ibv.ru', '17FCD4B6DE9331BF78F4C9AC1B5C807DFD230423', 
 '2010-04-14 11:39:08.000',True,False,'��������� ����� �������������','�������������� ���� "�����" (�������� � ������������ ����������������)','6027006032','1026000001796','(495) 775-4388', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tmmg@rpb.ru', 'B2022151BC6725F4EAE559C1EC385E725993EFDA', 
 '2010-04-14 11:55:10.000',True,False,'������ ������ �����������','�� "�����������" (���)','7724192564','1027739091280','967-17-80', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Natalya.Kurnakova@bancaintesa.ru', '5302E2B27AB2066568AA268FFF102A31C9314030', 
 '2010-04-14 12:58:57.000',True,False,'��������� �������','���� ������ ���','7708022300','1027739177377','967-30-60 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'genak@evrofinance.ru', '5C8CBB513588B97E04622AF47A0209BB9166C89C', 
 '2010-04-14 13:11:53.000',True,False,'��������� �.�.','��� ��� "���������� ����������"','7703115760','1027700565970','967-8171', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pn@prbb.ru', '96CA5ADD1B351342C887B54F7490210D3ED0F589', 
 '2010-04-14 14:02:10.000',True,False,'�������� ������� ','��� ��� "�������������"','7729086087','1027700508978','933-37-37 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tempbank@col.ru', 'A7673110E5D0F00BD9100168A61D5F3AF9B663EA', 
 '2010-04-14 14:08:25.000',True,False,'������� �.�.','��� ��� "��������"','7705034523','1027739270294','676-54-48', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Oksana.Shendrik@rbs.com', '4D7E14CE0880F83E8C62F2743FEDEAB0EEFDA2A8', 
 '2010-04-14 14:15:50.000',True,False,'������� ������','����������� ���� ��������� (���)','7703120329','1027739446349','9319141', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bogdanova@ctrust.ru', '5955EA732B02D760A128BD7C4A0AF0335CEC3A0E', 
 '2010-04-14 14:35:25.000',True,False,'��������� �������� ������������','���"�� "���"','7703676980','5087746238333','781-63-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'efremova@ikb.ru', 'A129C625AA2301A5473AD247D10EB08565B3FF87', 
 '2010-04-14 14:39:13.000',True,False,'�������� ���������','��� "�������������" (���)','7729014090','1037744002251','780-31-71', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sorokina@incredbank.ru', 'DF80E9491004A0E259ECFDEA17BC37586D19EE04', 
 '2010-04-14 15:19:23.000',True,False,'�������� ����� �������������','��� �� "����������"','7744002268','1027739341156','660-01-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'citybroker@mail.ru', '54463878B33B7584D3538990866164402FC5AA55', 
 '2010-04-14 22:45:15.000',True,False,'������� ���� �������������','��� "���� ������"','7736524278','1057747356666','(495) 251-62-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond@albank.ru', 'D90EB4F8F6C8133106FCB4E8629C4DE7169FB3A5', 
 '2010-04-15 10:34:06.000',True,False,'������� ������ ����������','��� "���������������" ���','1435138944','1031403918138','841132422488', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'marina.kudinova@nomura.com', '7ED9A90DD60169737C42D7C6A02EAD1E14B7CAC3', 
 '2010-04-15 10:38:05.000',True,False,'�������� ������ ��������','��� "������"','7704654757','1077757805267','(495)663-60-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sher@nbbank.ru', '2DE4EE73E609849032B370BC34BF60D64C05C0F2', 
 '2010-04-15 11:38:10.000',True,False,'��������� �.�.','��� "���"','7750005500','1097711000089','+7(495)780-33-16', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elnik@energotransbank.com', 'D1B861E734D298214A18D0D59575215F6899625F', 
 '2010-04-15 12:17:04.000',True,False,'����������� ����� ����������','�� "���������������"(���)','3906098008','1023900000080','8(4012)45-15-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@arbat-cm.ru', '6BE227EB1A8CBE14455E95FD415E635A972DB4CC', 
 '2010-04-15 12:47:48.000',True,False,'������ ���� �������','��� "��" ����� �����"','7701247577','1027739060117','8(495)545-42-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ershova@sinara-invest.ru', '3714BC2C79A232A4729E5B432C3E4F447609ACB5', 
 '2010-04-15 13:54:00.000',True,False,'������ ������� ���������','��� "������-������"','6666000413','1026600929629','(343) 310-39-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'msdealer@bk.ru', '5E59953938098F03F98364E7A3F969A1C601DB53', 
 '2010-04-15 14:13:31.000',True,False,'������� ������� ��������','��� "��������� ����� M&S"','5005006598','1025000922088','8 (496) 441-01-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mariya@barrel.ru', '96CA5ADD1B351342C887B54F7490210D3ED0F589', 
 '2010-04-15 15:22:18.000',True,False,'�������� �����','�� �������','7744002941','1037744001130','(495)7102939', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@bp21.ru', 'F7088B179FA6636B6013CB6C852FEA30340AA42E', 
 '2010-04-15 15:49:52.000',True,False,'������� �������� ������������','��� �������� "������-������� XXI ���"','7728205990','1027739136699','(495) 933-88-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ovenchakova@roscredit.ru', 'E6E4CC93CB70EF71BDBEA5D67E14DBA9C0E250E5', 
 '2010-04-15 17:21:32.000',True,False,'��������� ����� ��������','��� "���� ���������� ������"','7712023804','1037739057070','(495) 258-32-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'utsarenko@pallada.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2010-04-15 17:32:12.000',True,False,'������� ���� ������������','�������� ����������� �������� "������� ����� ����������"','7710199697','1027739145060','(495) 721-13-56 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'orlova@minbank.ru', 'D7FFEC830032D3171EBFA299B5892ABE84B0966F', 
 '2010-04-16 16:09:15.000',True,False,'������ ����� ����������','��� "����"','7725039953','1027739179160','(495) 958 - 50 - 02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'denis.cherkasov@bk.ru', '84BDD69B35F8AFCEB52B0329CBCAD91B543158E4', 
 '2010-04-17 17:36:36.000',True,False,'�������� �����','��� ������','5404401529','1095404021338','79139281641', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oemkinv@yandex.ru', '522C17BC7BD3259E99FA732FBEBF4091F3D51C68', 
 '2010-04-19 14:22:51.000',True,False,'������� ����� ����������','��� "�� "����-������"','3128022853','1023102357387','84725339638', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kvvk5@mail.ru', '19407E4C904A1499303946FFAA9F3DABF954A119', 
 '2010-04-19 14:54:48.000',True,False,'������ �.�.','��� "���������� ��������-��������� ��������"','1623003072','1021607752396','(84364) 2-08-19', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'b000b@cn.ru', 'DF80E9491004A0E259ECFDEA17BC37586D19EE04', 
 '2010-04-19 15:22:59.000',True,False,'����� ��������� ���������','��� "���������"','7024032396','1107024000093','8 913-204-63-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'strekalova@vika.ru', '6F3B179C23D58D30D517EB5FCCE3C98FA74414A6', 
 '2010-04-19 16:16:13.000',True,False,'���������� �������','��� "����-������"','7703345650','1027739228483','(495) 540-86-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.moiseeva@globexbank.ru', '42ACA7E49022F308612964631305EEC82DCDFF06', 
 '2010-04-19 16:49:08.000',True,False,'�������� �.�.','��� "�����������"','7744001433','1027739326010','411-82-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'novikov@brtbank.ru', '5DD2E51EC5DEC4F1D344515DC4621BAC15102580', 
 '2010-04-20 08:32:14.000',True,False,'������� ������� �����������','�� "���� �������� ����������" (���)','7744003173','1037711005804','4957803331', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tico@tibc.ru', 'A8010A18DE7773FF82A042C916F89AB22940808A', 
 '2010-04-20 09:01:33.000',True,False,'�������� ��������� �������','��� "�������� �������������-��������� ��������"','7106009486','1027100740688','(4872) 36-89-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gkurbatova@infoprom.ru', 'AB5D8D415E1AC67D82A0B03758F6B79897084802', 
 '2010-04-20 13:52:53.000',True,False,'��������� �.�.','��� �� "�������� ����"','7727690720','1097746287671','4957752882', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jonny@eif.ru', '03A06074A11F2FE5136294784516C8D69710821F', 
 '2010-04-20 14:02:58.000',True,False,'������� ����� �������','��� "����������������"','7728167022','1027739383462','499-123-29-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'hodjamatova@finam.ru', '7CCB15C3A807441B73A4479D5F275A7EF88C64E1', 
 '2010-04-20 14:54:33.000',True,False,'����������� ����� �������������','��� "�����"','7731038186','1027739572343','8(495)7969388', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'amurhimiy@mail.ru', '1ABE8F9146901E6C22478909B1D3F697F20F6277', 
 '2010-04-21 04:06:47.000',True,False,'������� ���� ����������','��� "����������������"','2801021077','1022840507095','416-2 44-47-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@crescofinance.ru', 'DD93685ADA74AE50387EC01F20C9BCB05B61153D', 
 '2010-04-21 09:00:27.000',True,False,'��������� ������ ���������','��� �� "������ ������"','7703703056','1097746422531','+(495)661 79 07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ReportsFCSM@ufginvest.ru', '1F3B204719558E8064874BF0F8F4C0561AAE0FBF', 
 '2010-04-21 09:31:39.000',True,False,'����� ����� ������������','��� "��� ������"','7710186602','1027700590301','(495) 721-12-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dudkin@regiobank.ru', '94515F2ECC1EECAA214805D9AEF8A431C68DFEB4', 
 '2010-04-22 06:48:36.000',True,False,'������ �.�,','��� �����-���������','2702070059','1022700000047','4212 74-78-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@banksoyuz.ru', '2BE8A5201981A8E8EFB335F23F42E9D293E8F1E7', 
 '2010-04-23 07:43:05.000',True,False,'������� ������ �������','��� "����"(���)','7714056040','1027739447922','729-55-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Olga.Ovchinnikova@absolutbank.ru', '4C4E23BB8C8AD193A5DEB356AE64B5C49322F378', 
 '2010-04-23 08:27:56.000',True,False,'����������� ����� ���������','��� "������� ����" (���)','7736046991','1027700024560','(495) 777-71-67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lanik@agrostandard.ru', 'E80A3F266673651B8EFB7122C0088291EB95D038', 
 '2010-04-23 08:31:05.000',True,False,'����� �.�.','�������� � ������������ ���������������� "����������� �������� "���� ��������"','7704582894','1057749599335','(495)545-36-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gavrilov@nkmb.ru', '85FDB6818B9339C54A1C2E443B765A8FF2512132', 
 '2010-04-23 15:01:44.000',True,False,'�������� ������ �������������','����������� ������������ ���� "������������� ������������� ����" �������� ����������� ��������','4216003682','1024200001770','(3843)390430', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ydubrovina@markscapital.ru', 'B5DC7DAEA0388D37180ECD01043FB698F2B68CB7', 
 '2010-04-23 19:24:56.000',True,False,'��������� �.�.','��� "����� �������"','7708213601','1037708002100','(495) 988-0656', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'diana_tarachenko@mail.ru', '316BCFAF6844AD6942FA0DEF5E713ECE0F75232A', 
 '2010-04-24 10:04:14.000',True,False,'��������� ����� ����������','��� "������-������"','7701148142','1027739464521','8-9670248528', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ic-talan@rambler.ru', '7F7FCD0C799FB7A64EBDADD3984242C3A35F04DA', 
 '2010-04-26 11:36:06.000',True,False,'������� ������� �����������','��� "�������������� �������� "�����"','7707683890','5087746607856','(495) 629-51-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@ru.almazbroker.com', 'BFFE99566F4F134D484038139066D309D20CA8C5', 
 '2010-04-26 11:38:17.000',True,False,'����� �������','��� �� "�� �����"','7721588631','5077746889600','(495) 737 82 17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@omegainvest.ru', '3479C5F380741B4F5810575B7D658D4FCC75A9B6', 
 '2010-04-26 13:15:56.000',True,False,'������� ����� ���������','��� �� "����� ����� ����������"','7728652558','1087746356950','(495) 411-94-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'petrova@tandem.ru', '8EE48595A5BD0BD5F4E343C2D9E8CA296769D2FF', 
 '2010-04-27 11:26:11.000',True,False,'������� ���� ����������','��� "����������"','7703074584','1027739051053','(499)256-09-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mou@rncb.ru', '3B3F2DAC7F9E0EC1736E9B31022E19674BA89B7D', 
 '2010-04-27 12:18:52.000',True,False,'�������� ���� �������','���� (���)','7701105460','1027700381290','(495)232 9086 (1064)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'titanin@aaanet.ru', 'E12FD76FEA95EA2496A1FE41A6C3254D890E8D34', 
 '2010-04-27 12:31:24.000',True,False,'������ �.�.','��� "�����-������"','6164028447','1026103264659','863 240-60-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'EPavlitsky@dtsys.ru', 'A10E6818E40BC335250230B1874F7762AA8FEED0', 
 '2010-04-27 16:01:16.000',True,False,'��������� ������� �����������','��� "XXI ��� - ��"','7727163988','1027739592759','951-3504', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'backof@petroinvest.ru', '73B103C94AFE2E67C99F29B96336397F8659CE02', 
 '2010-04-29 08:47:33.000',True,False,'�������� ������� ����������','��� "�����������"','7838324750','1057810388690','(812) 528-31-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nika@ellipsbank.ru', '10DDDF28612DC8E5A874B326B78C016F760E7277', 
 '2010-04-29 12:36:13.000',True,False,'�������������� ������� ���������','�������� ����������� �������� ������������ ���� "������ ����"','5253004189','1025200000055','(831)212-52-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alla@bcs.ru', '2E8FA596743FDAB036144B70296D763D9D60BE37', 
 '2010-04-30 13:36:35.000',True,False,'��������� ���� ����������','�������� � ������������ ���������������� "��������������� ������������������ �����������"','5407257111','1035403212646','(383)2300600', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'VTBAMBackoffice@vtbcapital.com', '263F1E5518DA2B6F9ABB0E85E27BE37A90D8BD7C', 
 '2010-04-30 16:23:26.000',True,False,'���� ������, ���� ��������','��� ������� ���������� ��������','7701140866','1027739323600','7255540', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sekr@spasskshifer.ru', '13F928DA87147FF6361626741C1F8B677A3D156A', 
 '2010-05-04 04:12:25.000',True,False,'������� ������ Ը�������','��� �����','2510000386','1022500818340','4235251082', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivfedulova@autoland.ru', '178F0D450FF0E1F5732D7E93D76ACEA46DB32314', 
 '2010-05-04 06:17:41.000',True,False,'�������� ����� ������������','��� ��������������','6660132389','1026604951031','37-27-000', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sec@preodbank.ru', 'E465319DC8A320933EEC8C1F96205B0DB732E639', 
 '2010-05-04 09:05:53.000',True,False,'������� �.�.','�� "�����������"','7701051006','1027700215817','228-30-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '1rusfond@1rusfond.ru', '052EC7665CD9AE012A5708BB092653758D78A085', 
 '2010-05-04 16:55:47.000',True,False,'������ ���� �������������','��� "������ ������� ���������� ����"','7729053290','1027739129868','74957211174', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rustem@zambank.ru', '65D43E3032AF3FEB9BD0F0A44C0AA8A183A515E3', 
 '2010-05-06 13:49:26.000',True,False,'��������� ������ �����������','��� ��� "��������������"','7714044415','1027739661498','(499) 238-81-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kmbess@mail.ru', 'B481BA4650706883164EF927FA7920E5B8259D9A', 
 '2010-05-07 11:45:09.000',True,False,'�������� �������� �������������','��� �� "������"','2635107776','1072635023636','88652956831', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'avgust@admbank.ru', 'B71826117A3B3E41E9DC7D1E265681936E4797CA', 
 '2010-05-07 12:49:50.000',True,False,'������������ �.�.','��� �� ��������������','7704010544','1037739763753','223-07-36 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'smirnova@umbank.ru', '40AE34DA18A76CCB2EBD156A30ACA06B5B16E2F9', 
 '2010-05-07 13:07:49.000',True,False,'�������� ���� �����������','��� "�����������"','4214005204','1024200006434','8-351-247-49-92', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'i.savchenko@bfa.ru', 'A9875FB1A7551494D785C503DED5D603E3B83799', 
 '2010-05-07 14:40:31.000',True,False,'�������� ����� ��������','��� "�� "���"','7825481139','1027809172368','(812) 329-15-95 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pavel@pskovvtormet.ru', '6928660798154921F364DDC70C72C511F660467C', 
 '2010-05-12 08:03:30.000',True,False,'���������� ����� ����������','Vtormet','6027007501','1026000955705','79211124060', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bhz@bk.ru', '41526A13D92FCE945D831C4CD00B95FC47CA6B3C', 
 '2010-05-12 08:37:34.000',True,False,'������� �.�.','��� "��������� ���������� �����"','5000000401','1027739596642','712-70-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'amzdirect@mail.ru', 'F689C25B086C918ACA02142479C055906A855D1B', 
 '2010-05-12 12:01:17.000',True,False,'����� ��������� ����������',' ��� " �����"','6601002451','1026600507427',' ( 34346 ) 94504', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kalinina@narcred.ru', 'DF80E9491004A0E259ECFDEA17BC37586D19EE04', 
 '2010-05-12 13:25:18.000',True,False,'�������� ������� ����������','��� ���� "�������� ������"','7750005436','1097711000034','(495)959-17-90 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'admins.ryz@psb.ru', '73BA88108ACBF81134E462A1A4283649321012AB', 
 '2010-05-12 16:07:53.000',True,False,'��������� �����','��� "��������������"','3803202000','1023800000322','(812)332-72-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tuhkur@open.ru', 'CA9F7DE6471A05ACAD27C9542BBF3247B480A396', 
 '2010-05-12 16:38:21.000',True,False,'������ ����� ��������','��� "���������� "��������"','7744000140','1027700505744','660-33-70 (3122)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kourova@gib.ru', '17D89645A414486FAFF6A3D06397235541991728', 
 '2010-05-12 17:16:10.000',True,False,'������� ������� �������','����� ������ ���� (���)','7750004312','1087711000057','(495) 589-92-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olgahod@finam.ru', 'D620DB1E5C034326AAE1D0855F7322D3677F996E', 
 '2010-05-12 17:51:19.000',True,False,'����������� ����� �������������','��� "�� "�����" ','7701334244','1037701027980','84957969388', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rogovaya@navigatorbank.ru', 'ABEB7E9047084646ED13544B39E43D42BE5E8FF0', 
 '2010-05-13 14:12:37.000',True,False,'������� ����� ����������','���� "���������" (���)','7704046967','1027739109276','6427942', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kovalenko@russlavbank.com', '897E228C70015B8B1C28532DEC45E59BB178F01E', 
 '2010-05-13 15:07:23.000',True,False,'��������� ������� ���������','��� "�����������" (���)','7706193043','1027739837366','(495) 236-21-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'm.suhanova@brocompany.com', '900E2DED210F439A59178013A4E796227692801E', 
 '2010-05-13 15:15:42.000',True,False,'�������� �.�.','��� "����� ������"','7705808015','1077759835515','812-313-19-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zag@finxplus.ru', '30959632D4798F4205B92802CD9FA0104D199E85', 
 '2010-05-13 16:11:26.000',True,False,'��������� ������� �����������','�������� � ������������ ���������������� "����������� �������� "������ ����"','5024089134','1075024006144','710-78-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'di@delta-invest.ru', '7F62E40388350004C6E90CFB1B4845EE169A7B11', 
 '2010-05-13 18:08:45.000',True,False,'������ ������ ����������','��� �� "������-������"','8602048452','1028600610147','3462328484', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@vip-invest.ru', '0F935D9D4D30C3E3C54BF5A02EBCEE3EB4D69430', 
 '2010-05-13 18:34:09.000',True,False,'���������� ��������� ����������','�������� � ������������ ��������������� "��� �������������� �������"','7713328284','1027713012601','(495) 739-82-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'margo.kos@mail.ru', '908A8F781F13FE98C248251839192AFD78B5F28D', 
 '2010-05-13 21:28:13.000',True,False,'������� ��������� �������������','��� "�������������"','7451028481','1027402918245','8351-2621222', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oaogkxred@yandex.ru', '9292A4D1B35E5F67CA882E40A72A71AFB4DC48D8', 
 '2010-05-14 07:58:27.000',True,False,'�������� ��������� ����������','��� "�������-������������ ���������  �������"','6911022708','1056910001785','(848242) 58-011', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alena081063@yandex.ru', 'B8F2B5FF191C01E2FE5815C9363340A8DE6C8D1A', 
 '2010-05-14 10:45:18.000',True,False,'��������� ����� �������������� ','��� "�����"','5835001003','1025801207321','89023527073', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena.khaylova@nordea.ru', '295D141FCECC4CE1E32C9B36BF36F389F559BC1A', 
 '2010-05-14 10:51:09.000',True,False,'������� ����� ����������','��� "������ ����" ','7744000398','1027739436955','-1376', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kulagina@economiks.ru', 'F170691CA9D15B600785FE3B741DF7A38991D733', 
 '2010-05-14 13:26:41.000',True,False,'�������� �.�.','�� "���������-����" (���)','7704075196','1027700523718','(495) 933-42-38 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'astarcap@mail.ru', 'D99C3A4A101ACB91A6ACF97B5A642D9092F4EE4B', 
 '2010-05-14 15:00:21.000',True,False,'�������','��� �� ����� �������','7701176453','1027739140285','6219229', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'innanamestnik@mail.ru', '00297B9F222DA51E0AD978DED7D116E04583AB27', 
 '2010-05-17 06:53:59.000',True,False,'�������� ���� ����������','�������� ����������� �������� "�������� �������������"','5916022234','1095916000290','83427399141', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rimskih@sevnb.ru', 'DDF5C584F82C26B95BB8C1ECEC9C6E80C25E66A1', 
 '2010-05-17 09:51:56.000',True,False,'������� �������� ����������','��� "�������� �������� ����" (���)','1101300820','1021100000074','(8212)440675', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Julia@libra.ru', 'AB64EE7AA4D9643B60737C229EE3646603BC9720', 
 '2010-05-17 11:07:49.000',True,False,'���� ������� ','��� ����������� �������� "����� �������"','7727516062','1047796528493','(495) 580-77-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ysokolova@lockobank.ru', 'D68EBEC0F7321CC010D4DA70C4DF81ED8D9A2AA9', 
 '2010-05-17 11:40:56.000',True,False,'����� ������� ���������, �������� ���� �������','��� �� "����-������"','7722691889','1097746445180','(495) 7390729', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'y0404@yandex.ru', 'CCE2BC7954D61F2EF81280BAD30A0A9F0EF044F1', 
 '2010-05-17 19:38:01.000',True,False,'�������� ������','��� "�� "���"','7720581016','5077746545344','8-916-975-59-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'asazonova@mtbank.co.ru', 'B410B458AC3A4BF6843562EBB529870ED337FBFA', 
 '2010-05-18 12:06:43.000',True,False,'�������� ���������� �������������','��� "�� "������������"','7730045575','1027739067696','(495)684-20-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Klimovas@cfb.ru', 'A308FFCBAA8133F5C9255A1A35FA6A2283E4A1E8', 
 '2010-05-18 14:09:03.000',True,False,'������ ��������� ���������','��� "���� ���"','7704111969','1027739542050','8-495-514-08-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ktm05@list.ru', '412B20D8DCD2A7BBF6E02C4D25EAD9E9BE5B8582', 
 '2010-05-18 14:32:31.000',True,False,'������ �.�.','���"����������� ����������"','7814318324','1057810264137','4956280', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stocks@instafxgroup.com', '354D68E8D4E8C2C810FA53EF6FDBABEEBE9554E2', 
 '2010-05-18 14:39:10.000',True,False,'�������� �.�.','��� "�������������� �������� "����������"','7733661413','1087746789712','(4012) 61-64-58', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'karpovich@jtbank.ru', '5FDC50C7BA43005417AC9B512FC4A041BB8E780A', 
 '2010-05-18 16:03:59.000',True,False,'�������� ����� ����������','���� ��� �� ���� �������� ����������� �������� ','7713001271','1027739121651','(495) 662-45-45', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'AShtek@mbrd.ru', '97A7C9478E735609C05CEC9C731E7F1491CEA42E', 
 '2010-05-18 16:04:32.000',True,False,'���� ����� ���������','��� "����" (���) ','7702045051','1027739053704','8-(495)-637-14-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kurka15@mail.ru', '196CA7349D3C0CE0A680F7A40EBC97AC2120E5BD', 
 '2010-05-19 12:51:48.000',True,False,'�������� �������� ����������','��� "��"������������"','1512014833','1091512000480','518455', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'maluka@uk-continental.ru', '8D724353845059C40050DC2552D9BAE9EFF6F4D2', 
 '2010-05-20 09:09:58.000',True,False,'������ �.�.','��� �� "������������"','7722563365','1057749298639','(495) 797-36-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'backoffice@instandart.ru', '1DBC3017EFE1F5F8AF5B5F4DA39DEED655FB6673', 
 '2010-05-20 13:32:50.000',True,False,'������� �.�','��� �� "�������������� ��������"','7737523774','1077760004090','(495) 665-02-41 (226)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@pkb.ru', '6E04AAFD1717C2B95B3130738F4D0302672F012C', 
 '2010-05-20 14:01:26.000',True,False,'����������� ����� �������','��� ���� "������������"','7707284568','1027739340584','(495)623-46-57', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'globcap@mail.ru', '2560D1907F8C6CFF77A6BE1AF4561F687107A462', 
 '2010-05-21 07:50:08.000',True,False,'������ ������ ������������','�������� � ������������ ���������������� "�������������� �������� "������ �������"','7704723640','1097746160643','73912805421', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Safakulevodrsp@mail.ru', 'BEBF264531AAB04EFFF0955563EA9E1151F93422', 
 '2010-05-21 12:23:59.000',True,False,'���������� �.�.','��� "������������� ����"','4519005520','1084524000098','8352432-11-44', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena.makukhina@db.com', '96626BECA56E5BCCE1A80D51F0A0FF73AA8EE376', 
 '2010-05-21 15:30:23.000',True,False,'�������� ����� ���������','��� "����� ����"','7702216772','1027739369041','933-91-04', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'backoffice@starfin.ru', '95D622E171C7DD0B1DD19F4B394D8DD2B3CF08DD', 
 '2010-05-24 10:23:18.000',True,False,'����� �������','��� "�������"','7736539250','1067746574830','(495) 436-00-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@homecredit.ru', 'E4EE643C24290FDAED3BECE9268171053B8E4109', 
 '2010-05-24 12:06:11.000',True,False,'�������� ������ ����������','��� "��� ����"','7735057951','1027700280937','(495) 785-82-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'serg_b@list.ru', 'CCCE328B53C210168F271BDFB146AC68A958476A', 
 '2010-05-24 17:02:29.000',True,False,'������ ������ ������������','��� "������-������-�"','7709797685','1087746790680','8-919-766-76-29', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'irina@perspplus.samaralan.ru', '020445060A29EFC23B0FD111592BEB678FEEF4AD', 
 '2010-05-25 05:54:17.000',True,False,'����������� ����� ������������','��� "�� "����������� ����"','6312031164','1026301416173','(846) 270-42-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'chamaev-stroindustria@mail.ru', '91D2A89B336A998B9EA45704157398106E3FAA09', 
 '2010-05-25 11:15:14.000',True,False,'��������� ����� ����������','���"��������������"','2015040948','1072031000304','8(8712)222256', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'audit5@boncom.ru', '633C13E849683E3313B7412A4F69FB61C1B9243B', 
 '2010-05-25 11:41:33.000',True,False,'��������� ������� �������� ','��� "������"','2127326529','1032127009309','8 (8352) 48-15-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'y_andreev@apfrublev.ru', 'D0619C4271F2C04C77309B4F2FB2E3C495FAE56F', 
 '2010-05-25 14:22:56.000',True,False,'������� ���� �����������','��� "�������-������"','3907039076','1043902823029','(4012) 70 29 33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'east@peterlink.ru', 'AA52C5FF33E9ECE7C3C479019E7890175386773B', 
 '2010-05-26 11:21:14.000',True,False,'��� ������ ������������','��� "������"','7804432839','1107847057779','7035036', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edo_fcsm@nwfc.ru', '751A3FE1D26CADD9948CECC6DF25AC41D1DA823E', 
 '2010-05-26 11:38:10.000',True,False,'�������� ������� ������������','��� "������-�������� ���������� ��������"','7825073771','1037843008323','(812) 333-25-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alexey.minaev@crediteurope.ru', 'D62ADEAD32AF306730CB06FAD1E5FFC06B1F2E11', 
 '2010-05-26 12:12:36.000',True,False,'������ ������� ������������','��� "������ ������ ����"','7705148464','1037739326063','(495) 981-38-00 --2051', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'glarina@elfin-finance.ru', '9AE49EFAB8F28B24C3022B3D4D00CA9E5C242703', 
 '2010-05-26 12:53:35.000',True,False,'������  �.�.','��� "�� �����-������"','7731003673','1027739395166','8(495) 589-92-26', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lara@ifcbank.ru', '8294E7232E83D2083332805F185136EC65CD4D5B', 
 '2010-05-27 11:13:50.000',True,False,'���������� ������ ���������','��� ��� "������������� ���������� ����"','7744000038','1027700056977','+7(495) 287-0274', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'skorina@acef-bank.ru', 'E1850A390982D9E25EAC25AA52BE73B7D0EF711D', 
 '2010-05-27 12:42:24.000',True,False,'������� ���� ����������','�� "����-����" (���)','7744003159','1037711005045','74952280848', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vnii_et@mail.ru', '9680E9A02C1E1AC98A4079C944F8CF4854EBD81B', 
 '2010-05-27 19:43:35.000',True,False,'����������� �.�.','��� "��������������������"','3903003985','1023900761609','(4012) 213410', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'V.Pomortsev@nes.032.ru', 'FD8349767EA53C0C27DC9D8C69C98C8C4C361F74', 
 '2010-05-28 09:43:21.000',True,False,'�������� ������� �������������','��� "���"','3245503712','1093254007934','+7 (4832) 921389', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'regord@afc.ru', 'FB1985B8297FCC5287373C1F8AD388E69FF91994', 
 '2010-05-28 10:38:10.000',True,False,'��������� ����� ����������','��� "���������.������� � ����������"','7722164402','1027739135247','(495)-7837006', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'findep_ndc@nsbank.ru', '9047DD48FE3A70A0DC35357EB60318609DE1E3C2', 
 '2010-05-28 11:22:32.000',True,False,'�������� �������� ��������','�� "�� ����" (���)','7744001024','1027739198200','276-11-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@elitfin.ru', '994918556305BD4DBE73B75365CD06697D6AC437', 
 '2010-05-28 14:39:09.000',True,False,'����� ������ ����������','�������� ����������� �������� �������������� �������� "�����-������"','7722563189','1057749282480','8 915 074 06 06', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'borinessa@btrapeza.ru', '5D1CD7C0410AF2F75B2C3F2C4961B4F603F66921', 
 '2010-05-31 10:30:59.000',True,False,'��������� ������ ����������','�������� ����������� �������� "����������� �������"','5031041214','1026200662080','8-496-52-2-21-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'efremov@rostinvest.ru', 'D3EA121CAC5E7C188D9C2734AF4376C4639A9DF7', 
 '2010-05-31 13:58:11.000',True,False,'������� ��������� ���������','�������� � ������������ ���������������� "�������������� �������� "����������"','7707515021','1047796226917','(495) 775-86-75', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'grigoreva@nfgam.ru', '1BC4BACAEE44053A2E620BD4BD21C3277A1A13F9', 
 '2010-06-01 12:41:55.000',True,False,'���������� ������','�������� � ������������ ���������������� "����������� �������� "��� ����� ����������"','7704528336','1047796605790','+7(495) 775-18-47  6263', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'broker@sfinkom.ru', '8DC33AFD30FFC4D88AFE129D6F342A9100797548', 
 '2010-06-03 15:54:07.000',True,False,'�������� ������ ����������','��� "���"','7707661978','1087746522291','(495) 629-51-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gari@akoula.com', 'D4BB15E1BA390EE028D4E33AA1CEEB15EEE9C435', 
 '2010-06-04 08:40:58.000',True,False,'���������� ���� ������������','��� "���������� ������"','7715541191','1047796807387','8 (915) 235-05-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Kadriya.Safiullina@alpari.org', '93B5294225A1292B31747768A6AFA6FE5F1A861C', 
 '2010-06-04 13:21:02.000',True,False,'���������� ������ ��������','��� "�������-������"','1655164523','1081690059330','(843) 26 76 111', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk@r-capital.ru', '557B309D1A95197662CF993C6E40F9C760AA4CA1', 
 '2010-06-04 13:42:03.000',True,False,'������� ������� ���������','��� "�� "������ �������" ','7709688830','1067746798206','79050365555', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tipoks@list.ru', 'B87DFBB33CCB2370EDF2CD8042D26BD2DC2D6F71', 
 '2010-06-04 14:54:38.000',True,False,'������� �������� ����������','��� "��������� ����������"','5250014196','1025201987227','(83145)2-13-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'goszaem@sbrf.ru', '00E16CAEE5CFD89BDD08BC30BF1C02B5D45FB784', 
 '2010-06-08 08:05:54.000',True,False,'���������� ������� ������������','����������� ������������ �������������� ���� ���������� ��������� (�������� ����������� ��������)','7707083893','1027700132195','(495)505-88-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'poddymnikova@bankimperia.ru', '07C113E70A90FEB19F8E142D78382F07C2CBA215', 
 '2010-06-08 13:16:34.000',True,False,'������������ ������� ��������','��� "���� �������"','7719025494','1027739673642','787-77-77 (20-39)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jvs@stroycredit.ru', '31C81AC7F380EE9D33DBAC6C67A33BC5891B7468', 
 '2010-06-08 15:34:06.000',True,False,'����������� ������� ������������','��� �� "�����������"','7744003511','1037711012525','730-17-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'okhrabry@verityadvisors.ru', '9B8D60504D36CF6D74B0066FAA8BABCA060437BB', 
 '2010-06-10 16:12:19.000',True,False,'������� ���� ����������','������ ���������','7710716786','1087746574673','-1457', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ananina5@e1.ru', 'BA278A781E896FAD19BD91FD37D5898001C06543', 
 '2010-06-11 08:26:54.000',True,False,'��������� ������� ��������������','��� "����������� "�������"','6664008495','2106671145865','251-98-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gerasimova-ev@sbrb.ru', '1D6C9B5608C3A2430232132CC2CB4BB4A2C41F29', 
 '2010-06-11 10:13:40.000',True,False,'���������� ��������� ������������','��� "�������������"','8602190265','1038605506906','(3462) 52-08-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@rgs-am.ru', 'AF5D41A27271B8B3BD79FF76DAE408C8AD02F471', 
 '2010-06-16 08:56:15.000',True,False,'�������� ������ ����������','�������� � ������������ ���������������� "��� ���������� ��������"','7731191498','1027700012119','(495) 921-20-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'boffice@intrustbank.ru', '2E5B1E5242A47AD1B6EFA01294BE1A79EDA7E636', 
 '2010-06-16 17:05:52.000',True,False,'������ �.�.','�����������  ������������ ���� "�����������" (�������� ����������� ��������)','7736193347','1027739249670','739-05-79', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivleva@allianz-invest.ru ', '901EC174CEE0ECCD7690876752FEF7465B56BC13', 
 '2010-06-21 12:25:10.000',True,False,'������ ����� ����������','��� "������ ����������"','7704239292','1027700266813','+7 (495) 737-37-73', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ukpi.ru', '5334229443A7B3B05147B4D0A5595FB75509E53B', 
 '2010-06-22 09:27:44.000',True,False,'������ ��������','����','7706111837','1027739209431','(495) 642-6730', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Fenchak.AV@esteit-invest.ru', '8C75C4D7FC84917A6063DE86FF957FCAF3BEF27E', 
 '2010-06-22 15:02:30.000',True,False,'������ ������� ����������','��� ����������� �������� "������ ������"','7708622379','1067760938167','(495) 504-09-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mono@monomah.ru', 'DBCDC5E05A0DDD3F84F2589ECA4692E4E83023BE', 
 '2010-06-23 12:46:52.000',True,False,'������� ���� ����������','��� "�� �������"','5408258380','1085473000018','383 363-01-71', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'MMohova@amtbank.com', 'E59F20D530AED6CFD57F88C516B9C9D3FA557731', 
 '2010-06-23 17:14:37.000',True,False,'������ ������ �������������','��� "��� ����"','7722004494','1027700182366','(495) 363-09-09', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'avista_tmp@inbox.ru', '2F39A526AB087389A7144691D93316B0ECF466F8', 
 '2010-06-24 11:29:49.000',True,False,'���� ������� ����������','�������� ����������� �������� "������������������ ����������� ������"','5048080363','1025006392025','(+495) 787-31-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'MalakhovskayaIS@solid.ru', '72FAE46828FE520A24E35328DDF6118D4E3124F7', 
 '2010-06-25 14:04:16.000',True,False,'����������� ����� ���������','��� �� "������������','6316028910','1026300001848','(846) 270-33-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Osinki81@mail.ru', '30F5D5D4C0BA11595B37A4A36179AC5376F04AEE', 
 '2010-06-28 12:25:51.000',True,False,'������� ��������� �������������','��� �� "����������"','7744002211','1027739028855','662-50-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mr_vvv@mail.ru', '39E418E0E9E6EFFCC81A1492BE98C67453421977', 
 '2010-06-28 15:16:22.000',True,False,'���������� �������� ������������','brclub','7720580206','5077746450040','8(495)502-34-29', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Svetlana.Borisova@vtbcapital.com', '901F9A5AD1332A55C9D5398D30844ED347041A7B', 
 '2010-06-28 15:55:53.000',True,False,'�������� �������� �����������','�������� ����������� �������� "��� �������"','7703585780','1067746393780','+7 (495) 663 6427', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'headoffice@cibcag.ru', '795E73B551AE037BC08784DF264541679CF66FE3', 
 '2010-06-29 13:07:50.000',True,False,'��������� �.�.','�������� � ������������ ����������������   "��������� ������������� ���� �� ������ ������ ����"','7704652051','5077746934315','(495)6974691', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@averse.ru', '5F217558AE5BDBB14A9A0F2B4D5655E9F1DA4487', 
 '2010-06-29 14:50:58.000',True,False,'������� ����� ���������','��� "�� "�����"','7826087992','1027810333242','(812)332-13-49', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lav231181@mail.ru', '985867BB53DB30E85CFB6776C143031844609D0C', 
 '2010-06-30 09:41:14.000',True,False,'������� ���� ����������','��� "�������������"','5216017454','1085254000930','9200156734', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kireeva@nrb.ru', 'E7AD9CFD57D7C44720DFAED8FEDECFDAAD8C05CF', 
 '2010-06-30 15:16:22.000',True,False,'������� ������� ���������','��� "������" (���)','7703211512','1027700458224','(495 )213-32-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Aleksandr.Smirnov@mbr.ru', '7D3F31FEDEEBB104684310CCFF72E235FD3CF4DB', 
 '2010-07-01 15:04:06.000',True,False,'������� ���������','�� "������������� ���� ��������" (���)','7744001218','1027739378600','(495) 287-35-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ludmila888@mail.ru', 'A6110C86496901265FC862CC52066F8B9E01D588', 
 '2010-07-01 15:09:23.000',True,False,'����������� ������� �����������','����������','7706696311','5087746010952','8(903)762-68-39', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@russipoteka.ru', '0803B81FAFD649DF8741D5246EC6021139C657A2', 
 '2010-07-02 14:22:38.000',True,False,'���� ��������� ����������','�� "������� ��������� ����" (���)','5433107271','1025400001637','684111', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'raida1974@mail.ru', 'ED4892943E027AC5C5A8339A27490DCB8799C53D', 
 '2010-07-02 14:36:47.000',True,False,'��������� ����� ������������','��� "������������ ����� ������������ ������������"','3017001784','1023000854293','8(8512)77-85-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'usb@msk.pvbank.ru', '5FBD1FFE5F4DEDE8BBBB5D1DAB55BE15A7813AF7', 
 '2010-07-05 17:55:51.000',True,False,'������� �������','��-���� (���)','7303008900','1027300001354','8422-42-16-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'belik@fora-capital.ru', 'F55D853DF96AA7D22865FA1E040CDD453977207B', 
 '2010-07-05 18:05:57.000',True,False,'����� ������ ������������','��� �� "����-�������"','7713273620','1027739283550','(495) 984-6844', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'i-semenova@yandex.ru', 'F54321B35FA13D0B321201447EBBBEF71FCC27A9', 
 '2010-07-06 10:20:22.000',True,False,'�������� �����','��� "�� ������"','7706140517','1027739165167','8 905 513 28 49', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dvn@slbm.ru', '57853559AD49E62D207E54007681A3A60BF4525E', 
 '2010-07-06 13:52:17.000',True,False,'�������� ������� ������������','��� "���������� ����" (���)','7722061076','1027739121849','(495) 745-8423', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'infrastruktura@mail.ru', 'B5F13DE2F1CF352F8C712D858AB3CFC27EC490B7', 
 '2010-07-07 15:31:22.000',True,False,'��������� ���� ����������','��� "��� "��������������"','4708002443','1024701478878','81368-34076', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tyurina@c-g-i.ru', 'EED78AAE9853D129A5042A5B01E1F17EB37D2545', 
 '2010-07-08 16:34:32.000',True,False,'������ ����� ����������','��� ������� "��������-������"','7715750741','1097746125641','8-9057015188', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'effi07@mail.ru', '1343E36245F72F6996087BEA6A7333F32BCD3E85', 
 '2010-07-09 09:51:50.000',True,False,'������� ������� ����������','��� "��� ����"','7707623789','5077746551438','8-495-6818017', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tde@ufs-federation.com', 'A66AA3B3FA282AC8D7BB3591913201CD19F0E1B8', 
 '2010-07-09 15:32:33.000',True,False,'������� ������� ����������','��� "�� "� �� �� ������"','7716605994','1087746607574','74957379391', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bacoff@intrast.ru', 'C7ADA3339FCC9B0A2265F5427A24D1CCE8B1DA5F', 
 '2010-07-09 15:33:36.000',True,False,'������� �.�.','��� "�� "�������"','7736030670','1027739328760','(495) 956-06-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ic-mashtab@mail.ru', '29E07B7501164FAEC744E084BAAAACAC90CE9134', 
 '2010-07-10 17:17:37.000',True,False,'������ �����','��� "�� "�������"','7705881752','1097746095974','(495) 644-64-71', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'svetlana@dfs.ru', 'BB3A5D6E1D6BABE19C2B5CB99F91C693D4506E05', 
 '2010-07-12 10:59:17.000',True,False,'���������� �������� �������','��� "��������-���������"','7018001248','1027000885339','3822-555038', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mil1612@mail.ru', '1162D61ED051BDE10196FF7F5A72F5DEA795DF3E', 
 '2010-07-12 16:05:52.000',True,False,'��������� ������� ���������','��� �� "�������"','3127000279','1063127013179','79194331888', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@apabank.ru', 'ACA81664C793F75DB2C26CF58BF24487438F12C1', 
 '2010-07-13 09:40:17.000',True,False,'���� �.�.','��� "�������" (���)','7705031219','1037700043732','+7 (495) 251-51-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'AShmelev@kbivanovo.ru', '8B35434394C7B0C84B7B321527FB37C0E251440B', 
 '2010-07-14 12:15:41.000',True,False,'������ �.�.','��� �� "�������"','3702062934','1043700028679','(4932) 32-58-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'company@zao-srk.ru', '61DE8B4205D95B2B7B29A2547AB6EDED73B19D66', 
 '2010-07-15 07:25:46.000',True,False,'������� ����� ������������','�������� ����������� �������� "��������� ��������������� ��������"','4217027573','1024201467510','(3843) 749138', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'T.Savenko@abr.ru', '03C7FC44328C1F5D5CF5DEE8ECFDA04DF47E9573', 
 '2010-07-15 08:27:16.000',True,False,'������� ������� ����������','��� "�� "������"','7831000122','1027800000084','(812) 325-63-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'umg@umg-nt.ru', 'F192B7D88E9EC1F5B78BDA82876C801C2BB55A65', 
 '2010-07-16 08:48:50.000',True,False,'������������ ����� �������������','��� ����������������','6623032788','1069623033116','(3435) 34-69-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'arbitr.63@yandex.ru', '0B560033058E0E31F91CEE9F457F078D997C6CD1', 
 '2010-07-16 09:58:26.000',True,False,'��������� ��������� ���������','�������� ����������� �������� "������������ ���� "���������"','6315226644','1086315008372','(846) 2411573', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'o.korshunova@spii-spb.ru', '84B88571CF58ED2E32162403057A96C4A507BD89', 
 '2010-07-19 13:28:41.000',True,False,'��������� ����� ���������','��� "����"','7813337814','1067847033858','(812) 230-85-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aravinskaya@alfacapital.ru', 'AB5AD2A5AE7DD4BA746EAA3FEE4A3C688C8DB385', 
 '2010-07-19 15:24:06.000',True,False,'��������� ���� ������������','��� �� "�����-�������"','7728142469','1027739292283','+7 (495) 797-31-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'maya.makurina@gazprombank.ru', 'C2650CC9746779CD9BA6F5A2E0B3DE080D4DF1A8', 
 '2010-07-20 12:55:09.000',True,False,'�������� ����','��� "�����������-���������� ��������"','7722515837','1047796382920','(495)980-4058', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tarasovan@fiabank.ru', 'D3C5460DA8480F16A647AD390B88B928A9ECC9E0', 
 '2010-07-27 15:55:55.000',True,False,'�������� �������� ���������','��� "���-����"','6452012933','1026300001980','84823650001121', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rvp@veb.ru', '4FE3B39D3060174431440CE63F7A90884DF58AA3', 
 '2010-07-27 17:21:19.000',True,False,'�������� ����� ����������','��������������','7750004150','1077711000102','74957219384', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'esajf@akbars.ru', 'FAA189233FB9FE45B64B1EF007C553D5E4F100A9', 
 '2010-07-28 10:11:36.000',True,False,'������������ ����� ���������','����������� ������������ ���� "�� ���� (�������� ����������� ��������)','1653001805','1021600000124','+7(843)2925285', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jurist@capitalkredit.ru', '116AE0BC3CCA1DBE6DA7C0914FE16C69B18F5CBD', 
 '2010-07-28 11:05:13.000',True,False,'������ �������','��� �� "��������� ������"','7718103767','1027739199927','495-795-07-60 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ndcbcnc@ncapital.ru', 'F12D076F315697EAE75D6F58639BFC9B16AE0D98', 
 '2010-07-28 11:20:08.000',True,False,'����� �.�.','��� ���������� �������� "��������"','7708598817','1067746571683','(495)544-41-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ndcucnc@ncapital.ru', 'F12D076F315697EAE75D6F58639BFC9B16AE0D98', 
 '2010-07-28 11:21:57.000',True,False,'�������� �.�.','��� �� "����-�������"','7719566165','1057748347315','(495) 544-41-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depozitar@avangard.ru', 'E26667F8BFE486B75BB67372BA15FC04FE6F2C3A', 
 '2010-07-28 15:23:12.000',True,False,'��������� ���� ����������','��� ��� "��������"','7702021163','1027700367507','49573773732273', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Y.Scherbakova@avangard-am.com', '16020BBEFF0E82EECAFA68E48EDD2D63A5DBBB04', 
 '2010-07-30 10:30:26.000',True,False,'��������� �.�.','��� "�� "�������� ����� ����������"','7707615756','1077746184450','(843)211-21-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga.samorukova@pioneerinvestments.com', '144C4E7FC34C637FF5BA6AA5404F8CB5D8E4560E', 
 '2010-07-30 10:58:56.000',True,False,'���������� ����� ����������','��� "������ ���������� ����������" ','7709729981','5077746308129','(495) 258 50 20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'i.emelyanov@greenfield.ru', 'B85FBE05FA06A9D9355123FCDC3454282D7E654F', 
 '2010-07-30 11:32:34.000',True,False,'��������� ���� ����������','��� "������������"','7701000940','1027700314113','(495)623-75-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'b.alekseev@intigr.com', 'BCCE217281414009BF4EE5C929CB07F3FD20105F', 
 '2010-08-02 10:21:15.000',True,False,'�������� ����� ����������','��� "���"','7730614158','1097746493129','-1332', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ifceconomica.ru', '60C1FDD24AD4E4BAFEEC6F7D7FE85E1D5620F696', 
 '2010-08-02 15:45:30.000',True,False,'������� ��������� ������������','�������� � ������������ ���������������� "������������� - ���������� �������� "���������"','7702715436','1097746564585','89031067817', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'begma@isbroker.ru', '33D6518082875DB4A46816F0C3787E559C64C471', 
 '2010-08-02 17:44:37.000',True,False,'����� ��������� ����������','��� "��-������"','7701733190','1077757844713','(495) 665-01-35', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Oil_ttdn@sibtel.ru', '09120F9FA871EA1C20E634662CAF1B5926F64414', 
 '2010-08-03 12:12:25.000',True,False,'���������� ������ �����������','��� "������� � ���������� ������ �����','7202037817','1027200807666','8(3452) 41-61-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf@aaanet.ru', '0B84F1796457F5CEAB309A9FF6C8790D25E0D7C1', 
 '2010-08-03 14:29:26.000',True,False,'���������� ��������� ���������','��� "����������"','6161022380','1026102899811','(863) 297 75 76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sazonova@vebcapital.ru', '73E39AC4165ECD5D294B05C295A0EBC62C123602', 
 '2010-08-03 15:40:48.000',True,False,'�������� �������� �������������','�������� � ������������ ���������������� "�������������� �������� ��������������� ("��� �������")"','7708710924','1097746831709','+7 (495) 987 67 10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Alakaevrr-rms@yandex.ru', 'F75F8578582E3EAC8FDDF288C2623CACD0B2B711', 
 '2010-08-04 09:54:45.000',True,False,'������� ������ �����������','���-������','8602657870','1058602122810','83462630546', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm@incredbank.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2010-08-04 11:29:11.000',True,False,'�������� �.�.','��� �� "����������"','7750005595','1107711000033','495-660-01-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ic-financeinvest.ru', '604CF0633C4E2408197CDF09020EDB625C106209', 
 '2010-08-04 12:03:36.000',True,False,'������� ��������� �������������','�������� � ������������ ���������������� "�������������� �������� "������������"','7708700490','1097746282787','8(495) 510-15-28', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'anna.osipova@expobank.ru', 'ABB064DB0B398CF7317C219F93AF6046D892A533', 
 '2010-08-04 12:22:57.000',True,False,'���� �������','��� "�������� ����"','7729065633','1027739504760','+7 (495) 2283131', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'trade@depozitpremium.ru', 'A61BD3A84502FC553B92DB7D913F3A526E8994A7', 
 '2010-08-04 12:33:07.000',True,False,'������ ����� ���������','��� "������� �������"','7728615820','5077746438677','+7(495)933-48-86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'trade@business-union.com', 'D168DEE026C679FC0581FC6B4A1959B6102EE681', 
 '2010-08-04 14:12:34.000',True,False,'��������� ������� ����������','��� "�� ������� ����"','7729630035','1097746153163','(495)933-48-85', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uwcbroker.ru', 'BA7644AD799FAB59BF5E8B2CFDD485A1CCE5EC7A', 
 '2010-08-04 17:13:05.000',True,False,'������� �����','��� "�������������� �������� "������� ����� �������"','7733635396','1077763934830','84012721702', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dr@iqcapital.ru', 'D3CEAF577D01F474BD89E47743F3DF468E1F2118', 
 '2010-08-05 13:36:21.000',True,False,'������� ����� �����������','��� �� "�� ��� �������"','7713615289','5077746438810','8 (495) 797-35-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Berdnikova@utb.udm.ru', '66F557B71F582806F315E0D37977FBC8901FAA81', 
 '2010-08-09 11:30:30.000',True,False,'��������� ������� �������','��� "��������� ��������� ����"','1831027349','1021800001190','(3412)510013', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kurashova_tv@banktc.ru', '8DF02B6A30576172D6484F47D47FD0047B4F876B', 
 '2010-08-09 14:05:03.000',True,False,'�������� ������� ���������� ','�� "����� ������� ����" ���','2302013017','1027739445535','495 787-93-97 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lvov@nfr.ru', '768263353CC4565403E45695A9F73160B5B36C93', 
 '2010-08-09 15:49:11.000',True,False,'����� ������� ����������','���� ������������','7702252795','1027700128488','8-926-585-21-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rip@sineft.ru', 'A5848436872BC48819AA01AF4C75020353C040AC', 
 '2010-08-11 11:15:00.000',True,False,'������� ������ ����������','��� "�����������������"','8602039063','1028600588246','(3462) 42-11-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ramzaev@viking.spb.ru', '8A07C5AEEA8220BEA748B4F534E6404BA4A1183D', 
 '2010-08-12 10:12:01.000',True,False,'������� ������ ����������','��� "��� "������"','7831000098','1027800000250','320-33-20 (7504)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yazykova@kbmil.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2010-08-12 10:14:41.000',True,False,'������� ��������� ��������������','������������ ���� "��������� ����" (�������� ����������� ��������)','7744002839','1027744004617','(495) 788-98-68 2131', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@energoholding.biz', '44A3CB502F4A17A7F2E81B3AC66BCB3240A1E25F', 
 '2010-08-12 10:16:32.000',True,False,'������ ��������� �������������','��� "�������������"','7715781186','1097746685398','(495) 649-65-71', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'cb5@itbank.ru', '69655D23AC603DE5E4EB59D4AA2BFFF9760C8F72', 
 '2010-08-12 12:09:29.000',True,False,'�������� �.�.','��� ��� "�� ����"','5503008333','1025500001163','(3812) 24-70-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena3503@gmail.com', '0340A7001CE208083FB764EAE03D3FCF8C106702', 
 '2010-08-13 11:10:58.000',True,False,'��������� ����� ������������','��� �� "���������� �������"','7715695635','1087746476212','8-906-095-28-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'titova@zapad.ru', 'C9DCD8B2E261861A2C521084EC14CB47D4DF4101', 
 '2010-08-13 12:50:30.000',True,False,'������ ������� ����������','��� �� "��������"','7731015830','1027739585422','(495) 620-94-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'savinkina_ie@tm-trust.ru', '425B8D17FBDEB3E336E0AA9CC41739C715647DF3', 
 '2010-08-13 13:22:02.000',True,False,'��������� ����� ����������','��� "��-�����"','7706566601','1057746132674','(495) 6420965', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dpoznyak@gmail.com', 'DDBACD10AB2C57C081AC4C161D23743B018F81EE', 
 '2010-08-14 17:49:04.000',True,False,NULL,'������������ #2 (������)','6658311197','1086658014024','NULL', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'katodvluki@mail.ru', '2F315EFA4EC4E38B835A117D90077474D5B38652', 
 '2010-08-16 12:14:12.000',True,False,'�������� ���� ����������','��� ��� �����-��','6025010474','1026000899759','8(81153)92708', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lesservis_l@mail.ru', '09A91119000AF865F4E13EEF80D6B95C4279718F', 
 '2010-08-16 20:34:36.000',True,False,'�������� ������� ���������','��� "���������-�"','7735095900','1035008854704','79153922890', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'u1u2u3@mail.ru', '1548835786115233AF979F90163895A14D96F422', 
 '2010-08-17 10:23:46.000',True,False,'��������� ���� �����������','��� "�������� ������ "������"','1435189586','1071435010492','8-914-233-64-05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'iakatyeva@rosenergobank.ru', 'FC51BB445B9A482A4BE29315CF250D167227C911', 
 '2010-08-17 16:41:32.000',True,False,'�������� ����� �����������','�� ����� (���)','6167007639','1027739136622','(495)918-16-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kolesnikovaes@kibank.ru', '3BA8236DC2D367A60AFC82853FDD8DAD4C4D4D0D', 
 '2010-08-19 12:52:15.000',True,False,'����������� ��������� ���������','��� "��������������"','2309074812','1022300000029','(861) 210-49-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lbsk@mail.ru', '88478EFFB11F18FA860F24527CDAA04A72B33419', 
 '2010-08-24 13:52:47.000',True,False,'�������� ������� ���������','��� "��"','7310000684','1027300784818','(34767)63715', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kruglov@obiks.ru', 'A68D8EDE7C3E8B9A6A91CFB3DA3F9448A92048AB', 
 '2010-08-24 15:17:26.000',True,False,'������� ������� �����������','��� "�����"','7732504030','1057746429696','(495)3801138', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'S_V_Krylova@ific.ru', '5879DA6AE70D46A9C56769F9C326A4DBF67D08C7', 
 '2010-08-26 16:53:16.000',True,False,'������� �.�.','�������� � ������������ ��������������� "���������� �������������� �������� "�����������"','7708158767','1027739019296','(495) 935-83-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ATP-UGL@mail.ru', '3DEC68E398D0723A428797304094566A60F6A401', 
 '2010-08-27 09:45:09.000',True,False,'����������� ���� �����������','��� "��������� ���"','2283004078','1022202283674','83857922203', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@sdkgarant.ru', '873CBA0C33B15C81062CABB2164945E04604803E', 
 '2010-08-27 19:01:15.000',True,False,'������� �.�.','��� "��� "������"','7714184726','1027739142463','(495) 777-56-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'KorotaevAV@sinara-group.com', '23B01C0CB90D606BA4951E8383875763668E476C', 
 '2010-08-30 11:44:26.000',True,False,'�������� ����� ������������','�������� � ������������ ���������������� "����������� �������� ���"','6671222690','1076671014518','(343) 310-33-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'miv@sbrfreso.ru', 'A002D180FAA834777F53BC783BDE7EDCE888678B', 
 '2010-08-30 14:46:02.000',True,False,'������� ����� ����������','��� "�������� ����"','7709383740','1027739519708','(499)6811010', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a.grishin@expocapital.ru', '4A7B9AA83F3455A90C6A8F31057ECF55DE64FABF', 
 '2010-08-30 16:35:45.000',True,False,'������ ��������� ����������','���� "����� �������" (���)','7750005845','1137711000096','(495) 258-61-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf@nsbank.ru', 'A0212FBC1ECF3388BC364D35C1793F7BC99A41F9', 
 '2010-08-31 12:34:02.000',True,False,'��������� ����� �������������','��� "����� ���"','7727220570','1037739724021','(499) 128-83-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tsuvorova@solidinvest.ru', '8984E9CCFC2BC7D2B52C62D2D031CF1FAE34E86D', 
 '2010-08-31 15:25:48.000',True,False,'�������� �������','����� ����������','7706150949','1027700227180','228-70-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivanova-sarsonmc@yandex.ru', 'A1F649BC916E4823B3FD32E46E9527A28BE4A3A7', 
 '2010-08-31 15:33:36.000',True,False,'������� ������� ����������','�������� ����������� �������� "����������� ��������" ������"','7715770882','1097746496726','8-903-550-66-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Nadezhda.Statsenko@pens1.ru', '5C4FE15ECA9F5E5AC4F9D76A83944EF38C37149D', 
 '2010-08-31 15:37:25.000',True,False,'������� ����� ����������','�������������� ����������� "������������� ���������������� ����������������� ���������� ����"','7107035601','1027100973976','(4872) 36-24-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'feo@npf-almaz.ru', '916647AF7C2724B2F4519D5336F143A4C7704C5A', 
 '2010-09-01 04:00:00.000',True,False,'������� ���� �����������','����������������� ���������� ���� "�������� �����"','1433011269','1021400968555','(41136) 3-23-91', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@vialdi.ru', 'F2BA99985E160DDDA9A54213DF3A44E2FDA3A4A1', 
 '2010-09-01 10:14:23.000',True,False,'�������� �.�.','��� "�� "�������"','7717547664','1057749530937','(495)5079085', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lobanova@mcenter.ru', 'C59557453F00D33C768C08EBEFDC429752B435B5', 
 '2010-09-01 12:16:45.000',True,False,'�������� ����� �����������','��� "����������� �������� ����������-�����"','7702160110','1027700049981','(495) 733-91-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'allaz@mconsulting.ru', '0A2DA294162EF2BE4869B92727B971FC03B796A0', 
 '2010-09-01 12:25:32.000',True,False,'������ ����� ���������','��� "���������� ����������"','7702203678','1027700050168','(495) 733-91-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buh@npfresurs.ru', '4E0B836C669AEC1CEE5D371D1065CC3D7CD68A22', 
 '2010-09-02 17:10:42.000',True,False,'������ ����','��� "������"','6163021086','1026103166748','78632645956', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Buh6_sp@NpfSberBanka.ru', '374C89B70691F4528323092E56E4D1270D86F83C', 
 '2010-09-03 11:36:16.000',True,False,'������� ��������� �������������','����������������� ���������� ���� "���������� �������"','7714110227','1027739250659','785-38-87 (10-59)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pf@chelinvest.ru', '0300D5EEEE5006B2923966F86B1F470190940527', 
 '2010-09-03 13:00:22.000',True,False,'��������� �.�.','��� "�������-������"','7451050350','1027402906520','(351) 261-23--84', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@lhcap.ru', 'A20485C7AA81E4E32A57DC8C75AD4696078CFF96', 
 '2010-09-03 13:31:19.000',True,False,'������� ��������','�������� � ������������ ���������������� "�������� �������"','7706678802','1077764218960','787-16-36 4746', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@alcapital.ru', 'B0D42A489E926C7A394B3D4A65F5446BAFDC3FB6', 
 '2010-09-03 16:14:27.000',True,False,'������ ��������� ����������','�������� ����������� ��������  "����������� �������� "������ �������"','7730539863','1067746478766','4991484835', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Fcc@funds-custody.ru', '36B7CEC99D4C6A38DF77BFF9828961272D37369F', 
 '2010-09-06 10:44:31.000',True,False,'������ ��������� ����������','�������� � ������������ ���������������� "����������� ����������� ������"','7816097864','1037843110690','(812) 6435505', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vik_invest_k@mail.ru', 'BADEFA2716FA3FDF5CE0901D8F571F0F97E4395A', 
 '2010-09-06 15:44:18.000',True,False,'�������� ����� ��������','��� "�������� �������������� ��������"','2126003765','1022101131645','(8352) 41-44-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npfdoroga@aaanet.ru', '95BDAD789C70DB0FE4AD55F348EEFC056AEDBC14', 
 '2010-09-07 09:56:24.000',True,False,'��������� ����� ��������','��� "������"','6167014065','1026104139500','(863) 291-00-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf230@fondgub.ru', 'F580A60B348324B990A9A4B6EFDC5F8E6FB18DEE', 
 '2010-09-07 14:58:50.000',True,False,'�������� �������� ����������','��� "����������"','6317023791','1026301418538','3108230', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@progress-finance.ru', '901601F8D1B5ED35250BBDEAAB2E58731896C54F', 
 '2010-09-07 15:35:21.000',True,False,'������� ���� ���������','��� �� "��������-������"','7701640763','1067746204162','(495) 502-9423', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivl@vfp.ru', '94D1E8A403DDF3569E53EDDEA5D04DFCD66D5F24', 
 '2010-09-07 15:38:33.000',True,False,'����� ����� ����������','��� "���� ���������� ����������"','7713093113','1037739000716','8 499 256-51-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'titan@infopac.ru', '3425E8A33610BD92BD898256FA9777824BCCB695', 
 '2010-09-07 15:40:08.000',True,False,'�������� ���� ����������','��� "�����"','6322016373','1036300999327','88482229596', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'trust@esoo.ru', 'F617D68C7BED1F52855A4FCEB4DB84194CD8C1C9', 
 '2010-09-08 07:47:45.000',True,False,'����� ������� �������������','������������ ����������������� ���������� ���� "�������"','5610009275','1025601025713','(3532)77-53-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dnv@npf-mosenergo.ru', 'EB47C89192C8720FDC3091A4AC28B5815EE4131D', 
 '2010-09-08 15:00:59.000',True,False,'������� �.�.','��� �� ���������','7705013763','1027700211208','(495)287-16-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond@gpbf.ru', '1692D958093527AA117487917564981CBDBD12C3', 
 '2010-09-08 16:00:47.000',True,False,'�������� ������� ������������','��� "�����������-����"','7728528110','1047796809796','8 (495) 661-58-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'm.maksimov@npfts.ru', '4CCD7811DBE54FC2E1C1CB4208E8A26C78F9ABC3', 
 '2010-09-08 17:07:18.000',True,False,'�������� ������ ���������','����������������� ���������� ���� "�������-����"','7825349490','1027809183852','937-27-47 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'karpukhina@kapital-pif.ru', '98EE285B1425E9CACBF29DBDF0F370DA3B8EFABA', 
 '2010-09-09 10:52:00.000',True,False,'��������� ������ ����������','�������� � ������������ ����������������  "����������� �������� �������� ������ �������������� �����"','7702513045','1047796009128','(495)745-51-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uklidercapital.ru', 'EBE18E37377738E1D5BE9796C56D46DE0B6AD528', 
 '2010-09-09 14:36:31.000',True,False,'������� ������ �����������','��� ����������� �������� "����� �������"','7706591164','1057748018954','(495)740-29-91', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'st_rost@aaanet.ru', '9A630F5EA736799593534B625EF51C68FE6032B2', 
 '2010-09-09 14:59:04.000',True,False,'����� ����� ����������','��� ��� "������-����"','6166016158','1026100001938','(863)253-55-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'izhukovskiy@aurora-am.ru', '103B81A9FAB3381E96ABC0A8DD0DC188B3985354', 
 '2010-09-10 11:52:57.000',True,False,'��������� ���� ��������','��� "������ ���������� ��������"','7707634460','1077758546690','(495) 748-01-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npfkub@mail.kubtelecom.ru', '6364F348DF052575EEE785CB4D9A2BB7A27188D7', 
 '2010-09-10 15:04:03.000',True,False,'�������� �������� ��������','��� "��������� ���������� ����"','2308026830','1022301200492','(861)2550280', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@riccapital.ru', '3295E88C553D71A53754E38360AAD313FE36348D', 
 '2010-09-10 15:30:58.000',True,False,'�������  �������� ����������','��� �� "��� �������"','7706625945','1067746806082','495 933-22-22 114', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'hmeleva@nrbank.ru', '07A321439EBD9DADC76CE2C085FE3DE8324D52DA', 
 '2010-09-10 16:01:44.000',True,False,'������� ����� ����������','��� �� "�� ����"','7744000172','1027739627750','(495) 797-92-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kuzina@inpf.ru', '18FE311B570DBC33D75A8499878A0FDB28F798D1', 
 '2010-09-13 16:03:15.000',True,False,'������ ����� ����������','��� "��������������"','7705592126','1047796245782','495 660 82 24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rudaevani@uralsib.ru', '579B7973F350C8D082559B2992018FAA67F86313', 
 '2010-09-13 16:33:45.000',True,False,'������� ������� ��������','��� "�� �����"','7704212090','1027739001531','(495) 788-66-48', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'inf@capital-style.ru', '6B15F1FD3691ED2C0E56B9B56357C07CF42A2DCF', 
 '2010-09-14 07:20:59.000',True,False,'���������� �������� ���������','��� �� "�������-�����"','7706652480','5077746420692','(343)253-54-03(04)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'BabkinaNB@npfn.ru', '2B48E4AB1B63A11B355AE2AC3EEFA3B3DEDAF46E', 
 '2010-09-14 10:13:38.000',True,False,'������� ������� ��������� ','��� "��������"','7706019126','1027739137766','(495)7834784', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ukevraz@yandex.ru', 'DE8ECB20AA9EB344A77469F10E8415799DF184B5', 
 '2010-09-14 12:04:30.000',True,False,'�������� ������ �����������','�������� � ������������ ���������������� "����������� ����������� ��������"','7707613163','1067761460030','8-916-249-24-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sed@npfsr.ru', '92D8E2D2FE6A355F504ADCB1C5D957C9A7990D34', 
 '2010-09-14 12:31:46.000',True,False,'��������� ����� ����������','��� "���������� ��������"','4826021717','1024800834178','8 (4742)23-20-38', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tatyana@realtyfunds.ru', '33F89FE0338DDCCBD653004D73FECB2384ACA98B', 
 '2010-09-14 13:57:44.000',True,False,'��������� ����� �������������','��� "��������� - ����� ����������"','7702335547','1027702001502','84956459667', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npfprs@sovintel.ru', '7A3F5864E43A5E8641D6CE6A4957A7550AA39168', 
 '2010-09-14 15:14:50.000',True,False,'�������� �. �.','��� "���������������"','7722241576','1027739177630','641-38-57', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'na.ilykhina@sgcm.ru', '4B0860A382C39EF28402980655E19A6E5003734D', 
 '2010-09-14 17:13:55.000',True,False,'������� �.�.','��� "�� ����"','7713648502','1087746425535','+7495 662-70-20 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '3u@1npf.com', '78A623392300B0AE4EAB99282AAFCF15819AB651', 
 '2010-09-16 13:47:47.000',True,False,'������� ������� ���������','��� "������ ������������ ���������� ����"','7724186930','1027700539503','(495)603-29-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nnpf@nnpf.ru', 'F63331AD34FD7469769B51045C445C1D7405A73A', 
 '2010-09-17 15:45:17.000',True,False,'��������� ������ ����������','����','7726260499','1027739004897','233-02753', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lera695@gmail.com', 'E9243108F62ABFDDE238C581500195757252005E', 
 '2010-09-20 08:39:10.000',True,False,'�������� ������� �������','��� "����������� �������� "�������"','6452068044','1026402670909','8452283926', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'brokar@mail.ru', 'E5145D40FEC1B1D183AD839FB2130402A2E61D7E', 
 '2010-09-20 11:41:03.000',True,False,'�������� �.�.','��� "�����"','6905080003','1026900516026','323662', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@tkb-bnpparibasip.com', '5BE39A59DDD12D016028446A07C46AF2073BF08B', 
 '2010-09-21 15:43:19.000',True,False,'������� ������� ����������','��� ��� ������ ���������� �������� (���) ','7825489723','1027809213596','(812) 332-73-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'abatasheva@trust-centr.ru', 'F82599F536595031C190F859709EE14BCA4C00EE', 
 '2010-09-21 17:35:04.000',True,False,'�������� ��������','��� "�� "����� �����"','7701853176','1097746641673','+7 (495) 771-32-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'trunilin@kb-rtb.ru', '9AE3B82137E35859B88B37EEC9DE680A922F0E44', 
 '2010-09-22 11:22:09.000',True,False,'�������� ��������� ���������','��� "������� ��������� ����" (���)','2627016420','1022600000158','(495) 782-62-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'livshitc@bk.ru', 'F393BBFF49D470D203223CD6EABDB8C3EF12284F', 
 '2010-09-27 16:20:00.000',True,False,'������ �.�.','��� "����������� � �����"','7709049873','1037739744745','(499)975-34-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'semen_off@list.ru', '04714D08BE20E0CCEE2A4984A6A223A915BF4C8E', 
 '2010-09-27 16:43:25.000',True,False,'���� �������','���� ���� (����� ���)','7707526376','1047796659327','467', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'soloviev.roman@suntelfinance.ru', '6CB6048747E76A4491F317F06CE5CACFD4B58FC5', 
 '2010-09-29 17:14:56.000',True,False,'�������� ����� �������','���"������ ������"','7708705868','1097746448029','+7 (926) 698 08 37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sberbank-fa@sberbank-fa.ru', 'E85401C4AEFA430F469146F736D4E4F32E90B760', 
 '2010-10-01 12:15:44.000',True,False,'���������� ����� �������������','��� "��������������� ���������"','7736618039','1107746400827','(495) 913-17-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@agana.ru', 'FCE4C050F5103985F6FC8E59AFBB2B23D2022703', 
 '2010-10-01 14:53:38.000',True,False,'������� ������ �����������','�������� � ������������ ���������������� "����������� �������� "�����"','7706219982','1027700076513','8(495)9801331', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'katya@tppfund.ru', 'E8BAC498C27B2AE5BBC8C9EB56CAFE651E15BD19', 
 '2010-10-01 14:57:28.000',True,False,'��������� ��������� ���������','��� "�������-������������ ���������� ����"','7710432583','1027710001142','(495) 915-53-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'filatov@bcs.ru', 'EF6AC3063576D63381134BDB923397F5D7D3C771', 
 '2010-10-01 16:38:39.000',True,False,'������� ����� �������������','��� "�������� ���"','5406121446','1025402459334','(383) 223-55-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'n.gafurova@tkbc.ru', '520B74BE4B2AA2517C59152EA5A1FEA75018867D', 
 '2010-10-04 11:44:29.000',True,False,'�������� �����','��� ������� (���)','7704750813','1107746255352','9813430', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '4erniy@bdkbank.ru', '446752B5C9EEF0DB4F78665210221293ED1E570C', 
 '2010-10-07 11:09:58.000',True,False,'������ ��������� ����������','��� "���� ������������� ������������"','7744001955','1027739064396','(495) 223-44-00 (250)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'guseva@dsk1.ru', '5D337D9923BA4D03D06BD8E846BF7DC73AA7E1F4', 
 '2010-10-12 16:19:14.000',True,False,'��������� ������� ����������','����������������� ���������� ���� "�������"','7707030250','1027739161988','89160129396', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'y.antonova@ricfin.ru', 'E484F06841468B620F01886B0CA7BC8ED83F9A0E', 
 '2010-10-13 16:09:04.000',True,False,'�������� ���� ����������','��� "���-������"','7721618854','1087746506990','933-22-22 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'litvinyuk@bigpension.ru', '87DDAA198C083ADD6E35B136FD9FF08B3BB25BD8', 
 '2010-10-14 09:37:14.000',True,False,'�������� �.�.','�� ���','6166019374','1036166003334','933-52-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mosina@tnk-vladimir.ru', '4C3049873D458FFA8C1211F535979AC18B6BB395', 
 '2010-10-14 11:12:55.000',True,False,'������ ����� �������','��� "���-��������"','3328301565','1023301457948','933-52-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'd.zeleneva@zpif.ru', '208C42933F2E42590598958274D0A43E7757CEB9', 
 '2010-10-14 19:16:51.000',True,False,'�������� ����� ����������','��� "����������� �������� "���������������"','7706402272','1037706053351','(495) 796-88-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mpolunina@ukprofinvest.ru', '450AEFE7EE518F111BD7D4160A6D729F9310C898', 
 '2010-10-15 10:36:53.000',True,False,'�������� ����� ������������','�������� � ������������ ���������������� "����������� ��������  "���������������� ����������"','7710646105','5067746900909','(495) 786-88-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'D.Baranov@npf-opk.ru', 'D677CC6173BE6034598E3CA06F79983266AD8377', 
 '2010-10-19 13:01:12.000',True,False,'������� ������� �������������','��� "��� ���"','7804036828','1037808016278','(812)329-4401', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'reports@npfsberbanka.ru', '7926C602AA3573033DC5D190964EBABFA8189D2C', 
 '2010-10-20 10:50:45.000',True,False,'������� ��������� �������������','��� ���������','7707072411','1027739317924','785-38-87 (10-59)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Diana.Lazutkina@npflg.ru', 'A0EFA4563FE8A749A9A054B1064E2C067F0D8B34', 
 '2010-10-20 14:18:10.000',True,False,'��������� ����� ���������','�� "��� "������-������"','7708059100','1027700419449','411-55-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Juliya.Mozhanova@pens1.ru', '7F83E3CBDDB390DE6E025C9115425B2FA3E168EB', 
 '2010-10-20 14:22:09.000',True,False,'��������� ����� ���������','�� ��� "���������� �������"','5250006533','1025201982960','411-55-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'reshetnikova-l@yandex.ru', '9A3DCD5CC6830D8B703672EC17D2EA9A88377DAD', 
 '2010-10-22 10:43:09.000',True,False,'����������� ������� �����������','��� ��� "��������� ��������������� �����������"','7202010861','1027200780749','8-912-391-84-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'irina.nedilskaya@namc.ru', '5250A95EE4D550465169CEB34293085905487DF7', 
 '2010-10-22 12:15:56.000',True,False,'���������� �����','��� "������������ ����������� ��������"','7716219043','1027716000366','(495)662-70-30', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'redrum187@mail.ru', 'F3CC187311E54B54EF9DEB11CB52945AAF92D10C', 
 '2010-10-23 07:09:29.000',True,False,'�������� �����','����� � ���','7721135286','1037739297210','89151253537', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a.torgashova@mail.ru', '08634231C4A23A34F0CC6984DF670AD46D8D52B0', 
 '2010-10-25 12:47:30.000',True,False,'��������� ���� ���������','��� "�������"','6315201086','1026300958606','270-61-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ykk@adtinvest.ru', 'F78142829C584B4D03EDE7E9BA3E9DE5BF67CDC5', 
 '2010-10-25 17:10:52.000',True,False,'������� ������ ��������������','��� "�� "����-���� �������"','7826153807','1027810275349','(812) 677-85-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk@alpinexmanagement.ru', '6DDB10D2609AC8D8AF13EB2586FDBAE8BA9B1FC6', 
 '2010-10-25 17:55:17.000',True,False,'����������� ������ ','��� �� "��������� ����� ����������"','7710630225','1067746645350','(495)-660-22-96', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@am-pifagor.ru', '54448899C2D266CB4F05EB917D3189E730413A7E', 
 '2010-10-26 08:39:24.000',True,False,'��������� �������� ���������','�������� � ������������ ���������������� "����������� �������� "�������"','7447136484','1087447011497','(351) 796-64-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Voloshina@mpbank.ru', 'F95D88B90F1CC03FFA3E31AECB2B31CAB0B9C3D5', 
 '2010-10-26 13:37:54.000',True,False,'�������� ��������� ��������','�� "��������������� �������� ����" ���','7709044402','1027739326911','(499)-267-72-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond-ugol@mtu-net.ru', 'E2C2D69D46CDC3C61A9CFD56A46730C187B228CB', 
 '2010-10-27 10:56:48.000',True,False,'������������� �.�.','��� "�����"','7704005985','1027739075341','(495) 625-07-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Aleks3055@yandex.ru', '5251EE3426889E177091FB1BC79B1FEF56EC8D44', 
 '2010-10-27 13:40:16.000',True,False,'��������� ������� �����������','��� "�������� "����"','2311016712','1022301811432','(861) 252-11-28', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'penfosib@mail.ru', 'B9C26D996F8408BACDFC3F6C9AE4311EBC4FC7A3', 
 '2010-10-28 13:15:58.000',True,False,'��������� ����� ���������','��� "�����-����!','4214005099','1024201389816','(384-75)2-93-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bonaqva000@mail.ru', '335C4B5FE1284216DC9723EA1640A1FBAE15573A', 
 '2010-10-29 08:22:13.000',True,False,'��������� ���� ����������','��� "��������� ����������"','5426100162','1025405012368','89232402056', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@infra-invest.ru', '21F2812A07C404AC2D4D1FDF229C457661D1AD3A', 
 '2010-10-29 12:18:28.000',True,False,'��������� ����� ���������','��� "�� "���������������� ����������"','7707525541','1047796624083','(495)648-47-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'main@astercom.ru', 'D476053722A8EB9929DD636C920E54C9FA62DFAF', 
 '2010-10-29 13:57:02.000',True,False,'������� ������','��� �� "��������"','7719273031','1037719026113','(495) 626 0578', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'I.Sitkin@tfb.ru', 'BA07F20BF52A862AEA47403342AF11E81D1BC1C7', 
 '2010-10-29 15:32:41.000',True,False,'������ ���� �������������','�������� ����������� �������� "����������� �������������� ������������ ���� "�����������"','1653016914','1021600000036','(843) 2911692', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aleksander_khasin@rambler.ru', '8F4E6DB39B7BC777B939976A1805CBF65EAA1DBE', 
 '2010-11-01 09:10:35.000',True,False,'����� ��������� ����������','��� �� "������"','8506001518','1038500597772','9642694117', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ilyina@vtbnpf.ru', 'B0F7F10201B64F1B00D2E2F3369170A7EFC283DB', 
 '2010-11-01 13:49:56.000',True,False,'������ ����� �������','�������������� ����������� ����������������� ���������� ���� ��� ���������� ����','7714007734','1037739183470','228-09-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ndc@baltica.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2010-11-01 16:15:06.000',True,False,'������ �.�.','��� "�������" (���)','3900000834','1023900001993','8-495-984-55-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'roschin@trustunionestate.ru', '0A73BFA8A977EF570B55AFA0EAFAA2C6DB61C6EC', 
 '2010-11-02 10:46:05.000',True,False,'����� �������� ������������','��� �� "���������� � ����� ������������"','7713615507','5077746454660','(495) 6578677', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ukfc.ru', 'D22247004EE5475936E386383A2396F31E0ED9C4', 
 '2010-11-02 12:01:58.000',True,False,'�������� ������ ����������','�������� � ������������ ���������������� "����������� �������� "������ �������','7708521733','1047796241272','745-8736', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mustafaev@trustunionam.ru', '722521487B7D01ABF671139C27DE4045E3BE9C2E', 
 '2010-11-02 12:45:43.000',True,False,'��������� ����� ���������','�������� � ������������ ���������������� "���������� ����� ����������"','7705794926','5077746882384','(495) 641-44-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm@ingosinvest.ru', '808658E6BBEB347E02B38EAE894567E39762DDC9', 
 '2010-11-02 15:22:10.000',True,False,'������ ������ ������������','��� �� "���������� - ����������"','7705136973','1027700339666','(495) 7204898', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jruklinskaya@ambbank.ru', '3C47025474E883ABFC2819434E42C0E8A391DD87', 
 '2010-11-08 12:34:51.000',True,False,'���������� ���� ������������','��� "��� ����"','7723017672','1027700034372','(495)651-63-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'khardin@centrotrust.ru', '71E1110C00C78195566EDC78259D43492A30F7CC', 
 '2010-11-08 14:39:35.000',True,False,'������ ����� ����������','��� "�����������"','7715667525','1077760573923','(495) 640-12-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nlapina@sheksna.com', '007952B66FE795A8A34C2201696FE3EA10A83014', 
 '2010-11-09 11:26:26.000',True,False,'������ ������� ����������','�������� ����������� �������� ������������ "�����������"','3524001230','1023502290514','(81751) 2-74-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nadezhda.vinogradova@kolaregion.ru', '45EE4D277F30C0AFA4F1DC1FD30A61804286DC81', 
 '2010-11-10 13:31:44.000',True,False,'����������� ������� ����������','��� "�������������"','5105041148','1055100064480','8-8152-69-25-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'examen@educenter.ru', 'FB17E8F332A2A92017E8D826C951D1DD60DFD16F', 
 '2010-11-10 14:28:59.000',True,False,'��������� ������� ��������','��� "������� ����� ���"','7701314720','1027701012965','964-94-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'etk@mcd.ru', '86493FA8990D7E147BE7560DD247D608CD0BD020', 
 '2010-11-11 13:42:40.000',True,False,'���������� ����� ���������','�������� ����������� �������� "�������������� ��������� ��������"','7701870485','1107746226785','8(495)221-13-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@euro-trust.ru', 'D646600105B12826506AE37748A63B14020034CB', 
 '2010-11-11 14:30:30.000',True,False,'������ ������� ������������','�� ��� "���������"','7727514594','1047796459952','7(495)7276360', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vestafinans@mail.ru', '2D3632931DDDADC3095E469C3D9FCCF2B5F3E0BE', 
 '2010-11-11 21:50:46.000',True,False,'��������� ����� ����������','��� "�� "�����"','7714800439','1107746084269','89268269616', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@finpartners.ru', '04CB3BB698017F869A91DAA50F5FD991B230BBA2', 
 '2010-11-12 09:41:55.000',True,False,'����������� ����� ��������','��� �� "��������� ��������"','7722557234','1057748376190','+7(843)5189918', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'N.valiullina@peramo.ru', '4023D9263BF9F969A73D7DD0F4983CB5E3AF0DAD', 
 '2010-11-13 10:45:02.000',True,False,'���������� ������� ���������','��� "������"','7716618947','5087746367143','749-65-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena_m@skrin.ru', 'DAED822C59E0ACE71CF257EEF3EF72EF03E63414', 
 '2010-11-15 16:15:50.000',True,False,'��������������. ����� ���������','��� ������� ����� "�����"','7701233888','1037739652939','(495) 787-24-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'merk@kulz.ru', 'A52E35326A0F741A722A9035EF71A0B2F2FA9D47', 
 '2010-11-16 07:07:53.000',True,False,'��������� ��������� ������������','��� "�������-��������� �������� �����"','6666000100','1026600931675','(3439)375-675', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Galinskaya-NV@mrz.ru', '7BCD70BFC3CB683A12C5218DBD08D7F7B6B6D3E9', 
 '2010-11-16 09:49:46.000',True,False,'��������� ������� ����������','��� "��������������� ��������������� �����"','1901003859','1021900520883','7257514', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'golovanov@tensor.ru', 'B15F11A1F62A707DE4B4A7668679CDFB8DD2A46B', 
 '2010-11-16 16:06:20.000',True,False,'��������� ������� �������������','��� "�������� "������"','7605016030','1027600787994','89159797401', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'i.farhutdinova@investcapitalbank.ru', '4A84A3BD816C543168006778DF5A14FAF9E99DFA', 
 '2010-11-17 12:03:30.000',True,False,'������������ ����� �����������','��� "�����������������"','278129399','1060200012685','8(347)2913761', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aaksenenkova@raiffeisen.ru', 'EE5818A55ED39F9A4FF8A569E56A1F9D67E97D3A', 
 '2010-11-18 11:06:44.000',True,False,'����������� ����','����������������� ���������� ���� "����������"','7714011709','1027700181794','+7(495)745-52-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kovalevskaya@ifru.ru', '3C2412E737593897A5E1A3DDDA1A32580124D509', 
 '2010-11-18 11:06:56.000',True,False,'����������� ����� �������������','���� "�������� ��������� ����� � ����������"','7710264307','1027700197051','(495)797-95-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@aruji-am.ru', '8A31F9B5D258A56C78DB9EEF6420738E258764F7', 
 '2010-11-18 17:30:17.000',True,False,'���������� ����������','��� "������ ����� ����������"','7706627396','1067746901540','(495) 644-40-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@meridian-invest.ru', '5DAA9559B685E8EAE6FFFA518BD5E95A613E714A', 
 '2010-11-19 12:33:24.000',True,False,'������� ��������� ����������','��� "��"�������� ������"','7717573914','1067760011857','(495) 545-38-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'churina@alor.ru', 'EB36A35F6015F24AE7A075ACD63CEE4A94F2080B', 
 '2010-11-21 17:07:26.000',True,False,'������ �.�.','��� "����-��������"','5260105974','1025203032304','(831) 419-86-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'afanasiev@mvbank.ru', '3BFA12EA054B2F307FE30C46ABE70C33F38E64B3', 
 '2010-11-22 10:23:58.000',True,False,'��������� ������� �������','��� "���������� ���������� ����" (���)','7717005245','1027739481362','(495) 645-35-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'permzoovetsnab@yandex.ru', '8B16EEAA38CBA59F58EB02D15E386EDED5B0B3ED', 
 '2010-11-24 11:25:09.000',True,False,'������ ����� �������������','permzoovetsnab','5905237118','1055902872299','8(342)226-58-06', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@partner-am.ru', 'AAFF09D9A669AA8A6C2B7CCDF40C6C87E1402AEA', 
 '2010-11-24 13:59:52.000',True,False,'��������� ����� ����������','�������� � ������������ ���������������� "����������� �������� "�������"','7743665390','1077761975807','(495)783-74-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@primbank.ru', 'EC036FBF3DF4C1F6D04D1D399C89A2BC28A6E990', 
 '2010-11-25 03:04:15.000',True,False,'��������� ����� ��������','��� ��� "��������"','2536020789','1022500000566','(423) 2228651', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@acpt.ru', '9C1772CFF35EB074A48CBC9809BE818FFDA288E2', 
 '2010-11-25 14:40:45.000',True,False,'������� �.�.','��� "������-���"','6320007704','1026301976854','8482 33-89-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@npf-atom.ru', '4498AB534DE49F6550687F79266432CB01222DFB', 
 '2010-11-25 15:02:58.000',True,False,'������� ����� �����������','��� "����������"','7705002151','1027739425207','+7 (495) 666-29-45', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'iklebanova@generalippf.ru', '57790F874C28A983E815273CE2A0A4F71DB56464', 
 '2010-11-26 15:13:39.000',True,False,'��������� �.','��� "��������� ���"','5023004963','1025004913152','(495) 785-82-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Ksenya.Shikina@belon.ru', 'F562DD95B4CAFDE6C926C38DBF4521888E3F634D', 
 '2010-11-29 13:29:33.000',True,False,'������ ������ ����������','��� "�����"','5410102823','1025403902303','(38452)2-44-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@infinity-capital.ru', '86EBA8728531782E7AB878B1CD9C8216513B0560', 
 '2010-11-30 11:03:24.000',True,False,'��������� ����� ������������','��� "��"�������� ������� �����"','7718613817','1067760046419','(495) 545-38-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@glocapital.ru', 'E80AB4630FC9C566D05D463F5E60813CC41CC8B4', 
 '2010-12-01 16:00:15.000',True,False,'��������� ������ �������','��� "��"������ �������"','7719628245','5077746472986','(495)545-38-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'actuary@npfimperia.ru', 'F9B9931216BC9DA48F9C67A30AAB4CA39B7C3959', 
 '2010-12-01 16:15:30.000',True,False,'������� ����� �����������','����������������� ���������� ���� "�������"','3444042495','1023403433900','+7 (8442) 96-26-05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'karimov@tfgroup.ru', '44E9A06BD7A97914E26628F8359A8A9910F2B057', 
 '2010-12-10 10:12:11.000',True,False,'������� ������ ���������','��� ��ʔ�����������ϔ','7708168370','1037739614604','+7 495 772-97-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'invest@uk-vectra.ru', '84D771DCA5BC57FE5C7B40B864CE98146C625968', 
 '2010-12-10 13:11:00.000',True,False,'��������� �����','��� "������"','7726512594','1047796839375','237-78-86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'interstom@aaanet.ru', '8BEED676DDFD195F416A1609F0FE585E48700EAB', 
 '2010-12-14 16:05:47.000',True,False,'�������� �.�.','��� "����������"','6150028060','1026102230648','(863)2990440', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '26290506@mail.ru', '3C591304699D34AE83990EE89C6A65EF91B21B58', 
 '2010-12-15 10:32:14.000',True,False,'�������� ���� �������','����������������� ��������������� �������  ���������� ������� ����������������� ����������� ��������� �������� ��������� �����','6660008991','1026605238923','2546236', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'asm@finam.ru', '5030B9199344E9580C7224ED56AF2B4D37F55249', 
 '2010-12-15 11:59:01.000',True,False,'���������  ������� ���������','�������� � ������������ ���������������� "����������� �������� "����� ����������','7744002606','1037739042285','7969388', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ukrkt.ru', '0B6A7495BD7B865207950612BC9F0857FFBB6537', 
 '2010-12-15 12:10:11.000',True,False,'������� ������� ������������','��� "��"������� ������������� ��������"','7701862519','1107746009271','(495)640-29-58', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'trast_holding@mail.ru', '5152134EB3CA9F57056A2436DCB7BC4BA326EFD7', 
 '2010-12-15 20:10:37.000',True,False,'�������� ������ ������������','��� "�����������������"','5609044902','1055609006155','79068389343', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@nfksber.ru', '3A58AC1900430BD195CA97FF3C72CAAF5FF4B765', 
 '2010-12-16 11:07:07.000',True,False,'�������� ����� �������������','��� "���-����������"','2128051450','1032128014280','(8352)55-02-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'NSK_ZAO@mail.ru', 'B7577C5171646572F20C0060D0338BD90A73C4CE', 
 '2010-12-17 12:13:49.000',True,False,'����������� �������� ����������','��� "���"','7708522582','1047796269608','(495)412-2292', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '19@maxima-invest.ru', '62B95AF15A7B9C0560DA1C88B081A6AA9BD07EAB', 
 '2010-12-17 16:21:25.000',True,False,'��������� ��� ����������','��� "����������������"','2309085853','1032304947003','89298266966', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alexandra@severgazbank.ru', '37CBC0369278D415FB180A84F03E4273C89B797A', 
 '2010-12-20 08:25:41.000',True,False,'����������� ���������� ����������','�������� ����������� �������� "����������� �������� "��������� ��������������"','7722590680','5067746966470','(8172) 57-36-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ryabov@olma.ru', 'D2B80A858E7A9E85E090466D8FB556E1D3F24B99', 
 '2010-12-20 10:16:56.000',True,False,'����� �.�.','��� �� "����"','7711056412','1037700036384','(495) 960 31 29 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Kurskoeloto@yandex.ru', 'E41EEC0BE8A9CBCF22BBD8C610205C4F0BED1F35', 
 '2010-12-20 17:16:07.000',True,False,'�������� �.�.','��� ������� �������','4629034940','1024600952782','(4712)530765', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@mnhb.ru', 'F62292723E614A132B0767416DD190C051C6BFEC', 
 '2010-12-21 11:51:45.000',True,False,'�������� �������� �����������','��� "���������� ��������������� ����"','7744000990','1027739337581','(495) 223-01-01 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@rpg-am.ru', 'CD5C5A6299053D7D7D42F03D97EA8B41310C8032', 
 '2010-12-21 13:47:00.000',True,False,'��������� �������','�������� � ������������ ���������������� "����������� �������� "���������������"','7723627413','1077759966756','8(495) 640-40-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vorushilina@verbacapital.ru', '109AB5F08A095188B4AED3D873AF51360F85DBAB', 
 '2010-12-21 13:48:00.000',True,False,'���������� ������ ��������','��� "�������������� ����������� "������������"','5404265403','1065404008339','(383) 3-281-282', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'romanova@dohod.ru', '78C0830EEBEF4717188A14D82F2216B6085F69B2', 
 '2010-12-22 15:30:49.000',True,False,'�������� �.�.','���"��"������"','7826685368','1027810309328','(812) 635-68-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stepanova@maxwell.ru', '0854DD290239CD95BB231E40BBE71BB3B8628FD4', 
 '2010-12-23 11:28:21.000',True,False,'��������� �����','�������� � ������������ ���������������� "�������� ������� ����������"','7728647050','1087746129723','(495)782 88 80', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@gera-llc.ru', '0A0B646A1AED3327AC1AFA5376F8AAFA510E218D', 
 '2010-12-23 18:21:15.000',True,False,'������� ����� ����������','�������� � ������������ ���������������� ����������� �������� "����"','7737524746','1077761587397','(343) 217 26 69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'admin@rossexport.com', 'F0C8E70E3AE03EC86EAE441362611B2595695C24', 
 '2010-12-24 14:39:25.000',True,False,'������� �.�.','��� "���"','6324013025','1106324007151','79222950303', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sdmitriev@a-practic.ru', 'CC6E308F39D41C46026B7DA194856EBD9F9AE4DB', 
 '2010-12-27 10:45:35.000',True,False,'�������� ������ ����������','��� "��������� ����� "�������"','7714143134','1027739479723','4117272', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'knv@ukkalita.com', 'CE727AD81C359A93D8EE6391D6D0352CF7C385A0', 
 '2010-12-27 13:11:02.000',True,False,'��������� �������','�������� � ������������ ���������������� ����������� �������� "������� ��� ������"','7710684453','1077758611842','+7 (495) 956-03-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga@vesna.nnov.ru', '4DE158BC38B6DFD05290D91363FF99D06CE8FAE9', 
 '2010-12-27 16:15:42.000',True,False,'������ �.�.','����� "�����"','5253000240','1025203030104','8(831)436-76-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'apb@okhta.com', 'C2223FD11FD7A7C0ECB3CE1266694C7D438E98D9', 
 '2010-12-27 16:30:24.000',True,False,'������� �.�.','��� "�� "������� � ��������"','7806138649','1037816019340','(812)324-05-24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@btween-am.ru', '9C4FE3888957424F8266DAA2DF53483D679EE186', 
 '2010-12-28 09:01:47.000',True,False,'�������� �����','�������� ����������� �������� "�.���� ����� ����������"','7702639922','5077746720519','(383)3356653', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'se@mdtrust.ru', 'C34BD377A6672BECFB5870590843B4737E6E62ED', 
 '2010-12-28 11:31:04.000',True,False,'������� ������ �����������','��� �� "�� �����"','7710684478','1077758611908','(495) 697-78-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sps@finsb.ru', '00B47A7E202138F4C702B354DEC8C642852F2604', 
 '2010-12-28 16:16:52.000',True,False,'�� �������� �������','��� "���� ���������"','7750004270','1087711000013','(495) 777-77-87 (1440)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Ivanova_d@yk-rfr.ru', '95BA9D199B95244A3DEE6C59CFBE17EA984A33B3', 
 '2010-12-30 11:58:39.000',True,False,'������� �����','�������� ����������� �������� "����������� �������� "������������������"','7705619057','1047796705692','(495) 234 39 67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'm.akchurin@rambler.ru', '5A1A0B6A6F670618EC3B061720DA14A306A786A9', 
 '2011-01-08 13:15:07.000',True,False,'������� �.�.','���"�������-�����"','7328508983','1077328003499','+7(909)3589393', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'menatour@mail.ru', '268C54683411CB7A3D325F89FEC436E08BAC7E61', 
 '2011-01-10 11:23:33.000',True,False,'�������� ������� ������������','��� "�������"','2224002470','1022201758149','(3852) 633731', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npfspb@npfspb.ru', 'F88F73BF00958D44D2C8DA431CFA0808FCE78348', 
 '2011-01-12 12:26:44.000',True,False,'��������� �������','��� "�����-���������"','7809025820','1027810349820','(812) 332-26-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zinaida@gik.ru', 'D26F35F5AF2B217352B52D632246C84A79E2DFF9', 
 '2011-01-12 13:52:15.000',True,False,'��������� ������� ����������','��� "���" ','2308076398','1022301214275','861 268-01-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@trust-kapital.ru', '08E2A18C2925CDE5E0E3922094AD709CE48FF36A', 
 '2011-01-12 17:17:27.000',True,False,'�������� ���� ����������','�������� � ������������ ���������������� "����������� �������� �����-�������"','7707659954','1087746443971','(499) 917-06-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'roman.levin@gazprombank.ru', 'B52BFAD1D1BA915A27B1B22E9F08156DF99CD5DF', 
 '2011-01-14 13:07:07.000',True,False,'˸��� ����� ����������','����������� (�������� ����������� ��������)','7744001497','1027700167110','(495) 9137800 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@lesmir.com', '46AD3C50813161B170521C6F7CFA6CC63B2C915C', 
 '2011-01-17 12:37:46.000',True,False,'���������� ����� ����������','��� "������ ���"','3907037329','1033902834130','74012669012', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ira@baltgroup.ru', '0CC85662D28CD999D37A61F347FF9EC0AB273692', 
 '2011-01-17 15:00:27.000',True,False,'���������� ����� ������������','��� "����-����"','7825347687','1037843040399','(812)448-51-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'port-garant@mail.ru', '6A14704010F8C19FC283B42AF40596CB3B8D7D7B', 
 '2011-01-18 11:39:00.000',True,False,'��������� ����� ����������','��� "����-������"','2540028188','1022502259042','84232495179', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'denis.sharshov@gmail.com', '176551F21E1E10A6D0CE6742A143C2D782C01C4C', 
 '2011-01-19 15:45:00.000',True,False,'������ ����� ������������','�������� ����������� �������� "����� ��������"','7813478340','1107847253513','79219444774', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'o_chromova@mail.ru', 'FBA294D69F02271F3090566121A99F0C61091342', 
 '2011-01-19 15:52:18.000',True,False,'������� �����','��� "���������������� ������������ ��������"','7705816263','1077762186996','(495)708-37-16', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gekas@rkmail.ru', 'E2EF43AFAED6D96A1810052CF2D99D5A29F95F6E', 
 '2011-01-20 18:16:47.000',True,False,'�������� ���� ����������','������� �����','1003001854','1101039000260','88142791395', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Feddepcenter.info@gmail.com', '80250B6D1716909435BC9D4394EA991795C159B4', 
 '2011-01-21 15:10:52.000',True,False,'������� ������ ����������','�������� � ������������ ���������������� "����������� ������������ �����"','7726624185','1097746088384','79671285469', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'al.velman@mail.ru', '80889973CFC9CB2F54D60629E7D66E7BE00F58FD', 
 '2011-01-24 11:42:50.000',True,False,'������� ��������� ������������','��� "������ ���� �����������" ','5408255886','1075473014011','299-89-48 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alexander_velman@hotmail.com', '80889973CFC9CB2F54D60629E7D66E7BE00F58FD', 
 '2011-01-24 11:45:55.000',True,False,'������� ��������� ������������','��� "������������������"  ','411021896','1025403660336','299-89-48 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bersil@mail.ru', '441F8368D1ADCBA8B45C05F5B18570E217090F9E', 
 '2011-01-24 12:09:34.000',True,False,'������� �������� ���������','������������ ����������������� ���������� ���� ���������� ��������� "������"','1655022511','1021602864117','(843)2389421', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npodz@rambler.ru', 'B654FCA29274462A1B0B8D6FBCA19D1D25025C26', 
 '2011-01-25 10:22:24.000',True,False,'������� ������� ������������','������������� ������������������ ��������� ���������','4243005449','1054243009754','83844999142', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@zenit.ru', '238DBB6A058409E19CA6C035AC7C1253C2C47917', 
 '2011-01-25 14:15:26.000',True,False,'�������� ��������� ������������','��� ���� �����','7729405872','1027739056927','(495) 937-07-37 (32-25)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@zhfund.ru', 'EAAA928A8ED95397F79D2F4D04A84AE9686C3678', 
 '2011-01-28 15:42:22.000',True,False,'��������� �.�.','��� "����������','7825442475','1037843062938','8(812)290-09-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sintezgrupp@yandex.ru', 'CB854258D8EBCA7E1DCE918E41CB1FAC82B7594D', 
 '2011-01-31 09:59:29.000',True,False,'���������� �������� ����������','��� "������ �����"','7719609274','1067759055242','495-7390246', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ukoif.ru', '767DA92F68DF1228F6DDF626BC68B2EED7C378B9', 
 '2011-01-31 13:41:06.000',True,False,'�������� �.�.','��� "����������� �������� "������������ �������������� �����"','7705660190','1057746775965','8 (495) 411 62 03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rnza@yandex.ru', 'E8A1844B6C55E722CFDFB3CD416DA0EE65A45688', 
 '2011-01-31 15:23:34.000',True,False,'������������� ���������� ������������','�������� ����������� �������� "����-����"','5408158025','1025403650524','(383) 316-59-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'diamondio@gmail.com', '04714D08BE20E0CCEE2A4984A6A223A915BF4C8E', 
 '2011-02-01 06:03:39.000',True,False,'������ �.�.','��� ����','7713085659','1027739768924','79263825710', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena.ruzanova@rlpnpf.ru', '5DE43B4BEE2F687C11DFE88C5FA8D68B37455F54', 
 '2011-02-02 10:22:16.000',True,False,'�������� �����','����������������� ���������� ���� "��������� ����� � ������"','7815027751','1037869018252','9812981', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oao_gazkomplekt@mail.ru', '453C80730FE5CF0013CDB8198FB3B4AA8A9E879B', 
 '2011-02-02 13:03:54.000',True,False,'���������� ������� ��������','��� "�����������"','7709031080','1027739149481','(495)7289186', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'accounts@fcsm.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 now(),True,True,NULL,NULL,NULL,NULL,NULL, False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ryov@fincor.ru', '806031665945A5CB49CF372B1EF3E741BD526229', 
 '2011-02-04 03:09:03.000',True,False,'������� ������','�� "���"','7841367289','1077847530771','812-6999999', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uknomos.ru', '2C212C2B05F047832C471C5B85722ACF7CDC865D', 
 '2011-02-04 16:15:52.000',True,False,'����� �����������','��� "����������� �������� �����-�����"','7705906164','1097746828387','8-926-235-73-06', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rarovskaya@bigpension.ru', '789B1C1DF643ABF9CE17C007700B7E79E7885DEF', 
 '2011-02-08 15:18:40.000',True,False,'��������� ������� �������������','����������������� ���������� ���� "�������"','7705373815','1027700266880','(495) 730-60-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'iav@lanta.ru', 'EDBF78C904E2AB571E044F51DDD1CC77E38FA818', 
 '2011-02-08 17:59:12.000',True,False,'����� ��������� ������������','�������� ����������� �������� ������������ ���� "����� - ����"','7705260427','1037739042912','495-957-00-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buh@npf-alemar.ru', 'EFEAC8E7B82305ECF81A8E2B26D0E33A8C7AE64D', 
 '2011-02-09 12:12:02.000',True,False,'�������� �������� �������������','��� "������-�����������"','7709050702','1025402468530','8(499)160-0087', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'o.saprykina@kitnpf.ru', 'D59DEAA3397A56521140207A519547AA85E6AFD2', 
 '2011-02-09 14:49:48.000',True,False,'��������� ������ ������������','��� ������ ����������������� ���������� ����','7841326204','1027710026508','+7 (495) 641 4414 (50307)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sergey@invest-a.ru', '9EA71944CC046EFD6648760648E0BE093BF1EB32', 
 '2011-02-09 18:59:02.000',True,False,'������ ������ ����������','��� ��� "�����"','7715675558','1077762674076','+7(495)783-74-17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 't.pavlakova@europf.com', 'C3D6EDD3A1A3005C9198BF786AD5D52767CBB7F4', 
 '2011-02-10 10:36:31.000',True,False,'��������� ������� ������������','��� "����������� ���������� ����"','7813033728','1027806866548','(495) 777-80-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'eu_malinina@vitabank.spb.ru', 'EA7093FC9F0A4606A222F50AB653285F54131E69', 
 '2011-02-10 12:21:34.000',True,False,'�������� ����� �������','��� ��������','7831000147','1027800000183','(812) 325 99 99 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'agropromtmn@mail.ru', 'F02822771DF592A982F98F5D3FDB85E480362269', 
 '2011-02-10 12:31:53.000',True,False,'�������� ���� �����������','��� "�������������������"','7202002067','1027200774369','46-36-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@npfrgs.ru', 'C43DFF300AA2338BD4A8840B8EE3815332E67BA9', 
 '2011-02-11 10:53:47.000',True,False,'�������� ������ ����������','����������������� ���������� ���� "���"','7714288355','1027714027912','797-32-95 (0771731)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mvu@pfpsb.ru', '68D5A7CF477A22249FB675D1B8E07491A73499ED', 
 '2011-02-11 16:48:37.000',True,False,'�������� ��������� �������','��� "���������� ���� ���"','7830002712','1037843029432','+7 812 322-94-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'thapkina@regionfund.ru', '89A9A6F0ADD44F6A94FE84F24F0CF02FC4015410', 
 '2011-02-14 10:24:10.000',True,False,'���������� ����� �������������','��� "����������"','7727178110','1027700322330','499-134-16-05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'texnocom1@yandex.ru', '7A86D907CF6C135D536F4799E4379270CDAE38A0', 
 '2011-02-14 10:31:16.000',True,False,'������ �.�.','��� "����"','5902100362','1025900523770','8-912-885-5909', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ladstroy@list.ru', '24F7008918E6DF088BBE3D09D7E8003DC3B65E86', 
 '2011-02-14 14:01:50.000',True,False,'�������� �. �.','��� "�����"','7826007612','1027810301200','380-08-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'arinvest@mail.ru', 'F828DDB334BE4CDCC2AFF40FE12CEC5F0C742995', 
 '2011-02-14 22:01:44.000',True,False,'�������� ������� ����������','��� "�������� "��������"','5406213351','1025402449247','73832279498', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sav@npf-strategy.ru', 'F51F2C09AA0B9A95031AF58742F2CB039786729F', 
 '2011-02-15 07:36:27.000',True,False,'�������� ���������� ����������','��� "���������"','5902111999','1025900510261','(342) 210-35-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sor-valia@mail.ru', '96A3A14BF5EF0B4F6ACCF8D21D94F2CB8E0EFEA0', 
 '2011-02-15 09:52:53.000',True,False,'�������� ��������� ����������','����������������� ���������� ���� "���� ����������� �����������"','7719027999','1037739321400','(499) 464-37-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'support@garant-prof.org', 'B3F43518069A8FA2B042485DAEE64886428B70F6', 
 '2011-02-15 22:17:09.000',True,False,'������ ���� ������������','���� ������-����','7736003814','1027739416704','74957826912', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'anker2k4@gmail.com', '9FB17D2052E6EF48BF6A9D52F35F24927D1617D5', 
 '2011-02-16 14:00:19.000',True,False,'�������� ����� ��������','��� "����-���������"','6009007065','1076009001683','79219653162', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vivanov@icfo.su', '02F10EA76A8C1DFC32A441464E6219E5C3E22753', 
 '2011-02-16 18:55:36.000',True,False,'������ �������� ����������','��� "����"','7841371158','1077847607760','88127197460', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'S.Trofimova@dosto.ru', 'B195035D433DB0EE7E2C940AC13440D14A6818D6', 
 '2011-02-17 14:42:56.000',True,False,'��������� �������� ������������','�������� ����������� �������� "����������� �������� "���������"','7707014681','1027700276977','(495) 933-02-66', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'magnitogorsk@npfszs.ru', 'BE4E137CEB10E866CEEE0812F067445137A416D2', 
 '2011-02-17 15:35:39.000',True,False,'������� ����� �������������','����������������� ���������� ���� "���������� ������ ��������"','7445006916','1027402167803','8 (3519) 23-62-09', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@npf-transneft.ru', '4D82B62BF3C07A3611F11B7862334999611F471E', 
 '2011-02-17 17:41:59.000',True,False,'�������� ������ ��������','���"����������"','7706215201','1027739470770','(499) 799-8597', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edo-opeka-npf@mail.ru', 'AA1457D9AFDC0D6E4B4B24D5D3E9C290209D77AE', 
 '2011-02-21 20:16:17.000',True,False,'���������� �.�.','�������������� ����������� ����������������� ���������� ���� "�����"','7725153864','1037725045423','8(495)9541176', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'FrenkelDM@tn-invest.ru', '56623C142CDED7DACD3E0693ED1937C3A299E459', 
 '2011-02-24 13:34:29.000',True,False,'�������� ������� ����������','�������� � ������������ ���������������� ����������� �������� "���������� ������"','7715790430','1097746845679','(499)799-85-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ktagf@mail.ru', 'B9E9C5EB047E03529257A631F8EA053D5F253B38', 
 '2011-02-28 11:08:51.000',True,False,'�������� ������� �������������','�������� ����������� �������� "������-����������������� �������� "�����"','5010042778','1115010000148','(49621)-2-82-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Irina.Koroleva@mdmnpf.ru', '9F73A97849A602F3C9C5F1CFB6BD4F9CDF565638', 
 '2011-03-01 17:31:42.000',True,False,'�������� ����� ����������','����������������� ���������� ���� "���"','7714222724','1037739312610','(495) 797-95-00 (543-01)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tulinov@npfe.ru', 'B2414685F6B52099F8480EB5CF89611C3B181A3D', 
 '2011-03-01 17:45:15.000',True,False,'������� ������� �������','��� "����-�����-����"','7705391067','1027700577057','8495-710-88-28(18-52)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'semerenko@neftegarant.ru', '7E3C8381CBA036A6368BBF0A820AC72A6A4A3BC6', 
 '2011-03-02 09:00:47.000',True,False,'��������� ������ ����������','��� "�����������"','7706210370','1027739163638','84995766555', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'myashina@expr.ru', '2EC335D9EAF015C01CAF1198C467EAFC70A8E9D8', 
 '2011-03-03 09:41:51.000',True,False,'����� ����� ����������','���� "��������-������"','7719020344','1027739290270','495 925-07-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Y.Glukhova@gencap.ru', '05713A426E0D624B3FBD89651DDE62E8F9206825', 
 '2011-03-04 11:41:04.000',True,False,'������� ���� ��������','��� �� "�������� �������"','7709811202','5087746543242','(495) 539-53-39', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'efremovaas@vair-group.ru', '882587418D9BCF12E393F51AC67F8048B60DE050', 
 '2011-03-04 13:41:10.000',True,False,'�������� ��������� ���������','��� "��� "����"','7733735143','1107746436984','(495)926-25-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mokerova@agidel-am.ru', '5A4AD87AC385F9D29CFA5C75EFFACE7379ED7297', 
 '2011-03-04 15:18:47.000',True,False,'��������� ����� �������������','�������� ����������� �������� "����������� �������� "�������"','5902859431','1095902009598','(342) 209-01-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nadezhda_fabrich@mail.ru', '52F4A559B8111DDCEE6EB52778F1C9B47D6B1D63', 
 '2011-03-05 10:17:52.000',True,False,'��������� ������� ���������','��� �� "��������������"','4205216287','1114205003032','905-900-43-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'd.basevich@npfstayer.ru', '4B7306583118BBBA344D121E4DF5B556E7EEDA7A', 
 '2011-03-05 12:48:19.000',True,False,'������� �.�.','��� "������"','7709010259','1027700071090','(495)500-30-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'd.basevich@npfallianz.ru', 'A09C093CFBB95FCB00FC3DD29A6D329730FF240B', 
 '2011-03-05 13:28:46.000',True,False,'������� �.�.','��� "������"','7706520068','1047796004299','(495)500-30-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yurist_dnk@dnk.su', '72D48EC7B3D98DFD4D8DDBD2F1B6780660D91C84', 
 '2011-03-09 13:38:03.000',True,False,'��������� ������� �����������','��� "���"','2801079510','1022800528596','8 (4162) 339-181', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'konsta-a@mail.ru', '5FB1436FAD359513899BFA200DB233CF4C815EDD', 
 '2011-03-10 14:05:10.000',True,False,'����� ������� ����������','��� "�� "�������"','7710669920','5077746473019','(495)544-93-44', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nvoronova@pulsbank.ru', '4DF9DCCCE986D397A27D4F97F4876D62C03A51D6', 
 '2011-03-10 15:04:00.000',True,False,'������� ��������','��� �� "����� �������"','7709233110','1027739068158','(495)913-25-80', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kolchanova@agidel-am.ru', '5A4AD87AC385F9D29CFA5C75EFFACE7379ED7297', 
 '2011-03-11 13:15:55.000',True,False,'��������� ����� �������������','�������� ����������� �������� "�������������� ���� "�������-1"','5908007088','1025900507445','(342) 209-01-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'shustova@agidel-am.ru', 'D934AA898F141A5D3C48E18A5207D46C13415D88', 
 '2011-03-11 13:22:46.000',True,False,'��������� ����� �������������','�������� ����������� �������� "�������������� ���� "������"','5902101528','1025900507687','(342) 209-01-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aris@uk-aris.com', 'EB4101F584EA0E8E9BF0F378C54383A00654A11D', 
 '2011-03-11 14:15:37.000',True,False,'���������� ��������� ���������','�������� � ������������ ���������������� "����������� �������� "��.�.�."','2310121841','1072310001708','(861) 262 04 94', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivan-taraskin@yandex.ru', '5E8FC89DDD42FE55F0C3336B48343FD043403EA6', 
 '2011-03-14 08:24:50.000',True,False,'�������� ���� ��������','��� "����"','5030007588','1025003752069','89653282600', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dreus@npfe.ru', '23F3E241601CCB111DEF363B6018B4CA7AE34A30', 
 '2011-03-14 14:59:30.000',True,False,'����� ����� ����������','����������������� ���������� ���� ����������������� (�������������� �����������)','7705001599','1027700348202','8495-710-88-28(18-35)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf-rapf@yandex.ru', '528FA3D5333B308B9C59C69788FD5D4E6F2A52FF', 
 '2011-03-14 16:41:16.000',True,False,'���������� �.�.','�� ��� ������-������','7810347229','1037821033690','(812)2447218', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Sobakinskih_VL@surgutneftegas.ru', '4845F4A89A5A7937D33CC6E1D21C5D0458CDEA9E', 
 '2011-03-14 16:45:06.000',True,False,'����������� ������� ����������','��� "����������� ���������� �����������"','8602104467','1028600581657','(3462) 42-11-75', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ekovaleva@skmg.ru', '3D97EA4E3C227219E2ED75DC92F5B8BA10632E5B', 
 '2011-03-14 19:31:49.000',True,False,'�������� ��������� ���������','��� �� "�������"','7729566990','1077746271437','(495) 651-81-58', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mvik@chelinvest.ru', 'DCDBB78FD6A0AD149170CD325A046AB05BB419E2', 
 '2011-03-15 07:33:00.000',True,False,'������� ������ �������������','��� "�����������  ����������� ��������"','7451190893','1037402891790','(351) 263-83-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf@mail.kht.ru', 'B6A11231DEBA5707AA92724B9E544AE6CC48EAA3', 
 '2011-03-15 08:18:00.000',True,False,'�������� ��� ������������','��� "��������������"','2721047175','1022701289775','(4212) 38-32-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lihacheva@imperialtrust.ru', 'C53DC481654F4360122ECE0BF867160057E62EFB', 
 '2011-03-15 14:28:43.000',True,False,'�������� �.�.','��� "�������� �����"','7723756313','1107746318723','(495) 720-66-73', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uk-dynasty.ru', 'F953EB4D2D9D6D3473603CD51070948BAF7EA690', 
 '2011-03-15 17:16:23.000',True,False,'������� ��������� ����������','��� "�� "��������"','7840365137','1077847466707','812 331-51-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'konstantin.berezikov@aviva.ru', '84CB3B97DD0BAE0F5023B8B35438158BEE5CAB9E', 
 '2011-03-15 18:21:45.000',True,False,'��������� ���������� ����������','��� "����� ���������� �����������"','7743053767','1027739310170','4117114', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kubuk@mail.ru', '9D76F5685E0762A7D7965677E0664DD0DF29499A', 
 '2011-03-16 09:40:00.000',True,False,'�������� ������ ���������','��� "��������� ����������� ��������"','2309084747','1032304942812','88612104387', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ukintegral@tvcom.ru', 'BA5ACFD230421D7E023D551E9E85B3F5C13A7A52', 
 '2011-03-16 09:40:48.000',True,False,'�������� ���� ����������','��� "����������� �������� "��������"','7704528329','1047796605779','(4822)799-700', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dmitry.maslov@interpromleasing.ru', '636617E54DF07B707D35993BEF3E8B26A20C5AD3', 
 '2011-03-16 11:34:55.000',True,False,'������ ������� ����������','��� "���������������"','7715541716','1047796824140','495-937-80-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'control@npfvernost.ru', 'E699AF515E5A86F5534845FBBCC8851E5CEBC09B', 
 '2011-03-16 13:16:34.000',True,False,'�������� ��������� �������������','��� "��������"','7725068070','1027739128944','(495) 710-88-28   (17-32)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Antonova_NS@kazanhelicopters.com', '1F40E3CAB8A184EC51987F4D47872CA0A1934EC7', 
 '2011-03-17 13:12:13.000',True,False,'�������� ������� ���������','����������������� ���������� ���� "��������� ����������� �����"','1661005401','1021603881925','(843) 549-79-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf@mv.ru', '51394884216A49C6C61DFA31724D887163854AA4', 
 '2011-03-17 13:28:56.000',True,False,'������� ����� ������������','��� "����������"','7303008210','1027301174482','(8422)420035', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@npfbksb.ru', '572C14A5BBA7B3DC6033C1275BD384F70B4969D4', 
 '2011-03-17 15:16:21.000',True,False,'���������� �. �. ','��� "��������������"','5905005710','1025901222203','+7 (347) 246-52-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf182@mail.ru', '7C9E11CFD272034464D9D730540574565B0B6418', 
 '2011-03-17 15:17:43.000',True,False,'�������� �.�.','�� ��� "������������ ������"','5902120672','1025900526740','(342) 236222', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivanovaea@npfzb.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2011-03-17 17:29:36.000',True,False,'������� ���������','��� ������ ��������','7708153166','1037739482890','74957834784', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'msav@pervobank-asset.ru', '29308689DE3700A00BC17F863B1F50C4B7FD6758', 
 '2011-03-18 09:42:56.000',True,False,'�������� �.�.','��� �� "��������� ����� ����������"','6316130695','1086316002200','(846) 240-53-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'burakov_eb@gw.tander.ru', '78AB2CD486821B183275B8E657673A383204C34B', 
 '2011-03-18 11:01:58.000',True,False,'������� ������� ���������','��� "������"','7703032601','1037739421983','8612109810', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ppi@partner-pi.ru', 'BD7A2C6DEAA8CCDA8D0D4092231F95C4A87C4824', 
 '2011-03-18 12:33:10.000',True,False,'�������� ������� �����������','��� "���������������������"','7106074679','1067106042190','8 (4872) 25-41-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'perspektiva@npfond.ru', '8E54F6A365BFF107BD7504866736B5EB0D1F9E80', 
 '2011-03-18 15:06:41.000',True,False,'�������� ������� �������������','��� "����-�����������"','6606015574','1026600732817','(343) 297-40-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ek@sberinvest.ru', '43BD865B7195F301214B2E4C8ED22BE0599A18DF', 
 '2011-03-18 15:12:03.000',True,False,'�������� ��������� ����������','��� �� "����������"','7707513842','1047796179804','+7 (495) 9337617', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Kolbina_SN@surgutneftegas.ru', 'D0F10A93C6F355DDC83B0E888B7375380CD479B4', 
 '2011-03-21 07:59:04.000',True,False,'��������� ����� ����������','����������������� ���������� ���� "��������������"','8602161747','1028600583032','8(3462) 42 11 86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aa.banin@stalfond.ru', 'B80B485E6BCB62FDCEB973052CC0D1E6C871515A', 
 '2011-03-21 12:00:25.000',True,False,'����� ��������� �����������','����������������� ���������� ���� "���������"','3528052620','1023501238144','8202536642', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@szuk.ru', '2F69EED8BAED9ACE503994275C44BE22BD83A8C4', 
 '2011-03-21 12:13:53.000',True,False,'�������� �.�.','��� "������-�������� ����������� ��������"','7810152808','1037821001756','(812) 702-50-57', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pfond-026@inbox.ru', '025803D84FA4622D2D93B4589CB11B570929D431', 
 '2011-03-21 12:59:37.000',True,False,'�������� ����� ���������','��� "��������"','7710156541','1027700381796','8-903-561-85-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk_info@metropol.ru', '546221D5B0BD2F92A84B83BDEE52EC3C3108FE01', 
 '2011-03-21 13:34:52.000',True,False,'��������� �.�.','��� "��"���������"','7706285907','1027706025885','(495)741-70-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf_general@mail.ru', '01CC01C19E84EFCB5F68FC99DC5964327065E4AB', 
 '2011-03-22 11:04:09.000',True,False,'�������� ����� ���������','��� "����������� ���������� ����"','7809023822','1027801525971','8-903-561-85-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fedotova@comail.ru', '8FBEFF68D16BEBC950CEEB5A90C41034B5BB8B83', 
 '2011-03-22 12:28:59.000',True,False,'�������� ������ �����������','��� "��� - ���������� ���"','7723152671','1027739337504','(495) 355-67-97', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dpbo@sseu.ru', '6F6941A25517D75B4E2ED98629FC5DC89C504658', 
 '2011-03-22 13:58:33.000',True,False,'�������� ����� ����������','��� ��� "��������� ��������������� ������������� �����������"','6318100897','1026301505120','78462240912', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'maria.gladinuk@gmail.com', 'BEF7E50BD34F02089D378BF44E2A738AC58A6CB4', 
 '2011-03-22 15:07:53.000',True,False,'�������� ����� ���������','�������� � ������������ ���������������� "���� ������"','7728716353','1097746709664','89163587714', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vvlapshov@bigpension.ru', '6A1E946CFA85F122E94E8DBA4825D81D11F6505F', 
 '2011-03-22 15:58:14.000',True,False,'������ ������ ������������','��� "����"','8605004580','1028601357498','+7(495)953-52-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rozhkova@kontrust.ru', '097E68972208621EF6A392C5FA371A407CBA67CC', 
 '2011-03-23 07:58:22.000',True,False,'��������� ��������� ����������','��� "��������������� - ��������� ����� "��� - �����"','4501012759','1024500527512','(3522) 411-677', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'larisa@npfprof.ru', 'DCA207B2E7BEE86CFD740DB5F62DF7F5BC737DE6', 
 '2011-03-23 13:08:39.000',True,False,'�������� ���������� ������������','��� "�����"','7736046783','1027739566271','775-07-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tatyana@npfprof.ru', 'DCA207B2E7BEE86CFD740DB5F62DF7F5BC737DE6', 
 '2011-03-23 13:12:15.000',True,False,'�������� ���������� ������������','��� "����������������"','8602164900','1028600585430','775-07-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'abramova@npfopf.ru', 'ECFDBAD77865A110E162D5256E0ED0FA44C4693A', 
 '2011-03-23 14:21:03.000',True,False,'�������� �.�.','��� "���"','3801055166','1023800517620','499 747 74 37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf338@mail.ru', 'B87312A2FBF457E765CBA4EED7678E9FF319B3BB', 
 '2011-03-24 10:14:16.000',True,False,'������� ���� ����������','����������������� ���������� ���� "�������"','1835044810','1021801652025','(3412)52-68-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'milcom@milcom.ru', '6744A0CC3A37A85EE81C23390B36F2EE3580EC3F', 
 '2011-03-24 14:29:46.000',True,False,'�������� ��������� ���������','��� �� "������-������"','7449012678','1027402694307','(351)251-26-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fbsystem@ntb.volga.ru', '07F86DE12610E35FCC1EEDD6436D79768BE71640', 
 '2011-03-24 15:40:48.000',True,False,'�������� ������� ����������','   �������� ����������� �������� ����������� �������� "���������� ������ �������"','7718636878','5077746470456','(8482)483561', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf@tolcom.ru', '7D51E723FE8DD17910AEBF53187BB1B99FE0BC38', 
 '2011-03-25 13:38:55.000',True,False,'������� ������ ��������','����������������� ���������� ���� �.�������� "�������������"','6320009638','1036301001318','8482 28 90 45', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'chubarova@consultcentr.ru', '66B01FF453E988ADD168465B2BBBE7F26C1F8851', 
 '2011-03-26 13:09:54.000',True,False,'�������� ����� ����������','������� �������� ���','6144009020','1026102023420','8 298 226 22 41', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'chernov@pfrus.ru', 'A4F6AD7540ACEC0C13C38F0121A29C3B17707C44', 
 '2011-03-28 11:20:08.000',True,False,'������ ��������� ����������','����������������� ���������� ���� "����"','7707288668','1027700422001','+7 (495) 637-90-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'riosar@yandex.ru', '6B00CEEEB1FA44FFE153B324FBAFD80EA187F384', 
 '2011-03-28 14:01:39.000',True,False,'�������� �.�.','��� "���������������� �������""','1327005922','1081327000260','8(8342)47-16-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'specdep@smai-mc.ru', 'AB59B242F94115AE8A1E5F309FA119717F4B3546', 
 '2011-03-29 07:15:33.000',True,False,'������� ������ �������','��� "�� "��.���"','2460069319','1052460051126','89130302074', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'haa@pscb.ru', '9361C96245F2C184C15D23B36735B7330AC7934E', 
 '2011-03-29 09:39:53.000',True,False,'������� ��������� �������������','��� "����"','7831000965','1027800000227','(812) 332-31-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'levanovich@zerich-npf.ru', 'EA8A5CD20B26F80ED82C568BE57121B18F0F7237', 
 '2011-03-29 11:16:22.000',True,False,'��������� ������ ����������','��� "�����"','6165058606','1026103304644','(495)7370099', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fokina@upravlyaem.ru', '325BFD595C218174F417CB9090058E923D21F7B4', 
 '2011-03-29 15:30:41.000',True,False,'������ ������� �����������','�������� � ������������ ���������������� "����������� �������� ���������"','7718218817','1027718000067','(495)662-40-92', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Elena.Menshikova@pan.spb.ru', '6EF30828BDA4CEFBB96FFEF037D5CB1F1F36DB81', 
 '2011-03-29 16:18:58.000',True,False,'���������� ����� �������','��� "�� ���-�����"','7813302586','1047855017330','(812) 363 25 30', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'control@npfrodnik.ru', '8F3B526CCA4699E0BFCB48066DAB14CCEFD67BC2', 
 '2011-03-29 16:55:57.000',True,False,'����� ������ ����������','��� ������','7704202743','1037739485431','84992638053', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'korfin@bk.ru', '4C4DB234CC819CE2C28FDD093E375D84C0FB5922', 
 '2011-03-30 12:16:14.000',True,False,'������ ������� ����������','��� "������������� �������"','2309070021','1022301984242','234-15-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf200@yandex.ru', 'CE6CD72591C738BAAB3313FD52339A636555ABDB', 
 '2011-03-30 12:37:44.000',True,False,'�������� ������� ��������','��� �����������','7701008441','1027739851303','499 135 43 85', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edo@npf-stroycomplex.ru', 'D606CE1F787898097FDBF78E53C9BBBE5FC3B0FC', 
 '2011-03-30 15:52:59.000',True,False,'�������� ��� ����������','�� ��� "�������������"','7703007972','1037739039337','84956885634', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena.kumrikova@ingos.ru', '961ED78804CDAEB292F6AB8D1315ED17D3C8DF61', 
 '2011-03-30 17:02:29.000',True,False,'��������� ����� ��������','��� "��"����������"','7707061480','1027700094531','921-32-23 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@captainfund.ru', 'F1205C2A140351ADDE7F459CD522D14CE0B4EBBE', 
 '2011-03-30 17:58:36.000',True,False,'����������� ������ ������������','����������������� ����������  ���� "�������"','7805033210','1037811011150','(812)363-19-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'orspb@yandex.ru', '0904553F25C4A73E07093790655A350D78CB2C9F', 
 '2011-03-30 18:55:55.000',True,False,'����� ������� ����������','����������������� ���������� ���� "������������ �������"','7805218309','1037811026835','(812)292-49-59', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'corporate@natinco.ru', 'BFA2E892975F8F255400D47E7D5ACC456B354483', 
 '2011-03-31 13:33:01.000',True,False,'�������� ����� ����������','�������� ����������� �������� "����������� �������� "��� ��������"','7716509585','1047796474824','+7 (495) 225-33-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yagoncev@ruscons.ru', '4529FA523A7DF3410F9EAD8CEFB770808548FA3D', 
 '2011-03-31 14:54:54.000',True,False,'������� ����� ����������','�������� � ������������ ���������������� "����������� �������� "����"','5906073053','1075906002127','(342)260-94-79', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Evsyukova@k-m-i.ru', 'C6695A1E67863709B55C8BADE4C0841996656C91', 
 '2011-03-31 16:42:48.000',True,False,'�������� ������','��� "��"������-������"','7701668173','1067746780210','(495) 775-07-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npfpravo@ukcm.ru', 'F9B1FE9A38D38086FA84EBD99FAD722BCD18158E', 
 '2011-03-31 17:20:24.000',True,False,'���������� ����� �������','����������������� ���������� ���� "�����"','1661001823','1021603887161','(843)264-45-71', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond@npfpodd.ru', '434BF5407A361F3DE2D919DD926626356BE16E6D', 
 '2011-04-01 09:26:33.000',True,False,'������� ���� �������������','��� "���������"','7702184432','1027739221333','(495)783-25-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond@npfveb.ru', '4E662C3DF88BEBD7FC1E1A38470D2542074E4510', 
 '2011-04-01 09:34:54.000',True,False,'������� ���� �������������','��� "��������������"','7728224464','1027739220794','(495)783-25-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond@npfvtb.ru', '7E496635DA054A6C3B07AF4D3AD50FA65BCCEFF4', 
 '2011-04-01 09:37:30.000',True,False,'������� ���� �������������','��� "������������"','7704149183','1027700289044','(495)783-25-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tam@trinfico.ru', '037EB820470614F8A5CE8A527CFCBD54F9DB1409', 
 '2011-04-01 11:48:36.000',True,False,'�������� ������ ���������','��� "����������� �������� ��������"','7701155020','1027700084730','725-25-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ic@smai.ru', '4B965F66FF03FD86580F970CEC0AF6105EAE414C', 
 '2011-04-01 12:47:22.000',True,False,'�������� ������� �����������','�������� � ������������ ���������������� "�������������� �������� "��.���""','5259028242','1025202830333','(391) 251-46-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sozinova@olmabank.ru', 'DB00CFC24DCD465B9FA14B978525EBCEFEC6B615', 
 '2011-04-01 12:53:26.000',True,False,'�������� ����� ������������','��� "����-����" (���)','7707071263','1027739024642','(495) 699-39-59', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@vostok-invest.ru', '60DC2C695DF4F15B426313711A378139A2C44972', 
 '2011-04-01 14:24:39.000',True,False,'����������� ���� ����������','�������� � ������������ ���������������� "�������������� �������� "������-������"','6367002370','1026303505139','(846) 270-85-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@sozidanie99.ru', '3EB7A04AB96B4A0772D227ADCC8CA8DFF57D3F29', 
 '2011-04-01 16:59:55.000',True,False,'������ ����� �������','��� �������������� �������� "���������-99"','7705026931','1027739009979','8(495)937-58-92', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'contact@evrotrast.com', 'C63F12F32986ECA0E6777CE7515E8130433C9AA2', 
 '2011-04-01 17:15:07.000',True,False,'����������� ������ ����������','�������� � ������������ ���������������� ����������� �������� "���������"','5262211350','1075262010064','(831)278-61-91', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kontroler@abcbroker.ru', '734B08740EDACB91B58E92C97925738168057187', 
 '2011-04-04 06:23:02.000',True,False,'������ �.�.','��� "���"','5402455173','1055402028098','(383) 3630630', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mihan800@ya.ru', '41A4BBEFDAC0A348D40DC4030CFA8AC9077DAB8D', 
 '2011-04-04 13:00:14.000',True,False,'�������� ������ ������������','���� "�����"','1833012041','1021801512963','3412405322', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'news@asn-news.ru', 'FEB89FDB1C3D3F6AC54914C88B5488051E4E1670', 
 '2011-04-04 18:20:20.000',True,False,'����������� ����� ��������','���','7709561390','1047796602920','+7 (495) 921-23-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@invest-bz.ru', 'E484F06841468B620F01886B0CA7BC8ED83F9A0E', 
 '2011-04-05 12:27:04.000',True,False,'������ ������','�������� � ������������ ���������������� "�������������� ������ ������"','7701862438','1107746008017','-1511', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@psse.ru', '032AD2005CA009F6D0062036B486DFF9807AFE7D', 
 '2011-04-05 16:02:27.000',True,False,'������� ����� ����������','����������� �������� ���','7722692635','1097746469754','(495) 620-59-17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'degalceva.n@newday.su', '47AC4A6B48F29841C8BBD49FE391D42ECF1C3DE3', 
 '2011-04-06 12:26:49.000',True,False,'������� ������� �������������','�������� � ������������ ���������������� "�������������� �������� "������"','3128062084','1073128003508','74725436585', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@manfin.ru', 'FD75DA7A031B3AB5D2A98B44FA54E6FE3A95B4E5', 
 '2011-04-07 15:36:50.000',True,False,'������ �.�.','���"����������������"','7709804396','5087746110250','(495)231-27-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ai.zyryanova@stalfond.ru', 'BBE6B517B094130C36F9AF56E9585E43BA5AC963', 
 '2011-04-07 16:42:57.000',True,False,'�������� ��������� ��������','��� "����������� �������� ����������"','3528113671','1063528069440','8 495 980 95 32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'svetlanacmic2007@rambler.ru', 'E452CB7AB418D06C94EA58B0036A7B5172F9031C', 
 '2011-04-08 15:35:38.000',True,False,'������� �.�.','�������� � ������������ ���������������� "����������� �������� ����"','7706271887','1027739144487','(495)956-48-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lbaranova@sputnik-am.ru', '6036F2658FED1F40C96DF07CADCDA57C6A64D135', 
 '2011-04-08 15:49:55.000',True,False,'�������� ������� �����������','��� "�� "�������-���������� ���������"','7744000951','1027739145830','(495) 725-50-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'futures@mosenex.ru', 'EEB06C9DA2DC95C43A2DEC56F72EE73F175DC8A0', 
 '2011-04-11 11:49:53.000',True,False,'���������� ���������� ������������','�������� ����������� �������� "���������� �������������� �����"','7714737057','1087746499675','+7 495 232-4120', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'omskstroiproekt@yandex.ru', '11C5DCA9AB1FB195C405692865819C5A9FEF3CA0', 
 '2011-04-11 12:38:35.000',True,False,'������� �.�.','��� ���������������','5504103283','1055507033130','381-2-31-65-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oksana_osi@mail.ru', 'E1B9323F3FE92C7DAE5C8A3A55B7FF6B386DB23C', 
 '2011-04-11 13:37:30.000',True,False,'������� �.�.','��� "����� ������-������"','7706134256','1027739432203','8-(495)-617-1436', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@gelicap.ru', 'A1AA16C0FFAC2F0098DD6EE38333B2C933E85B02', 
 '2011-04-11 16:51:58.000',True,False,'�������� ������������ ��������','��� "�������������� �������� �������������"','7718645551','5077746805000','+7 (8452)234863', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'konstantinova@sdm.ru', '7D8C32BA94E6F91578300E179E30ACD338681548', 
 '2011-04-11 18:04:57.000',True,False,'�������������','�� "���-����" (���)','7733043350','1027739296584','(495) 705-90-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sdo@nsd-nnov.ru', 'DCF15CB297D0E9F7FF02E97B11C872D3C8F44A98', 
 '2011-04-12 14:08:40.000',True,False,'������� ����� ���������','��� "���"','5262257041','1105262009160','(831)4129182', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@bastion-neva.ru', '450B05AE791044EF4BAAF6478124F8C976635F4A', 
 '2011-04-12 17:58:54.000',True,False,'������ ����� ����������','��� "�� "�������-����"','7842358368','1077847392127','(812)275-63-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'icuc@yandex.ru', '0B28BEBA43AB9AFA0F7BFF31CFF64A9449ED8B3A', 
 '2011-04-13 09:58:18.000',True,False,'����������� ������ ���������','��� �� "����-�������"','7715697865','1087746532521','89037073388', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@invm.ru', 'F05E88DA1C53854EBC784767091F84BA7C81B0AC', 
 '2011-04-13 12:24:05.000',True,False,'������� ����','�������� � ������������ ���������������� "������ �����"','7701863449','1107746032690','(495)967-86-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'EZharova@MOSKB.ru', 'C96F629D7B6A4F0E5BA76FF9BABE711535D12D3F', 
 '2011-04-13 13:08:43.000',True,False,'������ �.�.','�� "��������������" (���)','7750005612','1107711000066','792-38-79 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bk@printbank.ru', '1981FCAD7C09133CF7568149C3C88BED70BA6003', 
 '2011-04-13 16:17:37.000',True,False,'�������� �������� �����������','��� "���������"','7744002927','1027744005618','(499)4809980', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@npfo.ru', '1B4D23DB5BE57184E297E715FBED62CB7F8ED796', 
 '2011-04-14 10:52:13.000',True,False,'��������� ����� ������������','��� "�����������"','6673083668','1036604790970','(343) 379-22-97', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kalinina@nrcreg.ru', '6481F04B74509E7F35E16B0C0E9445CF62C2C94E', 
 '2011-04-14 13:25:36.000',True,False,'�������� ���� ������������','��� "������������ �����������"','7705038503','1027739063087','(495)9268164', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'udfr@mvkb.ru', '01568D2B91889C91BCFB62CEA94E5E979F6D4FDE', 
 '2011-04-15 10:20:42.000',True,False,'��������� ���� ������������','���  �� �����������������','7744002797','1027744003100','(499)267-66-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm@icvector.ru', '5E0D49D678114C91EA8E4F786E20FE7F7F5A56B6', 
 '2011-04-15 12:16:16.000',True,False,'������� ������� ����������','��� �� "������"','7720268195','1027720004993','8(495)2345954', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@riabank.ru', 'F605D3D8F73BE8FA3FE73CDDBACED977F698D61D', 
 '2011-04-18 16:44:36.000',True,False,'�������� ���� ����������','������� (���)','7750005563','1097711000122','937-35-16 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rosnerud@mail.ru', '6C2DDE4ED55BF68D867499AEE16191079FA6D880', 
 '2011-04-19 10:49:00.000',True,False,'��������� �.�.','��� "��������"','6712008650','1076712000551','84814954244', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'admin@e-notary.ru', 'DC2FFC65F4DE80F975DB83276230A13E91853F68', 
 '2011-04-19 13:35:51.000',True,False,'������� ����� �������������','��� "������-���"','7714028893','1027700239863','(495) 663-30-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ajax-capital.com', 'ACE0F84A9428E11788EE6F36050C9EBA69F8A2CC', 
 '2011-04-19 15:55:20.000',True,False,'���������� ������ �������','��� "�� ����-�������"','7737547503','1097746074744','970-10-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'julia.nokhrina@gs.com', '83B83CE19A835DF8C4D0B473F1CF493EC981BD01', 
 '2011-04-19 18:20:34.000',True,False,'�������� ����� ����������','��� "������� ���� ����"','7750005387','1087711000112','74956454029', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ikp@fa.ru', 'D16829175881D56A9184598AC1E63AE6C28F00DD', 
 '2011-04-20 12:19:32.000',True,False,'����������� �.�.','���������� �����������','7714086422','1027700451976','74992704614', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tes@elby-ic.ru', '44781D04FA4A47571B6E608B2D86AF2CFF511CE8', 
 '2011-04-20 15:41:36.000',True,False,'������������ ������� ����������','�������� ����������� �������� "�������������� �������� "����"','7810226697','1027804848642','(812) 655-50-05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tikhonova@phnet.ru', '23CF08552FB2FD960ED54EEED69EABA33FF8ED07', 
 '2011-04-22 17:58:21.000',True,False,'�������� ������� ����������','��� ���������� �������� "������ ����"','7704002409','1027700400936','(495)232-31-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'it@usdep.ru', '45585FD48F3F87D8D780B33669512072DBE7E99E', 
 '2011-04-25 11:16:36.000',True,False,'������ ����� �������������','��� "���"','7734634500','1107746288320','(495) 665-34-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tpm@trinfico.ru', '0A6598E58312D4D5C54A873650D9858925AE5EB9', 
 '2011-04-25 15:03:53.000',True,False,'����������� ���� ����������','��� "�������� ������� ����������"','7727528950','1047796947857','725-25-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf@ufa.bcs.ru', '39DA77AA10FC3EE7B6793DCF4A4504D7417F597B', 
 '2011-04-25 17:16:19.000',True,False,'����������� ������� ���������','��� "���������� ���������� ����"','276025669','1020202869950','(347)2921-337', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'petrowskoe@mail.ru', '524ED32BCA2FE09666D32C2D69C040EB52FE9AFC', 
 '2011-04-26 12:36:17.000',True,False,'����� ����� ����������','��� �� "����������"','5026003277','1025003178507','(495)552-1828', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'markina@slaviabank.ru', '98CF796B5DE603A4BCF4E777DE77D23A5122B8B3', 
 '2011-04-26 15:14:23.000',True,False,'������� ������� ����������','����������� ������������ ���� "������" (�������� ����������� ��������)','7726000596','1027739228758','969-24-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@amadeusinvest.ru', 'EC54BA347A57AC52BF96FDF8FD27AB0E430EAA3F', 
 '2011-04-27 18:41:42.000',True,False,'�������� �������','��� �� "�������"','7705917198','1107746375274','(495)-221-49-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@rnpf.ru', '4C13DD35B93DD982102B6D42B916D72006E0944E', 
 '2011-04-28 11:23:31.000',True,False,'������� ���� ����������','���� "��������� ��������"','7107037711','1027100971920','(383) 218-84-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zytsar@admin-npf.ru', 'FF458554B087B920F2DEF59D70A81118A8047EAF', 
 '2011-04-28 11:49:40.000',True,False,'������ ������ �������','��� "�������"','6321040172','1036301024484','287-16-38', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'c.invest@mail.ru', '537BBEE5666FEEF1A023CC82370EBFCC4BC2848B', 
 '2011-04-29 13:34:10.000',True,False,'������ ������ ������������','��� "�� "����� ����������"','7838347370','1067847447106','8-(812)-575-36-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uksozidanie.ru', '8AD84832F6E5DD9CFCFD1D87626F3C05C2E90D0C', 
 '2011-05-03 16:41:10.000',True,False,'���������� �.�.','�������� ����������� �������� ����������� �������� "���������"','7705903220','1097746734986','(495)517-72-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'L.Yabbarova@kamkombank.ru', '5D81A810D06F09CA824F8BB9319ED6CBC6828601', 
 '2011-05-04 19:26:14.000',True,False,'�������� ������ ��������','��� "����������"','1650025163','1021600000840','(8552) 700-194', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'soft@neal.ru', '3231748C034670D642B873FBAC0ED99D80C7C755', 
 '2011-05-04 20:59:18.000',True,False,'�� ������ �������������','�� "�������� ������" (���)','7744002275','1027739175056','9700006 (158)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ester_sweet@mail.ru', 'A1E006F7ECA76C045C60F37C7247F262A928636D', 
 '2011-05-05 19:36:22.000',True,False,'�������� ��������� ������������','��� "��-�����"','7202218098','1117232018551','89220019001', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'o.n.ukolova@mail.ru', '6B6E809A91AF7DB322FFD80E115686BF2B6BF683', 
 '2011-05-05 19:46:04.000',True,False,'������� ����� ����������','�������� ����������� �������� ��������� ���������� ����������� "���������"','4027012392','1024001179400','89109151151', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'knyazev@koltsovo.ru', '29C0F9422AB3F0DB9BC6BEE58E29FD2D488668F9', 
 '2011-05-06 10:20:40.000',True,False,'������ ������ ���������','��� "�������� ��������"','6608000446','1026605419202','-799', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'titov@prfhm.ru', '96F4CB915738A95B261B06A593DA38D5871DB519', 
 '2011-05-06 16:56:51.000',True,False,'����� �.�.','��� "������ ����"','814116269','1030800751970','(499)235-60-96', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'p.stefanovich@sdmdk.ru', '2EC32759BD1C7A7465C3D7B74E7C02DA49F70A45', 
 '2011-05-11 17:05:05.000',True,False,'���������� ���� �������������','��� "���"','7702617527','5067746542342','8(495)684-1849', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'malov@isbank.ru', '61468DF4835B10742BF9949AEEC9642FBB32CFF1', 
 '2011-05-11 20:14:23.000',True,False,'����� ���������� ����������','��� �� "�� ����"','7744001673','1027739339715','(495) 613-56-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@holdcap.ru', '601BFE88112B9041F67DB8B079EFB8195E478AA6', 
 '2011-05-12 14:16:57.000',True,False,'�������� ������','�������� ����������� �������� "������� �������"','1654043389','1021602830754','(843) 523-74-85', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@azimutbank.ru', '4194223B986E91BB7322576869A41FDFB4AEF486', 
 '2011-05-12 15:06:58.000',True,False,'������� ����� �������','��� "������"','7725065199','1027700570523','8-499-158-74-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@avers-capital.ru', '07454BB29DA491693A56A1375D003F9643D0EDEC', 
 '2011-05-12 16:17:17.000',True,False,'�������� ������ ������������','�������� � ������������ ���������������� "����������� �������� "�����-�������"','6317063219','1066317001850','8(846)310-67-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '111111@11111111.ru', 'AB5D8D415E1AC67D82A0B03758F6B79897084802', 
 '2011-05-13 14:33:11.000',False,False,'1','1','7728506589','1047796128379','1', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@city-am.ru', '4FA3988C052BB5061212C6F8DFBF186614314F19', 
 '2011-05-16 12:17:27.000',True,False,'������� ���� ����������','�������� � ������������ ���������������� "���� ���������� ��������"','7701861138','1097746832358','933-99-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'courier@ktinvest.ru', 'D015E8B93E49701CBB8C526531D49D03CABC5515', 
 '2011-05-16 14:35:27.000',True,False,'��������� ��������� �����������','��� �� "��������� ������""','7705419964','1027739333907','8(8482)22-35-85', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sergey.otrubyannikov@millennium-capital.ru', '3C6A200DC3522A4C199A4F42F742FBDDAE743822', 
 '2011-05-17 12:25:28.000',True,False,'������������ ������ ������������','��� "���������� �������" ','7714529850','1037789073464','7750320', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kasfandiyarov@it.ru', '9AE3B82137E35859B88B37EEC9DE680A922F0E44', 
 '2011-05-17 15:30:01.000',True,False,'����������� ������ ���������','IT_Test_LK','7135971070','4569097509317','79273488059', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rusinvestcom@mail.ru', '2545F7B0082BE6C99ABDD7EFE5D2A680B473B371', 
 '2011-05-17 16:32:29.000',True,False,'�������� ����� ������������','�������� �  ������������ ����������������  "������� ��������������  ��������"','4826056205','1074823007434','8 (496 44) 93-907', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'skcapital@tashir.ru', '970BFBCAAE7C07B3F2E94EBF7A97B66EF98231DE', 
 '2011-05-18 14:47:40.000',True,False,'������� ����� ����������','��� "��"��-�������"','7709701619','5067746786300','(495) 514-20-67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@citadel-am.ru', '9517754CA04E803989A099AE8CCE66EDBCEC0248', 
 '2011-05-18 14:48:08.000',True,False,'�������� ������','�������� � ������������ ���������������� "�������� ������ ����������" ','7708554841','1057746368096','(495) 737-81-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mi.azaryan@uk-sever.ru', '1BB1387A22FE3327CBB08441A75E0FC09BD3979F', 
 '2011-05-18 15:01:07.000',True,False,'������ ������ ���������','�������� � ������������ ���������������� "����������� �������� "����� ����� ����������"','3528111628','1063528065611','8(495)980-95-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nikitin@orgraf.ru', '79E7D5B1E504035C98FE80DEE449AD2D5BB48437', 
 '2011-05-19 13:20:30.000',True,False,'������� �������� ������������','��� "�� ������"','7722268810','1027722003132','8(495)673-32-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bumagi@aktivbank.ru', '922163EB6875E8D07D52E55F9663F0180CB2E18F', 
 '2011-05-19 16:01:37.000',True,False,'����������� ����� ������������','��� ��� "����� ����"','1326024785','1021300001029','8(8342)241475', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gatsko@econika.ru', 'CAEF0158C163F3900987D0B8A5501503C9FC2930', 
 '2011-05-20 16:49:53.000',True,False,'����� ������ ���������','��� "�� �������-������"','7714718329','1077762399109','684-31-94 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@bickening.ru', 'C47F79A8453877B310854E0F8B9957DADC210A0E', 
 '2011-05-20 17:24:01.000',True,False,'��������� ������ �����������','��� "��� "������"','7707515409','1047796240106','+7 (499) 5530174', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'doc@bazisinvest.ru', 'C792BC32402F8F632FE26CEF983416F5A6EF2BAF', 
 '2011-05-23 15:14:04.000',True,False,'������ ������� ��������','��� "�� "�����-������"','7703163072','1027739356699','(495) 783-07-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aulukaev@aikoscompany.ru', '4BC788907B7E2A4A05B39E6AAE9434C68DF34FFE', 
 '2011-05-24 17:02:24.000',True,False,'������� ����� ���������','��� "�������� �����"','7726636173','1097746544851','89262493442', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sokol@ipf-agro.ru', '0476AF2AFAE977E09C1AC36A271EAA45DD54BC73', 
 '2011-05-26 12:55:45.000',True,False,'����� �����','��� "�� "���"','7702636689','5077746345309','(495) 258-44-71', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'popova@atlantauk.ru', 'F76D4CC33E20098887A31571BE5FB3D969CB406D', 
 '2011-05-26 13:03:42.000',True,False,'������ ����� �������','��� "�� "�������"','7706651991','5077746343615','(495) 258-75-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vbroker@mail.ru', '0831013AAA71DDACC7B205A156EC00C9C2D885DE', 
 '2011-05-27 16:11:57.000',True,False,'������� ������� �����������','��� �� "��� ������"','7701852503','1097746628814','8-495-222-14-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'taxcom@taxcom.ru', '9FB6519742683ACE8D52800DB6CE352D9EF5664B', 
 '2011-05-27 16:53:59.000',True,False,'����������� ��','��� �������','7704211201','1027700071530','4957390797', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'morport07@rambler.ru', '72CAF33288DE474747A150E79555FE15FCDED1DA', 
 '2011-05-30 07:18:06.000',True,False,'����� ����� ������������','�������� ����������� �������� "���������� ������� �������� ����"','2460073530','1062460004628','89535956360', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'abilkueva@lissi.ru', '2FCE5E070AC3D8E62208A253E8C32285C94B03EA', 
 '2011-05-31 14:32:09.000',True,False,'��������� �.�.','��� "�����"','7727206293','1027700106081','(495)513-33-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'EZhukova@rencap.com', 'F3CC187311E54B54EF9DEB11CB52945AAF92D10C', 
 '2011-05-31 14:56:35.000',True,False,'������ ����� ���������','�������� � ������������ ���������������� "����������� �������� "��������� �������"','7710438874','1027710010778','(495)258-7770', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 't.frolova@mgpls.net', 'A3ABE831A01376A1C5E97F15E073BCD5A1962AD9', 
 '2011-05-31 15:55:01.000',True,False,'������� ������� ����������','��� "���"','7723566915','1067746445964','(812) 337-64-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@penguin-capital.ru', '6C7DE05BE70E52620491AEEE8FA9D998912FFB55', 
 '2011-06-06 16:38:05.000',True,False,'�������� �������� ���������','�������� � ������������ ���������������� �������������� �������� "������� �������"','7705910308','1107746108480','(495) 5806328', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'brokers@rusfincorp.ru', '926FD38CAF491E27E42E8FB1969ADB5086B6A78A', 
 '2011-06-06 17:38:04.000',True,False,'��������� ������� �������������','���� "���������� ���������� ����������" �������� ����������� ��������','7744003127','1037744005771','+7 495 692-9061', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@ukbinfinam.ru', '72CD4AA6B1BEC125C9C22F35D41D3CD5B617FE84', 
 '2011-06-07 11:55:37.000',True,False,'���� ��������','��� "�� ��� ����� �����"','7710288918','1037739222620','74952760777', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@ccmanagement.ru', 'C9F58B3E464A1D59115D395CB9348A6ACB6750AC', 
 '2011-06-07 12:30:03.000',True,False,'�������� ����� ������������','��� "������� ������� ����������"','7706663387','1077757807808','(8352) 55-02-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'timerbulatov@ruscapif.ru', 'D04B94CA96E0E0133FD3B56E6265583FEC7536C1', 
 '2011-06-07 13:03:32.000',True,False,'������������ ����� ���������','��� "����"','7727531423','1057746014908','7875990', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uk-s.ru', 'F7080E2441C9674E0A5D9633A78CDFDEA299142B', 
 '2011-06-07 14:27:08.000',True,False,'������� �������� �������������','��� "�� "�����������"','7710644010','5067746662759','(495) 229-00-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'igf@ig-favorit.ru', '452A024283F19D91F791C99B61FBE2F0C4CA1EAD', 
 '2011-06-07 16:36:53.000',True,False,'��������� ����� ��������������','��� "�� "�������"','7718807379','1107746413037','(495) 215-04-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@sberbank-fc.ru', '7BD7BC38F7B441B7492BF5A44C3F086B8CA9B871', 
 '2011-06-08 15:44:11.000',True,False,'������� ������ ����������','��� "��������-������"','7736617998','1107746399903','+7 (495) 641-04-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'registrator@pcrc.spb.ru', '98622E0D18934D2A16A7DF3429ED5CBCBA64331E', 
 '2011-06-09 16:25:41.000',True,False,'������� �.�.','����','7816077988','1027801569014','+7 812 327 43 69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'prilepo@derzhava.ru', '75B3542141BEB7442C314D41FE736E233E22AFB1', 
 '2011-06-09 18:09:10.000',True,False,'������� ����� ��������������','����������� ������������ ���� �������" �������� ����������� ��������"','7729003482','1027739120199','495 380-04-70 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rd.info@region.ru', 'CD02A9FDD9ABC2AF39B8B9076262A163049ECA68', 
 '2011-06-10 12:10:42.000',True,False,'�������� ������ ������������','��� "������  �����������"','7708227080','1037708029633','(494)777-29-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@meridian-realty.ru', '16CC622946472D29D894E8138FF77203C3AF6948', 
 '2011-06-10 14:40:08.000',True,False,'������� �.�.','��� �� "��������"','7720555094','1067746696291','8-499-126-49-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dnv@elby.spb.ru', 'A1E0C1C78CEFB4919BF689862EB4F2FB819B7A3E', 
 '2011-06-10 16:13:16.000',True,False,'��������� ��������','��� "�� "��"','7810138828','1027806862016','(812) 702-50-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a.chuvilin@ucnu.ru', '4AA3C5FCFEFC9E9E0AF23EE1B952C0F83541F257', 
 '2011-06-10 16:38:29.000',True,False,'������� ������� ����������','��� �� "�������� ����������"','7723625776','1077759414281','(495) 585-81-24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fev@kremlin-am.ru', '91BCADE443BBA7874F98C06AB41424FB86006E92', 
 '2011-06-14 14:53:31.000',True,False,'�������','��� "��"������"','7804393259','1089847243001','406-72-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@strategy-mc.ru', 'B4BB63B2047DDF62AA1E75C8E34815BD72CCEE17', 
 '2011-06-15 16:59:47.000',True,False,'��������� ������ ����������','��� �� "���������"','7728617521','5077746555080','(495) 797-38-28', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pravdina_m@is15.ru', 'D12F3BE8378957C35AC42822B4695B05060A6B99', 
 '2011-06-15 17:16:04.000',True,False,'������� ����� ����������','��� "�����������-15"','7707201995','1037739743414','7399344', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'cenbum@bank.kurgan.ru', 'CEBFDDD4C05E2D2AE91968F3F659856D4D2FE8F9', 
 '2011-06-16 09:21:56.000',True,False,'������������� ������ �������','��� ���� "������"','4501010247','1024500000029','(3522) 600-419', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alesenkoln@npfsm.ru', 'D8305539BE2FC50C5D47AC66E021F5F173F38892', 
 '2011-06-16 10:24:51.000',True,False,'�������� �.�.','��� "��������� ���������������"','2466057038','1022402658365','+7391(2273384)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'st-rekom@belgtts.ru', 'FF59EE3E66DC2728FC4B7845A380F622556F6BD9', 
 '2011-06-16 11:26:10.000',True,False,'���������� ����� ����������','��� ������������������ ����������� "�����"','3128060841','1073128002056','(4725)33-30-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sda@skbkontur.ru', '8469B0B3BEB523EEC2090B141EFA574D6B4D47E4', 
 '2011-06-16 12:11:51.000',True,False,'�������� ������� �������������','��� �� ��� ������','6663003127','1026605606620','4956044812', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ukmik.ru', 'EE44175C4F223F21541EC402D05C631A32499EFB', 
 '2011-06-16 12:19:51.000',True,False,'��������� ������� ���������','��� "�� "������������"','7710684439','1077758611743','+7 (495) 222-28-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond@ricom.ru', '0E787CF8F440A1B0F41B0DCFA62C953308CA90B5', 
 '2011-06-16 14:38:49.000',True,False,'�������� �����','��� ������������ �������� ������-����Ҕ','7704501207','1037739860718','(499) 241-53-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rvv@mfunds.ru', 'FA4076FF61DA0649721B5FF77D5CC82E9BA7932B', 
 '2011-06-20 11:38:47.000',True,False,'�������� �������� ����������','�������� ����������� �������� ����������� �������� "������������ ����������"','7705630029','1047796896729','(495) 745-21-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'janna@eufn.ru', '3F1894CEDCAE9872B582D0EDD7246EDB44864D29', 
 '2011-06-20 12:28:31.000',True,False,'�������� ����� �������','��� "�� "�����������"','7701008530','1027739083570','545-35-35', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dukin@prospect-online.ru', '00A16E30A270BBF7D6D8241661B1930AEDA881A8', 
 '2011-06-20 18:46:15.000',True,False,'����� �.�.','�������� ����������� �������� "�������� ������"','7711079917','1027739010210','8(495)937-37-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'komissarov@merkury-m.ru', '82A8A54120E90898A8FFDD97BC6998673FA44224', 
 '2011-06-21 11:20:14.000',True,False,'���������� ������� ����������','�������� ����������� �������� ����������� �������� "������� ����� ����������"','7717567195','5067746456432','(495)740-48-67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jg@hspb.ru', 'A14853EDFCD0EF6538108805E7D2437391E8A7AF', 
 '2011-06-21 15:14:51.000',True,False,'�������� ���� ������������','��� "���������� ��"','7842302012','1047855051595','(812) 644-44-35 (-36)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@soglasie-npf.ru', '8AD5A92F85854068A6DBA1E2CB93CDE4F084D2C4', 
 '2011-06-21 17:52:50.000',True,False,'���������� �����','��� ��������','7701018150','1027700084895','6509432', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@pif-energ.ru', '771A8BF350A311898BA7F9F5F181703CCA3DA3A0', 
 '2011-06-22 12:17:02.000',True,False,'��������� �.�.','��� "�� "�������������"','7825680984','1027809213695','(812) 458-58-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ric-am.ru', '30C6C1FB4612E977E1A6A99FEFE3D0DE1CDF65A4', 
 '2011-06-23 13:38:09.000',True,False,'�������� ���� �������','�������� � ������������ ����������������  "����������� �������� "������� �������������� ����','7715510034','1047796094521','8(495) 6615518', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk@m-auri.ru', '9F7B54F323BF1139E4AF4BEB0CA15EF5AD890A62', 
 '2011-06-23 16:38:40.000',True,False,'�������� ������� ���������','��� "�� "������ ����"','7701802580','5087746197314','(495) 363-09-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@russ-invest.ru', '78FE833C795E9E963D309D6A6D5CE8B0EA54DF32', 
 '2011-06-24 16:14:47.000',True,False,'������� ������� ����������','�������� ����������� �������� "����������� �������� ����-������"','7704511396','1047796052920','(495)363-93-80', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pm-kabakchi@rsb.ru', '142AD48ADA77E0966D9D33F22DBDC60B444E9515', 
 '2011-06-28 10:27:00.000',True,False,'������� ���� ����������','��� "�� "������� ��������"','7719593666','1067746694564','(495) 287-78-41', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vodolazskaya@brok.ru', 'B1CC8ED9714F9C53717AF37AA073A212C7E6A3E8', 
 '2011-06-28 17:30:34.000',True,False,'����������� �������� ����������','��� "����������� ����������� ��������"','7825107082','1027809183885','(812)325-81-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk-ten@ten-development.ru', 'AC5CED6CE7D07BADA13686B09C6F2FE83349F816', 
 '2011-06-29 11:59:18.000',True,False,'�������� ���� ����������','��� �� "���-�����������"','7728622666','5077746853730','(495) 981 10 99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.astashkina@vngarant.sar.ru', '40C8C2B550DDF2DFD8EE7384EE8029D069E3D881', 
 '2011-06-30 16:09:28.000',True,False,'��������� �������� ����������','��� "������-������"','5254025015','1025202197525','(831)3038797', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'help3@1.ackom.net', '2FCE5E070AC3D8E62208A253E8C32285C94B03EA', 
 '2011-06-30 16:22:48.000',True,False,'������� ������ ���������','��� �������������� ����� "�����"','2635049852','1022601941625','78652951095', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mir_ev@ukbi.ru', 'FDECA041B0DE303511FF58513DB4059601E02662', 
 '2011-06-30 18:58:36.000',True,False,'������������ ����� ����������','��� "����������� �������� "������ � ����������"','7704504286','1037739928852','(495) 780-96-73', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'orlovairina2007@mail.ru', '817F55A37A924BEC2B0E07EE13F74DCFF7DD6E74', 
 '2011-07-01 16:04:36.000',True,False,'������� �.�.','�������� ����������� �������� "������������� ����� ����������� � ����������"','7706232630','1027739072602','(495)956-48-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pinaev@elkursk.ru', '7241EB5AB44CA5695E3C7980CD9D45E7ABB9054C', 
 '2011-07-04 13:48:44.000',True,False,'������ �.�','��� "����������� �����"','4632044232','1044637018755','(4712) 545-575', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ntradition@mail.ru', 'AF1103B3C4403B03DD3A4AC7AC5E024FA1ABA346', 
 '2011-07-04 18:11:07.000',True,False,'�������� ������� �������','����������������� ���������� ���� "��������"','7710065703','1027700266791','(495) 637-03-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'v.kalinova@aurum-investment.ru', '8820A1933C40E94C3895827FEEA3A9FD585D3D27', 
 '2011-07-05 11:18:09.000',True,False,'�������� ���� ����������','��� �� "����� ����������"','7706660033','5077746875355','931-98-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'capital@nrc-capital.ru', 'BA1A27D36F30D9A6281C9400DF8F687762D6F7D1', 
 '2011-07-05 12:01:25.000',True,False,'�������� ������ ������������� ','��� �� "���-�������"','7704572215','1057748363892','8 (903)112-73-46 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'derbentkolos@mail.ru', '6E88A693A340A3E4AA5BFC634816EA7E2A93806D', 
 '2011-07-05 16:51:11.000',True,False,'�������������� ������� ����������������','��� "�����-����������� �������� ��������������"','542002576','1020502002894','9034245550', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'support@ssb.msk.ru', '85643E4F00476897B24CA67EE3EE7DEA9CBC2A83', 
 '2011-07-05 17:04:59.000',True,False,'����� ����� ������������','��� "�������������"','7706074938','1027739412359','(495)755-5666', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'TupitsynAA@uralsib.ru', 'AA01031A1563B677161949A74A6DDF872850E828', 
 '2011-07-06 12:50:57.000',True,False,'������� ��������� ����������','�� �� "������� ����� ����������"','7736213272','1027739680650','(495)-785-12-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'guseva@zerich.com', '2780DEDA9E4E78D56CFAABA5587E932AF729ABD5', 
 '2011-07-07 16:09:16.000',True,False,'������ ����� �����������','��� "����������� �������� �����"','7744000380','1027739043870','(495) 737-64-94', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edo@rcfcap.com', '767DA92F68DF1228F6DDF626BC68B2EED7C378B9', 
 '2011-07-11 10:43:31.000',True,False,'���������� ����� ����������','��� "�� �� �� ������� �����"','7722511455','1047796239116','(495)933-54-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tichomirova@deal-bank.ru', '55624F3E85C398EE75605B1A7DC3F0A18638475A', 
 '2011-07-12 15:26:40.000',True,False,'���������� ����','���-����  (���)','7744001521','1027739007218','739-29-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fndc@co.ru', '0C3374B11FD919F7E1344BE70533B3509A93388E', 
 '2011-07-13 18:27:16.000',True,False,'������� ������� ������������','�������� ����������� �������� "����������� �������� ���������"','7703349849','1027739567294','221-24-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@bk.irk.ru', 'BBB498DA2BF8DE2DF4E8BBDE92F9EF188A2F731C', 
 '2011-07-14 14:06:38.000',True,False,'��������� ���� �������������','��� "�� "����������� �������"','3808167888','1073808011111','8-3952-29-10-80', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'hafizov@rhcom.ru', 'EEF1F52BA827EED1FBEB61A80D8843C62A5CD60E', 
 '2011-07-14 15:43:05.000',True,False,'������� ������ ����������','��� "����������� �������� ������"','7701894373','1107746867128','(495)2314181', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jkhanina@flagman-finance.ru', 'D4C265649F9FCB6A079430E9CC1D2B88B114F282', 
 '2011-07-15 12:36:18.000',True,False,'������ ���� ��������������','��� "�� "������� ������"','7701789315','1087746766678','(495)220-51-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'meu@avbank.ru', '3DD1AC75817AB2EBC20AD13F056AB7757CF970CD', 
 '2011-07-15 15:15:55.000',True,False,'��������� ����� �������','�������� � ������������ ���������������� "����������� �������� "��� - ���������� ��������"','7714725118','1087746000737','(8482) 40-73-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'popova_o@albionfinance.ru', 'F3CC187311E54B54EF9DEB11CB52945AAF92D10C', 
 '2011-07-18 11:51:17.000',True,False,'������ ������ ����������','��� "����� �����"','7703603950','5067746107391','231-11-24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ukmukinfo@yandex.ru', 'F3CC187311E54B54EF9DEB11CB52945AAF92D10C', 
 '2011-07-18 12:24:00.000',True,False,'��������� ������� ����������','��� "������������� ����������� ��������"','7701711439','5077746343770','978-24-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@academfin.ru', 'E55AC3828EFC805BBB9FBB28119F2EA69164F092', 
 '2011-07-18 12:58:26.000',True,False,'����� �.�.','�������� � ������������ ���������������� "������-������"','7706735747','1107746305743','959-01-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'snezhinka@snezhinsk.ru', '8F98CD8E8DDEB03C1EC1AE1357F08B77E65F90A9', 
 '2011-07-19 14:52:24.000',True,False,'��������� ������ �����������','��� "��������� "��������"','7423101154','1117423000364','3514632823', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ufa@invcapital.ru', '9FBC271A83A0D1D96E5A3860E83A5195C3942B47', 
 '2011-07-19 15:09:00.000',True,False,'������� �.�.','��� �� "����"','278087082','1020203220355','(347) 2721377', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'finindus@rambler.ru', '578A8DD60527685C955075F70E78EFDBB3CA9D16', 
 '2011-07-19 15:12:48.000',True,False,'������� �.�.','��� "���������� ���������"','7716618930','5087746367055','(347) 272-74-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sokolova@unisoncapital.ru', 'A9FC175FCED9371D9DE9324F65E8F86CF626FE09', 
 '2011-07-19 21:23:53.000',True,False,'�������� �����','��� "������ �����"','7202088890','1027200807380','(3452)419999', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ybukharov@mdcg.ru', '8DEA4266F487017A5961F3C3ABCBC91246BA532B', 
 '2011-07-20 16:41:49.000',True,False,'������� ���� �������������','��� "��� ���������� ��������"','7703665442','1087746608421','(495)797-8065', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@nevskycp.ru', '87697937EBAFD6B214AE39F91B04FA40899BC633', 
 '2011-07-20 17:27:48.000',True,False,'����������','������� ������� �������� (���)','7841300855','1047855027990','(812) 325 63 79 (6420)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stl@fraktal.perm.ru', '89DF3AE3EA9F4918FF45A097CEA9F5B1135CDB87', 
 '2011-07-21 09:52:22.000',True,False,'���������� ������� ����������','��� "�� "��������"','5902219417','1105902002030','8 (342) 237-60-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uk-edinstvo.ru', '346AB7412AC6B035BFEEE0AAB6FB3CB27FB5AE5F', 
 '2011-07-21 10:31:03.000',True,False,'����� ������� ����������','��� "����������� �������� "��������"','7719661436','1077763699100','(495) 789-88-29', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Natalya_Chertkova@mittrust.ru', 'DD85BE2612FE8545536A0E5AA1936EC056D16E56', 
 '2011-07-21 11:30:25.000',True,False,'�������� �������','��� "�����������������"','7709380235','1027709000197','495-989-60-35', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buch-npf@rsinsurance.ru', '289CCFDABAE778669CEB4C3F8A2C8BA2F3805141', 
 '2011-07-21 13:20:40.000',True,False,'������������ ���� �.','��� "������� ��������"','7825679192','1027809170620','495 926 77 60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@strategy-amc.ru', '79CAB47C49068633CB5C956E18F44E513F542A9C', 
 '2011-07-21 15:21:06.000',True,False,'������� ������ ����������','�������� ����������� �������� ����������� �������� "���������"','7710261440','1027700160234','495 380-14-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lukianova_ka@mail.ru', '1C58384FB1D42CE384C56B75CBCA3C250F94C7B8', 
 '2011-07-21 15:27:03.000',True,False,'��������� ������ �������������','��� �� "��������"','7804433159','1107847062652','(812) 363-43-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dealing@ska-bank.smolensk.bz', 'E60AA58902F2B3FAFD4E7C71D7180942B32C0874', 
 '2011-07-21 15:44:50.000',True,False,'������ �������� �������������','��� "���-����"','6730012151','1026700000073','(4812) 201646', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@rcinnovations.ru', '52E8D0A3AA4E03A30E889E952F9D3356B66FE3E7', 
 '2011-07-21 17:21:59.000',True,False,'������� ���� ������������','����������� �������� "����-������� ���������" (�������� � ������������ ����������������)','7730624685','1107746294578','(495) 589-13-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uk.kamas.ru', '813E0EE6CCD22BB08CA4E53DA1366482E66DCEDB', 
 '2011-07-22 10:57:45.000',True,False,'�������� ����� ������������','��� �� "����� ����������"','1650108797','1031616047473','+7(8552)35-80-67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'chernyaev@uamc.ru', 'D25B16ACC92C04336F119804384A94482EE5ECD5', 
 '2011-07-22 16:09:48.000',True,False,'������� ������ ���������','��� "������������ ����������� ��������"','7707608090','1067759612007','+7(495) 287-08-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edo@mdcg.ru', '6912041106673B153546A2DBFBA33AFF0114508F', 
 '2011-07-25 13:58:38.000',True,False,'������� ���� �������������','��� �� "������������� �������"','7722538136','1057746221950','797-80-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fincapitalam@rambler.ru', 'F9D9FD41E3F0B3C22B377532E848F7259DBFF6C1', 
 '2011-07-25 15:50:35.000',True,False,'��������� ������� �����������','��� "����������� �������� "���������� �������"','7701868380','1107746173677','(495)234-27-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'avsfinance@avsgroup.ru', 'CCE62DEF3894A3967901B488F790D02D93672659', 
 '2011-07-26 07:12:15.000',True,False,'������� ����� ������������','��� �� "��� ������"','6672238301','1076672027299','73433792297', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pnpf@mail.ru', 'AD598C99AF7E17409C06E2AA7DB6756D5DBA3627', 
 '2011-07-26 14:14:00.000',True,False,'��������� ����� �������������','�� "���������� ���"','5036013147','1025004701941','(4967) 54-51-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'irin-r@rambler.ru', 'CE27AA3FEADE27737F87FA6C4DA0268E574AE751', 
 '2011-07-26 19:58:26.000',True,False,'������� ����� ����������','��� "�� "������"','7725682730','1097746774718','(495) 792-3902', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ftam@mail.ru', '0E1739176B026C467C1A884B31673F05A2B56445', 
 '2011-07-27 12:00:08.000',True,False,'������������ ���� ��������','�������� � ������������ ���������������� "����������� �������� "������ ����� ����� ����������"','7706671050','1077760969879','+7 (495) 210-17-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elash@yandex.ru', '860AFE405E948622500D2D209E6982E876AD9F3A', 
 '2011-07-27 14:33:30.000',True,False,'������ ������� ����������','�������� � ������������ ���������������� "����������� �������� "������"','7702640572','5077746751550','71967955273', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ramgroup.ru', 'D34AE2E1C3428C9CC98DB0252509B2FC3C6FBDC9', 
 '2011-07-27 16:06:56.000',True,False,'������ ������� ��������','�������� ����������� �������� "����������� �������� "��� ����������"','7709711832','1067759958067','8-926-022-45-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'DorzhievaLS@bbank.ru', '524C8C4AED3E2550E78E98C87DCAD781E92ED545', 
 '2011-07-27 16:30:05.000',True,False,'�������� �.�.','��� �� "����������"','323045986','1020300003460','(3012) 297-103', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'm.gordeeva@eufn.ru', 'D8C7C496D5B539CDFAF9EE0C6CA0182DD72AC6C5', 
 '2011-07-27 19:09:20.000',True,False,'�������� ����� ����������','�������� ����������� �������� "����������� �������� "�����������"','7701548736','1047796532178','641-23-54 (177)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@nezav-al.ru', '4C00DB27EE90ED3876251FA37D8E0BFDFB125079', 
 '2011-07-28 12:57:31.000',True,False,'���������� ������ ����������','�������� ����������� �������� ����������� �������� "����������� ������"','7733636640','1077764148582','(495)797-34-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'BackOfficeUA@uralsib.ru', '9B9049A761F0E4630EDB17AAC06CB77F76E6B11E', 
 '2011-07-28 14:03:06.000',True,False,'������� ���� ����������','��� "����������� �������� �������"','7702172846','1027739003489','785-1212 +2878', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'BackOfficeAM@uralsib.ru', 'BD04F73BB2DA8568D01F331C42D764F7476FA826', 
 '2011-07-28 14:04:47.000',True,False,'������� ���� ����������','��� "�������-���������� ���������"','7704144347','1027739000816','785-1212 +2878', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nlapushkina@sangri.ru', '9B72EC13015803ABA19119CC6CB620F49ECE2375', 
 '2011-07-28 17:20:40.000',True,False,'�������� ����� ������������','�������� � ������������ ���������������� "����������� �������� "������"','7715703741','1087746702493','(495) 646-04-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stabilnost-uk@mail.ru', '2A8F5D4905C0BF77D8F9D3A36A08BC1AB934C17B', 
 '2011-07-28 17:25:20.000',True,False,'������ ������ ����������� ','�������� � ������������ ����������������  "����������� �������� "������������" ','7706728919','1097746778799','(495) 514-42-48', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tezov@bioprocess.ru', 'BB20AE30A71DAE67ABE7DFA1B325DED3B6F76F15', 
 '2011-07-28 17:32:12.000',True,False,'����� �������� ����������','�������� � ������������ ���������������� "����������� �������� "���������� ������� ��������"','7703610669','5067746956052','(495) 974-74-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'i-u@invest-ural.ru', 'DA327EA73C0D950D5E047C0CE4C43A6DF7C5C887', 
 '2011-07-29 09:42:38.000',True,False,'�������� ������ ����������','��� �� "������-����"','6671253434','1086671002582','(343) 264-72-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'katia2407@yandex.ru', 'E1B227505940F3F2E8CC859DD13D6D91D5A9DCFD', 
 '2011-07-29 10:44:01.000',True,False,'������ ������ ����������','��� "���������� ����� "������"','7814079267','1037832010040','8-911-221-77-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pabasis@mail.ru', 'AF1F76DF13240E580372F6F759C7139C94E14C48', 
 '2011-07-29 15:21:41.000',True,False,'���������� ����� �������������','�������� ����������� �������� "����������� �������� "������"','7723650420','1087746275066',' (495) 643-40-77 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kaprezdu@mail.ru', 'AF1F76DF13240E580372F6F759C7139C94E14C48', 
 '2011-07-29 15:27:09.000',True,False,'������ ������� ��������������','�������� ����������� �������� ����������� �������� "�������������"','7709763809','1077761961452','(495) 991-75-94', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'esteit-capital@mail.ru', 'AF1F76DF13240E580372F6F759C7139C94E14C48', 
 '2011-07-29 16:17:00.000',True,False,'�������� ���� �������������','�������� ����������� �������� ����������� �������� "������ �������"','7715793625','1107746041820','(495) 922-91-30', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@oreol-2010.ru', 'D384B1F57FD8C07AA1018117168133F7D88C7D9C', 
 '2011-07-29 17:36:30.000',True,False,'����������� ���� ����������','��� "�� "�����"','7704750193','1107746237147','(495) 231-46-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edo@uk-ula.ru', '973FDBDC8591FD486148189657111FD0B2B5A7B4', 
 '2011-07-29 21:31:06.000',True,False,'������ �.�.','��� "�� "���" ','7722647939','1087746613800','788-58-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'farida@fc-gid.ru', '2C8F0AF73B43FEA9E8AAFF5B5F24574C52EFDD81', 
 '2011-08-01 10:56:30.000',True,False,'������� ������ ��������������','�������� � ������������ ���������������� "����������� �������� ��� ������"','1831122419','1071831004850','(3412) 912-861', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'annati@nwudc.ru', '2FCE5E070AC3D8E62208A253E8C32285C94B03EA', 
 '2011-08-01 12:24:17.000',True,False,'������ ���� ����������','�������� ����������� �������� "�������������� �����"','7806122720','1037816019647','(921)945-69-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@mpsfond.ru', '705AC91AC837B5D6B6760228153D166930B8DAE8', 
 '2011-08-01 12:53:39.000',True,False,'��������� ������� ������������','���"������������-����"','7710058270','1027739106856','699-8705', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk_sgs@mail.ru', '955F1B33B8A30B5F6FDAC8D90A7183A53735ACFC', 
 '2011-08-01 15:15:39.000',True,False,'��������� ������� �������������','�������� � ������������ ���������������� "����������� �������� "��������������"','8602168728','1108602002882','(3462) 23-39-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'trust@ronintrust.ru', 'A2AF0A09BC88440A1E37F9A253CD1AB437817B30', 
 '2011-08-01 17:22:04.000',True,False,'���������� �������� ����������','��� "����� �����"','7709379423','1027739348999','(495) 645-88-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@interfinanceua.com', '5D928408202A2BB4F33BF1BE7A58AED5EC8E2E7E', 
 '2011-08-01 21:04:12.000',True,False,'������ ������� ��������','���"����������� ��"','7707628890','5077746871802','(495)623-86-84', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'profgar@icomisp.ru', '7D6152FC51AEEA3469F92AA16C5CE05CE09F9A73', 
 '2011-08-02 09:44:45.000',True,False,'������� ��������� ���������� ','�������� � ������������ ���������������� ����������� �������� "������ ������"','7710593076','1057747611272','(495)980-24-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@landcap.ru', '801310ACC28E469235F59F45BCBE09572439D3C0', 
 '2011-08-02 10:21:09.000',True,False,'�������� ����� �������','�������� � ������������ ���������������� "���� �������"','7736585626','5087746301870','(499) 755-55-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'boa@dskf.ru', 'FDEACCE4ADBC1C57617968F1B0BCFFA8BC94364B', 
 '2011-08-02 11:57:00.000',True,False,'����������� �.�.','��� "�� ���������"','7714644250','1067746483705','8(495)940-05-06', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'krasnova@am-navigator.ru', '1C1112141068D85576F735403603712410FF0AD8', 
 '2011-08-02 13:27:57.000',True,False,'�������� ����� ������������','��� "����������� �������� "���������"','7725206241','1027725006638','7 495 710 88 28', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk@bcs.ru', 'DAC14991F8A4AF92F47962CEA3FB87DD4453EB86', 
 '2011-08-02 13:54:34.000',True,False,'�������� ����','�������� ����������� �������� ����������� �������� "������������������"','5407191291','1025403200020','8 383 210 50 20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vera@sibiryak.ru', '0CC4A6E75C06C23845142B83DA0F54B247FD3328', 
 '2011-08-02 15:00:57.000',True,False,'������ ����� ������������','����������� �������� "�������"','5433157025','1045404361529','3832280028', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kudinov_av@yamalmc.ru', 'E791DE1C3C334C6520DF6728298E2E45ACBA5FA7', 
 '2011-08-02 15:05:41.000',True,False,'��� ������� ���������','�������� ����������� �������� "�������� �� ���������� ������������ "����"','7703194377','1027700455188','(495) 225-94-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ukuser1@bcs.ru', 'C1A4A1A087DFB8AD56645911AB04D188DA587B74', 
 '2011-08-03 06:11:06.000',True,False,'�������� ����� ����������','�������� � ������������ ��������������� ����������� �������� "������������������ - ����� ������������"','5407018473','1065407138070','8 383 210 50 20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'liana@tsbank.ru', '455F400FC36179B1867AE26BFC6CD3646CFC1F64', 
 '2011-08-03 09:48:52.000',True,False,'���������� ����� �������������','��� "���"','4401008903','1024400003759','84956605500', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fox_ik@rambler.ru', '9AE3B82137E35859B88B37EEC9DE680A922F0E44', 
 '2011-08-03 11:07:48.000',True,False,'������ �������� ������������','��� �� "�� ����������"','7722557280','1057748379292','(843)510-96-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tmp@mstream-group.ru', '65CB112000F9457C3F2FCDDAA55CBF2F67576DEC', 
 '2011-08-03 13:52:51.000',True,False,'��������� �����','��� ����������� �������� "����� ����� ����������"','7725590582','1067760723568','89104406815', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buh@uk-orion.ru', '7D45C46E03AC665C2829E249BDCC19B7309A9C8A', 
 '2011-08-03 14:20:50.000',True,False,'�������� ����� ���������','��� "�� �����"','7813305971','1047855067303','(812) 313-82-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'D.Andrianov@gazfond.ru', '309CB60B5477DABB45A22594566B2C38A66C977B', 
 '2011-08-03 15:15:20.000',True,False,'��������� ����� ��������','����������������� ���������� ���� "�������"','7736149919','1027739570253','(495) 721 8383', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pay@geocapital.ru', '2164B768B9D7F027162D8DDCEFD35820A3650742', 
 '2011-08-03 23:16:00.000',True,False,'���������  ������ �������','��� "�� "����������"','5029144685','1107746860748','+7(495)9889605', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'postarel@mail.ru', 'E2FE0B9DA5C1387D0FAF80D39A57192794DDCA41', 
 '2011-08-04 10:29:58.000',True,False,'������ �������� �����������','��� "����"','1435079488','1021401061230','-4203', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'admin@uk-profinvest.ru', 'F999B30DD613EADB07C2F87A82FE7A58CB770097', 
 '2011-08-04 12:12:00.000',True,False,'������� �.�.','��� "�� ����������"','7612037793','1077612002610','(4852) 744-844', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vbukatin@volgaex.ru', '353771A1CC51C0BD78C67B916E2922A88EB812C6', 
 '2011-08-04 12:46:21.000',True,False,'������� �������� �����������','��� ��� "��������-�����"','6454027396','1026400001836','(8452) 30-40-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'std08@mail.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2011-08-04 13:42:19.000',True,False,'�������� ����� �����������','�������� � ������������ ���������������� "����������� �������� "��������� ��������� ���"','7704627898','1067760813141','514 61 17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'marusyak@bazinam.ru', '05D5E0BF09C1B344BDA682F0AFA50636F8D059E2', 
 '2011-08-05 13:30:31.000',True,False,'������� �����','��� �� "������� ����������"','7709777858','1087746174163','(495)980-8020', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@synergy-am.ru', 'D461F6EDC4003A85D348911026961F6626AF745D', 
 '2011-08-05 15:33:55.000',True,False,'�������� ������','�������� � ������������ ���������������� "����������� �������� "��������"','7702640420','5077746748118','+7(495)646-11-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'o.gorbatova@guk.su', '9AE3B82137E35859B88B37EEC9DE680A922F0E44', 
 '2011-08-05 16:41:19.000',True,False,'��������� ����� ���������','��� "���"','7708601347','1067746666844','8 (495) 589-05-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'veteran03@mail.ru', '0308D6A86E5DDDE129887297224336C11BB811A3', 
 '2011-08-05 16:56:47.000',True,False,'������� ������ ���������','����������������� ���������� ���� "������ ���������������� ���������� ���� "�������"','7707289340','1037700082672','8 916 621-14-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'evgenya@rfci.ru', 'CBBF57CA99FFA431261809E690D3D72CF259445B', 
 '2011-08-08 09:39:34.000',True,False,'�������� ������� ���������','�������� � ������������ ���������������� �������������� �������� "��������-�������� �����"','7446045354','1057421016047','+ 7 (3519) 25-32-24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ktroitskaya@crownam.ru', 'EEC8AC4BC84314A6647F06AC167D63A81098C246', 
 '2011-08-08 11:24:32.000',True,False,'�������� ������','��� "�� "������"','7801468210','1089847176814','(495) 981-65-17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'admc.su@yandex.ru', 'E912A4DB0E4B362A60990A10A5E9CF9495592F79', 
 '2011-08-08 11:58:15.000',True,False,'��������� ������� ���������','�������� � ������������ ���������������� "�����-������� ����������� ��������"','6167075170','1106195006862','78632003705', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'may@fmt.ru', 'B8B6A1A2D0B20F11CC433A9B439E195B2A6711B1', 
 '2011-08-08 12:12:16.000',True,False,'������� ������ �������','��� "�� "������-����������"','6321105912','1026301983487','(8482) 36-56-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'victory@victory-am.ru', '7673BFC22DD30DB4FB41A55C8C96535A123F07C2', 
 '2011-08-08 13:42:27.000',True,False,'������ ������ �������������','��� �� "������� ����� ����������"','7702602471','1067746540201','(495) 661-54-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mefremova@alltech.ru', '2D330BB44D974399BA7CF8D8DBD326B8E28235EB', 
 '2011-08-08 13:53:06.000',True,False,'�������� ������','��� �� �� "������" ','7731538870','1067746319695','8(495)786-67-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'smu@depoplaza.ru', '7C1CDC616374C98AA725A2992984D017B769C59A', 
 '2011-08-08 16:30:50.000',True,False,'����� ������','���  "�� "����-�����"','7731530091','1057748104996','(495) 987-18-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga@realtyfunds.ru', '7C33DA3C3E0833C58572CA5E7AC159995A4B0ACD', 
 '2011-08-09 10:27:23.000',True,False,'�������� �����','��� "������ �����"','7708705804','1097746579908','8 (499) 272 52 55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'user_0584@nhp.ru', 'DBD36B8583EBB9C786FB9C84C9EAD47F4A19769B', 
 '2011-08-09 12:53:57.000',True,False,'����������� ������� ����������','��� "���� ������-����������"','7701628727','1057749094314','8-903-227-65-71 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uksv@infonet.nnov.ru', '6CCC62D2909F8DA03C04AE7CEF44907DE6025727', 
 '2011-08-09 13:11:04.000',True,False,'��������� �.�.','��� "����������� �������� ���������� ���"','5260141958','1045207483705','8314395031', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'osipova-eg@region.ru', '5CF12CD1613E34206BA0EFA8A199C700755D3077', 
 '2011-08-09 13:32:00.000',True,False,'������� ����� �����������','��� ������� �����','7744001049','1027739926818','(495) 777-29-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nv@elby-ip.ru', '0AB2BD81BE4DE47E186CAB084843C2689BD4B88B', 
 '2011-08-09 13:56:12.000',True,False,'������ ������� ����������','�������� � ������������ ���������������� "����-�������������� ��������"','7813169662','1027806859750','8(812)655-50-05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm@infina-am.ru', '56983155591C7473E1CD72FB7A4CD095FF165012', 
 '2011-08-09 14:49:26.000',True,False,'������������� ������� ������������','�������� ����������� �������� "����������� �������� "������"','7734551935','5067746966579','(495) 780-43-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edofcsm@ukmdm.ru', 'DAB31A6A25F9042E9B3C7EDE34345D07D4B9DC6C', 
 '2011-08-09 15:37:27.000',True,False,'������ ��������� ������������','�������� � ������������ ���������������� "����������� �������� ���"','7825443207','1037843036285','+7 (495) 777-78-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'am@veles-capital.ru', '33D3D429CF72DC1F61E2DA7485C7841EE3F65C2B', 
 '2011-08-09 17:12:32.000',True,False,'��������� ���� ����������','�������� � ������������ ���������������� "����������� �������� ����� ����������"','7703523568','1047796515470','+7(495)258-19-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@unvest.ru', 'A52B4D5E986A4E12DCFA88B7C19C85015EC11201', 
 '2011-08-09 18:15:25.000',True,False,'�������� ��������� ���������','�������� � ������������ ���������������� ����������� �������� "����� ������ �����"','7715615686','5067746491160','+7(495)723-55-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'galsiz@mail.ru', 'B2654BC76DD8D04AC6AF7859D840847789E089F2', 
 '2011-08-10 08:37:08.000',True,False,'�������� ������ �����������','��� "�����������-1"','2504000490','1022501901850','89024810603', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 't.imarova@untr.ru', '6F623ED35A86C60C7355EC8ADA7BFF01BCA837E4', 
 '2011-08-10 12:14:29.000',True,False,'�������','����������� �������� "����� �����"','7710460654','1037710029719','663-38-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ovtutcheva@gmail.com', '25FB6BED131247230E457AB940BF28CAC722B6F9', 
 '2011-08-10 14:07:40.000',True,False,'������� ������ ����������','�������� � ������������ ���������������� "����������� �������� "�����" ','7708609201','5067746202750','972-74-49', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lkprofit@mail.ru', '42369745F4E033CA7EF3580335AD54327EE19E42', 
 '2011-08-10 14:45:39.000',True,False,'������� ����� ������������','��� "�� ������"','6316141785','1096316001528','+7 937 203 13 02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'capital-incom@yandex.ru', '59F5E02CE1C10C94422C1D79551B2DFD9FE292E6', 
 '2011-08-10 14:49:52.000',True,False,'��������� ��������� ������������','��� "�������������� �������� "�������-�����"','7707625803','5077746727823','(846) 207-14-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk-stnn@stnn.ru', 'A9567C6AD3427B07564E53089772DA803CDFB638', 
 '2011-08-11 13:07:30.000',True,False,'�������� ������ �������������','��� �� "������� ������"','5260266570','1095260013023','(831)2-960-900', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pension@mmbank.ru', '72E2749201051E2C142873B8387D89F3F8085045', 
 '2011-08-11 16:00:04.000',True,False,'������� ���� ����������','�������� � ������������ ���������������� "���������� ������"','7722270922','1027722009941','+7 (495) 925-80-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@rosinfocominvest.ru', '605E8ABDC4FD99A5F865392986C4009F2C999AEC', 
 '2011-08-11 17:19:02.000',True,False,'������� ����� ����������','��� "����������������"','7710692969','1077761509759','8 (499) 504-22-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '123321@mail.ru', 'A5268B1A2B5D650FCE5E1AC2F8D31850DCD6FCFB', 
 '2011-08-12 09:20:45.000',False,False,'��� ����� ����','1','0','0','1', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rgf@uk-rgf.ru', '1E0A301F9F62BE08504D64CFEE5EC9890AAAF09E', 
 '2011-08-12 10:48:56.000',True,False,'������ �.�.','��� �� "���������������"','7701248637','1025000658650','2346185', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'e.vshivkova@tfb.ru', '3F3C1A2BAEAACD16041B613D4373E01EDCC564BD', 
 '2011-08-12 10:51:49.000',True,False,'�������� ������� �������������','�������� ����������� �������� "����������� �������� "��� �������"','7709683208','1067746696951','(843) 2-911-782', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'am@globalcap.ru', '828BDCAA81A5EDA3D6D46E7E0D17FA737FE4B5B3', 
 '2011-08-12 13:08:27.000',True,False,'������������ ����� ��������','��� "�� ������ �������"','7710561645','1047796805924','(495)953-68-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'parma-nwfc@pfc.ru', '51AC59374D498E211EF66694CD3F3693514406B6', 
 '2011-08-12 14:16:18.000',True,False,'�������� ����� �����������','�������� � ������������ ���������������� ����������� �������� "�����-����������"','7706178479','1027700345276','(342)2105977', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dacapital@mail.ru', '25FB6BED131247230E457AB940BF28CAC722B6F9', 
 '2011-08-12 16:14:50.000',True,False,'��������� ����� �������������','�������� ����������� �������� "�� �������"','7725589763','1067760369445','972-74-49', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vmishenko@rwmcapital.ru', 'E292E6330AD551D1D30881BA270909D739B8F2F5', 
 '2011-08-12 17:01:53.000',True,False,'������� �������� ����������','��� �� "��� �������"','7722563196','1057749282810','84656607030', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sfc-am@mail.ru', '0190EF581F2C71ED4939664DC4D1E6750CE2C50E', 
 '2011-08-12 17:47:07.000',True,False,'������ ������� ������������','��� �� "��������� ���������� ����������"','7707512951','1047796140259','79055579093', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oligopol@list.ru', 'B74832067F8FB283C71DD7F1796E2731414AE13F', 
 '2011-08-12 17:49:04.000',True,False,'������ ������� ������������','�������� � ������������ ���������������� ����������� �������� "������� ������" ','7728609440','1077746284252','79055579093', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@kapinvest-m.ru', '902A0D377E5659C643594FAD02774753E4D1EC89', 
 '2011-08-15 10:45:07.000',True,False,'����� ������ ����������','��� "�� "������� ������ ����������"','7709735880','5077746689224','(495) 795-06-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@zaonic.ru', '14C49971A3EA6BC22C1E155ECBC7E650413D68F6', 
 '2011-08-15 11:14:12.000',True,False,'���������� ���� ���������','�������� ����������� �������� "����������� �������������� ��������"','3445012920','1023402639700','(8442) 97-03-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gaponova@fkpft.ru', '3A7478BB03CA0CCC419DFE53DEAEDBD84F3B21F5', 
 '2011-08-15 11:36:02.000',True,False,'�������� ������� ����������','��� "�����"','7729393909','1037700144976','8 (499) 755-74-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ukpi-osd@mail.ru', '25F50042C3C238C22BA6ACBC2DADBA1DF37EE1B3', 
 '2011-08-15 12:44:43.000',True,False,'��������� �������','��� "�� ������������ ����������"','7725529468','1057746027767','(495)981-93-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'semina_s@serveying.ru', '9AE3B82137E35859B88B37EEC9DE680A922F0E44', 
 '2011-08-15 14:32:01.000',True,False,'������ �������� ������������','��� ����������� �������� "�������-�����-������"','1660108972','1071690075930','(843)510-96-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@pragmacapital.ru', '34B8E850FD12E9C7EBF2F1C31F0F2BF40CD61074', 
 '2011-08-15 15:20:45.000',True,False,'������ �������� ���������','��� "�� "������ �������"','7718581523','1067746469658','+7 495 926-5682', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uk-kontinent.ru', '0FCC5E7121D193D44ED27782321E9961E031CC61', 
 '2011-08-16 10:28:23.000',True,False,'������ ��������','��� "�� "���������"','7701720017','5077746720739','(495) 517-63-80', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tanya@bfd.ru', 'DF0A55FBC8627EC9E99CAE63638A4B219301DA00', 
 '2011-08-16 12:10:44.000',True,False,'��������� ������� ����������','��� "����������� �������� "�������-������"','3808083878','1023801018922','(3952) 24-01-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bashfic@alor.ru', '4F1341EF280464730E0709153F044B4D6AA9D94C', 
 '2011-08-16 14:23:24.000',True,False,'������ ���� ����������','��� "���������� ���"','274088744','1020202555174','(347)291-74-74', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edo@terra-am.ru', '4730DB3B9DD3668E4289C1D19F8A1E910E5863D8', 
 '2011-08-16 14:28:08.000',True,False,'������� ����� ���������','�������� � ������������ ���������������� "����������� �������� "����� ����� ����������"','5003079784','1105003003742','(495) 280-07-38', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sum_05@bk.ru', 'CDC9F610295DA331B2F3A3DD62998586A723BC94', 
 '2011-08-16 17:59:58.000',False,False,'������ ���� ��������','��� "�����"','7841446766','1077847807762','+7 (812) 888-88-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'koa@npf-gefest.ru', '2578A878C431389F0BEF56CF48775C102403BCAA', 
 '2011-08-17 10:52:10.000',True,False,'�������� ����� �������������','��� "������"','7710032955','1027739137524','(495) 989-60-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@investrateg.ru', 'B4D938B94F7EA07B85533FDBE39B65F0C2C62A7C', 
 '2011-08-17 14:10:02.000',True,False,'��������� ����� �������������','�������� � ������������ ���������������� ����������� �������� "��������� ����������"','7727591983','1067758541135','(499) 763-6120', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk_cm@mail.ru', 'A6507210C183D89CD225364091EAFB1F747F78A5', 
 '2011-08-17 16:41:15.000',True,False,'��������� ����� �����������','���"�� "����� ����������"','7701868359','1107746172907','8(9044)713018', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'spb@ekaservice.ru', '2D96AB04AD224D904941BB7FF03396DBE8930987', 
 '2011-08-19 10:09:24.000',True,False,'������ ������ ��������','���-������ �������','7723530997','1057746158997','89166879649', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gazgeo-garant@rambler.ru', '31A4B46F5CC40FAA501BB2B156779BD1DEE68B09', 
 '2011-08-19 11:29:48.000',True,False,'������� ���� ����������','��� "������-������"','6901006619','1026900560224','(4822)77-87-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lenta@krupa-muka.ru', 'F207F9BC716C597EDC3146DF70BA425D54D45E58', 
 '2011-08-19 15:32:31.000',True,False,'������� ������� ����������','�������� ����������� �������� "����������� �������� ��������������"','4401012829','1024400511981','(4942) 49-08-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivlev-npf@mail.ru', '8685AB6D3746346E17A3DA3206C6F902F7246AAB', 
 '2011-08-22 11:54:03.000',True,False,'����� ������ ������������','����������������� ���������� ���� "����������"','7729158486','1027700397152','8-495-431-50-97', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mc@custom-capital.com', '469F268F381A2EF9E7DF4114448848FDC214CE84', 
 '2011-08-22 11:58:07.000',True,False,'������������ ������� ������������','��� "����������� �������� "������ �������"','5902212884','1075902012735','(342) 299-99-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uc@kartoteka.ru', 'B424009F9A77B19ACAE46D2DE9A516E2EBF541E0', 
 '2011-08-22 14:00:44.000',True,False,'��������� ������ �������','��� "����������� ���������"','7713038962','1027700177130','88001008555', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zg@goldguild.ru', 'DCE3E7D9D853A4578BB787FA43EEA9CF72C3819A', 
 '2011-08-23 09:42:05.000',True,False,'������� ��������� �������','�������� � ������������ ���������������� "�������������� �������� "������� �������"','7701888059','1107746687014','(499) 235 30 31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'I2BF@vtbcapital.com', '263F1E5518DA2B6F9ABB0E85E27BE37A90D8BD7C', 
 '2011-08-23 10:55:57.000',True,False,'��������� ����� ����������','��� ��� ������� �������� ��������� �������� ','7703714516','1107746056483','(495)7255540', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alex@agio-finance.ru', 'ED740F3A21FC346F9D66441DA8C964983BC9EB95', 
 '2011-08-25 13:30:07.000',True,False,'����� ��������� ����������','��� "���������� �������� ����"','4346007053','1034316500591','(8332) 711-000', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@grandiscapital.ru', '6224D01A525A116C7C663F9E7437D6E0698E8076', 
 '2011-08-25 14:48:21.000',True,False,'�������� ����','��� "�� "������� �������"','7723623472','1077758757010','7(495)2844407', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'goldgroupcompany@gmail.com', '1E2897270819B481B722808FB2B6AFDDD686B66C', 
 '2011-08-25 17:18:54.000',True,False,'������ �����','��� �� "��-����������"','7710684830','1077758801328','495-558-46-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf@rfpo.ru', 'A50661CA64451D2F301529293C474FA0A9A58E1D', 
 '2011-08-26 15:53:39.000',True,False,'�������� ��������','��� "������� ���� ���������� �����"','7704039536','1037700015430','(499)160-00-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'czpf@rfpo.ru', 'DDFD74CF00BC0EAE8E16D5CB4561E240B8191FA1', 
 '2011-08-26 15:58:08.000',True,False,'�������� ��������','��� "������-�������� ���������� ����"','7804154839','1027802505730','(499)160-00-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'talykova@financeproject.ru', '11C98F9E41C5ED064D5AD59A24438EFCF290B41A', 
 '2011-08-26 17:09:04.000',True,False,'���� ��������','��� "���������� ��������������"','7713622487','5077746881416','(495) 755-50-60 (1614)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'contact@standart-invest.org', '75E5D4F96162F5CF08AF94664EB49FF7965F9AFC', 
 '2011-08-29 12:19:31.000',True,False,'��������� ���� ����������','�������� � ������������ ���������������� ����������� �������� "��������-������"','6674318778','1086674032983','(343) 382-91-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vitushkin@creditural.ru', 'D57E45686CD01FDA14852BB8BEBCB747B9E254CA', 
 '2011-08-30 09:52:00.000',True,False,'�������� ������� ���������','��� ������ ���� ����','7414006722','1027400000638','8(3519)24-89-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '317619@belinfonalog.ru', 'F24484C4BA7722459AFFA12639825CE125CB61BA', 
 '2011-08-30 11:02:57.000',True,False,'����� ������ ����������','��� "������������"','3123101188','1033108000540','79103639944', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Alexey.Egorov@erbebank.com', '028D74F09D36B2E97F46BEDFAAF50ADD3EE91572', 
 '2011-08-30 11:12:04.000',True,False,'������ ������� ����������','�������� ���� (���)','7744000888','1027700259058','(499) 254 72 00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@nkk-sd.ru', '1A7147E7AE94E4250C3D4A1D6596D5EA577E60BB', 
 '2011-08-30 12:45:09.000',True,False,'���������� �.�.','���','7707592234','1067746739554','8(495)280-06-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ilona@gothica.ru', 'B81834FDFA3862F7BA2A18C625AA3C2FA71FF47F', 
 '2011-08-31 07:46:04.000',True,False,'������� ������� ���������','�������� � ������������ ���������������� "��������������� ���������� �����" ','5405282715','1045401954267','(383)2187581', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bk@gothica.ru', '5F60ED8B4FF30A0A41B6C73BA2603D0F9B9E3DF9', 
 '2011-08-31 12:23:58.000',True,False,'������� ����� ����������','�������� � ������������ ���������������� "���������� �������� ������"','5405262148','1035401943939','(383) 2187-581', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nadezda.demidenko@bcc-msk.ru', 'AB442C130CE8ECE98D1AD0F8B97E8D13341F67B6', 
 '2011-08-31 15:58:26.000',True,False,'��������� ������� ����������','�������� ����������� �� "���� �����������" �������� � ������������ ���������������� "���� ���-������"','7750004263','1077711000201','+7 (495) 707-22-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a_guminuyk@mc-alliance.ru', 'EE0982574C5BF2BE2CA18F84F2645D354C6CF352', 
 '2011-09-02 10:22:02.000',True,False,'������� ��������� ���������','��� "��"������','5407039628','1075407027354','(3822) 24-88-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk@region-don.ru', 'F1CE186352E50300775E0C39E0A583E9E028D42F', 
 '2011-09-02 10:44:52.000',True,False,'������������ ���� ����������','�������� � ������������ ���������������� "����������� �������� "������ ���"','6163079463','1066163056190','8(863)2910580', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a.antipov@stgorod.ru', '72F13E677C13A55E7F4A8C1F8D7E3CEF809C4C9C', 
 '2011-09-02 12:45:02.000',True,False,'������� �.�.','��� �� "������ �����"','7707325292','1037707022990','(495)3613123', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kurs@vluki.ru', '1AFC39DCEA146A334B6CFBE79C683BDC8FF6BA4F', 
 '2011-09-02 15:21:29.000',True,False,'��������� �.�.','��� "����"','6025013517','1026000901464','8(81153)3-62-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'degtev-av@1mbank.ru', '9C8EABC3DA20BA417B2465D10FE86472332E4175', 
 '2011-09-05 11:36:23.000',True,False,'������ ������� ������������','���� "������������" (���)','2310050140','1022300001063','8612790557', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'angelina.batasheva@ghpgroup.com', '23DBDC2F46989D1909FB4F053B11DD571CAED3A0', 
 '2011-09-05 11:51:12.000',True,False,'�������� �������� ����������','��� ����������� �������� ��� ��� ��','7702526051','1047796382237','+7 (495) 411 5311', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kravchenko_ea@uk-universal.ru', 'A3FF7A3B43B33966303B177163097B5C0477D606', 
 '2011-09-05 17:27:16.000',True,False,'��������� ����� �������������','��� "����������� �������� "���������"','7701728457','5077746938561','8 926 614 78 18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sveta_k@profbalance.ru', 'A456BF2B278170A54DC1AB693EA90D30DC500FC7', 
 '2011-09-06 14:59:03.000',True,False,'����������� �������� ����������','�������� � ������������ ���������������� ����������� �������� "����������� ������� ����������"','7714706482','1077758733161','8 (495) 234-98-29', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'project@palitra-finance.ru', '3B59FB12172EB5666BE7DAD92E46BB156443C82B', 
 '2011-09-07 14:54:47.000',True,False,'�������� ������� ������������','���"��"�������������"','7726660828','1107746741354','(495)633-75-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zaigraev@e-moskva.ru', 'A4BE809071DFA2D64F413D46D53490F768616AB7', 
 '2011-09-07 15:14:31.000',True,False,'�������� ��������� �������������','��� "����������� ������"','7707314029','1027707013806','988-22-73', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'apet@nordbrook.ru', '3EA0C25157E1AE645349A5CA26A198FED42BC881', 
 '2011-09-08 11:32:24.000',True,False,'��������� �������� �������������','��� "����������� �������� "��������"','7710322830','1027739420994','(495) 258-92-84', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@sibcaptrust.ru', '13F490813AE0F7B5E34AFCA39AA68156F81C1BEF', 
 '2011-09-09 09:31:33.000',True,False,'������ ������� ������������','�������� � ������������ ���������������� ����������� �������� "���������������"','5402487136','1075402016370','(383) 211-90-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sadikova@ipotekart.ru', 'C751EACF950662F7A2555D1F69956A1A0CF593D7', 
 '2011-09-09 14:17:17.000',True,False,'��������� ���� ���������','��� �� "���� ����������"','7715640298','5077746308020','79179362090', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'compliance@dominantaasset.ru', '780E98062CA6AA378A09E38C2E3DBBCE4AB8188F', 
 '2011-09-11 16:22:43.000',True,False,'���������� ����','�������� � ������������ ���������������� "��������� ����� ����������"','7710861695','1107746038740','495 540-55-67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'titova@ocgpi.ru', '1494E19F522DBEC99E442F2BB71DC5161868B429', 
 '2011-09-12 15:31:26.000',True,False,'������ ����� ����������','��� "�� "��� �� ��"','7725616128','1077759683880','650-77-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Andrew@ppafond.ru', '2A4784D2E98FE4B7F98A3CCDB3DF14D4CCAEA8DC', 
 '2011-09-12 17:29:54.000',True,False,'���������� �.�.','��� "������ ������������ ������"','1654036021','1021602847859','(843) 264-44-19', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@ci-funds.ru', '5D12588E7C340C4FDC9E31DB4B030DC78B138A45', 
 '2011-09-13 12:54:39.000',True,False,'���������� ������','��� �� "������� ������"','7714547360','1047796263756','(499) 550-14-04', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lifebroker@mail.ru', '3CBA825BB3A01BFB5DFE3A084617A3D1ADD4FCDE', 
 '2011-09-14 11:15:57.000',True,False,'����� ����� �������������','��� "���� ������"','7725686206','1107746034153','+7(495)663-61-79', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'davlikanova@ratio-capital.ru', '3214D80B10447428D1A7E3B2A552111D386305AB', 
 '2011-09-14 12:51:28.000',True,False,'����������� ����� ���������','�������� ����������� �������� "����������� �������� "�����-�������"','7725217596','1037725001027','(495)984-63-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'inform@mcsputnik.ru', 'A32E4F3124C1D117080AD93BD075B4EB626C8BCE', 
 '2011-09-14 14:52:51.000',True,False,'��������� �.�.','��� "�� "�������"','7704652277','5077746938726','(812)335-07-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bydancev@garant.ru', '47FF18DD80DBDEB10378B862A6D29EAFBCC0D7DD', 
 '2011-09-14 16:05:48.000',True,False,'�������� ������ ����������','��� "����������� ��������"','7729633131','1097746293886','(495)647-98-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pronyaeva@atb.su', '129FC1AF235A07052D7317563063F2306AEFECDD', 
 '2011-09-14 20:16:11.000',True,False,'�������� ������ �������','��������-������������� ���� (�������� ����������� ��������)','2801023444','1022800000079','(495)9883061', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'reporting@pension-am.ru', 'E701DC737DB0FA4ED38CE6235A3588A69A0E0B82', 
 '2011-09-15 11:37:18.000',True,False,'������� ������� ���������','��� "�� "���������� ����������"','7701853183','1097746641684','+ 7(499) 241-60-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aakoval@vfp.ru', 'A39059CEFA9E94D110F1276EF057132AAA611ADC', 
 '2011-09-15 15:18:23.000',True,False,'������ ��������� ����������','��� "���� ���������� ����������"','7713006720','1037739020550','(499) 256-52-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ludmila@gerfin.ru', '25F50042C3C238C22BA6ACBC2DADBA1DF37EE1B3', 
 '2011-09-15 16:17:31.000',True,False,'�������� ����� �����������','�������� � ������������ ���������������� "����������� �������� "������" ','7714697950','5077746878633','748-05-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcstavros@rambler.ru', '96DF39D0E7CA437558EFF359BD9B0C579E942852', 
 '2011-09-16 13:27:10.000',True,False,'��������� ��������� ����������','��� "���������� ��������"�������"','7702746441','1107746936945','74994017746', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'karpenkov@amc-pcapital.ru', '3502139082D857292AF87A9DFE2E080E234429AA', 
 '2011-09-21 13:01:10.000',True,False,'��������� ������ ��������','��� "�� "��������-�������"','7722612968','5077746869910','8-925-0102923', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dmvas@yandex.ru', '0A195D9E13DB80F8CF65B4375C6717A524BEE1CC', 
 '2011-09-22 12:24:01.000',True,False,'����� ������� ����������','��� ������-���','6673240328','1116673008539','(495) 212-21-12 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@oboronregistr.ru', 'EE01D4EC7635D77B4DDB466BBE7AA0307CAC1EDA', 
 '2011-09-23 10:56:01.000',True,False,'������� �.�.','��� "�������������"','7731513346','1047796702843','(495)788-75-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'etyk@bk.ru', '5F3A53DD9B577C3AFC3CFBB1DD5E0FDA7E2F085E', 
 '2011-09-26 12:55:48.000',True,False,'������� ���� ����������','�������� � ������������ ���������������� " ����������� ��������� ����������� ��������"','7719619265','1077746281139','89104337944', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sla@actions.perm.ru', 'C7042AF04079CEE33D502FB23C6DD37C0204A924', 
 '2011-09-27 09:00:36.000',True,False,'���������� ������� �����������','�������� � ������������ ���������������� "�������������� �������� "�������� �����"','5902827091','1055900307561','8(342)2700-310', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kiu@npf-uralfd.ru', '3EEA4D374487F74D45C4A00292D2D69A27770A41', 
 '2011-09-29 15:43:00.000',True,False,'��������� ����� �������','�� ��� "���� ��"','5906033100','1025901377776','(342)2570223', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@euroins.ru', 'F8403BCCAEAE29E5A8AB45ED4372858F7590294D', 
 '2011-09-30 13:05:36.000',True,False,'����� ������ ������������','�������� ����������� ��������� �������� "����������� ������������� �����������"','7706628777','5067746032283','6265800', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'schukin@rcsinvest.ru', '47AAC7577C86DFED2C902D27EAC8AD171A695B47', 
 '2011-09-30 17:30:26.000',True,False,'����','��� "���"','7736235212','1037736017241','+7 (495) 921-0863', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'admin@uralliga.ru', 'E29809FFE82BDB360B62D1021143CC767E6D0D9A', 
 '2011-10-03 08:54:30.000',True,False,'������� ������ ������������','��� �� "��������"','7453011395','1027400000803','+7 (351) 267-07-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond@olma.ru', '388547BB40F8DE2B2AED059369FCCEECAF739DD1', 
 '2011-10-04 12:02:10.000',True,False,'����������� ������� ����������','��� "����������� �������� "����-������"','7707500642','1037739831326','699-82-41', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bair@sh.ru', 'B6CDBE3F8EF5259795CC4B654DB0D3AE85373F54', 
 '2011-10-05 12:36:57.000',True,False,'������� ���� �����������','�������� � ������������ ���������������� "����� ����� ����������"','5407035239','1075407017982','(383) 218-87-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'hunter_onpantera@mail.ru', 'B2E5F64BA3AA77BC5F38DFE4E5D932BCBB871EB3', 
 '2011-10-06 13:53:40.000',True,False,'����� ������ ����������','��� "����������"','268030947','1020202085144','8-3473-20-53-86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a.firstkov@bankcor.ru', 'BE033FE920E0C81529F8C585E6792D94A1AD782C', 
 '2011-10-06 14:13:23.000',True,False,'�������� ������� ������������','�������� ����������� �������� "����������� ������������ ���� "���"','3444074465','1023400000271','(8442) 26-75-45', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@dominion-am.ru', '41EE3BCA1C26675AD00234E416DA22C6D780DCC7', 
 '2011-10-06 15:35:44.000',True,False,'������� ���� �������','��� �� "��������"','7709804389','5087746110030','(495)790-74-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'v.striganov@fcpf.ru', 'D4B9CAB911556F61E95971639FB7590F44DCAD96', 
 '2011-10-10 10:33:18.000',True,False,'��������� �.�.','��� "����"','7704133578','1027739088410','9150240488', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kochanova@ks-bank.ru', '6DCCF30A25242DE5D3F2DE4503A5CCC62E9CB0E9', 
 '2011-10-10 16:05:23.000',True,False,'������� ����� ������������','����� "�� ����" (���)','1326021671','1021300000072','(8342)32-75-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uktrast@aaanet.ru', 'C6BD716ABA3004EBD17F70F7F74E9F085277F8BA', 
 '2011-10-11 10:47:11.000',True,False,'�������� ����� ����������','�������� � ������������ ���������������� "����������� �������� ������������������ ����������� ������� "���������� ��������� ��������"','6167065214','1026104143735','8(863)2910800', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'niy@bankfininvest.ru', 'CC9A5A034F31B969359DD3E77A5BF0B9F621AA30', 
 '2011-10-11 12:27:10.000',True,False,'�������� ����� �������','��� "���� ���������"','6436003219','1026400002310','(495) 777-000-3 1211', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@buzulukbank.ru', '8A073798830BA33C6507D1D9536CD2C72398FBBD', 
 '2011-10-11 13:15:33.000',True,False,'�������� ������ ��������','��� ���������� "���"','5603009098','1025600002064','73534244307', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@highwai.ru', 'B99086AF1A9D36D0256C27EA47016D354417E6D2', 
 '2011-10-11 15:33:08.000',True,False,'�������� ������ ������������','��� "�� "����������"','7805464819','1089847242682','(343) 378-64-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'support@naufor.ru', '285C79056A03A1872885558E8061E893E074ECF7', 
 '2011-10-12 10:33:34.000',True,False,'��������� �.�. ','��� "�������������� ����� "������" ','7709613659','1057747179379','+7(495)787-77-74', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@geofinance.ru', '49AA0A281922504797851148142EA51E0BAA0D01', 
 '2011-10-12 11:52:06.000',True,False,'������ ������ ����������','��������  ����������� �������� �������������� �������� "����������"','7708665238','1087746316635','728-49-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@akbars-capital.ru', '15DE852F388AF733CD2829C553D1332EE4412087', 
 '2011-10-13 12:26:23.000',True,False,'������� ����� ������������','��� �� "�� ���� �������"','1435126890','1021401047799','(843) 523-25-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dolqaleva@mcri.ru', '582D347C00ED4870AEFE8B1AA5FEC50B88103751', 
 '2011-10-13 15:44:39.000',True,False,'��������� �������� ����������','��� "����������� �������� "�������� ����������"','7703551413','1057747030000','499 256-23-49', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ukac@comail.ru', '3F00E8D9D72D73D82BCC3A06B9A549A9142F43DB', 
 '2011-10-13 20:23:16.000',True,False,'��������� ������ �������������','��� "����"','7714243273','1027739021144','(499)747-74-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'albert8@devoncredit.ru', 'D3D61A14016E3B43497288359FA378EF8195A840', 
 '2011-10-14 14:16:49.000',True,False,'����������� ������� �����������','����������� ���� "�����-������" (�������� ����������� ��������)','1644004905','1021600002148','(8553) 45 64 04 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'centavr_invest@mail.ru', '46477A1BDB88EEED82F00F49DB8CA3501A194004', 
 '2011-10-17 10:08:40.000',True,False,'������� ������� ����������','��� "������� ������"','4345234180','1084345015512','(8332) 375374', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ibpr@yandex.ru', '6E1FFDC4C942E611D25B279300814EA480B6A756', 
 '2011-10-17 13:51:11.000',True,False,'�������� �.�.','��� "����� ���"','6164069394','1026103277551','8632414672', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nata@abb-bank.ru', 'B4D5A3E1E60EA99B52A4BB36DC327CB1012C2A29', 
 '2011-10-17 14:40:50.000',True,False,'�������� ������� ������������','������������ ���� "���" (�������� ����������� ��������)','7744000158','1027739080852','(495) 748-1313', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uralim.ru', '09074878E159E7AD9FC3A91C2531D140623E2506', 
 '2011-10-18 13:12:15.000',True,False,'������� ������ ����������','��� "���� ������ ����������"    ','5904185160','1085904005880','(342) 241-21-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'davydkin@zerich.com', 'C9DCD8B2E261861A2C521084EC14CB47D4DF4101', 
 '2011-10-18 16:12:33.000',True,False,'�������� ������ �������������','�������� � ������������ ���������������� "����� ������" ','7704603343','1067746704585','(495) 737-05-80', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'garina@zerich.com', 'F6DE954873D7F826A4F33DC3BC496807E6C4084F', 
 '2011-10-19 15:35:24.000',True,False,'������ ����� ����������','�������� ����������� �������� "����� ������������ ��������"','7704518200','1047796285492','(495) 737-05-80', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'universal@msk-garant.ru', '22B3C261D8035F75410E896C603A735BC3D9F94D', 
 '2011-10-20 12:34:20.000',True,False,'����� ���� �������','����������������� ���������� ���� "���������"','7723014992','1037739637682','(495) 923-52-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pugachevag@bankglobus.ru', 'DD59F9FED2B7BF6EDE699BC1875EED42E8E17FB4', 
 '2011-10-20 13:36:28.000',True,False,'������� ������ �����������','��� "������" (���)','7725038220','1027739050833','(495)6440011', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'loschilova@horizon.ru', 'F5606D9C8CBFBBD294EE977660520EA8D77E3EC5', 
 '2011-10-20 16:45:54.000',True,False,'�������� ������� ����������','��� "�� "��������"','7718599915','5067746340525','(499)793-42-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'spirin@mail.ru', 'FCCB9FD388644D92030D9F226F009A32BFC749DD', 
 '2011-10-21 04:21:30.000',True,False,'������ ������� ������������','��� "��������"','7703037470','1037700041169','79084458899', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alekhin@npfsocmir.ru', '308984BF656B0D56965BAB4444EC45B3DA92F831', 
 '2011-10-25 11:39:22.000',True,False,'������ ������� ����������','��� "���������� ���"','7726031481','1037700130038','(495)5140618', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stocks@pervbank.ru', '2D9CD55054E748F9E65E40B44E9C4112FD86022A', 
 '2011-10-26 07:30:06.000',True,False,'���������� ���� ���������','��� "����������������"','6625000100','1026600001823','8(3439)661183', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'back_office@ikac.ru', '037461C4B15C47AAE1E3651076BFD368EB4FEEB3', 
 '2011-10-26 11:50:31.000',True,False,'��������� ������ �������������','��� �� "������������� �����"','7718546254','1057746780959','(499)747-74-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'romanov@promtrad.ru', '025803D84FA4622D2D93B4589CB11B570929D431', 
 '2011-10-26 12:08:26.000',True,False,'������� ������ ������������','�������� � ������������ ���������������� "������������ ��������"','7721100460','1027739465533','921-08-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'SkorohodE@mfc.ru', 'BF17BB11A593F291E3592E34F68A58497B88E62A', 
 '2011-10-26 14:22:25.000',True,False,'�������� ��������� ����������','�������� � ������������ ���������������� ����������� �������� "�������"','7717668676','1107746097502','74959800049', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gai@mspbank.ru', 'D7E7BD3C451659B1D69823C783C07E32237A5104', 
 '2011-10-26 17:24:50.000',True,False,'�������� ���� ��������','���� ������ ���������� "���"','7704760674','1107746602820','(495)783-79-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'plygun@npfbm.ru', '41404408EB010E58CDF8516AC4798ED8C969F318', 
 '2011-10-28 10:34:34.000',True,False,'������ ���� �������������','��� "���������� ���� ����� ������"','7715018810','1027739462167','(495) 985-8000', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'marina@figroup.ru', '39D74838F6E949D8B60F4883FDBEC8FA04742352', 
 '2011-10-28 12:19:17.000',True,False,'�������� �.�.','��� "������"','7826041772','1027810222197','812-540-58-04', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'i.tumaykina@npfdoverie.ru', '25EDD1D891EAF46C7057E6BAC0541CF75377B85A', 
 '2011-10-31 10:25:19.000',True,False,'��������� ����� ������������ ','����������������� ���������� ���� "�������"','5260068120','1025203023460','(495)6041406 2609', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rnpf@mail.ru', 'B58A6C1966C84CA983DC25F2E1B34B5F602F0974', 
 '2011-10-31 10:00:36.000',True,False,'�������� ����� �����������','���� "���������� ������"','323079897','1020300966620','(3012)55-97-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'anton@ekey.ru', '833DDFA5073602771670085ABD23ED0C2D3F37AA', 
 '2011-10-31 14:44:27.000',True,False,'��������� ����� ���������','��� "�������������� �����"','5260112900','1025203039840','79601860725', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ptbcapital@gmail.com', '7BD6A133A77B9CE3573ABF99CD56EF5361F4AA57', 
 '2011-10-31 13:56:04.000',True,False,'�������� �������� ������������','��� "��� �������"','274162148','1110280064586','(347) 276-43-24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elizaveta@absolut.ru', '8E5987C42E08A9BF5EDFD589A82B0477CBA3BAA2', 
 '2011-11-01 15:52:58.000',True,False,'����������� ��������� ����������','�������� ����������� �������� "�������-������"','7709050780','1027739301776','7(495) 925-00-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'back@stalinv.ru', 'B5469972FD084551E4908425BE44276183919E5A', 
 '2011-11-02 15:04:07.000',True,False,'�������� ������� ����������','��� "�����-������"','7826021127','1037863009370','(812) 633-31-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kp@mctrust.ru', 'DFD0BE9928F2CCE0ECCAB7633B182D39279F923F', 
 '2011-11-03 15:12:05.000',True,False,'������ ��������','��� "����������� �������� "������� ������� �����"','7707559942','1057748134025','(495)223-70-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nfc@nfc-2010.ru', 'EC64B58DB394D61A4077CCC0CB3743F86EFDFE4C', 
 '2011-11-07 19:23:52.000',True,False,'�������� ������� ����������','��� "�� "����������� ���������� �����"','7701862460','1107746008127','89163091949', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk_altyn@ufamail.ru', 'EFB9E22D52B83FF52188262DC4C40DBAC7BE535F', 
 '2011-11-08 14:20:36.000',True,False,'�������� ����� ����������','��� "��"�����"','274120973','1070274006560','495-363-07-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kamalov_ru@mail.ru', 'A55E5B47ED7D30A4A7C91054A1CFD3BF25361CEC', 
 '2011-11-09 12:38:36.000',True,False,'������� ������ ��������','��� "�������"','4307014226','1114307000092','89127064566', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'knv@nskbl.ru', 'F50F878F5E7F2B2E7ECF56AF324B463F34BE07C1', 
 '2011-11-10 11:33:14.000',True,False,'������������� ������� ����������','������������� ���������� ������������ ���� "������������" (�������� ����������� ��������)','5404154492','1025400000010','(383) 361-30-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@trinfico-pm.ru', 'CE377F75686333277D4EA04AB44C452218511CA0', 
 '2011-11-10 12:21:25.000',True,False,'�������� ������ ����������','�������� ����������� �������� "������ �������������� ���� ������������ ��������"','7704038268','1027739441971','(495) 725-2500', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bobko@alor.ru', 'F56C02207E615F4BD6733605E5EA42F06E179AC4', 
 '2011-11-10 15:43:53.000',True,False,'����� ����� ����������','�������� ����������� ��������','7706028226','1027700076117','(495) 9802498', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@tetis-capital.ru', '4EACFADB979869AA4F4113A2D712D7DB0EFDE070', 
 '2012-01-13 16:55:57.000',True,False,'��������� ������� ����������','�������� � ������������ ���������������� "����� �������"','7709853192','1107746374262','(495) 797-2500', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vita@vitadepozit.ru', '20F28730183F3D57938F758320AF7EE16B45A9C7', 
 '2012-01-13 17:03:45.000',True,False,'������ ����� �������������','��� "����-�������"','7716573661','5077746437698','8442946666', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'shevnina@bc.sp.ru', '6E8D597CFDD94C7967586BE2BDEFD09057751A44', 
 '2012-01-13 17:32:06.000',True,False,'������� ����� ����������','�������� � ������������ ���������������� "����������� �������� "�����"','7813316363','1057810095551','(812) 325-92-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mahalin@aetp.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2010-08-02 13:44:23.000',True,True,NULL,'NULL','NULL','NULL','NULL', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lan1979@mail.ru', '1F8353370CD3DA850E65ED5BC005ADE61EDF598A', 
 '2012-02-01 16:33:51.000',True,False,'���','test1','5257034988','1025202397043','1234567', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mironov@aetp.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2010-08-02 13:44:23.000',True,False,'MES','mironov','1111111111','111111111111','7123456', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'amalpha-style@mail.ru', '9F131A2EEFFA45631AF4A5BC7B4CC22DBCC3DBF1', 
 '2012-02-22 10:30:46.000',True,False,'�������� �.�.','��� �� "����� �����"','7715664027','1077759482525','8 (499) 703-12-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fc-glass@mail.ru', '36633F76DC1EEFE6F5B2DCD1544DE801EAF53ED6', 
 '2012-02-22 10:37:54.000',True,False,'������� �.�.','�������� � ������������ ���������������� "���������� �������� "GLASS"','3436008194','1023404961436','(84457) 5-00-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@armada-invest.ru', '391EB5B234907CCF18D30B17AC59CA91E6D6C40E', 
 '2012-02-22 10:46:04.000',True,False,'�������� ������ ����������','�������� � ������������ ���������������� "�������������� �������� "������"','7704723224','1097746143527','740-33-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sazonova@ik-forum.ru', 'A96D9B4756D59FD2C809389BF530AFA4A9F4FBF5', 
 '2012-02-22 10:55:48.000',True,False,'�������� ����� ������������','��� "�� "�����"','7706548521','1047796618979','785-94-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'larisa@srkregion.com', 'DA70B98D81BBD51FEA8CD3FCAC8B2F223F9D2EF0', 
 '2012-02-22 11:16:33.000',True,False,'������� ������ ����������','�������� � ������������ ���������������� "������������������ ��������������� �������� "������"','7107066293','1027100964681','(4872)307-644 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@314am.ru', '8DAC038B936A486B9979E2FD96DE4E0527D261A5', 
 '2012-02-22 11:22:49.000',True,False,'��������� ��������� ����������','��� "������ ��������� ����������� ��������"','7725607660','5077746853696','(495)931-98-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fklexa1@gmail.com', 'C01D96D58EE092671B7EDB58E0D8C2607F03B64F', 
 '2012-02-22 11:23:19.000',True,False,'�������� �������� �������������','��� �� "���� ������"','7714829212','1117746054920','89686110417', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@trend-uk.ru', '20A6AA2D1427ACEE227DC05265AA671A79F98C02', 
 '2012-02-22 11:25:17.000',True,False,'������ ������� ��������','��� �� "�����"','1633006555','1021607352656','+7 (495) 988-39-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@emerald-am.ru', '76F2E36BDB3173F7DC30ABC7DEE4188F4B8EF46B', 
 '2012-02-22 11:32:52.000',True,False,'����� ������ ������������','��� �� "�������� �����"','7719637948','5077746957745','(499)703-18-28', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zemlia@zemlia.ru', '4AD50D44A73A3DCD15B3423EC8E3ED6EBDD2F974', 
 '2012-02-22 11:35:56.000',True,False,'��������� ����� �������������','�������� ����������� �������� "�����"','5502014510','1025500733840','(3812) 252-259', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@investtransgas.ru', 'DA8F2C7CED81903A3F9C8F35BA54E383D9E2D925', 
 '2012-02-22 11:36:25.000',True,False,'�������� �������� ��������������','��� �� "��������������"','7706659976','5077746874079','(499)703-14-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'andrej.avinov@mrcbroker.ru', 'EB2EAADA3DC7CD9F231FDE7E2B23464CB25BF502', 
 '2012-02-22 11:39:21.000',True,False,'������ ������ ����������','��� �� "��.��.��."','7705780063','1077746379996','+7 (812) 777-50-26', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@mt-i.ru', '6504C42CA484A9BF3466A5B1B41A112E18F622D3', 
 '2012-02-22 11:50:57.000',True,False,'�������� ������� �������������','��� ��� "�� ������"','5258086114','1095258004797','(8312) 432-29-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ic-investline.ru', '04E4D6EE40AE5CA61F011E222C4A49075DBD3092', 
 '2012-02-22 11:52:22.000',True,False,'������� ������� ������������','��� "�� "����������"','7721724147','1117746349632','(495)915-04-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@tdfi.ru', '6FF25C867B2F367DD05C4EA315CAF62F8A96FB96', 
 '2012-02-22 11:54:21.000',True,False,'��������� ����� �������','��� "�������� ��� �������� � ����������"','7723721790','1097746401323','500-84-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@activ-projekt.ru', 'C56E7002A5B041C4954C5ED1548DCC315D16DFBA', 
 '2012-02-22 11:55:57.000',True,False,'������� �������� ����������','��� "��� "�����-������"','7708734153','1117746113990','(495) 502-38-71', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Lbov-av@mail.ru', '614008CA01F439FE761A0153092FF083F552B173', 
 '2012-02-22 12:03:15.000',True,False,'���� ������ ���������','��� "���-������"','5904258330','1115904017492','89504728662', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'viktoria@rdc.spb.ru', 'D0AFC698ADAE8EFD529DDA9DE157D5C729754161', 
 '2012-02-22 12:06:49.000',True,False,'��������� �������� ����������','�������� ����������� �������� "�����-������������� ��������-������������ �����"','7812001441','1027810242074','(812)324-39-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Sbatyuk@academcapital.com', '22643207D2FFD236AA742F4FD07052633A8F521E', 
 '2012-02-22 12:12:58.000',True,False,'����� �������� ����������','��� �� "�������������"','5404165046','1025401484591','(383) 203-41-06', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yaroshevich@allianzrosno.ru', 'F3CC187311E54B54EF9DEB11CB52945AAF92D10C', 
 '2012-02-22 13:10:28.000',True,False,'������� ������� �������','��� "������������� �������������� ��������" ','7709324907','1027700421605','495-231-31-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'akazmina@reestrrn.ru', '6EAD568DAE66B1F2E0E01ACDEE4920F0A5FAA3B1', 
 '2012-02-22 14:15:26.000',True,False,'�������� ���� ����������','��� "������-��"','7705397301','1027700172818','(495) 411-79-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'u.a.stepanov@chelindbank.ru', '8D9A5BEBCE3994E85F1F1D519F1D16D8BC54C57A', 
 '2012-02-22 14:18:55.000',True,False,'�������� ���� �������������','��� "����������"','7453002182','1027400000110','(351) 239-83-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ic-platform.ru', '88E2A14DD0C3AE3FD45E7AEDB16DD216A63C311A', 
 '2012-02-22 14:22:08.000',True,False,'������� ������� �������������','��� �� "���������" ','7705485910','1037705006052','+7 (495) 6266779', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'back@kapital-m.ru', 'F9177F913C207A866A0A5712569A09EA387FAA1C', 
 '2012-02-22 15:42:44.000',True,False,'�������� �.�.','��� "������� ����������"','1657054445','1051629038405','88435257455', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'irina@ankorinvest.ru', '6AD8B58D63065AB484B13CCB07674056800A327A', 
 '2012-02-22 15:47:44.000',True,False,'�������� ����� ���������','��� "����� ������"','7717662120','1097746688050','(495) 980-88-44', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alexandra.popovina@moscowpartners.com', 'A67FDA56B996832ECCF3C8F9F5EE1E6BCDF58C41', 
 '2012-02-22 15:50:38.000',True,False,'�������� ���������� �������������','�������� � ������������ ���������������� "���������� ��������"','7729605134','1087746796060','74957875256', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tov@advance.ru', '558A566F15D47B16AE3E942AE8E8E11827C73205', 
 '2012-02-22 16:10:38.000',True,False,'���������� ������ ������������','�������� ����������� �������� "�������������� �������� "������"','7803002223','1027809203630','(812)325-43-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'svetal@spvb.ru', 'DA94257A3502A34C63A2601C318AE4615EEEBF28', 
 '2012-02-22 16:13:03.000',True,False,'�������� �������� ����������','��� ���','7841364200','1077847456609',' (812) 324-38-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'finmeridian@yandex.ru', '5CC8774561AF8B0D6349E3C6403A5E7DA6BBAC36', 
 '2012-02-22 16:25:48.000',True,False,'��������� ����� ����������','��� "�� "��������"','7725700002','1107746609156','495-967-17-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sedova@ikrost.ru', '7577687488F4728D0AEA2113F49440483D59DD3D', 
 '2012-02-22 17:20:15.000',True,False,'������ ������� ��������','��� "�������������� �������� "����"','7839389831','1089847341980','(812) 400-05-05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dmitriy@west-capital.ru', 'FE3B88666550C52DE5FC970437A9E32B135B8D8D', 
 '2012-02-22 17:24:20.000',True,False,'���������� ������� ����������','��� "���� � �������"','7706666349','1077758800602','(495) 663-02-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'back@ifg-almaz.ru', '2DA7BE2A04BFEBA9F170AF61E16EAC2918676E60', 
 '2012-02-22 17:32:47.000',True,False,'������� ������� ������������','��� "��� "�����"','7705941698','1117746113979','(495) 212-23-41', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'consulting@tezor.ru', 'D56A155EFFC60C21D5E9D68E288C217890731048', 
 '2012-02-22 18:08:36.000',True,False,'������ ���� ��������������','��� "�������������� �������"','7709783330','1087746338130','4955068625', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@lloydstsbltd.ru', '3E3186B257BEAA2C7D2F5E57FC39314A53F4157B', 
 '2012-02-22 18:32:03.000',True,False,'������ ������� �����������','��� "������ ���"','7722557731','1057748453883','4955177952', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'okarasev@third-rome.com', '7BB9F454871D3E4D216763DBFF4618E4524E6D40', 
 '2012-02-22 18:41:34.000',True,False,'������� ���� ����������','��� "������ ���"','7743744813','1097746318548','+7(495)7559779', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bar73@mail.ru', 'DCA87A49981A519D07D7C4E986EEAF435E4A6D74', 
 '2012-02-22 19:34:52.000',True,False,'���������� ����� �������������','���  �� "������� � ������"','7725240549','1037725052694','79600702859', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'o.gromykina@ukprivilegiya.ru', '82916A3E9546B044A88C39129508460E54753896', 
 '2012-02-24 08:24:19.000',True,False,'��������� ����� ����������','��� "����������� �������� "����������"','7708724934','1107746604910','(499)390-17-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nosov@backson.com', '9B7843EDC21C71147F9457A6A305CC3F0A4379B8', 
 '2012-02-24 08:26:53.000',True,False,'����� ������� ����������','�������� ����������� �������� ���������� �������� "������-������"','5407166390','1025403197346','(383)-231-1655', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@progressinvest.ru', '3D88D18AE4252A221E2720080D6A1835ECB2DED7', 
 '2012-02-24 09:27:16.000',True,False,'��������� �������� ����������','��� �� "������������� �������������� ����"','7723594775','1067760723546','+7(495) 645-3725', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vvpishkina@severstal.com', '2BF16602B43D2E816E78585A3156246F227B4E9D', 
 '2012-02-24 09:41:14.000',True,False,'������� ���� ����������','��� "����������� "��������"','3528139976','1083528007771','(8202) 53-31-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'muhaeva@gazneftbank.ru', 'AA1F0EE611998FB9C40769EC314C7C8B57CAA795', 
 '2012-02-24 09:55:57.000',True,False,'������� ������� ������������','�������� ����������� �������� "��������-�������������� �������� "������-����"','6452045978','1036405200457','(8452)277534', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'cb@ncubank.ru', 'AE9F7317EB88E312447C2C03C4CB0E64A19AF353', 
 '2012-02-24 10:00:42.000',True,False,'������������ ���� ����������','��� "����� ��������� ����" (���)','7711070030','1027739515781','(495) 933-54-91', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'partner-info@aaanet.ru', '09B874B10AFE8AD62DF561E0A69ACEEB6ED0BDBF', 
 '2012-02-24 10:01:24.000',True,False,'�������� ����� ���������','�������� � ������������ ���������������� "�������"','6164096630','1026103267277','(863) 267-92-94', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uc@sibtelecom.net', 'CBCD6CF251E2DFD1921A726EA2B3E5F092FA3309', 
 '2012-02-24 10:02:37.000',True,False,'������ ������� �����������','�������� ����������� �������� "��������� �������������������� ��������"','8601044127','1118601001133','78003331234', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'degtev_av@mx.kubankredit.ru', '9C8EABC3DA20BA417B2465D10FE86472332E4175', 
 '2012-02-24 10:42:51.000',True,False,'������ ������� ������������','�� "������ ������" ���','2312016641','1022300003703','78612749288', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'andronik@cfgr.ru', '59E492B34F9CBB1104DA917582B58A50C1170BD5', 
 '2012-02-24 10:49:38.000',True,False,'������� ����� ��������','��� "��������� ���������� ������"','7719566366','1057748376531','8-495-504-40-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Yury.Danko@fbank.ru', 'DCFFFF356B8BF2BDB4AA9302D01B1D5F4710EFC2', 
 '2012-02-24 10:57:02.000',True,False,'������ ���� ����������','��� "������������ �����"','7710152071','1027739149855','(495)980-19-26', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@sdkgranit.ru', '3F6BE9E327303BCDCD5ABB4674DF3759E3E7EFDA', 
 '2012-02-24 11:19:14.000',True,False,'������ ������� �������','��� "��� "������"','7703747769','1117746550668','(495) 221-31-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'litosh@cfillc.ru', 'F4B97172C12FA51907703B33CBB7FC3BB4EB814C', 
 '2012-02-24 11:32:48.000',True,False,'����� �������� ����������','�������� � ������������ ���������������� "����� ���������� ����������"','7708680162','5087746197336','7397297', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dmytenkov@partneraeroflota.ru', '85E40E97177FB62A4B768A87DBD3959270B6ABA4', 
 '2012-02-24 11:42:47.000',True,False,'�������� ������� ����������','��� "������� ���������"','7710278500','1027700190396','(495)363-09-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'merkushova@zapad.ru', 'C4B99BCBE9BF353FDCB4A184083C256E458AA2AE', 
 '2012-02-24 11:43:01.000',True,False,'��������� ���� ����������','��� ���� ��������','7750005637','1117711000010','+7 (495) 620-94-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ork@ork-reestr.ru', 'F1938FD0FB5AD76366D86C4AB9B5DC4C4BDCF9DE', 
 '2012-02-24 11:45:16.000',True,False,'���������� ������� �������������','�������� ����������� �������� "������������ ��������������� ��������"','7705108630','1027700036540','(495) 775-1820 (125)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'SapozhkovaEV@uralsib.ru', 'DC6D24B4F8B5981D2DB7ACEDACC334F4823F831E', 
 '2012-02-24 12:08:31.000',True,False,'��������� ����� ����������','��� �� "�������"','7732006444','1027700025682','(495) 705-92-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mpgsb@reg.avtlg.ru', 'FF546E39CCB4986E3778E8F8ED36E19E5E7D0E0B', 
 '2012-02-24 12:09:02.000',True,False,'������������ ������ ��������','�������� ����������� �������� ������������ ���� "������������ ����������������"','3437008951','1023400000535','(84463) 2-74-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@usdep.ru', '45585FD48F3F87D8D780B33669512072DBE7E99E', 
 '2012-02-24 12:16:21.000',True,False,'������ ����� �������������','��� "���"','7723811155','1117746703579','+7 (495) 665-34-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'karelsky@citytrast.ru', '1D97D185D08C613F41F79ECD5248F31B1084A6C8', 
 '2012-02-24 12:18:03.000',True,False,'���������� �.�.','��� "��"���������"','7705770650','1067761322991','(495) 720-66-73', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rdo@newreg.ru', 'AC5129380B3CD5998687F2A5D3D035E027EF4A6B', 
 '2012-02-24 12:34:38.000',True,False,'������� ���������� ����������','��� "��������-������������ �����������"','5407163110','1025402475174','383-335-73-39', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'boffice@line-invest.ru', '9A09C7938D1C73D47F8532F3A0B18CB45962BE31', 
 '2012-02-24 13:08:20.000',True,False,'������� ����� ��������','��� "��"����� ����������"','7718581509','1067746469560','(495) 645-00-85', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@tala.su', 'B1D12102A35D10995DF69FE7755F350043518CF7', 
 '2012-02-24 13:31:20.000',True,False,'����� ������ ���������','��� "�� "����"','7719569705','1057748738101','+7(495) 698-36-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'loschakova@kamas.ru', '4A930A58AA667363C05AE39B9F4C87502CBD9F91', 
 '2012-02-24 13:48:15.000',True,False,'�������� ����� ���������� ','��� ��� "����������"','1650029778','1021601371384','(8552)35-80-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ticap.ru', 'BF4BAB2532C24B6791A05C5BB7D39C4A52C270A1', 
 '2012-02-24 13:49:00.000',True,False,'�������� ������ ����������','OOO "������������������"','7715779275','1097746648020','495 517-62-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'SITILAND@mail.ru', '5DC8723A87740EA9914EFCC08E29F4C817BB1023', 
 '2012-02-24 14:13:06.000',True,False,'������� ��������� ����������','��� "�� "����-����"','7718728744','5087746335573','8-495-642-52-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'control.remmaks@hotmail.com', 'F575DA2F7E1BC874807D787D5AA718B5CD7FF9D0', 
 '2012-02-24 14:19:28.000',True,False,'������ ����� ���������','��� "�������-��"','7708718465','1107746298758','89200195221', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dogovor@finstr.ru', 'EB1E8498A11AF38D4325240A9CB800C619A78FD3', 
 '2012-02-24 15:06:03.000',True,False,'������� ���� ������������','�������� � ������������ ���������������� �������������� �������� "���������� ���������"','7707596380','1067746918502','8 495 984 51 60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stock@euroalliance.ru', '03076E19C2E8D973D09A4A737CF203BDD5D2164F', 
 '2012-02-24 15:09:19.000',True,False,'������ ��������� ����������','euroalliance','3702030072','1023700001467','(4932) 59-09-29', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pogudo@ffin.ru', '60EDF2DD8CB2B575934292F436A330DE746FF846', 
 '2012-02-24 15:23:46.000',True,False,'������ �.�.','��� �� "������ ������"','7705934210','1107746963785','79057795411', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'SBISBI@mail.ru', '790EA6E968D74C812DDE668542A0924269E85C71', 
 '2012-02-24 16:13:01.000',True,False,'������ �.�.','��� "���"','7708681335','5087746297601','(495) 642-55-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ikstart@mail.ru', '1800CE7DBB7AC036E5566616C3A3241733C4000A', 
 '2012-02-24 16:16:49.000',True,False,'������� ����� ���������','��� ��� "�����"','7714783529','1097746375308','(495) 642-49-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'veksel@element-capital.ru', '05A012BEF75129377DEF8896B4A9E9B5C1EC7804', 
 '2012-02-24 16:18:27.000',True,False,'������ ������ ����������','�������� � ������������ ���������������� "�������"','7725707706','1107746912558','9 495 640 40 58', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'diamand_diamand@mail.ru', 'DECE056C3BA49B9A0CC9AF535106835078A527C8', 
 '2012-02-24 16:21:04.000',True,False,'������� �.�.','��� �� "�������"','7723708849','1097746126653','(495) 518-73-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rozenbah@1cb.ru', 'BFF49116790740F364FF2EEB9E4A8527C1D9DAB6', 
 '2012-02-24 16:22:01.000',True,False,'����� ��������','��� "������ ���������� ����"','7744003039','1037711002339','+7(495) 276-06-16 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'globalfinans@mail.ru', '402E282E87A1442CE38D747B6E2FB8413093F394', 
 '2012-02-24 16:23:26.000',True,False,'������� �������� ������������','��� �� "������������"','7727687036','1097746161875','(495) 585-67-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'astracap@mail.ru', '5E050789D29103B488C575AF01C30F766BEB4FAE', 
 '2012-02-24 16:25:28.000',True,False,'������� �������� �������������','��� �� "������������"','7718754744','1097746101232','(495) 505-43-24  ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'eadix@mail.ru', 'ED08C2096C2D84DB4666B58439575EC0E231AC64', 
 '2012-02-24 16:31:48.000',True,False,'���� ���� ','�������� ����������� �������� "�����������" ','7706145709','1027739600591','(495) 518-73-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk-diana@bk.ru', 'F89B7A08C68C9A225C7ED213A9C16D9839D2AC29', 
 '2012-02-24 17:39:49.000',True,False,'��������� �������� ����������','��� "�� "�����"','7719280945','1037719046628','+7 495 981-93-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fomichev@icbe.ru', 'B2EDCDA01ECC6E7DB830BB21B02B848195035036', 
 '2012-02-24 18:00:12.000',True,False,'������� ��������� ������������','�������� ����������� �������� �������������-������������ ���� "�����������"','3905041369','1023900000761','(4012)57-39-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info.skyinvest@gmail.com', 'E836848919557B0DB4C92FA94973BF5D9DF5C12B', 
 '2012-02-24 18:46:20.000',True,False,'������ ��������','��� "����������"','7710861712','1107746038872','74956461182', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@siver.irk.ru', '7B6A41F9B2E7A140EAB5C98A47B30DBA587776CB', 
 '2012-02-27 04:41:34.000',True,False,'���������� ���������� ���������','�������� ����������� �������� ���������� �������� "�����" ','3811009560','1023801011541','8 (3952) 70-61-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'department@tmapb.ru', '771B7690831AD731ADF0626E090431B89776958A', 
 '2012-02-27 07:12:22.000',True,False,'����������� ����� �������������','��� "������������������"','7202026861','1027200000080','(3452) 413506', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fp@fp63.ru', 'C7588E881D7E5D1F856008B1FD63B3950DDB55F9', 
 '2012-02-27 09:11:52.000',True,False,'������� ������ �������������','�������� � ������������ ���������������� "���������� �������"','6311091629','1066311053049','(846)241-63-80', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'neftegazinvest@rambler.ru', 'B43D114EC5FB5B4FCB15E1A61883C4FDFA6CCB99', 
 '2012-02-27 09:40:58.000',True,False,'�������� �����','��� �� "��������������"','278044441','1020203219772','(347) 272-22-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'u.s.arutunova@krd.akbk.ru', '7C0353E315680B43ED95EED606DCFD65A9372020', 
 '2012-02-27 10:09:18.000',True,False,'��������� ���� ���������','����������� ������������ ���� "����������" (�������� ����������� ��������)','2338002040','1022300002670','(861) 254-24-05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'finstatus2011@yandex.ru', '5735A68FB37CD6989FBC390DB0D746F513C888D4', 
 '2012-02-27 10:21:58.000',True,False,'���������� �.�.','��� "�� "������ ������"','7725716154','1117746118423','(347) 246-39-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@centr-invest.ru', '0539F3C3145C24E76726223F114F5B6880746F40', 
 '2012-02-27 10:43:14.000',True,False,'�������� �.�.','��� �� "�����-������"','7726050935','1026901729205','(495) 544-03-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'filonova@vcb.ru', '79AD3696BED0C3D751892CB3501C8E50ACF41E8F', 
 '2012-02-27 10:55:47.000',True,False,'�������� ���� �������������','�������� ����������� �������� ������������ "�����-������" ����','6310000192','1026300001815','8(846)372-06-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ushakova@kbmkb.ru', '23E46AFAB7C0F48A81F67517C5DF5200B831ECF7', 
 '2012-02-27 11:22:16.000',True,False,'������� ������� ���������','������������ ���� ��������������� ����������� ���� (�������� � ������������ ����������������)','7706015210','1027739076144','(495) 258-89-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'brith@bk.ru', '7ADFB04BCA46156039762AD830844121A485ECDC', 
 '2012-02-27 11:40:38.000',True,False,'�.�.����������','��� "��"����"','7701847045','1097746486287','89250069864', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fo@aaanet.ru', '9A3A3AB8D9D3F1CFBEBD590D32E2EC6A3F339999', 
 '2012-02-27 12:14:03.000',True,False,'������ ����� ����������','��� ���� "�����������"','6164026390','1026103273382','(863) 267-90-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Sheronova@mkb.ru', '5FD681B8A39BE261BF4BBD3DB8E85F9C8CE7855F', 
 '2012-02-27 12:22:46.000',True,False,'�������� ���� ����������','��� "���������� ��������� ����" ','7734202860','1027739555282','+7 (495) 797-42-22-6093 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oksana.parasyuk@paritet.ru', 'C2BB9C52A93E32145753B7C9C4715B256C95F1D8', 
 '2012-02-27 12:38:39.000',True,False,'������� ������ ����������','�������� ����������� �������� "��� �������"','7723103642','1027700534806','74959947275', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'director@mos-finance.com', 'DB175F15510140C02CBDAE5FFE517A1ACA17B3D4', 
 '2012-02-27 12:44:10.000',True,False,'������������� ����� ������������','��� �� "���������"','7715750702','1097746125311','(985)643-34-19', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@ikarkada.ru', 'DA743A14E37CEB5B2C949E60C2350DA2D57970E9', 
 '2012-02-27 13:34:57.000',True,False,'�������� �.�.','��� "�������������� �������� "������"','7708704092','1097746500202','925-200-15-04', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kdn@ifc-profit.ru', '1F3309DCBB268886A654A3ED61EC693E7A58B632', 
 '2012-02-27 13:35:23.000',True,False,'����� ������� ������������','��� "��� "������"','5029075368','1045005504499','(342) 210-37-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@westerngate.msk.ru', '37204086C8565FA8BDCD1FA3746A870522A5E2D3', 
 '2012-02-27 13:47:58.000',True,False,'�������� ������ ����������','�������� � ������������ ���������������� "���������� �������� "������� ����"','7743719101','5087746612355','925-833-58-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@ikfinterra.ru', '8109A03C6219E43FC7F534643F14BD2FBE9F77BE', 
 '2012-02-27 13:49:59.000',True,False,'������� ������� ��������','��� "�� "��������"','7743698892','1087746826199','(926) 466-74-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ado_kamaganova@devon-mc.com', 'CE9768E8D3C07718CA18CDF3F88E299251A2BE73', 
 '2012-02-27 14:02:36.000',True,False,'���������� ������ ��������','�������� � ������������ ���������������� "����� ���������� �������"','7702751120','1117746020314','(495) 228 07 51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alex@grandcap.net', '4B6E6D684DE0C1F3E3157968F63E702505E15152', 
 '2012-02-27 14:04:49.000',True,False,'������ ��������� ���������','��� "�� ����� �������"','7715781718','1097746694275','(499) 246-80-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'risk@riskinvest.ru', 'D1B8E591BCECACB0377CEBB6AD4B41AE00F893F1', 
 '2012-02-27 14:22:59.000',True,False,'������� ���� ���������','��� "����-������"','7714097336','1027739137568','495-229-32-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mfo.garantiya@mail.ru', '5F6B7C138DEC6AF9FD2BE57752FEEFD56C243B5C', 
 '2012-02-27 14:38:40.000',True,False,'�������� ������� ����������','��� ��� ��������','3528183196','1113528012564','8-911-505-46-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'latishka-uv@rambler.ru', 'AF5D7121F0A89204A56EA0EBBBF2290896BA6834', 
 '2012-02-27 14:47:28.000',True,False,'������ ���� ������������','��� "�������"','3528179986','1113528007141','89210889368', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'barinova@invest.energogarant.ru', '33BD6A82A425C5F65BB879FBC4B580E4C811797D', 
 '2012-02-27 14:56:15.000',True,False,'���������� �����','��� �� "������������-������ ���"','7705513188','1047705048566','(916) 067-8771', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'schepnova@mcbank.ru', '84DCF2CBF9DA3106191D49898754D5DE0C710E81', 
 '2012-02-27 15:11:51.000',True,False,'������� �.�.','��� ���� "������-����"','7703033450','1027739045124','981-85-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@centcom.su', '3E3956A7D8B1C16BD42D295ED4B8BF25925A69AD', 
 '2012-02-27 15:16:17.000',True,False,'������ ������ �������������','��� "���"','7708703684','1097746476222','+7(495)623-67-09', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'neboginav@gmail.com', 'B8D0EE8FA9A5F5DB3D58815C17933B7AFD9E0E9D', 
 '2012-02-27 15:20:06.000',True,False,'������� ������ ������������','��� �� "Z-�������"','6315566288','1036300461801','89023712658', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ifc-liga.ru', '723D0BB1624BA6A8F77A8ECF895CD077ABEB8D0D', 
 '2012-02-27 15:36:39.000',True,False,'������� ����� ������������','��� "��� "����"','7707748805','1117746339864','(495) 989 85 02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@akropol-finans.ru', 'C309937101B0BAD0539B6FC6CAA8CEE58D1A7591', 
 '2012-02-27 15:42:32.000',True,False,'������ ����� �������','��� "�������� ������"','7701873630','1107746306029','(495) 517-72-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'RekaLV@belsocbank.ru', '70B1BD0EFD40367E51B8F769E8BA2A31FA0730C1', 
 '2012-02-27 15:56:09.000',True,False,'������� ��������','��� ��� ���������������','3123004233','1023100000560','84722336328', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ik-finko.ru', '512967061E9F598B34566B2D3EA3C37BEA297326', 
 '2012-02-27 15:59:31.000',True,False,'������� �������� ��������������','��� "�� "�����"','7716686520','1117746264668','(495) 988 33 53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@finansresheniya.ru', '4F851222C98DD6BB46C18453072B39BC4CF261B2', 
 '2012-02-27 16:00:36.000',True,False,'�������� ������ ����������','��� "���������� �������"','7727666357','5087746309976','(495) 502-37-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@fk-kapital.ru', '1A60C0DBEDFA66929905A500ADB5E95042B06762', 
 '2012-02-27 16:06:25.000',True,False,'�������� ������ ����������','��� "�� "�������"','7722679899','1097746082378','+7(495)627-74-91', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ig-active.ru', 'BE10759918BFB40914D6468B37F1F71AFC49FD78', 
 '2012-02-27 16:07:33.000',True,False,'������� ��������� ������������','��� "�� "����� ������������"','7718807354','1107746412773','(495) 508-70-96', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ocb@rtbank.ru', '5676871A8A7C9E90D765D79A36A2768F70DD1066', 
 '2012-02-27 16:17:05.000',True,False,'���������� ���� �������','��� ��� "������������"','5261005926','1025200001001','(831) 465 80 99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ifkkurz@kursktelecom.ru', '0ECBEC1F4C642B6716B832C95CFF8BD1BA46692B', 
 '2012-02-27 16:27:36.000',True,False,'�������� ������� ������������','�������� ����������� �������� "�����-������"','4629003966','1024600938780','(4712)514619', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depcentr@bk.ru', '5AB9747137E7AFE259E63692910C6764AB917290', 
 '2012-02-27 16:33:16.000',True,False,'��������� ��������� �����������','��� "������������ �����"','7719643652','1077758587576','(495)9976932', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.lyubeznov@ofin.ru', 'ECA80ED59061A06D41DC948464FEFE4FD4D26F1E', 
 '2012-02-27 16:45:38.000',True,False,'�������� ������ ������������','��� "������������ �������"','7725069041','1027739061404','(495) 954-01-17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk@troikaventure.ru', '1E97A4BB736D4CE4973F39B2C12909A4EAA1A672', 
 '2012-02-27 16:58:48.000',True,False,'������� ������ ����������','�������� ����������� �������� "����������� �������� "������ ������ �������"','7704654394','1077757721480','(495) 663-77-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'soboleva@inv-city.ru', 'CCF463EA36C7497B1575B4B82D66DBD535F2C090', 
 '2012-02-27 17:00:59.000',True,False,'�������� �����','��� "�� ������ ����"','7718817200','1107746672593','(495) 984-55-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'phaetonfin@mail.ru', 'E523297EBFD1F0E6840C4F0E3D7BB4F93C694289', 
 '2012-02-27 17:51:24.000',True,False,'������ ������ ���������','��� "�� "������ ������"','7702753671','1117746117048','89687244925', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'paratov@uk-nit.ru', '8E9C086C906AE605232E0FA77519B7C349C8B7C3', 
 '2012-02-27 17:56:44.000',True,False,'������� �.�.','��� "�� "���"','7735520922','1067746722977','(495)739-65-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@aktiv-am.ru', 'CD683C9A5382F351793956C7C108BB2E5B01848D', 
 '2012-02-27 19:50:01.000',True,False,'������� �.�.','��� �� "����� ����� ����������"','7718783921','1097746710973','84955048217', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '5048637@gmail.com', '73FCF64BF9BDEB6A55F3B79610F0FB24977215E5', 
 '2012-02-27 20:23:39.000',True,False,'������� �. �.','��� "��-������"','7725577535','1067746913695','84955048637', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bfk@kmscom.ru', 'A16A8CA585A8B58C4EB1C2FB0F988BFBD03328FD', 
 '2012-02-28 06:10:22.000',True,False,'������ ������ ��������','�������� ����������� �������� "���"','2726007592','1022700514748','79141775607', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gorizont@koks.metholding.ru', 'F9978031E96002D3B5681A7B20E934DFF229451B', 
 '2012-02-28 06:32:19.000',True,False,'����� ������� ����������','��� "��� "��������"','4207023795','1024200678226','+7(3842) 39-61-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rustfin@mail.ru', 'BDA2E07B6028587D49325F1E0DF3603CF3A6EF1E', 
 '2012-02-28 06:47:40.000',True,False,'��������� �������� ����������','��� "�������"','5260321118','1125260000854','79201113703', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'andrey.astahov@gmail.com', '3B5FB1CFF0A58277B980E0B557478B6A6DDC70C8', 
 '2012-02-28 08:57:12.000',True,False,'������� ������ ','��� "�������-������"','7715725061','5087746398482','(495) 212-23-41', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'serg_b1@list.ru', 'CCCE328B53C210168F271BDFB146AC68A958476A', 
 '2012-02-28 09:45:35.000',True,False,'������ ������ ������������','��� �� "���������"','7715804250','1107746257101','8-919-766-76-29', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'serg_b2@list.ru', 'CCCE328B53C210168F271BDFB146AC68A958476A', 
 '2012-02-28 09:47:32.000',True,False,'������ ������ ������������','��� "����� ������"','7733673955','5087746335551','8-919-766-76-29', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@tm-capital.ru', '1F167185BE637CD387136959BED8C0266AD72F3E', 
 '2012-02-28 09:48:45.000',True,False,'������ ����� ���������','��� "�� "��-�������"','7202146823','1067203154424','8(3452)696434', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fos@partner-reestr.ru', '1077EA32B93E218322C2B82652231C8645CBE936', 
 '2012-02-28 09:56:43.000',True,False,'��������� �.�.','��� "����ͨ�"','3528013130','1023501236098','(8202) 53-60-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vvch@mnm.ru', '729E819F9F32FEF7E68483370AEA11E9DE3FCF8E', 
 '2012-02-28 10:24:50.000',True,False,'����� ��������','��� "�� "��������� ����������"','6658078543','1026602314958','3432102698', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '108@gazfin.ru', 'E8341BB2F0D1E29289A9A6A06E67186F4BFA0604', 
 '2012-02-28 10:51:33.000',True,False,'�������� ����� ����������','�������� � ������������ ���������������� "������� ����� ����������"','7705934845','1107746983662','79257443102', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@quatro-finance.ru', 'FC942776221AECE756E994F550EE07D8B328BB37', 
 '2012-02-28 11:33:36.000',True,False,'��������� �.�.','��� "������ ������"','7706618112','1067746539607','(846) 275-67-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@maklerinvest.ru', '67DADC01EFD1C98125E598F08794E96B9DC998C0', 
 '2012-02-28 12:07:49.000',True,False,'���������� ����� �������������','��� "������"','7705934115','1107746960530','8(495)792-54-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'goradzanov@mail.ru', 'C004082BABE77CE1A48F9E0462AC00A5006AF6F5', 
 '2012-02-28 12:17:26.000',True,False,'���������� �.�.','��� "�� ������"','7723615714','5077746872957','+7 495 411 53 11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@broker-capital.ru', 'A21A306F0471E587D9B9F3A731F3F2D680DB95E5', 
 '2012-02-28 12:34:24.000',True,False,'������ ������ ������������','��� "�������������� ��� "������-�������"','7704657099','1077758554675','(495)648-93-85', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'MatveevaA@horizon.ru', 'D96DE6D0E3D59F24626C44C7263D11906C9B5BBA', 
 '2012-02-28 12:55:40.000',True,False,'�������� ����','��� ��������-������','7702644658','5077746938649','8(499)793-42-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'trustcapital@trust.ru', 'D5AAD46A58C14D0FB9EA900C4321D4E4D85864B3', 
 '2012-02-28 13:11:13.000',True,False,'������� ������� ����������','�������� � ������������ ���������������� "����������� �������� "������� �������"','7709391942','1027709021757','(495) 789-98-38', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pnh-buh@mail.ru', '2FCE5E070AC3D8E62208A253E8C32285C94B03EA', 
 '2012-02-28 13:44:24.000',True,False,'�����������','����','2308010140','1022301191714','2-28-30-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '130@gazfin.ru', 'E8341BB2F0D1E29289A9A6A06E67186F4BFA0604', 
 '2012-02-28 13:57:03.000',True,False,'�������� ����� ����������','�������� ����������� �������� "��� ������ �������������� �����"','7719561939','1057747709920','74956601436', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '123@gazfin.ru', 'E8341BB2F0D1E29289A9A6A06E67186F4BFA0604', 
 '2012-02-28 13:59:47.000',True,False,'�������� ����� ����������','�������� ����������� �������� "����������� �������� ��� �������"','7728595035','5067746759812','74956601436', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'otchet@ukmrf.ru', '7F9904EA459C5E7503F905D7B11E11F8C8842E59', 
 '2012-02-28 14:20:51.000',True,False,'��������� �.�.','���  �� "���"','7705641430','1057746091699','(495)642-0023', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sergey@capdev.ru', '35F49907EE4A236F4174E7787027537B6E57C852', 
 '2012-02-28 14:49:49.000',True,False,'������� ������ �������������','�������� ����������� �������� �������������� �������� "�������� ��������"','7709804413','5087746110964','(495)9815883', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ric-invest.ru', '1DBC3017EFE1F5F8AF5B5F4DA39DEED655FB6673', 
 '2012-02-28 14:58:06.000',True,False,'��������� ������','��� "��������������� �������������� ��������"','7702730025','1107746305457','(495) 626-82-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'san@moscow-bank.ru', '3D2F0137B4A0F0BDEE95BAD045EB341EE54C607A', 
 '2012-02-28 15:18:13.000',True,False,'������� ��������','��� ���� "������"','7714038860','1027739126865','74957779797', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@zemsky.ru', '05D0D7448BC3A33109DD496273B80D9303FAC5B5', 
 '2012-02-28 15:20:39.000',True,False,'���� ���� ����������','��� ��� "������� ����"','6316030651','1026300001826','(8464)98-54-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'velizhanin@astralnalog.ru', '2574D19D4825492CBFA1DB732CD6E5D0FD52126B', 
 '2012-02-28 15:35:30.000',True,False,'��������� ������ �������������','�������� ����������� �������� "������ ������"','4029017981','1024001434049','4842788999', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zsd@rostcp.ru', '354DFA13AF37376F7B07B64F864328CBB29B730C', 
 '2012-02-28 16:19:33.000',True,False,'������� �.�.','�������� ����������� �������� "�.�.�.�. ������������� �������"','5503039596','1025500736238','(495)9809045', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@prime-mark.com', 'C11B746BB7EEC48A143E5C4AA8FD44536485AE31', 
 '2012-02-28 17:35:59.000',True,False,'�������� ������ ��������������','��� "����� ���� ���������� ��������"','7707698640','1097746098911','+7(495) 9897752', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'OSuschinskaya@rencap.com', 'F3CC187311E54B54EF9DEB11CB52945AAF92D10C', 
 '2012-02-28 20:07:51.000',True,False,'C�������� �����','�������� � ������������ ���������������� "����������� �������� "��������� ������������"','7703721922','1107746441330','(495) 258-77-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vsi@acmetelecom.ru', 'A053C9EDB62246DFF20F031497B2F56F46ABEF2D', 
 '2012-02-29 10:25:22.000',True,False,'������� ������� �����������','��� "�������������� �������� "��������"','4501016440','1024500509626','(3522) 41-87-56', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tib@tib.ru', '5902B21121170C97FF86C71D96E561B528BF9F01', 
 '2012-02-29 11:04:34.000',True,False,'��������� ��������� ����������','��� ��� "�������������"','1653005038','1021600000751','(843) 231-72-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Lypol@yandex.ru', '123A3AA986AF3731A5FEFCBE52A3E6E77D174498', 
 '2012-02-29 12:19:27.000',True,False,'������� ������ �����������','�������� ����������� �������� "���������� ��� "��������"','7716557099','5067746341251','89167564515', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'DMikhailov@rencap.com', 'F3CC187311E54B54EF9DEB11CB52945AAF92D10C', 
 '2012-02-29 12:38:49.000',True,False,'���������� ��������� ��������','�������� � ������������ ���������������� "��������� ���������� ������������"','7710667112','5077746319800','+7 (495) 744-55-66', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '167@nm.ru', '30959632D4798F4205B92802CD9FA0104D199E85', 
 '2012-02-29 12:43:30.000',True,False,'������� ���� �������','���"���������� ��������" �������"','4706020126','1034701334227','(495)2216057', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'galina.izhberdieva@fbank.ru', '9D75DF004DF332DB48F513CE6CEA02513C6AF7B9', 
 '2012-02-29 13:43:16.000',True,False,'���������� �.�.','��� "���� �����"','7712002554','1027700140753','495 980-19-24 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kapital@kapitalinvest.ru', '98EE285B1425E9CACBF29DBDF0F370DA3B8EFABA', 
 '2012-02-29 15:20:37.000',True,False,'������ ������ ����������','��� �� "����������� ��������"','7722569991','1067746314085','84956376626', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'v-f-b@yandex.ru', '123A3AA986AF3731A5FEFCBE52A3E6E77D174498', 
 '2012-02-29 15:42:52.000',True,False,'�������� ������� ����������','�������� ����������� �������� "��� �������� ������"','7716557081','5067746341064','89167564515', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'acla-invest@yandex.ru', '123A3AA986AF3731A5FEFCBE52A3E6E77D174498', 
 '2012-02-29 15:46:18.000',True,False,'�������� �������� ����������','�������� ����������� �������� "�����-������"','7729360484','1027739772422','89167564515', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'chikunov@rea.ru', '4CB664C8168A88C5DB4922B5649F6616BFDF10C1', 
 '2012-02-29 15:48:38.000',True,False,'������� ������ ������������','����� ��� "��� ��. �.�. ���������"','7705043493','1037700012008','89168859717', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'direct-trade@yandex.ru', '123A3AA986AF3731A5FEFCBE52A3E6E77D174498', 
 '2012-02-29 15:53:15.000',True,False,'�������� ������� ����������','�������� ����������� �������� "����������� �������� ���� "�������"','7710333021','1027739533117','89167564515', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'anna.aganina@rdif.ru', '0599699652F57815CCE033B875B88F74EA2D9ECA', 
 '2012-02-29 16:58:05.000',True,False,'����  �������','��� "��  ����"','7708740277','1117746429371','8 (495) 644-34-14 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uni-trayd.ru', '2E8900707DBC642F9B94420D2060AC1962DBC39D', 
 '2012-03-01 11:16:46.000',True,False,'��� ���� ���������','��� "��������"','7728759438','5107746052563','74957620980', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'broker@metallurgbank.ru', 'E646A01AE84907CB2A9DE88288DCB4D75372CC44', 
 '2012-03-01 11:31:42.000',True,False,'�������� ���� ����������','��� �� "���������"','7703010220','1027739246490','785-70-75', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'slava@secret-net.ru', 'A3E4FBB3345DB317795CB28869A2763A8CA0C772', 
 '2012-03-01 12:19:44.000',True,False,'������ ������� �����������','��� "����� �������������� ������������"','2225100262','1092225000779','+ 7 (3852) 200-464', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'marusina-dv@rostatus.ru', '99CA600888A9DBAB9B3911A1F139104AC6CEC8CA', 
 '2012-03-01 13:21:55.000',True,False,'�������� ����� ������������','�������� ����������� �������� "��������������� �������� "������"','7707179242','1027700003924','(495) 974-83-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'enikulina@zarech.ru', 'FC7CAC443216940CBCECEB03A7ED8A9EE8F9DD6D', 
 '2012-03-01 15:51:10.000',True,False,'�������� �.�.','��� "�������" (���)','1653016664','1021600000586','(843) 557-59-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@alpina-tm.ru', '3529AB8F069D4540819FCC6229E83F1F4E8690F2', 
 '2012-03-01 16:16:55.000',True,False,'��������� ������ �������','�������� ����������� ��������  "����������� �������� "�������"','7704772704','5107746055500','8(499) 142-93-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sokolova@unisontrust.ru', 'B63C215938D2BE9F531CB0B7A401595E19E16C60', 
 '2012-03-02 08:40:31.000',True,False,'�������� �����','��� "������ �������"','7203097350','1027200807424','(3452) 419999', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rmk-brok@yandex.ru', '089CC30256C57558F3E3291C5362CE8EB3983B6E', 
 '2012-03-02 13:40:32.000',True,False,'�������� �.�.','��� "���-����"','7814012590','1037832010083','3226491', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bsk-1@narod.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2012-03-02 13:44:33.000',True,False,'���������� ���� ��������','��� "���� ��� -1"','3128033037','1023102362645','8-4725-249373', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Olga.Kalgina@dnbbank.ru', '69D2D79873384C31F88AC902C3F2E02B33647CF3', 
 '2012-03-02 15:49:19.000',True,False,'�������� ����� �����������','��� "��� ����" ','5107040020','1025100001838','(8152) 555-370', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alekseev@tradeportal.ru', '2AFCCFD009F4AF905FABDF4B51F906B58BA121DE', 
 '2012-03-02 17:07:07.000',True,False,'�������� ������� �������','��� "�����-������"','7705776324','1077746285319','74959818488', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@ipb.ru', '6DF15A465E9376B010939D3372FDD1EE8FB14C04', 
 '2012-03-05 09:08:15.000',True,False,'������� ������ �����������','�� "�����������������" (���)','7724096412','1027739065375','4996139706', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Havin.VE@kubank.ru', '9AE3B82137E35859B88B37EEC9DE680A922F0E44', 
 '2012-03-05 10:27:48.000',True,False,'����� ����� ��������','�� "������ �����"','6608001425','1026600001955','8-904-54-49-230', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Depozitarnieuslugi@yandex.ru', '3770A7091907F883B93D97D14E0246900F722175', 
 '2012-03-05 13:40:28.000',True,False,'������� ����� ����������','��� "���������� � ������������ ������"','7708553319','1057746275510','(985) 410-63-79', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'avsoldatova@sberkey.ru', '8112377478C39D50FC1F28F70A3714D95735711C', 
 '2012-03-05 13:58:14.000',True,False,'��������� ����� ������������','��� "��������"','7707752230','1117746480334','+7 926 074 22 79', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dubinina@tubank.ru', '32F85AAB336145B1702CDA5A7B0EAD9F22BD22C6', 
 '2012-03-05 14:07:51.000',True,False,'�������� ����� ����������','����������� ������������ ���� "������������������" (�������� ����������� ��������)','6900000283','1026900001765','(4822) 32-17-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fipk@rambler.ru', '227CFA9FA6FD1E65CFFA606C335FBAB5A207D8C3', 
 '2012-03-05 14:50:47.000',True,False,'��������� ���� ����������','����������� ��������������� ��������������� ��������� ���������� ������� ����������������� ����������� "��������������� ����������� ������������ �������� ���������� ���������"','7701050066','1027739332565','74956998965', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lav@bpsb.ru', 'D809B3D2F4F556C5BFE9C567AAF9BAD4F6AE0EEB', 
 '2012-03-05 15:11:12.000',True,False,'��������� ���� ����������','��� "���������������������"','3123004240','1023100000153','(4722) 322158', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@status-finance.ru', 'ADEC4734BCF76E783040BFD44B9C63474589BF28', 
 '2012-03-05 16:30:30.000',True,False,'��������� �.�.','��� "������ ������"','7710861705','1107746038828','(499)966-75-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.markova@thbank.ru', '159C91E687377FADDF8DE2C871B3368BEB43E64B', 
 '2012-03-06 10:07:05.000',True,False,'������� �.�.','��� "���������������"(���)','6320007246','1026300001881','(8482)71-81-75', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mdf_admzima@mail.ru', 'E66EA1771810FF2F56AF8AAE664AA474FF0ED8AC', 
 '2012-03-06 10:11:36.000',True,False,'��������� �������� ���������','�������������� ����������� "���������������� ���� ��������� ������ � �������� ������������������� ������ ���� � ���������� ������"','3814997510','1113850040963','89041318776', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rifc@co.ru', '7558E5C394BCEAF297084C585E9BADCBC177D728', 
 '2012-03-06 11:11:11.000',True,False,'�������� �.�.','��� "��������� �������������-���������� ��������"','7708102073','1027739701351','5648700', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@rumosinvest.ru', '99763173B255AE7465620F262EC843F2782D6D32', 
 '2012-03-06 12:53:17.000',True,False,'������� ������� �������������','��� ���������� �������� "�����������"','7715762602','1097746352670','(495)969-26-04', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '9137759@crosna.ru', '6628896A25E9DE9C4B6690EC1A7B492F8FE42F8C', 
 '2012-03-06 13:08:55.000',True,False,'������� ����� ����������','��� "������-����"(���)','7703002999','1027739175859','74959137759', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ryazanova@ik-edinstvo.ru', '8C11710D372797D1825CF7C78712F0A2D2ABFE5F', 
 '2012-03-06 14:46:50.000',True,False,'�������� ��������� ����������','��� "�������������� �������� "��������"','6311125959','1116311006756','(846) 373-8101', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivan.02.88@mail.ru', '1D8D8BC02670666313C3DE1F4ED8C3224C1AF407', 
 '2012-03-07 13:25:16.000',True,False,'������� ���� ������������','��� "����"','4006003118','1054002021765','79273503504', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ik.tbi@mail.ru', '267E44B66FCD6123924C5158964B84A8CB31C059', 
 '2012-03-11 10:12:45.000',True,False,'���� ������� ����������','��� �� "����� ��� ������"','7710750459','1097746279234','(495) 628-97-21 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uk-severyanka.ru', '156E6ADF94E81E2122E8197B1484BF1C2C5DD885', 
 '2012-03-11 11:03:11.000',True,False,'��������� ����� �������������','�������� � ������������ ���������������� "����������� �������� "���������"','7713502214','1037739809337','(495) 280-76-79', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'backoffice@banksaratov.ru', 'D41359742C8C9935B30D94D4590A53478330FEB3', 
 '2012-03-11 11:10:35.000',True,False,'������� ������� �����������','��� ���� "�������"','6455000037','1026400002089','(8452) 56-04-26', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@alshamscapital.com', 'A72D0704D94626416823E12D1756588FD9415322', 
 '2012-03-11 12:19:24.000',True,False,'������� ������ �������� ����','�������� � ������������ ���������������� "�������������-���������� �������� "��-���� �������"','7838409980','1089847239690','(495)6814261', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kuharenko_yv@kombank.lipetsk.ru', '2102A3064A3C431B493D4C2C55285EE2D391E1F0', 
 '2012-03-11 14:45:15.000',True,False,'��������� ���� ����������','��� "�������������"','4825005381','1024800001852','(4742)32-92-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dcb@orgbank.ru', 'FB353D4B807EE66036E0925657EEAD971ADCBDB0', 
 '2012-03-11 15:50:42.000',True,False,'��������� ���� �����������','������������ ���� "������������� ����������� "�������" (�������� � ������������ ����������������)','7736176542','1027739267896','(495)730-30-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'BOGUNOVA@CMBANK.RU', '6772E7EC15BCEBCC7C2EFDD3EB7210DD03A1AA9F', 
 '2012-03-12 12:09:20.000',True,False,'�������� ����� ������������','���� ������-������','7705011188','1027739069478','(499) 237-55-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Galushko.Oksana@kami.ru', '6691AF4C84EF3BA56436B9995F62750B2C544840', 
 '2012-03-12 17:46:17.000',True,False,'������� ������ ����������','��� "����� ��� ����"','7733091756','1027739767241','(4852) 727-555 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depsec@necklace.ru', 'E97E37BB2265ECC0592D7DC7B533884A14367437', 
 '2012-03-13 12:39:08.000',True,False,'�������� ������ ����������','��� �� "������-����"','7707040963','1027700409791','79037237124', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yudina@prml.ru', 'BFC94C36D35ECD57773E71098ADD270683A35D28', 
 '2012-03-13 13:02:40.000',True,False,'����� ������ ����������','��� "�������� "�������"','4205029262','1024200681130','8(3842)75-77-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Efirkina_SU@kredbank.ru', '3CDAE3A05B8C6CFED77C626A56F9EBE492277228', 
 '2012-03-13 16:29:00.000',True,False,'�������� �������� �������','����������� ������������ ���� "�������������������" (�������� ����������� ��������)','2129007126','1022100000064','(8352) 58-17-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'root@bbank.bryansk.ru', '0C7B98F594DEC7533F1DD3A3C90D02BF449E76E0', 
 '2012-03-13 17:00:55.000',True,False,'����������� ����� ������������','������������ ����������� ���� "������-����" �������� ����������� ��������','3232005484','1023200000010','(4832)58-95-56', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'cb2@tkpb.tambov.ru', 'B4B0D85EFECFB32A96F1D0460FA4065AC69F58CD', 
 '2012-03-14 09:41:27.000',True,False,'���������� ������ �������������','��� "����" (���)','6829000028','1026800000017','(4752) 71 20 81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Gridasov@mkbank.ru', 'A85D8C79825FABD32F3EF66A76356C67A1EA1D78', 
 '2012-03-14 11:46:42.000',True,False,'�������� �.�.','��� �� "�� ����"','7744000599','1027739140220','495 748 54 24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'investprofit@investprofit.net', '40061C6AED876AC9B3FA09E41BC02E8D78D8D95F', 
 '2012-03-14 14:35:52.000',True,False,'�������� ������� �������','��� �������� �������� "������������"','7722269652','1027722004507','+7 495 722-10-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mitpravo@mail.ru', '02CB2E654777A1D8A0C3B24C1951263299FF66B0', 
 '2012-03-14 15:16:35.000',True,False,'������� ������ ��������','��� ���-�����','5904239955','1105904018439','3422590058', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'euro-fin-trust@rambler.ru', '26CC7881B85239C2B3219F44271B36169F030ED5', 
 '2012-03-14 15:22:06.000',True,False,'�������� ������ ���������','��� "����������� �������� "���� ��� �����"','7701853842','1097746654664','(843) 513 40 27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'anekt1@bk.ru', 'FA62F3D42AE06CE3C99E970D27FAA879AD58B4DA', 
 '2012-03-14 16:12:04.000',True,False,'��������� �.�.','���"�����"','7702226185','1027739293163','(495) 790-65-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a.ivannikova@tsbnk.ru', '9AE3B82137E35859B88B37EEC9DE680A922F0E44', 
 '2012-03-14 17:41:00.000',True,False,'���������� �.�.','��� "��������������" (���)','7730059592','1027739582089','(495)7863773', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olgav@roscap.com', 'B10C38D927F60017CD1662B3E07CB8A1DA5F3D70', 
 '2012-03-15 11:11:29.000',True,False,'��������� ����� ��������','��� "���������� �������" (���)','7725038124','1037739527077','(495) 775-86-86 (10837)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@nkotrc.ru', '4C82D939501AC8052FB1B61F46529E88C9A70EEA', 
 '2012-03-15 16:27:03.000',True,False,'������� ���� �����������','��� ��� "�������� ��������� �����"','7107063101','1027100002687','(4872) 25-07-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'GribanovaAN@baltbank.ru', '430DAE7D5C3DAC98F802FCEA21B2225D3BB5227A', 
 '2012-03-15 17:05:18.000',True,False,'��������� �.�.','��� "���������� ����"','7834002576','1027800011139','(495) 644-41-06', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zukova@kbb.ru', '17F244DEA7AA4A55895A67442FC8E1D73D309DB3', 
 '2012-03-16 10:09:23.000',True,False,'���������� ������ ����������','����������� ���� "�����������������" (�������� ����������� ��������)','4216004076','1024200001814','(3843) 76-32-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tambov84@yandex.ru', '3DCE7F60B4072017FE40278A8A29BF5B4659759E', 
 '2012-03-17 16:39:05.000',False,False,'������ ����� ���������','��� �������������-������������ ������ / ������ ����� ���������','7806466801','1117847528325','89811221300', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yurchenko@centrinvest.ru', 'B21CF1DBAC25E1E0A4FE952B8ED8DF074C51EB1F', 
 '2012-03-19 11:15:02.000',True,False,'������� �.�.','��� �� "�����-������"','6163011391','1026100001949','863-267-44-35', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Elena.Shardakova@mdmbank.com', '0489437A61A0350726739D2EBBBE2C5506D5A144', 
 '2012-03-19 11:33:32.000',True,False,'��������� ����� �������','��� "��� ����"','5408117935','1025400001571','797-95-00(54290)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@wmcm.ru', '8185D1307FFFFBB34845A215B6472A6C2DB9490A', 
 '2012-03-19 14:07:02.000',True,False,'�������� ����','��� "����������� ������� �����������"','7701844855','1097746433553','(495) 287-29-71', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vma@snb.ru', 'D7B4074B33264CCC58965EF7CC5093F36CA01777', 
 '2012-03-19 14:17:02.000',True,False,'������� ������ �������������','��� "������������"','7202072360','1027200000321','(3452)799-307', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'NMarusova@npfb.ru', '86681EB64027205565D20CB39B1E4965324AA87C', 
 '2012-03-19 14:40:12.000',True,False,'�������� ������� �������������','��� "��������������"','7710180174','1027739112103','(499)262-47-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ifkperspektiva.com', '66205FADDC621E778FF0C72736F431344D3480A1', 
 '2012-03-19 14:59:03.000',True,False,'������� ������ ����������','���" ��� �����������"','7719670960','1087746262515','(495) 783-90-28', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@depoline.ru', '6E4403AFCA34037224BA70783979EAA3DB08E3B2', 
 '2012-03-19 15:14:56.000',True,False,'���������� �������� �������������','�������� � ������������ ���������������� "����-����"','7708724998','1107746608749','+7 (495) 920-10-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ooobkinvest@gmail.com', 'B3F27162875D8BC22658469EB27CBCB715B5E223', 
 '2012-03-19 15:26:09.000',True,False,'��������� ����� �������������','�������� � ������������ ���������������� "�� ������" ','7715861811','1117746291442','79851791413', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kaskiyr@gmail.com', '9C812E54E9B2E88AED63EFA46298E908138ABB92', 
 '2012-03-19 15:44:01.000',True,False,'������� ����� ������������','��� "�������-������"','7709858994','1107746623401','89067622261', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Garant@ufanet.ru', '8F14A1C377D1FCC7593D1F9920D1C966AA07FB08', 
 '2012-03-20 08:35:12.000',True,False,'����������� ������ ���������','��� "���������� ����������"','7714285026','1027714021180','8(347)2448451', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'AvdeevKB@uralprombank.ru', '8F0B6964E271E733DE84073DFE54BE52B35D5C0A', 
 '2012-03-20 09:52:32.000',True,False,'������ ���������� ����������','��������� ������������ ���� (�������� ����������� ��������)','7449014065','1027400001727','(351) 239-65-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'chubarova.iv@gazprom-neft.ru', '0204635F39A1CF689951781F7ACF1182B6441D20', 
 '2012-03-20 10:18:46.000',True,False,'�������� ����� ������������','��� "������� �����"','5504036333','1025501701686','+7(812) 363-31-52 (+3105)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'brokdom_af@yahoo.com', '2056536384903BB5F2752A0C92FE5C3F7C330C94', 
 '2012-03-20 12:26:00.000',True,False,'��������� ������� ����������','���  "�� "�����-������"','7708736111','1117746227543','79859675876', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nadezhda.sergeeva.2011@inbox.ru', 'BAC831143C827723074B29694875EF4E361D5E9A', 
 '2012-03-20 12:41:41.000',True,False,'�������� ������� ������������','���  ������������� �������"','7710761860','1097746837979','+7 (495) 9629069738', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depalyans@yahoo.com', 'FFBEACE503AE42BCE915E1AB5E9C357AFA9ACB9F', 
 '2012-03-20 13:47:48.000',True,False,'������ ������� ����������','��� "������������ ������"','7729669829','1107746983640','774-46-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'centrrezerv@gmail.com', '920385D08AF226470DFF3147C3EB69AE922C393D', 
 '2012-03-20 14:39:54.000',True,False,'��������� �������� ����������','�������������� �������� "�����������"','7706552736','1047796726515','8(495)7663199', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Fomin@geobank.ru', 'E2293EBFD6D96DB1E4D124D7BA378FD2B1D690B9', 
 '2012-03-20 15:02:48.000',True,False,'����� ������� ���������','�� "�������" (���)','1102008681','1021100000393','(495)254-15-98   47-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'urist@solingcompany.ru', 'EDD7AE7969AB29E9A7817673DCDFB743FB76C036', 
 '2012-03-20 15:11:17.000',True,False,'�������� ������� �����������','��� "������ �������"','7708096461','1027739081754','(495)9253313', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Svetlana.Serikova@isbank.com.ru', 'C333AA88ED9957135F7C0C8569D5628E010AB871', 
 '2012-03-20 16:06:07.000',True,False,'�������� ��������','��� "������"','7706195570','1027739066354','(495) 232-1234', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bolgarchuk@vedis.ru', '75747D6AFF613E7598C335BE878C8D9F367E3C07', 
 '2012-03-20 16:29:35.000',True,False,'��������� ������� ����������','��� "������� ���������� ��������"','7714800284','1107746079407','(495) 797-80-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'otchetmtk@gagarinbank.ru', '830875184A14C769FE10BE4949B9475C1CF688BA', 
 '2012-03-20 16:43:48.000',True,False,'�������� ����� �������������','��� �� "�����������"','7729078921','1027739325624','+7 (495) 430-66-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'igor121060@mail.ru', '821E59F071C6576C2D5A766BB0178184E62E45E8', 
 '2012-03-21 12:02:38.000',True,False,'������� ����� �����������','��������� ����������������� ����������','3808026703','1023801030813','79025122056', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'evlahova@yandex.ru', '5752AE2050E68CA94E0D333C878CEB95BC609E7D', 
 '2012-03-21 12:47:33.000',True,False,'�������� �.�.','����� ��� "���� (����)"','6163022805','1026103165538','863-263-53-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gft8@mail.ru', 'E8341BB2F0D1E29289A9A6A06E67186F4BFA0604', 
 '2012-03-21 13:41:42.000',True,False,'�������� ����� ����������','��� "����"','7701863657','1107746037871','74997443102', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vex@mskic.ru', 'B82007DAD4C7503CE15C9A517E0DB5E9438F816D', 
 '2012-03-21 14:29:55.000',True,False,'������� ������ ��������','���"���������� �������������� ��������"','7701705450','1077746350219','(495)9815883', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a_zhdanov@orbank.ru', 'B3E1A0E9954D1A01AABC1932C2763E5490C7EA3C', 
 '2012-03-21 14:31:32.000',True,False,'������ ��������� ��������','�������� ����������� �������� ������������ ���� "��������"','5612031491','1025600002230','8 (3532) 79-72-94', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depotrade@mail.ru', 'E47DB7466FD4680C85A3F6FD83371347B44ED209', 
 '2012-03-21 18:06:09.000',True,False,'�������� �������� ����������','�������� � ������������ ���������������� "���������"','7734638174','1107746489949','(495) 648-54-06', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aktivar@tfb.ru', '5E936BE45FAE70E2A876E49DD86A279A8402050F', 
 '2012-03-22 09:14:32.000',True,False,'�������� ������','�������� � ������������ ���������������� "����������� �������� "�������"','1655110951','1061655057419','(843) 2-911-782', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Uschenko@vbrr.ru', 'F66DC9BC0444061B2C43362DD2E90ABA140D9981', 
 '2012-03-22 09:38:05.000',True,False,'������ ���� ���������','��� "����"','7736153344','1027739186914','8(495)933-03-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'backoffice@inresbank.ru', '344CD4FDDF5D9EF36D0C83877F4E13437938DBA1', 
 '2012-03-22 11:19:14.000',True,False,'������ ����� �����������','���������" ���','7709000765','1027739320003','+7 (495) 708-03-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'anna@khovansky.ru', '5A8284452540A04E010C79E63A112971707A41A8', 
 '2012-03-22 12:05:07.000',True,False,'��������� ���� ����������','����������� ������������ ���� "���������" (�������� ����������� ��������)','7717016448','1025000002411','8 (495) 6877717', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'hsf@priobye.ru', '4608C8148008F405F672B580312801B4479B9678', 
 '2012-03-22 14:07:05.000',True,False,'�������� �.�.','��� ��� "�������"','8603010518','1028600002100','(3466) 41-26-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'chepelyuk@vegabank.ru', '9EDD533036CF7CDB01E2BEE7EC7D3C640D50C528', 
 '2012-03-22 15:03:01.000',True,False,'������� �.�.','�� "����-����" (���)','7727095209','1027739059710','918-24-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'patrikeeva@eurocitybank.ru', '28182894CF205F7FD306978CF879B8FCF2FF162A', 
 '2012-03-23 09:59:45.000',True,False,'���������� ����� ����������','��� �� "������������"','2632052342','1022600000059','(495) 777-27-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'investsoyuz@bk.ru', '29ED5662AB90674DEE74CE75968CF7ABE2EDA687', 
 '2012-03-23 10:36:32.000',True,False,'�������� ����� �������','�������� � ������������ ���������������� "����������"','7708716041','1107746207469','79826571854', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'optimainvest@bk.ru', '1971583ABE680719A1A62B1681869362EC2789A3', 
 '2012-03-23 11:12:15.000',True,False,'������ ������� ����������','�������� � ������������ ���������������� "������������"','7730622688','1107746165878','(499) 391-91-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fincons-u@bk.ru', '420F3F6E8CBF08FD8F9060C40B303407B5E5EA41', 
 '2012-03-23 11:23:56.000',True,False,'������ ��������� ���������','�������� � ������������ ���������������� "����������-�"','7743783058','1107746466354','(495) 505-27-86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'enalaysis@mail.ru', '283FE4089A549AEE7D45776FD91D378C623EBA80', 
 '2012-03-23 11:28:30.000',True,False,'�������� ����� ���������','�������� � ������������ ���������������� "���������"','7743782992','1107746465530','(495) 641-81-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vostok-inv@bk.ru', '7955E83693ED99E9EB094E76EBFC86ED954548E8', 
 '2012-03-23 11:34:18.000',True,False,'������� �������� ������������','�������� � ������������ ���������������� "������������"','7722721220','1107746522322','(499) 391-92-86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'linctrade@mail.ru', '6EB955BCE4BD44E258B34CD4AF8F406CC71A2ED0', 
 '2012-03-23 11:43:35.000',True,False,'����������� ������ ����������','�������� � ������������ ���������������� "���������"','7713708078','1107746438425','(495) 646-68-17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'broker.persona@mail.ru', '085CBB1D4AE6FA0AA63EC186B6CE2717CEBB891A', 
 '2012-03-23 11:56:11.000',True,False,'������� ������� ������������','�������� � ������������ ���������������� "������������ ������"','7720690270','1107746536842','(495) 645-07-16', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'centr-strategy@mail.ru', '2915E6070249DED27F0089DC103A355338F3A2C6', 
 '2012-03-23 12:16:30.000',True,False,'����������� ������ ����������','�������� � ������������ ���������������� "����� �������� ���������"','7718809591','1107746460073','(495) 233-87-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rusfinanse@bk.ru', 'AFDA2DF4F06AC50E53A53AC58AAB03412FC8D718', 
 '2012-03-23 12:21:07.000',True,False,'���������� �������� ������������','�������� � ������������ ���������������� "���������"','7722723530','1107746596760','(495) 648-85-16', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'maks-inv@mail.ru', '4A9387D6D77A6A91489CA3781942254FE22606B2', 
 '2012-03-23 14:03:54.000',True,False,'������� ����� ����������','�������� � ������������ ���������������� "����-������"','7722723001','1107746575716','(495) 648-82-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'reserval@list.ru', 'EB82E21F7B915FF83E55E72DAEC13E26482ECFC7', 
 '2012-03-23 14:07:23.000',True,False,'��������� �������� ����������','�������� � ������������ ���������������� "������������"','7730628979','1107746561768','(495) 641-83-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'invest_tr@mail.ru', '13CBC3F08D46D0FEC8DA4EFB8F8DF250858F57F8', 
 '2012-03-23 14:20:21.000',True,False,'����� ��������� �������','�������� � ������������ ���������������� "������ ��������"','7721697133','1107746517944','(495) 647-83-96', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'finanspr@mail.ru', '62B3B7B5CDDE43695D0BDEF4C61270875C6ED766', 
 '2012-03-23 14:24:43.000',True,False,'��������� ������� ����������','�������� � ������������ ���������������� "���������� �������"','7722720530','1107746498562','(495) 648-42-29', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'iriscus@bk.ru', '9898B5F1FA3095549611263C754BDA1B77097045', 
 '2012-03-23 14:28:36.000',True,False,'�������� ����� ���������','�������� ����������� �������� "���� �������"','7718723640','5087746175083','(495) 641-87-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'str-inno@list.ru', '09D9F9C92A7A4A2024401988E3823B25B6ADDFB3', 
 '2012-03-23 14:32:22.000',True,False,'������� ������� ������������','�������� � ������������ ���������������� "��������� � ���������"','7719744308','1107746171180','(495) 782-54-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'perseitrust@mail.ru', 'F07F8BA439B815127CC5A6E694AF90F709158AD8', 
 '2012-03-23 14:44:30.000',True,False,'���������� �������� ������������','�������� � ������������ ���������������� "�����������"','7730626675','1107746438403','(495) 782-72-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sunrisegrup@bk.ru', '5CC472B2C6FF864F7A89D985444D614D54D72760', 
 '2012-03-23 14:48:58.000',True,False,'��������� ���� �������������','�������� � ������������ ���������������� "�����������"','7723683129','5087746301715','(495) 641-83-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'trade-invest@inbox.ru', 'E6260A32B8AB5034153AF07008807EF3A1AE8ADD', 
 '2012-03-23 14:52:47.000',True,False,'��������� ����� ����������','�������� � ������������ ���������������� "����� ������"','7723616041','5077746881746','(495) 733-95-73', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'avalon-invest@mail.ru', '7C118B79718E48E3976E92EAAFA738088FC124EC', 
 '2012-03-23 14:55:56.000',True,False,'������� ������ ����������������','�������� � ������������ ���������������� "������-������"','7710259547','1027739642622','(495) 502-57-58', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'spektrfinans@mail.ru', '7FCFE5C31D672860D8E7325DA93579CD88CC6072', 
 '2012-03-23 15:01:28.000',True,False,'������� ����� ������������','�������� ����������� �������� "������������"','7705885370','1097746186328','(495) 648-81-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rashid077@ya.ru', '340A2C3AD394EF3456EAE95B592C9732BB9813F3', 
 '2012-03-23 16:36:14.000',True,False,'������� ����� �������','��� ��������','6439066841','1076439003387','9271227701', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ik-talisman.ru', 'EE59A9DFF4F396D371AE16EFC7083B4A572E7806', 
 '2012-03-26 11:47:19.000',True,False,'������ ����� ������������','�������� � ������������ ���������������� �������������� �������� "��������"','7701783419','1087746572792','(495)662-43-71', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'NVidyakina@vtb-leasing.com', '75EF071018FF5AF7AF720AAE2E723888B954F94E', 
 '2012-03-26 14:43:09.000',True,False,'�������� �������','��� ��� ������','7709378229','1037700259244','+7 (495) 514-16-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'skripchinskaya_a@novahovcb.ru', 'CAEF0158C163F3900987D0B8A5501503C9FC2930', 
 '2012-03-26 14:46:43.000',True,False,'������������ �����','������������������ (���)','7319000865','1027300001630','(495)660-55-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@rwmn.ru', '305FC5640746862D4CC746E1E558AF3AB06AF4AA', 
 '2012-03-26 14:53:53.000',True,False,'������� ����� ����������','��� "���"','7710881927','1117746048925','8-495-916-60-26', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kaurova@transbank.ru', 'F1880F9C90DC89E968FD45B6746BCE32B98EF6F6', 
 '2012-03-26 15:07:32.000',True,False,'������� �.�.','��� �� "������������"','7710070848','1027739542258','495-747-93-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mfo-rybliartem@rambler.ru', 'F256EBFEFD088CDE4E034B9CA21753709814FC1E', 
 '2012-03-26 15:44:29.000',True,False,'������� ����� ������������','��� �����','4329012818','1114329000719','89123354872', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '134@gazfin.ru', '0290934A049D14B6CE78118EB6374032FA26EF26', 
 '2012-03-26 18:08:36.000',True,False,'����� �.�.','��� "�� "�����������"','7713012890','1027739693431','(495) 660-14-38', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sifp24@gmail.com', '4BF11F3B4E4C6E4E2D31EC28E274E1D2F4D481C2', 
 '2012-03-27 08:04:52.000',True,False,'����������� �������� ���������','�������� ����������� �������� "���� "�������"','2466020655','1022402650082','(391)2520930', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'krivko@most-info.ru', '71E9B6EEBFBEC02021F57DBB932B0A67949AE73B', 
 '2012-03-27 09:11:17.000',True,False,'������ ������ �������������','��� ��������-������������','6659140843','1069659052760','+7(343)290-61-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ur@uralniti.ru', '0360B285037A81F43430F39883C9CF40A4C371CF', 
 '2012-03-27 09:17:13.000',True,False,'������� ������� ����������','��� ��������','6659005604','1026602949955','(343)355 24 75', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'm.alehina@m73.ru', 'DF8DF62F787705E0E3E9F65600832B2D5A0C1B47', 
 '2012-03-27 09:32:56.000',True,False,'������� ����� ������������','��� "���������� �������� ���-�����"','6679000266','1116679000679','+7(343)2535073', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'admin@amf22.ru', '879CAE91A3E8D832C7B23347999529F2344559DA', 
 '2012-03-27 10:40:06.000',True,False,'����������� �.�.','��������������� �������� � ������������ ���������������� "����������������"','2222798497','1112223009799','(3852) 71-77-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tvm@garantia.nnov.ru', '55185F17B0BC8B6B6DC794FCB6DB6A9ED2F0E573', 
 '2012-03-27 11:30:14.000',True,False,'�������� ������� ����������','��� "��� ���������������"','5254004350','1025200001254','(831) 430 86 09', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rustelsmol@mail.ru', '534C7551D295F6DDDDD75608324FABD59C4AACFB', 
 '2012-03-27 12:12:44.000',True,False,'������� �. �.','��� "����-�������"','9530000002','1086731015172','(495) 647-70-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yralsfin@gmail.com', '080E896577A9DF48A20E5EEB69940CA3B42F705E', 
 '2012-03-27 12:49:37.000',True,False,'�������� ���� ����������','�������� � ������������ ���������������� "����� ������"','7716669324','1107746639329','(495) 193-52-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'b.kapital@mail.ru', '0B16BB8402F0C98F5AC5C58B87A983F9111083CB', 
 '2012-03-27 14:11:45.000',True,False,'������ �������� ����������','��� �� "������� �������"','7701664154','1067746696720','2208159', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'invest_buro@mail.ru', '0B16BB8402F0C98F5AC5C58B87A983F9111083CB', 
 '2012-03-27 15:15:25.000',True,False,'������ ��������� ���������','��� "�������������� ����"','7705886609','1097746217777','7207369', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Atayan@mpcb.ru', 'D09E17D4068BA05B066995D859D4B7ADC4A1E7E7', 
 '2012-03-27 15:54:24.000',True,False,'����� ������� ����������','��� ���','7744000327','1027739259360','787-72-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ooopik7@gmail.com', '1F20A5F550585BC716F27ED80F6C15CABE1FBEFC', 
 '2012-03-27 16:24:20.000',True,False,'��������� �.�.','OOO "���"','7839384632','1089847241164','NULL', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lubarec@hml.ru', '8C3DF06CDBB3259770DEC14F7EB21CDDE3A70838', 
 '2012-03-27 17:20:54.000',True,False,'������� ����� �������������','���� "���" ���','7750005570','1107711000011','4995037768', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga.ivanova@auroracm.ru', '6091B89D19C61E25C68AB08A6F4910188C294D02', 
 '2012-03-27 18:11:43.000',True,False,'������� ����� ������������','��� "�� "������ ������� ����������"','7702748142','1107746993551','602-02-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vis.invest@mail.ru', '5B06B8560C565C7E0EA9F98E700E58BE82923B62', 
 '2012-03-28 10:17:52.000',True,False,'������� ����� ���������','��� "�����-������"','5249005616','1025201747889','(8313) 26-83-49', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vrnfic@alor.ru', '7FB1756665D763B01576CD0CA00768FDF434AFC2', 
 '2012-03-28 10:41:57.000',True,False,'�������� �������� ����������','�������� � ������������ ���������������� "�����������  �������� �������� �����"','3664063081','1053600100003','(473)2205546', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '5008500@mail.ru', 'C6D1FCD496DC3A4D5ED0559AD33AE4C1EA3008C0', 
 '2012-03-28 12:44:25.000',True,False,'��� ��������� ����������','��� "�����"','6143077789','1116174005529','9185593952', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'reception@ikmtec.ru', '44C7D7EABF3D1C9C2585606786E7F46A9A9DC074', 
 '2012-03-28 13:19:55.000',True,False,'����� ����� ������������','�������� ����������� �������� "�������������� ��������������� ��������-�������������� ��������"','7736203676','1027739021749','(495) 780-80-49', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'back@compinvest.ru', '91B05980373DEAC58616D50D6A504EA823999994', 
 '2012-03-28 13:32:01.000',True,False,'������� ��������� ����������','�������� ����������� ��������  "�������� ����������� ����������"  ','7707575528','1067746197640','(495)780-88-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'finance@prioritetbank.ru', '709031DCA3647C3DEEC463438C59915F6D152D58', 
 '2012-03-28 14:22:38.000',True,False,'������ ������������������','��� ���� "���������"','7715024193','1036303380850','373-05-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aam@akibank.ru', '2EDD50DF010BEA02CB436FAD91736948FC822F1B', 
 '2012-03-28 15:37:21.000',True,False,'�������� ������� ��������������','��� "�������"','1650002455','1021600000839','(8552) 33-37-35', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'delincomp@gmail.com', 'B9EBD5FE3D0644830D60E191F0FF571157EE5CBB', 
 '2012-03-28 17:08:49.000',True,False,'�������� ����� ����������','��� �� "������� ����������"','7705929073','1107746791074','+7 (499) 243-03-55 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jscapitalinvest@mail.ru', '44D157C36CE52555507B42E487D6E95EFEC5267C', 
 '2012-03-28 17:23:15.000',True,False,'��������� ���������','��� "�������������"','7810051165','1067847193402','3226491', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ifkkur@kursktelecom.ru', '4321DE5D603DBB4BF0641FEA3D4A0CE69FBD733A', 
 '2012-03-29 09:42:16.000',True,False,'������� �.�.','��� "�����"','4629004328','1024600938779','84712520271', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'clos@list.ru', 'D5C83F04A471B941C8DB2DFEEFABCE1829A08ADC', 
 '2012-03-29 10:17:16.000',True,False,'������� ��������� ������������','��� "�������"','3662055744','1033600017835','89518731977', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'investkomservis@gmail.com', '3AEAF11559157F5C4B14E1D4CB80C344A1425699', 
 '2012-03-29 12:59:35.000',True,False,'�������� ����� ���������','��� "���������������"','7715849042','1117746054931','(499)347-60-35', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'marin_sok1984@mail.ru', '0FCA474069258D9E94542967352213C7CD39B00B', 
 '2012-03-29 13:21:24.000',True,False,'������ �.�.','��� "�������� ����"','2632801464','1112651016411','8-928-982-58-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'academinvest@bk.ru', 'D7D8BA3677702666D090AD84956498D8605F1E6D', 
 '2012-03-29 13:57:51.000',True,False,'����� ������� ���������','��� "������-������"','7715849483','1117746060321','(499) 5069710', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ossfin.ru', 'DABE47EA893192E96E85F076EFCFB9FDB65DE9C4', 
 '2012-03-29 14:05:16.000',True,False,'���� ������ ������������','��� "�.�.�.-������"','7720706025','5107746072770','(499) 408-12-48', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aktivinvestltd@gmail.com', '3AEAF11559157F5C4B14E1D4CB80C344A1425699', 
 '2012-03-29 14:12:17.000',True,False,'�������� ���� ������������','��� "�����������"','7701873535','1107746304929','+7 (499) 991-02-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'r.invest@gmx.com', '60263EB6C91826F2C39CEBFB50F03B2B2CB96D8B', 
 '2012-03-29 14:18:53.000',True,False,'������ ����� �������������','��� "����-������"','7705934041','1107746958505','499  347-86 63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zaofcbt@gmail.com', 'F37DC677AD25BB1A1D1FAFC2E68B15F1DCE5F03E', 
 '2012-03-29 14:24:43.000',True,False,'������� ������� �������','��� "�� "��"','7705885891','1097746201630','499 409-05-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'openinvest@bk.ru', 'C69827BEA8BB7E17A143837032D2919D4438813A', 
 '2012-03-29 15:12:11.000',True,False,'��������� ��������� ��������','��� �� "����� ���������� ������������"','7733636632','1077764148351','(351)256-26-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'back-off@nbdbank.ru', '45BFC5884760F5F10B251EA9EA8550D1820A3B92', 
 '2012-03-29 15:20:59.000',True,False,'�������� �������� �����������','��� "���-����"','5200000222','1025200000022','(831) 22-000-22 +3185', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vak@hml.ru', '8DF6BC64755E4D33F8F9D32645D9D5AD3DA83214', 
 '2012-03-29 15:40:37.000',True,False,'������ �������� �����������','��� "������ ���" (�������������� ����������)','7720507799','1047796329965','(499) 503-77-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf@feib.ru', '4C703F8519751074D21599CB4FAC448575A39EFC', 
 '2012-03-29 16:02:08.000',True,False,'������� ������ ����������','��� "��������������"','7704225902','1027700356090','(499)245-96-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'iklochkova@svyaznoybank.ru', '8227026165AA8086E34A861263838F5349CDE04A', 
 '2012-03-29 18:24:20.000',True,False,'�������� �.�.','������� ���� (���)','7712044762','1027739019714','+7(495)786-88-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '7823861@mail.ru', '104538B2DBCE8320B984CFEE681073AA09B09B8D', 
 '2012-03-29 23:05:04.000',True,False,'��������� ������� ����������','�������� ����������� �������� ������-���������������� ����������� "������� ������������� ����������� �������" ','7730617617','1097746696189','7823861', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stock@snbank.ru', '87254D5E897BAEFEE6CA3513E79533C0E923E0C9', 
 '2012-03-30 09:46:56.000',True,False,'�������� ������ ���������','���� "����������" ���','7423004062','1027400009064','8-351-2475974', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'iudin@inkaro.ru', '47A054BE615F728014720A315BAB8A3BFEFA25C3', 
 '2012-03-30 11:18:59.000',True,False,'����� ������ ������������','��� "����������" (���)','7710144056','1027700050510','74957770960', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zhanna.slavskaya@gmail.com', '395A9EDB2C08336641993DC27C3F1192EF2E6B2B', 
 '2012-03-30 14:23:47.000',True,False,'�������� ����� ���������','��� "�������������"','7710908939','1127746189427','+7(499) 6782139', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk-proekt@mail.ru', '085C0B71173862D7BBDCF54A0C2C4F70CD3C3123', 
 '2012-03-30 14:39:07.000',True,False,'������ ����� �����������','��� "����������� �������� ������"','7704136882','1037739227130','8-(495)695-06-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ingd.ru', '3CDCB111E4839EF0D3027D7731A6563A7B9A19E4', 
 '2012-03-30 14:43:47.000',True,False,'�������� ������� ����������','�������� ����������� �������� "����������� "��������"','7722659028','5087746168274','(925) 740-66-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ruslan@brokyard.com', '99D2EAF2F5E0E99833D2A2BF8E0F6D14F7DFCA39', 
 '2012-03-30 18:44:27.000',True,False,'������� ������ �����������','��� "������� ���"','7710861737','1107746040698','(926)266-3024', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ik-maksat@mail.ru', '3B799559FA95901BE70B7C1C426CE5FDE6514B64', 
 '2012-03-31 07:59:30.000',True,False,'�������� �����','��� "�� "������"','268046263','1070268003190','3473411718', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'shatov@sckb.ru', '0A2B26E279672365E3A7581350281332FE891049', 
 '2012-04-02 09:42:29.000',True,False,'����� ������ ����������','��� �� "���������� �����������"','8602190032','1028600003410','(3462) 51-76-16', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stocks@emb.ru', '73AB3A770124BF8BC7AE7DB1EB9E97C9CB5F58E8', 
 '2012-04-02 09:59:02.000',True,False,'������� ������� ���������','�������� ����������� �������� "���������������� ������������� ����"','6608005109','1026600000074','(343) 377-67-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mailbox@eximbank.ru', 'ABEF3EB2F9188D595CD14C7949B787E3CFB032DD', 
 '2012-04-02 10:05:52.000',True,False,'��������� ������ ����������','��������������� ������������������ ���������� ���������-��������� ���� (�������� ����������� ��������)','7704001959','1027739109133','725-61-59', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aviapolis@yandex.ru', '316C28566A222702E9AE51FD1D6E9BAF096800E5', 
 '2012-04-02 10:09:37.000',True,False,'���������� ����� �������������','�������������� ����������� "����������������� ���������� ���� "���������"','7714097833','1037700025703','(495) 780-34-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zaikov@bankrs.ru', '54208684A2A0314ACEE2EF165179BB8E8DB6370C', 
 '2012-04-02 10:12:19.000',True,False,'������ �������� ����������','��� �� "���� �������� � ����������"','7715027290','1027739152737','(495)600-40-80  107', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ofsfr@kbumb.ru', '3DA0F7BD38F2FB6BD54550E3E6342017F61A09CA', 
 '2012-04-02 10:52:48.000',True,False,'��������� ������� ������������','�������� � ������������ ���������������� ������������ ���� "��������� ��������������� ����"','6615001384','1026600000063','(343) 375-65-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'biw@plus-bank.ru', '8EBE2156067DBA3C02E15FE3FBD2C865C2566A1B', 
 '2012-04-02 11:08:44.000',True,False,'�������� ���� �������������','��� "���� ����"','5503016736','1025500000624','(3812) 32-12-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk-ekspert@inbox.ru', '040D525AF81A140EBD405AFF8119B6420817DCD7', 
 '2012-04-02 11:25:58.000',True,False,'������� ��������� ��������� ','�������� � ������������ ���������������� "����������� �������� "�������"','7804415505','1097847144658','(812) 510-86-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@elbin-bank.ru', '39D237C42B15206A691D387DC62311D950CF4464', 
 '2012-04-02 12:39:24.000',True,False,'���������� ������� �����������','����������� ������������ ���� "������" (�������� ����������� ��������)','541002446','1020500001103','8(8722)64-15-92', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga.rodzyanova@icicibank.com', '7554A59849F9717025CE383FB973F3410ABBF7A4', 
 '2012-04-02 13:30:17.000',True,False,'��������� ����� ����������','���������� ���� ������� (���)','4003011294','1024000002806','(495) 981 49 88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ca@orbitacom.ru', '71DED98CF1FA1D2F55FA2C355C4034ED145F22A9', 
 '2012-04-02 13:47:25.000',True,False,'����� ������� ����������','��� "������"','2309072734','1022301424507','2010144', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ct.invest.info@gmail.com', '7C99D330BC4F0972E039E78B4D13883995461F7B', 
 '2012-04-02 16:38:09.000',True,False,'������� ����� �������������','�������� � ������������ ���������������� "���� ������� �������"','7701796030','1087746984544','8-905-586-89-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'support@pfsbank.ru', 'B24FB06949D66D94871EC904C9D213914D91EF69', 
 '2012-04-02 16:59:39.000',True,False,'������ ������ ����������','�������� ����������� �������� ������������ ���� "�����������-���������� ��������������','7744002187','1027700136408','911-72-49', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'loladzevg@rambler.ru', '11808A409EF50C6CF92C80AD84FEBEBB3E9ED7A1', 
 '2012-04-02 17:00:41.000',True,False,'������� �������� �������','��� "�����-������"','5040104478','1115040000085','8(919)729-81-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'hrapova@greencombank.ru', '85A03311A09452AD7B93EB1E001D979CF3FD8AB0', 
 '2012-04-03 09:35:40.000',True,False,'������� ������� ������������','��� �����������','3819001330','1023800000190','(39543)38878', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kirillova_ii@krvostok.ru', '6AD4DF2A6F18D4B13C7067F1B06C30175D046BC9', 
 '2012-04-03 10:07:08.000',True,False,'��������� ����� ��������','��� "�� "�������������������"','7716580524','5077746957426','(843) 293-20-75', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kaz@akbnkb.ru', 'CE10C5562271B55B7F554F35BA51D86EE7699AB0', 
 '2012-04-03 10:54:39.000',True,False,'������� ������� �������������','��� "�����������" ���','7744002821','1027744002989','74957803115', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Pushkareva_NI@promtransbank.ru', 'D71DEB298D625EC06898F16D45F31832253CAA59', 
 '2012-04-03 10:58:11.000',True,False,'��������� �.�.','��� "�������������"','274045684','1020200000083','(347)272-50-79', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kazna@kmbank.ru', 'B3584B1118EAD0397C8AC94E7503CB4167DDC270', 
 '2012-04-03 11:09:02.000',True,False,'�������� ������� ������������','���� "��������� ����"','7750004094','1077711000014','510-63-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcsm@bcosm.ru', '9BEB0AC406CC06B180D9504EA52FEA4C81614B08', 
 '2012-04-03 11:36:47.000',True,False,'������ ������ ���������','��� �� "������"','7744001930','1027739019373','8(495)258-89-84', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Potapova@akcept.ru', '0C9D4407E7247B7C5339FD38C58B3EE82F0F2636', 
 '2012-04-03 11:58:57.000',True,False,'�������� ���� �������','��� �� "������"','5405114781','1025400000427','(383) 219-18-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ekalachina@gmail.com', '656CD9A232D157A9DAC352300D7D1E8FBCE584F2', 
 '2012-04-03 12:22:37.000',True,False,'�������� �.�.','��� �� ����������','7734516680','1047796726669','(8342)230118', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vam@vkbank.ru', 'FDB48A08FCA383AD022DD6F4DF7D9F20113B550D', 
 '2012-04-03 13:26:09.000',True,False,'�������� ��������� ����������','��� "�����-������� ����"','6317009589','1026300001782','846-2639924', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@fk-sv-trade.ru', '08FA3AB6E1B92879B297B70F112754323734296D', 
 '2012-04-03 13:55:16.000',True,False,'��������� ����� �������������','�������� � ������������ ���������������� ���������� �������� "��-�����"','7714847726','1117746626580','(499)346-39-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'remlo@bcs.ru', '1E0764AFB968E579B4275756E82EF7BFD152AA34', 
 '2012-04-03 14:38:11.000',True,False,'������� ����� �������������','��� "��� - �������������� ����"','5460000016','1055400000369','8(383)211-90-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'np-sodeistvie@mail.ru', '632B284A7F3496A99F9136AFE7B996FD7AD973E8', 
 '2012-04-03 16:03:09.000',True,False,'�������� ������� ����������','�������������� ����������� "���������� ������ � �������� ������������������� ���������� ������ ����������� ��������� �������"','6357090113','1026303314278','8-84656-28091', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@bdb.ru', '8BC37F58D1F1E01B17E28979AFE1A912194FF4A8', 
 '2012-04-03 16:14:37.000',True,False,'����������� �����','�� "���" (���)','7705012463','1027739635054','915-26-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'krasnobaev_m@centrcombank.ru', 'F8684AF50E6A8D7C9F2457B200F6E61556638F68', 
 '2012-04-04 08:39:14.000',True,False,'��� ������� ����������','����������� ������������ ���� �������� � ������������ ����������������','7703009320','1027739019527','(495)953-06-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vasileva@nokss.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2012-04-04 09:31:32.000',True,False,'��������� ����� �������','��� ���������','3442028061','1023400000018','(8442)23-92-30', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gorchakov@nkbank.ru', 'CA7ED873EB32BB6AC6D1A3E290BACCA6680E287B', 
 '2012-04-04 13:42:58.000',True,False,'�������� �.�.','��� "�� ����"','7734205131','1027739028536','4954118844', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'saparova@aversbank.ru', 'BDDB5B74DF45CFF48142B3EC74EC992CCDF7A9CD', 
 '2012-04-04 13:59:13.000',True,False,'�������� ������� ����������','��� ��� "�����"','1653003601','1021600000993','78432921664', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ollu_82@mail.ru', '57B1FF5F74CBF762BB3758378EA2AF9880753B6C', 
 '2012-04-04 15:45:24.000',True,False,'��������� ����� ����������','��� "�����-�"','5905285915','1115905003466','89641953130', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jnazarova@rsb.ru', 'A1E047A8826141A44C6ABBB69E44703E4650916A', 
 '2012-04-04 17:03:51.000',True,False,'�������� ���� ���������','��� "���� ������� ��������"','7707056547','1027739210630','797-84-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'golovina@baikalcredobank.ru', '5375F1CA951CAD360AAED7998E508781A69859E0', 
 '2012-04-05 06:44:25.000',True,False,'�������� �.�.','�� "���������������" (���)','3807002717','1023800000278','(3952)241602', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stroymaterial@e1.ru', '7746E068A62714733267A68C5BC9C4FA6646E68E', 
 '2012-04-05 09:43:56.000',True,False,'��������� �������','��� "��������������"','6660000978','1036603489559','(343)  355-19-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bailova@vyborg-bank.ru', '425EBBB8A78B2674D2BEAA774914D4812B528E2B', 
 '2012-04-05 12:04:42.000',True,False,'������� �.�.','��� "������-����"','4704000029','1024700000071','(81378) 3-00-39', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'perm_fond@mail.ru', 'FD13B8BB3FDD059544A1D323D3FC799DC1AD40A2', 
 '2012-04-05 12:27:27.000',True,False,'�������� �����','��� ���','5948021770','1025902399060','(342)278-78-97', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'v.valeeva@energobank.ru', '35F4273D563A0928558398547A674EC7167D9FE6', 
 '2012-04-05 14:04:02.000',True,False,'�������� �.','��� "����������" (���)','1653011835','1021600000289','(843)2316043', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'd.povar@uralfd.ru', '302B0ED4281113E81026585B68DEA95E6389970A', 
 '2012-04-05 14:39:35.000',True,False,'����� ������� �������','��� ��� "���� ��"','5902300072','1025900000048','(342) 240-10-79', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'petrolay@mteb.ru', 'F34C95A376E5F6FED77C9C3E54160E4151B7B775', 
 '2012-04-05 14:55:19.000',True,False,'�������� �.�.','��� "����������������"','7701014396','1027739253520','623-36-41', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ffms@pchbank.ru', 'B30C39BCF158ADE037D08CBCE806E002B4BB2F2F', 
 '2012-04-05 17:40:52.000',True,False,'��������� ������� �������������','��� ��� "���"','7744003134','1037711004011','+7 (495) 223-03-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'shalaikin@dvbank.ru', '86845220987432703761853A87E27FFA69C1E29E', 
 '2012-04-06 05:49:49.000',True,False,'�������� ������ ������������','�������� ����������� �������� "��������������� ����"','2540016961','1022500000786','(423) 251-64-12 (293)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mordovfinans2011@mail.ru', '9050318ACF2D2CEBC89B01E807C4AA1D43F07F78', 
 '2012-04-06 09:53:15.000',True,False,'���������� �������','��� "������-������"','1326217963','1111326000521','89279738856', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stock@promsberbank.ru', '9BE27AA05FEC7525FA7A068BCE33B9AAB57AAF8C', 
 '2012-04-06 10:11:35.000',True,False,'�������� ������� ������������','�������� ����������� �������� "������������ �������������� ����"','5036037772','1025000000090','8(495)9335809', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@monolitbank.ru', '9A2DCEAEDA5397BD73E7000CD80D2E8097E9ED16', 
 '2012-04-06 10:42:28.000',True,False,'������� ������ �������','��� �� "�������"','7735041415','1027739599535','8(495)783-84-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ku403@euro-mail.ru', '4EF8EE20C8F9BF2D132377037A09FB80AD3E3408', 
 '2012-04-06 11:19:31.000',True,False,'��������� �. �.','��� "�������" (���)','7726016846','1027739077200','495 - 628-8517', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'borodina@cibank.ru', '9AFCFAFFE99D6606A09B0347616D88F2A35490A1', 
 '2012-04-06 12:38:49.000',True,False,'�������� ����� �������������','��� "���� ������ ����"','7831001422','1027800000095','78123240679', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@dekr.ru', '94ED9632733923DFE8EAC87CC67ED66BECA3C5FB', 
 '2012-04-06 13:35:34.000',True,False,'�������� �������� ���������','�������� ����������� �������� "�������������� �������� "������"','7722660288','5087746246220','8-901-182-32-94', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'trustdm@gmail.com', '4CE3AAFB781FE8E753195689EAE16F4612E0CFA8', 
 '2012-04-06 16:16:49.000',True,False,'�������� ������ ��������','��� �� "�� �����"','7701797926','5087746044183','(495) 649-61-24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'M.Kantserova@leader-invest.ru', '3A043357D8467533A76D764498E2DBDDB6420701', 
 '2012-04-08 14:38:24.000',True,False,'��������� ������','��� "�� "�����"','7726661740','1107746785410','8 495 280-05-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sst-63@mail.ru', '6CA0E27C5DAB95EE742F6FE0C5247818CE2E13F1', 
 '2012-04-09 11:12:33.000',True,False,'������� �.�.','����������������','6321141212','1046301067559','(8482)78-42-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a8708595@yandex.ru', '64E6387E5FB7299E3242F813DE6832FFED325FB2', 
 '2012-04-09 12:21:05.000',True,False,'������� ���� ����������','��� "������� �����"','7704687255','1087746540375','(926) 818-88-56', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'microfond_26@mail.ru', '131F05CAFC860CFC8109C437302D275AF49CA0A3', 
 '2012-04-09 13:02:45.000',True,False,'������� �.�.','�������������� ����������� "���� ������������������� ��������� ������ � �������� ������������������� � �������������� ����"','2634091033','1102600002570','8(8652) 37-14-19', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mfoexpress@yandex.ru', '7D6ABEC004931B46224E1EAC683F69980F712894', 
 '2012-04-09 16:03:19.000',True,False,'�������� ���������','��� "��� ��������"','5904265176','1125904003125','73422477461', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.zaharov@mos-oksky.ru', '4DB22401A32B0E6CA394670A9754CDAA381807E6', 
 '2012-04-10 10:21:07.000',True,False,'������� ������ �����������','��� ������� "������"','5249046404','1025200000330','6650404', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tupikina_ea@srbank.ru', 'DBF6E9E45E1C85B71A7AD693BF220540494E2A14', 
 '2012-04-10 10:40:25.000',True,False,'�������� ��������� �����������','��� "������-������" ���� (���)','6829000290','1027739058720','(495)649-3434 (170)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@metrobank.ru', '975F46B75FBB07F006BDDA2FA15B83D7188EAD1F', 
 '2012-04-10 10:48:31.000',True,False,'��������� �.�.','��� "���������"','7725053490','1027739033453','7393705', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'golovach@kremlinbank.ru', '474B02F8A1E4D1A744B935D395ECBEB2802A296A', 
 '2012-04-10 13:09:25.000',True,False,'������� �.�.','������������ ���� "�����������" (�������� � ������������ ����������������)','7706006720','1027739881223','499 241 88 14 (157)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@rsbank.ru', 'EF04136AB0743C40E7A0C0179B77F8307B399023', 
 '2012-04-10 15:05:08.000',True,False,'��������� ����� �����������','��� �� "���������"','3444064812','1023400000964','(8442) 99-50-19', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sidelnikov@novobank.ru', '292849D8074FB532EA6114BDFB09BA45705F97E9', 
 '2012-04-10 16:15:04.000',True,False,'����������� �.�.','��� ��� ��������','5321029402','1025300002925','88162731994', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'titova_ev@doncombank.ru', 'E2A46631637AB4BB718A888DFF2D409DB131A23E', 
 '2012-04-10 16:41:01.000',True,False,'������ ����� ����������','��� "����������"','6164102186','1026100001817','(863) 203-61-94 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mbk@vitasbank.ru', '4EE6199EF870B5EF64A5A30DDE0DF30EEB5A1D3C', 
 '2012-04-10 19:01:26.000',True,False,'������� �����','��� "����� ����"','7716079036','1027739287355','(495)472-04-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rnb@runetbank.ru', 'B28FB4FBDDA9CB2ED7B14300FC158BE90C4F18F5', 
 '2012-04-10 19:20:25.000',True,False,'���������� �.�.','��������� (���)','7724176675','1037711002581','(495)967-66-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Fond-gorbunova@yandex.ru', '01CC01C19E84EFCB5F68FC99DC5964327065E4AB', 
 '2012-04-11 13:51:38.000',True,False,'���������','������� ���� ��������� �������������������','5957819438','1045902203324','8-3425923486', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'udalih.kos@gmail.ru', 'B8035D3FFF5FC67B50A2AB690C42F6730CF50710', 
 '2012-04-11 14:32:50.000',True,False,'������ ���������� ����������','��� �� "��������"','7705917342','1107746379916','(926)915-52-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'finkomuniti@gmail.com', 'FBDF932E0794FA4B132D82DF610A6D8AE4B37A15', 
 '2012-04-11 14:36:17.000',True,False,'������� ������� ��������','��� "������������"','7724779232','1117746122560','(495) 565 11 05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 't.lyubimova@sovbank.ru', '716083175B412F950A09F70214FE56115AB1D655', 
 '2012-04-11 14:59:42.000',True,False,'�������� ������� �������������','��� ���� "���������"','3525024737','1027800000040','8126100450', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'it@donaktiv-bank.ru', '9477FC3EEBF0C932BBC46E49CE16DC4D1C2A1A68', 
 '2012-04-11 16:04:13.000',True,False,'�������� ������ ��������','��� ��� "������������"','6165029771','1026100001839','74952324422', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'smbku@aaanet.ru', '6628CEAD8DE38AB8AEB1AEC2EDE12D517BFBF269', 
 '2012-04-11 16:18:22.000',True,False,'�����������','��� ��� "�����������"','6166003409','1026100002026','(863) 254-81-66', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zaykov@nota-bank.ru', '9DE26704AD7425373FE87893FA7861AD4BF46746', 
 '2012-04-11 16:33:01.000',True,False,'������ ������ �����������','����-���� (���)','7203063256','1027739019000','8(495)2210010', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's_ddd@mail.ru', 'C5E70291104507B7AE98CE8E5073EAC6586BB25A', 
 '2012-04-11 17:23:33.000',True,False,'���������� �.�.','��� "���� �������� �������" (���)','1500000240','1021500000202','88672545212', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'argus@chel.surnet.ru', '75484BB60CE52180B4188CFCF06C4EF1AE41A514', 
 '2012-04-11 18:58:13.000',True,False,'����� ������ ��������','��� ��� "����-������"','7453003965','1037403862143','73519001366', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'advancecapital@mail.ru', '584162F07AA256147A19982D03EC89AD1CF83E73', 
 '2012-04-12 12:01:43.000',True,False,'�������� ������ ���������','�������� � ������������ ���������������� "������ �������"','7606084080','1117606003680','8(4852)59-54-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'knayb_mi-invest@mail.ru', 'FBEC59D7BAE5AB2A90D6C82EBB620FB837032F7D', 
 '2012-04-12 12:48:55.000',True,False,'��������� ����� ������������� ','��� "������������� ����������"','7725206040','1027725006352','89655409507', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@com-investgroup.ru', '9E27E8921AA3C787D4A188E739EB8F79BA8296BC', 
 '2012-04-12 13:09:49.000',True,False,'������ �����������','��� "���������"','7702682413','5087746008510','8-499-681-01-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'OPopova@generalippf.ru', '9408082A8C1F5B90FEB92C2D8E9FF554DCFDB2F5', 
 '2012-04-12 13:34:23.000',True,False,'������ �.�.','��� "��������� ��� ����������� �����"','7744001803','1027739031099','(495) 785-82-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'SOvcharenko@generalippf.ru', '9408082A8C1F5B90FEB92C2D8E9FF554DCFDB2F5', 
 '2012-04-12 13:42:27.000',True,False,'������ �.�.','��� "��������� ��� ����� �����������"','7709323491','1027739236018','(495) 785-82-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sam@avtotransportnik.ru', '4627754E13CCF7BC271206FD4D5E2FE81F8C3580', 
 '2012-04-12 15:27:44.000',True,False,'������ ��������� ����������','��� "��������� "���������������� ������"','2365000772','1052313057543','8616767745', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'DStrokov@gsb.ru', '0E7B6981EB512D7DDB0A126EF2E0B28C2443FC01', 
 '2012-04-12 16:54:01.000',True,False,'������� ������� �������������','��� ��� "������������"','7744000165','1027739224941','(495) 231-27-97 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '54479@mail.ru', '7EC39355AFCC461C8F13FBBAA3E33B8619C4ABC3', 
 '2012-04-12 22:47:37.000',True,False,'������ �.�.','�������� � ������������  ���������������� "��������� �������"','7715780915','1097746679304','89260795610', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'infobris@bris-invest.com', 'DA798A155BB270171FA67FB488981AEFC38FA0B2', 
 '2012-04-13 09:37:10.000',True,False,'��������� ���� �������������','��� "����-������"','7705905795','1097746814912','600-40-80 (216)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@capxxi.ru', 'DF80E9491004A0E259ECFDEA17BC37586D19EE04', 
 '2012-04-13 11:02:56.000',True,False,'��������� �.�.','��� ��� "�������-XXI ���"','7715780866','1097746678600','8(499)246-44-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ofk@agtrade.ru', 'ACE0F84A9428E11788EE6F36050C9EBA69F8A2CC', 
 '2012-04-13 11:38:48.000',True,False,'��������� ���� �������������','��� "��� ���"','7719175411','1037739367050','74956633201', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@argentm.ru', '0BF440CA956D855EDE9B3BE7131D38E8553AD412', 
 '2012-04-13 14:27:02.000',True,False,'���������� ������� ����������','��� ��� "��������"','7726628260','1097746234409','+7(945)988-68-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'anton@ekey.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2012-04-13 15:52:15.000',True,False,'��������� ����� ���������','��� "���� ��"','7709851935','1107746291586','79601860725', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vcabank@astpage.ru', '035F002C663EC9718C07CD8AB5E3B526F1C975AE', 
 '2012-04-13 16:09:34.000',True,False,'������������ �.�.','�����-���������� ����������� ���� (�������� ����������� ��������)','3015011755','1023000000210','(8512)39-29-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ssb@ik-ft.ru', '6371D8A9997950E9AC1718691B62AFAFE32436AC', 
 '2012-04-13 16:20:44.000',True,False,'���� ������ ���������','��� "���-�����"','7722646332','1087746535865','74999951020', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bonurs@bk.ru', 'D7D8BA3677702666D090AD84956498D8605F1E6D', 
 '2012-04-13 18:03:07.000',True,False,'������� ����� ���������','�������� � ������������ ���������������� "������ ������"','7702687404','5087746314739','(495) 648-55-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@cfinteh.ru', '8015A34F5E812AD31E2790B61C8F3C8863B7AED5', 
 '2012-04-13 18:10:01.000',True,False,'������ ���� ��������','�������� ����������� �������� "����� ���������� ����������"','7815024824','1027809225102','499 409-05-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'svksvk@inbox.ru', '9923533D391101327CB17B73BDB249FA3F8096A3', 
 '2012-04-16 09:27:41.000',True,False,'�������� ����� ����������','�� "������" (���)','7716081564','1027739326207','8(499)184-35-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'roman-ul@ya.ru', '79805AD88DE02E1EBEA3494030F7857C92D48FD7', 
 '2012-04-16 10:41:11.000',True,False,'��������� ����� ������������','��� "������� ����"','7328060959','1107328002946','8937-275-24-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oooukstroyinvest@mail.ru', 'A52B4D5E986A4E12DCFA88B7C19C85015EC11201', 
 '2012-04-16 17:35:48.000',True,False,'������ ����� ������������','�������� � ������������ ���������������� ����������� �������� "�����������"','7709734526','5077746553099','89104553143', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tarabukina@tuymaada-leasing.ru', 'C7574E40FFAAAFBEA5922F9ADA149EAB6A6FDA8C', 
 '2012-04-17 04:35:59.000',True,False,'����� ��������','��� �� �������-������','1435203015','1081435005882','358642', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bank_kholmsk@sakhalin.ru', 'D50A2CA9684202D61AC29761CE6D329AF0C6D966', 
 '2012-04-17 08:03:01.000',True,False,'������� ����� ����������','��� "�������������"','6509006951','1026500000141','(42433) 66-130', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bublevich@drg.dol.ru', '0ABF3498984AF536FA106BA7BD03D6DC68F08E91', 
 '2012-04-17 15:39:42.000',True,False,'�������� ������� �����������','��� "���������"','6704000505','1026700535773','8 (48144) 6 83 91', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nbashkirceva@vnov.acron.ru', '5072E75FAE34EEF85F0C5118AD6FE170E6DD993C', 
 '2012-04-17 15:57:43.000',True,False,'���������� ������� ����������','��� "�����"','5321029508','1025300786610','+7(8162)-99-67-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'abfn@pochta.ru', '43D6152E05270A1CAC5687EB8328E40DCFD8A4BB', 
 '2012-04-18 10:57:29.000',True,False,'����� ������� �������������','�������� ����������� �������� "������� ������"','7726301434','1027739216207','+7 (916) 459 9754', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kochetova@radian.ru', '9D50EB8F22A2F83BDCCF92000B2AE46B4177262E', 
 '2012-04-18 12:53:36.000',True,False,'�������� ������� ����������','��� "������" (���)','3810006800','1023800000014','(3952)25-10-44(252)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bobylev@baikalinvestbank.ru', '8D61CB99C11C4C7DD643796BD3543BAB73D44E68', 
 '2012-04-18 13:08:11.000',True,False,'������� �.�.','��� "����������������"','3801002781','1023800000124','(3952)258-807', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Stenina@uralca.ru', '002D06CFA11FE4F99A333179EECA09C186EB2C3F', 
 '2012-04-18 13:36:49.000',True,False,'C������ ����','��� "�� �����"','6672340270','1116672011334','9(343) 227-00-42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'garnik@pshb.ru', '4175C086881E85718FDA763726F7704D428F7DC4', 
 '2012-04-18 14:54:16.000',True,False,'������ ������ ����������','��� "���������������"','6449011425','1026400002012','8 (495) 780-42-96 109', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edo@mfund.ru', '06E754E813B19E1D9D23DCCA09CD3CF26D306A87', 
 '2012-04-18 17:00:02.000',True,False,'�������� ������ ������������','�������� � ������������ ���������������� "�����" ','7734639227','1107746534059','8-985-211-18-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'novikova@rus-helicopters.com', 'F74E9F412A92BF0DEB73C372A31DF2CF5C0C9C69', 
 '2012-04-19 13:09:26.000',True,False,'�������� ������� ����������','��� "��������� ������"','7731559044','1077746003334','(916) 449-0644', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ospiridonova@fpb.ru', '66516D45DB04ABB3F3F2CDB5E80812B7874BAC4C', 
 '2012-04-19 14:37:18.000',True,False,'����������� ����� ����������� ','����������� ������������ ���� "���������-������������ ����" (�������� ����������� ��������)','7707077586','1027739174759','+7(495)7300333', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alexnext@chemexim.ru', '11CA8AC4F3A29F4D8DF5E61A2098AFB3206B91A3', 
 '2012-04-20 14:47:06.000',True,False,'�������� ������� ������������','��� ������������ ���� "������������"','7719048170','1037739700613','79265937005', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aksenova_ep@sberinbank.ru', '4A6B6C8E1541C4C25F4339B990C76BF7F5083DFD', 
 '2012-04-20 17:28:30.000',True,False,'���������� �����','��� "��������������"','6608001457','1026600002065','8(343)379-22-94', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dengi-vofm@mail.ru', '5DFF3E085DD5D842A7F50C383453705AB9C08860', 
 '2012-04-23 13:24:58.000',True,False,'������� ������� ����������','��� ����������� ��������� ���� �������������������','3662169325','1113668042179','89102815500', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@trustfinance.ru', '302F4123B44CA4058E8EE4ED7FE35A58F8760BB4', 
 '2012-04-23 18:14:31.000',True,False,'������ ���������','�������� � ������������ ���������������� ����������� �������� "�����������"','7728616292','5077746454000','8(495) 739-87-52 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yg.burceva@mrsk-volgi.ru', '8E3E50F93A12D9D6A4A55DF07180001D07A8B185', 
 '2012-04-24 11:51:40.000',True,False,'������� ���� �����������','�������� ����������� �������� "��������������� ����������������� ������� �������� �����"','6450925977','1076450006280','(8452) 30-24-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@dkolimp.ru', 'F843EEBCECD6D5488F646D0BED28BEC505C7E73E', 
 '2012-04-25 14:01:27.000',True,False,'������� ������� ���������','�������� � ������������ ���������������� "������������ �������� "�����"','7728756250','1107746966986','+7(499)346-3864', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'inbox@igatlant.ru', 'B2DC50E4F90ED872A089A096084AC33E91C1E4A5', 
 '2012-04-26 13:45:25.000',True,False,'�������� ������ ����������','�������� ����������� �������� "�������������� ������ "������"','7724285995','1037724052519','+7(495)926-86-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'usmanova@azkapital.utk.ru', '3D8F8D58FA8BD1B99FA87ABAEEADCF6400113C07', 
 '2012-04-26 13:50:32.000',True,False,'��������� ����� ���������','��� "����������� ��������"','6659013965','1026602948404','(343) 365-37-27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'MeschaninovaMA@trcont.ru', '585FB8DE7C727633F2865DD5EE7E70341AD3A225', 
 '2012-04-26 14:26:06.000',True,False,'��������� ����� �������������','��� "��������������"','7708591995','1067746341024','89168367285', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oksanamikrofin@yandex.ru', 'BEF131E0ADF0919C96BEE265E246CCDE02CA1150', 
 '2012-04-27 11:05:25.000',True,False,'����� ������ ����������','��� �����������','4401129295','1114401006444','89109585544', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'op-npf@yandex.ru', '88591E8535B488EF920407D6218EF6680C65BB33', 
 '2012-04-27 11:28:14.000',True,False,'���������� �������','�� ��� "������������ �����������"','7712031890','1037739428870','74959411857', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'eniseikredit@mail.ru', 'B13BB3C0250EDD3CED1B6F42D36F80676C2FAD41', 
 '2012-04-27 11:43:57.000',True,False,'������ ��������� ��������','��� "������������"','1902023689','1111902000308','8(39042)22410', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mgrebenyuk@eastkom.ru', 'D908C8742B49C1401C0D88D304C0BCECFF2F2357', 
 '2012-04-28 12:15:27.000',True,False,'�������� �.�.','��� "�� "��� �������"','7744001539','1037739732073','+7 (495) 989 - 10 - 53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'globus@aviel.ru', 'DF80E9491004A0E259ECFDEA17BC37586D19EE04', 
 '2012-04-28 17:22:30.000',True,False,'�������� ����� �����������','���"��������"','5040071688','1065040035961','4964638811', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kislyakov_v@mail.ru', 'F87E91548F5A0CF098D946E254FD2A73F232E35B', 
 '2012-04-28 17:40:29.000',True,False,'�������� ������ ���������','�������� � ������������ ���������������� "����������� �������� "������������"','2309128391','1112309004213','8-928-667-02-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond@blagovestfond.ru', 'B6B7FEB0FF507012E5818CFF7D75841978A1F077', 
 '2012-04-30 17:06:02.000',True,False,'������� ������� ���������','��� "���������"','6231021143','1026201264242','8(495)7923933', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'VGorshkov@mrsk-ural.ru', '55A458B1C8681D2F9991B11615D85EFC1ECAC47B', 
 '2012-05-02 09:55:25.000',True,False,'������� ������� ���������','��� "��������������� ����������������� ������� �������� �����"','6671163413','1056604000970','(343)215-26-67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aleksey.malakhov@ladacredit.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2012-05-02 11:09:04.000',True,False,'������� ������� ������������','��� �� ����-������','6320013240','1026300003465','8482 34-78-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tsbu@mail.ru', '08CA1D02117DF00AB0599F865C607E24C7AFE67E', 
 '2012-05-03 17:19:19.000',True,False,'����� �.�.','���� "���"������ ������"','7729019147','1037739455159','8(985)960-4460', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@zaim.pro', '9392AFA5EFF6CAEFC38FCBE65D4440C1975F17C2', 
 '2012-05-04 11:52:22.000',True,False,'�������� ������ �����������','��� "����� ����������� �����������"','3015093638','1113015003507','8(8512)444-555', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sakovskaya@feib.ru', 'AA874FA7308A9221C769FB887BC8BFD0A6C6F880', 
 '2012-05-04 12:53:46.000',True,False,'��������� ������� ����������','��� "������������"','7705038550','1027700514049',' 645-36-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lider@obninsk.com', '36BEDF5C769410330C20F3CB72F05C4E9769677C', 
 '2012-05-09 02:01:11.000',True,False,'������� ����� ��������������','�������� ����������� �������� "�����"','4025047713','1024000941755','8-(48439)-403-55', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@bpjsc.ru', '720E66DAB8FBDCEB5ABFAA9B485C48DBC376122B', 
 '2012-05-10 10:14:49.000',True,False,'���������� ������ ���������','��� "������ �������"','7709880326','1117746507537','(495) 540-540-8', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@investfc.ru', '5B65D7B0A8305F62AC2D62222E0C6369FDF3936B', 
 '2012-05-10 15:34:48.000',True,False,' �������� �������� ������������','�������� � ������������ ���������������� "�������������-���������� �����"','7703753723','1117746783440','89035346791', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pomozhem@aliansfin.com', '68E21D73780A32EBCC62A166AC9E79B6222F30C5', 
 '2012-05-11 10:52:29.000',True,False,'������ ������� �����������','��� "������-2011"','7453230460','1117453004437','89080802267', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rostafinans@rambler.ru', '5432F620226A71B83CA8276C9963AAC641C3F0B7', 
 '2012-05-12 10:36:51.000',True,False,'������ ������� �������������','��� "�����������"','6164306415','1126164001303','(863) 203 58 59', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'specbitmontaj@bk.ru', '65590EDD19D5C37C2083A1550AA20CBC95C83150', 
 '2012-05-14 11:36:30.000',True,False,'������ ������� ����������','��� "�������������"','7706013886','1027739754074','(499) 238-32-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'shitimech@bk.ru', 'D1DAA93F58B75C25A9F92AC659ABA030321329E2', 
 '2012-05-14 14:07:50.000',True,False,'���������� ��������� ���������','OOO "��� � ���"','5921020306','1065921023189','83425638183', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'management@fdom.ru', '17AD171BA2BA4A6451178D14C489BB76A09D067E', 
 '2012-05-14 14:25:42.000',True,False,'������� ����� ���������','��� "�� "���������� ���"','5902837519','1075902004606','+7 (342) 235-17-97', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@stolichnayamc.ru', '7F529FFB2A329C9C1E508BBEA3B25F2ADC7A214A', 
 '2012-05-14 17:09:28.000',True,False,'�������� �.�.','��� "��� ���������� ��������"','7725600047','5077746308140','(495)280-73-67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@itcmsk.com', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2012-05-14 18:07:34.000',True,False,'�������� ����� �����������','�������� � ������������ ���������������� "����������� �������� "������ �������� �������"','7701890820','1107746769822','495 678 10 42', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'maxim@sbercred.ru', 'EE6FD69D113483FAB29AC8804E22855D15307B50', 
 '2012-05-15 11:20:37.000',True,False,'������� ����� ����������','��� �������� ����','7734096330','1037739299685','785-54-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'batodugarov@gmail.com', 'C01293CDD287515600C4CD717B2E833C0D32D265', 
 '2012-05-16 10:35:17.000',True,False,'������� ���� ��������','��� "����"','315990182','1120327003610','898-333-0000-1', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'orellift@gmail.com', '0FFC9FC5C3DD1A3600DDAC06CBF12A337A7CAA83', 
 '2012-05-17 09:05:17.000',True,False,'������� ������� ��������','��� ����������','5751023466','1025700765221','8(4862)557582', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yur@fetr.org', '0D35FF1CEDDAAC6E919B73C8C4A2542EC8BF9B2F', 
 '2012-05-18 10:10:04.000',True,False,'�������� ������� �������������','��� "������������� �������� �������"','5005001180','1025000922077','4964420201', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'straj@info.ryazan.ru', '3E316B080FF616395FE796C328E848F43842E65D', 
 '2012-05-21 11:20:50.000',True,False,'�������� ��������� �������������','��� ��� "�����"','6234097920','1116234012718','(4912) 25-61-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@hmnpf.ru', 'D6185CA258ECFD9756242F85EB1E9CF3EE8EFC08', 
 '2012-05-22 08:16:57.000',True,False,'�������� ����� ������������','�����-���������� ����������������� ���������� ����','8601010255','1028600512270','(3467) 354438', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'konto@mail.wplus.net', '7474FC84A1F3796D18DA16E2E0C74905307068DB', 
 '2012-05-22 12:13:46.000',True,False,'������� ������ ����������','��� "�����"','7805029742','1027809186745','(812)272-02-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'shentmz@yandex.ru', 'D4F1222A2EDC9875ECDD991176EE467D67BAE062', 
 '2012-05-22 19:43:50.000',True,False,'��������� ����� �������������','��� "�������� �������"','1609008093','1031639802677','89375232744', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dudinka.kbu@mail.ru', '7851528C41EAED6B626DEC0D8373792B787100AC', 
 '2012-05-28 10:32:41.000',True,False,'��������� ������� ���������','�������� ����������� �������� "�������� ������� �����"','2469001700','1112469000027','73919131801', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Elena.Klemesheva@chelpipe.ru', '2E893D1DBD8B51947A531B2B1058B48E4B1202EA', 
 '2012-05-28 11:43:33.000',True,False,'��������� ����� ��������','�������� ����������� �������� "����������� �������������� �����"','7449006730','1027402694186','(351) 259-03-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Annvladim@mail.ru', 'BE7D9D9FF205B895AFF9A006E61F1F54EE6B5EDC', 
 '2012-05-29 10:58:03.000',True,False,'��������� ���� ������������','��� "��� �������"','1660135052','1101690005284','89003237009', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ic-trend.ru', '9048226EA503D8CF618AA32A81B349D7745518C7', 
 '2012-05-29 13:00:05.000',True,False,'��������� ��������� ����������','��� �� "�����"','7708733569','1117746074027','+7 (495) 988-39-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'annadovgalenko@gmail.com', '506BF0957E3072B875DE0EF06F30EF68E12C36CF', 
 '2012-05-29 16:41:24.000',True,False,'����� ����� ����������','��� "�� "����"','3528122838','1073528004220','(8202) 57-97-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@factoring-media.ru', 'C0406821ABF539BDEFBBEA8D38725A5D6F2A4C44', 
 '2012-05-31 13:45:56.000',True,False,'������� ����� �������','��� "��������� �����"','7722745358','1117746315136','(495) 501-76-09', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sk.n2010@yandex.ru', '94179D306638B6AE7E01F595B90141D815A85E2D', 
 '2012-06-03 16:16:56.000',True,False,'���������� ������� ����������','��� "������� ����������"','6154082903','1026102576444','(8634)313-879', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'i.kuznetsova@asbconsulting.ru', 'B33DC43D5A5D1FDBD256201A8D08A496062DA53D', 
 '2012-06-05 18:03:50.000',True,False,'��������� ����� ����������','��� "���� �������� �����"','7744000430','1037739697357','89104532732', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'serg-finam@mail.ru', '52518D20D8DED36706ADB4E411E675E561DEB741', 
 '2012-06-06 10:09:11.000',True,False,'�������� ������ ��������','�����','1659064819','1061684095109','8-917-247-11-43', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'muminat77@yandex.ru', '59D400FC6D028EF59B5CA0E9E885CBE2D5528EE5', 
 '2012-06-06 17:35:29.000',True,False,'����������� ������� �����������','������������� ���������� ���������� ������ �������� "���� ������������������� ��������� ������ � �������� ������������������� ������ ��������"','545023077','1090545000743','89285752656', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@extratrade.ru', '7570D4D5408DA6FA10ED919BCB2A9050C8D16433', 
 '2012-06-07 13:57:41.000',True,False,'���������� ����� ��������','��� �� "������ �����"','7705919340','1107746450921','+7(495)646 49 62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@royal-trust.ru', '8DC33AFD30FFC4D88AFE129D6F342A9100797548', 
 '2012-06-13 16:53:47.000',True,False,'�������� ���� ����������','��� "���� �����"','7725712061','5107746053333','74957400628', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@atlantdu.ru', '2DE65BD443AEAAE0B4757FA5830C9A3B718B0136', 
 '2012-06-18 14:36:34.000',True,False,'������� ������ �������','�������� � ������������ ���������������� "�������������� �������� "������"','7721634905','5087746234747','+7 (495) 692-66-66', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alex1-333@mail.ru', '78A389CA47A7C81F3F48AC6A02C8CCC094C25736', 
 '2012-06-21 14:23:20.000',True,False,'�������� �����','����-2','3201000302','1023202740330','8-953-277-68-90', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rysha.07@mail.ru', '7A59D4A8F4876DAFBB160B62F680B487EE50D985', 
 '2012-06-22 14:36:05.000',True,False,'��������','����-����','259007170','1030202233291','89174777216', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zfo@mail.ru', 'E142C0CB97F3625211A9F28A7C6FC7ED26C0AC6E', 
 '2012-06-22 14:43:29.000',True,False,'������ ���� �����������','��� "������ - ������"','548001681','1020502331860','89034293902', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dianagru@yandex.ru', 'CEBF417ED3F0C3D6AB2BC3C79744BA757D2A9002', 
 '2012-06-24 20:28:43.000',False,False,'������� �.�.','��� "����������"','6234078589','1106234002698','89105649009', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'microcredit@list.ru', '886E7AFA52FFE25B8E3E21B4DBE3A2FF4CAD3498', 
 '2012-06-26 16:35:47.000',True,False,'��������� ������ ��������','���" �����������"','1655192859','1101690025964','89270390960', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pkfkran@yandex.ru', '63EA075B55165AE723237B832196DF043C40C288', 
 '2012-06-27 09:48:48.000',True,False,'������ ���� ���������','�������� � ������������ ����������������  "��������������� ������������ �����  "����"  ','5904201662','1095904000653','(342) 249-77-69', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'GARANTFINKOM@gmail.com', '25F50042C3C238C22BA6ACBC2DADBA1DF37EE1B3', 
 '2012-06-29 11:46:46.000',True,False,'���������� ������� �������','�������� � ������������ ���������������� ����������� �������� "������ ������"','7838470102','1127847041332','(812) 332-26-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'admin@zlatkgh.ru', '165D57F569CBF00229473214732234776313FC92', 
 '2012-07-02 13:10:14.000',True,False,'������ ���������� ����','�������� ����������� �������� "������� ���������� ���������"','7404050987','1087404001574','+7 (3513) 63-31-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'expert@expert-rk.ru', 'FF2993195330FB94D5BBBD743EE2BE4C5A513295', 
 '2012-07-04 12:04:12.000',True,False,'�������� ����� ����������','���" �������"','4442001135','1024400522068','4942372237', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stosha2108@mail.ru', '6AEB146C6F10C1E2C15CE5B7BC05131D511EA678', 
 '2012-07-05 12:30:42.000',True,False,'�������� ����� ������������','��� "�����������"','1105021754','1111105000412','8-9125522899', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ukperspektiva.ru', '133EC4CD8FA6F27F553BCFB06B25B547159BE6C6', 
 '2012-07-05 15:50:31.000',True,False,'����� ������� ����������','��� �� "�����������"','7743787574','1107746584406','(495)660-88-16', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ilya@metropol.ru', 'D5E5EA8DBF3362A70AC9B687EAFBD8B0BF884966', 
 '2012-07-05 16:22:45.000',True,False,'������� ���� ����������','�������� � ������������ ���������������� "�������������� ���������� �������� "���������"','7704036711','1027739083130','(495) 933-33-10 (2010)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ic-nfd@mail.ru', '95FFAF33A6A27BBB74110582C6C8C811F0F8A4C3', 
 '2012-07-11 17:47:59.000',True,False,'������� ������� �������','�������� � ������������ ���������������� "�������������� �������� "������������ ���������� ���"','7734672859','1127746051927','(495) 798-74-32', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'solov@runabank.ru', '08475525EBC8219803BC9D634AB0925287E6C838', 
 '2012-07-12 10:15:16.000',True,False,'��������� ����� ����������','��� ��� "����-����"','7701041336','1027739295968','4952233440', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'introshina@yandex.ru', 'EE424CDE847F49383CB14704A9CBF52E0B889985', 
 '2012-07-12 16:49:21.000',True,False,'������� ���� �������������','�������� ����������� �������� "���������"','8622023150','1128622000968','8-950-531-6513', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'csb@invb.ru', '34D573D7703C5C2F3C01F025329E2C4E788BDDDD', 
 '2012-07-16 17:20:23.000',True,False,'��������� ������� ������������','��� "��-����������"','106000547','1022300001811','(861) 259-52-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aw@agana.ru', 'DA917EF3193FC746E035EC80827DEE56543DE0AF', 
 '2012-07-18 11:51:15.000',True,False,'���������� �. �,','��� "�������������� ���������"','7718782484','1097746686510','74959801331', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vv-financeservice@mail.ru', '2B8AD8880BEE5E99440E1E5EAAA2EC39D7076B6A', 
 '2012-07-18 12:09:13.000',True,False,'�������� ������� ������������','�������� � ������������ ���������������� "��-������������"','7720748321','1127746304388','(926)8329748', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'partners@cryptostandart.ru', '8238498C7517F122BFBE8D9CDF6219E2C3739BC6', 
 '2012-07-20 12:31:57.000',True,False,'������� ���� ������������','��� ��������������','6163100972','1106195001494','(863)219-00-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zao@proksi.ru', '67866AB319310E9F8651AB6BBF7FF834D4E6407A', 
 '2012-07-20 15:20:50.000',True,False,'������ ����� ��������','��� �������������� �������� ������','7701000971','1027700550030','8-495-681-93-58', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mv_kuznetsova@mastbank.ru', 'CC0947B729B5AC306430EEC0E576F99FEE95FBCC', 
 '2012-07-24 09:56:56.000',True,False,'��������� �.�.','��� �� "����-����"','7744001761','1027739199124','8(495)221-55-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'newlimit@mail.ru', '98802E5393720F986D0812F2005667E8700AD07C', 
 '2012-07-24 14:14:04.000',True,False,'������ ������ �����������','��� "��� "��������"','7743065177','1027743012890','+7(495)450-12-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pcrb_privat@mail.ru', 'C5C6A2C139EE6E82AB7799490E6FD817C9906B82', 
 '2012-07-25 10:28:17.000',True,False,'��������� ��������� ���������','��� ��� "�������� ����� �������� �������"','5904988697','1125900000093','(8342) 240-28-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@e-xp.ru', '6826D1E5D85AC2EFCBD3326D88EC9EF08C59FD43', 
 '2012-07-25 14:39:31.000',True,False,'������ ����� ����������','��� "����������"','7447133821','1087447008550','79322310110', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@fk-raduga.ru', '4532135EB7ECD0D086C46828C75517BCDF5565A6', 
 '2012-07-25 17:22:57.000',True,False,'������ ������� ����������','��� "��"������"','7719764671','1107746941268','89262470859', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'SvetPro@rdb.ru', 'FBEC59D7BAE5AB2A90D6C82EBB620FB837032F7D', 
 '2012-07-30 09:28:34.000',True,False,'�������� �������� ����������','��� "����������"','7718011918','1027739857958','(495)276-00-22 (2412)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'finmarket@udm.net', '02669E50D6CFAB6319C79EE3D16C280F5117A66B', 
 '2012-07-31 10:38:35.000',True,False,'��������� ��������� ��������','��� "���������"','1839003276','1101839000526','83413943408', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kayros@bor.kamchatka.ru', 'CC60683B4FC648A0DE847AF7026BF0291656D059', 
 '2012-08-02 02:05:12.000',True,False,'������� ����� ����������','�������� � ������������ ���������������� ��������� �������� "������"','4101024245','1024101017753','(8 4152) 21-83-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gmz2013@yandex.ru', 'F398671690B8E9D1AFB517F6AED859818CD01485', 
 '2012-08-03 09:23:54.000',True,False,'�������� ��������� ������������','��� ��� "���������������"','4802000820','1024800524044','(47461) 2-27-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lbelyaev@eldokument.ru', '2FCE5E070AC3D8E62208A253E8C32285C94B03EA', 
 '2012-08-03 15:37:55.000',True,False,'������ ������ ����������','�������� ����������� �������� "������������ �������������� ����� ������������� ����������� ����������� �������� �������" (��� "��� ���")','7743767874','1107746036991','8(495)787-60-50 ( 8 916 6058239)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sibirskizaim@yandex.ru', '30E1E8C1879D7BFA98C6D9E8E5AD5996F4FF4BB4', 
 '2012-08-04 09:40:09.000',True,False,'������� ���������� �','��� "��������� ����"','4217144090','1124217003800','8-906-927-56-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nikita_umnov@rgs.ru', '8397E135A4D8B7C5CC5B879A879DB7E6BFE97234', 
 '2012-08-07 10:10:27.000',True,False,'����� ������ ���������','��� "�����������"','5027089703','1025003213641','(495)783-24-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alex@cib.ru', '49C9ADEB420D3EC99D4F43B4DE3C899C26FEC551', 
 '2012-08-07 13:36:21.000',True,False,'������� ����� ����������','�� "������������ ����" (���)','7730030466','1027739441014','+7(495)234-95-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gardian.ic@gmail.com', '4F1251E6BE19F1545F583E0EEC6E22FE141F321B', 
 '2012-08-07 14:55:34.000',True,False,'���� ������ ����������','�������� � ������������ ���������������� �������������� �������� "�������"','7706774182','1127746359157','+7 (495) 669-95-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gusevatv@pragmatic-am.ru', '691481BE4E82AC86D730FFED94C18F9537C6F2C6', 
 '2012-08-08 12:33:55.000',True,False,'������ ������� ������������','�������� ����������� �������� ����������� �������� "���������"','7709811393','5087746563780','+7(495)626-82-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yurkinat@feib.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2012-08-08 16:21:19.000',True,False,'��������� �����','��� "�� ������������������"','7717586529','5077746555024','(499)242-16-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.vrk3@yandex.ru', '769EA1720AB889025228E7E94FE651C855910A18', 
 '2012-08-09 10:07:59.000',True,False,'��������� ���� ����������','�������� ����������� �������� "�������� �������� ��������"','7708737500','1117746294115','84992603211', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@fortisinvest.ru', '94510AEEB02CAF6A8C99E79EE8B731D558D32DCF', 
 '2012-08-09 11:55:44.000',True,False,'��������� �.�.','��� "�� "������-������"','7707757447','1117746702325','(495)739-62-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'isaeva@rfabank.ru', 'B82DA9F33816559FC849D5C1ACA3CEFA75887853', 
 '2012-08-09 14:02:16.000',True,False,'������ ������� ������������','��� ��� "���"','901001024','1020900001770','84955306000149', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dobjanskiy@yandex.ru', 'BFC890149A7CC28544C1591582186AFC33E6559E', 
 '2012-08-10 07:38:26.000',True,False,'������ ��������� ����������','��� "��� ������"','2466251660','1122468030167','89029276146', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Larisa_Kuricina@mail.ru', '820EA80AAF9267E54F87EB1802AC354D74C9F908', 
 '2012-08-13 15:17:25.000',True,False,'�������� ������ �����������','��� "�����"','6676000966','1126676000747','(34360)32236', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ant774@yandex.ru', '752986CE04D0B0DF5140D7C5823186AF3DB576FA', 
 '2012-08-13 21:35:36.000',True,False,'������ ����� �������','��� "����"','5029163409','1125029005276','8-495-211-71-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fedosovmu@arsagera.ru', 'DE58CF73732601BD6C83D89E17C0B82ED7740014', 
 '2012-08-17 12:27:13.000',True,False,'������� ������ �������','��� "�� "��������"','7840303927','1047855067633','(812) 313 05 30', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@investural.ru', '658571717B5FB1B03520283A3371FB64345179B8', 
 '2012-08-17 14:20:27.000',True,False,'������ ������ ��������','�������� ����������� �������� "����������� �������� "�����������"','6672190931','1056604452497','8-343-359-44-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'max669@bk.ru', 'B35DA586011762814ABE4AD55B59DC8318668E18', 
 '2012-08-17 16:03:52.000',True,False,'������� ������ ����������','��� "���"','5256103607','1115256004412','89200791693', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'antal60@inbox.ru', 'A045541A5403C5E490A24228DBD90E4D9F9BA280', 
 '2012-08-20 19:49:21.000',True,False,'������� ������� ����������','�������� ����������� �������� "������������ ���������� ��������"','7722606844','5077746436411','8-910-454-59-85', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'inovikova@univer.ru', '7E86192F27F0DA19FB48CEB245FBA5CEC49D6D6E', 
 '2012-08-22 18:11:55.000',True,False,'�������� ����� ����������','�������� � ������������ ���������������� "������ ����������"','7722268930','1027722004595','+7(495)7925550', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ulia-d@taxnet.ru', '7515DB681C0AFADA2EA310869C7073F393DF337A', 
 '2012-08-23 11:56:56.000',True,False,'�������� ���� ����������','��� "�������"','1655045406','1021602855262','8432319225', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'novcr@mail.ru', '70558AD1FA71267C1D5DBFF1B40FF8F1A2662949', 
 '2012-08-24 09:24:17.000',True,False,'������� ���� �������������','��� "������������ ��������� ����� ������������"','5321122151','1075321008586','8-963-330-68-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@nomos-sibir.ru', '5CA278119A311CDE146A6C362B0087493E474A63', 
 '2012-08-28 08:17:50.000',True,False,'�������� ����� ������������','��� "�����-����-������"','5405158186','1025400003420','(383)2103295', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@newtu.ru', 'B903E84CC86C7D0AE552186D93F58AA0AB018BB7', 
 '2012-08-29 13:13:40.000',True,False,'��������� ������� ��������� ','�������� ����������� �������� "����� ���������� ����������"','7705919149','1107746444046','+7 (863) 268-90-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'spectrumcap@yandex.ru', '46D8382D271717A59212E5E1662AB7A1CDD7A4E4', 
 '2012-08-29 17:36:36.000',True,False,'������������� ����� ����������','��� �� ������-�������','6163105410','1106195009865','74955087762', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dpotok.ru@mail.ru', 'DA504F956413327D8DC56AF6FA2EEC285869DAFB', 
 '2012-08-29 18:19:32.000',True,False,'�������� ������� ����������','�������� � ������������ ��������������� "�������� �����"','2453016838','1122448000586','8(913)5100789', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'investkavkaz@mail.ru', 'F3C3BC23EA06B1F945D188D153525FE37EC57AE7', 
 '2012-08-30 10:32:17.000',True,False,'�������� ������� �������������','��� "������-������"','2632803648','1122651009194','8-(87951)-6-00-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Gavrilova_NB@dccapital.ru', '683F0504C19A38DD60632853FE97A706FFA6FDA5', 
 '2012-08-30 15:32:16.000',True,False,'��������� ������� ���������','��� "�� "�� �� �������"','7718585341','1067746574147','(495) 988-26-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a@tatspirtprom.ru', 'BBE6EC9C15809EB512363AFDB78F209E616CCEB5', 
 '2012-08-31 16:13:22.000',True,False,'����������� ����� ���������','��� "������������"','1681000049','1041626847723','567-34-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kopylov@autotracker.ru', 'DC8CC73CB9A50456182AFF409C1B86483A0F1C55', 
 '2012-09-01 01:20:40.000',True,False,'������� �������� ����������','�������� ����������� �������� "������� ������������� ����������"','7722558855','1057748610996','89153858823', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'viktoriy-2509@mail.ru', 'A42ECEDF61417970F6A23F4499769E37C110431B', 
 '2012-09-05 11:50:13.000',True,False,'�������� �������� ����������','��� ���','3127008599','1103127000151','9056793058', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fincom.ooo@yandex.ru', 'A77B68538230F69AB5A0EF70D3C69FAB3B9E8D33', 
 '2012-09-06 12:11:45.000',True,False,'�������� ���� ����������','��� "������"','1841025894','1121841003613','89199123621', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ira.ira377@yandex.ru', '7ECA336BFFF775F0F54669BF5A983BAC522AE66F', 
 '2012-09-07 16:05:23.000',True,False,'������� ����� ��������','��� "������ �������"','2224154593','1122224004154','89132180038', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'am-business@mail.ru', '959AB7B3B440C3901FB1C4A788B5CF70CA9E9ABD', 
 '2012-09-10 10:29:39.000',True,False,'������� ������� ������������','�������� � ������������ ���������������� "����������� �������� "������ �����"','7704750235','1107746237829','74993720490', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Nash-vek@mail.ru', '74C8CE5B6521F8958DF98D117B37175641C4BE54', 
 '2012-09-11 02:22:20.000',True,False,'������� ����� ���������','��� "��� ���"','4909100095','1084910002924','89148575777', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ibcvelear@yandex.ru', 'A85760779AA1508E62648A68E5E11B9371814312', 
 '2012-09-11 17:02:04.000',True,False,'������ ������ ��������','��� "��� "������"','6454092290','1096454000686','79033284705', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ukbpt.am@gmail.com', '5E82303EB8C5D92FD3D2C166E2C4C7AA6E1B3C21', 
 '2012-09-12 10:05:14.000',True,False,'���������� ������� ����������','��� �� "������������������"','7724812578','1117746924569','79178732207', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sk-yaroslavna@mail.ru', 'F8A685B1E87F1A3E00B9C67DCCD516EBDC774786', 
 '2012-09-14 11:21:19.000',True,False,'��������� ������� ��������','��� ��������� �������� "��������"','7713080072','1027700397317','8(495)661-77-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ernest.arakhchyan@investflot.com', 'F3F6235C27D379EB60362ECF93121C79689A2964', 
 '2012-09-14 13:05:30.000',True,False,'������� ������ ��������','��� �� "����������"','6317021441','1026301415030','84956376645', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'julia.shumilova@soglasie.ru', '812292E85DE5F3F4F9F26B280DCA2C8B72D0FB59', 
 '2012-09-17 13:28:06.000',True,False,'�������� ���� �������','��� "�� "��������"','7706196090','1027700032700','74957390101', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@rshbins.ru', '7F628D94956A4F1168236D9B59D584D1B4905682', 
 '2012-09-17 14:55:34.000',True,False,'������ ��������� ����������','��� �� "����-�����������"','3328409738','1023301463503','+7 (495) 213-09-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'moskovia@trtk.ru', '13120DBB3770205E821A742A3B1BCE8BCEC110F7', 
 '2012-09-18 09:50:56.000',True,False,'�������� �.�.','��� �� "��������"','5046005297','1025006037495','(495) 777-70-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fog@reso.ru', '5535F11556341F4A0946B4A940FE77549080DE93', 
 '2012-09-18 11:06:17.000',True,False,'��������� ����� �����������','���� "����-��������"','7710045520','1027700042413','730-30-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@kupech.ru', '8C4A08BD20E0D73473680B99BF404767EAD500BF', 
 '2012-09-18 15:33:05.000',True,False,'������� ����� ���������','��� "��������� �������� "����������"','5503029968','1025500734357','+7(495)685-9467', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kassa911.74@gmail.ru', '82DD82E4C666B148911CD976812AF3FCD5A20ECD', 
 '2012-09-19 10:08:02.000',True,False,'���������� ������ ���������','��� "����� 911"','7451342112','1127451012810','8(351)2355000', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mail@ukprivilegia.ru', 'F91779EA73538038D78312CDCA2003AAD0E1C60F', 
 '2012-09-19 13:53:55.000',True,False,'�������� ���� �����������','�������� ����������� �������� ����������� �������� "����������"','7705918145','1107746408340','(499) 391-13-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'evkrasnova@ahml.ru', '03E74035961A4B5AF0DBFE9A79FE119893F31170', 
 '2012-09-19 14:57:42.000',True,False,'�������� ����� ������������','��� "�� ����"','7727709314','1107746041545','(495) 775-47-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Elena.Shtab@kapital-re.ru', '6E840833669605DB7DF59485282C0657A7E5F74E', 
 '2012-09-24 11:50:44.000',True,False,'���� ����� ����������','�������� ����������� �������� "������� ���������������"','814084842','1020800002871','(495) 620-95-34 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'korotkova@skgelios.ru', 'ABE2D41F7B500B10C8BB1134DD75CB5304273B20', 
 '2012-09-27 10:06:01.000',False,False,'��������� �������','��� ��������� �������� "������"','7705513090','1047705036939','8-495-981-96-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tomilina@obrbank.ru', '6FD0DA82E8BE4CDF3491021DEEDFD30C41783BA4', 
 '2012-10-01 17:08:57.000',True,False,'�������� ����� �������','���� "�����������" (���)','7736017052','1027739265355','8-926-584-72-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tkacheva@trustbk.ru', '3F47C5FEE7840F27F2641BFC46F3A3DB93F43A41', 
 '2012-10-02 15:54:26.000',True,False,'������� ������� �������������','��� "��������� ���������� ��������"','7706530891','1047796240513','(495) 646-72-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pure.katty@gmail.com', '71DA656F7CC7B62FD0F6C3E5776F84463544CF5A', 
 '2012-10-03 15:08:38.000',True,False,'��������� ������������','��� "������� ����"','7203267972','1117232038483','79612068888', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'msk@mini-dengi.ru', 'FBC11AA126E9D65CE5E8AE8C7F469B156F6D4EFE', 
 '2012-10-03 19:05:07.000',True,False,'����� ��������� �������','��� "�������� ������"','7839448702','1117847349223','79262952696', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kredit@cmf-kapital.ru', '98EE285B1425E9CACBF29DBDF0F370DA3B8EFABA', 
 '2012-10-06 11:44:52.000',True,False,'���������� ���� ����������','��� ����� ������������������� �������','3408010924','1123455001603','89023843185', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'expressfinance2@mail.ru', 'D01DD23D584C99392C134843F973A66AB7951ACB', 
 '2012-10-08 17:24:06.000',True,False,'������ ���� ��������','��� "�������� ������"','1513001587','1101513001413','88672445333', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk@edelweiess.ru', '82DD82E4C666B148911CD976812AF3FCD5A20ECD', 
 '2012-10-08 18:15:16.000',True,False,'��������� ����� ','�������� � ������������ ���������������� "����������� �������� "���������"','7704764968','1107746770031','+7(495)790-74-12 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aktivdengi86@mail.ru', '13691F6EB657A9AFB4CC041925F0A86BCD1E5183', 
 '2012-10-09 11:50:54.000',True,False,'������� ������� �������������','��� "����������"','8603190959','1128603017707','89222555411', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uc@itk23.ru', '7E0FD82A38BF13DEADE035BB4D4FA7A1555AC3BF', 
 '2012-10-10 12:02:57.000',True,False,'�������� ����� �����������','��� "���"','2310152134','1112310000220','(861)278-7777', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '906777777@mail.ru', 'B20EB2E4A5200A5A146E274A6160F7BB53583B6B', 
 '2012-10-10 15:22:54.000',True,False,'������ ����� ������������','��� "���������"','7702752406','1117746066503','-760265', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nst2007@mail.ru', 'C9FD85CA5C24338ECC6C94DB4CF5AF8EF14F350B', 
 '2012-10-11 04:35:08.000',True,False,'������� �������� ������������','�������� � ������������ ���������������� "��������� �������� "������������ ��������� ��������"','7710353155','1027739198970','4991363994', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk.ik@inbox.ru', 'ECB5262438A5462CEFC41683270C257280F42316', 
 '2012-10-11 10:31:59.000',True,False,'������� �.�.','��� "�� "���������-�������"','4205057615','1034205070195','4952878452', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tanyagolushkova@mail.ru', 'ACF7D453CDDEA374910DE518E395414DE18419AA', 
 '2012-10-11 18:12:26.000',True,False,'��������� ������� ����������','�������� ������ �������������� ���� ������������ "������ ���� ������������ "��� ����������� �������� � ������������ ���������������� "����������� �������� "������-�����"','6454074653','1056405422875','89276264656', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'grand-capital@grand-capital.org', 'DFBBFF1D9AECC81DB833ABD97E1C19E9B49A0BEF', 
 '2012-10-12 11:17:52.000',True,False,'������� ���� ����������','��� "�� "�����-�������" ','6164264807','1076164006270','8(863)269-89-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'filippova@forward-invest.ru', 'D1C7D607EDAC9728A4667831B2B2AFE1F5606C43', 
 '2012-10-12 14:26:09.000',True,False,'��������� ������� ���������','�������� � ������������ ���������������� �������������� �������� "�������"','7728645215','1087746006040','(495)437-15-67', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'atlas-capital@yandex.ru', '9E4A2FA6998B26BA8718CC55FF09D4ADEF1F8CB2', 
 '2012-10-12 14:31:50.000',True,False,'��������� �������� ����������','��� "����������� �������� "�����-�������"','7726571039','5077746993495','(495)633-75-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nsokolova@rosinter.ru', '3618EEE15E08F488B1240439071D596485136469', 
 '2012-10-15 16:15:37.000',True,False,'�������� ������� ����������','�������� ����������� �������� "�������� ���������� �������"','7722514880','1047796362305','(495) 788-44-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf169@npf.avtograd.ru', '4B7F39E736B9E28E689A63BAE64FAAFD5E8DF877', 
 '2012-10-18 10:29:14.000',True,False,'������ ��','��� ��� �������','6320008715','1036301014562','8 (8482) 51-77-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'farmakom@list.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2012-10-18 13:06:33.000',True,False,'�������� ����� ����������','��� "��������"','5504029590','1025500987324','370-234', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'urist@slavdvor.ru', '9D0819F11F185FF5044A53B3A6CB0348AED06434', 
 '2012-10-18 13:22:54.000',True,False,'�������� ����� �������������','�������� � ������������ ���������������� ����������� �������� "���-�����������"','7715665599','1077760004354','79519074413', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'orlovatv@nig.ru', 'B36A0D2A5DCFBD5CD64E8D4EB40BB5969FB42ECE', 
 '2012-10-19 10:00:25.000',True,False,'������ ������� ����������','���� "������������ ��������� ������"','5008018432','1025001202148','8(495)788-33-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk-cam@mail.ru', 'BB8525C210B1233B75B3C5808C883888C2C0D437', 
 '2012-10-19 14:44:10.000',True,False,'��������� �������� ������������','�������� � ������������ ���������������� "����������� �������� "���� ����� ����������"','7702790063','1127746400946','(3462)774717', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'grandlangepas@gmail.com', 'BA270B8A10641843710E015B1363B76652AF1064', 
 '2012-10-21 16:39:56.000',True,False,'������ ������','��� "�����-�"','8603167318','1098603006853','79227790101', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@avtotorgbank.ru', 'AA4AD13DC35DD155D1CA6D569DBFAB86FA6A9F4E', 
 '2012-10-22 14:11:47.000',True,False,'��������� �������� ������������','��� "������������"','7727038017','1027739408290','8 (495) 730-51-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Larina@pib.ru', '26F4F52E2E8E993199ED162D6E94D4379324EA34', 
 '2012-10-22 14:26:41.000',True,False,'������ ����� ����������','����������� ������������ ���� "�����������-�������������� ����" (�������� ����������� ��������)','7734052372','1037739297881','(495) 969-29-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gabdulkhakova@insurinvest.ru', '56F5F2ECB08A639D7A824C84024FD5BE0DAE43AC', 
 '2012-10-22 15:07:34.000',True,False,'������������ ����� ����������','��� "��������� ����������"','7728306117','1037728057762','73472912615', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'custody@florabank.ru', 'B9EFBB59BD879B5F51E5A25477D724073ADA335D', 
 '2012-10-23 17:21:38.000',True,False,'��������� ������','�������� ����������� �������� ������������ ���� "�����-������"','7744000197','1027739082425','(495) 2340154', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'd.grudcyn@rublev.ru', 'D03C11BE94D774B1EDCED24BF85B720DEF48B038', 
 '2012-10-24 11:21:30.000',True,False,'������� ������� ����������','�������� ����������� �������� ������������ ���� "������"','7744001151','1027700159233','4959335848', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kat387@yandex.ru', 'C9F12B2C28D4F1E5D74E195F54CC6F661D3ACF9E', 
 '2012-10-25 09:10:51.000',False,False,'������ ��������� ����������','��� "���������"','6674368458','666666666666','79126347270', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'MolyakovaSE@frescobank.com', 'F3D44F65EEB7D39006875C8BB09F287299BDB2D1', 
 '2012-10-25 12:01:06.000',True,False,'�������� �������� ����������','������ ���� (���)','7604011640','1027600000152','+7(945)775-86-56', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sergeeva@tembr.ru', '6F1BB4C7A537BDEB44FF0EFE0C9EF6D68C9D3F7A', 
 '2012-10-25 16:00:53.000',True,False,'�������� �������� �������������','��� "�����-����"','7707283980','1027739282581','8-495-363-44-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'o.pushaeva@solidbank.ru', 'C4877730244CFB42DB9BF3F9C1961C432592784F', 
 '2012-10-26 10:25:49.000',True,False,'������� ����� �����������','��� "����� ����"','4101011782','1024100000121','(4152)266720', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Balance@genbank.ru', '452BCDE91C05D9A2084D59CDA8505A7FF4E0E363', 
 '2012-10-26 10:34:06.000',True,False,'�������� ��������� ����������','�� "�������" (���)','7725049158','1027739692727','(495) 777-55-45 2024', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'logunov@npbank.ru', '1D008FBA64553CD1789884D05E930D80574A39D6', 
 '2012-10-26 12:10:42.000',True,False,'������� ������ ����������','��� "����� ������������ ����"','3524004713','1027739306638','8(495)9894701 (226)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ukpl.ru', '4E6C68B6B9421CD8D1FE6B1C4C04CCB431324558', 
 '2012-10-26 12:25:10.000',True,False,'������� ��������� �����������','��� "�� "�������-����"','2310151405','1102310007513','(8619)441400', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ndarovskih@uralfinance.com', '5326C5292979D96DDA955AFDEBDEF83E7BAB3E99', 
 '2012-10-26 12:39:01.000',True,False,'��������� ������� ����������','��� �� "����������"','6654001613','1026600001361','(343)222-11-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elog@zaofinans.ru', '57E1370781D0E7225330D13E306579D68F9A58C8', 
 '2012-10-26 14:58:54.000',True,False,'������� ������� �������������','������ ','7453228599','1117453002260','89227504443', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'house-capital@mail.ru', '30191CAA2D7E6B798E128A4C64C3260E2B9468BB', 
 '2012-10-27 08:10:31.000',True,False,'���������� ��������� ����������','��� "�������� �������"','1902024996','1121902001396','89130543555', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo777@lesprombank.ru', '5ED41137083CED8F24036CDA398CF91D9BCD26D3', 
 '2012-10-29 10:34:07.000',True,False,'��������� �.�.','��� "�����������"','7705052226','1027739030110','(495)624-87-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fininvest2@gmail.com', 'DB20E5D9B506D447994CE664E762520135DE53F5', 
 '2012-10-29 11:31:14.000',True,False,'������������ ������� ��������������','��� "���������"','545025148','1120545000113','89285155146', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sagibat@enobank.ru', 'C73D38C3292F412D6E8B42AE72192955FBD5D71B', 
 '2012-10-29 11:33:07.000',True,False,'������������ ������� ���������','��� "���" (���)','541002460','1020500000509','(8722) 609999', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ik_karat@mail.ru', '21DA083B40A3F695118538F01464398DA4EBF2D1', 
 '2012-10-29 15:27:59.000',True,False,'�������� ������� �������������','��� "�����"','1655024413','1021602853579','8435186871', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rosam@rosavto.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2012-10-29 15:31:56.000',True,False,'��������� ����� ������������','��� �� "�����������"','7717004724','1027700067394','495-380-30-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a@united.ru', '5DBE1E59FC0FD44F51D3149AA54BB43D7C5E633F', 
 '2012-10-30 05:14:02.000',True,False,'������� ����� �����������','����������� �������������� ������������ ���� "���������� ������������ ����" (�������� ����������� ��������)','2447002227','1022400000160','(391) 277-96-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zvv@bankermak.ru', 'C7A56D11EAABDE6BF961F5FC4B029E061B7A027C', 
 '2012-10-30 10:20:06.000',True,False,'����� �������� ������������','��� ���� "�����"','8603001714','1028600003497','83466495045', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bank@bank-accent.ru', 'D8A47A1DAB090F9DE3977BA5871D2ED158460584', 
 '2012-10-30 13:15:06.000',True,False,'�������� �������� �������������','��� ��� "������"','5613000182','1025600000865','(3537)216964', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'evgenia.khlystova@bnpparibas.com', '13E22ABDA385C606CC554548C64F6B4AC6FD3D36', 
 '2012-10-30 15:34:03.000',True,False,'������� �����','��� ������ ���� �������� ����������� ��������','7744002405','1027700045780','74957856000', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gudumak@nefteprom.com', '36BF52C93DC8A5D4829F8BACD43070888738DE37', 
 '2012-10-30 15:50:15.000',True,False,'������� �������� ����������','����������� ������������ �������� �������������-������������ ���� � �������� ����������� ��������','7701020946','1027739345281','(495) 787-48-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'epyatopal@mail.ru', 'A9C3B3990F8C1709EA77BCE16261937B0656981F', 
 '2012-10-31 04:07:31.000',True,False,'������� ����� �����������','��� "�����-������"','2536121829','1022501276830','(423) 240-00-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'admin@fpkbank.ru', '8984F8C87529610C222385D2CCB1FC84ECAF6BBE', 
 '2012-10-31 11:43:35.000',True,False,'������� ������','�� "��������� ������������ �������"','7730082256','1037739768604','(495) 232-5662', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'man@lightbank.ru', '92DBE2BA0E80DD4D1ACAF53A2C7064314D168594', 
 '2012-10-31 17:55:51.000',True,False,'������� ������� ����������','��� �� "��������"','7710046757','1027739223775','+ 7 (495) 644-33-37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'jnkjr@mail.ru', '4661FF0BD4403C39AB41078CF27DD617987EFAE4', 
 '2012-11-01 10:07:43.000',True,False,'��������� �������� ���������','��� "�������������������"','3123283153','1113123008789','(4722)424-300', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'katalevich.pavel@sogaz-med.ru', '790D9B6694248F47702F8BE52EB715222A197B6E', 
 '2012-11-01 15:12:24.000',True,False,'������� ���� ����������','��� "��������� �������� "�����-���"','7728170427','1027739008440','(495)225-23-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tlesja@mail.ru', '7A9619AF0C6E5A66154B70392BE3EA26BDD3C81A', 
 '2012-11-02 16:06:59.000',True,False,'��������� ����� �����������','��� "������ ������"','7612044575','1127612000647','9206564887', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'usakh@otlnal.ru', '4B4E71B85124CAD3F6B499A4A50928C2C0266995', 
 '2012-11-05 13:08:48.000',True,False,'�������� ��������� ����������','��� "�������� �������� - ����-���������"','6501250010','1126501006543','89244905577', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'baikov.1982@gmail.com', '627BB2ACDDBC955647D273E3AB0D6A81D54ADE11', 
 '2012-11-06 08:53:50.000',True,False,'������ ��������� ���������','��� "��"���������� ��������"','7702651253','1077759756667','89165262388', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@rivermanagement.ru', '8FA6250A7177CD539A757FD93F4187C5411AE18D', 
 '2012-11-06 11:44:41.000',True,False,'�������� ������� ����������','��� "����� ����������"','7707500836','1037739835000','8 (495) 951-38-48', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'agency52@gmail.com', '7269D91C49D67B148C97A9518CFFEA6C37FDAFAD', 
 '2012-11-06 15:58:42.000',True,False,'������� ��������� ��������','�������� � ������������ ���������������� "�������������� �������� "������ �������"','3525194947','1073525017642','(8172) 72-82-04', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'baikov_1982@mail.ru', '627BB2ACDDBC955647D273E3AB0D6A81D54ADE11', 
 '2012-11-07 00:40:59.000',True,False,'�������� ���� �����������','��� "���"���������� �������"','7715631871','1077746077969','9165262388', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ifc-mfs@yandex.ru', '83246E33AAE804A063AE610940823E608320AA47', 
 '2012-11-07 12:11:42.000',True,False,'������� ����� ��������','��� "�������������-���������� �������� "��������������� ���������� ����"','7719112669','1027739000145','(499) 165-93-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nadejda@zaural.ru', 'A4DBE4757D99147034597C185D31F407C20BBDC2', 
 '2012-11-07 12:24:33.000',True,False,'�������� ������ �����������','���"����������� "�����"','4501014033','1024500507569','41-88-63 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'otego.lobanov@yandex.ru', 'CDD31EF797668739CC7B17DB917F4EB9AC3AF968', 
 '2012-11-07 17:21:55.000',True,False,'������� ������� ��������','��� �����','7820324407','1117847103428','79313439998', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ramzia@nasko-life.ru', '075C1CF50EF4C5587AA4BD7D0A63607F8334DF50', 
 '2012-11-08 08:42:38.000',True,False,'������������� ������ ����������','��� "�� "����� �����"','1644030824','1041608004789','(843) 557-83-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'support@uc-bank.ru', '1267010126ADEAFC224AD7EEEF1138E97CF53EDD', 
 '2012-11-08 11:05:26.000',True,False,'��������� �.�.','��� ��� "������������� ������"','7736009502','1027700040477','8-495-785-69-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivanova-aa@yandex.ru', '1472E1A9CE7F184502AC9F2D35610C60A9012E55', 
 '2012-11-08 13:22:54.000',True,False,'������� ���� �������������','��� ��������� �������� "������"','5029060795','1025003529077','(812) 431-73-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'potapov@geopolis.ru', 'EC2CF4E48590D38DE5447DFD96D0A62CA9347295', 
 '2012-11-08 17:11:58.000',True,False,'������� ������� �������','���� "��������"','7704059892','1027739802683','+7 (495) 223-33-63', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'MedvedevaSI@cfcrus.ru', '32A594108AC4F0C1CDA5EA4711FDDF5AA77C6F39', 
 '2012-11-09 10:22:59.000',True,False,'��������� �������� ��������','��� "���-�����������"','7701304264','1027739074747','8 (495) 231-23-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Irak61@mail.ru', '75A3C8DB328F7DEF18F4C75F3DD0B9AE20ED5669', 
 '2012-11-09 11:22:18.000',True,False,'��� ����� �������','��� ���� ��������� ������ � �������� ������������������� ������ ������-���������','3851001335','1103800002338','89025690078', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'komarova@metroton.ru', 'ED62A62E5A7746062EF901C1641E277585DEBEFD', 
 '2012-11-09 15:05:34.000',True,False,'�������� ������� �����������','��� "�� "��������"','7707547591','1057746748751','(495) 650-26-92', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@volgacapital.ru', '7F813B70BD370B0526FD0B5F7F559152C26EBDE5', 
 '2012-11-09 19:16:17.000',True,False,'����� ����� ����������','��� "������������"','6317062166','1066317002070','(846) 379-10-66', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sviridova@credit.biysk.ru', '06878B094ABCA1F547D899B06456BC7D746AC3B1', 
 '2012-11-12 08:17:54.000',True,False,'��������� ����� ������������','�������� � ������������ ���������������� ��������� �������� "��������-������"','2227006410','1022200555959','8 (3854) 32-85-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kmv@depo.vstcb.ru', '7D44F7C2F6C2854EE8CF14950D375F634C488F99', 
 '2012-11-12 09:12:01.000',True,False,'������� ������ ����������','�������� ����������� �������� "��������-��������� ������������ ������������ ����"','3808000590','1023800000047','(3952)286312', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'genco-credit@mail.ru', '805511B71BA3762519E5BB250001206B392040AF', 
 '2012-11-12 09:24:20.000',True,False,'�������� ������� ����������','��� "������"','1435260180','1121435014788','79247657887', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ishmurzina@chulpan.ru', 'A5E43DF8690E7626055C534FABCB30974421BCE5', 
 '2012-11-12 10:17:00.000',True,False,'��������� �.�.','��� �� "������-�����"','1644039560','1061644063690','(88553) 455-325', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'finance@ingvar.ru', 'F01D77F6AA2277FA83473CBD6EAA831029258517', 
 '2012-11-12 12:38:50.000',True,False,'�������� ����� ����������','��� ��� "������"','7714103893','1027700484459','74953632475', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'glavbuh@kama-re.ru', '2230E6CBAE570950195C8E5F5E0D354B3E338E57', 
 '2012-11-12 13:51:43.000',True,False,'������������� ������ ����������','��� "�� "���� ��"','1644012166','1031608002106','(843) 557-83-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ooobroker@bk.ru', '1597262EFAE2332777C1330668D56712D55BB15A', 
 '2012-11-12 14:31:58.000',True,False,'�������� ������ ������������','��� "�������"','1651067430','1121651003088','8-953-491-57-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'evtushenko@regardins.ru', 'ED448971A97AD48BB85A5C357073BFA49E311519', 
 '2012-11-12 17:47:51.000',True,False,'��������� ������ ����������','��� "������ �����������"','7715350990','1027715006879','8 495 781-00-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buh@vsal.ru', '1BF31D0AF62AB5B15D1695E437107E97A7EED31D', 
 '2012-11-13 03:09:46.000',True,False,'���������� ������� ��������','��� ��������� ����������� ����������� "��������-��������� ������"','2538069934','1022501895350','(423) 244-68-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ionova@trustchel.ru', 'F7FE3D3CF1423194226E66D1B9D2F52F14EBE05F', 
 '2012-11-13 07:41:19.000',True,False,'������ �.�.','��� �� "�����"','7453232884','1117453006890','8(351)267-09-62', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uralcapital74.info@yandex.ru', '05297A00C990629A5ED7F4D77FC78028DD4FD314', 
 '2012-11-13 10:54:59.000',True,False,'������� ��������� ���������','��� "��� "�����������"','7447215432','1127447013297','+7(351)22-009-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'voytenko@osk-ins.ru', '93FA75146E3186A51C6487CD024846C336BC5799', 
 '2012-11-13 14:49:53.000',True,False,'�������� ������� ����������','��� "���"','6312013969','1026301414930','(846)276-03-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'shilov_b@vivat-retail.ru', '1604AE932E97196048D23DF1F0777F9CFDC34F6B', 
 '2012-11-13 15:09:32.000',True,False,'����� ����� ������������','��� "�����-�����"','5903048771','1045900388258','8-902-630-30-19', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buh@in-touch.ru', '8653827FCD8456ACE84D94A56660B9E1EFF914FC', 
 '2012-11-13 15:18:35.000',True,False,'������ ������ �����������','��� "����� �����������"','6315212497','1026300955724','8-(495)-726-51-06 (2106)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'finans.grupp@inbox.ru', 'CDAA753D1B33EE35F16E71861172FD4283EB11BB', 
 '2012-11-13 17:22:21.000',True,False,'�������� ��������� ���������','��� "�����������"','7716699061','1117746770184','79101799971', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@goldenstonemorgan.ru', '07BF43068F3963926A7DF306D567FEA539A10080', 
 '2012-11-13 17:46:01.000',True,False,'���������� ��������� ��������','�������� � ������������ ���������������� ����������� �������� "������ ����� ������ ����� ����������"','7702768653','1117746624698','(495) 785-96-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'hadiullina@abstr.ru', 'D1BF09A404E2AF8563A6FE6B89DEA67BFE9313BF', 
 '2012-11-14 10:51:26.000',True,False,'���������� ������ ���������','��� "�� ���� �����������"','1658131075','1111690088124','(843)5249624', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'shirokov@tzh.ru', 'CE647242BD61C7C041730A0894D7F99AA2AD32E5', 
 '2012-11-14 14:23:41.000',True,False,'������� ������� ����������','��� "��������� �������� ����-����"','7604036010','1027600676630','(4852) 660-400', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'selena_sk@mail.ru', 'DAF9D44226228CBBB9399063B31D0B69A4512C9D', 
 '2012-11-14 16:25:42.000',True,False,'��������� ����� ��������','��� "��������� �������� "�������-����� �����������"','7714553444','1047796367552','(812) 322-63-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kurochka@talisman-so.ru', '4E026C6DF5A94551DD7DC9D185047293B738B29D', 
 '2012-11-15 10:32:51.000',True,False,'������� ����','��� "��������� �������� "��������"','1655004449','1021602840181','(843) 221-14-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'potapova@askomed.ru', '7CE9136BECC6B2207679084FB71AEA924B086A1C', 
 '2012-11-15 11:48:16.000',True,False,'�������� ����� �����������','��� �� �������','6311009328','1026300954789','(8462)3325519', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga-office@krossinv.ru', 'A73B17BF13B069F971E7F3D42871D671D4AB92C6', 
 '2012-11-15 13:23:37.000',True,False,'�������� ����� �������������','��� "���������������" (���)','7710008416','1027700546895','544-48-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ppomgnp@gmail.com', '3D74DC9B9D0B72B000FB3BED7E4F7AE17025EA1C', 
 '2012-11-15 16:20:21.000',True,False,'������ ������� ������������','��� "���������������"','7716027013','1027700285645','74991827683', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'chepurnyh@strizh.izhnet.ru', '249A353B55F6C1CB0A97ACD181210C210C60B35E', 
 '2012-11-16 10:14:28.000',True,False,'�������� ��������� ��������','��� "��������� �������� "������! �����������"','1835022084','1021801653587','(3412)90-84-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'GagarinaTA@tnic.ru', '551217AFC0CD2FA9CFB79DF71F22C3EEBB525BAE', 
 '2012-11-16 11:42:02.000',True,False,'�������� ������� ����������','��� "����������������� ��"','7709217038','1027700089670','(495) 664-28-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aovl1984@mail.ru', 'CF8251C06DFF38596525CD054D213EBC5367FB2C', 
 '2012-11-16 16:46:17.000',True,False,'�������� ����� ������������ ','�������� ����������� �������� "����-������"','7729017478','1027739036192','(495)729-58-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kasko_buh@avtic.sc.ru', 'ED6C63611E9DF2533917C8363200254CC6182DDF', 
 '2012-11-19 11:08:23.000',True,False,'��������� ����� ��������','��� "�� "����� - ��"','4825007460','1024840829815','74742771847', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'marina.a.savenkova@boc.ru', '4AAEA21627D2A9771C99A7D94CEC87D478B63A62', 
 '2012-11-19 16:34:34.000',True,False,'��������� ������ �����������','��� "���� ����� (����)"','7706027060','1027739857551','8 495 795 04 66', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'perminov-m@gaz-is.ru', '839DF16C159521C0235914B22FC28D75B4DC5CEA', 
 '2012-11-19 16:38:11.000',True,False,'�������� ������ ������������','��� "�� ���"','7805544260','1117847050199','+7 (812) 3052052', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dlrz@list.ru', '4D1AA1C4982D972B0A5363AEFC9847B3028B395E', 
 '2012-11-20 10:00:45.000',True,False,'�������� ��������� ������������� ','��������� �������� "����������" � ������������ ����������������','2702030458','1022700913366','(4212) 41-51-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'granta-kazan@mail.ru', '893DA7C8C541757CD1146576A892ECB5E3C46566', 
 '2012-11-20 16:49:49.000',True,False,'������� ����� ����������','�������� � ������������ ���������������� "��������� ��������� ����� "������"','1655230261','1111690077751','+7(843)2644275', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'so_sbk@mail.ru', 'ACD2E79CC7704C083FA2100F603857EAF332539C', 
 '2012-11-20 17:08:03.000',True,False,'������� ������ ����������','�������� ����������� �������� "��������� �������� "�������-���"','2130031324','1072130019378','(8352) 62-32-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'makarenko@megarussd.com', 'C6C38F003691A7516A5E5DE90D16FB917CA27B63', 
 '2012-11-20 20:16:45.000',True,False,'��������� ����� �������','��� �� "��������-�"','7728115553','1027739151186','(495) 967-92-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vornakov.sdvb@mail.ru', 'C2CA4ABF15C997AF1755F008AD6C2DF3F5629203', 
 '2012-11-21 09:18:35.000',True,False,'�������� ����� �����������','��� "��������� ��� "��������� �������"','2536149976','1042503037125','79147909327', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ank@ank-pki.ru', '227D79D0B51980D36C040DA678920E10F4B0180F', 
 '2012-11-21 15:43:02.000',True,False,'������� ������ ��������','��� "���"','7841010190','1047841003990','8(812)644-70-74', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Egorova@dal-rosmed.kht.ru', 'DC369F1F7A8C7ED22D40FB512AF65192C12106E8', 
 '2012-11-22 08:38:07.000',True,False,'���� ���� ����������','��� "��������� �������� "����-������"','2721130120','1022701131760','89144048879', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'smp@tyumen.ru', '36217C5DF5CE290F5021B8DC5346635663C573E3', 
 '2012-11-22 08:40:49.000',True,False,'�������� ������� ��������','��� "���-237"','7202075940','1027200778692','8(3452)47-20-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'diana@irmail.ru', '3F3009FDB7AEFDD2C865D581CF05754581E5596A', 
 '2012-11-22 08:50:44.000',True,False,'��������� ������� ����������','��� "��������� �������� "�����"','3801013896','1023800517410','(3955) 673991', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.koptelov@paritet-sk.com', '1B086E20051444CC5C59ECDFC8A11C3126285428', 
 '2012-11-22 09:48:45.000',True,False,'�������� ������ ����������','��� �� "�������-��"','7705233021','1037739298442','4959582380', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sshaihutdinova@16sp.ru', '3F20B6BF41324563FF721D0C0F7983B5E6ED6E90', 
 '2012-11-23 09:59:54.000',True,False,'������������ ����� ���������','��� ��� "��������"','1660046892','1021603626021','(843) 5-700-717', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'IChelysheva@sot-trans.ru', '1471D9FE7FDF7D16A3B2D2F58607C1D02F2738C7', 
 '2012-11-23 10:03:04.000',True,False,'�������� ����� ����������','��� "��������� �������� ��������������� ����������"','7709258588','1027739381251','8(495)280-07-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kub@tomline.ru', '3F5830A2913691AD9F8C21EB03D625BF58FA675B', 
 '2012-11-23 11:43:52.000',True,False,'�������� ����� ������������','��� ���','7017154234','1067017162673','-286703', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mskfinansirovanie@gmail.com', '62CB8A040FE7BF6A4A17BB88FE32382D653EE621', 
 '2012-11-23 12:42:29.000',True,False,'��������� ������ ���������','��� ��������������','7729705964','1127746199448','8 916 944-75-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@eurofg.com', 'B2EDB08B3F412D49288B80FC9CBCBFC67BD32827', 
 '2012-11-23 12:59:31.000',True,False,'�������� ��������� �������������','�������� ����������� �������� "��� ���������� ��������"','7705918106','1107746408087','(495) 995-70-57', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@invest-it.ru', 'E8228AF967416B603BDA42EF6E66B9E2EFE89FF3', 
 '2012-11-26 11:19:29.000',True,False,'����������� ������� ��������','�������� � ������������ ���������������� "������������������"','5406707128','1125476058960','(383) 217-70-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@saurun-invest.ru', '7D6F6F389814A8F4AB1F72E991C5F078B459416D', 
 '2012-11-26 11:34:40.000',True,False,'������  ������  ����������','��� �� "������"','7813520425','1117847528700','(812) 303-85-06', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kononovaele@yandex.ru', 'DF80E9491004A0E259ECFDEA17BC37586D19EE04', 
 '2012-11-26 12:26:56.000',True,False,'�������� ����� �����������','��� "��������� �������� ���������"','7708215214','1037708003442','+7(495)5142020', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Tumenev@ic-eni.ru', 'CA107D18E58C9636F0A56DD49B3C844020B760A9', 
 '2012-11-26 14:21:47.000',True,False,'������� ���������� �������������','��� �� "���"','6163010542','1026103159532','8-(863)-2-955-351', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kontur99@gmail.com', '37157964A082DDFCC59BC9D9365474D783BB7602', 
 '2012-11-26 14:37:45.000',True,False,'�������� ���� ����������','��� "�����-������"','7735513121','1057747635791','8(495)5041266', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zischita@mail.ru', 'C22435180557735A56EC5089FF967B8BCD28418B', 
 '2012-11-27 10:00:45.000',True,False,'����� ������� ����������','��� "��������������������"','7536004810','1027501158277','(30-22) 44-66-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'razzhivina.i@yandex.ru', '73CD24F8336326195E9DBF3B760ADEE0E71D1BB4', 
 '2012-11-27 10:20:40.000',True,False,'��������� ����� �������������','�������� � ������������ ���������������� ��������� �������� "������� �����������"','7606001534','1027739608005','(495) 287-03-33 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'am112@mail.ru', 'CC28CEDA287FFB98F5A9BB534A103AB578438785', 
 '2012-11-27 11:15:31.000',True,False,'���������� ���� ����������','��� ���','7702059544','1027739527881','89466822988', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga.litvakova@ingos.ru', 'F540BA92932AAD4510D274FE68110B708889FF02', 
 '2012-11-27 14:53:55.000',True,False,'��������� �����','��� "�� "����������-�����"','7702501628','1037739872939','89265872901', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buh@smpins.ru', 'FDAF87693F19A83B318ACBC1F4198B9DBD9919CE', 
 '2012-11-28 09:47:24.000',True,False,'���������� ����� ���������','��� "���-�����������"','7728306068','1037728057840','(495) 536-94-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nuc@nucrf.ru', 'FED844D5EF34BEC1E0A8EB9692AA2F0B05673ACA', 
 '2012-11-28 10:25:19.000',True,False,'������ ���������� ����������','��� "������������ �������������� �����" ','7722766598','1127746036494','+7(495)690-92-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'starkova_n@taisu.ru', '6F01994EC995E291DCCBCB36700F059D0F12D467', 
 '2012-11-28 11:32:37.000',True,False,'�������� �.�.','��� "��������� �������� "���������"','7705453852','1027739101444','(495)739-09-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'support@insros.ru', 'D68C0E30E612222DDFA1CF0F91AE1F0601989CB6', 
 '2012-11-28 15:40:32.000',True,False,'������� ����� �������','�������� � ������������ ���������������� "�������� ����������� ����� "������"','7717508312','1047796263283','8(495)7995574', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Skripnikovs2008@gmail.com', 'A722B5B45E9F6A470487852BE7533CD2303DD787', 
 '2012-11-29 21:13:19.000',True,False,'����������� ����� ���������','��� "������������� ���������� �����������"','6682001950','1126682002083','89527258702', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Smirnovaiz@infotecs.ru', '3651FC83B166A2CFA35AE4E64782A93B45C6C6A2', 
 '2012-11-30 10:17:59.000',True,False,'�������� ����� �������������','��� "�������� �������� �����"','7743020560','1027739113049','(495) 737-93-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stas.p@kordon-rnd.ru', 'C24F08DA4BF890BFFE1F7A3FB92E2E336BBE229E', 
 '2012-11-30 11:00:03.000',True,False,'��������� ��������� ��������','�� ��� ������','6168022453','1086168002205','89286027562', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'koopsib@mail.ru', '287EBF1B25EE23DA528E5392EFA59FE1F4C2D960', 
 '2012-11-30 12:29:09.000',False,False,'�������� ��������� ����������','�� ��� "������������� ��������"','5406571621','1115400000902','8-383-222-61-15', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ikarpova@russianre.ru', '839C0175EBAC2CEF5A44422003D82304BD7AEC52', 
 '2012-11-30 17:11:13.000',False,False,'������� ����� �������������','��� "������� ���������������� ��������"','7707088309','1027739122498','+7(495) 933-8883', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'asstra@aaanet.ru', '4DCD7824A77405090CAF0E9FE0763DFE54949313', 
 '2012-12-03 09:11:12.000',False,False,'������� ������ �����������','��� ��� "������!','6165014126','1026103719289','88632950072', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ssukhomlinov@mail.ru', 'A0974187152A9AB50C836E48BA50EF62136FA1C2', 
 '2012-12-03 14:33:38.000',False,False,'���������� ������ ����������','��� "���"��������"','7744003688','1047744005022','74957868843', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'siv@zhasko.ru', 'A1CF21D9668874839128C7714FFC1FC6C808D2E9', 
 '2012-12-04 10:31:37.000',False,False,'�������� ����� ����������','��� �� "�����"','3525013446','1023500898805','(8172) 72-71-44', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'natz@suzhaso.ru', '8D789C3DABAEBF52527550F6FBC180FC4591D82C', 
 '2012-12-04 11:57:41.000',False,False,'�������� ������� ����������','����������','7451032223','1027402905485','(351) 268-31-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'erazova@gsmk.ru', 'F54321B35FA13D0B321201447EBBBEF71FCC27A9', 
 '2012-12-04 16:11:37.000',False,False,'������ ������� ��������������','�������� ����������� �������� "��������� ��������� ����������� ��������"','7825457129','1027809172489','(812)764-25-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'parnas_investment@mail.ru', '543F33C75802E846E315C81A6B88C050E3401099', 
 '2012-12-05 11:35:38.000',True,False,'��������� ������� ����������','�������� � ������������ ���������������� "��� �����"','7810865830','1127847216860','8(812)457-18-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'derjava@bk.ru', 'F8418B97C385066BC45061DDFBF073C0B4240FE8', 
 '2012-12-05 13:17:37.000',False,False,'��������� ����� ����������','�������� � ������������ ���������������� "��������� ��������" �������"','7713290785','1027739516078','(499) 254-79-44', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zni@insotek.com', '680F05144F612F3436BA8198ECB2724B931B2AA3', 
 '2012-12-05 15:22:16.000',False,False,'������� ������� ��������','INSOTEK','7743043470','1027700307623','(495)783-60-18', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'cgk-chebok@mail.ru', '77AA41D4238C17383C3D896C698F6C95298A821C', 
 '2012-12-06 13:13:22.000',True,False,'������� ������� ���������','��� "����� ��������� ������������"','1655148987','1071690073498','8-927-998-88-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'urist@elmash.net', 'A77CEC607B836649D143C0C87D10762DD1221462', 
 '2012-12-10 13:28:10.000',True,False,'������� ���� ����������','��� ��� "�� �������"','1840003538','1111840012943','(3412) 43-33-45', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'E.Saushkina@finans-invest.ru', 'F374461634FCE6800F9E277634998FF3D445B6CB', 
 '2012-12-10 16:38:19.000',True,False,'�������� ������� ���������','��� "������-������"','7720033605','1027739123576','8(495)913-48-84', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ast-am@mail.ru', 'BC49C6EA051B178F59EDAC9CF688F3D717641B75', 
 '2012-12-10 17:35:43.000',True,False,'������� ���� ������������','��� ����������� �������� "���"','7701881889','1107746518868','8(812)458-72-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'chesnok72@mail.ru', '370D8CCC97D0025FF3426769E0A1EFD3ABC3DACC', 
 '2012-12-11 12:44:39.000',False,False,'������ ������ ������������','��� ���������� ����������� ������ ������','6154126011','1126154008969','79094060101', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena.smolyaninova@soglasie.ru', '7A9C5BB6A4A34AB4373D599C599EDFF82BBAB7A9', 
 '2012-12-11 13:03:38.000',False,False,'������������ ����� ����������','��� �� "��������-����"','7706217093','1027700035032','(495)783-26-41 31-04', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ELENA@stocks.surgutneftegas.ru', 'CE50F6817057CFC3124A728FDB09B394B38E3E48', 
 '2012-12-12 09:24:31.000',True,False,'������������ ����� ����������','��� "��������������"','8602060555','1028600584540','9222593413', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'suhareva@novikom.ru', '021DFE6C9F98CCE81467629ECF3775A9EB36A680', 
 '2012-12-12 10:24:50.000',True,False,'�������� ����� ������������','��� �� "������������"','7720555129','1067746696852','89269743399', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'shiryaev@inventumrussia.ru', '7BD509A267249824B91853367E5B9DCFE0E93A8D', 
 '2012-12-13 14:12:02.000',True,False,'������ ���� ���������','��� ��������','7704808485','1127746413684','(495) 792 55 95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pcrsk@mail.ru', '25F297282840B45C06905A9B0F8E7C17A8289E68', 
 '2012-12-14 12:24:12.000',False,False,'���������� �������� ������������','��� "��������� �������� "���������������"','7744000373','1027700005651','(495) 967-19-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@icentr.ru', 'C73A5BDD59299C035DD0B41B0F78F7E1DD859154', 
 '2012-12-14 14:30:22.000',True,False,'�������� ������� ����������','��� "���������"','3328430017','1043302000719','8-800-100-53-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@krona-invest.ru', '45E572D18D0D7005089E12E0DC9478FA4032EFA4', 
 '2012-12-17 17:36:11.000',True,False,'����� ������� �����������','�������� � ������������ ����������������  "�������������� �������� "�����"','7721655180','1097746046837','8 (495) 604-46-16', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ykskc@mail.ru', '45E572D18D0D7005089E12E0DC9478FA4032EFA4', 
 '2012-12-17 17:43:29.000',True,False,'������� ������ �������������','�������� � ������������ ����������������  �� "���� ������� �����"','7721724179','1117746350556','8 (495) 604-46-17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'inplena@mail.ru', 'DAFEC02D3B5951893C8BDA21496E42DCC25BF3DE', 
 '2012-12-18 14:11:29.000',False,False,'��������� ����� ���������','��� "��������� �������� "������-�����"','5018073496','1025002030294','(495) 796-80-86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@legion-am.ru', '33BF683AC5E66116C7FB336611D0475AC9B1C665', 
 '2012-12-18 15:42:01.000',True,False,'������� ����','��� "�� "������"','7447213516','1127447011020','+7 (351) 223-46-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pod@cbx.ru', '685BEE4C0CEEA649862ADEC10554E8A8092A04A2', 
 '2012-12-20 16:31:49.000',False,False,'���������� ����� �����������','��� ��������������� ��������� �������� "���������"','2128021209','1022101133317','8352 28 64 84 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elenak@alexbank.ru', '44B1E00CB9E1383DC768D051E7765F9E638E2CFA', 
 '2012-12-21 15:16:44.000',True,False,'����� ����� ����������','��� ���� "���������������"','7831000080','1027800000194','8 (812) 324-85-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'shilovsvarsenal@gmail.com', 'C9DCD8B2E261861A2C521084EC14CB47D4DF4101', 
 '2012-12-24 12:33:45.000',False,False,'����� ������ ����������','�������� � ������������ ���������������� "��������� �������� "��������"','7705512995','1047705003895','+7 499-277-7979 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'avangard-stv@mail.ru', 'F87DCB55CD2D2DE8F6709D16518615B4EB3D7B09', 
 '2012-12-25 12:59:34.000',False,False,'�������� ������� ����������','��� "����"','2983008790','1128383001416','79115579651', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ik.apriorinvest@gmail.com', 'DA59C973DDA3C1F0F2138778F0D4C11639520091', 
 '2012-12-25 15:10:01.000',True,False,'������ ���� ������������','��� "�� "������ ������"','6311997692','1116311005579','89879692087', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'arlen@granit-polis.ru', '5141E5C103105A75EC3E9050B4DE4687B6D10B2D', 
 '2012-12-25 15:41:16.000',False,False,'������� ������ ��������������','��� ��"������-�����"','7744002300','1027739642754','8(495)580-39-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'chulpan_med@mail.ru', 'E818314A8FD6B3A398A0CE64BDEC39AA052D691B', 
 '2012-12-27 10:02:46.000',False,False,'����������� �������� ���������','�������� � ������������ ���������������� ��������� ����������� ����������� "������-���"','1644031803','1041608018726','(8553) 45-48-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'natalya.serg.1984@mail.ru', '83B56611CBE54DBD051A7AC4EF978670C9558F7B', 
 '2012-12-27 10:48:59.000',True,False,'��������� ����� ����������','�������� � ����������� ���������������� ��������������� ����������� "��������� ������"','6617017051','1096617000369','8(34384)666 57', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'svetulya-cok@yandex.ru', 'A537692CB5771C65853F2C01BECB611AD407A83D', 
 '2012-12-28 10:26:51.000',True,False,'��������','��� �������','5256036414','1025202271467','89425694228', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@rshb-am.ru', '6995B2C25052DF908798E46F0858C04A6E27BE52', 
 '2012-12-28 12:59:08.000',True,False,'������� ������ ����������','��� "���� ���������� ��������"','7704814182','1127746635950','(495) 660-47-65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'co-akzept@mail.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2013-01-05 13:02:35.000',False,False,'�������� ���� ��������','��� �� "������"','7744000253','1027700051565','(985) 976-04-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sibinvestagent@gmail.com', '09DA5A577A7FA8FFFED08F3031370054EFB8C553', 
 '2013-01-08 14:22:37.000',True,False,'�������� ���� ������������','��� "��������������"','7203281367','1127232050164','79220401188', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fkcb@plgm.ru', '83C806242641BD0638161270CF556D7DA4A18A07', 
 '2013-01-09 11:58:47.000',True,False,'������� ������ ����������','��� "��� "��������"','1655020909','1021602845153','(843) 299-77-11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'struchkov@irp.ru', '3D7C57C463F85C34FBFC218ADBE71747AE466210', 
 '2013-01-09 14:38:04.000',True,False,'�������� ������ �������������','��� "���"','7730511459','1047796604063','(495) 783-8008', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'RAW@bankofkazan.ru', 'ACDFAC2E7BF3BD5B584E7B5011E4AC7B85E61354', 
 '2013-01-11 11:59:57.000',True,False,'���������� �.�.','��� ���� "���� ������"','1653018661','1021600000014','(843)5249713', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uk@stolichnaya-am.ru', '9777515F10F8C7973DE620184BC871478708A811', 
 '2013-01-11 16:53:02.000',True,False,'�������� ������� ���������','��� "�� "���������"  ','7733798859','1127746266515','+7(495) 726-06-54', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tochony@ifinco.com', 'CE919E1F925DB6118194A6151BE54D5B87558317', 
 '2013-01-11 18:02:25.000',True,False,'������� ���������� �������','��� "�����-������"','2208020063','1112208001751','89069696660', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@fk-management.ru', '679E2BC1CFDCCB08F769FDCB575450D7483DA2B6', 
 '2013-01-15 08:49:38.000',True,False,'��������� ��������� ���������','�������� � ������������ ���������������� "����������� �������� "������ ������� ����������"','5903096976','1125903001355','220-31-31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ito@so-rs.ru', 'D337C5C74D4533EA0D7C4F52ED6114F511B65EC0', 
 '2013-01-15 10:32:31.000',False,False,'��������� ������ ����������','�������� � ������������ ���������������� "��������� �������� "������ ����"','7722503415','1037789077908','84952218028', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'profikanet@gmail.com', '0158C26B50E3CEAABABF3E50FDDCC02C352ADCC3', 
 '2013-01-15 14:31:11.000',True,False,'��� ���� ���������','��� �� "�������� ��������"','7842477453','1127847339993','79252453282', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kdekhati@rencredit.ru', '90A06499B131D920E48FD6A516C1F73D59E9628B', 
 '2013-01-15 15:21:36.000',True,False,'������ �������� ����������','������������ ���� "��������� �������" (�������� � ������������ ����������������) ','7744000126','1027739586291','4957834600 (12385)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@murpbank.ru', 'CAF2EF5D2FEADABA3905F7E8F8A9CEF31F69DB57', 
 '2013-01-15 17:02:21.000',True,False,'����� �������� �������','�������� ����������� �������� ������������ ��������� ����������� "������������� ��������� ��������� ������"','6608006818','1026600002384','(495) 988-00-16', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sschervonaya@severstal.com', 'E61C76777FAEA7CE4AE57222DADF1A2C71D755C5', 
 '2013-01-17 12:10:51.000',True,False,'�������� �������� ���������','��� "����������"','3528000597','1027700125628','-8324', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bearus27@gmail.com', 'C0A6E473DA0EAFE2677747B01223A02722DB7987', 
 '2013-01-17 12:54:21.000',False,False,'�������� ������ ���������','�������� ����������� �������� "��������� �������� "���������"','2702030521','1022700930020','89243001719', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'E.Philipova@sibir.ttk.ru', 'DF80E9491004A0E259ECFDEA17BC37586D19EE04', 
 '2013-01-21 10:14:53.000',True,False,'��������� ��������� ����������','��� "���� ����"','2465276100','1122468042377','(8391)216-04-90 (4101)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'medstrax.mo@mtu-net.ru', 'CA4F0704E86EFAD329336D0D7808F3A446ECBA4E', 
 '2013-01-21 11:24:21.000',False,False,'������� ����� �����������','��� "��������� ����������� �������� "���������� � ������������"','7714041502','1027739477985','495 781-96-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kitova@astrametall.ru', '1480016F1838D40043A101816C34EFA2D5E7C214', 
 '2013-01-22 09:48:45.000',False,False,'������ ����� ����������','��� ��� "�����-������"','7414006585','1027402236620','(3519) 43-83-47', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'AlinaAkishina@dme.ru', '097E00C1146C27FFC589BA833672CFA1F87E9F6D', 
 '2013-01-23 15:55:21.000',True,False,'�������� ���� ���������','��� "��� "�������������"','5040008566','1025005124275','158523117598', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsfr@future.ru', 'C5B3A179584936864B90387986FFDD591871FD39', 
 '2013-01-24 17:22:55.000',True,False,'�������� ������� ����������','��� "������" (���)','7722022528','1027739153573','(495) 737-8643', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'v260362p@yandex.ru', 'F84B78E62FDB9A75F57EBD2DEC3697D82113A521', 
 '2013-01-25 15:13:29.000',True,False,'�������� ������� ��������','��� "����� ���������� ���"','7801392271','1057812752502','88001008812', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'director@chitazaim.ru', '7AF99FD7DE2B8F075FA32DD6BD62A8F474EF3976', 
 '2013-01-28 07:31:28.000',True,False,'����� ������� ������������','��� ����� ������ � ����� "��������"','7536131054','1127536007213','79144640999', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pulsera@rambler.ru', 'B4D26535E5B4A4CD1E05F3B698537987FE0289C5', 
 '2013-01-28 21:12:04.000',True,False,'��������� ������ ����������','��� "�������-�����"','3128027499','1023102372776','(4725)43-59-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.vlasova@dalena.ru', 'EC6CDC3776681BA981E6D291FFE1B5C4F42048D5', 
 '2013-01-28 17:24:59.000',True,False,'������� ����� ������������','��� ��� "������"','7720069320','1027700135540','(495) 673-10-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ivanova@euro-ins.ru', '12232D539ABEA9125E7E740191E750D45CEDC870', 
 '2013-01-29 15:11:15.000',False,False,'������� �.�.','��� ��� "�������"','7714312079','1037714037426','84959265155', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dvdufkhb@gmail.com', 'B41AEA09670E15C458961CE595E6140A5F5020D5', 
 '2013-01-30 02:50:07.000',True,False,'������� ������� ����������','��� "�� ���"','2723142963','1112723005988','75-63-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vsi@sov-ins.ru', 'EBB6FBF572701DDA2AB82CF567020FB3D28274DE', 
 '2013-01-30 15:25:25.000',False,False,'������������ ������ ��������','��� "��������� �������� "���������"','7835003413','1047833028704','(812) 3350388', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uk-sb-capital.ru', 'A32FA59355CDE6EBF139DFB2D41BC854FECD8A81', 
 '2013-01-30 17:44:08.000',True,False,'��������� ����� ����������','��� "����������� �������� "��-�������"','4703127826','1124703001422','8122320089', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'svv@zaotrest.ru', '12B0D7DB4D2791E5012B7DEB574323A2CAE27B1D', 
 '2013-01-31 13:24:16.000',True,False,'�������� ������� ������������','��� "�����"','5904164403','1075904008311','+7 (342) 298-11-25', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'at@aricapital.ru', '1A7E3DF12249D0F6EFAF686E702CDA731835AD35', 
 '2013-02-01 09:47:41.000',True,False,'������� ���������','�������� � ������������ ���������������� ����������� �������� "����������"','7705525627','1127747149155','(495) 225-93-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'silvika@finance.izhnet.ru', '58E543D1E8CE1659735ADB2206A775EB16CA5DB7', 
 '2013-02-04 16:58:58.000',True,False,'������ �������� ��������','��� "�������-������"','1834040919','1071840005071','8 (3412) 46-99-46', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elenak5@rambler.ru', '3708041073EC01538813466F28E67C1342993745', 
 '2013-02-04 17:02:23.000',True,False,'�������� ����� ����������','���"���"','4714024237','1104714000137','4497797', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@ghpam.ru', 'F087EB7FA93935A9BE812F31C2F219CE7C7D6744', 
 '2013-02-05 11:57:01.000',True,False,'������� ������ ���������','�������� � ������������ ��������������� "��� ��� �� - ���������� ��������"','7715917398','1127746354977','+ 7 495  411-62-19', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 't.rusinova@piter-trust.ru', 'DF1790B78FB4D7D8A461614CB293B8286949772B', 
 '2013-02-06 11:35:41.000',True,False,'�������� ������� ����������','�������� ����������� �������� "�������������� �������� "����� �����"','7842350721','1079847061150','(812) 336-65-86  ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zaonis2012@gmail.ru', 'F49D5D3F9244803F4010C0F5E8F12547C2889DFC', 
 '2013-02-06 14:27:40.000',True,False,'������ ������ ���������','��� "���"','7725752265','1127746228972','8(499)766-92-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zao.energetik@mail.ru', '33088641A222D62FFE4BF5F50A243EC35D5BE22F', 
 '2013-02-07 08:23:03.000',True,False,'��������� ������ ������������','��� "���������"','2353015823','1022304841789','8 861 30 5 85 75', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mos-9@mail.ru', 'EE47A8B3F1BDDE6FBC298AAB984D38194A0851CB', 
 '2013-02-08 09:06:50.000',True,False,'�������� ��������� ��������','��� "���-9"','5053004248','1035010653040','8 (496)-574-34-77', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'upc.aviator@yandex.ru', 'BF71C4BB64941AAABEDFFF34E227A6106747E158', 
 '2013-02-08 09:08:05.000',True,False,'�������� ���� ������������','��� "��� "�������"','7701915344','1117746302233','84992610120', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gsbc@gsbc.ru', '03759A9F0E4A7F759F94D858E468AFC92CADE313', 
 '2013-02-08 10:18:09.000',True,False,'������� ������� ����������','��� "������ ���������� ��"','7710058544','1037739173702','+7 (495) 933-33-22', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gazeta.bikin.ru@gmail.com', 'C60FF91E4C8AE2E76E5CBB7308600DE3F694C3FC', 
 '2013-02-09 11:25:51.000',True,False,'��������� �������� ����������','��� "��������� ���������������� ��������"','2707004853','1082713000831','89142045292', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tech@tsinsurance.ru', '289CCFDABAE778669CEB4C3F8A2C8BA2F3805141', 
 '2013-02-12 10:10:30.000',False,False,'������������ ���� ���������','��� "������� �������� �����������"','7703370086','1037703013688','+7 (495) 926-77-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'g.hlopov@klinlab.ru', 'C846D69BF1702C9C00FC4426DB74D59D1B651E79', 
 '2013-02-12 11:12:42.000',True,False,'������ �������� ��������','�������������� ���','5020000618','1025002587895','8(49624)58476', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'anna.babenko.87@mail.ru', '74923FC4D47BCDC61A95F2E55B939F1B9282BD3A', 
 '2013-02-12 13:28:40.000',True,False,'������� ���� ������������','�����-������','3123317356','1133123002484','89517625759', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ktv@rinco.su', 'BAA81FD14998A524CB9214167E87152EF28F5BFC', 
 '2013-02-12 14:58:18.000',False,False,'������� ������� ����������','��� "������������ ��������� ��������"','1832008660','1021801434643','74956600690', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena.grasko@brgroup.su', 'C5839A81FCB2B0C47E2E568DC4B3929B208D20DC', 
 '2013-02-12 15:32:38.000',True,False,'������� ����� ����������','��� "������ "������ �����"','7717741132','1127747241698','8(495)775-69-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nesterov@allegroclassica.ru', 'EEF1084A7D371FF9A7C6716AD49DD828E5263584', 
 '2013-02-13 08:32:52.000',True,False,'�������� ������� ���������','��������  ����������� �������� "������� ��������-���������� �������� "������"','5018035451','1035003365451','+7(916)530-5403', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gl.buh@edengivam.ru', '78A8A6DD6A649236E5AD9EE05F1CA35C94E2390C', 
 '2013-02-13 10:46:30.000',True,False,'���������� ����� �����������','��� "���������"','1655254880','1121690075980','(843) 236-10-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mfu01@mail.ru', 'F72AE16B29FAC38A7E83FE1507BEDD702BF47C8A', 
 '2013-02-13 23:23:49.000',False,False,'��������� ����� ����������','��� ��� �����������-��','105064330','1110105002171','79618189999', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'astrkapital@mail.ru', '260F7827AB28839D6EC02EA0CFC56B833AC7C9C6', 
 '2013-02-14 13:45:39.000',True,False,'��������� ����� ������������','��� ���� ������������','3015097336','1123015002681','89881729659', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bizon@online.ptt.ru', 'D3B5F2B5005DB228AF10A4769C475C5E51415264', 
 '2013-02-14 14:53:15.000',True,False,'������ ����� �����������','��� "�� "����� ����"','7702675720','1087746700931','74956625330', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nazarov@nflk.ru', 'DFF54B35020F593DFF7382C4AD0536C22796F009', 
 '2013-02-15 10:45:53.000',True,False,'������� ������� �����������','���"�������-������"','4217095823','1074217006632','8-906-931-88-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kerimow.samir@gmail.com', '4E8B244BDD3B02E2001F96CC0121D432DF9156A4', 
 '2013-02-18 13:12:26.000',True,False,'������� ����� ����������','��� "�� ����������"','7706236385','1027700143624','8(495) 258-34-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mes-03@mail.ru', 'A50DBD22599676D6B79A03D22D7638D1B2632A2B', 
 '2013-02-18 15:00:52.000',True,False,'����������� ������ ���������','��� "��� ��������������"','7705008315','1037739028073','84956338614', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vshevchenko@npf.nornik.ru', '90C2FAE66782CE26F3FC51CFC933E43F9564D566', 
 '2013-02-18 15:56:10.000',True,False,'�������� �������� ����������','��� "��������"','7717020571','1027739308937','849578347844476', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vla8643@ya.ru', '4272146D7F0985368A03B663C8BF365D1EFAD094', 
 '2013-02-18 17:28:40.000',True,False,'������� �������� ����������','��� "������ �� �������� ������������"','7456013048','1127456004697','79048100062', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Olga_grabchak@mail.ru', '39417CAA1FDEFAD22E27A3768A3F94A38481580D', 
 '2013-02-18 18:10:00.000',True,False,'������� ����� ����������','��� "����� ������������ "�������� �������"','7328067383','1127328000964','89372703310', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 's.suhanov@center-inform.ru', 'AB4BFCA6D6563C1B760B1ED13F97E2756381A142', 
 '2013-02-19 14:43:21.000',True,False,'������� ������ ����������','���� "�����������"','7841016636','1097746185195','812 303-90-20 02392', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alinez@mail.ru', 'E204F5DEDDA5C2ED6F660D3DCEDBB37275F478E2', 
 '2013-02-19 16:39:01.000',True,False,'��������� �������� ����������','��� "���������������"','7701262328','1027700036210','4956609714', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ljv51974@mail.ru', 'A1AA3A06A4BBE2C7ACF2216D7AE898A4E8FFD64D', 
 '2013-02-21 07:48:31.000',True,False,'�������� ������� ����������','���� �������� ��������','6148011333','1026102158642','89518244660', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lzhuravleva@rt-invest.com', 'F612487F7E291FC011FE03019EF94E753A389CCD', 
 '2013-02-22 10:59:46.000',True,False,'��������� ������� ����������','�������� � ������������ ���������������� "��-������"','7714842767','1117746465000','(495) 660-13-10', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rusreit@gmail.com', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2013-02-25 12:24:12.000',True,False,'������� ������� �������������','�������� ����������� �������� "����������� �������� "�������"','7707723783','1107746284634','726-45-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@gpb-capital.ru', 'D159EDCE4E074771BA695B14E5D2158A3BD9D207', 
 '2013-02-27 11:39:22.000',True,False,'������ ������� ���������','��� "��� �������"','7722557072','1057748346985','(495)988-23-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'darya_l@mzpro.ru', 'D7560842DF44E9FE9792C854682B5A261DC0D70F', 
 '2013-02-28 15:45:47.000',True,False,'������� �.�.','��� �� "��������"','5905280096','1105905004798','89128805208', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Inertek@sp.ru', 'A2AC912B553456A600DD8D59BCCA4D6695DC379C', 
 '2013-02-28 16:49:14.000',False,False,'������� ����� ����������','�������� � ������������ ���������������� "��������� �������� "�������"','7835003420','1047811024062','-1294', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'abasova@gefest.ru', '9B156A592F36C94CC6A023225D320D66BE8B49DA', 
 '2013-03-01 10:54:22.000',False,False,'������� ����� ����������','�������� ����������� �������� "��������� ����������� �������� "������"','7713101131','1027739214777','+7(495)782-05-86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'garmaev.angarsk@mail.ru', 'A0487E3C95AAAF46492B9B04116ABD19CEF8EC33', 
 '2013-03-02 08:10:39.000',True,False,'�������� ������� ����������','��� "������� +"','3851998857','1133850007961','8-950-104-55-34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'izubalii@gmail.com', '9921C3DE700C1A8C8536FCBAC7BCBDFE96542584', 
 '2013-03-05 12:16:07.000',True,False,'������� ���� �������������','��� "�����������"','7724851390','1127747074729','8(495)984-05-38', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zaoktc@mail.ru', '46CDF3D8BD6818CD512E8C98D2CDC472C3F180D0', 
 '2013-03-06 08:55:16.000',True,False,'��������� ������� ������������','��� "���"','3305715390','1123332001715','8(495)6436976', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fsurazh@mail.ru', '864FDA85310D0B92A82E4BFD9D390CB0ED117DBB', 
 '2013-03-06 11:35:13.000',False,False,'���������� ������� ������������','���� ������������������� �.�����','3253500970','1113256021053','8(48)330-2-60-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'serviceuc@belregion.ru', '2D5F7019A2172B258BA30C29DDD9A6D97DFC3DB1', 
 '2013-03-11 10:26:00.000',True,False,'���������� ������� ��������','���� "������������ �������������� ����"','3123077111','1063023014010','+7(4722) 31-39-24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'PetropavlovskiyV@yandex.ru', '18B563C14D3A06ABADAD11173441401EEDE5FF54', 
 '2013-03-11 11:43:41.000',True,False,'��������������� ��������','��� "����� ������"','7730084599','1027700067680','8(903)1311117', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'iaroslavtceva@paritycapital.ru', 'FB3A14887628D84B7C15E496C99060735372300C', 
 '2013-03-13 09:27:33.000',True,False,'����������� ���� ����������','��� "�� ������ �������"','7703736037','1117746033558','+7 (473) 200-82-48', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'belkina@ra-am.ru', '0676C2102A2414CD671126154EB72D978718A2E9', 
 '2013-03-14 10:44:32.000',True,False,'������� ������� �������������','��� "�� "��������"','7716721373','1127746574944','+7(495)765-2992', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond@novotroitsk.org.ru', 'D731ECF00BABD13F8AA215DC2935139C67CFD849', 
 '2013-03-14 11:09:26.000',True,False,'��������� ����� ���������','���� ��������� ������ � �������� ������������������� �������������� ����������� ����� �����������','5607018209','1045603206725','8(3537)67-49-72', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fcbp@yandex.ru', '7477D1698DFB522367CB0CF659B0C3D1912DC0D5', 
 '2013-03-15 07:12:37.000',True,False,'�������� ������� ������������','��� "������������ �������"','5902230072','1125902009200','89028325154', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'infomoneymarket@mail.ru', '9120A3C8A0EF99CCD4A2DF5400ACE26C839C84FC', 
 '2013-03-15 12:27:04.000',True,False,'�������� ������� �������������','��� "����������"','6452103122','1136450003260','8-917-326-2728', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'M_Fedotov@npfuvz.ru', '420D261BD3B2DE0C1F32DA8BCE58F645A5CC3307', 
 '2013-03-15 13:55:18.000',True,False,'������� ������ ���������','��� "�������������������"','6662062747','1026605393165','(343)269-44-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ignatyev@rk.t72.ru', '313927B0EC43915B6A6EA1927346E9E4A99A9084', 
 '2013-03-18 09:22:51.000',True,False,'�������� ������� �������','�������� ��� ��','7204141323','1097232001701','89220022041', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mira30.kaz@mail.ru', '56908B95423DA43349E1483AFC87DE07E3F7FC9E', 
 '2013-03-18 15:52:15.000',True,False,'������� �����','��� "������"','1661035533','1131690014147','8-9872997604', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'global_fin@mail.ru', '3970043D5C53DAC5D95C2F554E820B3C4BE3C932', 
 '2013-03-20 15:36:32.000',True,False,'�������� ������ �������������','�������� � ����������� ���������������� "�������������-���������� �������� ��������������"','6453123227','1126453004083','8(8452)75-63-30', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rmfpp@yandex.ru', '60E5E4182B3A560AF37B85759327EF1CFE0BF8C6', 
 '2013-03-21 10:13:51.000',True,False,'������� ������ �����������','���������� ������������� ���� ��������� �������������������','6164074612','1026103286593','863 298-81-21 89185588121', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gecha79@mail.ru', '27F595DA3F987208FD1A4770D7FAA6A94B440130', 
 '2013-03-22 15:52:08.000',True,False,'���� ������� ������������','��� "���������� ����������� ����"','7722100825','1037739703121','89104229149', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Ocenka-Altai@mail.ru', '62DB0222912F53D4B889A5C735BF933419BF4654', 
 '2013-03-25 08:04:41.000',True,False,'������ ������� ���������','��� "������-������"','2221201750','1122225017320','8-385-2-57-25-17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kspizumrud@mail.ru', 'D26D78A2AC86DB5641D1B02BD5587406C57AD19A', 
 '2013-03-25 08:58:56.000',True,False,'������� ��������� ������������','��� "�������"','2466229946','1102468023723','8(391)2921064', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '4532879@mail.ru', '5B83304C2234B91558F4E9317952A4A014975615', 
 '2013-03-25 13:48:02.000',True,False,'�������� ����� ����������','�������� ����������� �������� "���������-�������������� �������� "��������������" ','7708099247','1027739269194','8-910-453-28-79', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'masha66655@mail.ru', 'E47225B2391D7C01AF06ADEC9F003EC05884FA69', 
 '2013-03-26 09:38:28.000',True,False,'������� ����� ���������','��� "�����"','1216019359','1101224000987','89613744884', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pr@am-ese.ru', '15A90AF062C1523F06600B3EABCB3733D6057A90', 
 '2013-03-26 11:12:54.000',True,False,'��������� ����� ����������','�������� � ������������ ���������������� "����������� �������� "��� ���������� ��������"','7701961492','1127746483732','+7(495)783-09-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'balans@mrggroup.ru', 'C07429EF667D3E93F32E614B3DEA4A62D3DE839D', 
 '2013-03-26 16:03:05.000',False,False,'������� ����� ������������','��� �� "�������� ������"','8901010104','1028900507668','+7(495)649-3010', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@center-am.ru', 'BC3E6195C8251744385366E397EFE4F4DEA08EA0', 
 '2013-03-27 12:39:48.000',True,False,'�������� ����� �������������','�������� � ������������ ���������������� "����������� �������� "���������� �����"','7722775850','1127746356616','(343) 287-38-89', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'msfo@irkutskenergo.ru', 'AFAAD7DC5F65160D01E721E581149E9668BE4133', 
 '2013-03-27 13:30:05.000',True,False,'������� �.�.','��� "�������������"','3800000220','1023801003313','8(3952)790-637', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'usmanova@azkapital.ru', '6DEC6FE04E81C5C93E4BE61733DF5E39C32DA505', 
 '2013-03-28 12:34:32.000',True,False,'�������������� ��������� ��������','�������� � ������������ ���������������� "����������� �������� "���������� � ���������"','6660045619','1036603502363','(343) 341-56-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fincomp.sr@mail.ru', '0CCB0DF5B01FE29EBC9A3FBACE0ABB2740F9248E', 
 '2013-03-28 13:26:37.000',True,False,'����� ����� �������','�������� � ������������ ���������������� "�������"','6454130731','1136454000902','89878121186', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'npf@ckb-rubin.ru', 'FC3F75F0613F3B5FAE329A73F12D78E8CF610DEE', 
 '2013-03-28 16:17:42.000',True,False,'������� ����� ����������','��� "�������"','7826078162','1027810259377','(812)494-18-24', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sistema-l@yandex.ru', '1401E141A5FC3AB3E9301AC0D0C160041374753A', 
 '2013-03-29 10:12:28.000',True,False,'���������� ����� �������������','��� "�������-������"','7802321724','1057810238430','(812) 324-31-36', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zelenko@vsk.ru', 'ECAC24DF7B8CF65B426B65811939C07632F55187', 
 '2013-03-29 10:44:47.000',False,False,'������� �������� �����������','��� "���-����� �����"','7730175542','1047730001813','8 (495) 785-27-76 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gea@rc-rezerv.ru', '5B50ACA656CBFDA9D0CDB739B80BE3FE0F3142EB', 
 '2013-03-29 11:42:35.000',True,False,'���������� ��������� ����������','�������� � ������������ ���������������� "������������ �������� "������"','5008041960','1065047060341','(495) 647-76-86', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'TubolevaAA@uralsib.ru', '144307EFB0236EBED64E5091690317FF67048C16', 
 '2013-04-01 11:11:52.000',True,False,'�������� ���� �����������','��� "�������"','7704221506','1037700015473','89253777622', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mfmr2011@mail.ru', '5F6B7C138DEC6AF9FD2BE57752FEEFD56C243B5C', 
 '2013-04-01 11:12:09.000',True,False,'��������� ����� �������','���� �������� �������� �������������� �������������� ������','7403005484','1037400561021','3513821879', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tatuana_l@mail.ru', 'AA8CCF02C5BA5CF56E1E56952E7F08BD0E05A3CA', 
 '2013-04-01 13:59:12.000',True,False,'������� ������� ����������','��� ����-������','2204052353','1102204005750','8-906-963-71-29', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'potashkov777@rambler.ru', '146814FC0488A7393B6FD6D0142D5F0F54E12270', 
 '2013-04-01 14:30:31.000',True,False,'�������� �.�.','��� "��� ��"','7310103739','1077310002439','79033372508', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elena8680@mail.ru', 'DE3C743F07A31CFF08B7CE78011B9E3E10E0E63A', 
 '2013-04-02 00:26:18.000',True,False,'�������� ����� �������','��� "��������-������"','6330056490','1136330000916','89379922455', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alexey.solozhenkin@unityre.ru', '8AA7376FCD9B146CBA61C9963347C4B8312D598D', 
 '2013-04-02 13:59:13.000',False,False,'���������� ������� �������������','�������� � ������������ ���������������� "��������� � ���������������� �������� �����"','7803034240','1037800007288','4959566589', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bk1@vlink.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2013-04-02 14:41:41.000',True,False,'����� ������ ����������','��� "���-�����"','3445076539','1053460076900','+7(8442)93-16-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aapdigt@gmail.com', '7F3C6CDB23833B3AEF0359CE14F9A7BEB1C45C5C', 
 '2013-04-03 16:32:42.000',True,False,'��������� ������ ����������','��� "�������� ����������"','1215039585','1021200769237','8(8362)33-70-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gs-say@mail.ru', 'C6CAEABAAAF83D3DEE4F9BDAAE9C13E176028E72', 
 '2013-04-04 12:56:31.000',True,False,'�������� ���� ��������','��� "��������������� ���������� ��������"','3010625014','1083022000819','8-927-072-00-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '2918235@ngs.ru', '5A0F23867BBEBD49BD8DC9CEBFDC7227B4F989A9', 
 '2013-04-05 09:21:36.000',True,False,'������ ���� ����������','��� "�����"','5408300169','1135476028082','(383) 291-82-35', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Semenihina@ubrr.ru', '2574D19D4825492CBFA1DB732CD6E5D0FD52126B', 
 '2013-04-05 11:50:42.000',True,False,'���������� ������� ����������','��� "��������� ���� ������������� � ��������"','6608008004','1026600000350','(343)2647372', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'acc69@mail.ru', '02C93AEEA3C27472951B147F668C0FFD0EC8564A', 
 '2013-04-05 14:29:49.000',True,False,'������� ������� �����������','��� "���������� �������� "������-��"','7714111213','1027700089680','84959603504', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@vesta-a.ru', '8301985949299868731F3899910F35AF46AC7A17', 
 '2013-04-08 10:47:34.000',True,False,'��������� �����','�������� ����������� �������� "�����"','7704757505','1107746495108','+7(905) 515-4578', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@priam-a.ru', 'C996C013B630AE3DB9D4ECEE3F4483DFC99D5985', 
 '2013-04-08 10:50:13.000',True,False,'��������� �����','�������� ����������� �������� "�����"','7704757470','1107746494646','+7(905) 515-4578', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'request@stockterminal.ru', 'ED0D6B8EAE474CF4F98F3BA0028162D77701CD8D', 
 '2013-04-08 13:54:23.000',True,False,'����������� ��������','�������� ����������� �������� "���������"','7726586540','1087746092774','(495) 767 81 37', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'europe@lobnya.com', '9AE3B82137E35859B88B37EEC9DE680A922F0E44', 
 '2013-04-10 10:01:15.000',False,False,'������� ������� ���������','��� �� "������"','5025012060','1025003076548','84955791802', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dinara@kreditech.com', '79065709C8CF094E9EAE20ACD73BBB968C5ED1DD', 
 '2013-04-10 11:42:21.000',True,False,'��������� ������ ���������','��� "�����"','5501246928','1135543003793','79152077770', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'che_dv@torg-fin.ru', 'B68DA6E9002D9028C4B1BD04FD517756F7896F76', 
 '2013-04-10 11:49:38.000',True,False,'������ ������� ����������','��� ����� ���������� ����� "�������" ','7730644191','1117746368112','89151516125', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'general@micro-cash.ru', '58E98EE648F9379C584FA836C40904E0777D4089', 
 '2013-04-10 17:23:07.000',True,False,'���������� ������ �������������','��� �����������','7719821697','1127746719022','84959992837', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'garipova@npfsr.org', '4BB36E080EC4BD773CE3F838B5CE02226D05CFB7', 
 '2013-04-11 06:31:55.000',True,False,'�������� �������� ����������','��� "��������� ������������"','2466253522','1122468043653','8(391)2273384', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'co01-4@bsoinsur.ru', 'ED5F826393F16468F46D7CDCFBCEE7E53A183D88', 
 '2013-04-11 11:15:17.000',False,False,'���������� ����� ����������','��� "���������� ��������� ��������"','7714034590','1027700533145','8(495)540-5110', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Razina19860503@gmail.com', '4FC71DAB19D7F99AC5897F7538C426DF18DC7613', 
 '2013-04-11 12:23:20.000',True,False,'������ ������� ��������','��� "������"','267015985','1100267000987','8-964-959-99-99', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dengy.tyt888@gmail.com', '96CC9A7048354EE90FC2E3CD5C954816C7DDB2B1', 
 '2013-04-11 13:22:27.000',True,False,'������� ������ ���������','��� "�������������"','2311138686','1112311007456','88612786230', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'feniks-finans@mail.ru', 'ACF48F37AE3873D5E41A94D33E3605B691EF4437', 
 '2013-04-15 14:27:57.000',False,False,'���������� ����� ������������','������-�������������������','4632166897','1124632010403','79202036010', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gracia_biysk@mail.ru', '353659067C751F4948F0BD6EBAD3F1FB71B8F31D', 
 '2013-04-15 17:02:35.000',True,False,'������ ����� ����������','������','2204060026','1122204004581','89627956201', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'hil9@mail.ru', '0D19A8982EA771A728FB6C7AF61F9389424F0206', 
 '2013-04-15 17:04:59.000',True,False,'������� ����� ����������','��� �����','1215111094','1061215079859','8362 73 17 08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zi2zi@yandex.ru', 'BAA4809C41CD28DC6224859735019FE3690AA6D3', 
 '2013-04-15 18:18:37.000',True,False,'������ ���� ���������','�������� � ������������ ���������������� "������ �� �������� ������������"','7207023783','1137232016569','89048882928', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lipovavb@yandex.ru', '86D608FF55F9C2CB590BDA0C3EA2DE589043D3C0', 
 '2013-04-16 10:19:16.000',False,False,'����������� �������� ���������','��� "�����������"','3456000098','1133456000094','8-903-468-17-28', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ooo.dnd@yandex.ru', '85EA7203D257CBB5A8E5C78E187273EB4E84D4F4', 
 '2013-04-16 17:25:04.000',False,False,'������� ������� ���������','��� "������ �� �������"','2902074815','1122932008583','8(8184) 56-42-21', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@vcca.ru', 'AD12E2D33526DD6CEAC5D1698E3311623DCFB446', 
 '2013-04-17 07:19:18.000',False,False,'�������� ����� �������������','�������� � ������������ ���������������� "������������������� ����������� ��������� ����������"','6501038550','1026500521740','(84242)75-55-74', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fond.proh@mail.ru', 'CBC3002E7714D723F33A5AEE4E9AA541F441E066', 
 '2013-04-17 13:50:34.000',True,False,'�������� �.�.','������� ��������������� �������������� ������','716002538','1030700153923','89287244083', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buhtiyarova@bihouse.ru', 'A328ED1EF15EEE58AB1996197173ECB5532DDFCB', 
 '2013-04-17 14:18:38.000',False,False,'���������� ������ �������','��� ���','7734249643','1027734002383','74957555335', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mdv@pharmsynthez.com', '23057826CD2E14AE919FDB08F206DF2B6FBF235E', 
 '2013-04-17 15:18:24.000',True,False,'������������ ������� ����������','��� "����������"','7801075160','1034700559189','8-921-425-0088', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kisf_irk@rambler.ru', '8A746DC23E0892152A7AA667969FB6C2424491D2', 
 '2013-04-18 07:06:52.000',True,False,'������ ���� �������','��� "������ ������ ������"','3812142244','1123850031370','89501134597', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rask@rostel.ru', '89CA5D2FA0BBFD87CB25B84020007CB06A34108D', 
 '2013-04-18 14:21:00.000',False,False,'��������� ������ �������','��� "�� "����"','6164203522','1026103271171','(863) 205-43-01', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sharifulin@bk.ru', '2356CDAB3EE6A1BB9DBA0FE82508248B8DCA261D', 
 '2013-04-20 16:23:08.000',False,False,'��������� ������� �����������','��� "�����"','5906116860','1125906009272','79128874288', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@trustrezerv.ru', '4D92A59E10CA39141206676CC4DCF8BE50DC3615', 
 '2013-04-22 11:54:38.000',False,False,'������ ������ ����������','�������� ����������� �������� "��������� ��������� ��������"','7710578825','1057746547594','+7 (495) 972-23-13', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'marina.sotova@allianz.ru', '08571178AEDBB6C2867369FA825DEBD5D327F73C', 
 '2013-04-22 17:28:40.000',False,False,'������ �.�.','��� �� "������"','7702073683','1027739095438','84959562105', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nkachalkina@yandex.ru', 'ED4FD67B7CCF89DB2763C6628249E94B3DB02449', 
 '2013-04-23 11:33:12.000',True,False,'��������� ������� ������������','��� "�� �����"','5003101630','1125003006150','+7(495)9798818', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'autolen@mail.ru', '780628E063981368AA4A8AD21348F0C41092988F', 
 '2013-04-23 11:51:21.000',True,False,'������� ����� �����������','��� "������������ �12"','7728013537','1027739372220','8 963 691 79 51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rosles@aaanet.ru', 'C32CEFAAD4911C4964B99BDC745785D443A15D99', 
 '2013-04-23 16:13:50.000',False,False,'��������� ������ ����������','��� �� "������"','6165148850','1086165004210','8-863-200-27-17', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'EAndreeva@medexpress.ru', '4156FB81A73EBE68CFB44C69E1B98988C186439A', 
 '2013-04-24 09:46:27.000',False,False,'�������� ��������� ����������','MEDEXPRESS','7803025365','1037843040465','494-94-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zhilkina@mastergarant.ru', '03186D5ECD9816D401CADEAC15C0307FE750D8FC', 
 '2013-04-24 10:14:20.000',False,False,'������� ������� �������������','��� "�� ������-������"','7744000246','1027739068631','84957754992', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'urova2005@yandex.ru', '8D3E350BF691A8332B21F2312EC7568785C0A273', 
 '2013-04-24 10:33:45.000',False,False,'������ ������� �������������','����������� ���� ��� "��� "����-���"','3666114486','1043600049041','47322553213', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ergofsfr@ergorussia.ru', '1BB643BBD3F33A1F8AFB0AA5379D1C7649BE3926', 
 '2013-04-24 17:13:28.000',False,False,'�������� �������� �����������','�������� ����������� ��������� �������� "���� ����"','7815025049','1027809184347','78126002050', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'AMinin@ncsp.com', '3F83F110461C3E7EDEDEC465D6AA82B425219735', 
 '2013-04-25 10:34:17.000',False,False,'����� ������� ��������','��� "����"','2315004404','1022302380638','(8617) 60-49-52', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'E.Lesnyak@leader-invest.ru', '81370DAFDECD5C38744124E1BBF45D17E6FC8DB3', 
 '2013-04-25 10:54:34.000',False,False,'�������� ����� ','��� "���-���"','7705821841','1077763816195','(495) 280-02-97', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'tatyanina@lesk.ru', '3FF9F4DEF751850E91E084EDB94B2328A6BACA9A', 
 '2013-04-25 12:33:19.000',False,False,'��������� �.�.','��� "����"','4822001244','1027700058286','(4742)23-73-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'O.Katysheva@leader-invest.ru', '31CAD92AD061103181317A1D9BAB247179CE9C31', 
 '2013-04-25 13:11:32.000',True,False,'�������� ������','�������� ����������� �������� "������"','7726510759','1047796720290','495 280-05-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'I.Terekhova@leader-invest.ru', '31CAD92AD061103181317A1D9BAB247179CE9C31', 
 '2013-04-25 13:21:55.000',True,False,'�������� ������','�������� ����������� �������� "���-������"','7726510741','1047796720245','495 280-05-60', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '89288800100@mail.ru', 'B74055CA89574AAC702DD144EA6F4B9F4477B0B1', 
 '2013-04-25 13:24:54.000',False,False,'�������� ������� ����������','��� "���������"','2361009541','1132361000155','79384031850', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zaynullina@list.ru', '05949CFCFCDFA8B6AF1960445DA2C1B0613D8B3A', 
 '2013-04-25 15:22:28.000',True,False,'���������� �.�.','��� "�-����"','1655217944','1111690047578','89172618364', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'manakova@vip-polis.ru', 'E77479B2D322474F75ACD7E4E64AF6F30487F22A', 
 '2013-04-25 17:49:47.000',False,False,'�������� ����� �������','��� "�� "�������-�����"','7733500790','1037739806367','8-495-223-77-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dubossarskaya-uv@rao-esv.ru', '39E314FE639283360DFBD14CF89DCE9465C3FB85', 
 '2013-04-26 09:05:37.000',False,False,'������������ ���� ������������','��� "��� �������������� ������� �������"','2801133630','1087760000052','4952876703', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'invest@invest-telecom.ru', '200BC445226E89264FD34F2A3E470DE25AAF624B', 
 '2013-04-26 11:04:57.000',True,False,'��������� ��������� ����������','��� "������-�������"','7719241103','1037739032781','+7 (495) 961-24-81', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'krylovamv@tmk-group.com', '4C780C4636A8A88205D8ABB50E075592C37D968F', 
 '2013-04-26 11:16:07.000',False,False,'������� ����� ������������','���','7710373095','1027739217758','74957757600', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kruk@dvec.ru', 'DF80E9491004A0E259ECFDEA17BC37586D19EE04', 
 '2013-04-29 05:31:45.000',False,False,'�������� ������� �����������','�������� ����������� �������� "��������������� �������������� ��������"','2723088770','1072721001660','(423)26-57-301', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'MNV@nat-in.ru', '6287C1124DE84A2C3CABC471057EA0E37E6EA1B1', 
 '2013-04-29 12:50:22.000',False,False,'������ ���� ����������','�������� � ������������ ���������������� "������������ ��������� ���"','5024011970','1025002871332','8(495) 691 8247', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sk_allega@mail.ru', 'B02600C83A4789A00DE9A5723474E6513271B4D6', 
 '2013-04-29 18:09:38.000',False,False,'��������� ������ ����������','��� "�� "������"','5029066282','1025003520519','74952235497', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'multipolis@mail.ru', '24EE8470E807BDB5F42F332D55233E29138A15A3', 
 '2013-04-29 18:24:30.000',False,False,'������� ��������� ������������','��� �� "�����������"','7703237060','1037739710997','74952311143', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oxana@investstrakh.ru', '701092E3B8871D044DBAB3E32F0FD9FC67078984', 
 '2013-04-30 13:43:29.000',False,False,'�������� ������ ����������','��� ��� "�����������"','7707043450','1027739149547','74956786262', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oao@rosset-kzms.ru', '5CF12CD1613E34206BA0EFA8A199C700755D3077', 
 '2013-04-30 13:52:38.000',False,False,'�������� �.�.','��� ����','5916000015','1025901844132','(34273)20374', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ZaharovaMK@kuazot.ru', '9480A0F8270E629CC63A65BDDA897C477C3B02AD', 
 '2013-04-30 16:50:50.000',False,False,'�������� ����� ��������������','��� "������������"','6320005915','1036300992793','8(8482)561689', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kozlitina@366.ru', '110770CE01B92E4C63FF7B9B07CB30B634684B2C', 
 '2013-04-30 17:57:12.000',False,False,'��������� ������� ����������','��� "�������� ���� 36,6"','7722266450','1027722000239','74957925209', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mkrylova@scor.com', '83C5216E8BC90CA3539D0845A28C9211B1CA33DA', 
 '2013-05-06 14:32:52.000',False,False,'������� ����� ����������','�������� � ������������ ���������������� "���� ���������������"','7710734055','5087746664814','8 495 660 93 65', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bonumcap@gmail.com', '777D3B4004A7DD177F6236B5712161BF89D5E977', 
 '2013-05-06 17:06:27.000',True,False,'������������ ������ ������������','��� "����� �������"','7722757160','1117746760130','+7 (903) 551 0816', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'luz-garant@mail.ru', 'C9CAB08729AE4F6E2BC94D42E27222CAA9714587', 
 '2013-05-07 16:07:46.000',False,False,'��������� ��������� ����������','�������� ����������� �������� "��������� �������� "������ �"','7704038451','1027739235963','(495) 792-31-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'accounting@revoup.ru', '2CE948D8F6E8D70C269B89DE3E9D5209183413D0', 
 '2013-05-07 18:07:39.000',True,False,'����� ������� ������������','��� "���� ����������"','7724856208','1127747215133','+7 (926) 837-53-30', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'service@saprins.com', '1F20A5F550585BC716F27ED80F6C15CABE1FBEFC', 
 '2013-05-08 12:07:55.000',False,False,'������ �������� ��������','��� ""��������� �������� ������� ������ �������������"','7726298100','1027739033244','+7 499 559-99-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ashildt@e-portal.ru', '59BDF3E92E2E9505D456EFD990A87563ED976EDA', 
 '2013-05-13 13:54:47.000',True,False,'������ ������ ������������','��� "�-������"','5503065726','1025500739384','79136309157', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'admin@ckbasu.ru', '94015A32778A6C536DF7E35FFA4E5A25FE053DDC', 
 '2013-05-13 17:05:51.000',True,False,'���������� ����� ������������','��� "��������-��� ���������������"','7723007466','1027739553291','84991794256', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lipetsk.su5@gmail.com', '818886EB93196E7EA177A8C43E42D87DC01C3BF4', 
 '2013-05-14 08:30:54.000',True,False,'��������� ����� ����������','��� "�������� �����������"','4823056775','1134827001627','(4742) 25-89-05', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ooo.investholding43@mail.ru', 'B0BB8D2C020FE29F464687FBAA183983182B840D', 
 '2013-05-15 08:59:09.000',True,False,'������������ �������� ����������','��� "�������������"','4329014501','1104329000390','79091407017', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ucsdo@sdo.ru', '9A2BD5E3E2FAD20D58454C5D06E990FB5BC2884F', 
 '2013-05-15 09:22:30.000',True,False,'������� �������� �������������','��� ���','6905067250','1026900512176','(4822)509961', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'user_rm3@su155.com', '66B3F609AC711A30C106C313D26DED43B40ADA30', 
 '2013-05-15 10:27:08.000',True,False,'������ ������ �������������','��� "�� "���� ����������"','7708770000','1127746720056','(495) 967-14-92 4160', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga@irbis.tv', '1839F9B90B7A0D4D2CC5971629E96E5CE744867B', 
 '2013-05-15 11:11:45.000',True,False,'��������� ����� ����������','��� "�����"','6950105771','1096952018426','(4822)71-02-50', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'komplinov@bias.ru', 'F35D1BCC52DD08204861EC6DD83D0E2BA562B8A6', 
 '2013-05-15 12:27:12.000',True,False,'��������� ������ �������������','�������� � ������������ ���������������� "���������� �������������-������������� �������"','7728706436','1097746449613','79265891463', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ib-nalchik@bk.ru', '31BF5BFA5C3188011D2A095AC3D512034409C9B5', 
 '2013-05-16 10:07:45.000',True,False,'���������� ��������� ����������','��� "��� ����-���������"','721009708','1040700231791','(8662)753198', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ooo.regionyug@mail.ru', 'B61988515D2A02B6FFDE0940DD21884F1AA9D79E', 
 '2013-05-16 14:56:28.000',False,False,'������� ����� ����������','��� "������ ��"','2311145027','1122311003847','89882482882', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buh@ribank.ru', 'EA4D5D3A0C60B775441E4AE4FE212D18B46ACF66', 
 '2013-05-16 16:37:30.000',True,False,'�������� ��������� ����������','��� ���� "���"','7704019762','1027739588205','8 (495) 232 34 34', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'coldream@yandex.ru', 'FA3873DC4862CD94D0ED5B300F0F23D1A651D92C', 
 '2013-05-17 11:54:53.000',True,False,'����� ������� �������������','��� "���"','1701049729','1111719001448','(39042) 2-19-61', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@urezerv.ru', 'AD1F8B614B647811CBCF04395AAF7122DD9E0C68', 
 '2013-05-20 07:53:43.000',True,False,'�������� ����� ����������','��� "������"','2209023910','1022200815812','8(38557)59998', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uralsoyuz.ru@mail.ru', '6653148036AB1D31BF249EFB069E28ABBE6C66E4', 
 '2013-05-20 08:17:01.000',True,False,'������� ���� ������������','�������� ����������� �������� "����������� ����� "��������� ����"','7705040076','1027700302310','8-495-2320204', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@uralsoyuz.ru', '6653148036AB1D31BF249EFB069E28ABBE6C66E4', 
 '2013-05-20 08:20:20.000',True,False,'������� ���� ������������','�������� ����������� �������� "��������������� "��������� ����"','7716103289','1037700021765','8-495-2320204', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ural_buh@mail.ru', '6653148036AB1D31BF249EFB069E28ABBE6C66E4', 
 '2013-05-20 08:26:01.000',True,False,'������� ���� ������������','�������� ����������� �������� "����"','277027813','1020203089037','8-495-2320204', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'batabata@mail.ru', '430CE9404C16B8AD1C30F7642DC0FD07BB40B962', 
 '2013-05-22 13:29:11.000',False,False,'������ ����� ������������','��� "��������"','813005781','1130813000229','89272836502', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Staryh.YD@mrsk-1.ru', '0125EFAC2306774A1D4E07E890615C8B86E807E7', 
 '2013-05-23 10:14:53.000',True,False,'������ ���� ����������','�������� ����������� �������� "��������������� ����������������� ������� �������� ������"','6901067107','1046900099498','79197860097', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sales@skynet-kazan.com', 'D79FC8CA79B686BC3C2774EA395E0EA930015B22', 
 '2013-05-27 14:23:26.000',True,False,'����������� ����� ����������','��� "�����������"','1655202761','1101690062088','843 5 620 640', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info.uk.optimus@gmail.com', '126234404EEF74C1E766677B9EBAC755E00ECAB0', 
 '2013-05-27 15:18:01.000',True,False,'�������� ��������� ���������','�������� � ������������ ���������������� ����������� �������� "�������"','7705969125','1117746929508','+7(495)966-23-73', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'victor-cepelev@rambler.ru', 'D9FDF7472AA71DAF05A79A763E15B1EF86D74793', 
 '2013-05-28 13:02:27.000',False,False,'������� ������ ����������','��� "������������ ��������� �������� ����������"','5503054210','1035504002467','(3812)30-47-70', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'investfinans.ryazan@gmail.com', 'C33E5FA377756419E5B1C5A95066866D7A55314B', 
 '2013-05-28 21:36:32.000',True,False,'�������� �����','��� "������������"','6234114283','1136234002992','(4912) 51-12-83', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Valy89@inbox.ru', 'B79B500657677C44F922AE5C0899D7BA2DA1DA13', 
 '2013-05-29 06:09:11.000',True,False,'������� ����� ����������','������','5405473420','1135476068640','89134629609', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'garantia.plus@yandex.ru', 'ECEED011B344933B9DD59654BC0FD05469C2DDF2', 
 '2013-05-30 04:34:13.000',True,False,'��� ����� �������������','��� "�������� ����"','2204055629','1112204003361','9627968816', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a_prikhodchenko@rosneft.ru', '8F90BE7AE938824C1B16026460C9B9695A2215BD', 
 '2013-05-31 14:19:13.000',True,False,'����������� ������� ����������','��� "�� "��������"','7706107510','1027700043502','+7 499 517-8888 (3540)', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'o.petrakova@aresbank.ru', '40CDE29054AECF7BA7C31706B64788A70F3AFDEB', 
 '2013-06-04 15:41:31.000',True,False,'��������� ����� ���������','��� �� "��������"','7718104217','1027739554930','795-3288 ', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@vrca.ru', '6BB492596FD48638062216081C0810AF013181EE', 
 '2013-06-06 14:31:25.000',True,False,'�������� ������� ��������������','��� "���������� �������������� �����"','1655073770','1031621013962','(843) 533-88-33', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'support@profi-uc.ru', '278850D97963775D8C37F9926569678A1EA74EDA', 
 '2013-06-06 16:00:25.000',True,False,'������ ������ ������������','�������� � ������������ ���������������� "�������������� ����� �����"','3702626281','1103702019970',' (+7 4932) 58-68-78', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'expressznk@yandex.ru', 'DE8D27738B5B271142945E30ED400D4969AFE23F', 
 '2013-06-07 11:36:02.000',False,False,'������ ������ ����������','��� ��� "��������-�����"','4217153754','1134217003800','73843200771', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dostupzaim@mail.ru', '130B1978DE1A99897AD9AF8E822C33EB1DDE50E6', 
 '2013-06-08 15:31:32.000',True,False,'�����','��� "������� ������"','3664120646','1123668038669','89034200700', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@vneshprominvest.ru', '649365C139B3A67F81D106816C57A20EB160AA9B', 
 '2013-06-10 10:27:51.000',True,False,'���� ����� �����������','�������� � ������������ ���������������� "������������������"','7704216810','1027700439821','(499) 245-96-49', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dankrasnodar@mail.ru', 'ED9E85CEFFBA522961F4D6A8705C1852D89EB9E6', 
 '2013-06-10 14:26:28.000',True,False,'�������� �������� �������������','��� "���"','2308056842','1022301212339','(861) 253-96-71', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stars@vrnt.ru', '230FD523724EC2BD98C4940F8E31973D3888EAF4', 
 '2013-06-10 15:35:47.000',True,False,'�������� ��������� �������������','��� "�� "���������"','6670065195','1046603524076','(343)2220203', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'oov@rutp.ru', 'A3B2327F47A480E77271209E2C5DEB0912A34B24', 
 '2013-06-11 12:52:56.000',True,False,'�������� ����� ������������','OOO "���"','2225096425','1082225007875','8-3852-271-110', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'invest-centr18@mail.ru', 'E5502E79C84849DADBF4ED5C51B51286738649DD', 
 '2013-06-17 09:16:06.000',True,False,'������� ������ ����������','��� "������-�����"','1832103507','1121832006449','79225000123', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a.kislov@razdolie.ru', '2D38C515628AA8EBA2A67D171228A526F6DE51FC', 
 '2013-06-17 10:29:42.000',True,False,'������ ������� ���������','��� "��������"��������"','7325057740','1057325098160','8 (8422) 41-28-82', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'radchenkoav@uecmo.ru', 'A262A80CFF36167B7FB3C4E516ABDCED8AAD8668', 
 '2013-06-18 08:26:37.000',True,False,'�������� ������� ����������','��� "������������� ����������� ����� ���������� �������"','5024115289','1105024006230','79162346049', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@dk-ip.ru', '7F3093EE4EB71D37BB94F7ACF01FD1A0907FA4D1', 
 '2013-06-18 10:08:13.000',True,False,'������ �.�.','��� "���"','7725776315','1127747199183','(495) 660-62-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'taklimova@urfinis.ru', '4C0722A87BF7248280C9722FA9B8C072079315F2', 
 '2013-06-18 16:23:47.000',True,False,'������� ������� ����������','�������� � ������������ ���������������� "������� �������"','7723593549','1067760298462','79031196717', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'em.petr@ya.ru', 'B70B4975F9350A0088EE1272CAAE3425158E408A', 
 '2013-06-18 21:26:07.000',False,False,'��������� ϸ�� ����������','��� "������"','3456000468','1133456000501','79047776344', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@lissi-crypto.ru', '85A8866A7CED2629382C9B411699334FC07089F4', 
 '2013-06-19 12:13:40.000',True,False,'�������� ������','��� "�����-����"','5054090835','1095018003420','+7(495) 589-99-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'finedvajzer@mail.ru', '1051C6AD90138CBF8E66E5FE7F575ECC9AD5823A', 
 '2013-06-19 13:48:59.000',True,False,'������ ������� ��������','��� "�����������"','5902226118','1125902006053','8-(342)-259-30-35', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'onza.07@yandex.ru', '7CBBCBD360D72BBC1EF2FD90319EFBCE0C02FC3F', 
 '2013-06-20 05:53:45.000',False,False,'������ ����� ����������','���� ��������� ������ ������������������� ��� ������������� ���������� ������� ���������� ����','1707002822','1021700595510','8-929-315-65-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'aruna-84@mail.ru', '0DA59FF72C15F67EDFC6F115A5868DFDBDB13A29', 
 '2013-06-20 08:37:11.000',False,False,'�������� ����� ����������','��� "�������� �������"','411153589','1110411000864','8(388-22)4-73-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'partnerplus2008@rambler.ru', 'C3C912A11CC09A91F2D72ACC99CADC078FD31E94', 
 '2013-06-21 09:33:27.000',False,False,'������ ������� �����������','��� "�������+"','4214028547','1084214000133','8-904-964-02-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'damdengi@bk.ru', '2FC1EE1C172E746A4791751923168ACBB8A0D344', 
 '2013-06-21 10:20:06.000',False,False,'�������� ����� ���������','��� "����"','1838011250','1121838001240','79128720200', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'semicheva@lexgarant.ru', 'ABA3BE145C0634C96D62289FD7EEF1EFE7ECE754', 
 '2013-06-21 14:03:57.000',False,False,'�������� ������� ���������','���� "����������"','7707086608','1037739019911','+7 495 621 98 11', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'makarov@ogi-group.ru', '2303A13087DE01A7DCBBB92BE237E41B2EEFFD7F', 
 '2013-06-21 20:05:27.000',False,False,'������� ������ ����������','��� "������ ���"','7719806650','1127746211625','89163014433', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'novi.dvor@mail.ru', 'DFB795E5ED2DB36B6D8576290FE9CD67F53F7360', 
 '2013-06-24 14:07:37.000',False,False,'������� ����� ����������','��� "����� ����"','5406745300','1135476073050','8(38452)20121', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'korvest@yandex.ru', '12A2BADB6314680B21C8309669A7B24FAC4DDB00', 
 '2013-06-24 16:53:44.000',True,False,'�������� �������� ����������','��� "�������"','7707286950','1027700486967','74994002653', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'a.kotysh@yandex.ru', '5F6B7C138DEC6AF9FD2BE57752FEEFD56C243B5C', 
 '2013-06-24 17:28:37.000',False,False,'����� ��������� ������������','��� "������ ������"','1001269574','1131001005508','89052999727', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lightkredit@mail.ru', 'F422722681BA2BB34A03502654DC9131228FCB51', 
 '2013-06-24 18:13:59.000',False,False,'����� ����� ��������� ','���������� ','2465271906','1122468026823','89620716436', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kdl@mascom-dv.ru', '440A6686CC4770D4B84FC60E284942E0CCDDCEAD', 
 '2013-06-26 09:24:22.000',True,False,'���� ������� ����������','�������� � ������������ ���������������� "��������������� ������������������ ����� ������������ ���������� "������"','2721110853','1042700129504','8(4212)454634', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'skpkmih@yandex.ru', '3D4B9B8ECD585992F15AB230F4702963566D5522', 
 '2013-06-27 09:33:45.000',False,False,'��������� ������� ����������','�������������������� ��������� ��������������� ���������� "������������"','3416031090','1023405568042','78446325047', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kpkg_fk@mail.ru', '5761B50DA3DFDA4913C6E31DA4B257B9621A86A6', 
 '2013-06-27 10:45:25.000',False,False,'������ ��������� ���������','��������� ��������������� ���������� ������� "������ ������""','4230006999','1134230000145','79235077176', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alagarcredit@mail.ru', 'AE2FF2C200D8443A1E15DBA4F5DF4173A4663DCF', 
 '2013-07-02 02:44:08.000',False,False,'������������ ��������� ���������','�������������������� ��������������� ��������� ���������� "������"','1430008765','1061430001742','84115144079', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'uc@cib46.ru', '8BBE2D7D17E496323EAC0ED40945FDB047633356', 
 '2013-07-02 09:57:07.000',True,False,'�������� ������� ����������','��� ��� "���-������"','4632073554','1064632052506','8(4712)77-03-04', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mfk2013@mail.ru', '8CE4513640BD05EA7235B0D5FC50276E87727936', 
 '2013-07-02 15:17:51.000',False,False,'������� ������� �������������','���"��� ��������"','4632176493','1134632003945','8(4712)77-03-23', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olesya-chuprova@mail.ru', 'B05D272AB3B3C8DE26A83E3BAC41E2D27663DC8D', 
 '2013-07-02 18:18:19.000',False,False,'������� ����� ��������','��� ���������','2808112742','1132808000368','89145962692', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vggs@vistcom.ru', 'EFC73948351669A3982F9EACC487F09758D5D57A', 
 '2013-07-03 10:49:54.000',False,False,'����� �������� ����������','��� "������������������������"','3434000337','1023403434625','(8442)234097', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zao-intern@yandex.ru', '6D9AB79B07AA3EA20B7D1E38ADBB6FAA71EB03F7', 
 '2013-07-03 14:08:16.000',False,False,'������ ������ ���������','��� "������"','7806018655','1037816018019','3872355', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'keysystem@2tbank.ru', 'DD65966896D98B7E64EAEBA5C4563AAFB6D94569', 
 '2013-07-03 14:28:56.000',True,False,'�������� ���������� ��������','����-�','2315126160','1062300007901','(495)587-78-88', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '2860676@gmail.com', 'A6BBFCECCF7A8E2B1DAC5904ED7047487E65E7C4', 
 '2013-07-03 22:10:49.000',False,False,'������ ������� �������������','��� "������-������"','5904287885','1135904007370','79526560676', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@parc.ru', 'E54E709772FCB2B784605E1D4259EA0858DD2847', 
 '2013-07-04 11:39:52.000',True,False,'����� ������ ���������','��� "��������������-�������������� ����� "�������"','6454066437','1036405414330','+7(8452)753333', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'edo@sdkgarant.ru', '1D2ABC90EF5E1DDCBBC97728C8333707DF4D6700', 
 '2013-07-04 14:31:33.000',True,False,'������ ������� ����������','��� "����������� �������� ������"','7714268951','1027739138217','84957775683', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'business@inno.mgimo.ru', 'F6AE4390028E6A6E7BF7C7C37C33A29914324555', 
 '2013-07-04 15:24:49.000',False,False,'�������� �������� ���������','�����','7729134728','1037739194217','8 (495) 434 91 53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@rostovdepo.ru', 'D12E2D306D83FF97FD283DD39EE16FA972B0A851', 
 '2013-07-04 16:27:39.000',True,False,'��������� ��������� ������� ','�������� � ������������ ���������������� "����������� ���������� �����������"','6164301424','1116164000215','8(863)268-83-93', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'elgorod@elkursk.ru', '0C974A28977896E7911DE705859602D685763915', 
 '2013-07-05 13:40:44.000',True,False,'������ ������� ����������','��� "����������� �����+"','4634008800','1064613002618','(4712) 545-575', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'mikro.finans2013@yandex.ru', '5F9FE0BA40746351D60990C96826D70A92ADBF5A', 
 '2013-07-08 16:25:03.000',False,False,'�������� �������� �������','��� ��� "�����������"','1510016899','1131510000203','89280748452', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'sargy_pink@mail.ru', 'C9C248BD171EA58A784662C59A320BB983FD1D4E', 
 '2013-07-10 04:10:29.000',False,False,'�������� ���� ����������','�������������������� ��������������� ��������� ���������� "������"','1430008719','1061430001401','84115124549', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'd.zaim@mail.ru', '9216EB5797D96C7DDFAE89039A18201031D1F302', 
 '2013-07-10 09:17:08.000',False,False,'������� ����� ����������','��� "������ ����"','5603039776','1135658013578','89228028080', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lebedev@narodnayakazna.com', '5D7B3D5B0A51BE1C2CB8A2A92F1DF5817B89C848', 
 '2013-07-11 16:15:16.000',False,False,'������� ������� �������������','��� ��� "�������� �����"','7705944339','1117746196501','+7(985)769-5030', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buhxp@mail.ru', 'EFF2963388E43DF426305425EF4591BCE1888BCA', 
 '2013-07-14 18:33:56.000',False,False,'������� ������','��� "������ �������"','6670350001','1136670009079','912-602-69-03', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'p.ogurtsov@forpostm.org', '44DBE2BCE2D3A0C8CD81160075E68FECB182BA49', 
 '2013-07-16 11:41:12.000',True,False,'������� ����� ��������','��� "�������-����������"','7701610550','1057747716729','8-916-352-03-76', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vologda-kredit@yandex.ru', 'AE099624C0F26741C79FD19EC8AAFBC75B1AE821', 
 '2013-07-16 11:46:22.000',False,False,'��������� ��������','������ "������� - ������"','3525124308','1033500037537','(8172) 58-08-98', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'lisasilvia@mail.ru', 'C235AC7FF4570A7B83CD798C14CB458CA777FEA8', 
 '2013-07-18 09:22:03.000',False,False,'���������� ����� ����������','�������� � ������������ ���������������� "���� ������"','2465291557','1132468024358','8-913-054-54-95', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'depo@nz.ru', '3DFA829D21D842A003D83F4CC59B78B51EF85310', 
 '2013-07-19 12:00:17.000',True,False,'��������� ������� ����������','��� "������",���','5008004581','1025000003830','(495)721-31-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'd.kuzmin@aecgroup.ru', '377E595758A299C36974DC42CD5F064F1F5A421D', 
 '2013-07-20 15:54:54.000',False,False,'������� ������� �����������','��� ��������� �������� ������������','7728757215','1107746998369','74957834836', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'almazova.n@uribus.ru', '77B0E7B19CCCC90A41D4344C4A13E32409FD5319', 
 '2013-07-22 11:58:15.000',False,False,'�������� ������� ����������','��� "��� "������"','6234108265','1126234010990','(4912)24-73-14', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zumorodok@mail.ru', '16DA31D9F438E629E8CE7F9F1A1CD91841250A16', 
 '2013-07-22 14:19:07.000',False,False,'��������� ����� ����������','��� "������� ������"','3444163500','1083444008515','89053363347', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vladislavsemkin@gmail.com', 'AC1C0540E74A9A45FE48FBA074D0DFCED473239D', 
 '2013-07-23 11:34:01.000',False,False,'Ѹ���� ��������� ����������','��� "������ �����"','5528029367','1075528001328','83812666697', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'n.v.ogorodnik@mail.ru', 'DF80E9491004A0E259ECFDEA17BC37586D19EE04', 
 '2013-07-23 13:25:24.000',False,False,'��������� ������� ����������','������������� ���� "���� ��������� ������ ������������������� �.��������-����������"','6612005905','1036600620462','3439) 370-388', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kpkg-s@bk.ru', '0B520F5F694BE264318A0D8A45531DBFD661969B', 
 '2013-07-24 13:45:45.000',False,False,'������ ���� �������������','���� "����������"','4217132183','1114217001524','89236280099', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'novonikkpsk@yandex.ru', '0547F5EB5456891A7C624196240C6C30FC741EBF', 
 '2013-07-24 14:42:33.000',False,False,'���������� ����� ����������','��������� ��������������� �������������������� ���������� "����������������"','3420008343','1023405770959','(84444)6-11-87', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'batoev_87@mail.ru', 'BC4C91F7A3446FA72AD653C2C63079BD31CCD7D3', 
 '2013-07-25 09:01:18.000',False,False,'������ ����� ���������','���� ��������� ������ � �������� ������������������� �� "������������ �����"','306228330','1090300000966','89644076689', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'buh@i-nt.ru', 'DD5DD2C031D64F4FC04EE82E98B65DBD42A7CC70', 
 '2013-07-29 13:46:09.000',False,False,'������ ������ �����������','��� "���"','7713291235','1027700054690','74956004125', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nikitina@mail.ru', 'BA22CFE9647DCDD1B15CD11B9BF835232F0EC8DA', 
 '2013-07-30 10:07:21.000',False,False,'�������� ����� ����������','VITAL-POLIS','7713286644','1027739035466','(495) 741-59-66', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'office@ibg.ru', 'C988190876FB74D15A7F0160CFD43B57CD37C6DC', 
 '2013-07-30 12:33:52.000',False,False,'������� �.�.','��� "��������� ������ ������"','3666068423','1023602616510','4732502050', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'bovto4ka@list.ru', '4ADB66D3C3FBC4AE3915F0CD385DA707611B3818', 
 '2013-07-30 14:10:06.000',False,False,'���������� ������� ������������','��� ��������','8607010974','1138607000410','83466944545', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'azina_44a@mail.ru', '741CE348331A3622C6B525F1CBB9CAA71C9537AF', 
 '2013-07-30 15:26:21.000',False,False,'������ �.�.','��� "���� ������"','1838011877','1121838002439','8(34147)4-05-08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nataljok05@mail.ru', '9C393FCED67D19452386A179CAE0C966A5AD9E35', 
 '2013-07-31 17:31:31.000',False,False,'�������� ������� �������','��� "����� ���������� ���������"','4826069412','1094823012921','+7(904)6934199', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'moreccpuppai@mail.ru', 'DEEBCBF9BABED9AFB820D6E4C519B048D344DB55', 
 '2013-08-01 11:16:23.000',False,False,'������ ����� ���������','�������� � ������������ ���������������� "��"','6682000594','1126682000620','79527280979', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'natalya.s1974@mail.ru', '87769285CADFDFBE248FE4CFFF17F544A846D220', 
 '2013-08-06 06:02:42.000',False,False,'������� ������� ����������','�������������� ����������� "������������� ���� ��������� ������ ������������������� �������������� ������"','4228009657','1064228000913','8   905  994 15 08', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'OOOPIA@mail.ru', '7D5BDF442DD0A647A8F445D15CD50B29A49BF82C', 
 '2013-08-06 11:44:41.000',False,False,'������ ������� �����������','��� "���������� ��������� ���������"','6434014335','1136432000539','9093303895', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kifc@bk.ru', '3A7AD3F1F3295DCDF59FC1CF691FB8C5BE170F24', 
 '2013-08-07 05:03:53.000',False,False,'��������� ������ ������������','�������� � ������������ ���������������� "��������� �������������� ���������� �����"  ','2463241173','1122468058899','79029237331', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Khisayeva@mail.ru', 'A2DAFAE4A1AA953C7B77FF0F1DB54FA274B03790', 
 '2013-08-07 09:01:11.000',False,False,'������� ������ ���������','��� ������ �� �������� ������������ ','273091751','1130280035236','89191454207', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'yaninaoksana@mail.ru', '636100098C0B0EAAD5EA1B0C1490AFFF51D59887', 
 '2013-08-09 14:10:24.000',False,False,'����� ������ �������������','��� "��������-������"','5503208710','1085543050944','8 913 964 13 31', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'julia.vlad2012@yandex.ru', '09DBE3B92411D929B22F87150A1D7BC7945703D6', 
 '2013-08-09 16:13:35.000',False,False,'��������� ���� ������������','�������� ����������� �������� "����������� �������� �����"','5320000979','1025300992530','89517256579', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 '3288155@mail.ru', 'D642C3960383BFFE8CEB4CEC30B593A6755DD1DC', 
 '2013-08-11 13:43:42.000',False,False,'������� ������','����','8622014363','1078622000423','+7(343)328-06-66', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'an0259@mail.ru', '0FCC5E7121D193D44ED27782321E9961E031CC61', 
 '2013-08-12 11:18:36.000',True,False,'������� ����� ���������','��� "�� "����� �������"','7722798920','1137746079107','9179409455', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'glav66@mail.ru', '8EC4A776B10E52057E8D1DB308A7DB90EBA6F745', 
 '2013-08-12 14:18:44.000',False,False,'�������� ������ ������������','��� "������������ ��������������"','5611068749','1135658021542','89033672710', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'nastja@fonmi.ru', '9B232905500EC5B0DE83B772547BC1FA500D28AA', 
 '2013-08-13 12:55:04.000',False,False,'��������� ���������','��� "����� � ��"','7734156396','1027700304422','8-495-621-97-02', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ic_vit@inbox.ru', '3B9F9B952144E15E071A0B18F2966610C9632DED', 
 '2013-08-13 18:04:39.000',False,False,'������� ����� ��������','��� �������','5507212308','1095543020616','79139886176', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'svk_dm@mail.ru', '42445C61A85F17B75AE857490FCA3FC7B2692CE1', 
 '2013-08-14 14:06:53.000',False,False,'�������� ����� ��������','��� "�������"','5007005790','1025001096328','89032566380', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'olga-dmitrov@mail.ru', 'D0853DE0D7526D5D9BB02F67307FBBAB823FBAFD', 
 '2013-08-14 14:13:02.000',False,False,'�������� ����� ��������','��� "���������"','5007005712','1025001101553','89032566380', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'egorkina.anyuta@yandex.ru', 'CA4C1B98BE6D997458451123A2A060423528A9E3', 
 '2013-08-14 18:27:16.000',False,False,'��������� ���� ���������� ','���������� ����','2130094860','1112130014017','8-919-668-38-51', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dneprovsk@list.ru', 'AD4BA27B6D0C19E17C6419F6AFC609CD44D87510', 
 '2013-08-19 07:25:24.000',False,False,'����������� ������� ����������','��� "�����"','326514940','1130327010550','89148460649', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'savkinab@yandex.ru', '447ED4C6A5C4B00AC7AAFEBB66BF225B4F6C11A0', 
 '2013-08-22 08:29:58.000',False,False,'������ ������� ���������','��� ����������','2914003537','1132904000327','89314042580', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Ik_vita_line@mail.ru', 'BF8E706E09F6643939490D8F458ABB4A534F3D1B', 
 '2013-08-22 12:03:03.000',True,False,'������ ������ �������������','��� "�������������� �������� "����-����"','6164309550','1126164016329','(863)256-39-44', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'avangardinvest@mail.ru', '584162F07AA256147A19982D03EC89AD1CF83E73', 
 '2013-08-22 12:58:31.000',True,False,'������ �������� ����������','��� "�������� ������"','7604233554','1127604016165','74852680963', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'kravchenkokrav@rambler.ru', 'DDCA2CD649E99A5F864CAD349C34B1D2E630B40B', 
 '2013-08-22 15:20:47.000',False,False,'��������� ������� �����������','�������� � ������������ ��������������� "����-�������"','6164259483','1066164208461','89185196784', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dogovor@360arz.ru', '18D1B2DC995CB6CBAA201D55C29070C56F151257', 
 '2013-08-23 09:51:58.000',False,False,'�������� �������� ����������','��� "360 ���"','6229059220','1076229003124','89679614245', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Natalia.Ostrovskaya@am-alemar.ru', 'D7D82797B49B91DE10F55F1BC14F49CCDA2E91B2', 
 '2013-08-23 15:30:59.000',True,False,'���������� ������� ���������','��� "�� "������"','5405175576','1025401902514','499 653-56-20', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pdolnikov@yandex.ru', 'B362FD12DD823D0FFBF9166ADB0D8DC6D9D5F102', 
 '2013-08-26 22:33:50.000',False,False,'��������� ����� ���������','��� "���������� ��������"','5010036534','1085010000360','(495) 543-86-96', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stiz-rossiay@yandex.ru', '58EEA67FF50DDF756825257B365F7C6C1F516314', 
 '2013-08-29 13:44:23.000',False,False,'������ ����� ������������','��� "������"','2631801535','1112651025431','8-8652-76-26-00', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'dadol@bk.ru', '74A884A56D174C93094232F2ACCDACA880DEB9F2', 
 '2013-08-30 20:21:35.000',False,False,'������� �������� ����������','��� "���+"','5614065320','1135658025931','+7(912)341-12-12', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pdk5@mai.ru', '55B776DD366EE4E8D05FCC1376607D2FF0832EAD', 
 '2013-09-02 14:11:52.000',False,False,'������������ ����� ���������','��� "���������� ���������� ��������"','5905266951','1085905009630','89630120447', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ariman02@mail.ru', '5EFE32FCEFF36EC02B330192265E7F4FB0EAF006', 
 '2013-09-05 13:57:28.000',False,False,'�������� ����� ������������','��� "������ ������"','1831161457','1131831003897','79658472607', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'active.m@bk.ru', '5D5BC0BBA68728BC928CA9A0D9B4D680221F3BAB', 
 '2013-09-17 07:12:12.000',False,False,'��������� ���� ������������','��� "����� ����"','2466264958','1132468043432','89232700993', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@gou-uic.ru', '7CD61F5A5104467FC05146596AEEBF802E0A49A8', 
 '2013-09-18 14:25:29.000',True,False,'�������� ��������� ����������','��� "������-�������������� ����� �"','6452937872','1086450008248','(8452)744-406', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'alliance-am@yandex.ru', '2B60696F0A648F7797C316AA58B93D6D56D83A0F', 
 '2013-09-24 18:10:06.000',True,False,'������� ������ ����������','��� "������ ����������"','7722793640','1127747205662','8-916-136-57-64', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'rustab@mail.ru', 'FB1BB3B094819330E4154BC2B083F4D05265BDAB', 
 '2013-09-25 12:44:01.000',False,False,'���������� ������ �����������','�������� � ������������ ���������������� "��������"','2014009046','1132031002421','8-928-087-28-92', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'stil-lider@yandex.ru', 'E2293EBFD6D96DB1E4D124D7BA378FD2B1D690B9', 
 '2013-09-27 11:50:01.000',False,False,'�������� �.�.','��� "����� � ��"','7438025410','1087438000760','73517504433', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'fpmpkurumkan@yandex.ru', '7D0E2B3CE94AFD97EBA92D9FA168297455963416', 
 '2013-09-30 10:36:58.000',False,False,'���������� ������� ���������','���� ��������� ������ ������������������� ������������� ������ ��','311005308','1090300000669','8(30149)41100', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Kharchenko@prisco.ru', 'AFEA95B76FA8FF925712D059AC380478CE8F9292', 
 '2013-10-03 09:29:56.000',False,False,'84236694668','��� "���������� ������� �����������"','2508018932','1022500695964','84236694505', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'serebryakova_e@kedrbank.com', '6C222272B208F947470A7AC1971870EF805EDF7E', 
 '2013-10-03 12:45:37.000',False,False,'����������� ��������� �������������','��� �� "����"','2451001025','1022400000655','(391)2743414', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vitalii_ufa@rambler.ru', 'B47A802D07553BEC6499F319AB8191354E45DA90', 
 '2013-10-08 07:22:52.000',False,False,'������� ������� �������������','��� "��� "�����"','273075830','1090280032292','(347)239-13-53', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'pana3009@yandex.ru', '17129ABFC375DB996DB29465BBDBF8AF011E95E9', 
 '2013-10-08 17:25:01.000',True,False,'������ ������� ������������','��� "������� ���"','7325096467','1107325003280','79876376714', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@nimbus-2013.ru', '04118473F4A6B8088DEB8C1266952B5621E5B2E3', 
 '2013-10-14 10:15:31.000',False,False,'�������� ����� ������������','��� "�� "������"','7725785729','1137746220039','(495) 231-46-68', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'marusyak@reserv-am.ru', 'AC83BEF1641EA6AAA45F462723F73B8D6129C7DF', 
 '2013-10-14 13:54:14.000',True,False,'������� ����� ����������','�������� � ������������ ���������������� ����������� �������� "������ ����� ����������"','7718784153','1097746714317','8-495-269-01-40', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'vcckirov@mail.ru', '4EAACA5A5B3C4820C8EB9AAF13757CB7179F2695', 
 '2013-10-15 11:30:33.000',False,False,'������� ��������� �������','�������� � ������������ ���������������� "������������-������"','4345288323','1104345018920','79127348539', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'zolotoy.pesok@bk.ru', 'F06427DD3E2B3D69D077DE6B5D9643354DBBAAD4', 
 '2013-10-20 03:15:02.000',False,False,'������� �.�.','��� ������� �����','2511084420','1132511003030','89147112161', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'Shilovpavel@hotmail.com', '63BD953816E396BE27620FB2663431EB8BF98042', 
 '2013-10-22 05:21:43.000',False,False,'����� ����� ��������','��� "����-����������� ���������"','6501017976','1026500523312','+7 (963) 289 25 27', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'gurovar@kbbmb.ru', 'AD3FDC69176FA0CFEEA1E4341AED6E317E9EFDE4', 
 '2013-10-22 12:23:10.000',True,False,'����� �.�.','�� ��� ���','5031032717','1025000006822','8-499-707-10-07', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'adamshirdi@mail.ru', '2693C8E41F8F4EF8F89E2E2A8E54147E49EB3B0C', 
 '2013-10-30 14:38:30.000',False,False,'���������� ���� �������','��� "������"','2013003838','1132031002058','89261112535', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ik_liberti@bk.ru', '600CC1E5BF43233D76CFB9CF799EDBC071085446', 
 '2013-10-30 14:43:05.000',True,False,'���������� ����� �������������','��� �� "�������������"','1656066046','1121690063538','8 (843) 2593455', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'ekalniczkaya@mail.ru', '2F219A1DC74F82A4019519051799B3462685B94F', 
 '2013-11-01 12:48:54.000',False,False,'���������� ����� �������������','��� "������ �� �������� ������"','7512005646','1137527000060','89144402876', False, NULL
);
insert into "UserAccount" 
(
 "Login", "Password", "CreateDate", "IsActive",
 "IsAdmin", "FIO", "Company", "INN", "OGRN", "Phone", 
 "EnableNotification", "CertificateID"
)
VALUES
(
 'info@volgainvest.ru', '91FFFC8069844C465A8CB7D0A5E4482CEE4D64E6', 
 '2013-11-07 16:36:09.000',False,False,'��������� ����� ����������','�����������','2130126745','1132130013531','78352562601', False, NULL
);


