ALTER TABLE "SED_ModelInfo" ADD COLUMN "ModelVersion" character varying(50);

update "SED_ModelInfo"
set
	"ModelVersion" = '2.16.2';