ALTER TABLE "Package" ADD COLUMN "IsImported" boolean;

DROP VIEW "PackageFullInfo";
CREATE OR REPLACE VIEW "PackageFullInfo" AS 
 SELECT "Package"."Name", "Package"."OwnerID", u."Login" AS "Owner", 
    "Package"."UploadTime", "Package"."InitialStatusID", 
    "Package"."CurrentStatusID", "ReportInfo"."IncomingNumber", 
    "ReportInfo"."IncomingDate", "ReportInfo"."SedNumber", 
    "ReportInfo"."SedDate", "Package"."BlobID", "Package"."ID", 
    s1."PrimaryCode" AS "CurrentStatusCode", 
    s2."PrimaryCode" AS "InitialStatusCode", "Package"."ReportID", 
    "Package"."SignatureType", "ReportInfo"."INN", "ReportInfo"."OGRN", 
    "ReportInfo"."Model", "ReportInfo"."Type", "ReportInfo"."DocTypeName", 
    "ReportInfo"."OrgTypeName", "ReportInfo"."ExternalID", 
    "ReportInfo"."Version", "ReportInfo"."OrgFullName", 
    "ReportInfo"."OrgShortName", "ReportInfo"."Address", "ReportInfo"."Subject", 
    "ReportInfo"."FirstName", "ReportInfo"."LastName", 
    "ReportInfo"."MiddleName", "ReportInfo"."Year", "ReportInfo"."Quartal", 
    "ReportInfo"."Month", "ReportInfo"."Date", "ReportInfo"."OrgTypeCode", 
    "ReportInfo"."DocTypeCode", "ReportInfo"."RoName", 
    "ReportInfo"."IsRoReport", s1."Info" AS "CurrentStatusInfo", 
    s2."Info" AS "InitialStatusInfo", 
    s1."Exception" AS "CurrentStatusException", "ReportInfo"."RoType",
    "Package"."IsImported"
   FROM "Package"
   JOIN "ReportInfo" ON "Package"."ReportID" = "ReportInfo"."ID"
   JOIN "Status" s1 ON "Package"."CurrentStatusID" = s1."ID"
   JOIN "Status" s2 ON "Package"."InitialStatusID" = s2."ID"
   JOIN "UserAccount" u ON "Package"."OwnerID" = u."ID";


ALTER TABLE "ReportInfo" DROP COLUMN "SedRequestEmlID";
ALTER TABLE "ReportInfo" DROP COLUMN "SedResponseEmlID";
ALTER TABLE "ReportInfo" DROP COLUMN "RoRequestEmlID";
ALTER TABLE "ReportInfo" DROP COLUMN "RoResponseEmlID";

ALTER TABLE "ReportInfo" ADD COLUMN "DocumentUrl" character varying(512);
ALTER TABLE "ReportInfo" ADD COLUMN "DocumentSignUrl" character varying(512);
ALTER TABLE "ReportInfo" ADD COLUMN "NotificationUrl" character varying(512);
ALTER TABLE "ReportInfo" ADD COLUMN "NotificationSignUrl" character varying(512);
ALTER TABLE "ReportInfo" ADD COLUMN "ProcessedDocumentUrl" character varying(512);


ALTER TABLE "Package" DROP CONSTRAINT "FK_Package_Blob_BlobID";
ALTER TABLE "Package" DROP CONSTRAINT "FK_Package_UserAccount_OwnerID";

ALTER TABLE "MailMessage" ADD COLUMN "CreateAt" timestamp without time zone;
ALTER TABLE "MailMessage" ALTER COLUMN "CreateAt" SET DEFAULT now();