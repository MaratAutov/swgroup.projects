CREATE TABLE "SED_DocMapSignature"
(
  "ID" serial NOT NULL,
  "Attr" character varying(255),
  "ModelName" character varying(255),
  CONSTRAINT "SED_DocMapSignature_pkey" PRIMARY KEY ("ID")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "SED_DocMapSignature"
  OWNER TO postgres;


INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������001','/������������001/������������������������/���');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������001','/������������001/����������������������������/���1');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������001','/������������001/��������������������������/���2');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������001','/������������001/���������������������/���3');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������001','/������������001/���������������������������/���4');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������002','/������������002/������������������������/���');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������002','/������������002/������������������������������/���1');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������002','/������������002/��������������������������/���2');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������002','/������������002/���������������������/���3');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������002','/������������002/���������������������������/���4');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������003','/������������003/������������������������/���');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������003','/������������003/��������������������������/���2');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������003','/������������003/�������������������������/���4');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������003','/������������003/����������������������������/���5');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������004','/������������004/��������������������������/���2');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������004','/������������004/���������������������/���3');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������004','/������������004/���������������������������/���4');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������005','/������������005/��������������������������/���2');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������005','/������������005/���������������������/���3');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������005','/������������005/���������������������������/���4');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������006','/������������006/���������������������/���');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������006','/������������006/��������������������������/���2');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������006','/������������006/�������������������������/���4');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������006','/������������006/����������������������������/���5');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������049','/������������049/����������������������������/���');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������059','/������������059/������������������091/��������������������������/���');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������060','/������������060/������������������092/��������������������������/���');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������061','/������������061/������������������093/��������������������������/���');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������007','/������������007/���������������������/���33');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������007','/������������007/��������������������������/���34');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������007','/������������007/���������������������/���35');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������007','/������������007/����������������������������/���36');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������������','/������������������/�����03/���������������������');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('����������','/����������/�����03/���������������������');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������������������������������������������','/������������������������������������������������/�����03/���������������������');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������������������������������������������','/������������������������������������������������/�����03/���������������������');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('��������������������������������������������������','/��������������������������������������������������/�����03/���������������������');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('�����������������������������������������������������������','/�����������������������������������������������������������/�����03/���������������������');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������������������������������������������������������������','/������������������������������������������������������������������/�����03/���������������������');
INSERT INTO "SED_DocMapSignature" ("ModelName", "Attr") VALUES ('������������','/������������/�����03/���������������������');




ALTER TABLE "SED_DocMap" ADD COLUMN SignMinValue integer NOT NULL default 1;
ALTER TABLE "SED_DocMap" ADD COLUMN signmaxvalue integer;





INSERT INTO "SED_DocMap"(
            "Type", "Code", "Name", signminvalue)
VALUES ('������������������', '301', '��������� � ������ ������ �������� ���������� �������������� ���� ������� ��������������� �����', 1);

INSERT INTO "SED_DocMap"(
            "Type", "Code", "Name", signminvalue)
VALUES ('����������', '302', '�������� ������ �������� ���������� �������������� ���� ������� ��������������� �����', 1);

INSERT INTO "SED_DocMap"(
            "Type", "Code", "Name", signminvalue)
VALUES ('������������������������������������������������', '303', '����������� ������������ ����������� �������� � �������������� ���������� ���������� ���������������� ���������� ��������� � ����������� �������� ����� ����������� �������� �� ����� ������ �����', 1);


INSERT INTO "SED_DocMap"(
            "Type", "Code", "Name", signminvalue)
VALUES ('������������������������������������������������', '304', '����������� ������������ ����������� �������� � ��������� ��������������� ������ ����������� ���������', 1);



INSERT INTO "SED_DocMap"(
            "Type", "Code", "Name", signminvalue)
VALUES ('��������������������������������������������������', '305', '����������� ������������ ����������� �������� � ������������� ������������� ��������� ����������', 1);

INSERT INTO "SED_DocMap"(
            "Type", "Code", "Name", signminvalue)
VALUES ('�����������������������������������������������������������', '306', '����������� ������������ ����������� �������� � ��������� ����������� ��������� ���������� ���������������� ���������� ���������, ����������� �������� ����� �����������
     �������� �� ����� ������ �����, ���� ����������� �������� ����� ���������� ���������', 1);

INSERT INTO "SED_DocMap"(
            "Type", "Code", "Name", signminvalue)
VALUES ('������������������������������������������������������������������', '307', '����������� ������������ ����������� �������� � ������������ ����������� ��������� ������������� ��������� �������� ���������� � �����������', 1);

INSERT INTO "SED_DocMap"(
            "Type", "Code", "Name", signminvalue)
VALUES ('������������', '308', '����� ����������� �������� �� ������� ��������������� �����', 1);