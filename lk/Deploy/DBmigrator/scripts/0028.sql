﻿DROP VIEW "PackageFullInfo";

ALTER TABLE "ReportInfo"
   ALTER COLUMN "Quartal" TYPE character varying(255);

CREATE OR REPLACE VIEW "PackageFullInfo" AS 
 SELECT "Package"."Name", "Package"."OwnerID", u."Login" AS "Owner", 
    "Package"."UploadTime", "Package"."InitialStatusID", 
    "Package"."CurrentStatusID", "ReportInfo"."IncomingNumber", 
    "ReportInfo"."IncomingDate", "ReportInfo"."SedNumber", 
    "ReportInfo"."SedDate", "Package"."BlobID", "Package"."ID", 
    s1."PrimaryCode" AS "CurrentStatusCode", 
    s2."PrimaryCode" AS "InitialStatusCode", "Package"."ReportID", 
    "Package"."SignatureType", "ReportInfo"."INN", "ReportInfo"."OGRN", 
    "ReportInfo"."Model", "ReportInfo"."Type", "ReportInfo"."DocTypeName", 
    "ReportInfo"."OrgTypeName", "ReportInfo"."ExternalID", 
    "ReportInfo"."Version", "ReportInfo"."OrgFullName", 
    "ReportInfo"."OrgShortName", "ReportInfo"."Address", "ReportInfo"."Subject", 
    "ReportInfo"."FirstName", "ReportInfo"."LastName", 
    "ReportInfo"."MiddleName", "ReportInfo"."Year", "ReportInfo"."Quartal", 
    "ReportInfo"."Month", "ReportInfo"."Date", "ReportInfo"."OrgTypeCode", 
    "ReportInfo"."DocTypeCode", "ReportInfo"."RoName", 
    "ReportInfo"."IsRoReport", s1."Info" AS "CurrentStatusInfo", 
    s2."Info" AS "InitialStatusInfo", 
    s1."Exception" AS "CurrentStatusException", "ReportInfo"."RoType", 
    "Package"."IsImported", 
    s1."AssignmentTime" AS "CurrentStatusAssignmentTime", 
    s2."AssignmentTime" AS "InitialStatusAssignmentTime"
   FROM "Package"
   JOIN "ReportInfo" ON "Package"."ReportID" = "ReportInfo"."ID"
   JOIN "Status" s1 ON "Package"."CurrentStatusID" = s1."ID"
   JOIN "Status" s2 ON "Package"."InitialStatusID" = s2."ID"
   LEFT JOIN "UserAccount" u ON "Package"."OwnerID" = u."ID";

