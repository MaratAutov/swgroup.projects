﻿CREATE OR REPLACE FUNCTION "IssetPackageHistory"("packageId" integer)
  RETURNS character varying[] AS
$BODY$
DECLARE 
	_year character varying(4);
	_month character varying(4);
	_quartal character varying(4);
	_date timestamp without time zone;
	_type character varying(255);
	_count integer;
	_inn character varying(12);
	report_type character varying(10);
BEGIN
	select ri."Year", ri."Quartal", ri."Month", ri."Date", ri."Type", ri."INN"
		into _year, _quartal, _month, _date, _type, _inn
	from "Package" p 
		inner join "ReportInfo" ri on p."ReportID" = ri."ID"
	where p."ID" = "packageId";
	RAISE NOTICE 'year: (%); month: (%); quartal: (%)', _year, _month, _quartal;

/* определение типа отчета */
	if (	_year is not null and _year <> '' and
		(_quartal is null or _quartal = '') and
                (_month is null or _month = '' )	) then
                RAISE NOTICE 'year';
                report_type := 'year';
                select count(*) into _count from "Package" p 
			inner join "ReportInfo" ri on p."ReportID" = ri."ID"
		where
			ri."Type" = _type and ri."Year" = _year and ri."INN" = _inn;
        elsif ( _year is not null and _year <> '' and
                _quartal is not null and _quartal <> '' and
                (_month is null or _month = '')) then
                RAISE NOTICE 'quartal';
                report_type := 'quartal';
                select count(*) into _count from "Package" p 
			inner join "ReportInfo" ri on p."ReportID" = ri."ID"
		where
			ri."Type" = _type and ri."Year" = _year and ri."Quartal" = _quartal and ri."INN" = _inn;
        elsif ( _year is not null and _year <> '' and
                (_quartal is null or _quartal = '') and
                _month is not null and _month <> '') then
                RAISE NOTICE 'month';
                report_type := 'month';
                select count(*) into _count from "Package" p 
			inner join "ReportInfo" ri on p."ReportID" = ri."ID"
		where
			ri."Type" = _type and ri."Year" = _year and ri."Month" = _month and ri."INN" = _inn;
        elsif ( (_year is null or _year = '') and
                (_quartal is null or _quartal = '') and
                (_month is null or _month = '') and
                _date is not null) then
                RAISE NOTICE 'date';
                report_type := 'date';
                select count(*) into _count from "Package" p 
			inner join "ReportInfo" ri on p."ReportID" = ri."ID"
		where
			ri."Type" = _type and ri."INN" = _inn and ri."Date" = _date;
        end if;
	
	return '{'||_count||', '||report_type||'}';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;