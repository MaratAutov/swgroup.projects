CREATE TABLE "MailMessage"
(
  "ID" serial NOT NULL,
  "Url" character varying(1024),
  "Subject" character varying(1024),
  "MailFrom" character varying(255),
  "MailTo" character varying(255),
  "PackageID" integer,
  "Info" text,
  CONSTRAINT "pkey_ID" PRIMARY KEY ("ID"),
  CONSTRAINT "fkey_PackageID" FOREIGN KEY ("PackageID")
      REFERENCES "Package" ("ID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Index: "fki_fkey_PackageID"

-- DROP INDEX "fki_fkey_PackageID";

CREATE INDEX "fki_fkey_PackageID"
  ON "MailMessage"
  USING btree
  ("PackageID");