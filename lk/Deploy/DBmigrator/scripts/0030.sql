﻿CREATE OR REPLACE FUNCTION "reimport"() RETURNS void
  AS
$BODY$
DECLARE 
	report_cursor CURSOR FOR select * from dblink('dbname=AccountImport port=5432 
            user=postgres password=123456','select 
	inn, ogrn, adress, appversion, lastname, fullname, 
	CASE WHEN incomingdate~E''^\\d{4}\\-\\d{1,2}\\-\\d{1,2}.*$'' THEN to_timestamp(incomingdate, ''YYYY-MM-DD'') ELSE to_timestamp(incomingdate, ''YYYY-MM-DD'') END as incomingdate,
	incomingnumber ,
	modelname, orgtypecode::integer, orgtypename, quarter, CASE WHEN doctypecode~E''^\\d+$'' THEN doctypecode::integer ELSE null END as doctypecode,
	CASE WHEN reportday~E''^\\d{4}\\-\\d{1,2}\\-\\d{1,2}.*$'' THEN to_timestamp(reportday, ''YYYY-MM-DD'') ELSE null END as reportday,
	reportmonth,
	doctypename, reportyear, rootname, shortname, info."входящийНомерПрисвоенныйПакету", info."датаПрисвоенияВходящегоНомера",
	firstname, middlename, replace(replace(documenturi, ''/'', ''\''), ''fs:'', ''fs:\old'') as documenturi, 
	replace(replace(documentsignuri, ''/'', ''\''), ''fs:'', ''fs:\old'') as documentsignuri, 
	replace(replace(notificationuri, ''/'', ''\''), ''fs:'', ''fs:\old'') as notificationuri, 
	replace(replace(notificationsignuri, ''/'', ''\''), ''fs:'', ''fs:\old'') as notificationsignuri,
	replace(replace(urlprocesseddocument, ''/'', ''\''), ''fs:'', ''fs:\old'') as urlprocesseddocument, 
	"краткоеНаименованиеРО", case when report.frommail is null or report.frommail = '''' then false else true end as isro,
	report.region as regioncode, p.status as nstatus, "report"."id" as reportid,
	"актуальныйСтатус", exceptionstring, creationdate, report.signtype, portalid
	from "reportpackage" report 
	                left join "поручениеобработкиотчетности" p on report.missionid = p.id
	                left join "информацияопакете" info on p.packageinfoid = info.ID
                    left join "уведомление" sed on sed.id = p.notifysedid')
    AS import(inn character varying(255), 
    ogrn character varying(255), 
    adress character varying(255),
    appversion character varying(255),
    lastname character varying(255),
    fullname text, 
    incomingdate timestamp,
    incomingnumber character varying(255),
    modelname character varying(255), 
    orgtypecode integer, 
    orgtypename character varying(255), 
    quarter character varying(255), 
    doctypecode integer,
    reportday timestamp,
    reportmonth character varying(255),
    doctypename character varying(255),
    reportyear character varying(255), 
    rootname character varying(255), 
    shortname character varying(255), 
    "входящийНомерПрисвоенныйПакету" character varying(255), 
    "датаПрисвоенияВходящегоНомера" timestamp,
    firstname character varying(255), 
    middlename character varying(255), 
    documenturi character varying(255), 
	documentsignuri character varying(255), 
	notificationuri character varying(255), 
	notificationsignuri character varying(255),
	urlprocesseddocument character varying(255), 
	"краткоеНаименованиеРО" character varying(255), 
	isro boolean,
	regioncode character varying(255), 
	nstatus character varying(255), 
	reportid character varying(100),
	"актуальныйСтатус" text, 
	exceptionstring text,
	creationdate timestamp,
	signtype character varying(255), 
	portalid character varying(255));
        
	curr_status_id bigint;
	first_status_id bigint;
	report_info_id bigint;
	package_id bigint;
BEGIN
-- clear
RAISE NOTICE 'clearing old...';
    delete from "Status" s where "ID" in (SELECT s1."ID" FROM "Package" p inner join "Status" s1 on (p."CurrentStatusID" = s1."ID" or p."InitialStatusID" = s1."ID") and p."IsImported" is true);
    delete from "ReportInfo" ri where "ID" in (select ri1."ID" FROM "Package" p inner join "ReportInfo" ri1 on p."ReportID" = ri1."ID" and p."IsImported" is true);
    delete from "MailMessage" mm where "ID" in (select mm1."ID" FROM "Package" p inner join "MailMessage" mm1 on mm1."PackageID" = p."ID" and p."IsImported" is true);
    delete from "Package" where "IsImported" is true;

RAISE NOTICE 'converting...';

FOR row1 IN report_cursor LOOP

-- statuses
    insert into "Status" ("PrimaryCode", "AssignmentTime", "PackageID", "SecondaryCode")
    values (case when row1."актуальныйСтатус" = 'PackageCorrupted' or row1."актуальныйСтатус" = 'SignatureError' THEN 2
    when row1."актуальныйСтатус" is null or row1."актуальныйСтатус" = '' then -1 else 5 end , 
    case when row1.creationdate is null then '0001-01-01' else row1.creationdate end, 0, -1);
    first_status_id := lastval();

    insert into "Status" ("PrimaryCode", "AssignmentTime", "Exception", "Info", "PackageID", "SecondaryCode")
    values (case when row1."актуальныйСтатус" = 'SignatureCorrect' THEN 5
    when row1."актуальныйСтатус" = 'Accepted' then 6 
    when row1."актуальныйСтатус" = 'IncomingNumberAssotiated' then 11
    when row1."актуальныйСтатус" = 'PackageNotRegistered' then 4
    when row1."актуальныйСтатус" = 'PackageCorrupted' then 1
    when row1."актуальныйСтатус" = 'SignatureError' then 2
    else -1 end, 
    case when row1.creationdate is null then '0001-01-01' else row1.creationdate end,
    row1.exceptionstring, row1.nstatus, 0, -1);
    curr_status_id := lastval();

    
-- reportinfo

    insert into "ReportInfo" ("INN", "OGRN", "Address", "Version", "MiddleName",
    "OrgFullName", "IncomingDate", "IncomingNumber", "Model", "OrgTypeCode", "OrgTypeName", "Quartal",
    "DocTypeCode", "Date", "Month", "DocTypeName", "Year", "Type", "OrgShortName", "SedNumber", "SedDate",
    "FirstName", "LastName", "DocumentUrl", "DocumentSignUrl", "NotificationUrl", "NotificationSignUrl",
    "ProcessedDocumentUrl", "RoName", "RoType", "IsRoReport") values 
    (row1.inn, row1.ogrn, row1.adress, row1.appversion, row1.lastname, row1.fullname, row1.incomingdate, row1.incomingnumber,
	row1.modelname, row1.orgtypecode, row1.orgtypename, row1.quarter, row1.doctypecode,row1.reportday,row1.reportmonth,
	row1.doctypename, row1.reportyear, row1.rootname, row1.shortname, row1."входящийНомерПрисвоенныйПакету", row1."датаПрисвоенияВходящегоНомера",
	row1.firstname, row1.middlename, row1.documenturi, row1.documentsignuri, row1.notificationuri, row1.notificationsignuri,
	row1.urlprocesseddocument, row1."краткоеНаименованиеРО",row1.regioncode, row1.isro);

    report_info_id := lastval();

--package

    insert into "Package" ("UploadTime", "SignatureType", "IsImported", "InitialStatusID", "CurrentStatusID", "ReportID", "BlobID", "OwnerID", "LastChangeTime", "PortalID", "KiasID")
    values (case when row1.creationdate is null then '0001-01-01' else row1.creationdate end,
	row1.signtype, true, first_status_id, curr_status_id, report_info_id, -1, 0, case when row1.creationdate is null then '0001-01-01' else row1.creationdate end,
	row1.portalid, row1.reportid
    );
    package_id := lastval();
-- mails
    
	insert into "MailMessage" ("Url", "Subject", "MailFrom", "MailTo", "PackageID", "Info", "CreateAt")
	select *
	 from dblink('dbname=AccountImport port=5432 
            user=postgres password=123456','select replace(replace(url, ''/'', ''\''), ''fs:'', ''fs:\old'') as url,
	subject, frommail, tomail, '||package_id||', 
	case when server = ''Report'' then ''From RO''
	when server = ''SED'' and (status = ''SED'' or status = ''Error'') then ''From SED''
	else ''To SED'' end as info,
	sentdate from
		imapmessagecard m 
	                inner join missionwebdavt w on m.id = w.mesid
	                inner join mailattach a on w.attachid = a.id
                    where reportpackageid ='''||row1.reportid||''' limit 10')
    AS import(
	url character varying(255),
	subject character varying(255),
	frommail character varying(255), 
	tomail character varying(255), 
	package_id integer,
	info character varying(255),
	sentdate date
    );

END LOOP;


END;
$BODY$
  LANGUAGE plpgsql VOLATILE

ALTER TABLE "Package" ADD COLUMN "notifytimeid" character varying(100);

  update "Package" p1
set notifytimeid= (select * from dblink('dbname=AccountImport port=5432 
            user=postgres password=123456','select n.notifytimeid from reportpackage p
inner join "поручениеобработкиотчетности" n on n.reportpackageid = p.id
where p.id='''||p1."KiasID"||'''') as import (notifytimeid character varying(100)))
where p1."IsImported" is true and p1."ID"
