CREATE TABLE "PackageDateLimit"
(
  "Year" integer,
  "Quarter1" date,
  "Quarter2" date,
  "Quarter3" date,
  "Quarter4" date,
  "YearLimit" date,
  "January" date,
  "February" date,
  "March" date,
  "April" date,
  "May" date,
  "June" date,
  "July" date,
  "August" date,
  "September" date,
  "October" date,
  "November" date,
  "December" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "PackageDateLimit"
  OWNER TO postgres;

-- Index: idx_year

-- DROP INDEX idx_year;

CREATE UNIQUE INDEX idx_year
  ON "PackageDateLimit"
  USING btree
  ("Year");