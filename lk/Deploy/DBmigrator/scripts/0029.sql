﻿INSERT INTO "SED_DocMap"("Type", "Code", "Name", signminvalue) VALUES ('ПакетУММ1', '09', 'пакет уведомления о маркет-мейкере;', 1);
INSERT INTO "SED_DocMap"("Type", "Code", "Name", signminvalue) VALUES ('ПакетУММ2', '09', 'пакет уведомления о допуске к участию в торгах;', 1);

ALTER TABLE "SED_DocMap" ADD COLUMN "Postfix" character varying(4);

UPDATE "SED_DocMap" SET "Postfix"='ук' WHERE "Type"='ПакетСООУРКИ001';
UPDATE "SED_DocMap" SET "Postfix"='ук' WHERE "Type"='ПакетСООУРКИ002';
UPDATE "SED_DocMap" SET "Postfix"='ук' WHERE "Type"='ПакетСООУРКИ003';
UPDATE "SED_DocMap" SET "Postfix"='ук' WHERE "Type"='ПакетСООУРКИ004';
UPDATE "SED_DocMap" SET "Postfix"='ук' WHERE "Type"='ПакетСООУРКИ005';
UPDATE "SED_DocMap" SET "Postfix"='ук' WHERE "Type"='ПакетСООУРКИ006';
UPDATE "SED_DocMap" SET "Postfix"='пиф' WHERE "Type"='ПакетСООУРКИ007';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ008';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ009';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ010';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ011';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ012';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ013';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ014';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ015';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ016';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ017';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ018';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ019';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ020';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ021';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ022';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ023';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ024';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ025';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ026';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ027';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ028';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ029';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ030';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ031';
UPDATE "SED_DocMap" SET "Postfix"='ук' WHERE "Type"='ПакетСООУРКИ032';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ033';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ034';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ035';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ036';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ037';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ038';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ039';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ040';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ041';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ042';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ043';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ044';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ045';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ046';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ047';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ048';
UPDATE "SED_DocMap" SET "Postfix"='пиф' WHERE "Type"='ПакетСООУРКИ049';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ050';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ051';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ052';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ053';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ054';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ055';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ056';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ057';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ058';
UPDATE "SED_DocMap" SET "Postfix"='пиф' WHERE "Type"='ПакетСООУРКИ059';
UPDATE "SED_DocMap" SET "Postfix"='пиф' WHERE "Type"='ПакетСООУРКИ060';
UPDATE "SED_DocMap" SET "Postfix"='пиф' WHERE "Type"='ПакетСООУРКИ061';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ062';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ063';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ064';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ065';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ066';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ067';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ068';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ069';
UPDATE "SED_DocMap" SET "Postfix"='нпф' WHERE "Type"='ПакетСООУРКИ070';
UPDATE "SED_DocMap" SET "Postfix"='пу' WHERE "Type"='ПакетСООУРЦБ1401';
UPDATE "SED_DocMap" SET "Postfix"='пу' WHERE "Type"='ПакетСООУРЦБ1402';
UPDATE "SED_DocMap" SET "Postfix"='пу' WHERE "Type"='ПакетСООУРЦБ1403';
UPDATE "SED_DocMap" SET "Postfix"='пу' WHERE "Type"='ПакетСООУРЦБ1404';
UPDATE "SED_DocMap" SET "Postfix"='пу' WHERE "Type"='ПакетСООУРЦБ1405';
UPDATE "SED_DocMap" SET "Postfix"='пу' WHERE "Type"='ПакетСООУРЦБ1406';
UPDATE "SED_DocMap" SET "Postfix"='пу' WHERE "Type"='ПакетСООУРЦБ1407';
UPDATE "SED_DocMap" SET "Postfix"='инс' WHERE "Type"='ПакетУСИО1';
UPDATE "SED_DocMap" SET "Postfix"='квин' WHERE "Type"='ПакетЛКИ1';
UPDATE "SED_DocMap" SET "Postfix"='увр' WHERE "Type"='ПакетРВЦД1';
UPDATE "SED_DocMap" SET "Postfix"='ап' WHERE "Type"='ПакетВРАЛ810';
UPDATE "SED_DocMap" SET "Postfix"='ап' WHERE "Type"='ПакетВРАЛ808';
UPDATE "SED_DocMap" SET "Postfix"='ап' WHERE "Type"='ПакетВРАЛ809';
UPDATE "SED_DocMap" SET "Postfix"='ап' WHERE "Type"='ПакетВРАЛ814';
UPDATE "SED_DocMap" SET "Postfix"='ап' WHERE "Type"='ПакетВРАЛ813';
UPDATE "SED_DocMap" SET "Postfix"='пу' WHERE "Type"='ПакетСООУРЦБ1408';
UPDATE "SED_DocMap" SET "Postfix"='пу' WHERE "Type"='ПакетСООУРЦБ1409';
UPDATE "SED_DocMap" SET "Postfix"='стс' WHERE "Type"='ПакетСООУРЦБ1418';
UPDATE "SED_DocMap" SET "Postfix"='рсс' WHERE "Type"='ПакетСООУРЦБ1419';
UPDATE "SED_DocMap" SET "Postfix"='м-м' WHERE "Type"='ПакетУММ1';
UPDATE "SED_DocMap" SET "Postfix"='м-м' WHERE "Type"='ПакетУММ2';

CREATE TABLE "IncomingNumber"
(
  "ID" serial NOT NULL,
  "Year" integer,
  "Number" integer,
  "Postfix" character varying(4),
  "GenerateTime" timestamp with time zone DEFAULT now(),
  "IncomingNumber" character varying,
  "PackageID" integer
)
WITH (
  OIDS=FALSE
);

CREATE OR REPLACE FUNCTION "GetIncomingNumber"("packageId" integer) RETURNS character varying AS
$BODY$
DECLARE 
	postfix character varying(4);
	incoming_number integer;
	prefix_year integer;
	total_number character varying(255);
	old_number character varying(255);
BEGIN

select "IncomingNumber" into old_number from "IncomingNumber" where "PackageID" = $1;
if old_number is not null then
	return old_number;
end if;

select sdm."Postfix" into postfix from "Package" p
	inner join "ReportInfo" r on p."ReportID"=r."ID"
	inner join "SED_DocMap" sdm on r."Type"=sdm."Type"
WHERE p."ID" = $1;
if postfix is null then
	postfix := 'пу';
end if;

prefix_year := substring(extract(year from now())::text from 3 for 2)::integer;
select max("Number") + 1 into incoming_number from "IncomingNumber" where "Postfix"=postfix and "Year"=prefix_year;

if (incoming_number is null) then
	incoming_number := 1;
end if;

total_number := concat(prefix_year,'-',incoming_number,'/',postfix);

insert into "IncomingNumber" ("Year", "Number", "Postfix", "IncomingNumber", "PackageID") 
values (prefix_year, incoming_number, postfix, total_number, $1);

return total_number;

END;
$BODY$

LANGUAGE plpgsql;