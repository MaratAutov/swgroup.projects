CREATE TABLE "Region"
(
  "ID" serial NOT NULL,
  "Name" character varying(255),
  CONSTRAINT "Region_pkey" PRIMARY KEY ("ID")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Region"
  OWNER TO postgres;