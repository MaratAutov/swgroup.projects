CREATE TABLE "UserMonitoring"
(
  "ID" serial NOT NULL,
  "Login" character varying(32),
  "Password" character varying(32),
  "CreateDate" timestamp without time zone NOT NULL DEFAULT now(),
  "IsActive" boolean NOT NULL,
  "IsAdmin" boolean NOT NULL,
  "FIO" character varying(255),
  "RegionUser" character varying(255),
  CONSTRAINT "UserMonitoring_pkey" PRIMARY KEY ("ID")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "UserMonitoring"
  OWNER TO postgres;