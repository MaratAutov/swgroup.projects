﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using Quartz.Impl;
using ProcessService.Jobs;

namespace ProcessService
{
    public static class Initialization
    {
        public static void InitShedulers()
        {
            // construct a scheduler factory
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // get a scheduler
            IScheduler sched = schedFact.GetScheduler();
            sched.Start();

            // define the job and tie it to our HelloJob class
            IJobDetail createTaskJob = JobBuilder.Create<CreateTaskJob>()
                .WithIdentity("createTaskJob", "processingPacket")
                .Build();

            IJobDetail processTaskJob = JobBuilder.Create<ProcessTaskJob>()
                .WithIdentity("processTaskJob", "processingPacket")
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger triggerCreate = TriggerBuilder.Create()
              .WithIdentity("triggerCreate", "processingPacket")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInSeconds(3)
                  .RepeatForever())
              .Build();
            
            ITrigger triggerProcess = TriggerBuilder.Create()
              .WithIdentity("triggerProcess", "processingPacket")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInSeconds(3)
                  .RepeatForever())
              .Build();

            sched.ScheduleJob(processTaskJob, triggerProcess);
            Console.ReadKey();
            sched.Shutdown();
            Console.ReadKey();
        }
    }
}
