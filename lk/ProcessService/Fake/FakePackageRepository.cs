﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Account.DAO;

namespace ProcessService.Fake
{

    public class FakePackageRepository : IProcessPackageRepository
    {
        static int statusMax = Enum.GetValues(typeof(PrimaryStatus)).Length - 1;
        public List<PackageFullInfo> GetTasks()
        {

            var rnd = new Random();

            List<PackageFullInfo> packages = new List<PackageFullInfo>();
            for (int i = 0; i < 100; i++)
            {
                PackageFullInfo p = new PackageFullInfo();
                p.ID = rnd.Next(100, 1000000);
                p.CurrentStatusCode = rnd.Next(0, statusMax);

            }
            return packages;
        }
    }
}

public interface IProcessPackageRepository
{
    List<PackageFullInfo> GetTasks();
}
