﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Web.Script.Serialization;
using System.Configuration;
using Account.Helpers;
using Account.Models;
using Account.DAO;
using Account.Tools;
using NLog;
using Account.DAO.SED;
using ServiceCore;

namespace Account.ReportService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://localhost:27278/ReportService.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ReportService : System.Web.Services.WebService
    {
        private static string ServiceName = "Report Service";
        public ReportService()
        {           

            if (log == null)
            {
                var logFile = ConfigurationManager.AppSettings["LogFile"];
                log = NLogger.GetLog(ServiceName, logFile);
                log.Debug("SED Core instance initialization");
            }

            log.Debug("SPOService initialized");
        }

        private int GetUserID(HttpRequest Request)
        {
            UserAccount acc;

            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                acc = serializer.Deserialize<UserAccount>(authTicket.UserData);
                if (acc != null)
                    return acc.ID;
            }
            return 0;
        }

        public static Logger log;

        static ServiceCoreInfo info =  new ServiceCoreInfo(new System.Collections.Queue()) { Name = "ReportService", IsActive = true };

        [WebMethod]
        public ServiceCoreInfo Status()
        {
            log.Debug("Started [Status]");
            return info;
        }

        /// <summary>
	    /// Передать отчётность в систему. Внимание: при повторных вызовах с теми же параметрами
	    /// вернёт последнее уведомление по ранее переданной отчётности.
	    /// </summary>
	    /// <param name="name">Имя пакета отчётности</param>
	    /// <param name="body">Тело пакета отчётности</param>
	    /// <returns>Секретный идентификатор для дальнейшего получения данных по пакету</returns>
	    [WebMethod]
        public string Submit(string name, byte[] body)
	    {
            if (body == null)
                throw new ArgumentNullException("Пакет не может быть Null");
            using (var cl = new SPOService.SPOServiceSoapClient())
            {
                string usrIP = HttpContext.Current.Request.UserHostAddress;
                string usrHost = HttpContext.Current.Request.UserHostName;
                string usr = String.Format("[{0}] {1}", usrIP, usrHost);
                info.Processed++;
                string id = null;
                try
                {

                    id = cl.UploadPackage(
                        name,
                        body,
                        GetUserID(Context.Request),
                        DateTime.Now,
                        false,
                        null
                    );
                    info.Succeeded++;
                    log.Debug("{0} Successfuly submit package {1}", usr, name);
                    return id;
                }
                catch (Exception ex)
                {
                    log.Error(String.Format("Error Submit package from {0}; name: {2}; body.length {3}; ex: {1}", usr, ex, name, (body ?? new byte[0]).Length ));
                    info.Failed++;
                    throw;
                }


            }
	    }

        /// <summary>
        /// Передать отчётность в систему с учетом предварительной авторизации методом LogIn. Внимание: при повторных вызовах с теми же параметрами
        /// вернёт последнее уведомление по ранее переданной отчётности.
        /// </summary>
        /// <param name="name">Имя пакета отчётности</param>
        /// <param name="body">Тело пакета отчётности</param>
        /// <returns>Секретный идентификатор для дальнейшего получения данных по пакету</returns>
        [WebMethod]
        public string SubmitAuth(string name, byte[] body)
        {
            if (GetUserID(Context.Request) == 0)
                return "Неавторизованный доступ";

            return Submit(name, body);
        }
    
        [WebMethod]
        public PackageList GetOutgoing(int? year, int? pageNumber, int? pageSize,
            string date, string fileName, string userNumber)
        {
            var table = new FilterTableModel<PackageModel, PackageFullInfo>()
            {
                DataModel = new PackageModel()
                {
                    Name = fileName,
                    IncomingDate = Convert.ToDateTime(date)
                },
                SortingField = "UploadTime",
                SortingType = "desc"
            };
            
            FilterTableHelper.InitOutgoingTable(
                GetUserID(Context.Request),
                year.HasValue ? year.Value.ToString() : null,
                table
            );

            if (pageSize.HasValue)
                table.DataList.PageSize = pageSize.Value;
            if (pageNumber.HasValue)
                table.DataList.Page = pageNumber.Value - 1;
            ;
            return new PackageList(table.DataList.RecordCount, table.DataList.GetCurrentPageList());
        }

        public class PackageList
        {
            public int TotalCount { get; set; }
            public List<PackageFullInfo> Packages { get; set; }

            public PackageList()
            {
            }

            public PackageList(int totalCount, List<PackageFullInfo> packages)
            {
                this.TotalCount = totalCount;
                this.Packages = packages;
            }
        }

        [WebMethod(EnableSession = true)]
        public string LogIn(string login, string password)
        {
            UserAccount user = UserService.Get(
                login, password
            );
            if (user != null)
            {
                JavaScriptSerializer serializer =
                    new JavaScriptSerializer();
                string userData = serializer.Serialize(user);

                FormsAuthenticationTicket authTicket =
                    new FormsAuthenticationTicket(
                            1,
                            login,
                            DateTime.Now,
                            DateTime.Now.AddMinutes(15),
                            false,
                            userData
                    );

                string encTicket = FormsAuthentication.Encrypt(
                    authTicket
                );
                HttpCookie faCookie = new HttpCookie(
                    FormsAuthentication.FormsCookieName, encTicket
                );
                Context.Response.Cookies.Add(faCookie);

                return null;
            }
            return "Неверное имя пользователя или пароль";
        }

	    /// <summary>
	    /// Опубликовать документ указанному пользователю. 
	    /// Внимание: при повторных вызовах с теми же параметрами
	    /// вернёт последнее уведомление по ранее переданной отчётности.
	    /// </summary>
	    /// <param name="displayName">Имя пакета отчётности для отображения в ЛК</param>
	    /// <param name="fileName">Имя файла с расширением</param>
	    /// <param name="body">Тело пакета отчётности</param>
	    /// <param name="destinationOgrn">Список идентификаторов </param>
	    /// <returns>Массив ОГРНов пользователей, которые получили документ. Отсутствие ОГРН означате,
	    /// что пользователь не найден, однако в случае регистрации он получит доступ к документу.</returns>
	    [WebMethod]
	    public string[] PublishDocument(string displayName, string fileName, byte[] body, string[] destinationOgrn)
	    {
		    //ReportsFacade facade = AppContext.Facades.New<ReportsFacade>();
		    //var docs = facade.PublishDocument(displayName, fileName, body, destinationOgrn);
            var docs = new string[0];
		    return docs;
	    }

        

        [WebMethod]
        public byte[] Download(int id)
        {
            if (GetUserID(Context.Request) == 0)
                return null;

            Package p = PackageService.GetPackage(id);
            Blob blob = PackageService.GetBlob(p.BlobID);
            return blob.Body;
        }

        [WebMethod]
        public Account.Tools.NotificationResult Notification(int id)
        {
            var packInfo = PackageService.GetFullInfo(id);
            Status status = StatusService.Get((int)packInfo.CurrentStatusID);
            if ((status.NotificationBlobID != null) && (status.NotificationBlobID != 0))
            {
                Blob b = BlobServise.Get((int)status.NotificationBlobID);
                return new Account.Tools.NotificationResult
                {
                    FileName = packInfo.Name,
                    Notification = b.Body,
                    Status = StatusService.GetSPOStatusString((int)packInfo.InitialStatusCode)
                };
            }
            else
            return NotificationHelper.Build(id);
        }
    }
}