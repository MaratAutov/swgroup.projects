﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.18052
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Account.ReportService.SPOService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://localhost:37446/SPOService.asmx", ConfigurationName="SPOService.SPOServiceSoap")]
    public interface SPOServiceSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://localhost:37446/SPOService.asmx/UpdateSEDMappings", ReplyAction="*")]
        void UpdateSEDMappings();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://localhost:37446/SPOService.asmx/Shutdown", ReplyAction="*")]
        void Shutdown();
        
        // CODEGEN: Контракт генерации сообщений с именем StatusResult из пространства имен http://localhost:37446/SPOService.asmx не отмечен как обнуляемый
        [System.ServiceModel.OperationContractAttribute(Action="http://localhost:37446/SPOService.asmx/Status", ReplyAction="*")]
        Account.ReportService.SPOService.StatusResponse Status(Account.ReportService.SPOService.StatusRequest request);
        
        // CODEGEN: Контракт генерации сообщений с именем Name из пространства имен http://localhost:37446/SPOService.asmx не отмечен как обнуляемый
        [System.ServiceModel.OperationContractAttribute(Action="http://localhost:37446/SPOService.asmx/UploadPackage", ReplyAction="*")]
        Account.ReportService.SPOService.UploadPackageResponse UploadPackage(Account.ReportService.SPOService.UploadPackageRequest request);
        
        // CODEGEN: Контракт генерации сообщений с именем Name из пространства имен http://localhost:37446/SPOService.asmx не отмечен как обнуляемый
        [System.ServiceModel.OperationContractAttribute(Action="http://localhost:37446/SPOService.asmx/UploadDocument", ReplyAction="*")]
        Account.ReportService.SPOService.UploadDocumentResponse UploadDocument(Account.ReportService.SPOService.UploadDocumentRequest request);
        
        // CODEGEN: Контракт генерации сообщений с именем package из пространства имен http://localhost:37446/SPOService.asmx не отмечен как обнуляемый
        [System.ServiceModel.OperationContractAttribute(Action="http://localhost:37446/SPOService.asmx/ValidatePackage", ReplyAction="*")]
        Account.ReportService.SPOService.ValidatePackageResponse ValidatePackage(Account.ReportService.SPOService.ValidatePackageRequest request);
        
        // CODEGEN: Контракт генерации сообщений с именем package из пространства имен http://localhost:37446/SPOService.asmx не отмечен как обнуляемый
        [System.ServiceModel.OperationContractAttribute(Action="http://localhost:37446/SPOService.asmx/ProcessPackage", ReplyAction="*")]
        Account.ReportService.SPOService.ProcessPackageResponse ProcessPackage(Account.ReportService.SPOService.ProcessPackageRequest request);
        
        // CODEGEN: Контракт генерации сообщений с именем response из пространства имен http://localhost:37446/SPOService.asmx не отмечен как обнуляемый
        [System.ServiceModel.OperationContractAttribute(Action="http://localhost:37446/SPOService.asmx/ProcessSedResponse", ReplyAction="*")]
        Account.ReportService.SPOService.ProcessSedResponseResponse ProcessSedResponse(Account.ReportService.SPOService.ProcessSedResponseRequest request);
        
        // CODEGEN: Контракт генерации сообщений с именем packInfo из пространства имен http://localhost:37446/SPOService.asmx не отмечен как обнуляемый
        [System.ServiceModel.OperationContractAttribute(Action="http://localhost:37446/SPOService.asmx/AcquireRoName", ReplyAction="*")]
        Account.ReportService.SPOService.AcquireRoNameResponse AcquireRoName(Account.ReportService.SPOService.AcquireRoNameRequest request);
        
        // CODEGEN: Контракт генерации сообщений с именем packInfo из пространства имен http://localhost:37446/SPOService.asmx не отмечен как обнуляемый
        [System.ServiceModel.OperationContractAttribute(Action="http://localhost:37446/SPOService.asmx/RegisterInReestr", ReplyAction="*")]
        Account.ReportService.SPOService.RegisterInReestrResponse RegisterInReestr(Account.ReportService.SPOService.RegisterInReestrRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class StatusRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Status", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.StatusRequestBody Body;
        
        public StatusRequest() {
        }
        
        public StatusRequest(Account.ReportService.SPOService.StatusRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class StatusRequestBody {
        
        public StatusRequestBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class StatusResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="StatusResponse", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.StatusResponseBody Body;
        
        public StatusResponse() {
        }
        
        public StatusResponse(Account.ReportService.SPOService.StatusResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://localhost:37446/SPOService.asmx")]
    public partial class StatusResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public Account.SPOService.ServiceCoreInfo StatusResult;
        
        public StatusResponseBody() {
        }
        
        public StatusResponseBody(Account.SPOService.ServiceCoreInfo StatusResult) {
            this.StatusResult = StatusResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class UploadPackageRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="UploadPackage", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.UploadPackageRequestBody Body;
        
        public UploadPackageRequest() {
        }
        
        public UploadPackageRequest(Account.ReportService.SPOService.UploadPackageRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://localhost:37446/SPOService.asmx")]
    public partial class UploadPackageRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string Name;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public byte[] Data;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int OwnerID;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public System.DateTime UploadTime;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public bool IsRO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string SubjectRO;
        
        public UploadPackageRequestBody() {
        }
        
        public UploadPackageRequestBody(string Name, byte[] Data, int OwnerID, System.DateTime UploadTime, bool IsRO, string SubjectRO) {
            this.Name = Name;
            this.Data = Data;
            this.OwnerID = OwnerID;
            this.UploadTime = UploadTime;
            this.IsRO = IsRO;
            this.SubjectRO = SubjectRO;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class UploadPackageResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="UploadPackageResponse", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.UploadPackageResponseBody Body;
        
        public UploadPackageResponse() {
        }
        
        public UploadPackageResponse(Account.ReportService.SPOService.UploadPackageResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://localhost:37446/SPOService.asmx")]
    public partial class UploadPackageResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string UploadPackageResult;
        
        public UploadPackageResponseBody() {
        }
        
        public UploadPackageResponseBody(string UploadPackageResult) {
            this.UploadPackageResult = UploadPackageResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class UploadDocumentRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="UploadDocument", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.UploadDocumentRequestBody Body;
        
        public UploadDocumentRequest() {
        }
        
        public UploadDocumentRequest(Account.ReportService.SPOService.UploadDocumentRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://localhost:37446/SPOService.asmx")]
    public partial class UploadDocumentRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string Name;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public byte[] Data;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int OwnerID;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public System.DateTime UploadTime;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int receiver;
        
        public UploadDocumentRequestBody() {
        }
        
        public UploadDocumentRequestBody(string Name, byte[] Data, int OwnerID, System.DateTime UploadTime, int receiver) {
            this.Name = Name;
            this.Data = Data;
            this.OwnerID = OwnerID;
            this.UploadTime = UploadTime;
            this.receiver = receiver;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class UploadDocumentResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="UploadDocumentResponse", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.UploadDocumentResponseBody Body;
        
        public UploadDocumentResponse() {
        }
        
        public UploadDocumentResponse(Account.ReportService.SPOService.UploadDocumentResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://localhost:37446/SPOService.asmx")]
    public partial class UploadDocumentResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string UploadDocumentResult;
        
        public UploadDocumentResponseBody() {
        }
        
        public UploadDocumentResponseBody(string UploadDocumentResult) {
            this.UploadDocumentResult = UploadDocumentResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ValidatePackageRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ValidatePackage", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.ValidatePackageRequestBody Body;
        
        public ValidatePackageRequest() {
        }
        
        public ValidatePackageRequest(Account.ReportService.SPOService.ValidatePackageRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://localhost:37446/SPOService.asmx")]
    public partial class ValidatePackageRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public Account.SPOService.UploadedPackage package;
        
        public ValidatePackageRequestBody() {
        }
        
        public ValidatePackageRequestBody(Account.SPOService.UploadedPackage package) {
            this.package = package;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ValidatePackageResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ValidatePackageResponse", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.ValidatePackageResponseBody Body;
        
        public ValidatePackageResponse() {
        }
        
        public ValidatePackageResponse(Account.ReportService.SPOService.ValidatePackageResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class ValidatePackageResponseBody {
        
        public ValidatePackageResponseBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ProcessPackageRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ProcessPackage", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.ProcessPackageRequestBody Body;
        
        public ProcessPackageRequest() {
        }
        
        public ProcessPackageRequest(Account.ReportService.SPOService.ProcessPackageRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://localhost:37446/SPOService.asmx")]
    public partial class ProcessPackageRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public Account.SPOService.InteropPackage package;
        
        public ProcessPackageRequestBody() {
        }
        
        public ProcessPackageRequestBody(Account.SPOService.InteropPackage package) {
            this.package = package;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ProcessPackageResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ProcessPackageResponse", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.ProcessPackageResponseBody Body;
        
        public ProcessPackageResponse() {
        }
        
        public ProcessPackageResponse(Account.ReportService.SPOService.ProcessPackageResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class ProcessPackageResponseBody {
        
        public ProcessPackageResponseBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ProcessSedResponseRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ProcessSedResponse", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.ProcessSedResponseRequestBody Body;
        
        public ProcessSedResponseRequest() {
        }
        
        public ProcessSedResponseRequest(Account.ReportService.SPOService.ProcessSedResponseRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://localhost:37446/SPOService.asmx")]
    public partial class ProcessSedResponseRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public Account.SPOService.SedResponse response;
        
        public ProcessSedResponseRequestBody() {
        }
        
        public ProcessSedResponseRequestBody(Account.SPOService.SedResponse response) {
            this.response = response;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ProcessSedResponseResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ProcessSedResponseResponse", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.ProcessSedResponseResponseBody Body;
        
        public ProcessSedResponseResponse() {
        }
        
        public ProcessSedResponseResponse(Account.ReportService.SPOService.ProcessSedResponseResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class ProcessSedResponseResponseBody {
        
        public ProcessSedResponseResponseBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class AcquireRoNameRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="AcquireRoName", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.AcquireRoNameRequestBody Body;
        
        public AcquireRoNameRequest() {
        }
        
        public AcquireRoNameRequest(Account.ReportService.SPOService.AcquireRoNameRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://localhost:37446/SPOService.asmx")]
    public partial class AcquireRoNameRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public Account.SPOService.PackageInfo packInfo;
        
        public AcquireRoNameRequestBody() {
        }
        
        public AcquireRoNameRequestBody(Account.SPOService.PackageInfo packInfo) {
            this.packInfo = packInfo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class AcquireRoNameResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="AcquireRoNameResponse", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.AcquireRoNameResponseBody Body;
        
        public AcquireRoNameResponse() {
        }
        
        public AcquireRoNameResponse(Account.ReportService.SPOService.AcquireRoNameResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class AcquireRoNameResponseBody {
        
        public AcquireRoNameResponseBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class RegisterInReestrRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="RegisterInReestr", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.RegisterInReestrRequestBody Body;
        
        public RegisterInReestrRequest() {
        }
        
        public RegisterInReestrRequest(Account.ReportService.SPOService.RegisterInReestrRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://localhost:37446/SPOService.asmx")]
    public partial class RegisterInReestrRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public Account.SPOService.PackageInfo packInfo;
        
        public RegisterInReestrRequestBody() {
        }
        
        public RegisterInReestrRequestBody(Account.SPOService.PackageInfo packInfo) {
            this.packInfo = packInfo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class RegisterInReestrResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="RegisterInReestrResponse", Namespace="http://localhost:37446/SPOService.asmx", Order=0)]
        public Account.ReportService.SPOService.RegisterInReestrResponseBody Body;
        
        public RegisterInReestrResponse() {
        }
        
        public RegisterInReestrResponse(Account.ReportService.SPOService.RegisterInReestrResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class RegisterInReestrResponseBody {
        
        public RegisterInReestrResponseBody() {
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface SPOServiceSoapChannel : Account.ReportService.SPOService.SPOServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SPOServiceSoapClient : System.ServiceModel.ClientBase<Account.ReportService.SPOService.SPOServiceSoap>, Account.ReportService.SPOService.SPOServiceSoap {
        
        public SPOServiceSoapClient() {
        }
        
        public SPOServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SPOServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SPOServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SPOServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void UpdateSEDMappings() {
            base.Channel.UpdateSEDMappings();
        }
        
        public void Shutdown() {
            base.Channel.Shutdown();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Account.ReportService.SPOService.StatusResponse Account.ReportService.SPOService.SPOServiceSoap.Status(Account.ReportService.SPOService.StatusRequest request) {
            return base.Channel.Status(request);
        }
        
        public Account.SPOService.ServiceCoreInfo Status() {
            Account.ReportService.SPOService.StatusRequest inValue = new Account.ReportService.SPOService.StatusRequest();
            inValue.Body = new Account.ReportService.SPOService.StatusRequestBody();
            Account.ReportService.SPOService.StatusResponse retVal = ((Account.ReportService.SPOService.SPOServiceSoap)(this)).Status(inValue);
            return retVal.Body.StatusResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Account.ReportService.SPOService.UploadPackageResponse Account.ReportService.SPOService.SPOServiceSoap.UploadPackage(Account.ReportService.SPOService.UploadPackageRequest request) {
            return base.Channel.UploadPackage(request);
        }
        
        public string UploadPackage(string Name, byte[] Data, int OwnerID, System.DateTime UploadTime, bool IsRO, string SubjectRO) {
            Account.ReportService.SPOService.UploadPackageRequest inValue = new Account.ReportService.SPOService.UploadPackageRequest();
            inValue.Body = new Account.ReportService.SPOService.UploadPackageRequestBody();
            inValue.Body.Name = Name;
            inValue.Body.Data = Data;
            inValue.Body.OwnerID = OwnerID;
            inValue.Body.UploadTime = UploadTime;
            inValue.Body.IsRO = IsRO;
            inValue.Body.SubjectRO = SubjectRO;
            Account.ReportService.SPOService.UploadPackageResponse retVal = ((Account.ReportService.SPOService.SPOServiceSoap)(this)).UploadPackage(inValue);
            return retVal.Body.UploadPackageResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Account.ReportService.SPOService.UploadDocumentResponse Account.ReportService.SPOService.SPOServiceSoap.UploadDocument(Account.ReportService.SPOService.UploadDocumentRequest request) {
            return base.Channel.UploadDocument(request);
        }
        
        public string UploadDocument(string Name, byte[] Data, int OwnerID, System.DateTime UploadTime, int receiver) {
            Account.ReportService.SPOService.UploadDocumentRequest inValue = new Account.ReportService.SPOService.UploadDocumentRequest();
            inValue.Body = new Account.ReportService.SPOService.UploadDocumentRequestBody();
            inValue.Body.Name = Name;
            inValue.Body.Data = Data;
            inValue.Body.OwnerID = OwnerID;
            inValue.Body.UploadTime = UploadTime;
            inValue.Body.receiver = receiver;
            Account.ReportService.SPOService.UploadDocumentResponse retVal = ((Account.ReportService.SPOService.SPOServiceSoap)(this)).UploadDocument(inValue);
            return retVal.Body.UploadDocumentResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Account.ReportService.SPOService.ValidatePackageResponse Account.ReportService.SPOService.SPOServiceSoap.ValidatePackage(Account.ReportService.SPOService.ValidatePackageRequest request) {
            return base.Channel.ValidatePackage(request);
        }
        
        public void ValidatePackage(Account.SPOService.UploadedPackage package) {
            Account.ReportService.SPOService.ValidatePackageRequest inValue = new Account.ReportService.SPOService.ValidatePackageRequest();
            inValue.Body = new Account.ReportService.SPOService.ValidatePackageRequestBody();
            inValue.Body.package = package;
            Account.ReportService.SPOService.ValidatePackageResponse retVal = ((Account.ReportService.SPOService.SPOServiceSoap)(this)).ValidatePackage(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Account.ReportService.SPOService.ProcessPackageResponse Account.ReportService.SPOService.SPOServiceSoap.ProcessPackage(Account.ReportService.SPOService.ProcessPackageRequest request) {
            return base.Channel.ProcessPackage(request);
        }
        
        public void ProcessPackage(Account.SPOService.InteropPackage package) {
            Account.ReportService.SPOService.ProcessPackageRequest inValue = new Account.ReportService.SPOService.ProcessPackageRequest();
            inValue.Body = new Account.ReportService.SPOService.ProcessPackageRequestBody();
            inValue.Body.package = package;
            Account.ReportService.SPOService.ProcessPackageResponse retVal = ((Account.ReportService.SPOService.SPOServiceSoap)(this)).ProcessPackage(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Account.ReportService.SPOService.ProcessSedResponseResponse Account.ReportService.SPOService.SPOServiceSoap.ProcessSedResponse(Account.ReportService.SPOService.ProcessSedResponseRequest request) {
            return base.Channel.ProcessSedResponse(request);
        }
        
        public void ProcessSedResponse(Account.SPOService.SedResponse response) {
            Account.ReportService.SPOService.ProcessSedResponseRequest inValue = new Account.ReportService.SPOService.ProcessSedResponseRequest();
            inValue.Body = new Account.ReportService.SPOService.ProcessSedResponseRequestBody();
            inValue.Body.response = response;
            Account.ReportService.SPOService.ProcessSedResponseResponse retVal = ((Account.ReportService.SPOService.SPOServiceSoap)(this)).ProcessSedResponse(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Account.ReportService.SPOService.AcquireRoNameResponse Account.ReportService.SPOService.SPOServiceSoap.AcquireRoName(Account.ReportService.SPOService.AcquireRoNameRequest request) {
            return base.Channel.AcquireRoName(request);
        }
        
        public void AcquireRoName(Account.SPOService.PackageInfo packInfo) {
            Account.ReportService.SPOService.AcquireRoNameRequest inValue = new Account.ReportService.SPOService.AcquireRoNameRequest();
            inValue.Body = new Account.ReportService.SPOService.AcquireRoNameRequestBody();
            inValue.Body.packInfo = packInfo;
            Account.ReportService.SPOService.AcquireRoNameResponse retVal = ((Account.ReportService.SPOService.SPOServiceSoap)(this)).AcquireRoName(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Account.ReportService.SPOService.RegisterInReestrResponse Account.ReportService.SPOService.SPOServiceSoap.RegisterInReestr(Account.ReportService.SPOService.RegisterInReestrRequest request) {
            return base.Channel.RegisterInReestr(request);
        }
        
        public void RegisterInReestr(Account.SPOService.PackageInfo packInfo) {
            Account.ReportService.SPOService.RegisterInReestrRequest inValue = new Account.ReportService.SPOService.RegisterInReestrRequest();
            inValue.Body = new Account.ReportService.SPOService.RegisterInReestrRequestBody();
            inValue.Body.packInfo = packInfo;
            Account.ReportService.SPOService.RegisterInReestrResponse retVal = ((Account.ReportService.SPOService.SPOServiceSoap)(this)).RegisterInReestr(inValue);
        }
    }
}
