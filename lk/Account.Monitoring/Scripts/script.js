﻿$(function () {
    if ($('.date').length) {
        $('.date').datepicker();
    }

    if ($('table').length) {
        $('table')
            .find('tbody tr:odd')
            .addClass('odd');
    }
});

function gather(url) {
    $('#stats').css('opacity', '0.3');
    $('.ui-dialog').remove();
    $.post(
        url,
        $('form').serialize(),
        function (data) {
            $('#stats').html(data);
            $('#stats').css('opacity', '1.0');            
            
            if (String(data).indexOf("Вход в систему мониторинга СПО - Федеральная Служба по Финансовым Рынкам") != -1)
            {
                location.href = url;
            }
        }
    );
    return false;
}

function gatherDateRange(url) {
    $('#stats').css('opacity', '0.3');
    q = $.post(
            url,
            { 'from': $('#from').val(), 'to': $('#to').val() },
            function (data) {

                if (String(data).indexOf("Вход в систему мониторинга СПО - Федеральная Служба по Финансовым Рынкам") != -1) {
                    location.href = url;
                }

                $('#stats').html(data);
                $('#stats').css('opacity', '1.0');
            }
        );
    q.fail(function (xhr, textStatus, error) {
        console.log(xhr.statusText);
        console.log(textStatus);
        console.log(error);
    })
    return false;
}

function setDates(from, to) {
    $('#from').val(
        $.datepicker.formatDate('dd.mm.yy', from)
    );
    $('#to').val(
        $.datepicker.formatDate('dd.mm.yy', to)
    );
}

function today() {
    var date = new Date();
    setDates(date, date);
}

function week() {
    var d = new Date();
    var to = new Date(d);
    var day = d.getDay();
    var diff = d.getDate() - day + (day == 0 ? -6 : 1);
    var from = new Date(d.setDate(diff));

    setDates(from, to);
}

function month() {
    var from = new Date();
    var to = new Date();
    from.setDate(1);

    setDates(from, to);
}

function infinite() {
    $('#from').val('');
    $('#to').val('');
}

function showReport(url, id, opts) {
    $.post(
        url,
        { 'id': id },
        function (data) {
            $('#report-' + id).html(data);
        }
    );

    $('#report-' + id)
        .attr('title', opts.title)
        .removeClass('report-info')
        .addClass('report-actions')
        .dialog({ width: opts.width, height: opts.height });
}

function resubmit(url, mode, id) {
    $.post(
        url,
        { 'mode': mode, 'id': id },
        function (data) {
            $('#report-' + id).dialog('close');
        }
    ).error(function (jqXHR, status, error) {
        console.log(jqXHR);
        $('#report-' + id + ' td.error').html(jqXHR.responseText);
    });

}



function resubmitAll(url, mode, ids) {

    $.ajax({
        url: url,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({            
            mode:mode,
            items: ids
        }),
        success: function (data) {

            $('#report-' + ids).html(data);            
        }

    });
}

function resubmitUser(url, id) {
    $.post(
        url,
        { 'id': id },
        function (data) {
            $('#report-' + id).dialog('close');
        }
    ).error(function () {
        $('#report-' + id + ' td.error').html('Ошибка редактирования пользователя');
    });
}