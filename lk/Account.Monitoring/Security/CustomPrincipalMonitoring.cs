﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using Account.DAO;

namespace Monitoring.Security
{
    /// <summary>
    /// Данный класс используется добавляет возможность обращаться к данным
    /// профиля пользователя хранящемуся в базе напрямую через объект User.
    /// </summary>
    public class CustomPrincipalMonitoring : IPrincipal
    {
        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role) { return false; }       

        public CustomPrincipalMonitoring()
        {
            this.Identity = new GenericIdentity("");
        }

        public CustomPrincipalMonitoring(string name)
        {
            this.Identity = new GenericIdentity(name);
        }

        public static CustomPrincipalMonitoring Anonymous
        {
            get
            {
                return new CustomPrincipalMonitoring();
            }
        }

        public UserMonitoring AccountMonitoring { get; set; }
      
    }
}