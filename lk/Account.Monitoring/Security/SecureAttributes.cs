﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Account.DAO;


namespace Monitoring.Security
{
    public class AllowSelfOrAdminAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
            string id = filterContext.ActionParameters["id"].ToString();
            CustomPrincipalMonitoring User = (CustomPrincipalMonitoring)filterContext.HttpContext.User;

            if (id != User.AccountMonitoring.Login && !User.AccountMonitoring.IsAdmin)
                    throw new HttpException(403, "Доступ запрещен");
           

            base.OnActionExecuting(filterContext);
        }
    }   

    public class AllowAdminOnlyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            CustomPrincipalMonitoring User = (CustomPrincipalMonitoring)filterContext.HttpContext.User;

            if (!User.AccountMonitoring.IsAdmin)
                throw new HttpException(403, "Доступ запрещен");

            base.OnActionExecuting(filterContext);
        }
    }   
}