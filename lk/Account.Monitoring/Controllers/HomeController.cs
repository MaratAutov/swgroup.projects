﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Script.Serialization;
using System.Configuration;

using Account.DAO;
using Account.Tools;
using Monitoring.Models;
using Monitoring.Security;
using Account.DAO.Models;
using Account.DAO.Services;
using Monitoring.SPOService;
using Account.MailService;
using System.IO;
using Monitoring.Helpers;
//using Progresoft.Accounts.ElectroSignature;

namespace Monitoring.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            if (User != null)
            {
                if (!User.Identity.IsAuthenticated)
                    return RedirectToAction("Login", "Account");
                return RedirectToAction("Statuses");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }            
        }


        [AllowAdminOnly]
        [HttpGet]
        public ActionResult UserMonitoring()
        {
            return View(new UserMonitoringModel());
        }

        [HttpPost]
        [AllowAdminOnly]
        public ActionResult UserMonitoring(UserMonitoringModel model)
        {
            var users = UserMonitoringService.GetStats();
            return PartialView("UserMonitoringPartial", users);
        }

        [HttpPost]
        public ActionResult ActionsUsers(int id)
        {
            var info = UserMonitoringService.GetFullInfo(id);
            return PartialView("_UserEditPartial", info);
        }

        [HttpGet]
        public ActionResult Statuses()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Statuses(DateTime? from, DateTime? to)
        {
            if (!from.HasValue)
                from = DateTime.Parse("01.01.2009");
            if (!to.HasValue)
                to = DateTime.Now;

            var stats = StatusService.GetStats(from.Value, to.Value);
            var model = new List<StatusStats>();
            for (int i = 0; i < (int)PrimaryStatus._LAST_; ++i)
            {
                var s = stats.Find(m => m.PrimaryCode == i) ?? new StatusStats()
                {
                    PrimaryCode = i,
                    Count = 0
                };
                model.Add(s);
            }
            return PartialView("_StatusesPartial", model);
        }

        [HttpGet]
        public ActionResult DateLimit()
        {
            return View(new PackageFilterLimitModel());
        }

        [HttpPost]
        public ActionResult DateLimit(PackageFilterLimitModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            if (!model.Year.HasValue)
                model.Year = null;
            if (!model.Quarter.HasValue)
                model.Quarter = null;
            if (!model.Month.HasValue)
                model.Month = null;
            List<PackageShortInfo> rows = null;
            switch (model.Period)
            {
                case PackagePeriodType.Year:
                    rows = PackageDateLimitService.GetYearStats((int)model.Year);
                    break;
                case PackagePeriodType.Quarter:
                    rows = PackageDateLimitService.GetQuarterStats((int)model.Year, (int)model.Quarter);
                    break;
                case PackagePeriodType.Month:
                    rows = PackageDateLimitService.GetQuarterStats((int)model.Year, (int)model.Month);
                    break;
            }
            return PartialView("_DateLimitPartial", rows);
        }

        [HttpGet]
        public ActionResult DateLimitList()
        {
            return View(PackageDateLimitService.GetAll());
        }

        [HttpGet]
        public ActionResult DateLimitEdit(int year)
        {
            return View((PackageDateLimitModel)PackageDateLimitService.Get(year));
        }

        [HttpGet]
        public ActionResult DateLimitCreate()
        {
            return View("DateLimitEdit", new PackageDateLimitModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DateLimitCreate(PackageDateLimitModel model)
        {
            if (!ModelState.IsValid)
                return View("DateLimitEdit", model);
            PackageDateLimit p = model.ConvertToDAO();
            PackageDateLimitService.Add(p);
            return RedirectToAction("DateLimitList", "Home");
        }

        [HttpPost]
        public ActionResult DateLimitDelete(int? year)
        {
            PackageDateLimitService.Delete(year);
            return PartialView("_DateLimitListPartial", PackageDateLimitService.GetAll());// RedirectToAction("DateLimitList", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DateLimitEdit(PackageDateLimitModel model)
        {
            if (!ModelState.IsValid)
                return View("DateLimitEdit", model);
            PackageDateLimit p = model.ConvertToDAO();
            PackageDateLimitService.Update(p);
            return RedirectToAction("DateLimitList", "Home");
        }

        [HttpGet]
        public ActionResult Users()
        {
            return View(new UserFilterModel());
        }

        [HttpGet]
        public ActionResult UserRegister()
        {
            return View(new UserFilterModel());
        }

        [HttpPost]
        public ActionResult Users(UserFilterModel model)
        {
            if (!model.from.HasValue)
                model.from = DateTime.Parse("01.01.2009");
            if (!model.to.HasValue)
                model.to = DateTime.Now;
            if (((model.inn == null) || (model.inn.Trim() == "")) &&
                ((model.orgName == null) || (model.orgName.Trim() == "")) &&
                  model.currentStatus == null)
            {
                var users = UserService.GetStats(model.from.Value, model.to.Value);
                return PartialView("_UserPartial", users);
            }
            else
            {
                if (model.inn != null) model.inn = model.inn.Trim();
                if (model.orgName != null) model.orgName = model.orgName.Trim();
                var usercount = UserService.GetCountByInnOrName(model.inn, model.orgName);
                if (usercount != 0)
                {
                    var users = UserService.GetStats(model.from.Value, model.to.Value, model.inn, model.orgName, model.currentStatus);
                    return PartialView("_UserPartial", users);
                }
                else
                {
                    ModelState.AddModelError(
                        "UserFilterModel",
                        "Организация не найдена");
                    var users = UserService.GetStats(model.from.Value, model.to.Value, model.inn);
                    return PartialView("_UserPartial", users);
                }
            }            
        }

        [HttpPost]
        public ActionResult UserRegister(UserFilterModel model)
        {
            if (!model.from.HasValue)
                model.from = DateTime.Parse("01.01.2009");
            if (!model.to.HasValue)
                model.to = DateTime.Now;
            
                var users = UserService.GetStatsRegister(model.from.Value, model.to.Value);
                return PartialView("_UserRegisterPartial", users);
            
        }

        [HttpGet]
        public ActionResult Packages()
        {
            return View(new PackageFilterModel());
        }

        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();            
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public ActionResult Packages(PackageFilterModel model,string orderField)
        {
            if (!model.from.HasValue)
                model.from = DateTime.Parse("01.01.2009");
            if (!model.to.HasValue)
                model.to = DateTime.Now;

            int currentStatusCode = model.Package.CurrentStatusCode.HasValue ? 
                                    model.Package.CurrentStatusCode.Value : -1;
            int initialStatusCode = model.Package.InitialStatusCode.HasValue ?
                                    model.Package.InitialStatusCode.Value : -1;
            try
            {
                var packages = PackageService.GetStats(
                    model.from.Value,
                    model.to.Value,
                    model.Package.ID,
                    model.Package.Name,
                    model.Package.Owner,
                    model.Package.Signature,
                    currentStatusCode,
                    initialStatusCode,
                    model.Report.INN,
                    model.Report.OGRN,
                    model.Report.Model,
                    model.Report.Type,
                    model.Report.OrgType,
                    model.Report.DocType,
                    model.Report.Year,
                    model.Report.Quartal,
                    model.Report.Month,
                    model.Report.Date,
                    orderField
                );
                return PartialView("_PackagesPartial", packages);
            }
            catch (Exception ex)
            {
                Tools.Instance.log.DebugException("Error processing package stats", ex);
                return PartialView("_PackagesPartial");
            }
        }

        [HttpPost]
        public ActionResult Report(int id)
        {
            var info = PackageService.GetFullInfo(id);
            return PartialView("_ReportPartial", info);
        }

        public class Items
        {
            public int[] items { get; set; }
        }

        [HttpPost]
        public ActionResult PackagesHistoryByOrganization(OrganizationHistoryInfo info)
        {
            List<HistoryInfo> history = new List<HistoryInfo>();
            PackageFullInfo package = new PackageFullInfo();
            package.INN = info.INN;
            package.Type = info.Type;
            package.Year = info.Year;
            package.Quartal = info.Quartal;
            package.Month = info.Month;
            package.Date = info.Date;
            package.DocTypeCode = info.DocTypeCode;
            switch (info.report_type)
            {
                case "year":
                    history.Add(PackageHistoryService.GetHistory_Year(package));
                    break;
                case "quartal":
                    history.Add(PackageHistoryService.GetHistory_Quarter(package));
                    break;
                case "month":
                    history.Add(PackageHistoryService.GetHistory_Month(package));
                    break;
                case "date":
                    history.Add(PackageHistoryService.GetHistory_Date(package));
                    break;
            }
            
            return PartialView("_PackagesHistoryPartial", history);
        }

        [HttpPost]
        public ActionResult PackagesHistory(Items ids)
        {
            if (ids == null || ids.items == null)
                return null;
            List<HistoryInfo> history = new List<HistoryInfo>();
            foreach (int id in ids.items)
            {
                var info = PackageService.GetFullInfo(id);
                if (info.DocTypeCode == null)
                    history.Add(new HistoryInfo(info, "Не удалось распознать тип пакета"));
                else
                {
                    try
                    {
                        ExtPackagePeriodType periodType = PackagePeriodUtils.GetPackagePeriodType(info);
                        switch (periodType)
                        {
                            case ExtPackagePeriodType.Year:
                                history.Add(PackageHistoryService.GetHistory_Year(info));
                                break;
                            case ExtPackagePeriodType.Quarter:
                                history.Add(PackageHistoryService.GetHistory_Quarter(info));
                                break;
                            case ExtPackagePeriodType.Month:
                                history.Add(PackageHistoryService.GetHistory_Month(info));
                                break;
                            case ExtPackagePeriodType.OnDay:
                                history.Add(PackageHistoryService.GetHistory_Date(info));
                                break;
                        }
                    }
                    catch
                    {
                        history.Add(new HistoryInfo(info, "Не удалось распознать периодичность подачи отчета"));
                    }
                    
                }
            }
            return PartialView("_PackagesHistoryPartial", history);
        }

        [HttpGet]
        public ActionResult PackagesHistoryReport()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PackagesHistoryReport(DateTime? from, DateTime? to)
        {
            if (!from.HasValue)
                from = DateTime.Parse("01.01.2009");
            if (!to.HasValue)
                to = DateTime.Now;

            return PartialView("_OrganizationHistoryListPartial", PackageHistoryService.GetOrganizationList(from.Value, to.Value));
        }

        [HttpGet]
        [AllowAdminOnly]
        public ActionResult Workflow()
        {
            return View(new PackageFilterModel());
        }

        [HttpPost]
        public ActionResult Actions(int id)
        {
            var info = PackageService.GetFullInfo(id);
            return PartialView("_WorkflowPartial", info);
        }

        [HttpPost]
        public ActionResult ActionsList(Items ids)
        {
            if (ids == null || ids.items == null)
                return null;
            List<int> list = new List<int> { };
            foreach (int id in ids.items)
            {                
                list.Add(id);
            }
            return PartialView("WorkflowListPartial", list);
        }

        //обработка только пакетов в статусе SED_Awaiting у которых есть полученные ответы от СЭД сохраненные в БД
        private void ResubmitSedResponceFromDB(int id)
        {
            var p = PackageService.GetFullInfo(id);

            if (p.CurrentStatusCode != (int)PrimaryStatus.SED_Awaiting)
            {
                Tools.Instance.log.Error("Error processing package id {0}. Must be in SED_Awaiting status", id);
                return;
            }

            var mailDBO = MailMessageService.GetFirstMessageFromSed(id);
            if (mailDBO == null)
            {
                Tools.Instance.log.Error("Not found mail record from package id {0}", id);
                return;
            }
            string filename = null;
            System.Net.Mail.MailMessage mailmsg = new System.Net.Mail.MailMessage();
            try
            {
                filename = MailStoreHelper.GetAbsoluteFileName(mailDBO.Url);
                mailmsg.Load(filename);
            }
            catch (Exception ex)
            {
                Tools.Instance.log.Error("Not loading message url {0} Exception :: {1}", filename, ex);
                return;
            }

            SedResponse response = new SedResponse();
            response.Subject = mailDBO.Subject;

            if (mailmsg.Attachments.Count != 1)
            {
                Tools.Instance.log.Error("Not finding attachment for mail id {0}", mailDBO.ID);
                return;
            }

            byte[] buffer = new byte[256];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = mailmsg.Attachments[0].ContentStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                response.XmlData = ms.ToArray();
            }
            using (var cl = new SPOService.SPOServiceSoapClient())
            {
                try
                {
                    cl.ProcessSedResponse(response);
                }
                catch (Exception ex)
                {
                    Tools.Instance.log.Error(String.Format("Error reprocessing sed package id = {0} error = {1} innerexception = {2}", id, ex, ex.InnerException));
                    throw;
                }
            }
        }

        void ResubmitReestrRegistration(int id, int mode)
        {
            using (var cl = new SPOService.SPOServiceSoapClient())
            {
                try
                {
                    if (mode == 6)
                        cl.ReAcquireRoName(id);
                    else if (mode == 7)
                        cl.ReRegisterInReestr(id);
                }
                catch (Exception ex)
                {
                    Tools.Instance.log.Error(String.Format("Error reprocessing sed package id = {0}; mode = {3}; error = {1} innerexception = {2}", id, ex, ex.InnerException, mode));
                    throw;
                }
            }
        }

        public void ResubmitPacket(int mode, int id)
        {
            if (mode == 5)
            {
                ResubmitSedResponceFromDB(id);
                return;
            }
            else if (mode == 6 || mode == 7)
            {
                ResubmitReestrRegistration(id, mode);
                return;
            }
            var package = PackageService.GetFullInfo(id);
            if (package == null)
                throw new HttpException(503, "Error processing requested id");

            var blob = PackageService.GetBlob(package.BlobID);
            if (blob == null)
                throw new HttpException(503, "Error processing blob for requested id");

            // валидация для нее не нужно читать архив
            if (mode == 4)
            {
                UpdateStatusPackage.Build(
                        package.ID,
                        PrimaryStatus.Accepted
                    );
                using (var cl = new SPOService.SPOServiceSoapClient())
                {
                    var p = ConvertToPackage(package);
                    cl.ValidatePackage(new SPOService.UploadedPackage()
                    {
                        Package = p,
                        Data = blob.Body,
                        IsRO = package.IsRoReport,
                        Name = package.Name,
                        OwnerID = package.OwnerID
                    });
                }
                return;
            }

            IReportPackage report = ReportPackageManager.Load(
                "report.zip",
                blob.Body
            );
            byte[] xmlData = report.ReportBody;
            if (xmlData == null)
                throw new HttpException(503, "Error processing archive for requested id");

            switch (mode)
            {
                case 0: // Resubmit package from SPOService processing
                    UpdateStatusPackage.Build(
                        package.ID,
                        PrimaryStatus.Accepted
                    );
                    
                    using (var cl = new SPOService.SPOServiceSoapClient())
                    {
                        try
                        {
                            cl.ProcessPackage(new SPOService.InteropPackage()
                            {
                                Id = package.ID,
                                ExternalId = package.ExternalID,
                                Model = package.Model,
                                Type = package.Type,
                                XmlData = xmlData,
                                Version = package.Version
                            });
                        }
                        catch (Exception ex)
                        {
                            Tools.Instance.log.ErrorException(String.Format("Error reprocessing package id = {0} error = {1} innerexception = {2}", package.ID, ex, ex.InnerException), ex);
                            throw;
                        }
                    }
                    break;
                case 1: // Resubmit package from sending email to SED
                    UpdateStatusPackage.Build(
                        package.ID,
                        PrimaryStatus.SED_Awaiting
                    );
                    
                    using (var cl = new Account.Tools.MailService.MailServiceSoapClient())
                    {
                        var mail = MailMessageService.GetLastMessageToSed(package.ID);
                        if (!String.IsNullOrEmpty(mail.Url))
                        {
                            cl.EnqueueForSend(new Account.Tools.MailService.OutMessage()
                            {
                                EmlUrl = mail.Url
                            });
                        }
                        else
                            throw new ArgumentNullException("Message not saved in store");
                    }
                    break;
                case 2: // Resubmit package from processing SED response
                    using (var cl = new Account.Tools.MailService.MailServiceSoapClient())
                    {
                        var mail = MailMessageService.GetLastMessageFromSed(package.ID);
                        if (!String.IsNullOrEmpty(mail.Url))
                        {
                            cl.EnqueueForSend(new Account.Tools.MailService.OutMessage()
                            {
                                EmlUrl = mail.Url
                            });
                        }
                        else
                            throw new ArgumentNullException("Message not saved in store");
                    }
                    UpdateStatusPackage.Build(
                        package.ID,
                        PrimaryStatus.SED_Awaiting
                    );
                    break;
                case 3:
                    using (var cl = new Account.Tools.MailService.MailServiceSoapClient())
                    {
                        cl.EnqueueForFetch(id.ToString());
                    }
                    break;
                default:
                    throw new HttpException(503, "Error processing requested mode");
            }            
        }


        [HttpPost]
        public ActionResult Resubmit(int mode, int id)
        {
            ResubmitPacket(mode, id);
            return new HttpStatusCodeResult(200, "OK");
        }

        [HttpPost]
        public ActionResult ResubmitAll(int mode, Items ids)
        {
            Exception exc = null;
            int excid = -1; 
            int totalerrorcount = 0;
            foreach (int packageID in ids.items)
            {
                try
                {
                    ResubmitPacket(mode, packageID);
                }
                catch (Exception ex)
                {
                    totalerrorcount++;
                    if (exc == null)
                    {
                        exc = ex;
                        excid = packageID;
                    }
                    Tools.Instance.log.Error("Error resubmit package {0}; mode {1}; with error {2}", packageID, mode, ex);
                }
            }
            if (exc != null)
            {
                throw new Exception(String.Format("Error resubmit package total count {0}; first error package id {1}; with error {2}", totalerrorcount, excid, exc));
            }
            return new HttpStatusCodeResult(200, "OK");
        }       

        SPOService.Package ConvertToPackage(PackageFullInfo package)
        {
            SPOService.Package result = new SPOService.Package();
            result.BlobID = package.BlobID;
            result.CurrentStatusID = package.CurrentStatusID;
            result.ID = package.ID;
            result.InitialStatusID = package.InitialStatusID;
            result.Name = package.Name;
            result.OwnerID = package.OwnerID;
            result.ReportID = package.ReportID;
            result.SignatureType = package.SignatureType;
            result.UploadTime = package.UploadTime;
            return result;
        }

        [HttpPost]
        public ActionResult DeleteUser(int id)
        {
            return RedirectToAction("UserMonitoring", "Home");
        }

        [HttpPost]
        public ActionResult EditUser()
        {           
            return RedirectToAction("RegistrationRequest", "Home");
        }

        public ActionResult PackageCompare(string ids, int current_id)
        {
            if (String.IsNullOrEmpty(ids))
                return View("CustomError", new Exception("Нет файлов для сравнения"));

            string[] all_id = ids.Split(',');
            try
            {
                CompareHelper comparer = new CompareHelper();
                var result = comparer.Compare(all_id, current_id);
                return View(result);
            }
            catch (Exception ex)
            {
                return View("CustomError", ex);
            }
        }

    }
}
