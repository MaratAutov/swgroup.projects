﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Account;
using System.Net;
using System.Configuration;

namespace Monitoring.Controllers
{
    public class AccountServiceGetInfo
    {
        public void GetStatus()
        {
            try
            {
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["AccountURL"]);                
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
                    Error = "";               
                myHttpWebResponse.Close();
                IsActive = true;
            }
            catch (WebException e)
            {
                Error = string.Format("\r\nWebException Raised. The following error occured : {0}", e.Status);
                IsActive = false;
                SendMail(Error);
            }
            catch (Exception e)
            {
                Error = string.Format("\nThe following Exception was raised : {0}", e.Message);
                IsActive = false;
                SendMail(Error);
            }
        }

        public void SendMail(string error)
        {
            try
            {
                using (var cl = new Account.Tools.MailService.MailServiceSoapClient())
                {
                    cl.EnqueueForSend(new Account.Tools.MailService.OutMessage()
                    {
                        From = ConfigurationManager.AppSettings["NotificationEmail"],
                        To = ConfigurationManager.AppSettings["NotificationEmail"],
                        Subject = "Account",
                        Body = error,
                        IsHtml = true
                    });
                }
            }
            catch (Exception ex)
            {
                Tools.Instance.log.Error(ex);
            }
        }

        public AccountServiceGetInfo()
        {
            GetStatus();
        }

        public bool IsActive { get; set; }
        public string Error { get; set; }
    }

    public class ServicesController : Controller
    {
        //
        // GET: /Services/

        public ActionResult Index()
        {
            List<object> stats = new List<object>();

            AccountServiceGetInfo account = new AccountServiceGetInfo();

            stats.Add(account);
            

            using (SPOService.SPOServiceSoapClient cl = new SPOService.SPOServiceSoapClient())
            {
                stats.Add(cl.Status());
            }

            using (Account.Tools.MailService.MailServiceSoapClient cl = new Account.Tools.MailService.MailServiceSoapClient())
            foreach (var s in cl.Status())
            {
                stats.Add(s);
            }
            
            using (ReportService.ReportServiceSoapClient cl = new ReportService.ReportServiceSoapClient())
            {
                stats.Add(cl.Status());
            }
            
            return View(stats);
        }

    }
}