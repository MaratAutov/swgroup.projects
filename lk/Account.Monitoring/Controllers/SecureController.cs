﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Monitoring.Security;

namespace Monitoring.Controllers
{
    public class SecureController : Controller
    {
        protected virtual new CustomPrincipalMonitoring User
        {
            get
            {
                if( HttpContext.User.Identity.IsAuthenticated )
                    return HttpContext.User as CustomPrincipalMonitoring;

                else
                    return CustomPrincipalMonitoring.Anonymous;
            }
        }
    }
}