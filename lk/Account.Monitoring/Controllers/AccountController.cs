﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Script.Serialization;
using System.Configuration;
using Monitoring.Models;
using Monitoring.Security;
using Account.DAO;
using Account.Tools;
using System.IO;

namespace Monitoring.Controllers
{
    [Authorize]
    public class AccountController : SecureController
    {
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            returnUrl = returnUrl ?? "/Home/Index";

            if (User != null)
            {
                if (User.Identity.IsAuthenticated)
                    return Redirect(returnUrl);
            }            
            
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }        


        [HttpPost]        
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();            
            return RedirectToAction("Login");
        }

        private void RedirectAndCookie(bool remember, string username, UserMonitoring user)
        {
            JavaScriptSerializer serializer =
                            new JavaScriptSerializer();
            string userData = serializer.Serialize(user);

            DateTime expires = DateTime.Now.AddMinutes(FormsAuthentication.Timeout.TotalMinutes);
            if (remember)
            {
                expires = DateTime.Now.AddYears(1);
            }

            FormsAuthenticationTicket authTicket =
                new FormsAuthenticationTicket(
                     1,
                     username,
                     DateTime.Now,
                     expires,
                     remember,
                     userData
                );
            
            string encTicket = FormsAuthentication.Encrypt(
                authTicket
            );
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);

            FormsAuthentication.SetAuthCookie(username, true);

            faCookie.Expires = authTicket.Expiration;                
            
            Response.Cookies.Add(faCookie);           
        }

        private bool _userExist;

        public bool UserExist
        {
            get
            {
                return _userExist;
            }
            set
            {
                _userExist = value;
            }
        }


        public UserMonitoring UsersExist(string username, string password)
        {
            UserExist = false;

            UserMonitoring user = new UserMonitoring();

            
                user = UserMonitoringService.Get(
                    username, password
                );

                if (user != null)
                {
                    UserExist = true;                    
                }               
            
            return user;
        }
        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModels model, string returnUrl)
        {
            bool isActiv = false;
            string s = returnUrl;
            if (ModelState.IsValid)
            {
                UserMonitoring user = UsersExist(model.UserName, model.Password);

                if (UserExist)
                {
                    if (user.IsActive)
                    {
                        isActiv = true;
                    }
                }

                if ((UserExist) && (isActiv))
                {
                    RedirectAndCookie(model.RememberMe, model.UserName, user);                    
                        return RedirectToAction("Statuses", "Home");                   
                }
            }

            if (!UserExist)
            {
                ModelState.AddModelError("", "Неверное имя пользователя или пароль.");
            }
            else if (!isActiv)
            {
                ModelState.AddModelError("", "Пользователь не зарегистрирован.");
            }

            return View(model);            
        }

       
        [HttpGet]
        public ActionResult RegistrationRequest()
        {
            return View();
        }

        [AllowAdminOnly]
        [HttpGet]
        public ActionResult EditUser(string id, string returnUrl)
        {
            UserMonitoring acc = UserMonitoringService.GetByID(id);
            return View(new RegistrationRequestModel(acc));          
            
        }


        [HttpPost]
        public ActionResult ResetPassword()
        {
            var acc = new UserMonitoringModel();
            acc.Password = Membership.GeneratePassword(8, 1);
            return PartialView("_ResetPasswordPartial", acc);
        }


        [AllowAdminOnly]
        [HttpPost]
        public ActionResult DeleteUser(string id, string returnUrl)
        {
            if (User.AccountMonitoring.ID == Convert.ToInt32(id))
            {
                // Delete self ?
            }
            else
                UserMonitoringService.DeleteByID(Convert.ToInt32(id));

            return Redirect(returnUrl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegistrationRequest(RegistrationRequestModel model)
        {
            var dups = UserMonitoringService.Exists(model.Login);
            foreach (var u in dups)
            {
                if (u.Login == model.Login)
                    ModelState.AddModelError("Login", "Данный логин уже зарегистрирован");
            }

            if (ModelState.IsValid)
            {
                bool isAdmin = false;
                if (model.Role == "Администратор") isAdmin = true;
                try
                {
                    UserMonitoringService.Add(
                        model.Login,
                        model.Password,
                        model.RegionUser,
                        model.FIO,
                        true,
                        isAdmin
                    );
                   
                   // return View("RegistrationAccepted", model);
                    return RedirectToAction("UserMonitoring", "Home");
                }
                catch
                {
                    ModelState.AddModelError(
                       "RegistrationRequestModel",
                       "Непредвиденная ошибка обработки данных."
                   );
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(RegistrationRequestModel model)
        {
            try
            {
                UserMonitoring acc = UserMonitoringService.Get(model.Login);

                bool isAdmin = false;
                if (model.Role == "Администратор")
                {
                    isAdmin = true;
                }
                UserMonitoringService.Update(
                    model.Login,
                    model.Password,
                    model.RegionUser,
                    model.FIO,
                    isAdmin,
                    true
                );              


                return RedirectToAction(
                    "UserMonitoring", "Home", new { id = model.Login }
                );
            }
            catch
            {
                ModelState.AddModelError(
                   "UserMonitoringModel",
                   "Непредвиденная ошибка обработки данных."
               );
            }
            return View(model);
        }

        [NonAction]
        protected void ValidateNumeric(string str, int len, string field)
        {
            Regex seriaRegex = new Regex(@"^\d{" + len.ToString() + "}$");
            if (!seriaRegex.IsMatch(str))
                ModelState.AddModelError(
                    field,
                    "Неверное значение"
                );
        }
    }
}
