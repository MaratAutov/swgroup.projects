﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Account.DAO;
using Account.Tools;
using ICSharpCode.SharpZipLib.Zip;

namespace Monitoring.Controllers
{
    public class DownloadController : Controller
    {
        public ActionResult Package(string id)
        {            
            Package p = PackageService.GetPackage(int.Parse(id));

            if (!p.Name.EndsWith(".zip"))
            {
                p.Name = p.Name + ".zip";
            }
            Blob blob = PackageService.GetBlob(p.BlobID);
            return File(blob.Body, "application/zip", p.Name);
        }

        public ActionResult Report(string id)
        {           
            return GetFile(id, true);
        }

        public ActionResult Signature(string id)
        {
            return GetFile(id, false);
        }

        [HttpPost]
        public ActionResult DwonloadReports(string ids)
        {
            if (ids == null )
                return null;
            string[] ids_array = ids.Split(',');
            if (ids_array.Length == 0)
                return null;
            byte[] body = null;
            using (var m = new MemoryStream())
            {
                using (var s = new ZipOutputStream(m))
                {
                    foreach (var id in ids_array)
                    {
                        try
                        {
                            var file = GetReport(id.ToString());
                            var bodyEntry = new ZipEntry(file.FileDownloadName);
                            s.PutNextEntry(bodyEntry);
                            s.Write(file.FileContents, 0, file.FileContents.Length);
                        }
                        catch
                        {
                            //при ошибке просто игнорируем
                        }
                    }
                    body = m.GetBuffer();
                }
            }
            return File(body, "application/zip", "reports.zip");

        }

        FileContentResult GetReport(string id)
        {
            Package p = PackageService.GetPackage(int.Parse(id));
            
            if (p.IsImported == true)
            {
                ReportInfo rep = PackageService.GetReportInfo(p.ID);
                string filename = string.Empty;
                filename = MailStoreHelper.GetAbsoluteFileName(rep.DocumentUrl);
                return File(System.IO.File.ReadAllBytes(filename), "application/text", id + ".zip");
            }
            else
            {
                Blob blob = PackageService.GetBlob(p.BlobID);
                IReportPackage package = ReportPackageManager.Load("report.zip", blob.Body);
                return File(package.ReportBody, "application/text", id + ".xtdd");
            }
        }

        public FileContentResult GetFile(string id,bool report)
        {
            Package p = PackageService.GetPackage(int.Parse(id));
            if (p.IsImported == true)
            {
                ReportInfo rep = PackageService.GetReportInfo(p.ID);
                string filename = string.Empty;
                if (report)
                {
                    filename = MailStoreHelper.GetAbsoluteFileName(rep.DocumentUrl);
                }
                else
                {
                    filename = MailStoreHelper.GetAbsoluteFileName(rep.DocumentSignUrl);
                }
                string name = Path.GetFileName(filename);
                if (!name.EndsWith(".zip"))
                {
                    name = name + ".zip";
                }
                return File(System.IO.File.ReadAllBytes(filename), "application/text", name);
            }
            else
            {
                Blob blob = PackageService.GetBlob(p.BlobID);
                IReportPackage package = ReportPackageManager.Load("report.zip", blob.Body);
                if (report)
                {
                    var name = package.ReportFileName;
                    if (name.ToLower().EndsWith(".zip"))
                        name = name.Remove(name.Length - 4, 4);
                    if (!name.ToLower().EndsWith(".xtdd"))
                        name = name + ".xtdd";

                    return File(package.ReportBody, "application/text", name);
                }
                else
                {
                    return File(package.ReportSign, "application/text", package.SignFileName);
                }
            }
        }

        public ActionResult MailMessage(int id)
        {
            var mail = MailMessageService.GetMailMessage(id);
            string filename = MailStoreHelper.GetAbsoluteFileName(mail.Url);
            return File(System.IO.File.ReadAllBytes(filename), "application/text", Path.GetFileName(filename));
        }
    }
}
