﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Monitoring.Helpers
{
    public class DateTimeHelper
    {
        public static string ToDate(DateTime? dt)
        {
            if (dt == null)
                return "";
            DateTime dt1 = (DateTime)dt;
            return dt1.ToShortDateString();
        }
    }
}