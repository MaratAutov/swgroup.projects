﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Account.DAO;
using Account.Tools;
using System.Xml.Schema;
using System.Xml;
using System.Xml.XPath;

namespace Monitoring.Helpers
{
    public class fileHelper
    {
        public byte[] data;
        public XmlHelper xmlHelper;

        public fileHelper(byte[] data)
        {
            this.data = data;
            xmlHelper = new XmlHelper(data);
        }
    }

    public class CompareHelper
    {
        Dictionary<int, fileHelper> xtdd_files = new Dictionary<int, fileHelper>();
        fileHelper current_xtdd_file;
        string xsd_schem;
        CompareResult Result;

        public CompareResult Compare(string[] ids, int current_id)
        {
            Result = new CompareResult();
            
            if (ids.Length < 2)
                throw new InvalidOperationException("Для сравнения должно быть как минимум два файла");
            if (current_id != 0)
            {
                var p = GetPackage(current_id);
                PackageManager pm = p.pm;
                Result.ChangesHeaders.Add(GetHeader(p.package));
                pm.VerifyXtdd();
                xsd_schem = pm.xsdSchemeFinal;

                current_xtdd_file = new fileHelper(pm.ReportPackage.ReportBody);
            }
            Result.CompareCurrent = current_xtdd_file != null;
            foreach (string id in ids)
            {
                if (id != current_id.ToString())
                {
                    int idi = Int32.Parse(id);
                    var p1 = GetPackage(idi);
                    PackageManager pm1 = p1.pm;
                    xtdd_files.Add(idi, new fileHelper(pm1.ReportPackage.ReportBody));
                    Result.ChangesHeaders.Add(GetHeader(p1.package));

                    if (String.IsNullOrEmpty(xsd_schem))
                    {
                        pm1.VerifyXtdd();
                        xsd_schem = pm1.xsdSchemeFinal;
                    }
                }
            }

            iterateSchema(xsd_schem);
            return Result;
        }

        string GetHeader(Package p)
        {
            return String.Format("ID: {0} <br />Дата создания: {1}", p.ID, p.UploadTime);
        }

        PackageResult GetPackage(int id)
        {
            Package currentPackage = PackageService.GetPackage(id);
            Blob current_blob = PackageService.GetBlob(currentPackage.BlobID);
            PackageManager pm = new PackageManager();
            pm.Open(currentPackage.Name, current_blob.Body);
            return new PackageResult(currentPackage, pm);
        }

        class PackageResult
        {
            public Package package;
            public PackageManager pm;

            public PackageResult(Package p, PackageManager pm)
            {
                package = p;
                this.pm = pm;
            }
        }

        void iterateSchema(string path)
        {
            XmlSchemaSet ss = new XmlSchemaSet();
            XmlTextReader reader = new XmlTextReader(path);
            XmlSchema customerSchema = XmlSchema.Read(reader, ValidationCallback);
            ss.Add(customerSchema);
            ss.Compile();
            
            foreach (XmlSchemaElement element in customerSchema.Elements.Values)
            {
                iterateOverElement("", element, "");
            }
        }

        string GetElementDocumentation(XmlSchemaElement element)
        {
            if (element.Annotation == null)
                return "";
            foreach (var annot in element.Annotation.Items)
            {
                if (annot is XmlSchemaDocumentation)
                    if ((annot as XmlSchemaDocumentation).Markup.Length > 0)
                    {
                        XmlNode description = (annot as XmlSchemaDocumentation).Markup.Where(x => x.ParentNode.LocalName == "documentation").FirstOrDefault();
                        if (description != null)
                            return description.Value;
                    }
            }
            return "";
        }

        void ValidatePath(string path, XmlSchemaElement element, string documentation, XPathNavigator[] navigators = null)
        {
            try
            {
                string current_value = null;
                CompareChanges changes = new CompareChanges(element.Name, documentation);
                // при построении из отчета current_xtdd_file будет null его не нужно сравнивать
                if (Result.CompareCurrent)
                {
                    if (navigators == null) current_value = current_xtdd_file.xmlHelper.GetValue(path);
                    else
                    {
                        if (navigators[0] == null)
                            current_value = "-";
                        else
                            current_value = current_xtdd_file.xmlHelper.GetValue(path, navigators[0]);
                    }
                    changes.AddValue(current_value);
                }

                bool isChanges = false;
                int j;
                if (Result.CompareCurrent)
                    j = 0;
                else
                    j = -1;
                foreach (var file in xtdd_files.Values)
                {
                    j++;
                    string new_value = null;
                    if (navigators == null) new_value = file.xmlHelper.GetValue(path);
                    else
                    {
                        if (navigators[j] == null)
                            new_value = "-";
                        else
                            new_value = file.xmlHelper.GetValue(path, navigators[j]);
                    }
                    changes.AddValue(new_value);
                    if (Result.CompareCurrent && current_value != new_value)
                    {
                        isChanges = true;
                    }
                    else if (!Result.CompareCurrent)
                    {
                        foreach (var value in changes.Values)
                        {
                            if (new_value != value)
                            {
                                isChanges = true;
                                break;
                            }
                        }
                    }
                }
                if (isChanges)
                    Result.Changes.Add(changes);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        void ValidateUnboundedPath(string path, XmlSchemaElement element, string documentation)
        {
            try
            {
                XPathNodeIterator[] iterators = null;
                //получаем итераторы по коллекциям всех файлов
                if (Result.CompareCurrent)
                {
                    iterators = new XPathNodeIterator[xtdd_files.Count + 1];
                    iterators[0] = current_xtdd_file.xmlHelper.GetNodesIterator(path);
                    for (int i = 1; i <= xtdd_files.Count; i++)
                    {
                        iterators[i] = xtdd_files.Values.ToArray()[i - 1].xmlHelper.GetNodesIterator(path);
                    }
                }
                else
                {
                    iterators = new XPathNodeIterator[xtdd_files.Count];
                    for (int i = 0; i < xtdd_files.Count; i++)
                    {
                        iterators[i] = xtdd_files.Values.ToArray()[i].xmlHelper.GetNodesIterator(path);
                    }
                }

                int index = 0;
                bool[] end_rows = new bool[iterators.Length];
                while (true)
                {
                    index++;
                    for (int j = 0; j < iterators.Length; j++)
                    {

                        if (!end_rows[j] && !iterators[j].MoveNext())
                        {
                            end_rows[j] = true;

                            if (end_rows.Where(x => x == false).Count() == 0)
                                return;
                        }
                    }
                    XPathNavigator[] navigators = new XPathNavigator[iterators.Length];

                    for (int k = 0; k < iterators.Length; k++)
                    {
                        if (end_rows[k] == true)
                            navigators[k] = null;
                        else
                            navigators[k] = iterators[k].Current;
                    }

                    XmlSchemaComplexType complexType = element.ElementSchemaType as XmlSchemaComplexType;

                    if (complexType == null)
                        return;
                    // Get the sequence particle of the complex type.
                    XmlSchemaSequence sequence = complexType.ContentTypeParticle as XmlSchemaSequence;
                    if (sequence == null)
                        return;

                    // Iterate over each XmlSchemaElement in the Items collection.
                    foreach (XmlSchemaElement childElement in sequence.Items)
                    {
                        iterateOverUnboundedElement(navigators, "", childElement, documentation, index);
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        void iterateOverUnboundedElement(XPathNavigator[] navigators, string path, XmlSchemaElement element, string documentation, int index)
        {
            path += "/"  + element.Name;
            documentation += "/" + GetElementDocumentation(element) + String.Format(" (Индекс строки: {0})", index);
            if (element.ElementSchemaType is XmlSchemaSimpleType)
            {
                ValidatePath(path, element, documentation, navigators);
            }

            XmlSchemaComplexType complexType = element.ElementSchemaType as XmlSchemaComplexType;

            if (complexType == null)
                return;
            // Get the sequence particle of the complex type.
            XmlSchemaSequence sequence = complexType.ContentTypeParticle as XmlSchemaSequence;
            if (sequence == null)
                return;

            // Iterate over each XmlSchemaElement in the Items collection.
            foreach (XmlSchemaElement childElement in sequence.Items)
            {
                iterateOverUnboundedElement(navigators, path, childElement, documentation, index);
            }
        }

        void iterateOverElement(string path, XmlSchemaElement element, string documentation)
        {
            path += "/" + element.Name;
            documentation += "/" + GetElementDocumentation(element);
            if (element.ElementSchemaType is XmlSchemaSimpleType)
            {
                ValidatePath(path, element, documentation);
            }
            else if (element.MaxOccursString == "unbounded")
            {
                ValidateUnboundedPath(path, element, documentation);
                return;
            }
            
            // Get the complex type of the Customer element.
            XmlSchemaComplexType complexType = element.ElementSchemaType as XmlSchemaComplexType;

            if (complexType == null)
                return;
            // Get the sequence particle of the complex type.
            XmlSchemaSequence sequence = complexType.ContentTypeParticle as XmlSchemaSequence;
            if (sequence == null)
                return;

            // Iterate over each XmlSchemaElement in the Items collection.
            foreach (XmlSchemaElement childElement in sequence.Items)
            {
                iterateOverElement(path, childElement, documentation);
            }
        }

        public class CompareResult
        {
            public List<CompareChanges> Changes { get; set; }
            public List<String> ChangesHeaders { get; set; }
            // было ли сравнение с определенным пакетом или все пакеты равны
            public bool CompareCurrent { get; set; }

            public CompareResult()
            {
                Changes = new List<CompareChanges>();
                ChangesHeaders = new List<string>();
            }
        }

        public class CompareChanges
        {
            public string Name;
            public string Annotation;

            List<string> values = new List<string>();

            public void InsertValueAt(int pos, string value)
            {
                values.Insert(pos, value);
            }

            public void AddValue(string value)
            {
                values.Add(value);
            }

            public List<string> Values
            {
                get
                {
                    return values;
                }
            }


            public CompareChanges (string Name, string Annotation)
	        {
                this.Name = Name;
                this.Annotation = Annotation;
	        }
            
        }

        void ValidationCallback(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                Console.Write("WARNING: ");
            else if (args.Severity == XmlSeverityType.Error)
                Console.Write("ERROR: ");

            Console.WriteLine(args.Message);
        }
    }
}