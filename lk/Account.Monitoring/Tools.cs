﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Account.Tools;

namespace Monitoring
{
    public class Tools
    {
        public NLog.Logger log;

        static Tools _instance = null;
        public static Tools Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Tools();
                    string logFile = ConfigurationManager.AppSettings["LogFile"];
                    _instance.log = NLogger.GetLog("Monitoring", logFile);
                }
                return _instance;
            }
        }
    }
}