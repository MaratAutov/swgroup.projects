﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Account.DAO;

namespace Monitoring.Models
{   

    public class UserMonitoringModel
    {
        public int ID { get; set; }
        public string Login    { get; set; }
        public string Password    { get; set; }
        public string FIO  { get; set; }
        public string RegionUser { get; set; }      
        public DateTime CreateDate  { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }

        
    }
}