﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Account.Tools;

namespace Monitoring.Models
{
    public class PackageFilter
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
        public string Signature { get; set; }
        public int? CurrentStatusCode { get; set; }
        public int? InitialStatusCode { get; set; }
    }

    public class ReportFilter
    {
        public string INN     { get; set; }
        public string OGRN    { get; set; }
        public string Model   { get; set; }
        public string Type    { get; set; }
        public string DocType { get; set; }
        public string OrgType { get; set; }
        public string Year    { get; set; }
        public string Quartal { get; set; }
        public string Month   { get; set; }
        public DateTime Date  { get; set; }
    }

    public class PackageFilterModel
    {
        public DateTime? from { get; set; }
        public DateTime? to { get; set; }
        public PackageFilter Package { get; set; }
        public ReportFilter Report { get; set; }
    }

    public class UserFilterModel
    {
        public DateTime? from { get; set; }
        public DateTime? to { get; set; }
        public string inn { get; set; }
        public string orgName { get; set; }
        public int? currentStatus { get; set; }
    }

    public class PackageFilterLimitModel
    {
        
        public int? Year { get; set; }
        public Month? Month { get; set; }
        public Quarter? Quarter { get; set; }
        
        public PackagePeriodType Period { get; set; }  

        
        public PackageFilterLimitModel()
        {
            DateTime dt = DateTime.Now;
            this.Year = dt.Year;
            this.Month = (Month)dt.Month - 1;
            this.Quarter = (Quarter)((dt.Month - 1) / 3);
            
        }
    }

    
}