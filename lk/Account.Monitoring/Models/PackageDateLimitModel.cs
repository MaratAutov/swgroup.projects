﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Account.DAO;

namespace Monitoring.Models
{
    public class PackageDateLimitModel
    {
        public int ID { get; set; }

        //[Required(ErrorMessage = "- обязательное поле")]
        [Display(Name = "Год")]
        public int? Year { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "I Квартал")]
        [DataType(DataType.Date)]
        public DateTime Quarter1 { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "II Квартал")]
        [DataType(DataType.Date)]
        public DateTime Quarter2 { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "III Квартл")]
        [DataType(DataType.Date)]
        public DateTime Quarter3 { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "IV Квартал")]
        [DataType(DataType.Date)]
        public DateTime Quarter4 { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Лимит годовых отчетов")]
        [DataType(DataType.Date)]
        public DateTime YearLimit { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Январь")]
        [DataType(DataType.Date)]
        public DateTime January { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Февраль")]
        [DataType(DataType.Date)]
        public DateTime February { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Март")]
        [DataType(DataType.Date)]
        public DateTime March { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Апрель")]
        [DataType(DataType.Date)]
        public DateTime April { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Май")]
        [DataType(DataType.Date)]
        public DateTime May { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Июнь")]
        [DataType(DataType.Date)]
        public DateTime June { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Июль")]
        [DataType(DataType.Date)]
        public DateTime July { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Август")]
        [DataType(DataType.Date)]
        public DateTime August { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Сентябрь")]
        [DataType(DataType.Date)]
        public DateTime September { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Октябрь")]
        [DataType(DataType.Date)]
        public DateTime October { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Ноябрь")]
        [DataType(DataType.Date)]
        public DateTime November { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Декабрь")]
        [DataType(DataType.Date)]
        public DateTime December { get; set; }

        public static implicit operator PackageDateLimitModel(PackageDateLimit p)
        {
            PackageDateLimitModel temp = new PackageDateLimitModel();
            temp.Year = p.Year;
            temp.Quarter1 = p.Quarter1;
            temp.Quarter2 = p.Quarter2;
            temp.Quarter3 = p.Quarter3;
            temp.Quarter4 = p.Quarter4;

            temp.YearLimit = p.YearLimit;

            temp.January = p.January;
            temp.February = p.February;
            temp.March = p.March;
            temp.April = p.April;
            temp.May = p.May;
            temp.June = p.June;
            temp.July = p.July;
            temp.August = p.August;
            temp.September = p.September;
            temp.October = p.October;
            temp.November = p.November;
            temp.December = p.December;

            return temp;
        }

        public Account.DAO.PackageDateLimit ConvertToDAO()
        {
            Account.DAO.PackageDateLimit dao = new PackageDateLimit();

            dao.Year = this.Year;
            dao.Quarter1 = this.Quarter1;
            dao.Quarter2 = this.Quarter2;
            dao.Quarter3 = this.Quarter3;
            dao.Quarter4 = this.Quarter4;

            dao.YearLimit = this.YearLimit;

            dao.January = this.January;
            dao.February = this.February;
            dao.March = this.March;
            dao.April = this.April;
            dao.May = this.May;
            dao.June = this.June;
            dao.July = this.July;
            dao.August = this.August;
            dao.September = this.September;
            dao.October = this.October;
            dao.November = this.November;
            dao.December = this.December;

            return dao;
        }
    }
}