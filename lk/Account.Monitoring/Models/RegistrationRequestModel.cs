﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Account.DAO;
namespace Monitoring.Models
{
    public class RegistrationRequestModel
    {
        [Required]
        [Display(Name = "Регион")]
        public string RegionUser { get; set; }

        [Required]
        public int ID { get; set; }

        [Required]
        [RegularExpression(@"^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-zA-Z0-9]{1}[a-zA-Z0-9\-]{0,62}[a-zA-Z0-9]{1})|[a-zA-Z])\.)+[a-zA-Z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$", ErrorMessage = "Неправильный e-mail адрес")]
        [Display(Name = "Логин")]
        public string Login { get; set; }       

        [Required]
        [Display(Name = "ФИО контактного лица")]
        public string FIO { get; set; }      

        [Required]
        [StringLength(32, ErrorMessage = "Пароль слишком короткий (минимум {2} символов)", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Повторный ввод пароля")]
        [Compare("Password", ErrorMessage = "Введеные пароли не совпадают")]
        public string PasswordConfirmation { get; set; }

        [Required]
        [Display(Name = "Роль")]
        public string Role { get; set; }

        public RegistrationRequestModel()
        {
           
        }

        public RegistrationRequestModel(UserMonitoring acc)
        {
            From(acc);
        }

        public void From(object obj)
        {
            var acc = (UserMonitoring)obj;

            Login = acc.Login;
            Password = acc.Password;
            RegionUser = acc.RegionUser;

            if (acc.IsAdmin)
                Role = "Администратор";
            else Role = "Пользователь";       
            FIO = acc.FIO;
            ID = acc.ID;          

        }
    }

    
}