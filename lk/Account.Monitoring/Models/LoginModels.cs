﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace Monitoring.Models
{
    public class LoginModels
    {
        [Required(ErrorMessage="- обязательное поле")]
        [Display(Name = "Пользователь")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "- обязательное поле")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }       
    }   
    
}
