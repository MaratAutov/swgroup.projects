﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Net;


namespace DelayedUploadProcessingService
{
    public partial class Service1 : ServiceBase
    {
        private static Timer timer;
        private static string WatchDir = "C:\\DelayedUpload\\";

        public Service1()
        {
            InitializeComponent();

            timer = new Timer();
            timer.AutoReset = false;
            timer.Elapsed += new ElapsedEventHandler(DoWork);
        }

        protected override void OnStart(string[] args)
        {
            timer.Interval = 1000;
            timer.Start();
        }

        protected override void OnStop()
        {
            timer.Stop();
        }

        private void DoWork(object sender, ElapsedEventArgs e)
        {
            try {
                foreach (var file in Directory.EnumerateFiles(WatchDir, "*.info")) {
                    var info = File.ReadAllText(file).Split(
                        new string[] {Environment.NewLine},
                        StringSplitOptions.RemoveEmptyEntries
                    );
                    string fileName = info[0];
                    string name = info[1];
                    int ownerId = int.Parse(info[2]);
                    DateTime uploaded = Convert.ToDateTime(info[3]);
                    string dataFile = file.Replace(".info", ".data");
                    byte[] data = File.ReadAllBytes(dataFile);

                    using (var cl = new SPOService.SPOServiceSoapClient()) {
                        cl.UploadPackage(
                            fileName,
                            name,
                            data,
                            ownerId,
                            uploaded,
                            false,
                            null
                        );
                    }

                    File.Delete(file);
                    File.Delete(dataFile);
                }
            } catch (Exception) {
            } finally {
                timer.Interval = 5000;
                timer.Start();
            }
        }
    }
}
