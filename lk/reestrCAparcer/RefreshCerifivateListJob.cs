﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using NLog;
using System.Net;
using HtmlAgilityPack;
using System.IO;
using System.Threading;
using System.Security.Cryptography.X509Certificates;

namespace reestrCAparcer
{
    class RefreshCerifivateListJob : IJob
    {
        static string _siteUriPattern;
        public static string SiteUriPattern 
        {
            get
            {
                return _siteUriPattern;
            }
            set
            {
                _siteUriPattern = value;
                PageLink = _siteUriPattern.Substring(0, value.IndexOf("/", 8));
            }
        }
        public static string PageLink { get; set; }
        public static string counterFrom { get; set; }
        public static string counterTo { get; set; }
        public static string errorPattern { get; set; }
        public static int PageDownloadTimeout { get; set; }
        public static int CertDownloadTimeout { get; set; }
        public static string proxyUri { get; set; }
        static Logger logger = LogManager.GetLogger("RefreshCerifivateListJob");

        public void Execute(IJobExecutionContext context)
        {
            logger.Info("Getting certs");
            try
            {
                Console.WriteLine("Start at {0}", DateTime.UtcNow);
                List<CertificatePOCO> certs = GetCertificates();
                LoadCertificates(certs);
                ImportCertsInStore();
            }
            catch (Exception ex)
            {
                logger.Info(ex);
            }
            Console.WriteLine("End at {0}", DateTime.UtcNow);
        }

        static List<CertificatePOCO> GetCertificates()
        {
            int start = Convert.ToInt32(counterFrom);
            int end = Convert.ToInt32(counterTo);
            int pages = 0;
            List<CertificatePOCO> list = new List<CertificatePOCO>();
            for (int i = start; i <= end; i++)
            {
                string link = SiteUriPattern.Replace("*", i.ToString());
                HtmlDocument html = new HtmlDocument();
                WebProxy wp = new WebProxy(proxyUri);
                WebClient client = new WebClient();
                client.Proxy = wp;
                try
                {
                    client.Encoding = Encoding.UTF8;
                    string content = client.DownloadString(link);
                    if (content.Contains("Нет элементов для отображения"))
                        break;
                    html.LoadHtml(content);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
                pages++;
                var rows = html.DocumentNode.SelectNodes("//table[@class=\"gridTable\"]/tr");
                if (rows == null)
                    continue;
                for (int r = 1; r < rows.Count; r++)
                {
                    CertificatePOCO cert = new CertificatePOCO();
                    cert.UC = rows[r].ChildNodes[3].InnerText;
                    cert.Link = rows[r].ChildNodes[1].ChildNodes[0].Attributes["href"].Value;
                    cert.Number = rows[r].ChildNodes[7].InnerText;
                    list.Add(cert);
                }
                Thread.Sleep(PageDownloadTimeout);
            }
            logger.Info("Found {0} links in {1} pages", list.Count, pages);
            return list;
        }

        static void LoadCertificates(List<CertificatePOCO> certs)
        {
            WebProxy proxy = new WebProxy(proxyUri);
            WebClient downloader = new WebClient();
            downloader.Proxy = proxy;
            int count = 0;
            foreach (var cert in certs)
            {
                string sourceLink = PageLink + cert.Link;
                string destinationPath = ".\\certs\\" + cert.Number + ".cer";
                if (!Directory.Exists("certs"))
                    Directory.CreateDirectory("certs");
                if (!File.Exists(destinationPath))
                {
                    try
                    {
                        logger.Info("Downloading {0} to {1}", sourceLink, destinationPath);
                        downloader.DownloadFile(sourceLink, destinationPath);
                        count++;
                        Thread.Sleep(CertDownloadTimeout);
                    }catch(Exception ex)
                    {
                        logger.Error(ex);
                    }
                }
            }
            logger.Info("Downloaded {0} certificates", count);
        }

        static void ImportCertsInStore()
        {
            string[] fileNames = Directory.GetFiles(".\\certs", "*.cer");
            logger.Info("Found {0} file *.cer", fileNames.Length);
            int imported = 0;
            foreach (string fileName in fileNames)
            {
                X509Certificate2 certToFind = new X509Certificate2();
                byte[] rawData = File.ReadAllBytes(fileName);
                certToFind.Import(rawData);

                // Открываем store 
                X509Store store = new X509Store(StoreName.Root, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadWrite);
                X509Certificate2 ret = null;
                                
                //ищем наш сертификат в хранилище
                foreach (X509Certificate2 cer in store.Certificates)
                {
                    if (cer.Equals(certToFind))
                    {
                        ret = cer;
                        break;
                    }
                }
                // сертификата нет в хранилище
                if (ret == null)
                {
                    logger.Info("Importing in certificates store {0}", fileName);
                    store.Add(certToFind);
                    imported++;
                }
            }
            logger.Info("Imported {0} certificates", imported);
        }
    }
}
