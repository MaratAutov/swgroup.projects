﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;

namespace reestrCAparcer
{
    class Program
    {
        static void Main(string[] args)
        {
            RefreshCerifivateListJob.SiteUriPattern = ConfigurationManager.AppSettings["SiteUriPattern"];
            RefreshCerifivateListJob.counterFrom = ConfigurationManager.AppSettings["CounterFrom"];
            RefreshCerifivateListJob.counterTo = ConfigurationManager.AppSettings["CounterTo"];
            RefreshCerifivateListJob.errorPattern = ConfigurationManager.AppSettings["ErrorPattern"];
            RefreshCerifivateListJob.PageDownloadTimeout = Int32.Parse(ConfigurationManager.AppSettings["PageDownloadTimeout"]);
            RefreshCerifivateListJob.CertDownloadTimeout = Int32.Parse(ConfigurationManager.AppSettings["CertDownloadTimeout"]);
            RefreshCerifivateListJob.proxyUri = ConfigurationManager.AppSettings["proxyUri"];
            ISchedulerFactory schedFact = new StdSchedulerFactory();
            IScheduler sched = schedFact.GetScheduler();
            sched.Start();
            JobDetailImpl jobDetail = new JobDetailImpl("FSFRSync", null, typeof(RefreshCerifivateListJob));
            //SimpleTriggerImpl trigger = new SimpleTriggerImpl("Sync", "FSFR", -1, TimeSpan.FromSeconds(Convert.ToInt32(ConfigurationManager.AppSettings["SyncTimeout"])));
            CronTriggerImpl trigger = new CronTriggerImpl("FSFRSync", null, ConfigurationManager.AppSettings["CronExpression"]);
            sched.ScheduleJob(jobDetail, trigger);
        }
    }
}
