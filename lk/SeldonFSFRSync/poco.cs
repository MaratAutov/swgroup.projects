﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinesLogic
{
    public class Package
    {
        public string Name { get; set; }
        public byte[] Data { get; set; }
    }

    public class PostNotification
    {
        public string Name { get; set; }
        public string InsertedID { get; set; }
        public string Error { get; set; }
        public int ErrorCode { get; set; }
    }
}
