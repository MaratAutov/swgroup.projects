﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using Quartz.Impl;
using System.Threading;
using Account.DAO;
using NLog;
using System.Net.Http;
using Account.Tools;
using System.Net;
using System.Security.Cryptography;
using ICSharpCode.SharpZipLib.Zip;

namespace SeldonFSFRSync
{
    public class SeldonFSFRSyncJob : IJob
    {
        static Logger logger = LogManager.GetLogger("SeldonFSFRSyncJob");
        public SeldonFSFRSyncJob() { }
        public static string ServiceUri;
        public static string Login;
        public static string Password;

        public void Execute(IJobExecutionContext context)
        {
            logger.Info("Getting packages for sending");
            Console.WriteLine("Start at {0}", DateTime.UtcNow);
            List<Account.DAO.Package> packages = PackageService.GetPackageForSeldonSync();
            logger.Info(packages.Count == 0 ? "Not found packages for sending" : String.Format("Finded {0} packages", packages.Count));
            foreach (var p in packages)
            {
                bool sended = Send(p);
                Console.WriteLine("{0}: {1} : {2}", DateTime.UtcNow, sended ? "Sended" : "Error", p.ID);
            }
            Console.WriteLine("End at {0}", DateTime.UtcNow);
        }
        
        bool Send(Account.DAO.Package package)
        {
            string ErrorMsg = String.Empty;
            int ErrorCode = 0;
            try
            {
                logger.Info("Package ID: {0}", package.ID);
                HttpClientHandler handler = new HttpClientHandler();
                handler.Credentials = new NetworkCredential(Login, Password);
                HttpClient client = new HttpClient(handler);
                client.BaseAddress = new Uri(ServiceUri);
                BusinesLogic.Package newPackage = new BusinesLogic.Package() { Name = package.Name };
                newPackage.Data = GetData(package);
                string xtdd = Encoding.UTF8.GetString(newPackage.Data, 0, newPackage.Data.Length > 100 ? 100 : newPackage.Data.Length);
                logger.Info("Sending: {0}", xtdd);
                HttpResponseMessage response = client.PostAsXmlAsync("api/package", newPackage).Result;
                if (response.IsSuccessStatusCode)
                {
                    logger.Info("Sended", xtdd);
                    var res = response.Content.ReadAsAsync<BusinesLogic.PostNotification>().Result;
                    if (res == null || String.IsNullOrEmpty(res.InsertedID))
                    {
                        if (res != null)
                        {
                            ErrorMsg = res.Error;
                            ErrorCode = res.ErrorCode;
                        }
                        throw new Exception("Service not respond - OK");
                    }
                    logger.Info("Inserted ID {0}", res.InsertedID);
                    package.SeldonFSFRStatus = (int)SeldonFSFRSyncStatus.sended;
                    PackageService.Update(package);
                    logger.Info("Package fsfr status updatet to {0}", (int)SeldonFSFRSyncStatus.sended);
                    return true;
                }
                else
                    throw new Exception(String.Format("Not successed response status: {0}", response.StatusCode));
            }
            catch(ZipException ex)
            {
                package.SeldonFSFRStatus = (int)SeldonFSFRSyncStatus.fatalError;
                package.SeldonFSFRError = ex.ToString();
                LogError(package);
            }
            catch (SignatureNotFoundException ex)
            {
                package.SeldonFSFRStatus = (int)SeldonFSFRSyncStatus.fatalError;
                package.SeldonFSFRError = ex.ToString();
                LogError(package);
            }
            catch (Exception ex)
            {
                
                switch (ErrorCode)
                {
                    case 0:
                        package.SeldonFSFRError = ex.ToString();
                        package.SeldonFSFRStatus = (int)SeldonFSFRSyncStatus.error;
                        break;
                    case 1:
                        package.SeldonFSFRError = ErrorMsg;
                        package.SeldonFSFRStatus = (int)SeldonFSFRSyncStatus.error;
                        break;
                    case 3:
                        package.SeldonFSFRError = ErrorMsg;
                        package.SeldonFSFRStatus = (int)SeldonFSFRSyncStatus.fatalError;
                        break;
                    default:
                        package.SeldonFSFRError = ex.ToString();
                        package.SeldonFSFRStatus = (int)SeldonFSFRSyncStatus.error;
                        break;
                }

                LogError(package);
            }
            return false;
        }

        void LogError(Package package)
        {
            try
            {
                PackageService.Update(package);
            }
            catch { }
            logger.Error(package.SeldonFSFRError);
        }


        byte[] GetData(Account.DAO.Package p)
        {
            Blob blob = PackageService.GetBlob(p.BlobID);
            IReportPackage package;
            // из за ошибки в библиотеке orm lite массив может прийти кодированным в base64
            if (blob.Body[blob.Body.Length - 1] == (byte)'=')
            {
                string base64string = Encoding.UTF8.GetString(blob.Body);
                package = new ZipReportPackage(Convert.FromBase64String(base64string));
            }else
                package = new ZipReportPackage(blob.Body);
            return package.ReportBody;
        }
    }

    public enum SeldonFSFRSyncStatus
    {
        not_sended = 0,
        sended = 1,
        error = 2,
        fatalError = 3
    }
}
