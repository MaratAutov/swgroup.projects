﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using System.Configuration;

namespace SeldonFSFRSync
{
    class Program
    {
        static void Main(string[] args)
        {
            SeldonFSFRSyncJob.ServiceUri = ConfigurationManager.AppSettings["ServiceUri"];
            SeldonFSFRSyncJob.Login = ConfigurationManager.AppSettings["Login"];
            SeldonFSFRSyncJob.Password = ConfigurationManager.AppSettings["Password"];
            ISchedulerFactory schedFact = new StdSchedulerFactory();
            IScheduler sched = schedFact.GetScheduler();
            sched.Start();
            JobDetailImpl jobDetail = new JobDetailImpl("FSFRSync", null, typeof(SeldonFSFRSyncJob));
            SimpleTriggerImpl trigger = new SimpleTriggerImpl("Sync", "FSFR", -1, TimeSpan.FromSeconds(Convert.ToInt32(ConfigurationManager.AppSettings["SyncTimeout"])));
            sched.ScheduleJob(jobDetail, trigger);
            
        }
    }
}
