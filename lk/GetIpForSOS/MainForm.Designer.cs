﻿namespace GetIpForSOS
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFindAll = new DevExpress.XtraEditors.SimpleButton();
            this.meIPFromReestr = new DevExpress.XtraEditors.MemoEdit();
            this.meLog = new DevExpress.XtraEditors.MemoEdit();
            this.meSource = new DevExpress.XtraEditors.MemoEdit();
            this.meDestination = new DevExpress.XtraEditors.MemoEdit();
            this.btnDisinct = new DevExpress.XtraEditors.SimpleButton();
            this.btnFindInPackets = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.meIPFromReestr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meSource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDestination.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnFindAll
            // 
            this.btnFindAll.Location = new System.Drawing.Point(33, 13);
            this.btnFindAll.Name = "btnFindAll";
            this.btnFindAll.Size = new System.Drawing.Size(75, 23);
            this.btnFindAll.TabIndex = 0;
            this.btnFindAll.Text = "Найти все";
            this.btnFindAll.Click += new System.EventHandler(this.btnFindAll_Click);
            // 
            // meIPFromReestr
            // 
            this.meIPFromReestr.Location = new System.Drawing.Point(33, 54);
            this.meIPFromReestr.Name = "meIPFromReestr";
            this.meIPFromReestr.Size = new System.Drawing.Size(226, 289);
            this.meIPFromReestr.TabIndex = 1;
            // 
            // meLog
            // 
            this.meLog.Location = new System.Drawing.Point(33, 377);
            this.meLog.Name = "meLog";
            this.meLog.Size = new System.Drawing.Size(714, 96);
            this.meLog.TabIndex = 2;
            // 
            // meSource
            // 
            this.meSource.Location = new System.Drawing.Point(285, 54);
            this.meSource.Name = "meSource";
            this.meSource.Size = new System.Drawing.Size(226, 289);
            this.meSource.TabIndex = 3;
            // 
            // meDestination
            // 
            this.meDestination.Location = new System.Drawing.Point(539, 54);
            this.meDestination.Name = "meDestination";
            this.meDestination.Size = new System.Drawing.Size(226, 289);
            this.meDestination.TabIndex = 3;
            // 
            // btnDisinct
            // 
            this.btnDisinct.Location = new System.Drawing.Point(460, 13);
            this.btnDisinct.Name = "btnDisinct";
            this.btnDisinct.Size = new System.Drawing.Size(129, 23);
            this.btnDisinct.TabIndex = 0;
            this.btnDisinct.Text = "Убрать дубляж >";
            this.btnDisinct.Click += new System.EventHandler(this.btnDisinct_Click);
            // 
            // btnFindInPackets
            // 
            this.btnFindInPackets.Location = new System.Drawing.Point(175, 13);
            this.btnFindInPackets.Name = "btnFindInPackets";
            this.btnFindInPackets.Size = new System.Drawing.Size(103, 23);
            this.btnFindInPackets.TabIndex = 4;
            this.btnFindInPackets.Text = "Найти в пакетах";
            this.btnFindInPackets.Click += new System.EventHandler(this.btnFindInPackets_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 515);
            this.Controls.Add(this.btnFindInPackets);
            this.Controls.Add(this.meDestination);
            this.Controls.Add(this.meSource);
            this.Controls.Add(this.meLog);
            this.Controls.Add(this.meIPFromReestr);
            this.Controls.Add(this.btnDisinct);
            this.Controls.Add(this.btnFindAll);
            this.Name = "MainForm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.meIPFromReestr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meSource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDestination.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnFindAll;
        private DevExpress.XtraEditors.MemoEdit meIPFromReestr;
        private DevExpress.XtraEditors.MemoEdit meLog;
        private DevExpress.XtraEditors.MemoEdit meSource;
        private DevExpress.XtraEditors.MemoEdit meDestination;
        private DevExpress.XtraEditors.SimpleButton btnDisinct;
        private DevExpress.XtraEditors.SimpleButton btnFindInPackets;
    }
}

