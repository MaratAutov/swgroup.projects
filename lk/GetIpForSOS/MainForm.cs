﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using Account.Tools;
using System.Security.Cryptography.Pkcs;
using ICSharpCode.SharpZipLib.Zip;

namespace GetIpForSOS
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnFindAll_Click(object sender, EventArgs e)
        {
            meLog.Text = String.Empty;
            X509Store[] stores = { new X509Store(StoreName.Root,
                                        StoreLocation.CurrentUser), 
                                   new X509Store(StoreName.Root,
                                        StoreLocation.LocalMachine) };
            List<string> ips = new List<string>();
            List<string> urls = new List<string>();
            try
            {
                foreach (var store in stores)
                {
                    store.Open(OpenFlags.ReadOnly);
                    foreach (var cert in store.Certificates)
                    {
                        var url = GetCRLUrls(cert);
                        if (url != null)
                        {
                            foreach (var u in url)
                            {
                                if (!urls.Contains(u))
                                    urls.Add(u);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                meLog.Text += Environment.NewLine + (ex.ToString()) + Environment.NewLine;
            }
            ips = GetIPs(urls);
            meLog.Text += String.Join(Environment.NewLine, urls);
            meIPFromReestr.Text = String.Join(Environment.NewLine, ips);
        }

        private List<string> GetIPs(List<string> urls)
        {
            List<string> ips = new List<string>();
            foreach (var url in urls)
            {
                try
                {
                    IPAddress[] hostips = Dns.GetHostAddresses(url);
                    foreach (var ip in hostips)
                    {
                        string ipstr = ip.ToString();
                        if (!ips.Contains(ipstr))
                            ips.Add(ipstr);
                    }
                }
                catch(Exception ex)
                {
                    meLog.Text += Environment.NewLine + url + Environment.NewLine + (ex.ToString()) + Environment.NewLine;
                }
            }
            return ips;
        }

        string urlpattenr = @"(?:http|https):\/\/(.*?)\/";

        private string[] GetCRLUrls(X509Certificate2 cert)
        {
            foreach (var ext in cert.Extensions)
            {
                
                if (ext.Oid.FriendlyName == "Точки распространения списков отзыва (CRL)")
                {
                    string rawstr = Encoding.UTF8.GetString(ext.RawData);
                    meLog.Text += rawstr + Environment.NewLine;
                    Regex regex = new Regex(urlpattenr);
                    var matches = regex.Matches(rawstr);
                    string[] result = new string[matches.Count];
                    for (int i = 0; i < matches.Count; i++)
                    {
                        result[i] = matches[i].Groups[1].Value;
                    }
                    return result;
                }
            }
            return null;
        }

        private void btnDisinct_Click(object sender, EventArgs e)
        {
            meDestination.Text = String.Join(Environment.NewLine, meSource.Lines.Distinct<string>());
        }

        private void btnFindInPackets_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string[] files = Directory.GetFiles(fbd.SelectedPath, "document_sign.dat", SearchOption.AllDirectories);
                List<string> ips = new List<string>();
                List<string> urls = new List<string>();
                foreach (string file in files)
                {
                    try
                    {
                        //byte[] data = GetSignDataFromZip(file);
                        //if (data == null)
                        //    continue;
                        SignedCms signedCms = new SignedCms();
                        signedCms.Decode(File.ReadAllBytes(file));
                    
                        foreach (var cert in signedCms.Certificates)
                        {
                            var url = GetCRLUrls(cert);
                            if (url != null)
                            {
                                foreach (var u in url)
                                {
                                    if (!urls.Contains(u))
                                        urls.Add(u);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        meLog.Text += Environment.NewLine + (ex.ToString()) + Environment.NewLine;
                    }
                }
                ips = GetIPs(urls);
                meLog.Text += String.Join(Environment.NewLine, urls);
                meIPFromReestr.Text = String.Join(Environment.NewLine, ips);
            }
        }

        byte[] GetSignDataFromZip(string fileName)
        {
            byte[] data = File.ReadAllBytes(fileName);
            MemoryStream stream = new MemoryStream(data);

            byte[] _reportData = new byte[stream.Length];
            stream.Read(_reportData, 0, _reportData.Length);
            stream.Position = 0;

            using (ZipFile zip = new ZipFile(stream))
            {
                foreach (ZipEntry e in zip)
                {
                    if (!e.IsFile)
                        continue;

                    
                    using (Stream s = zip.GetInputStream(e))
                    using (BinaryReader br = new BinaryReader(s))
                        return br.ReadBytes((int)e.Size);
                        
                    
                }
                
            }
            return null;
        }
    }
}
