﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Security.Cryptography;

using ServiceStack.OrmLite;
using System.Web.Security;
//using ServiceStack.OrmLite.SqlServer;

namespace Account.DAO
{
    public static class ORM
    {
        private static OrmLiteConnectionFactory _factory { get; set; }
        private static bool IsInited;

        public static IDbConnection db
        {
            get
            {
                if (!IsInited || _factory == null)
                    Initialize();

                 return _factory.OpenDbConnection();
            }
        }

        private static void Initialize()
        {
            IsInited = true;
            string ConnectionString = string.Empty;
            
                ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection2"].ToString();
                       

            if (string.IsNullOrEmpty(ConnectionString))
                throw new ConfigurationErrorsException("ORM Connection string was not set");
            
            _factory = new OrmLiteConnectionFactory(
                ConnectionString,
                //SqlServerDialect.Provider
                PostgreSqlDialect.Provider
            );
           
            try
            {
                using (var _connection = _factory.OpenDbConnection())
                {
                    foreach (var i in DataSetup.Data)
                    {
                        string tableName = DataSetup.GetAlias(i.Key);
                        if (!_connection.TableExists(tableName))
                        {
                            _connection.CreateTable(false, i.Key);
                            if (i.Value != null)
                                _connection.ExecuteSql(i.Value);
                        }
                    }
                    _connection.ExecuteSql(DataSetup.SqlData);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public static string GetHash(byte[] data)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] hash = md5Hash.ComputeHash(data);
                return BitConverter.ToString(hash).Replace("-", "");
            }
        }

        /// <summary>
        /// Соль для паролей. Соль не менять: все пароли станут несъедобными!
        /// </summary>
        private const string Salt = "SJDa#of0ln";

        /// <summary>
        /// Волшебная строка из MSDN
        /// </summary>
        private const string HashMethod = "SHA1";

        public static string GetHash(string password)
        {
            string salted = password + Salt;
            return FormsAuthentication.HashPasswordForStoringInConfigFile(salted, HashMethod);
        }
    }
}
