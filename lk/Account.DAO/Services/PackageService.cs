﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Runtime.InteropServices;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;

namespace Account.DAO
{
    public static class PackageService
    {
        public static List<Package> GetAll()
        {
            using (var db = ORM.db)
            {
                return db.Select<Package>();
            }
        }

        public static IEnumerable<PackageFullInfo> GetAll(
            int ownerID,
            string year,
            DateTime uploadTime,
            string name,
            string externalID,
            DateTime createDate,
            string incomingID,
            DateTime regDate,
            string orderField,
            string orderType,
            bool showImported)
        {
            DateTime uploadDate = uploadTime.Date;
            DateTime yearDate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(year))
                yearDate = DateTime.Parse(year + "-01-01");

            using (var db = ORM.db)
            {
                return db.Select<PackageFullInfo>(
                    m => m.Where(
                        p => (ownerID == 0 || p.OwnerID == ownerID) &&
                                p.Name.Contains(name ?? "") &&
                                (p.IncomingNumber == null || p.IncomingNumber.Contains(externalID ?? "")) &&
                                (p.SedNumber == null || p.SedNumber.Contains(incomingID ?? "")) &&
                                (yearDate == DateTime.MinValue || (p.UploadTime >= yearDate && p.UploadTime < yearDate.AddYears(1))) &&
                                (uploadTime == DateTime.MinValue || (p.UploadTime >= uploadDate && p.UploadTime < uploadDate.AddDays(1))) &&
                                (createDate == DateTime.MinValue || (p.IncomingDate >= createDate && p.IncomingDate < createDate.AddDays(1))) &&
                                (regDate == DateTime.MinValue || (p.SedDate >= regDate && p.SedDate < regDate.AddDays(1))) &&
                                ((showImported == true && p.IsImported == true) || p.IsImported == null)
                    ).OrderBy(string.Format("ORDER BY \"{0}\" {1}", orderField, orderType))
                );
            }
        }

        public static void Create(Package package, ReportInfo report, IList<MailMessage> mails, Status initial, Status current)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                db.Insert(initial);
                initial.ID = (int)db.GetLastInsertId();
                db.Insert(current);
                current.ID = (int)db.GetLastInsertId();
                package.InitialStatusID = initial.ID;
                package.CurrentStatusID = current.ID;
                db.Insert(report);
                report.ID = (int)db.GetLastInsertId();
                package.ReportID = report.ID;
                db.Insert(package);
                package.ID = (int)db.GetLastInsertId();
                
                foreach (MailMessage mail in mails)
                {
                    mail.PackageID = package.ID;
                    db.Insert(mail);
                }
                trans.Commit();
            }
        }

        public static void Delete(Package package)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                db.Delete<Package>(package);
            }
        }

        public static Package Create(
            string name,
            byte[] data,
            int ownerId,
            DateTime uploadTime,
            bool isRo,
            string sedNum=null,
            DateTime? sedDate=null,
            DateTime? changeTime = null)
        {
            if (changeTime == null)
            {
                changeTime = DateTime.Now;
            }

            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                string hash = ORM.GetHash(data);
                List<Blob> blobs = GetBlobsByHash(hash, db);
                Blob blob = null;
                
                foreach (Blob b in blobs)
                {
                    //if (CompareArrays(Convert.FromBase64String(Encoding.UTF8.GetString(b.Body)), data))
                    if (CompareArrays(b.Body, data))
                    {
                        blob = b;
                        Package p = GetPackageByBlobId(b.ID, name, db);
                        if (p != null)
                        {
                            p.Existing = true;
                            return p;
                        }
                    }
                }
                
                
                if (blob == null)
                {
                    blob = new Blob()
                    {
                        Body = data,
                        Hash = hash
                    };
                    db.Insert(blob);
                    blob.ID = (int)db.GetLastInsertId();
                }

                ReportInfo rep = new ReportInfo()
                {
                    IsRoReport = isRo,
                    SedNumber = sedNum,
                    SedDate = sedDate
                };
                db.Insert(rep);
                rep.ID = (int)db.GetLastInsertId();

                Package package = new Package()
                {
                    Name = name,
                    OwnerID = ownerId,
                    ReportID = rep.ID,
                    BlobID = blob.ID,
                    UploadTime = uploadTime,
                    LastChangeTime = (DateTime)changeTime,
                    Existing = false
                };
                db.Insert(package);
                package.ID = (int)db.GetLastInsertId();
                package.Info = rep;

                string statusInfo = StatusService.GetStatusString(
                    (int)PrimaryStatus.Accepted, false
                );

                Status status = new Status()
                {
                    PrimaryCode = (int)PrimaryStatus.Accepted,
                    PackageID = package.ID,
                    AssignmentTime = DateTime.Now,
                    Info = statusInfo,
                    NotificationBlobID = null
                };
                db.Insert(status);

                package.CurrentStatusID =
                package.InitialStatusID = (int)db.GetLastInsertId();

                db.Update(package);

                trans.Commit();

                return package;
            }
        }

        public static Package GetPackage(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<Package>(
                    p => p.ID == id
                ).SingleOrDefault();
            }
        }

        public static PackageFullInfo GetFullInfo(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<PackageFullInfo>(
                    p => p.ID == id
                ).SingleOrDefault();

                
            }
        }

        public static bool IssetDublicateOutgoing(int packageID, string INN, string OGRN, string number, string year)
        {
            using (var db = ORM.db)
            {
                int count = db.Scalar<int>(@"select count(*) from ""Package"" p " +
                    @" inner join ""ReportInfo"" r on p.""ReportID""=r.""ID"" " +
                    @" where p.""ID"" <> {0} and r.""INN"" = {1} and r.""OGRN"" = {2} and r.""IncomingNumber"" = {3} and extract(year from p.""UploadTime"") = {4} ", 
                    packageID, INN, OGRN, number, year
                );
                return count != 0; 
            }
        }

        public static void Update(Package p)
        {
            using (var db = ORM.db)
            {
                db.Update(p);
            }
        }

        public static void UpdateReportInfo(ReportInfo r)
        {
            using (var db = ORM.db)
            {
                db.Update(r);
            }
        }

        private static string ValidateStringCardField(string field, int max_length)
        {
            return field == null ? "" : (field.Length > max_length ? field.Substring(0, max_length) : field);
        }

        public static void UpdateReportInfo(int packageID, Dictionary<string, string> cardInfo)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                var package = db.Select<Package>(
                    p => p.ID == packageID
                );
                var rep = db.Select<ReportInfo>().Join(
                    package, r => r.ID, p => p.ReportID, (r, p) => r
                ).SingleOrDefault();
            
                rep.IncomingNumber = cardInfo["RegNumber"] ?? "";
                rep.IncomingDate = cardInfo["RegDate"] == null ? DateTime.MinValue : Convert.ToDateTime(cardInfo["RegDate"]);
                rep.Address = ValidateStringCardField(cardInfo["Address"], 255);
                rep.Subject = ValidateStringCardField(cardInfo["Subject"], 2048);
                rep.FirstName = ValidateStringCardField(cardInfo["SignerFirstName"], 255);
                rep.LastName = ValidateStringCardField(cardInfo["SignerLastName"], 255);
                rep.MiddleName = ValidateStringCardField(cardInfo["SignerMiddleName"], 255);
                rep.OrgShortName = ValidateStringCardField(cardInfo["orgShortName"], 255);
                rep.OrgFullName = ValidateStringCardField(cardInfo["orgFullName"], 255);
                rep.OrgTypeName = cardInfo["orgTypeName"];
                if (cardInfo["orgTypeCode"] != null)
                    rep.OrgTypeCode = int.Parse(cardInfo["orgTypeCode"]);
                rep.DocTypeName = cardInfo["docTypeName"];
                if (cardInfo["docTypeCode"] != null)
                    rep.DocTypeCode = int.Parse(cardInfo["docTypeCode"]);
                rep.OGRN = cardInfo["OGRN"];
                rep.INN = cardInfo["INN"];

                var reportDate = cardInfo["reportDate"];
                if (string.IsNullOrEmpty(reportDate) && cardInfo.ContainsKey("reportDateAlt"))
                    reportDate = cardInfo["reportDateAlt"];
                if (string.IsNullOrEmpty(reportDate) && cardInfo.ContainsKey("reportDateAlt2"))
                    reportDate = cardInfo["reportDateAlt2"];
                if (string.IsNullOrEmpty(reportDate) && cardInfo.ContainsKey("reportDateAlt3"))
                    reportDate = cardInfo["reportDateAlt3"];
                if (string.IsNullOrEmpty(reportDate) && cardInfo.ContainsKey("reportDateAlt4"))
                    reportDate = cardInfo["reportDateAlt4"];

                if (!string.IsNullOrEmpty(reportDate))
                    rep.Date = DateTime.Parse(reportDate);

                if (string.IsNullOrEmpty(cardInfo["reportMonth"]) && cardInfo.ContainsKey("reportMonthAlt"))
                    rep.Month = cardInfo["reportMonthAlt"];
                else
                    rep.Month = cardInfo["reportMonth"];

                if (string.IsNullOrEmpty(cardInfo["reportQuartal"]) && cardInfo.ContainsKey("reportQuartalAlt"))
                    rep.Quartal = cardInfo["reportQuartalAlt"];
                else
                    rep.Quartal = cardInfo["reportQuartal"];

                if (string.IsNullOrEmpty(cardInfo["reportYear"]) && cardInfo.ContainsKey("reportYearAlt"))
                    rep.Year = cardInfo["reportYearAlt"];
                else
                    rep.Year = cardInfo["reportYear"];

                db.Update(rep);
                trans.Commit();
            }
        }

        public static void UpdateMailMessage(int packageID, string Url, string Subject, string MailFrom = null,
            string MailTo = null, string info = null)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                MailMessage mm = new MailMessage();
                mm.PackageID = packageID;
                mm.Url = Url;
                mm.Subject = Subject;
                mm.MailFrom = MailFrom;
                mm.MailTo = MailTo;
                mm.Info = info;
                mm.CreateAt = DateTime.Now;
                db.Insert(mm);
                trans.Commit();
            }
        }

        public static ReportInfo GetReportInfo(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<ReportInfo>(
                    p => p.ID == id
                ).SingleOrDefault();
            }
        }

        public static Blob GetBlob(int id)
        {
            using (var db = ORM.db)
            {
                Blob blob = db.Select<Blob>(
                    p => p.ID == id
                ).SingleOrDefault();
                if (blob == null)
                {
                    return null;
                }
                else if ((char)blob.Body.Last() == '=')
                {
                    blob.Body = Convert.FromBase64String(Encoding.UTF8.GetString(blob.Body));
                }
                return blob;
            }  
             
        }

        public static Blob GetBlobByPackageId(int id)
        {
            using (var db = ORM.db)
            {
                Package package =  db.Select<Package>(
                    p => p.ID == id
                ).SingleOrDefault();

                Blob blob = db.Select<Blob>(
                    p => p.ID == package.BlobID
                ).SingleOrDefault();

                if ((char)blob.Body.Last() == '=')
                    blob.Body = Convert.FromBase64String(Encoding.UTF8.GetString(blob.Body));
                return blob;
            }
        }

       

        public static List<PackageFullInfo> GetStats(
            DateTime from,
            DateTime to,
            int id,
            string name,
            string owner,
            string sign,
            int currStatus,
            int initStatus,
            string inn,
            string ogrn,
            string model,
            string type,
            string orgType,
            string docType,
            string year,
            string quartal,
            string month,
            DateTime date,
            string orderField)
        {
            if (!String.IsNullOrEmpty(docType))
            {
                var docmap = Account.DAO.SED.SEDMappingService.GetDocMapping();
                var dm = docmap.Where(p => p.Name == docType).FirstOrDefault();
                if (dm != null)
                {
                    orgType = null;
                    type = dm.Type;
                }
            }
            if (orderField == null)
            {
                orderField = "UploadTime";
            }
            using (var db = ORM.db)
            {
                return db.Select<PackageFullInfo>(
                    m => m.Where(p =>(id == 0 || p.ID == id) &&
                    ((name == null && p.Name == null) || p.Name.Contains(name ?? "")) &&
                    ((owner == null && p.Owner == null) || p.Owner.Contains(owner ?? "")) &&
                    (sign == null || p.SignatureType == sign) &&
                    (currStatus == -1 || p.CurrentStatusCode == currStatus) &&
                    (initStatus == -1 || p.InitialStatusCode == initStatus) &&
                    ((p.INN == null && inn == null) || (p.INN != null && p.INN.StartsWith(inn ?? ""))) &&
                    ((p.OGRN == null && ogrn == null) || (p.OGRN != null && p.OGRN.StartsWith(ogrn ?? ""))) &&
                    ((model == null && p.Model == null) || (p.Model != null && p.Model.Contains(model ?? ""))) &&
                    ((type == null && p.Type == null) || (p.Type != null && p.Type.Contains(type ?? ""))) &&
                    ((orgType == null && p.OrgTypeName == null) || (p.OrgTypeName != null && p.OrgTypeName.Contains(orgType ?? ""))) &&
                    ((year == null && p.Year == null) || (p.Year != null && p.Year.Contains(year ?? ""))) &&
                    ((quartal == null && p.Quartal == null) || (p.Quartal != null && p.Quartal.Contains(quartal ?? ""))) &&
                    ((month == null && p.Month == null) || (p.Month != null && p.Month.Contains(month ?? ""))) &&
                    (date == DateTime.MinValue || (p.Date == date.Date)) &&
                    (p.UploadTime >= from) &&
                    (p.UploadTime < to.AddDays(1))).OrderBy(string.Format("ORDER BY \"{0}\"", orderField))
                );
            }
        }

        public static List<Package> GetPackageForSeldonSync()
        {
            using (var db = ORM.db)
            {
                try
                {
                    int takeCount = 5;
                    int skipCount = 0;
                    List<Package> list = new List<Package>();
                    int count;
                    do
                    {
                        list = db.Select<Package>(p => p.SeldonFSFRStatus == null || p.SeldonFSFRStatus == 0 || p.SeldonFSFRStatus == 2).Skip(skipCount).Take(takeCount).ToList();
                        if (list.Count == 0)
                            break;
                        count = list.Where(p1 => p1.SeldonFSFRStatus == 0 || p1.SeldonFSFRStatus == null).Count();
                        skipCount += takeCount;
                    }
                    while (count == 0);
                    return list;
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return null;
                }
            }
        }

        public static PackageFilesInfo GetPackageFilesInfo(int packageid)
        {
            using (var db = ORM.db)
            {
                return db.Select<PackageFilesInfo>("SELECT p.\"ID\", ri.\"DocumentUrl\", ri.\"NotificationUrl\", ri.\"NotificationSignUrl\", ri.\"ProcessedDocumentUrl\" " +
                        "FROM \"Package\" p " +
                        "INNER JOIN \"ReportInfo\" ri ON p.\"ReportID\"=ri.\"ID\" " +
                        String.Format("WHERE p.\"ID\" = {0}", packageid)).FirstOrDefault();
            }
        }
                
        private static Package GetPackageByBlobId(int blobId, IDbConnection db)
        {
            return db.Select<Package>(
                p => p.BlobID == blobId
            ).SingleOrDefault();
        }

        private static Package GetPackageByBlobId(int blobId, string name, IDbConnection db)
        {
            return db.Select<Package>(
                p => p.BlobID == blobId && p.Name == name
            ).SingleOrDefault();
        }

        private static List<Blob> _GetBlobsByHash(string hash, IDbConnection db)
        {
            return db.Select<Blob>(
                b => b.Hash == hash
            );
        }

        private static List<Blob> GetBlobsByHash(string hash, IDbConnection db=null)
        {
            if (db == null)
            using (db = ORM.db)
            {
                return _GetBlobsByHash(hash, db);                
            }
            else
                return _GetBlobsByHash(hash, db);
        }

        private static bool CompareArrays(byte[] a, byte[] b)
        {
            if (a == null || b == null || a.Length != b.Length)
                return false;
            else
                return memcmp(a, b, a.LongLength) == 0;
        }

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int memcmp(byte[] b1, byte[] b2, long count);


    }
}
