﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;
using Account.DAO.Models;


namespace Account.DAO.Services
{
    public static class PackageHistoryService
    {
        public static HistoryInfo GetHistory_Year(PackageFullInfo pkgInfo)
        {
            using (var db = ORM.db)
            {
                List<PackageFullInfo> pkgs = db.Select<PackageFullInfo>(
                    p => p.INN == pkgInfo.INN && p.Year == pkgInfo.Year && p.DocTypeCode == pkgInfo.DocTypeCode)
                    .OrderByDescending(p => p.UploadTime).ToList();
                return new HistoryInfo(pkgInfo, pkgs);
            }
            
        }

        public static HistoryInfo GetHistory_Quarter(PackageFullInfo pkgInfo)
        {
            using (var db = ORM.db)
            {
                List<PackageFullInfo> pkgs = db.Select<PackageFullInfo>(
                    p => p.INN == pkgInfo.INN && p.Year == pkgInfo.Year && p.CurrentStatusCode != null &&
                    p.Quartal == pkgInfo.Quartal && p.DocTypeCode == pkgInfo.DocTypeCode).OrderByDescending(p => p.UploadTime).ToList();
                return new HistoryInfo(pkgInfo, pkgs);
            }
        }

        public static HistoryInfo GetHistory_Month(PackageFullInfo pkgInfo)
        {
            using (var db = ORM.db)
            {
                List<PackageFullInfo> pkgs = db.Select<PackageFullInfo>(
                    p => p.INN == pkgInfo.INN && p.Year == pkgInfo.Year &&
                    p.DocTypeCode == pkgInfo.DocTypeCode && p.Month == pkgInfo.Month).OrderByDescending(p => p.UploadTime).ToList();
                return new HistoryInfo(pkgInfo, pkgs);
            }
        }

        public static HistoryInfo GetHistory_Date(PackageFullInfo pkgInfo)
        {
            
            using (var db = ORM.db)
            {
                List<PackageFullInfo> pkgs = db.Select<PackageFullInfo>(
                    p => p.INN == pkgInfo.INN && p.Date == pkgInfo.Date &&
                    p.DocTypeCode == pkgInfo.DocTypeCode).OrderByDescending(p => p.UploadTime).ToList();
                return new HistoryInfo(pkgInfo, pkgs);
            }
        }
        
        public static List<OrganizationHistoryInfo> GetOrganizationList(DateTime from, DateTime to)
        {
            using (var db = ORM.db)
            {
                return db.Select<OrganizationHistoryInfo>(@"select distinct ri.""Year"", ri.""Quartal"", ri.""Month"", ri.""Date"", ri.""DocTypeCode"",
	                    ri.""Type"", ri.""INN"", (""IssetPackageHistory""(p.""ID"")::character varying(10)[])[1] as count, 
                        (""IssetPackageHistory""(p.""ID"")::character varying(10)[])[2] as report_type
                    from ""Package"" p
	                    inner join ""ReportInfo"" ri on p.""ReportID"" = ri.""ID""
                        inner join ""Status"" s on p.""CurrentStatusID"" = s.""ID""
	                    where s.""PrimaryCode"" in (" + String.Join(",", AcceptedStatus.ToArray()) + @") and 
                         (""IssetPackageHistory""(p.""ID"")::character varying(10)[])[1]::integer > 1 and p.""UploadTime"" > {0} and p.""UploadTime"" < {1}",  
                                                    from.ToString("yyyy-MM-dd"), to.ToString("yyyy-MM-dd"));
            }
        }

        public static List<string> AcceptedStatus = new List<string> { "7", "8", "9", "10", "11" };
    }
}
