﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.OrmLite;


namespace Account.DAO
{
    public static class FunctionsService
    {
        public static string GetIncomingNumber(int packageId)
        {
            using (var db = ORM.db)
            {
                return db.Scalar<string>("SELECT \"GetIncomingNumber\"({0});", packageId);
            }
        }
    }
}
