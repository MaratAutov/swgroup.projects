﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

using ServiceStack.OrmLite;
using ServiceStack.OrmLite.PostgreSQL;

namespace Account.DAO
{
    public static class UserService
    {
        public static UserAccount Get(string login, string password)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserAccount>(
                    u => u.Login == login && u.Password == ORM.GetHash(password)
                ).SingleOrDefault();
            }
        }

        public static UserAccount GetByInnOgrn(string inn, string ogrn)
        {
            if (String.IsNullOrEmpty(inn) || String.IsNullOrEmpty(ogrn))
                return null;
            using (var db = ORM.db)
            {
                return db.Select<UserAccount>(
                    u => u.INN == inn && u.OGRN == ogrn
                ).SingleOrDefault();
            }
        }

        public static UserAccount Get(string login)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserAccount>(
                    u => u.Login == login
                ).SingleOrDefault();
            }
        }

        public static UserAccount Get(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserAccount>(
                    u => u.ID == id
                ).SingleOrDefault();
            }
        }

        public static UserAccount GetReceiverByDocID(int id)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                var links = db.Select<DocumentLink>(
                    l => l.DocumentID == id
                );
                return db.Select<UserAccount>().Join(
                    links, d => d.ID, l => l.ReceiverID, (d, i) => d
                ).SingleOrDefault();
            }
        }

        public static UserAccount GetByInn(string inn)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserAccount>(
                    u => u.INN == inn
                ).SingleOrDefault();
            }
        }

        public static int GetCountByInnOrName(string inn, string orgName)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserAccount>(
                    u => (u.INN.Contains(inn ?? "")) &&
                         (u.Company.Contains(orgName ?? ""))
                ).Count;
            }
        }
                
        public static IEnumerable<UserAccount> Exists(string login, string inn, string ogrn)
        {
            using (var db = ORM.db) {
                return db.Select<UserAccount>(
                    p => p.Login == login || p.INN == inn || p.OGRN == ogrn
                );
            }
        }

        public static List<PackageUsersInfo> GetStats(DateTime from, DateTime to)
        {            

            using (var db = ORM.db)
            {
                return db.Select<PackageUsersInfo>(@"SELECT s.""ID"", s.""Company"", s.""INN"", COUNT(p.""ID"") AS ""Count""
		FROM ""UserAccount"" AS s LEFT JOIN ""Package"" AS p ON p.""OwnerID"" = s.""ID"" WHERE p.""UploadTime"" >= {0} AND p.""UploadTime"" <= {1} GROUP BY s.""ID""", from, to.AddDays(1));
            }
        }

        public static List<UserRegisterInfo> GetStatsRegister(DateTime from, DateTime to)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserRegisterInfo>(@"SELECT s.""ID"", s.""Company"", s.""INN""
		FROM ""UserAccount"" AS s WHERE s.""CreateDate"" >= {0} AND s.""CreateDate"" <= {1} ", from, to.AddDays(1));
            }
        }

        public static List<PackageUsersInfo> GetStats(DateTime from, DateTime to, string inn)
        {

            using (var db = ORM.db)
            {
                return db.Select<PackageUsersInfo>(@"SELECT s.""ID"", s.""Company"", s.""INN"", COUNT(p.""ID"") AS ""Count""
		FROM ""UserAccount"" AS s LEFT JOIN ""Package"" AS p ON p.""OwnerID"" = s.""ID"" WHERE p.""UploadTime"" >= {0} AND p.""UploadTime"" <= {1} AND s.""INN"" = {2} GROUP BY s.""ID""", from, to.AddDays(1),inn);
            }
        }

        public static List<PackageUsersInfo> GetStats(DateTime from, DateTime to, string inn, string orgName, int? status)
        {
            string query = String.Format(@"SELECT s.""ID"", s.""Company"", s.""INN"", COUNT(p.""ID"") AS ""Count""
		FROM ""UserAccount"" AS s LEFT JOIN ""Package"" AS p ON p.""OwnerID"" = s.""ID"" 
        LEFT JOIN ""Status"" as st ON p.""CurrentStatusID"" = st.""ID"" WHERE p.""UploadTime"" >= '{0}' AND p.""UploadTime"" <= '{1}' ", 
            from, to.AddDays(1));
            
            if (!String.IsNullOrEmpty(orgName))
                orgName = orgName.Replace('\'', '"');
            using (var db = ORM.db)
            {
                if (!String.IsNullOrEmpty(inn))
                    query += String.Format(@" AND s.""INN"" = '{0}' ", inn);
                if (!String.IsNullOrEmpty(orgName))
                    query += String.Format(@" AND s.""Company"" like '%{0}%' ", orgName);
                if (status != null)
                    query += String.Format(@" AND st.""PrimaryCode"" = {0} ", status);
                query += @" GROUP BY s.""ID""";
                return db.Select<PackageUsersInfo>(query);
            }
        }

        public static List<UserAccount> GetAll()
        {
            using (var db = ORM.db)
            {
                return db.Select<UserAccount>();
            }
        }

        public static List<UserAccount> GetAll(
            string login,
            string company,
            string inn,
            string ogrn,
            bool isActive,
            DateTime createDate,
            string orderField,
            string orderType)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserAccount>(
                    m => m.Where(
                        p => p.Login.Contains(login ?? "") &&
                             p.Company.Contains(company ?? "") &&
                             (createDate == DateTime.MinValue || (p.CreateDate >= createDate && p.CreateDate < createDate.AddDays(1))) &&
                             p.INN.StartsWith(inn ?? "") &&
                             p.OGRN.StartsWith(ogrn ?? "") &&
                             p.IsActive == isActive
                    ).OrderBy(string.Format(
                        "ORDER BY \"{0}\" {1}",
                        orderField,
                        orderType
                    ))
                ).ToList();
            }
        }

        public static void Add(
            string login,
            string password,
            string company,
            string phone,
            string fio,
            string inn,
            string ogrn,
            bool isActive,
            bool isAdmin)
        {
            using (var db = ORM.db)
            {
                db.Insert(new UserAccount()
                {
                    Login = login,
                    Password = ORM.GetHash(password),
                    IsActive = isActive,
                    IsAdmin = isAdmin,
                    Company = company,
                    FIO = fio,
                    Phone = phone,
                    INN = inn,
                    OGRN = ogrn,
                    CreateDate = DateTime.Now
                });
            }
        }

        public static void Delete(string login)
        {
            using (var db = ORM.db)
            {
                db.Delete<UserAccount>(w => w.Where(
                    m => m.Login == login
                ));
            }
        }

        public static void Update(
            string login,
            string password,
            string company,
            string phone,
            string fio,
            string inn,
            string ogrn,
            bool isActive)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                UserAccount acc = db.Select<UserAccount>(
                    u => u.Login == login
                ).SingleOrDefault();
                
                acc.Login = login;
                if (!string.IsNullOrEmpty(password) && acc.Password != password)
                    acc.Password = ORM.GetHash(password);
                acc.Company = company;
                acc.Phone = phone;
                acc.FIO = fio;
                acc.INN = inn;
                acc.OGRN = ogrn;
                acc.IsActive = isActive;
                db.Update(acc);

                trans.Commit();
            }
        }

        public static void Update(
           int id,
           string login,
           string password,
           string company,
           string phone,
           string fio,
           string inn,
           string ogrn,
           bool isActive)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                UserAccount acc = db.Select<UserAccount>(
                    u => u.ID == id
                ).SingleOrDefault();

                acc.Login = login;
                if (!string.IsNullOrEmpty(password) && acc.Password != password)
                    acc.Password = ORM.GetHash(password);
                acc.Company = company;
                acc.Phone = phone;
                acc.FIO = fio;
                acc.INN = inn;
                acc.OGRN = ogrn;
                acc.IsActive = isActive;
                db.Update(acc);

                trans.Commit();
            }
        }

        public static void ChangePassword(string login, string newPassword)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                UserAccount acc = db.Select<UserAccount>(
                    u => u.Login == login
                ).SingleOrDefault();

                acc.Password = ORM.GetHash(newPassword);
                db.Update(acc);

                trans.Commit();
            }
        }

        public static void ChangeNotification(string login, bool enabled)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                UserAccount acc = db.Select<UserAccount>(
                    u => u.Login == login
                ).SingleOrDefault();

                acc.EnableNotification = enabled;
                db.Update(acc);

                trans.Commit();
            }
        }
    }
}
