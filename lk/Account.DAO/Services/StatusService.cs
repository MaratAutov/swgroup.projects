﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;

namespace Account.DAO
{
    public static class StatusService
    {
        public static string GetStatusString(int status, bool html=true, string info=null)
        {
            PrimaryStatus s = (PrimaryStatus)status;
            string msg = null;
            string col = null;
            switch(s)
            {
                case PrimaryStatus.Accepted:
                    msg = "Пакет принят на обработку";
                    col = "blue";
                    break;
                case PrimaryStatus.Verified:
                    msg = "ЭЦП успешно проверена";
                    col = "green";
                    break;
                case PrimaryStatus.SED_Awaiting:
                    msg = "Пакет отправлен на регистрацию";
                    col = "blue";
                    break;
                case PrimaryStatus.SED_Registered:
                    msg = "Пакету присвоен входящий номер ЦА";
                    col = "green";
                    break;
                case PrimaryStatus.RO_Awaiting:
                    msg = "Пакет отправлен на получение кода РО";
                    col = "blue";
                    break;
                case PrimaryStatus.RO_Recieved:
                    msg = "Пакету присвоен код РО";
                    col = "green";
                    break;
                case PrimaryStatus.REESTR_Awaiting:
                    msg = "Пакет отправлен на регистрацию в Реестре";
                    col = "blue";
                    break;
                case PrimaryStatus.REESTR_Registered:
                    msg = "Пакет успешно зарегистрирован";
                    col = "green";
                    break;
                case PrimaryStatus.Rejected:
                    msg = "Ошибка обработки документа: ";
                    col = "red";
                    break;
                case PrimaryStatus.PACKAGE_ERROR:
                    msg = "Ошибка структуры архива: ";
                    col = "red";
                    break;
                case PrimaryStatus.SIGNATURE_ERROR:
                    msg = "Ошибка ЭЦП: ";
                    col = "red";
                    break;
                case PrimaryStatus.SIGNATURE_INVALID:
                    msg = "Подпись не легитимна: ";
                    col = "red";
                    break;
                case PrimaryStatus.REPORT_ERROR:
                    msg = "Ошибка структуры отчета: ";
                    col = "red";
                    break;
                case PrimaryStatus.REPORT_INVALID:
                    msg = "Ошибка чтения значимых полей отчета: ";
                    col = "red";
                    break;
                default:
                    msg = "Статус неизвестен";
                    col = "red";
                    break;
            }
            if (html) {
                return string.Format(
                    "<text style=\"color: {0}\">{1}</text>",
                    col,
                    info ?? msg
                );
            } else {
                return msg;
            }
        }

        public static string GetLkStatusString(int status, bool html = true)
        {
            PrimaryStatus s = (PrimaryStatus)status;
            string msg = null;
            string col = null;
            switch (s)
            {
                case PrimaryStatus.Accepted:
                    msg = "Документ загружен в систему";
                    col = "blue";
                    break;
                case PrimaryStatus.Verified:
                    msg = "ЭП корректна";
                    col = "green";
                    break;
                case PrimaryStatus.SED_Awaiting:
                    msg = "Документ принят к обработке";
                    col = "blue";
                    break;
                case PrimaryStatus.SED_Registered:
                    msg = "Документу присвоен входящий номер";
                    col = "green";
                    break;
                case PrimaryStatus.RO_Awaiting:
                    msg = "Документу присвоен входящий номер";
                    col = "blue";
                    break;
                case PrimaryStatus.RO_Recieved:
                    msg = "Документу присвоен входящий номер";
                    col = "blue";
                    break;
                case PrimaryStatus.REESTR_Awaiting:
                    msg = "Документу присвоен входящий номер";
                    col = "blue";
                    break;
                case PrimaryStatus.REESTR_Registered:
                    msg = "Документу присвоен входящий номер";
                    col = "green";
                    break;
                case PrimaryStatus.Rejected:
                    msg = "Документ не принят к обработке";
                    col = "red";
                    break;
                case PrimaryStatus.PACKAGE_ERROR:
                    msg = "Документ не принят к обработке";
                    col = "red";
                    break;
                case PrimaryStatus.SIGNATURE_ERROR:
                    msg = "Ошибка проверки ЭП";
                    col = "red";
                    break;
                case PrimaryStatus.SIGNATURE_INVALID:
                    msg = "ЭП не легитимна";
                    col = "red";
                    break;
                case PrimaryStatus.REPORT_ERROR:
                    msg = "Документ не принят к обработке";
                    col = "red";
                    break;
                case PrimaryStatus.REPORT_INVALID:
                    msg = "Документ не принят к обработке";
                    col = "red";
                    break;
                default:
                    msg = "Статус неизвестен";
                    col = "red";
                    break;
            }
            if (html)
            {
                return string.Format(
                    "<text style=\"color: {0}\">{1}</text>",
                    col,
                    msg
                );
            }
            else
            {
                return msg;
            }
            
        }

        public static string GetSPOStatusString(int status)
        {
            PrimaryStatus s = (PrimaryStatus)status;
            switch (s)
            {
                case PrimaryStatus.Accepted:
                    return "Processing";
                case PrimaryStatus.Verified:
                    return "SignatureCorrect";
                case PrimaryStatus.SED_Awaiting:
                    return "Accepted";
                case PrimaryStatus.SED_Registered:
                    return "IncomingNumberAssotiated";
                case PrimaryStatus.RO_Awaiting:
                    return "IncomingNumberAssotiated";
                case PrimaryStatus.RO_Recieved:
                    return "IncomingNumberAssotiated";
                case PrimaryStatus.REESTR_Awaiting:
                    return "IncomingNumberAssotiated";
                case PrimaryStatus.REESTR_Registered:
                    return "IncomingNumberAssotiated";
                case PrimaryStatus.Rejected:
                    return "PackageNotRegistered";
                case PrimaryStatus.PACKAGE_ERROR:
                    return "PackageCorrupted";
                case PrimaryStatus.SIGNATURE_ERROR:
                    return "SignatureError";
                case PrimaryStatus.SIGNATURE_INVALID:
                    return "InvalidSignature";
                case PrimaryStatus.REPORT_ERROR:
                    return "InvalidScheme";
                case PrimaryStatus.REPORT_INVALID:
                    return "ReportInvalid";
                default:
                    return "NotFoundStatus";
            }
        }

        public static PrimaryStatus GetPrimaryStatusByOldStatus(string status)
        {
            switch (status)
            {
                case "SignatureCorrect":
                    return PrimaryStatus.Verified;
                case "Accepted":
                    return PrimaryStatus.SED_Registered;
                case "IncomingNumberAssotiated":
                    return PrimaryStatus.REESTR_Registered;
                case "PackageNotRegistered":
                    return PrimaryStatus.Rejected;
                case "PackageCorrupted":
                    return PrimaryStatus.PACKAGE_ERROR;
                case "SignatureError":
                    return PrimaryStatus.SIGNATURE_ERROR;
                default:
                    return PrimaryStatus.NotFound;
            }
        }

        public static Status Get(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<Status>(
                    s => s.ID == id
                ).SingleOrDefault();
            }
        }        

        public static void UpdateStatusForPackages(
           int id,
           byte[] data,
           PrimaryStatus status,
           SecondaryStatus secondary = SecondaryStatus.UNSET,
           string info = null,
           string excInfo = null,
           bool IsUpdateInitialStatus = false)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                Package package = db.Select<Package>(
                    p => p.ID == id
                ).SingleOrDefault();

                Status package_status = null;

                package_status = new Status()
                {
                    PrimaryCode = (int)status,
                    SecondaryCode = (int)secondary,
                    Info = info ?? "",
                    Exception = excInfo,
                    PackageID = id,
                    AssignmentTime = DateTime.Now
                };

                db.Insert(package_status);

                int lastInsertedID = (int)db.GetLastInsertId();
                package_status.ID = lastInsertedID;
                package.CurrentStatusID = lastInsertedID;
                if (IsUpdateInitialStatus)
                    package.InitialStatusID = lastInsertedID;
                db.Update(package);
                db.Update(package_status);

                string hash = ORM.GetHash(data);

                Blob blob = null;
                blob = new Blob()
                {
                    Body = data,
                    Hash = hash
                };
                db.Insert(blob);
                blob.ID = (int)db.GetLastInsertId();

                package_status.NotificationBlobID = blob.ID;
                db.Update(package_status);

                trans.Commit();
            }
        }

        public static void RefreshStatusNotification(
           int statusid,
           byte[] data)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                string hash = ORM.GetHash(data);

                Blob blob = null;
                blob = new Blob()
                {
                    Body = data,
                    Hash = hash
                };
                db.Insert(blob);
                blob.ID = (int)db.GetLastInsertId();

                Status status = StatusService.Get(statusid);
                status.NotificationBlobID = blob.ID;
                db.Update(status);
            }
        }
                
        public static List<StatusStats> GetStats(DateTime from, DateTime to)
        {
            using (var db = ORM.db)
            {
                return db.Select<StatusStats>(@"SELECT DISTINCT s.""PrimaryCode"", COUNT(s.""PrimaryCode"") AS ""Count""
		            FROM (SELECT ""PrimaryCode"" FROM ""Status"" WHERE ""AssignmentTime"" >= {0} AND ""AssignmentTime"" <= {1}) AS s
		            GROUP BY s.""PrimaryCode""", from, to.AddDays(1));
            }
        }
    }

    public enum PrimaryStatus
    {
        NotFound = -1,
        Accepted = 0, 
        PACKAGE_ERROR,      //1
        SIGNATURE_ERROR,    //2
        REPORT_ERROR,       //3
        Rejected,           //4
        Verified,           //5
        SED_Awaiting,       //6
        SED_Registered,     //7
        RO_Awaiting,        //8
        RO_Recieved,        //9
        REESTR_Awaiting,    //10
        REESTR_Registered,  //11
        SIGNATURE_INVALID,  //12
        REPORT_INVALID,     //13
        _LAST_
    }
        
    public enum SecondaryStatus
    {
        UNSET = -1,
        ERROR_UNKNOWN = 0,
        ERROR_CANT_UNCOMPRESS_ZIP = 1,
        ERROR_FIELDS_EMPTY,
        ERROR_UNEXPECTED_FILES_COUNT,
        ERROR_WRONG_PROGRAMM_VERSION,
        ERROR_UNEXPRECTED_CONTENT_TYPE,
        ERROR_UNKNOWN_REPORT_TYPE,
        ERROR_WRONG_XTDD_VERSION,
        ERROR_PACKAGE_TYPE_UNSUPPORTED,
        ERROR_PACKAGE_ALREADY_REGISTERED,
        ERROR_PACKAGE_ALREADY_ACCEPTED,
        ERROR_UNKNOWN_XTDD_MODEL,
        ERROR_CANT_PARSE_HTTP,
        
        ERROR_XSD_VALIDATION,

        SIGNATURE_FILE_NOT_FOUND,
        REPORT_FILE_NOT_FOUND
    }
}
