﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;

namespace Account.DAO
{
    public class BlobServise
    {
        public static Blob Get(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<Blob>(
                    s => s.ID == id
                ).SingleOrDefault();
            }
        }
    }
}
