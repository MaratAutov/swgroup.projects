﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;

namespace Account.DAO
{
    public static class UserMonitoringService
    {
        public static UserMonitoring Get(string login, string password)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserMonitoring>(
                    u => u.Login == login && u.Password == ORM.GetHash(password)
                ).SingleOrDefault();
            }
        }

        public static UserMonitoring GetFullInfo(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserMonitoring>(
                    p => p.ID == id
                ).SingleOrDefault();
            }
        }

        public static UserMonitoring Get(string login)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserMonitoring>(
                    u => u.Login == login
                ).SingleOrDefault();
            }
        }

        public static UserMonitoring GetByID(string id)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserMonitoring>(
                    u => u.ID == Convert.ToInt32(id)
                ).SingleOrDefault();
            }
        }

        public static UserMonitoring Get(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserMonitoring>(
                    u => u.ID == id
                ).SingleOrDefault();
            }
        }



        public static IEnumerable<UserMonitoring> Exists(string login)
        {
            using (var db = ORM.db) {
                return db.Select<UserMonitoring>(
                    p => p.Login == login 
                );
            }
        }


        public static List<UserMonitoring> GetAll()
        {
            using (var db = ORM.db)
            {
                return db.Select<UserMonitoring>().ToList();
            }
        }

        public static void Add(
            string login,
            string password,
            string region,         
            string fio,           
            bool isActive,
            bool isAdmin)
        {
            using (var db = ORM.db)
            {
                db.Insert(new UserMonitoring()
                {
                    Login = login,
                    Password = ORM.GetHash(password),
                    IsActive = isActive,
                    IsAdmin = isAdmin,
                    RegionUser = region,
                    FIO = fio,                   
                    CreateDate = DateTime.Now
                });
            }
        }

        public static void Delete(string login)
        {
            using (var db = ORM.db)
            {
                db.Delete<UserMonitoring>(w => w.Where(
                    m => m.Login == login
                ));
            }
        }

        public static void DeleteByID(int id)
        {
            using (var db = ORM.db)
            {
                db.Delete<UserMonitoring>(w => w.Where(
                    m => m.ID == id
                ));
            }
        }

        public static List<UserMonitoring> GetStats()
        {
            using (var db = ORM.db)
            {
                return db.Select<UserMonitoring>().ToList();
            }
        }

        public static void Update(
            string login,
            string password,
            string region,           
            string fio,  
            bool isAdmin,
            bool isActive)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                UserMonitoring acc = db.Select<UserMonitoring>(
                    u => u.Login == login
                ).SingleOrDefault();
                
                acc.Login = login;
                if (!string.IsNullOrEmpty(password) && acc.Password != password)
                    acc.Password = ORM.GetHash(password);
                acc.RegionUser = region;               
                acc.FIO = fio;
                acc.IsAdmin = isAdmin;
                acc.IsActive = isActive;
                db.Update(acc);

                trans.Commit();
            }
        }

        public static void ChangePassword(string login, string newPassword)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                UserMonitoring acc = db.Select<UserMonitoring>(
                    u => u.Login == login
                ).SingleOrDefault();

                acc.Password = ORM.GetHash(newPassword);
                db.Update(acc);

                trans.Commit();
            }
        }       
    }
}
