﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;

namespace Account.DAO
{
    public static class PackageDateLimitService
    {
        public static PackageDateLimit Get(int year)
        {
            using (var db = ORM.db)
            {
                return db.Select<PackageDateLimit>(
                    u => u.Year == year
                ).SingleOrDefault();
            }
        }

        

        public static bool Exists(PackageDateLimit p)
        {
            using (var db = ORM.db)
            {
                return db.Select<PackageDateLimit>().Where(m => m.Year == p.Year).FirstOrDefault() != null;
            }
        }

        public static List<PackageDateLimit> GetAll()
        {
            using (var db = ORM.db)
            {
                return db.Select<PackageDateLimit>(
                    m => m.OrderBy("ORDER BY \"Year\" ASC")
                ).ToList();
            }
        }

        public static void Add(PackageDateLimit pdl)
        {
            using (var db = ORM.db)
            {
                db.Insert(pdl);
            }
        }

        public static void Delete(int? year)
        {
            using (var db = ORM.db)
            {
                if (year == null)
                {
                    db.ExecuteSql("DELETE FROM \"PackageDateLimit\" WHERE \"Year\" IS NULL");
                }
                else
                {
                    db.Delete<PackageDateLimit>(w => w.Where(
                        m => m.Year == year
                    ));
                }
                Console.WriteLine(db.GetLastSql());
            }
        }

        public static void Update(PackageDateLimit pdl)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                PackageDateLimit acc = db.Select<PackageDateLimit>(
                    u => u.Year == pdl.Year
                ).SingleOrDefault();

                acc.Quarter1 = pdl.Quarter1;
                acc.Quarter2 = pdl.Quarter2;
                acc.Quarter3 = pdl.Quarter3;
                acc.Quarter4 = pdl.Quarter4;

                acc.YearLimit = pdl.YearLimit;

                acc.January = pdl.January;
                acc.February = pdl.February;
                acc.March = pdl.March;
                acc.April = pdl.April;
                acc.May = pdl.May;
                acc.June = pdl.June;
                acc.July = pdl.July;
                acc.August = pdl.August;
                acc.September = pdl.September;
                acc.October = pdl.October;
                acc.November = pdl.November;
                acc.December = pdl.December;
                db.Update(acc);

                trans.Commit();
            }
        }

        public static List<PackageShortInfo> GetYearStats(int year)
        {
            PackageDateLimit limit = Get(year);
            if (limit == null)
                throw new Exception(String.Format("Не установлены сроки сдачи отчетности для {0} года", year));
            using (var db = ORM.db)
            {
                return db.Select<PackageShortInfo>("SELECT p.\"Name\", p.\"UploadTime\", ua.\"INN\" as OwnerINN, ua.\"Company\" as OwnerCompany, ua.\"Login\" as OwnerLogin, " +
	                                                    "ri.\"Year\", ri.\"Quartal\", ri.\"Month\", ri.\"Date\" " +
                                                        "FROM \"Package\" p " +
	                                                    "inner JOIN \"ReportInfo\" ri on p.\"ReportID\"=ri.\"ID\" " +
	                                                    "INNER JOIN \"UserAccount\" ua on p.\"OwnerID\"=ua.\"ID\" " +
                                                        "WHERE ri.\"Year\"='{0}' AND \"Month\" is null AND \"Date\" is null AND \"Quartal\" is null AND \"UploadTime\" > {1} ", year, limit.YearLimit.ToString("yyyy-MM-dd"));
            }
            
        }

        /// <summary>
        /// Кварталы передаются int начиная от 0. По крайней мере в форме 1100 кварталы начинаются с единицы, поэтому прибавляем единицу, в надежде что так они жранятся везде
        /// </summary>
        /// <param name="year"></param>
        /// <param name="quarter"></param>
        /// <returns></returns>
        public static List<PackageShortInfo> GetQuarterStats(int year, int quarter)
        {
            PackageDateLimit limit = Get(year);
            if (limit == null)
                throw new Exception(String.Format("Не установлены сроки сдачи отчетности для {0} года", year));
            DateTime qlimit = DateTime.MinValue;
            switch (quarter)
            {
                case 0:
                    qlimit = limit.Quarter1;
                    break;
                case 1:
                    qlimit = limit.Quarter2;
                    break;
                case 2:
                    qlimit = limit.Quarter3;
                    break;
                case 3:
                    qlimit = limit.Quarter4;
                    break;
            }

            using (var db = ORM.db)
            {
                return db.Select<PackageShortInfo>("SELECT p.\"Name\", p.\"UploadTime\", ua.\"INN\" as OwnerINN, ua.\"Company\" as OwnerCompany, ua.\"Login\" as OwnerLogin, " +
                                                        "ri.\"Year\", ri.\"Quartal\", ri.\"Month\", ri.\"Date\" " +
                                                        "FROM \"Package\" p " +
                                                        "inner JOIN \"ReportInfo\" ri on p.\"ReportID\"=ri.\"ID\" " +
                                                        "INNER JOIN \"UserAccount\" ua on p.\"OwnerID\"=ua.\"ID\" " +
                                                        "WHERE ri.\"Year\"='{0}' AND ri.\"Quartal\" = '{1}' AND \"Month\" is null AND \"Date\" is null AND \"UploadTime\" > {2} ",
                                                        year, quarter + 1, qlimit.ToString("yyyy-MM-dd"));
            }

        }

        public static List<PackageShortInfo> GetMonthStats(int year, int month)
        {
            PackageDateLimit limit = Get(year);
            if (limit == null)
                throw new Exception(String.Format("Не установлены сроки сдачи отчетности для {0} года", year));
            DateTime mlimit = DateTime.MinValue;
            switch (month)
            {
                case 0:
                    mlimit = limit.January;
                    break;
                case 1:
                    mlimit = limit.February;
                    break;
                case 2:
                    mlimit = limit.March;
                    break;
                case 3:
                    mlimit = limit.April;
                    break;
                case 4:
                    mlimit = limit.May;
                    break;
                case 5:
                    mlimit = limit.June;
                    break;
                case 6:
                    mlimit = limit.July;
                    break;
                case 7:
                    mlimit = limit.August;
                    break;
                case 8:
                    mlimit = limit.September;
                    break;
                case 9:
                    mlimit = limit.October;
                    break;
                case 10:
                    mlimit = limit.November;
                    break;
                case 11:
                    mlimit = limit.December;
                    break;
            }

            using (var db = ORM.db)
            {
                return db.Select<PackageShortInfo>("SELECT p.\"Name\", p.\"UploadTime\", ua.\"INN\" as OwnerINN, ua.\"Company\" as OwnerCompany, ua.\"Login\" as OwnerLogin, " +
                                                        "ri.\"Year\", ri.\"Quartal\", ri.\"Month\", ri.\"Date\" " +
                                                        "FROM \"Package\" p " +
                                                        "inner JOIN \"ReportInfo\" ri on p.\"ReportID\"=ri.\"ID\" " +
                                                        "INNER JOIN \"UserAccount\" ua on p.\"OwnerID\"=ua.\"ID\" " +
                                                        "WHERE ri.\"Year\"='{0}' AND \"Month\" = '{1}' AND \"Date\" is null AND \"Quartal\" is null AND \"UploadTime\" > {2} ", year, month + 1, mlimit.ToString("yyyy-MM-dd"));
            }

        }
    }
}
