﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;
using System.Runtime.InteropServices;

namespace Account.DAO
{
    public static class DocumentService
    {
        public static List<Document> Get(int userId)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                var links = db.Select<DocumentLink>(
                    l => l.ReceiverID == userId
                );
                return db.Select<Document>().Join(
                    links, d => d.ID, l => l.DocumentID, (d, i) => d
                ).ToList();
            }
        }

        public static List<Document> Get(int userId, 
            DateTime UploadTime,
            string name,
            string senderINN,
            string senderName,
            string ReadTime,
            string orderField,
            string orderType)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            { 
                    string select = string.Empty;
                    select = "SELECT p.\"ID\", p.\"BlobID\", p.\"Name\", p.\"OwnerID\", p.\"UploadTime\",p.\"ReadTime\" " +
                                                        "FROM \"Document\" p " +
                                                        "INNER JOIN \"DocumentLink\" dl on dl.\"ReceiverID\" = {0} AND p.\"ID\" = dl.\"DocumentID\" " +
                                                        "INNER JOIN \"UserAccount\" ua on p.\"OwnerID\"=ua.\"ID\" WHERE 1=1 ";
                    
                    if (!String.IsNullOrEmpty(name))
                    {
                        select += string.Format("AND lower(p.\"Name\") LIKE '%{0}%' ",name.ToLower());
                    }

                    if (UploadTime != DateTime.MinValue)
                    {
                        select += string.Format("AND ((\"UploadTime\" > '{0}' OR (\"UploadTime\" = '{0}')) AND \"UploadTime\" < '{1}') ", UploadTime.ToString("yyyy-MM-dd"), UploadTime.AddDays(1).ToString("yyyy-MM-dd"));
                    }

                    if (!String.IsNullOrEmpty(senderINN))
                    {
                        select += string.Format("AND ua.\"INN\" = '{0}' ", senderINN);
                    }

                    if (!String.IsNullOrEmpty(senderName))
                    {
                        select += string.Format("AND lower(ua.\"Company\") LIKE '%{0}%' ", senderName.ToLower());
                    }

                    if (ReadTime == "on")
                    {
                        select += "AND \"ReadTime\" is null ";
                    }

                    select += string.Format("ORDER BY \"{0}\" {1}", orderField, orderType);
                    return db.Select<Document>(select,userId).ToList();
                
            }
        }

        public static List<Document> GetOutgoing(int userId)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {

                return db.Select<Document>(d => d.OwnerID == userId).ToList();               
            }
        }

        public static List<Document> GetOutgoing(int userId,
            string name,
            string receiverINN,
            string receiverName,
            DateTime UploadTime,
            string orderField,
            string orderType)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                string select = string.Empty;
                select = string.Format("SELECT p.\"ID\", p.\"BlobID\", p.\"Name\", p.\"OwnerID\", p.\"UploadTime\",p.\"ReadTime\" " +
                                                    "FROM \"Document\" p " +
                                                    "INNER JOIN \"DocumentLink\" dl on dl.\"DocumentID\" = p.\"ID\" " +
                                                    "INNER JOIN \"UserAccount\" ua on dl.\"ReceiverID\"=ua.\"ID\" WHERE \"OwnerID\"= {0} ",userId);

                if (!String.IsNullOrEmpty(name))
                {
                    select += string.Format("AND lower(p.\"Name\") LIKE '%{0}%' ", name.ToLower());
                }

                if (UploadTime != DateTime.MinValue)
                {
                    select += string.Format("AND ((\"UploadTime\" > '{0}' OR (\"UploadTime\" = '{0}')) AND \"UploadTime\" < '{1}') ", UploadTime.ToString("yyyy-MM-dd"), UploadTime.AddDays(1).ToString("yyyy-MM-dd"));
                }

                if (!String.IsNullOrEmpty(receiverINN))
                {
                    select += string.Format("AND ua.\"INN\" = '{0}' ", receiverINN);
                }

                if (!String.IsNullOrEmpty(receiverName))
                {
                    select += string.Format("AND lower(ua.\"Company\") LIKE '%{0}%' ", receiverName.ToLower());
                }                

                select += string.Format("ORDER BY \"{0}\" {1}", orderField, orderType);

                return db.Select<Document>(select).ToList();                
            }
        }

        public static Document GetDocument(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<Document>(
                    p => p.ID == id
                ).SingleOrDefault();
            }
        }

        public static void UpdateReadTime(int id)
        {
            Document doc;
            using (var db = ORM.db)
            {
                doc =  db.Select<Document>(
                    p => p.ID == id
                ).SingleOrDefault();
                using (var trans = db.OpenTransaction())
                {
                    doc.ReadTime = DateTime.Now;
                    db.Update(doc);
                    trans.Commit();
                }
            }
            
        }

        private static Document GetDocumentByBlobId(int blobId, string name, IDbConnection db)
        {
            return db.Select<Document>(
                p => p.BlobID == blobId && p.Name == name
            ).SingleOrDefault();
        }

        private static List<Blob> _GetBlobsByHash(string hash, IDbConnection db)
        {
            return db.Select<Blob>(
                b => b.Hash == hash
            );
        }

        public static Blob GetBlob(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<Blob>(
                    p => p.ID == id
                ).SingleOrDefault();
            }
        }

        private static List<Blob> GetBlobsByHash(string hash, IDbConnection db = null)
        {
            if (db == null)
                using (db = ORM.db)
                {
                    return _GetBlobsByHash(hash, db);
                }
            else
                return _GetBlobsByHash(hash, db);
        }

        private static bool CompareArrays(byte[] a, byte[] b)
        {
            if (a == null || b == null || a.Length != b.Length)
                return false;
            else
                return memcmp(a, b, a.LongLength) == 0;
        }

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int memcmp(byte[] b1, byte[] b2, long count);

        public static Document GetOrCreate(
            string name,
            byte[] data,
            int ownerId,
            DateTime uploadTime,
            int receiver)
            
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                string hash = ORM.GetHash(data);
                List<Blob> blobs = GetBlobsByHash(hash, db);
                Blob blob = null;

                foreach (Blob b in blobs)
                {
                    //if (CompareArrays(Convert.FromBase64String(Encoding.UTF8.GetString(b.Body)), data))
                    if (CompareArrays(b.Body, data))
                    {
                        blob = b;
                        Document p = GetDocumentByBlobId(b.ID, name, db);
                        if (p != null)
                        {
                            p.Existing = true;
                            return p;
                        }
                    }
                }

                if (blob == null)
                {
                    blob = new Blob()
                    {
                        Body = data,
                        Hash = hash
                    };
                    db.Insert(blob);
                    blob.ID = (int)db.GetLastInsertId();
                }                

                Document doc = new Document()
                {
                    Name = name,
                    OwnerID = ownerId,
                    BlobID = blob.ID,
                    UploadTime = uploadTime,
                    Existing = false,
                    ReadTime = null,
                };


                db.Insert(doc);
                doc.ID = (int)db.GetLastInsertId();

                DocumentLink link = new DocumentLink()
                {
                    DocumentID = doc.ID,
                    ReceiverID = receiver
                };

                db.Insert(link);
                trans.Commit();

                return doc;
            }
        }
    }
}
