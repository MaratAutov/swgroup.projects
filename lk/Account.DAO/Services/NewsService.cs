﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;

namespace Account.DAO
{
    public static class NewsService
    {
        public static List<News> GetAll(string year, string month, DateTime datePosted, string body, string orderField, string orderType)
        {
            DateTime date1 = new DateTime();
            DateTime date2 = new DateTime();
            if (year != null)
            {
                if (month != null)
                {
                    date1 = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 01);
                    date2 = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 01).AddMonths(1);
                }
                else
                {
                    date1 = new DateTime(Convert.ToInt32(year), 01, 01);
                    date2 = new DateTime(Convert.ToInt32(year), 01, 01).AddYears(1);
                }
            }
            else
            {
                date1 = datePosted;
                date2 = datePosted.AddDays(1);
            }

            using (var db = ORM.db) {
                
                if (body == null)
                {
                    //return db.Select<News>(
                    //    m => m.Where(
                    //        p => datePosted == DateTime.MinValue || (p.DatePosted >= datePosted && p.DatePosted < datePosted.AddDays(1))
                             
                    //    ).OrderBy(string.Format(
                    //        "ORDER BY \"{0}\" {1}",
                    //        orderField,
                    //        orderType
                    //    ))
                    //);

                    return db.Select<News>(
                        m => m.Where(
                            p => datePosted == DateTime.MinValue || (p.DatePosted >= date1 && p.DatePosted < date2)

                        ).OrderBy(string.Format(
                            "ORDER BY \"{0}\" {1}",
                            orderField,
                            orderType
                        ))
                    );
                }
                else
                {
                    return db.Select<News>(
                        m => m.Where(
                            p => (datePosted == DateTime.MinValue || (p.DatePosted >= date1 && p.DatePosted < date2)) &&
                                 (p.Body.Contains(body))
                        ).OrderBy(string.Format(
                            "ORDER BY \"{0}\" {1}",
                            orderField,
                            orderType
                        ))
                    );    
                }
            }
        }

        public static List<NewsTiming> GetTiming()
        {
            using (var db = ORM.db) {
                return db.Select<NewsTiming>();
            }
        }

        public static News Get(string id)
        {
            using (var db = ORM.db) {
                int iid = int.Parse(id);
                return db.Select<News>(
                    p => p.ID == iid
                ).SingleOrDefault();                
            }
        }

        public static News GetByBody(string body)
        {
            using (var db = ORM.db)
            {
                return db.Select<News>(
                    p => p.Body == body
                ).SingleOrDefault();
            }
        }

        
        public static void Update(int id, int userId, string body)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                var n = db.Select<News>(
                    p => p.ID == id
                ).SingleOrDefault();
                if (n == null)
                    throw new ArgumentException("Новость не найдена");
                n.Body = body;
                db.Update(n, p => p.ID == id);
                trans.Commit();
            }
        }

        public static void Add(int ownerId, string body)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction()) {
                var now = DateTime.Now;
                db.Insert(new News {
                    Body = body,
                    UserPosted = ownerId,
                    UserLastModified = ownerId,
                    DatePosted = now,
                    DateLastModified = now
                });
                trans.Commit();                
            }
        }

        public static void Delete(int id)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction()) {
                db.Delete<News>(
                    n => n.ID == id
                );
                trans.Commit();
            }
        }
    }
}
