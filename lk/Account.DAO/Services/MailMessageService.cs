﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ServiceStack.OrmLite;

namespace Account.DAO
{
    public static class MailMessageService
    {
        public static MailMessage GetLastMessageToSed(int packageID)
        {
            using (var db = ORM.db)
            {
                return db.SingleOrDefault<MailMessage>(string.Format("\"PackageID\" = {0} AND \"Info\" = 'To SED' ORDER BY \"CreateAt\" DESC limit 1", packageID));
            }
        }

        public static MailMessage GetLastMessageFromSed(int packageID)
        {
            using (var db = ORM.db)
            {
                return db.SingleOrDefault<MailMessage>(string.Format("\"PackageID\" = {0} AND \"Info\" = 'From SED' ORDER BY \"CreateAt\" DESC limit 1", packageID));
            }
        }

        public static MailMessage GetFirstMessageFromSed(int packageID)
        {
            using (var db = ORM.db)
            {
                return db.SingleOrDefault<MailMessage>(string.Format("\"PackageID\" = {0} AND \"Info\" = 'From SED' ORDER BY \"CreateAt\" ASC limit 1", packageID));
            }
        }

        public static IList<MailMessage> GetAllMessageByPacketID(int packageId)
        {
            using (var db = ORM.db)
            {
                return db.Select<MailMessage>(m => m.Where(p => p.PackageID == packageId));
            }
        }

        public static MailMessage GetMailMessage(int id)
        {
            using (var db = ORM.db)
            {
                return db.SingleOrDefault<MailMessage>(String.Format("\"ID\" = {0}", id));
            }
        }
    }
}
