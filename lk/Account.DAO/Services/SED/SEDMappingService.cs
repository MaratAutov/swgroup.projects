﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ServiceStack.OrmLite;

namespace Account.DAO.SED
{
    public class SEDMappingService
    {
        private static List<ModelInfo> ModelInfoCache = null;
        private static List<DocMap> DocMapCache = null;
        private static List<OrgMap> OrgMapCache = null;
        private static List<DocMapSignature> DocMapSignatureCache = null;

        public static List<ModelInfo> GetConfigurations()
        {
            if( ModelInfoCache == null )
                ModelInfoCache = ORM.db.Select<ModelInfo>();
            return ModelInfoCache;
        }

        public static List<DocMap> GetDocMapping()
        {
            if (DocMapCache == null)
                DocMapCache = ORM.db.Select<DocMap>();
            return DocMapCache;
        }

        public static List<OrgMap> GetOrgMapping()
        {
            if (OrgMapCache == null)
                OrgMapCache = ORM.db.Select<OrgMap>();
            return OrgMapCache;
        }

        public static List<DocMapSignature> GetDocMapSignature()
        {
            if (DocMapSignatureCache == null)
                DocMapSignatureCache = ORM.db.Select<DocMapSignature>();
            return DocMapSignatureCache;
        }
    }
}
