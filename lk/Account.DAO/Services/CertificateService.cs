﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;

namespace Account.DAO
{
    public static class CertificateService
    {
        public static void Create(string signature)
        {
            Certificate cert = DecodeSignature(
                Convert.FromBase64String(signature)
            );
            using (var trans = ORM.db.OpenTransaction())
            {
                ORM.db.Insert(cert);
                trans.Commit();
            }
        }

        /// <summary>
        /// Расшифровать подпись и получить сертификат
        /// </summary>
        /// <param name="signedData"></param>
        /// <returns></returns>
        internal static Certificate DecodeSignature(byte[] signedData)
        {
            // расшифровываем подпись
            // объект, в котором будут происходить декодирование и проверка.
            SignedCms signedCms = new SignedCms(new ContentInfo(signedData), false);
            // декодируем подпись.
            signedCms.Decode(signedData);
            var signerInfo = signedCms.SignerInfos.OfType<SignerInfo>().FirstOrDefault();
            if (signerInfo == null)
                throw new ArgumentException("файл не подписан.");
            if (signerInfo.Certificate != null)
            {
                // Используем проверку подписи и стандартную 
                // процедуру проверки сертификата: построение цепочки, 
                // проверку цепочки, и необходимых расширений для данного 
                // сертификата.
                signerInfo.CheckSignature(false);
            }
            else
                throw new ArgumentException("подпись не содержит сертификата.");
            var certData = signerInfo.Certificate;
            return new Certificate
            {
                IssuerName = certData.Issuer.Substring(3),
                SerialNumber = certData.SerialNumber,
                SubjectName = certData.SubjectName.Name.Substring(3),
                ValidFrom = certData.NotBefore,
                ValidTo = certData.NotAfter,
                PublicKey = certData.GetPublicKey()
            };
        }
    }
}
