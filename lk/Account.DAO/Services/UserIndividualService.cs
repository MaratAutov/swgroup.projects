﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;
using System.Data;

namespace Account.DAO
{
    public static class UserIndividualService
    {
        public static void Add(
            string login,
            string password,
            string firstname,
            string secondname,
            string middlename,
            DateTime birthdate,
            string ucName,
            string ucINN,
            object pInfo,
            bool isActive)
        {

            UserIndividual user = new UserIndividual()
            {
                Login = login,
                Password = ORM.GetHash(password),
                FirstName = firstname,
                SecondName = secondname,
                MiddleName = middlename,
                BirthDate = birthdate,
                UC_Name = ucName,
                UC_INN = ucINN,
                Info = Encode(pInfo),
                IsActive = isActive
            };

            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                
                db.Insert(user); 
                trans.Commit();
            }
        }

        public static UserIndividual Get(string login)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                return db.Select<UserIndividual>(
                    u => u.Login == login
                ).SingleOrDefault();
            }
        }

        public static UserIndividual Get(string login, string password)
        {
            using (var db = ORM.db)
            {
                return db.Select<UserIndividual>(
                    u => u.Login == login && u.Password == ORM.GetHash(password)
                ).SingleOrDefault();
            }
        }


        public static void ChangePassword(string login, string newPassword)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                UserIndividual acc = db.Select<UserIndividual>(
                    u => u.Login == login
                ).SingleOrDefault();

                acc.Password = ORM.GetHash(newPassword);
                db.Update(acc);

                trans.Commit();
            }
        }

        public static IEnumerable<UserIndividual> Exists(string login, object pInfo)
        {
            using (var db = ORM.db) {
                return db.Select<UserIndividual>(
                    p => p.Login == login ||
                    (p.Info == Encode(pInfo))
                );
            }
        }

        public static List<UserIndividual> GetAll(
            string login,
            string firstname,
            string secondname,
            string middlename,
            bool isActive,
            DateTime birth_date,
            string orderField,
            string orderType)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                return db.Select<UserIndividual>(
                    m => m.Where(
                        p => p.Login.Contains(login ?? "") &&
                             p.FirstName.Contains(firstname ?? "") &&
                             p.SecondName.Contains(secondname ?? "") &&
                             p.MiddleName.Contains(middlename ?? "") &&
                             p.IsActive == isActive &&
                             (birth_date == DateTime.MinValue || (p.BirthDate >= birth_date && p.BirthDate < birth_date.AddDays(1)))
                    ).OrderBy(string.Format(
                        "ORDER BY \"{0}\" {1}",
                        orderField,
                        orderType
                    ))
                ).ToList();
            }
        }

        public static void Update(
            string login,
            string password,
            bool isActive,
            string firstname,
            string secondname,
            string middlename,
            DateTime birthDate,
            string ucName,
            string ucINN,
            object pInfo)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                UserIndividual acc = db.Select<UserIndividual>(
                    u => u.Login == login
                ).SingleOrDefault();

                acc.Login = login;
                if (!string.IsNullOrEmpty(password) && acc.Password != password)
                    acc.Password = ORM.GetHash(password);
                acc.FirstName = firstname;
                acc.SecondName = secondname;
                acc.MiddleName = middlename;
                acc.BirthDate = birthDate;
                acc.UC_Name = ucName;
                acc.UC_INN = ucINN;
                acc.IsActive = isActive;
                acc.Info = Encode(pInfo);
                db.Update(acc);

                trans.Commit();
            }            
        }

        public static void Delete(string login)
        {
            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                db.Delete<UserIndividual>(w => w.Where(
                    m => m.Login == login
                ));
                trans.Commit();
            }
        }

        public static byte[] Encode(object obj)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                var binary = new BinaryFormatter();
                binary.Serialize(stream, obj);
                stream.Position = 0;
                using (var reader = new BinaryReader(stream))
                {
                    int len = (int)stream.Length;
                    byte[] buffer = new byte[len];
                    reader.Read(buffer, 0, len);
                    
                    return buffer;
                }
            }
        }

        public static object Decode(byte[] data)
        {
            var binary = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream(data))
            {
                try
                {
                    return binary.Deserialize(stream);
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}
