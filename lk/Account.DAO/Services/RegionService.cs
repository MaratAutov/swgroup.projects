﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;

namespace Account.DAO
{
    public static class RegionService
    {
        public static Region Get(string name)
        {
            using (var db = ORM.db)
            {
                return db.Select<Region>(
                    u => u.Name == name
                ).SingleOrDefault();
            }
        }

        public static Region GetByID(int id)
        {
            using (var db = ORM.db)
            {
                return db.Select<Region>(
                    u => u.ID == id
                ).SingleOrDefault();
            }
        }
        public static List<Region> GetAll()
        {
            using (var db = ORM.db)
            {
                return db.Select<Region>().ToList();
            }
        }       
    }
}
