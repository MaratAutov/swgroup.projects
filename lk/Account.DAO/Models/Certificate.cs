﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("Certificate")]
    public class Certificate
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        [StringLength(100)]
        public string SerialNumber { get; set; }

        [StringLength(500)]
        public string IssuerName { get; set; }

        [StringLength(500)]
        public string SubjectName { get; set; }

        public DateTime ValidFrom { get; set; }

        public DateTime ValidTo { get; set; }

        public byte[] PublicKey { get; set; }
    }
}
