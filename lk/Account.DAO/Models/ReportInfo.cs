﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("ReportInfo")]
    public class ReportInfo
    {
        [PrimaryKey]
        [AutoIncrement]
        public int ID { get; set; }

        [StringLength(36)]
        [Default(typeof(string), "")]
        public string ExternalID { get; set; }

        [StringLength(25)]
        [Default(typeof(string), "")]
        public string Version { get; set; }

        [StringLength(255)]
        [Default(typeof(string), "")]
        public string OrgFullName { get; set; }

        [StringLength(255)]
        [Default(typeof(string), "")]
        public string OrgShortName { get; set; }

        public int? OrgTypeCode { get; set; }

        [StringLength(255)]
        [Default(typeof(string), "")]
        public string OrgTypeName { get; set; }

        public int? DocTypeCode { get; set; }

        [StringLength(255)]
        [Default(typeof(string), "")]
        public string DocTypeName { get; set; }

        [StringLength(4)]
        [Default(typeof(string), "")]
        public string Year { get; set; }

        [StringLength(4)]
        [Default(typeof(string), "")]
        public string Quartal { get; set; }

        [StringLength(4)]
        [Default(typeof(string), "")]
        public string Month { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(255)]
        [Default(typeof(string), "")]
        public string Address { get; set; }

        [StringLength(511)]
        public string Subject { get; set; }

        [StringLength(255)]
        [Default(typeof(string), "")]
        public string FirstName { get; set; }
        
        [StringLength(255)]
        [Default(typeof(string), "")]
        public string LastName { get; set; }
        
        [StringLength(255)]
        [Default(typeof(string), "")]
        public string MiddleName { get; set; }

        [StringLength(255)]
        public string IncomingNumber { get; set; }

        public DateTime? IncomingDate { get; set; }

        [StringLength(255)]
        public string SedNumber { get; set; }

        public DateTime? SedDate { get; set; }

        [StringLength(10)]
        [Default(typeof(string), "")]
        public string INN { get; set; }

        [StringLength(13)]
        [Default(typeof(string), "")]
        public string OGRN { get; set; }

        [StringLength(255)]
        [Default(typeof(string), "")]
        public string Model { get; set; }

        [StringLength(255)]
        [Default(typeof(string), "")]
        public string Type { get; set; }

        [StringLength(255)]
        public string DocumentUrl { get; set; }
        [StringLength(255)]
        public string DocumentSignUrl { get; set; }
        [StringLength(255)]
        public string NotificationUrl { get; set; }
        [StringLength(255)]
        public string NotificationSignUrl { get; set; }
        [StringLength(255)]
        public string ProcessedDocumentUrl { get; set; }

        [StringLength(512)]        
        public string RoName { get; set; }

        [StringLength(4)]
        public string RoType { get; set; }

        public bool IsRoReport { get; set; }        
    }
}
