﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("UserIndividual")]
    public class UserIndividual
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        [StringLength(32)]
        public string Login { get; set; }
        
        [StringLength(32)]
        public string Password { get; set; }
        
        public bool IsActive { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }
        
        [StringLength(255)]
        public string SecondName { get; set; }

        [StringLength(255)]
        public string MiddleName { get; set; }

        public DateTime BirthDate { get; set; }

        [StringLength(255)]
        public string UC_Name { get; set; }

        [StringLength(10)]
        public string UC_INN { get; set; }

        public byte[] Info { get; set; }
    }
}
