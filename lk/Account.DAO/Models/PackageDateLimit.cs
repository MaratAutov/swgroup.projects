﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("PackageDateLimit")]
    public class PackageDateLimit
    {
        [PrimaryKey]
        public int? Year { get; set; }

        public DateTime Quarter1 { get; set; }
        public DateTime Quarter2 { get; set; }
        public DateTime Quarter3 { get; set; }
        public DateTime Quarter4 { get; set; }

        public DateTime YearLimit { get; set; }

        public DateTime January { get; set; }
        public DateTime February { get; set; }
        public DateTime March { get; set; }
        public DateTime April { get; set; }
        public DateTime May { get; set; }
        public DateTime June { get; set; }
        public DateTime July { get; set; }
        public DateTime August { get; set; }
        public DateTime September { get; set; }
        public DateTime October { get; set; }
        public DateTime November { get; set; }
        public DateTime December { get; set; }
        
    }
}
