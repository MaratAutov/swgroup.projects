﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Account.DAO.Models
{
    public class Task
    {
        public int ID { get; set; }
        public int PackageID { get; set; }
        public ExecutionResult ExecutionResultCode { get; set; }
        public DateTime? StartExecution { get; set; }
        public DateTime CreationDate { get; set; }
        public string Exception { get; set; }
        public int? Step { get; set; }
        public int RestartCount { get; set; }
        public int PrevStatusID { get; set; }
        public int CurrentStatusID { get; set; }
    }

    public class PackageForTask
    {

    }

    public enum ExecutionResult
    {
        NotExecuted = 0,
        Error,
        FatalError,
        Executed
    }
}
