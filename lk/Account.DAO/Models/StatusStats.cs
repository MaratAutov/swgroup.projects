﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ServiceStack.DataAnnotations;


namespace Account.DAO
{
    //[Alias("StatusStats")]
    public class StatusStats
    {
        public int PrimaryCode { get; set; }
        public int Count { get; set; }

        //[Ignore]
        public string Status
        {
            get
            {
                return StatusService.GetStatusString(PrimaryCode);
            }
        }
    }
}