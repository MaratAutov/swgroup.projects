﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("Status")]
    public class Status
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        public int PackageID { get; set; }

        public int PrimaryCode { get; set; }

        public int SecondaryCode { get; set; }

        public DateTime AssignmentTime { get; set; }

        public string Info { get; set; }

        public string Exception { get; set; }

        public int? NotificationBlobID { get; set; } 

        public override string ToString()
        {
            return StatusService.GetStatusString(PrimaryCode);
        }
    }
}
