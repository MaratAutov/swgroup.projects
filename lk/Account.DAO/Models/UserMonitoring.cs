﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("UserMonitoring")]
    public class UserMonitoring
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        [StringLength(32)]
        public string Login { get; set; }

        [StringLength(32)]
        public string Password { get; set; }

        [Default(typeof(DateTime), "CURRENT_TIMESTAMP")]
        public DateTime CreateDate { get; set; }

        public bool IsActive { get; set; }

        public bool IsAdmin { get; set; }

        [StringLength(255)]
        public string FIO { get; set; }

        [StringLength(255)]
        public string RegionUser { get; set; }        
    }
}
