﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("Document")]
    public class Document
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        [References(typeof(Blob))]
        public int BlobID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [References(typeof(UserAccount))]
        public int OwnerID { get; set; }

        public DateTime UploadTime { get; set; }

        public DateTime? ReadTime { get; set; }


        [Ignore]
        public bool Existing { get; set; }
    }

    [Alias("DocumentLink")]
    public class DocumentLink
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        [References(typeof(Document))]
        public int DocumentID { get; set; }

        [References(typeof(UserAccount))]
        public int ReceiverID { get; set; }
    }
}
