﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("Blob")]
    public class Blob
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        public byte[] Body { get; set; }

        [StringLength(32)]
        public string Hash { get; set; }
    }
}
