﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("MailMessage")]
    public class MailMessage
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        public string Url { get; set; }
        public string Subject { get; set; }
        public string MailFrom { get; set; }
        public string MailTo { get; set; }
        
        public DateTime CreateAt { get; set; }
        
        [References(typeof(Package))]
        public int PackageID { get; set; }

        public string Info { get; set; }
    }
}
