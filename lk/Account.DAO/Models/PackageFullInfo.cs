﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;


namespace Account.DAO
{
    [Alias("PackageFullInfo")]    
    public class PackageFullInfo
    {
        // Package Part

        public int ID { get; set; }

        public int BlobID { get; set; }

        public int ReportID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public DateTime UploadTime { get; set; }

        public int OwnerID { get; set; }

        public string Owner { get; set; }

        public string SignatureType { get; set; }

        // Report Part

        [StringLength(255)]
        public string IncomingNumber { get; set; }

        public DateTime? IncomingDate { get; set; }

        [StringLength(255)]
        public string SedNumber { get; set; }

        public DateTime? SedDate { get; set; }

        public string ExternalID { get; set; }

        public string Version { get; set; }

        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string OrgShortName { get; set; }

        public string OrgFullName { get; set; }

        public string Subject { get; set; }

        public string Address { get; set; }

        public string INN { get; set; }

        public string OGRN { get; set; }

        public string Year { get; set; }

        public string Quartal { get; set; }

        public string Month { get; set; }

        public DateTime? Date { get; set; }

        public string Model { get; set; }

        public string Type { get; set; }

        public int? DocTypeCode { get; set; }

        public string DocTypeName { get; set; }

        public int? OrgTypeCode { get; set; }

        public string OrgTypeName { get; set; }

        public int? CurrentStatusID { get; set; }

        public int? InitialStatusID { get; set; }

        public int CurrentStatusCode { get; set; }

        public string CurrentStatusInfo { get; set; }

        public int InitialStatusCode { get; set; }

        public string InitialStatusInfo { get; set; }

        public DateTime? InitialStatusAssignmentTime { get; set; }
        public DateTime? CurrentStatusAssignmentTime { get; set; }
        
        [StringLength(512)]
        public string RoName { get; set; }

        [StringLength(4)]
        public string RoType { get; set; }

        public bool IsRoReport { get; set; }

        public string CurrentStatusException { get; set; }

        public bool? IsImported { get; set; }

        [Ignore]
        public string CurrentStatus {
            get
            {
                return StatusService.GetLkStatusString(
                    CurrentStatusCode,
                    true
                );
            }
        }

        [Ignore]
        public string CurrentStatusDetail
        {
            get
            {
                return ((PrimaryStatus)CurrentStatusCode).ToString();
            }
        }

        [Ignore]
        public string InitialStatus {
            get
            {
                return StatusService.GetLkStatusString(
                    InitialStatusCode,
                    true
                );                
            }
        }

        [Ignore]
        public string InitialStatusDetail
        {
            get
            {
                return ((PrimaryStatus)InitialStatusCode).ToString();
            }
        }

        IList<MailMessage> _mailMessages;
        [Ignore]
        public IList<MailMessage> MailMessages
        {
            get
            {
                if (_mailMessages == null)
                {
                    _mailMessages = MailMessageService.GetAllMessageByPacketID(ID);
                }
                return _mailMessages;
            }
        }

        [Ignore]
        public IEnumerable<MailMessage> ToSedMailMessages
        {
            get
            {
                return MailMessages.Where(x => x.Info == "To SED");
            }
        }

        [Ignore]
        public IEnumerable<MailMessage> FromSedMailMessages
        {
            get
            {
                return MailMessages.Where(x => x.Info == "From SED");
            }
        }
    }
}
