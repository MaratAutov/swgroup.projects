﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("News")]
    public class News
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        public string Body { get; set; }

        public DateTime DatePosted { get; set; }

        public DateTime DateLastModified { get; set; }

        public int UserPosted { get; set; }

        public int UserLastModified { get; set; }
    }

    [Alias("NewsTiming")]
    public class NewsTiming
    {
        public int Year  { get; set; }
        public int Month { get; set; }
    }
}
