﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;


namespace Account.DAO
{
    [Alias("Package")]    
    public class PackageShortInfo
    {
        // Package Part

        //public int ID { get; set; }

        //public int BlobID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }
        public DateTime UploadTime { get; set; }

        public string OwnerLogin { get; set; }
        public string OwnerINN { get; set; }
        public string OwnerCompany { get; set; }
        
        public int Year { get; set; }
        public int Quarter { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
    }
}
