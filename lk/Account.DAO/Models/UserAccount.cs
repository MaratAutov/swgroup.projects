﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("UserAccount")]
    public class UserAccount
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        [StringLength(32)]
        public string Login { get; set; }

        [StringLength(32)]
        public string Password { get; set; }

        [Default(typeof(DateTime), "CURRENT_TIMESTAMP")]
        public DateTime CreateDate { get; set; }

        public bool IsActive { get; set; }

        public bool IsAdmin { get; set; }

        [StringLength(255)]
        public string FIO { get; set; }

        [StringLength(255)]
        public string Company { get; set; }

        [StringLength(10)]
        public string INN { get; set; }

        [StringLength(13)]
        public string OGRN { get; set; }

        [StringLength(32)]
        public string Phone { get; set; }

        [Default(typeof(bool), "False")]
        public bool EnableNotification { get; set; }

        //[References(XCertificate)]
        public int? CertificateID { get; set; }   
        
    }
}
