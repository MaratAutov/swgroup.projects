﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO.SED
{
    [Alias("SED_ModelInfo")]
    public class ModelInfo
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        [StringLength(50)]
        public string Model { get; set; }

        [StringLength(255)]
        public string Attr { get; set; }

        public string Value { get; set; }
        public string ModelVersion { get; set; }

        public bool IsStatic { get; set; }
    }
}
