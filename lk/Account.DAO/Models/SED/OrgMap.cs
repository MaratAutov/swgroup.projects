﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO.SED
{
    [Alias("SED_OrgMap")]
    public class OrgMap
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }
        
        [StringLength(255)]
        public string Type { get; set; }
        
        [StringLength(50)]
        public string Code { get; set; }
        
        public string Name { get; set; }
    }
}
