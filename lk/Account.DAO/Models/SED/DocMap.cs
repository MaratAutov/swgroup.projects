﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO.SED
{
    [Alias("SED_DocMap")]
    public class DocMap
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }
        
        [StringLength(255)]
        public string Type { get; set; }
        
        [StringLength(50)]
        public string Code { get; set; }
        
        public string Name { get; set; }

        public int signminvalue { get; set; }

        public int signmaxvalue { get; set; }
    }
}
