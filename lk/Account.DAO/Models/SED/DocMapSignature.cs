﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO.SED
{
    [Alias("SED_DocMapSignature")]
    public class DocMapSignature
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        [StringLength(255)]
        public string Attr { get; set; }

        [StringLength(255)]
        public string ModelName { get; set; }

        [StringLength(255)]
        public string ModelVersion { get; set; }
    }
}
