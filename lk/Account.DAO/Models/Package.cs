﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("Package")]
    public class Package
    {
        [AutoIncrement]
        [PrimaryKey]
        public int ID { get; set; }

        [References(typeof(Blob))]
        public int BlobID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [References(typeof(UserAccount))]
        public int OwnerID { get; set; }

        [References(typeof(ReportInfo))]
        public int ReportID { get; set; }

        public DateTime UploadTime { get; set; }

        public DateTime LastChangeTime { get; set; }

        [References(typeof(Status))]
        public int? CurrentStatusID { get; set; }

        [References(typeof(Status))]
        public int? InitialStatusID { get; set; }

        [StringLength(25)]
        public string SignatureType { get; set; }

        public int? SeldonFSFRStatus { get; set; }
        public string SeldonFSFRError { get; set; }

        public string PortalID { get; set; }
        public string KiasID { get; set; }
        public string notifytimeid { get; set; }

        public bool? IsImported { get; set; }

        [Ignore]
        public string CurrentStatus
        {
            get
            {
                try
                {
                    return StatusService.Get(CurrentStatusID.Value).ToString();
                }
                catch
                {
                    return null;
                }
            }
        }

        [Ignore]
        public string InitialStatus
        {
            get
            {
                try
                {
                    return StatusService.Get(InitialStatusID.Value).ToString();
                }
                catch
                {
                    return null;
                }
            }
        }

        [Ignore]
        public bool Existing { get; set; }


        private ReportInfo _info;
        [Ignore]
        public ReportInfo Info
        {
            get
            {
                if (_info == null)
                    _info = PackageService.GetReportInfo(ReportID);
                return _info;
            }
            set
            {
                _info = value;
            }
        }
    }
}
