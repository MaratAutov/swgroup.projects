﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("UserRegisterInfo")] 
    public class UserRegisterInfo
    {
        public int ID { get; set; }
        public string Company { get; set; }
        public string INN { get; set; }
        public int Count { get; set; }
    }
}
