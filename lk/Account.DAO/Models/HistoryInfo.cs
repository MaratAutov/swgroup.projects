﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Account.DAO.Models
{
    public class HistoryInfo
    {
        public PackageFullInfo CurrentPackage { get; set; }
        public List<PackageFullInfo> AllPackages { get; set; }
        public string ExMessage { get; set; }

        public HistoryInfo(PackageFullInfo current, List<PackageFullInfo> all)
        {
            CurrentPackage = current;
            AllPackages = all;
        }

        public HistoryInfo(List<PackageFullInfo> all)
        {
            AllPackages = all;
        }

        public HistoryInfo(PackageFullInfo current, string exMessage)
        {
            CurrentPackage = current;
            ExMessage = exMessage;
        }
    }
}
