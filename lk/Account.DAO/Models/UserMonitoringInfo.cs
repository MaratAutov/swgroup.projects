﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using ServiceStack.DataAnnotations;

namespace Account.DAO
{
    [Alias("UserMonitoringInfo")] 
    public class UserMonitoringInfo
    {
        public int ID { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FIO { get; set; }
        public string Region { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
