﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Account.DAO.Models
{
    public class OrganizationHistoryInfo
    {
        public string Year { get; set; }
        public string Quartal { get; set; }
        public string Month { get; set; }
        public DateTime? Date { get; set; }
        public string Type { get; set; }
        public string INN { get; set; }
        public int? DocTypeCode { get; set; }
        public string count { get; set; }
        public string report_type { get; set; }
    }
}
