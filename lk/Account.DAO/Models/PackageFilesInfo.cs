﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Account.DAO
{
    public class PackageFilesInfo
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string DocumentUrl { get; set; }
        public string DocumentSignUrl { get; set; }
        public string NotificationUrl { get; set; }
        public string NotificationSignUrl { get; set; }
        public string ProcessedDocumentUrl { get; set; }
    }
}
