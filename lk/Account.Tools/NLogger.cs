﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using NLog;
using NLog.Targets;
using NLog.Config;


namespace Account.Tools
{
    public static class NLogger
    {
        private static Dictionary<string, Logger> loggers;

        static NLogger()
        {            
            loggers = new Dictionary<string,Logger>();
        }

        public static string ExceptionInfo(Exception exc)
        {
            string info = string.Format(
                "{1}{0}{2}",
                Environment.NewLine,
                exc.Message,
                exc.StackTrace
            );
            if (exc.InnerException != null) {
                info += string.Format(
                    "{0}{1}{0}{2}",
                    Environment.NewLine,
                    exc.InnerException.Message,
                    exc.InnerException.StackTrace
                );
            }
            return info;
        }

        public static Logger GetLog(string name, string logfile=null)
        {
            if (loggers.ContainsKey(name))
                return loggers[name];
            
            var config = new LoggingConfiguration();
            FileTarget fileTarget = new FileTarget()
            {
                FileName = logfile ?? "${basedir}/debug.log",
                Layout = "${longdate} :: [${threadid}] ${logger} :: ${message}"
            };            
            config.AddTarget("file", fileTarget);
            LoggingRule rule = new LoggingRule(
                "*", LogLevel.Debug, fileTarget
            );
            config.LoggingRules.Add(rule);

            FileTarget errorFileTarget = new FileTarget()
            {
                FileName = logfile ?? "${basedir}/debug.log",
                Layout = "${longdate} :: [${threadid}] ${logger} :: ${exception:format=Type,Message:maxInnerExceptionLevel=1}${newline}${stacktrace}"
            };

            config.AddTarget("errorFile", errorFileTarget);

            LoggingRule errorRule = new LoggingRule(
                "*", LogLevel.Error, errorFileTarget
            );

            config.LoggingRules.Add(errorRule);
            LogManager.Configuration = config;

            var logger = LogManager.GetLogger(name);
            loggers.Add(name, logger);

            return logger;
        }
    }
}
