﻿<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:av="http://www.it.ru/Schemas/Avior/ПУРЦБ"  version="1.0">
<xsl:template match="/">
<av:Уведомление xmlns:xserializer="http://www.it.ru/Schemas/Avior/XSerializer" xserializer:id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:av="http://www.it.ru/Schemas/Avior/ПУРЦБ" xsi:schemaLocation="http://www.it.ru/Schemas/Avior/ПУРЦБ ПУРЦБ.xsd">
  <av:ПостоянныйИдентификаторПакета>
    <xsl:value-of select="//ID"/>
  </av:ПостоянныйИдентификаторПакета>
  <av:НаименованиеПакета>
    <xsl:value-of select="//DocTypeName" />
  </av:НаименованиеПакета>
  <av:ВходящийНомерПрисвоенныйПакету>
    <xsl:value-of select="//SedNumber" />
  </av:ВходящийНомерПрисвоенныйПакету>
  <av:ДатаПрисвоенияВходящегоНомера>
    <xsl:value-of select="//SedDate" />
  </av:ДатаПрисвоенияВходящегоНомера>
  <av:ИсходящийНомерПакетаАдресанта>
    <xsl:value-of select="//IncomingNumber" />
  </av:ИсходящийНомерПакетаАдресанта>
  <av:ДатаПрисвоенияИсходящегоНомера>
    <xsl:value-of select="//IncomingDate" />
  </av:ДатаПрисвоенияИсходящегоНомера>
  <av:КодВозврата>
    <xsl:value-of select="//ReturnCode" />
  </av:КодВозврата>
  <av:ДатаИзменения>
    <xsl:value-of select="//StatusAssignmentTime" />
  </av:ДатаИзменения>
  <av:ИннПрофучастника>
    <xsl:value-of select="//INN" />
  </av:ИннПрофучастника>
  <av:ОгрнПрофучастника>
    <xsl:value-of select="//OGRN" />
  </av:ОгрнПрофучастника>
  <av:КраткоеНаименованиеРегиональногоОтделенияФсфрРоссии>
    <xsl:value-of select="//RoName" />
  </av:КраткоеНаименованиеРегиональногоОтделенияФсфрРоссии>
  <av:ПодробныйКомментарий>
    <xsl:value-of select="//ErrorDescription" />
  </av:ПодробныйКомментарий>
</av:Уведомление>
</xsl:template>
</xsl:stylesheet>