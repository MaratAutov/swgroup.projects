﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Account.Tools
{
    /// <summary>
    /// Interface for extraction and data verification
    /// </summary>
    public interface IReportPackage
    {
        void Load(byte[] data);
        string GetGzippedBase64Body();

        byte[] ReportBody { get; }
        byte[] ReportSign { get; }
        byte[] ReportData { get; }
        byte[] SignedData { get; }

        string ReportFileName { get; }
        string SignFileName { get; }

        string SignerExt { get; }
    }
}
