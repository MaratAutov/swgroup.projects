﻿using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using ICSharpCode.SharpZipLib.Zip;
using Progresoft.Accounts.ElectroSignature;


namespace Account.Tools
{
    public class ZipReportPackage: IReportPackage
    {
        byte[] _reportData = null;
        byte[] _reportBody = null;
        byte[] _signedData = null;
        byte[] _reportSign = null;
        string _signModExt = null;
        string _reportName = null;
        string _signName   = null;

        static readonly List<string> _supportedExtensions = new List<string>() {
            ".zip", ""
        };

        public ZipReportPackage(byte[] data)
        {
            Load(data);
        }

        /// <summary>
        /// Extracts report signature and report body parts from zip archive
        /// </summary>
        /// <param name="stream">zip archive stream</param>
        public void Load(byte[] data)
        {
            // ZipFile closes stream, so we don't need 'using (...)'
            MemoryStream stream = new MemoryStream(data);

            _reportData = new byte[stream.Length];
            stream.Read(_reportData, 0, _reportData.Length);
            stream.Position = 0;
            try
            {
                using (ZipFile zip = new ZipFile(stream))
                {
                    if (zip.Count < 2)
                        throw new PackageCorruptedException(
                            "Архив должен содержать минимум 2 файла в корневой " +
                            "директории архива: отчет и цифровую подпись"
                        );

                    foreach (ZipEntry e in zip)
                    {
                        if (!e.IsFile)
                            continue;

                        String ext = Path.GetExtension(e.Name).ToLower();
                        if (!SignatureEngine.Instance.Supports(ext))
                            continue;

                        String repName = Path.GetFileNameWithoutExtension(e.Name);
                        var newRepName = Regex.Replace(repName, "^((?:.+)zip)\\d+$", "$1");
                        ZipEntry r = zip.GetEntry(newRepName);
                        if (r == null)
                            throw new ReportNotFoundException(
                                "Имя файла цифровой подписи не совпадает " +
                                "с именем файла отчета"
                            );

                        try
                        {
                            _signedData = Decompress(zip, r);
                            if (r.Name.EndsWith(".txt"))
                            {
                                using (MemoryStream rStream = new MemoryStream(_signedData))
                                {
                                    MultipartParser mParser = new MultipartParser(
                                        rStream, Encoding.GetEncoding(1251)
                                    );
                                    if (mParser.Success)
                                    {
                                        _reportBody = mParser.FileContents;
                                        _reportName = mParser.Filename;
                                    }
                                    else
                                    {
                                        _reportName = r.Name;
                                    }
                                }
                            }
                            else
                            {
                                _reportName = r.Name;
                            }
                            if (_reportName.ToLower().EndsWith(".zip"))
                                _reportBody = Unzip(_signedData);
                            else
                                _reportBody = _signedData;
                            _reportSign = Decompress(zip, e);
                            _signModExt = ext;
                            _signName = e.Name;
                        }
                        catch (Exception exc)
                        {
                            throw new InvalidPackageException(
                                "Произошел непредвиденный сбой чтения архива",
                                exc
                            );
                        }
                    }
                    if (_reportSign == null)
                        throw new SignatureNotFoundException(
                            "Цифровая подпись не найдена"
                        );
                }
            }
            catch (Exception ex)
            {
                throw new InvalidPackageException("Невозможно открыть архив", ex);
            }
        }

        /// <summary>
        /// Checks if file can be represented by Zip package class
        /// </summary>
        /// <param name="fileName">report file name</param>
        /// <returns>'true' if file can be opened otherwise 'false'</returns>
        public static bool CanProcess(string fileName)
        {
            return _supportedExtensions.Contains(
                Path.GetExtension(fileName)
            );
        }

        public string GetGzippedBase64Body()
        {
            using (var compressedStream = new MemoryStream())
            using (var zipStream =
                new GZipStream(compressedStream, CompressionMode.Compress))
            {
                zipStream.Write(_reportBody, 0, _reportBody.Length);
                zipStream.Close();
                return Convert.ToBase64String(
                    compressedStream.ToArray()
                );
            }
        }

        /// <summary>
        /// Report body
        /// </summary>
        /// <returns>byte array of report data</returns>
        public byte[] ReportBody
        {
            get
            {
                return _reportBody;
            }
        }

        /// <summary>
        /// Report signature
        /// </summary>
        /// <returns>byte array of signature data</returns>
        public byte[] ReportSign
        {
            get
            {
                return _reportSign;
            }
        }

        /// <summary>
        /// Full report data
        /// </summary>
        /// <returns>byte array of report data</returns>
        public byte[] ReportData
        {
            get
            {
                return _reportData;
            }
        }


        public string SignerExt
        {
            get
            {
                return _signModExt;
            }
        }

        public string ReportFileName
        {
            get
            {
                return _reportName;
            }
        }

        public string SignFileName
        {
            get
            {
                return _signName;
            }
        }

        public byte[] SignedData
        {
            get
            {
                return _signedData;
            }
        }

        /// <summary>
        /// Gets decompressed file data as byte array
        /// </summary>
        /// <param name="file">zip archive</param>
        /// <param name="entry">zip file entry</param>
        /// <returns></returns>
        private static byte[] Decompress(ZipFile file, ZipEntry entry)
        {
            using (Stream s = file.GetInputStream(entry))
            using (BinaryReader br = new BinaryReader(s))
                return br.ReadBytes((int)entry.Size);
        }

        private static byte[] Unzip(byte [] data)
        {
            using (var stream = new MemoryStream(data))
            using (ZipFile zip = new ZipFile(stream))
            {
                foreach (ZipEntry e in zip)
                {
                    return Decompress(zip, e);
                }
            }
            return null;
        }
    }
}
