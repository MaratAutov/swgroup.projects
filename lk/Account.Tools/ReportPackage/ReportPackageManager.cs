﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Account.Tools
{
    public static class ReportPackageManager
    {
        /// <summary>
        /// Loads archive acqiring report and signature files as
        /// byte arrays
        /// </summary>
        /// <exception cref="InvalidPackageException"></exception>
        /// <exception cref="ConfigurationErrorsException"></exception>
        /// <param name="fileName"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IReportPackage Load(string fileName, byte[] data)
        {
            return new ZipReportPackage(data);
        }
    }
}
