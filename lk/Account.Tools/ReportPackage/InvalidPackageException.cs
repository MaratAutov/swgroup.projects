﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Account.DAO;

namespace Account.Tools
{
    public class InvalidPackageException : Exception 
    {
        public virtual SecondaryStatus Status
        {
            get
            {
                return SecondaryStatus.ERROR_CANT_UNCOMPRESS_ZIP;
            }
        }
        
        public InvalidPackageException(string msg) : base(msg) {
        }

        public InvalidPackageException(string msg, Exception inner): base(msg, inner) {
        }
    }

    public class PackageCorruptedException : InvalidPackageException
    {
        public override SecondaryStatus Status
        {
            get
            {
                return SecondaryStatus.ERROR_UNEXPECTED_FILES_COUNT;
            }
        }
        
        public PackageCorruptedException(string msg) : base(msg) {
        }
    }

    public class SignatureNotFoundException : InvalidPackageException
    {
        public override SecondaryStatus Status
        {
            get
            {
                return SecondaryStatus.SIGNATURE_FILE_NOT_FOUND;
            }
        }
        
        public SignatureNotFoundException(string msg) : base(msg) {
        }
    }

    public class ReportNotFoundException : InvalidPackageException
    {
        public override SecondaryStatus Status
        {
            get
            {
                return SecondaryStatus.REPORT_FILE_NOT_FOUND;
            }
        }
        
        public ReportNotFoundException(string msg) : base(msg) {
        }
    }
}
