﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using System.Configuration;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using ICSharpCode.SharpZipLib.Zip;
using Progresoft.Accounts.ElectroSignature;
using NLog;
using System.Web.Services;
using System.Web.Security;
using System.Web.Script.Serialization;

using Account.DAO;
using Account.DAO.SED;


namespace Account.Tools {

/// <summary>
/// Summary description for PackageManager
/// </summary>
public class PackageManager
{
    public static Logger log;
    /// <summary>
    /// Root directory for Xsd model templates used for XTDD validation
    /// </summary>
    private static string _xsdRootDir;

    /// <summary>
    /// Root directory for storing uploaded reports that passed validation
    /// </summary>
    private static string _archiveDir;

    public IReportPackage ReportPackage;

    public string Version;
    public string ExternalId;
    public string Model;
    public string Type;
    public string Signature;
    
    public string ErrorMessage;
    public SecondaryStatus ErrorStatus;
    public string ErrorException;
    public string xsdSchemeFinal;

    static PackageManager()
    {
        string logFile = ConfigurationManager.AppSettings["LogFile"];
        log = NLogger.GetLog("PackageManager", logFile);

        _xsdRootDir = Path.GetFullPath(LoadConf("XsdRootDir"));
        _CheckExistense(
            _xsdRootDir,
            true,
            "Неверно задана или отсутствует конфигурация пути файлов XSD\n{0}",
            _xsdRootDir
        );

        _archiveDir = LoadConf("ArchiveDir");        
    }

    /// <summary>
    /// Verify uploaded package
    /// </summary>
    /// <param name="fName">package file name with extension</param>
    /// <param name="stream">package data stream</param>
    public bool Open(string fName, byte[] data)
    {
        log.Debug(
            "Начали открытие пакета '{0}' [{1} bytes]",
            fName,
            data.Length
        );
        try
        {
            ReportPackage = ReportPackageManager.Load(fName, data);
            if (ReportPackage.ReportBody == null || ReportPackage.ReportBody.Length == 0)
            {
                throw new ReportNotFoundException("Не найден файл отчета");
            }
            log.Debug(
                "{0} :: Пакет успешно открыт как :: {1} ",
                fName,
                ReportPackage.GetType().Name
            );
            return true;
        }
        catch (InvalidPackageException exc)
        {
            ErrorStatus = exc.Status;
            log.ErrorException(
                string.Format(
                    "{0} :: Ошибка открытия пакета",
                    fName
                ),
                exc
            );
            ErrorMessage = exc.Message;
            ErrorException = NLogger.ExceptionInfo(exc);
            return false;
        }
        catch (Exception exc)
        {
            ErrorStatus = SecondaryStatus.ERROR_CANT_UNCOMPRESS_ZIP;
            log.ErrorException(
                string.Format(
                    "{0} :: Ошибка открытия пакета",
                    fName
                ),
                exc
            );
            ErrorMessage = exc.Message;
            ErrorException = NLogger.ExceptionInfo(exc);
            return false;
        }
    }

    private string GetUserData(int user_id)
    {
        string user_data = string.Empty;
        UserAccount acc;

        acc = UserService.Get(user_id); 
            if (acc != null)
            {
                user_data = string.Format("{0}({1})", acc.Company, acc.INN);
            }
        
        return user_data;
    }

#warning отправка ошибочных сертификатов нельзя их декодировать без обработки ексепшена
    public void SendMailVerifySignatureError(byte[] body, byte[] signature,int user_id)
    {
        string body_mail = string.Empty;
        string subject_mail = string.Empty;


        if (body == null)
            throw new ArgumentNullException("body");
        if (signature == null)
            throw new ArgumentNullException("signature");

        SignedCms signedCms = new SignedCms(new ContentInfo(body), true);        
        signedCms.Decode(signature);
        if (signedCms.SignerInfos.Count > 0)
        {
            foreach (SignerInfo current in signedCms.SignerInfos)
            {
                if (current.Certificate != null)
                {

                    body_mail = string.Format("Подписан: {0}<br><br>", current.Certificate.SubjectName.Name);                   
                   
                        try
                        {
                            body_mail += string.Format("<b>Содержимое сертификата:</b> {0}<br><br>", current.Certificate.ToString(false));
                        }
                        catch (Exception e)
                        {
                            body_mail += string.Format("Не удалось вывести содержимое сертификата {0}<br><br>", e);
                        }                        
                }
                else
                {
                    body_mail = "Подпись не содержит сертификата<br><br>";    
              
                }               
            }
        }

        subject_mail = GetUserData(user_id);
        
        try
        {
            if (ConfigurationManager.AppSettings["VerifySignatureEmail"] != null)
            {
                using (var cl = new MailService.MailServiceSoapClient())
                {
                    cl.EnqueueForSend(new MailService.OutMessage()
                    {
                        From = ConfigurationManager.AppSettings["VerifySignatureEmail"],
                        To = ConfigurationManager.AppSettings["VerifySignatureEmail"],
                        Subject = subject_mail,
                        Body = string.Format("<b>ЭЦП:</b> {0}<br><br> <b>Ошибка:</b> {1}", body_mail, ErrorMessage),
                        IsHtml = true
                    });
                }
            }
        }
        catch (Exception exc)
        {
            string msg = "Не удалось сохранить отправить письмо: {0}";                
          
            throw new Exception(msg, exc);
        }
    }

    public VerificationResult VerifySignature()
    {
        return VerifySignature(-1);
    }



    public VerificationResult VerifySignature(int user_id)
    {
        VerificationResult res = SignatureEngine.Instance.Verify(
            ReportPackage.SignedData,
            ReportPackage.ReportSign,
            ReportPackage.SignerExt
        );
        Signature = res.SingatureType;

        if (res.Status != SignatureStatus.Valid) {
            ErrorMessage = res.ErrorMessage;
            try
            {
                SendMailVerifySignatureError(ReportPackage.SignedData, ReportPackage.ReportSign, user_id);
            }
            catch (Exception ex)
            {
                log.DebugException("Eroor send mail notification about certificate error", ex);
            }
        }

        return res;        
    }

    string DocumentCatalogName = "document";
    /// <summary>
    /// Store package in archive
    /// </summary>
    /// <exception>ConfigurationErrorsException</exception>
    /// <param name="uuid">uploaded package processing ID</param>
    public string Store(string uuid)
    {
        log.Debug(
            "{0} :: Начали сохранение пакета :: {1}",
            ReportPackage.ReportFileName,
            uuid
        );

        string archiveRoot = Path.GetFullPath(_archiveDir);
        DateTime today = DateTime.Now;
        
        string targetDir = Path.Combine(
            archiveRoot,
            DocumentCatalogName,
            today.Year.ToString(),
            today.Month.ToString(),
            today.Day.ToString(),
            uuid
        );
        if (!Directory.Exists(targetDir))
        {
            try
            {
                Directory.CreateDirectory(targetDir);
            }
            catch( Exception exc )
            {
                string msg = string.Format(
                    "Не удалось создать архивную директорию: {0}",
                    targetDir
                );
                throw new ConfigurationErrorsException(msg, exc);
            }
        }

        string targetFile = Path.Combine(
            targetDir,
            ReportPackage.ReportFileName + ".bkp"
        );
        try
        {
            using (FileStream fStream = File.Create(targetFile)) {
                fStream.Write(
                    ReportPackage.ReportData,
                    0,
                    ReportPackage.ReportData.Length
                );
                log.Debug(
                    "{0} :: Пакет успешно сохранен на диск :: {1}",
                    ReportPackage.ReportFileName,
                    uuid
                );
            }
            
            return targetFile;
        }
        catch (Exception exc)
        {
            string msg = string.Format(
                "Не удалось сохранить файл в архиве: {0}",
                targetFile
            );
            throw new ConfigurationErrorsException(msg, exc);
        }
        
    }

    public void StoreSedResponce(string data)
    {

    }

    /// <summary>
    /// Verification for XTDD validness using XSD scheme
    /// Scheme is choosed using XTDD root element attributes:
    /// "appVersion" - version of supported reports
    /// "NamespaceURI" - should end with one of the following values:
    ///     АЛ / ЗПУ / ЛКИ / ПУРЦБ / Регистраторы / УММ / УРКИ / УСИО
    /// Root element name is used to choose concrete XSD scheme file
    /// 
    /// E.g. following root element:
    ///     <av:ПакетСООУРКИ031 ... appVersion="2.16.2" xmlns:av="http://example/УРКИ">
    /// will be validated with:
    ///     ...\2.16.2\УРКИ\УРКИ_ПакетСООУРКИ031.xsd
    /// 
    /// Raises:
    ///     ConfigurationErrorsException
    ///     InvalidPackageException
    /// </summary>
    public bool VerifyXtdd()
    {
        log.Debug(
            "{0} :: Начали верификацию XTDD",
            ReportPackage.ReportFileName
        );

        using (var stream = new MemoryStream(ReportPackage.ReportBody)) {
            string xsdFolder    = null;
            string xsdScheme    = null;
            string tgtNamespace = null;

            try {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stream);
                XmlNode node = xmlDoc.DocumentElement;

                tgtNamespace = node.NamespaceURI;
                xsdFolder    = tgtNamespace.Split('/').Last();
                xsdScheme    = node.LocalName;
                //ExternalId   = _GetAttributeValue(node, "externalId", true);
                ExternalId = Guid.NewGuid().ToString();
                Version   = _GetAttributeValue(node, "appVersion");
                log.Debug(
                    "{0} :: Readed XTDD root element :: [appVersion={1}, " +
                    "xsdFolder={2}, xsdScheme={3}]",
                    ReportPackage.ReportFileName,
                    Version,
                    xsdFolder,
                    xsdScheme
                );

                string xsdVersionRoot = Path.Combine(_xsdRootDir, Version);
                _CheckExistense(
                    xsdVersionRoot,
                    false,
                    "Заданная версия отчета не поддерживается: {0}",
                    Version
                );

                string xsdSchemeRoot = Path.Combine(
                    xsdVersionRoot, xsdFolder
                );
                _CheckExistense(
                    xsdSchemeRoot,
                    false,
                    "Задано неизвестное пространство имен в XTDD файле: {0} ",
                    xsdSchemeRoot
                );
                Model = xsdFolder;

                xsdSchemeFinal = Path.Combine(
                    xsdSchemeRoot,
                    string.Format("{0}_{1}.xsd", xsdFolder, xsdScheme)
                );
                _CheckExistense(
                    xsdSchemeFinal,
                    false,
                    "Задан неизвестный тип отчета в XTDD файле: {0} ",
                    string.Format("{0}_{1}", xsdFolder, xsdScheme)
                );
                Type = xsdScheme;

                xmlDoc.Schemas.Add(tgtNamespace, xsdSchemeFinal);
                xmlDoc.Validate(
                    new ValidationEventHandler(ValidationCallBack),
                    node
                );

                log.Debug("{0} :: ПАКЕТ ВАЛИДЕН", ReportPackage.ReportFileName);
                return true;
            }
            catch (Exception exc)
            {
                log.ErrorException(
                    string.Format(
                        "{0} :: Ошибка валидации",
                        ReportPackage.ReportFileName
                    ),
                    exc
                );
                ErrorMessage = exc.Message;
                ErrorException = NLogger.ExceptionInfo(exc);
                return false;
            }
        }
    }



    public List<string> GetSignatureList(byte[] body, byte[] signature)
    {
        List<string> list = new List<string> { };
        SignedCms signedCms = new SignedCms(new ContentInfo(body), true);
        signedCms.Decode(signature);
        if (signedCms.SignerInfos.Count > 0)
        {
            foreach (SignerInfo current in signedCms.SignerInfos)
            {
                if (current.Certificate != null)
                {                   
                    string[] sign = current.Certificate.SubjectName.Name.Split(',');

                    foreach (string s in sign)
                    {
                        if (s.Trim().StartsWith("CN="))
                        {     
                            if (s.Length > 3)
                            list.Add(s.Trim().Remove(0, 3));
                        }
                    }
                    
                }
            }
        }
        return list;
    }

    public bool VerifyFIOSignature()
    {        
        List<DocMapSignature> DocMapSignatureCache = null;
        DocMap DocMapCache = null;
        List<string> list_sign = new List<string>{};
        string version = string.Empty;
        // находим версию отчета

        using (var stream = new MemoryStream(ReportPackage.ReportBody))
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stream);
                XmlNode node = xmlDoc.DocumentElement;                
                version = _GetAttributeValue(node, "appVersion");
            }
            catch (Exception exc)
            {
                log.ErrorException(
                    string.Format(
                        "{0} :: Ошибка проверки ЭЦП с ФИО: не найдена версия пакета",
                        ReportPackage.ReportFileName
                    ),
                    exc
                );                
                ErrorMessage = exc.Message;
                ErrorException = NLogger.ExceptionInfo(exc);
                return false;
            }
        }

        list_sign = GetSignatureList(ReportPackage.SignedData, ReportPackage.ReportSign);
        using (var stream = new MemoryStream(ReportPackage.ReportBody))
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stream);
                XmlElement root = xmlDoc.DocumentElement;
                DocMapSignatureCache = SEDMappingService.GetDocMapSignature().Where(m => m.ModelName == root.LocalName && m.ModelVersion == version).ToList();
                DocMapCache = SEDMappingService.GetDocMapping().Where(m => m.Type == root.LocalName).FirstOrDefault();

                int max = 0;
                if (DocMapCache != null && DocMapCache.signmaxvalue != 0)
                    max = DocMapCache.signmaxvalue;
                else max = 1000;

                if ((list_sign.Count < max + 1) && (list_sign.Count > 0))
                    {
                        log.Debug(
                   "{0} :: Начали проверку ФИО с ЭЦП",
                    ReportPackage.ReportFileName);
                        if (DocMapSignatureCache.Count > 0)
                        {
                            XmlHelper xml = new XmlHelper(ReportPackage.ReportBody);
                            
                            foreach (string list in list_sign)
                            {
                                bool exist = false;
                                foreach (DocMapSignature map in DocMapSignatureCache)
                                {                                    
                                    string it = xml.GetValue(map.Attr);
                                    if (it != null)
                                    {
                                        if (list == it.ToString())
                                            exist = true;
                                    }
                                }

                                if (!exist)
                                {
                                    ErrorMessage = string.Format("{0} - подпись не найдена",list);
                                    ErrorException = string.Format("{0} - подпись не найдена", list);
                                    return false;
                                }
                            }

                            log.Debug("{0} :: ЭЦП совпадают с подписями", ReportPackage.ReportFileName);
                            return true;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        log.Debug("{0} :: Неверное количество ЭЦП", ReportPackage.ReportFileName);
                        ErrorMessage = "Неверное количество ЭЦП";
                        ErrorException = "Неверное количество ЭЦП";
                        return false;
                    }                
                
            }
            catch (Exception exc)
            {
                log.ErrorException(
                    string.Format(
                        "{0} :: Ошибка проверки ЭЦП с ФИО",
                        ReportPackage.ReportFileName
                    ),
                    exc
                );
                ErrorMessage = exc.Message;
                ErrorException = NLogger.ExceptionInfo(exc);
                return false;
            }            
        }       
    }  
                
       
        

    /// <summary>
    /// Callback for XTDD validation
    /// 
    /// Raises:
    ///     InvalidPackageException
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    private static void ValidationCallBack(object sender, ValidationEventArgs args)
    {
        if( args.Severity == XmlSeverityType.Error ) {            
            throw new InvalidPackageException(args.Message);
        }
    }

    /// <summary>
    /// Gets XmlNode Attribute value or raise exception
    /// 
    /// Raises:
    ///     InvalidPackageException
    /// </summary>
    /// <param name="node">xml node to search in</param>
    /// <param name="name">attribute name</param>
    /// <returns>@string attribute value</returns>
    private static string _GetAttributeValue(XmlNode node, string name, bool safe=false)
    {
        if (node.Attributes[name] == null)
        {
            if (!safe)
                throw new InvalidPackageException(string.Format(
                    "Переменная '{0}' не найдена в корневом элементе",
                    name
                ));
            return null;
        }
        return node.Attributes[name].Value;
    }

    /// <summary>
    /// Check for file or directory existence
    /// 
    /// Raises:
    ///     ConfigurationErrorsException
    ///     InvalidPackageException
    /// </summary>
    /// <param name="path">file or directory path to chech</param>
    /// <param name="isConfigDir">
    /// if isConfigDir is 'true' ConfigurationErrorsException
    /// will be raised if dir not exists, else
    /// InvalidPackageException will be raised
    /// </param>
    /// <param name="errorMsg">error message in case of exception</param>
    /// <param name="formatArgs">format args for errorMsg</param>
    private static void _CheckExistense(string path, bool isConfigDir, string errorMsg, params string[] formatArgs)
    {
        if(path == null)
            throw new ConfigurationErrorsException(errorMsg);

        if (Directory.Exists(path) || File.Exists(path))
            return;

        string msg = string.Format(
            errorMsg,
            formatArgs
        );
        if (isConfigDir)
            throw new ConfigurationErrorsException(msg);
        else
            throw new InvalidPackageException(msg);
    }

    private static string LoadConf(string var)
    {
        return ConfigurationManager.AppSettings.Get(var);
    }
}

}