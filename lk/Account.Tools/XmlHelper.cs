﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;


namespace Account.Tools
{
    public class XmlHelper
    {
        /// <summary>
        /// XPathNavigator object
        /// </summary>
        private XPathNavigator Navigator;
        /// <summary>
        /// NamespaceManager
        /// </summary>
        private XmlNamespaceManager nsManager;
        private string nsLocal;
        private bool useLocalNamespace;

        public XmlHelper(byte[] data, bool getWithNS=true)
        {
            useLocalNamespace = getWithNS;

            using (MemoryStream stream = new MemoryStream(data))
            using (XmlReader reader = XmlReader.Create(stream))
            {
                XPathDocument doc = new XPathDocument(reader);
                Navigator = doc.CreateNavigator();
                Navigator.MoveToFirstChild();
                nsManager = new XmlNamespaceManager(Navigator.NameTable);
                while (String.IsNullOrEmpty(nsLocal))
                {
                    
                    if (!string.IsNullOrEmpty(Navigator.Prefix))
                    {
                        nsManager.AddNamespace(
                            Navigator.Prefix, Navigator.NamespaceURI
                        );
                        nsLocal = Navigator.Prefix;
                    }
                    else if (Navigator.NamespaceURI != null)
                    {
                        nsLocal = "av";
                        nsManager.AddNamespace(
                            nsLocal, Navigator.NamespaceURI
                        );
                    }
                    if (!Navigator.MoveToChild(XPathNodeType.All))
                        break;
                }
                Navigator.MoveToRoot();
            }
        }

        private string XPathString(string s, string ns)
        {
            if (string.IsNullOrEmpty(ns) || !useLocalNamespace)
                return s;
            else
                return string.Join("/" + ns + ":", s.Split('/'));
        }

        public XPathNodeIterator GetNodesIterator(string xpath)
        {
            xpath = XPathString(xpath, nsLocal);
            return Navigator.Select(xpath, nsManager);
        }

        public string GetValue(string xpath, XPathNavigator navigator = null)
        {
            xpath = XPathString(xpath, nsLocal);
            // берем относительный путь если передали навигатор
            if (navigator != null && xpath.StartsWith("/"))
            {
                xpath = xpath.Remove(0, 1);
            }
            var e = (navigator ?? Navigator).SelectSingleNode(xpath, nsManager);
            if (e == null)
                return null;
            else
                return e.Value.Trim();
        }
    }
}
