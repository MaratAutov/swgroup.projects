﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Account.DAO;

namespace Account.Tools
{
    public enum PackagePeriodType
    {
        [Display(Name = "Годовая")]
        Year = 0,
        [Display(Name = "Квартальная")]
        Quarter,
        [Display(Name = "Ежемесячная")]
        Month
        
    }

    public enum ExtPackagePeriodType
    {
        [Display(Name = "Годовая")]
        Year = 0,
        [Display(Name = "Квартальная")]
        Quarter,
        [Display(Name = "Ежемесячная")]
        Month,
        [Display(Name = "На дату")]
        OnDay

    }

    public enum Quarter
    {
        [Display(Name = "I Квартал")]
        Квартал1 = 0,
        [Display(Name = "II Квартал")]
        Квартал2,
        [Display(Name = "III Квартал")]
        Квартал3,
        [Display(Name = "IV Квартал")]
        Квартал4
    }

    public enum Month
    {
        [Display(Name = "Январь")]
        January = 0,
        [Display(Name = "Февраль")]
        February,
        [Display(Name = "Март")]
        March,
        [Display(Name = "Апрель")]
        April,
        [Display(Name = "Май")]
        May,
        [Display(Name = "Июнь")]
        June,
        [Display(Name = "Июль")]
        July,
        [Display(Name = "Август")]
        August,
        [Display(Name = "Сентябрь")]
        September,
        [Display(Name = "Октябрь")]
        October,
        [Display(Name = "Ноябрь")]
        November,
        [Display(Name = "Декабрь")]
        December
    }

    public static class PackagePeriodUtils
    {
        public static ExtPackagePeriodType GetPackagePeriodType(PackageFullInfo pkg)
        {
            if (!String.IsNullOrEmpty(pkg.Year) &&
                String.IsNullOrEmpty(pkg.Quartal) &&
                String.IsNullOrEmpty(pkg.Month))
                return ExtPackagePeriodType.Year;
            else if (!String.IsNullOrEmpty(pkg.Year) &&
                !String.IsNullOrEmpty(pkg.Quartal) &&
                String.IsNullOrEmpty(pkg.Month))
                return ExtPackagePeriodType.Quarter;
            else if (!String.IsNullOrEmpty(pkg.Year) &&
                String.IsNullOrEmpty(pkg.Quartal) &&
                !String.IsNullOrEmpty(pkg.Month))
                return ExtPackagePeriodType.Month;
            else if (String.IsNullOrEmpty(pkg.Year) &&
                String.IsNullOrEmpty(pkg.Quartal) &&
                String.IsNullOrEmpty(pkg.Month) &&
                pkg.Date != null)
                return ExtPackagePeriodType.OnDay;
            else
                throw new ArgumentOutOfRangeException("Неизвестный тип периодичности отчета ежегодный/ежеквартальный/ежемесячный");
        }
    }
}
