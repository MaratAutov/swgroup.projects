﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using Account.DAO;
using Progresoft.Accounts.ElectroSignature;
using System.Configuration;
using System.Xml.Xsl;
using System.Xml;
using System.Web;
using System.Reflection;

namespace Account.Tools
{
    public class ClientResponseNotification
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string DocTypeName { get; set; }
        public string IncomingNumber { get; set; }
        public DateTime? IncomingDate { get; set; }
        public string SedNumber { get; set; }
        public DateTime? SedDate { get; set; }
        public string INN { get; set; }
        public string OGRN { get; set; }
        public string RoName { get; set; }
        public string ReturnCode { get; set; }
        public DateTime? StatusAssignmentTime { get; set; }
        public string ErrorDescription { get; set; }

        
        public void mappingFrom(PackageFullInfo package)
        {
            this.ID = package.ID;
            this.DocTypeName = package.DocTypeName;
            this.IncomingNumber = package.IncomingNumber;
            this.IncomingDate = package.IncomingDate;
            this.SedNumber = package.SedNumber;
            this.SedDate = package.SedDate;
            this.INN = package.INN;
            this.OGRN = package.OGRN;
            this.RoName = package.RoName;
            this.StatusAssignmentTime = package.CurrentStatusAssignmentTime;
            this.Name = package.Name;
            this.ReturnCode = StatusService.GetSPOStatusString(package.CurrentStatusCode);
            this.ErrorDescription = package.CurrentStatusInfo;
        }

        public void initialMappingFrom(PackageFullInfo package)
        {
            this.ID = package.ID;
            this.StatusAssignmentTime = package.InitialStatusAssignmentTime;
            this.Name = package.Name;
            this.ReturnCode = StatusService.GetSPOStatusString((int)package.InitialStatusCode);
        }

        public static ClientResponseNotification GetInstance(int packageID, bool IsInitial = false)
        {
            var packInfo = PackageService.GetFullInfo(packageID);
            ClientResponseNotification response = new ClientResponseNotification();
            if (IsInitial)
                response.initialMappingFrom(packInfo);
            else
                response.mappingFrom(packInfo);
            return response;
        }
    }

    public static class NotificationHelper
    {
        public static NotificationResult Build(int packageID, string status = null, bool IsInitial = false, string info = null)
        {
            byte[] xmlData = null;
            string signer = ConfigurationManager.AppSettings.Get("Signer");
            
            //var packInfo =   PackageService.GetFullInfo(packageID);
            var packInfo = ClientResponseNotification.GetInstance(packageID, IsInitial);
            if (status != null)
            {
                packInfo.ReturnCode = status;
                packInfo.ErrorDescription = info;
            }
            
            var serializer = new XmlSerializer(packInfo.GetType());           
            using (var stream = new MemoryStream()) {
                serializer.Serialize(stream, packInfo);
                xmlData = stream.ToArray();
            }

            string fName = "Уведомление.xtdd";
            byte[] notification = SignatureEngine.Instance.Sign(
                fName,
                // заглушка для преобразование в старое уведомление
                TransformToOldNotification(xmlData),
                new SignerDescriptor {
                    CN=signer
                }
            );

            return new NotificationResult {
                FileName = packInfo.Name,
                Notification = notification,
                Status = packInfo.ReturnCode
            };
        }

        public static NotificationResult GetOrCreate(
            string name, string status)
        {
            byte[] xmlData = null;
            string signer = ConfigurationManager.AppSettings.Get("Signer");

            ClientResponseNotification response = new ClientResponseNotification();
            response.Name = name;            
            response.ReturnCode = status;


            var serializer = new XmlSerializer(response.GetType());
            using (var stream = new MemoryStream())
            {
                serializer.Serialize(stream, response);
                xmlData = stream.ToArray();
            }

            string fName = "Уведомление.xtdd";
            byte[] notification = SignatureEngine.Instance.Sign(
                fName,
                // заглушка для преобразование в старое уведомление
                TransformToOldNotification(xmlData),
                new SignerDescriptor
                {
                    CN = signer
                }
            );

            return new NotificationResult
            {
                FileName = response.Name,
                Notification = notification,
                Status = response.ReturnCode
            };
        }
        /// <summary>
        /// Трансформация уведомления в вид который генерировало старое СПО
        /// Сделал трансформацию, так как точно пока непонятно останется этот вид или мы от него откажемся
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        static byte[] TransformToOldNotification(byte[] data)
        {
            var xd = new XmlDocument();
            
            xd.LoadXml(Encoding.UTF8.GetString(data));
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (var stm = new MemoryStream())
            using (XmlWriter writer = XmlWriter.Create(stm, settings))
            {
                Transformer.Transformator.Transform(xd, writer);
                return stm.ToArray();
            }
        }

        static class Transformer
        {
            static XslCompiledTransform _transformator;
            public static XslCompiledTransform Transformator
            {
                get
                {
                    if (_transformator == null)
                    {
                        _transformator = new XslCompiledTransform();
                        using (Stream xslStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(@"Account.Tools.TransformToOldNotification.xslt"))
                        using (XmlReader xslReader = XmlReader.Create(xslStream))
                        {
                            _transformator.Load(xslReader, XsltSettings.TrustedXslt, null);
                        }
                    }
                    return _transformator;
                }
            }
        }

    }
}
