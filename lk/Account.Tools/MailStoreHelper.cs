﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;

namespace Account.Tools
{
    public static class MailStoreHelper
    {
        static string archiveRoot = Path.GetFullPath(
            ConfigurationManager.AppSettings["ArchiveDir"]
        );
        static string MailCatalogName = "mail";

        public static MailStoreInfo GetFileToStore()
        {
            Guid mailGuid = Guid.NewGuid();
            DateTime dt = DateTime.Now;
            
            string emlrelative = MailCatalogName;
            emlrelative = Path.Combine(emlrelative, dt.Year.ToString());
            emlrelative = Path.Combine(emlrelative, dt.Month.ToString());
            emlrelative = Path.Combine(emlrelative, dt.Day.ToString());
            string emlPath = Path.Combine(archiveRoot, emlrelative);
            if (!Directory.Exists(emlPath))
                Directory.CreateDirectory(emlPath);
            emlrelative = Path.Combine(emlrelative, mailGuid.ToString() + ".eml");
            string eml = Path.Combine(archiveRoot, emlrelative);
            emlrelative = "fs:\\" + emlrelative;
            return new MailStoreInfo(eml, emlrelative, mailGuid);
        }

        public static string GetAbsoluteFileName(string relativeFileName)
        {
            relativeFileName = relativeFileName.Replace("fs:\\", "");
            return Path.Combine(archiveRoot, relativeFileName);
        }
    }
    
    


    public class MailStoreInfo
    {
        public readonly string FileName;
        public readonly string RelativeFileName;
        public readonly Guid guid;

        public MailStoreInfo(string fileName, string relativeFileName, Guid _guid)
        {
            FileName = fileName;
            RelativeFileName = relativeFileName;
            guid = _guid;
        }
    }
}