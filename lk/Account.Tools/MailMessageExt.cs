﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.IO;

using LumiSoft.Net.Mime;

namespace Account.MailService
{
    public static class MailMessageExt
    {
        public static void Save(this MailMessage Message, string FileName)
        {
            Assembly assembly = typeof(SmtpClient).Assembly;
            Type _mailWriterType =
              assembly.GetType("System.Net.Mail.MailWriter");

            using (FileStream _fileStream =
                   new FileStream(FileName, FileMode.Create))
            {
                // Get reflection info for MailWriter contructor
                ConstructorInfo _mailWriterContructor =
                    _mailWriterType.GetConstructor(
                        BindingFlags.Instance | BindingFlags.NonPublic,
                        null,
                        new Type[] { typeof(Stream) },
                        null);

                // Construct MailWriter object with our FileStream
                object _mailWriter =
                  _mailWriterContructor.Invoke(new object[] { _fileStream });

                // Get reflection info for Send() method on MailMessage
                MethodInfo _sendMethod =
                    typeof(MailMessage).GetMethod(
                        "Send",
                        BindingFlags.Instance | BindingFlags.NonPublic);

                // Call method passing in MailWriter
#warning на сервере нужно убрать передачу третьего параметра в метод, иначе возникает ошибка, видимо разные версии .net                
                _sendMethod.Invoke(
                    Message,
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new object[] { _mailWriter, true},
                    null);

                // Finally get reflection info for Close() method on our MailWriter
                MethodInfo _closeMethod =
                    _mailWriter.GetType().GetMethod(
                        "Close",
                        BindingFlags.Instance | BindingFlags.NonPublic);

                // Call close method
                _closeMethod.Invoke(
                    _mailWriter,
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new object[] { },
                    null);
            }
        }

        public static string TraceToString(this MailMessage Message)
        {
            Assembly assembly = typeof(SmtpClient).Assembly;
            Type _mailWriterType =
              assembly.GetType("System.Net.Mail.MailWriter");



            using (MemoryStream _fileStream =
                   new MemoryStream())
            {
                // Get reflection info for MailWriter contructor
                ConstructorInfo _mailWriterContructor =
                    _mailWriterType.GetConstructor(
                        BindingFlags.Instance | BindingFlags.NonPublic,
                        null,
                        new Type[] { typeof(Stream) },
                        null);

                // Construct MailWriter object with our FileStream
                object _mailWriter =
                  _mailWriterContructor.Invoke(new object[] { _fileStream });

                // Get reflection info for Send() method on MailMessage
                MethodInfo _sendMethod =
                    typeof(MailMessage).GetMethod(
                        "Send",
                        BindingFlags.Instance | BindingFlags.NonPublic);

                // Call method passing in MailWriter
                _sendMethod.Invoke(
                    Message,
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new object[] { _mailWriter, true },
                    null);

                StreamReader sr = new StreamReader(_fileStream);
                _fileStream.Position = 0;
                string str = sr.ReadToEnd();

                // Finally get reflection info for Close() method on our MailWriter
                MethodInfo _closeMethod =
                    _mailWriter.GetType().GetMethod(
                        "Close",
                        BindingFlags.Instance | BindingFlags.NonPublic);

                // Call close method
                _closeMethod.Invoke(
                    _mailWriter,
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new object[] { },
                    null);

                return str;
            }

        }

        static string fixMessageAttachmentHeader(string msg)
        {
            msg = msg.Replace("name=request.xml", "name=\"request.xml\"");
            msg = msg.Replace("name=document.zip", "name=\"document.zip\"");
            return msg;
        }

        public static Stream GetStream(this MailMessage Message)
        {
            string msg = Message.TraceToString();
            if (Message.Attachments != null && Message.Attachments.Count == 2 &&
                Message.Attachments[0].Name == "request.xml" &&
                Message.Attachments[1].Name == "document.zip")
                msg = fixMessageAttachmentHeader(msg);
            return new MemoryStream(System.Text.Encoding.UTF8.GetBytes(msg));
        }

        public static void Load(this MailMessage Message, string FileName)
        {
            using (FileStream _fileStream =
                   new FileStream(FileName, FileMode.Open))
            {
                var mime = Mime.Parse(_fileStream);
                foreach (MailboxAddress addr in mime.MainEntity.To)
                    Message.To.Add(new MailAddress(
                        addr.EmailAddress
                    ));
                foreach (MailboxAddress addr in mime.MainEntity.From)
                    Message.From = new MailAddress(
                        addr.EmailAddress
                    );

                Message.Subject = mime.MainEntity.Subject;

                foreach (var f in mime.MimeEntities)
                {
                    if (f.ContentDisposition != ContentDisposition_enum.Attachment)
                        continue;
                    Message.Attachments.Add(new System.Net.Mail.Attachment(
                        new MemoryStream(f.Data),
                        new ContentType(f.ContentTypeString)
                    ));                    
                }
            }
        }
    }
}