﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Account.Tools
{
    public static class MailTemplates
    {
        public static string TemplateDir { get; set; }

        public static string RegistrationRequestAcceptedUr { get; set; }
        public static string RegistrationRequestNotifyAdminUr { get; set; }

        public static string RegistrationRequestAcceptedFiz { get; set; }
        public static string RegistrationRequestNotifyAdminFiz { get; set; }        

        static MailTemplates()
        {
            TemplateDir = ConfigurationManager.AppSettings["MailTemplateDir"];

            RegistrationRequestAcceptedUr = File.ReadAllText(
                Path.Combine(TemplateDir, "RegistrationRequestAcceptedUr.html")
            );
            RegistrationRequestNotifyAdminUr = File.ReadAllText(
                Path.Combine(TemplateDir, "RegistrationRequestNotifyAdminUr.html")
            );          

            RegistrationRequestAcceptedFiz = File.ReadAllText(
                Path.Combine(TemplateDir, "RegistrationRequestAcceptedFiz.html")
            );
            RegistrationRequestNotifyAdminFiz = File.ReadAllText(
                Path.Combine(TemplateDir, "RegistrationRequestNotifyAdminFiz.html")
            );
        }        
    }
}
