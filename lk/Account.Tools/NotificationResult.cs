﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Account.Tools
{
    public class NotificationResult
    {
        public string FileName     { get; set; }
        public byte[] Notification { get; set; }
        public string Status       { get; set; }
    }
}