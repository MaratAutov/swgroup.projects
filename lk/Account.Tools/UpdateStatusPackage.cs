﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Account.DAO;
using System.Configuration;
using NLog;

namespace Account.Tools
{
    public static class UpdateStatusPackage
    {
        static string logFile = ConfigurationManager.AppSettings["LogFile"];
        static Logger log = NLogger.GetLog("PackageManager", logFile);

        // Для обновления статусов для первого сосздания используется Create
        public static void Build(
            int id,
            PrimaryStatus status,
            SecondaryStatus secondary=SecondaryStatus.UNSET,
            string info=null,
            string excInfo=null,            
            bool IsUpdateInitialStatus = false)
        {
            try
            {
                var data = NotificationHelper.Build(id, StatusService.GetSPOStatusString((int)status), info: info);
                StatusService.UpdateStatusForPackages(id, data.Notification, status, secondary, info, excInfo, IsUpdateInitialStatus);
            }
            catch (Exception ex)
            {
                log.DebugException(string.Format("Error updating status for pacakge id :: {0}; to status :: {1}", id, StatusService.GetSPOStatusString((int)status)), ex);
                throw;
            }
        }

        public static Package Create(
            string name,
            byte[] data,
            int ownerId,
            DateTime uploadTime,
            bool isRo,
            string sedNum = null,
            DateTime? sedDate = null)
        {            
            Package package = null;
            DateTime currentDate = DateTime.Now; 
            try
            {
                package = PackageService.Create(name, data, ownerId, uploadTime, isRo, sedNum, sedDate, changeTime: currentDate);
            }
            catch (Exception ex)
            {
                log.DebugException("Error creating package", ex);
                throw;
            }
            try
            {
                var notification = NotificationHelper.Build(package.ID, StatusService.GetSPOStatusString((int)PrimaryStatus.Accepted));
                StatusService.RefreshStatusNotification((int)package.CurrentStatusID, notification.Notification);
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Error creating notification for id :: {0}; to status :: {1}; exception :: {2}", package.ID, package.CurrentStatus, 
                    ex.ToString()));
                if (package != null)
                {
                    PackageService.Delete(package);
                }
                throw;
            }
            return package;  
        }
    }
}
