if (fieldType == typeof(byte[]))
{
    StringBuilder sb = new StringBuilder();
    sb.Append("0x");
    foreach (byte b in (byte[])value)
        sb.AppendFormat("{0:x2}", b);
    return sb.ToString();
}