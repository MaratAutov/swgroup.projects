﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using NLog;
using Account.Tools;
using System.ComponentModel;


namespace ServiceCore
{
    public static class ServiceCoreManager
    {
        public static Dictionary<string, object> Instances;
    }

    public class ServiceCore<T>
    {
        private Logger log;
        private ServiceCoreInfo Info;

        private Queue processingQueue;
        private EventWaitHandle enqueueHandle;
        //private Thread worker;
        BackgroundWorker bw;

        public delegate void EventHandler(T t);
        public delegate bool ProcessHandler(T t, ServiceCoreInfo info);
        public delegate void ErrorHandler(T t, Exception exc);

        public ProcessHandler OnProcess = null;
        public EventHandler OnEnqueue = null;
        public EventHandler OnSuccess = null;
        public EventHandler OnFailure = null;
        public ErrorHandler OnError = null;

        private bool isInited = false;
       
        private ServiceCore(string name, Logger logger=null)
        {
            log = logger ?? NLogger.GetLog(name);

            initServiceCore();
            Info.Name = name;
        }

        public static ServiceCore<T> GetInstance(string key, Logger log=null)
        {
            if (ServiceCoreManager.Instances == null) {
                ServiceCoreManager.Instances = new Dictionary<string, object>();
            } if (ServiceCoreManager.Instances.ContainsKey(key)) {
                return (ServiceCore<T>)ServiceCoreManager.Instances[key];
            } else {
                var instance = new ServiceCore<T>(key, log);
                ServiceCoreManager.Instances.Add(key, instance);
                return instance;
            }
        }
                
        public void InitEvents(
            ProcessHandler onProcess,
            EventHandler onEnqueue=null,
            EventHandler onSuccess=null,
            EventHandler onFailure=null,
            ErrorHandler onError=null)
        {
            if (isInited)
                return;

            OnProcess = onProcess;
            OnEnqueue = onEnqueue;
            OnSuccess = onSuccess;
            OnFailure = onFailure;
            OnError = onError;
            isInited = true;
        }             

        private void initServiceCore()
        {
            log.Debug("Service core initiation started");

            enqueueHandle = new EventWaitHandle(
                false, EventResetMode.ManualReset
            );
            processingQueue = Queue.Synchronized(new Queue());
            
            Info = new ServiceCoreInfo(processingQueue);
            Info.IsActive = true;

            RunThread();
            //worker = new Thread(routine => {
            //    serviceCore_QueueProcessing();
            //});
            //worker.IsBackground = true;
            
            //worker.Name = "Service-Core-Thread";
            //worker.Start();

            log.Debug("Service core initiation finished");
        }

        void RunThread()
        {
            bw = new BackgroundWorker();
            bw.DoWork += serviceCore_QueueProcessing;
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            bw.RunWorkerAsync();
        }

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            log.Debug("QueueProcessing complete finished{0}Error: {1}{0}Cancelled: {2}{0}Restarting thread...", Environment.NewLine, e.Error, e.Cancelled);
            //restart thread
            RunThread();
        }

        void ProcessingQueue()
        {
            while (Info.IsActive)
            {
                log.Debug("QueueProcessing going to wait");
                enqueueHandle.WaitOne();
                log.Debug(
                    "QueueProcessing event flagged (in queue: {0})",
                    processingQueue.Count
                );
                while (processingQueue.Count > 0)
                {
                    T t;
                    lock (processingQueue.SyncRoot)
                    {
                        t = (T)processingQueue.Dequeue();
                    }
                    log.Debug(
                        "QueueProcessing is ready to process {0}",
                        t
                    );
                    try
                    {
                        bool result = OnProcess(t, Info);
                        log.Debug(
                            "QueueProcessing OnProcess done result: {0}",
                            result
                        );

                        if (result && OnSuccess != null)
                        {
                            OnSuccess(t);
                        }
                        else if (!result && OnFailure != null)
                        {
                            OnFailure(t);
                        }
                    }
                    catch (Exception exc)
                    {

                        log.Error(
                            "QueueProcessing exception raised: {0}; Object: {1}",
                            exc,
                            t
                        );
                        if (OnError != null)
                        {
                            OnError(t, exc);
                        }
#warning возможно стоит добавить сообщение опять в очередь , делаю это 3 раза

                        lock (processingQueue.SyncRoot)
                        {
                            int hash = t.GetHashCode();
                            if (errorPackets.ContainsKey(hash))
                            {
                                int count = errorPackets[hash];
                                if (count > 2)
                                {
                                    errorPackets.Remove(hash);
                                }
                                else
                                {
                                    errorPackets[hash] = ++count;
                                }
                            }
                            else
                            {
                                // Чистим так как при возинкновении некоторых ошибок может не чиститься и накапливаться
                                if (errorPackets.Count > 1000)
                                    errorPackets.Clear();
                                errorPackets.Add(hash, 0);
                                processingQueue.Enqueue(t);

                            }

                        }
                    }
                }
                enqueueHandle.Reset();
            }
            
            log.Debug(
                "QueueProcessing thread [{0}] finished",
                Thread.CurrentThread.ManagedThreadId
            );
        }

        static Dictionary<int, int> errorPackets = new Dictionary<int, int>();

        private void serviceCore_QueueProcessing(Object sender, DoWorkEventArgs e)
        {
            log.Debug("QueueProcessing started");

            if (OnProcess == null)
            {
                log.Debug("QueueProcessing finished, because not process function");
                return;
            }

            while (true)
            {
                try
                {
                    ProcessingQueue();
                }
                catch (Exception ex)
                {
                    log.ErrorException(
                            "ServiceCore QueueProcessing exception raised:",
                            ex);
                }
            }            
        }

        public void Enqueue(T t)
        {
            log.Debug("Enqueueing new item: {0}", t);
            if (bw == null || !bw.IsBusy)
                RunThread();
                       
            try
            {
                if (OnEnqueue != null)
                {
                    OnEnqueue(t);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            try
            {
                processingQueue.Enqueue(t);
                enqueueHandle.Set();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        // Даже думать об этом не надо, НИКТО НЕ ИМЕЕТ ПРАВА отключить сервис
        public void Shutdown(bool clearQueue=false)
        {
#if DEBUG
            Info.IsActive = false;
            if (clearQueue)
            lock (processingQueue.SyncRoot) {
                processingQueue.Clear();
            }
            enqueueHandle.Set();
            enqueueHandle.WaitOne();
#endif
        }

        public ServiceCoreInfo GetInfo()
        {
            return Info;
        }

        public void ResetInfo()
        {
            Info.Processed =
            Info.Succeeded =
            Info.Failed    =
            Info.Errors    = 0;
        }
    }
}