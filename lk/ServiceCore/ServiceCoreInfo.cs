﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ServiceCore
{
    public class ServiceCoreInfo
    {
        private Queue _queue;

        public ServiceCoreInfo()
        {
        }

        public ServiceCoreInfo(Queue queue)
        {
            _queue = queue;
        }

        public override string ToString()
        {
            return string.Format(
                @"Is Active: {0}
                Pending: {1}
                Processed: {2}
                Succeeded: {3}
                Failed: {4}
                Errors: {5}
                ",
                IsActive,
                Pending,
                Processed,
                Succeeded,
                Failed,
                Errors                
            );
        }

        public bool IsActive { get; set; }
        public string Name { get; set; }
        public long Processed { get; set; }
        public long Succeeded { get; set; }
        public long Failed { get; set; }
        public long Errors { get; set; }
        public long Pending
        {
            get
            {
                return _queue.Count;
            }
            set
            {
            }
        }
    }
}
