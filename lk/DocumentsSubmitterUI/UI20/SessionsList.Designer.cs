﻿namespace DocumentsSubmitterUI.UI20
{
	partial class SessionsList
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionsList));
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.clearToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.resendToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.listViewMain = new System.Windows.Forms.ListView();
			this.reportNameColumnHeader = new System.Windows.Forms.ColumnHeader();
			this.statusColumnHeader = new System.Windows.Forms.ColumnHeader();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.transmitionStatusColumn = new System.Windows.Forms.ColumnHeader();
			this.statusStrip1.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
			this.statusStrip1.Location = new System.Drawing.Point(0, 254);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(672, 22);
			this.statusStrip1.TabIndex = 0;
			this.statusStrip1.Text = "statusStrip1";
			this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Margin = new System.Windows.Forms.Padding(22, 3, 0, 2);
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(97, 17);
			this.toolStripStatusLabel1.Text = "Идет передача...";
			// 
			// toolStrip1
			// 
			this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.clearToolStripButton,
            this.toolStripSeparator2,
            this.resendToolStripButton});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(672, 25);
			this.toolStrip1.TabIndex = 2;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// openToolStripButton
			// 
			this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
			this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.openToolStripButton.Name = "openToolStripButton";
			this.openToolStripButton.Size = new System.Drawing.Size(79, 22);
			this.openToolStripButton.Text = "&Загрузить";
			this.openToolStripButton.ToolTipText = "Загрузить новый отчет";
			this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
			// 
			// saveToolStripButton
			// 
			this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
			this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.saveToolStripButton.Name = "saveToolStripButton";
			this.saveToolStripButton.Size = new System.Drawing.Size(152, 22);
			this.saveToolStripButton.Text = "&Сохранить уведомления";
			this.saveToolStripButton.ToolTipText = "Сохранить все уведомления для \r\nзагруженных отчетов. \r\nВсе успешно обработанные о" +
				"тчеты будут удалены";
			this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// clearToolStripButton
			// 
			this.clearToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("clearToolStripButton.Image")));
			this.clearToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.clearToolStripButton.Name = "clearToolStripButton";
			this.clearToolStripButton.Size = new System.Drawing.Size(76, 22);
			this.clearToolStripButton.Text = "Очистить";
			this.clearToolStripButton.ToolTipText = "Очистить список отчетов";
			this.clearToolStripButton.Click += new System.EventHandler(this.clearToolStripButton_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// resendToolStripButton
			// 
			this.resendToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("resendToolStripButton.Image")));
			this.resendToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.resendToolStripButton.Name = "resendToolStripButton";
			this.resendToolStripButton.Size = new System.Drawing.Size(134, 22);
			this.resendToolStripButton.Text = "Повторить передачу";
			this.resendToolStripButton.ToolTipText = "Повторить передачу отчетов, для которых возникли ошибки";
			this.resendToolStripButton.Click += new System.EventHandler(this.resendToolStripButton_Click);
			// 
			// listViewMain
			// 
			this.listViewMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.listViewMain.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.reportNameColumnHeader,
            this.statusColumnHeader,
            this.transmitionStatusColumn});
			this.listViewMain.Location = new System.Drawing.Point(0, 28);
			this.listViewMain.Name = "listViewMain";
			this.listViewMain.Size = new System.Drawing.Size(672, 223);
			this.listViewMain.StateImageList = this.imageList1;
			this.listViewMain.TabIndex = 3;
			this.listViewMain.UseCompatibleStateImageBehavior = false;
			this.listViewMain.View = System.Windows.Forms.View.Details;
			// 
			// reportNameColumnHeader
			// 
			this.reportNameColumnHeader.Text = "Отчет";
			this.reportNameColumnHeader.Width = 253;
			// 
			// statusColumnHeader
			// 
			this.statusColumnHeader.DisplayIndex = 2;
			this.statusColumnHeader.Text = "Результат обработки в ФСФР";
			this.statusColumnHeader.Width = 243;
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "led_ok.GIF");
			this.imageList1.Images.SetKeyName(1, "led_error.GIF");
			this.imageList1.Images.SetKeyName(2, "led_alert.GIF");
			this.imageList1.Images.SetKeyName(3, "ajax-loader.gif");
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(3, 258);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(16, 16);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 4;
			this.pictureBox1.TabStop = false;
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
			// 
			// transmitionStatusColumn
			// 
			this.transmitionStatusColumn.DisplayIndex = 1;
			this.transmitionStatusColumn.Text = "Статус передачи";
			this.transmitionStatusColumn.Width = 110;
			// 
			// SessionsList
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(672, 276);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.listViewMain);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.statusStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimumSize = new System.Drawing.Size(430, 300);
			this.Name = "SessionsList";
			this.Text = "АРМ Загрузки Отчетности ФСФР";
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.UploadForm_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.UploadForm_DragEnter);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripButton openToolStripButton;
		private System.Windows.Forms.ToolStripButton saveToolStripButton;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton clearToolStripButton;
		private System.Windows.Forms.ListView listViewMain;
		private System.Windows.Forms.ColumnHeader reportNameColumnHeader;
		private System.Windows.Forms.ColumnHeader statusColumnHeader;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton resendToolStripButton;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.ColumnHeader transmitionStatusColumn;
	}
}