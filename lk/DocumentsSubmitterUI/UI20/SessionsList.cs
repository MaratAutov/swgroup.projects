﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace DocumentsSubmitterUI.UI20
{
	public partial class SessionsList : Form
	{
		public SessionsList()
		{
			InitializeComponent();
			UpdateState();
		}

		public void AddSession(UploadSessionController session)
		{
			listViewMain.Items.Add(session.Lvi);
			_sessionsList.Add(session);
			VisibleSessionsCount++;
			UpdateState();
		}
		List<UploadSessionController> _sessionsList = new List<UploadSessionController>();

		private int _visibleSessionsCount = 0;

		private int VisibleSessionsCount
		{
			get { return _visibleSessionsCount; }
			set
			{
				_visibleSessionsCount = value;
				//labelDragDropHint.Visible = _visibleSessionsCount == 0;
			}
		}

		#region DnD

		private void UploadForm_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				string[] filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
				//if (filePaths.Length == 1)
				{
					e.Effect = DragDropEffects.Copy;
					return;
				}
			}
			e.Effect = DragDropEffects.None;
		}

		private void UploadForm_DragDrop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				var filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
				foreach (var s in filePaths)
				{
					
					Action<string> @delegate = StartUpload;
					BeginInvoke(@delegate, s);
				}

			}
		}

		#endregion

		#region Ulpoad

		private void StartUpload(string obj)
		{
			var uploader = new Uploader
			{
				DstNotificationFileName = Path.GetTempFileName(),
				ReportName = Path.GetFileNameWithoutExtension(obj),
				SrcFileName = obj

			};
			var sessionControl = new UploadSessionController
			{
				Uploader = uploader,
				UploadState = UploadSessionController.State.InProcess
			};
			AddSession(sessionControl);
			ScheduleJob(sessionControl);
		}

		private void ScheduleJob(UploadSessionController uploadSessionController)
		{
			uploadSessionController.UploadState = UploadSessionController.State.InProcess;
			var w = new BackgroundWorker();
			w.DoWork += BackgroundUpload;
			w.RunWorkerCompleted += (sender, args) =>
			                        	{
			                        		uploadSessionController.HasUnsavedData = true;
			                        		uploadSessionController.UploadState = args.Cancelled
			                        		                             	? UploadSessionController.State.Failed
			                        		                             	: UploadSessionController.State.Succeeded;
			                        		UpdateState();
			                        	};
			w.WorkerReportsProgress = true;

			w.RunWorkerAsync(uploadSessionController);
			UpdateState();
		}

		private void BackgroundUpload(object sender, DoWorkEventArgs args)
		{
			BackgroundWorker w = (BackgroundWorker)sender;
			var sc = (UploadSessionController)args.Argument;
			try
			{
				sc.Uploader.Execute();
			}
			catch (Exception e)
			{
				w.ReportProgress(100, e.Message);
				args.Cancel = true;
				sc.Uploader.Error = e;
			}
		}

		#endregion

		void UpdateState()
		{
			bool hasUnsaved = false;
			bool hasInProcess = false;
			bool hasErrors = false;
			foreach (var c in _sessionsList)
			{
				hasUnsaved |= c.HasUnsavedData;
				hasInProcess |= c.UploadState == UploadSessionController.State.InProcess;
				hasErrors |= c.UploadState == UploadSessionController.State.Failed;
			}

			saveToolStripButton.Enabled = hasUnsaved;
			clearToolStripButton.Enabled = _sessionsList.Count > 0;
			pictureBox1.Visible = toolStripStatusLabel1.Visible = hasInProcess;
			resendToolStripButton.Enabled = hasErrors;
		}

		#region Clicks

		private void openToolStripButton_Click(object sender, EventArgs e)
		{
			if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
			{
				foreach (var fileName in openFileDialog1.FileNames)
				{
					StartUpload(fileName);					
				}
			}
		}

		private void saveToolStripButton_Click(object sender, EventArgs e)
		{
			int counter = 0;
			if (folderBrowserDialog.ShowDialog(this) == DialogResult.OK)
			{
				foreach (var c in _sessionsList.ToArray())
				{
					if (c.HasUnsavedData)
					{
						c.Save(Path.Combine(folderBrowserDialog.SelectedPath, 
						                    Path.GetFileNameWithoutExtension(c.Uploader.SrcFileName) + "_Уведомление.zip"));
						counter++;
						RemoveSession(c);
					}
				}
			}

			UpdateState();
		}

		private void clearToolStripButton_Click(object sender, EventArgs e)
		{
			foreach (var c in _sessionsList.ToArray())
			{
				if (c.UploadState != UploadSessionController.State.InProcess)
				{
					RemoveSession(c);
				}
			}
			UpdateState();
		}

		private void RemoveSession(UploadSessionController c)
		{
			listViewMain.Items.Remove(c.Lvi);
			_sessionsList.Remove(c);
		}

		private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{

		}

		private void resendToolStripButton_Click(object sender, EventArgs e)
		{
			foreach (var c in _sessionsList)
			{
				if (c.UploadState == UploadSessionController.State.Failed)
				{
					ScheduleJob(c);
				}
			}
		}

		#endregion

		private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
		{

		}



	}
}
