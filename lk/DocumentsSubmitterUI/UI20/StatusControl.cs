﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using DocumentsSubmitterUI.FFMS;

namespace DocumentsSubmitterUI.UI20
{
	public class UploadSessionController 
	{
		private const int PROCESSING_STATUS_INDEX = 1;
		private const int DOCNAME_INDEX = 0;
		private const int TRANSMITION_STATUS_INDEX = 2;
		private Uploader _uploader;

		public Uploader Uploader
		{
			get { return _uploader; }
			set { _uploader = value; }
		}

		public enum State
		{
			InProcess,
			Succeeded,
			Failed
		}

		private State _state;

		public State UploadState
		{
			get { return _state; }
			set
			{
				_state = value;
				UpdateToState();
			}
		}

		private bool _hasUnsavedData ;

		public bool HasUnsavedData
		{
			get { return _hasUnsavedData; }
			set
			{
				_hasUnsavedData = value;
				//labelNtf.ForeColor = value ? Color.Blue : Color.Gray;
			}
		}

		ListViewItem _lvi = new ListViewItem(new string[] {"","", ""});

		public ListViewItem Lvi
		{
			get { return _lvi; }
		}

		private void UpdateToState()
		{
			DocumentName = _uploader.ReportName;
			switch (_state)
			{
				case State.Failed:
					{
						StatusText = //"Не удалось передать пакет. " +
							(Uploader.Error != null ? Uploader.Error.Message : null);
						TransmitionStatus = UI20.TransmitionStatus.Failure;
						break;
					}
				case State.InProcess:
					{
						StatusText = null;
						TransmitionStatus = UI20.TransmitionStatus.Processing;
						break;
					}
				case State.Succeeded:
					{
						string status = _uploader.Status;
						StatusText = StatusToString(status);
						if (status == "SignatureError" ||
							status == "CertificateExpired" ||
							status == "CertificateRevoked")
							TransmitionStatus = UI20.TransmitionStatus.SuccessWithServerWarning;
						else //if (HasUnsavedData)
							TransmitionStatus = UI20.TransmitionStatus.Success;
						//else
						//    _lvi.StateImageIndex = -1;

						break;
					}
			}
		}

		private string StatusToString(string status)
		{
			switch (status)
			{
                case "Processing":
                    return "Документ загружен в систему";
                case "SignatureCorrect":
					return "ЭЦП корректна";
				case "CertificateExpired":
					return "Срок действия сертификата истёк";
				case "CertificateRevoked":
					return "Cертификат отозван";
				case "SignatureError":
                    return "Ошибка проверки ЭП";
				case "PackageCorrupted":
					return "Нарушена целостность документа";
				case "Accepted":
					return "Документ принят к обработке";
				case "InvalidScheme":
					return "Документ не соответствует схеме";
				case "InvalidSignature":
					return "Подпись нелегитимна";
				case "IncomingNumberAssotiated":
					return "Документу присвоен входящий номер";
				default:
					return "Неизвестный статус";
			}
		}

		public void Save(string filename)
		{
			int counter = 0;
			string dir = Path.GetDirectoryName(filename);
			string fn = Path.GetFileNameWithoutExtension(filename);
			string ext = Path.GetExtension(filename);
			while (File.Exists(filename))
			{
				counter++;
				filename = Path.Combine(dir, fn + "_" + counter + ext);
			}

			File.Copy(_uploader.DstNotificationFileName, filename);

			HasUnsavedData = false;
			UpdateToState();
		}

		public string StatusText
		{
			get { return _lvi.SubItems[PROCESSING_STATUS_INDEX].Text; }
			set { _lvi.SubItems[PROCESSING_STATUS_INDEX].Text = value;}
		}

		public string DocumentName
		{
			get { return _lvi.SubItems[DOCNAME_INDEX].Text; }
			set { _lvi.SubItems[DOCNAME_INDEX].Text = value;}
		}

		private TransmitionStatus _transmitionStatus;

		public TransmitionStatus TransmitionStatus
		{
			get { return _transmitionStatus; }
			set
			{
				_transmitionStatus = value;
				_lvi.SubItems[TRANSMITION_STATUS_INDEX].Text = 
					_transmitionStatusToString[value];
				_lvi.StateImageIndex = (int) value;
			}
		}

		#region mappings

		static UploadSessionController()
		{
			_transmitionStatusToString = new Dictionary<TransmitionStatus, string>
			                             	{
			                             		{TransmitionStatus.Success, "Передано успешно"},
			                             		{TransmitionStatus.Failure, "Ошибка"},
			                             		{TransmitionStatus.SuccessWithServerWarning, "Передано, но ФСФР вернул ошибку"},
			                             		{TransmitionStatus.Processing, "Передается..."}
			                             	};
			
		}

		private static IDictionary<TransmitionStatus, string> _transmitionStatusToString;
		private static IDictionary<string, string> _packageProcessingStatusToString;
		
		#endregion

	}


	public enum TransmitionStatus
	{
		Success = 0,
		Failure = 1,
		SuccessWithServerWarning = 2,
		Processing = 4
	}
}
