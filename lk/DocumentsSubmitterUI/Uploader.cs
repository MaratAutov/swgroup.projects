﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DocumentsSubmitterUI.FFMS;
using System.Net;
using System.Configuration;

namespace DocumentsSubmitterUI
{
	public class Uploader
	{
        string _notificationUrl = null;
        string NotificationUrl
        {
            get
            {
                if (_notificationUrl == null)
                    _notificationUrl = ConfigurationManager.AppSettings["NotificationUrl"];
                return _notificationUrl;
            }
        }

		public string SrcFileName { get; set; }
		public string DstNotificationFileName { get; set; }
		public string ReportName { get; set; }
		public Exception Error { get; set;  }
		public string Status
		{
			get;
			private set;
		}

		public void Execute()
		{
			var ws = new FFMS.ReportService();
			var buffer = File.ReadAllBytes(SrcFileName);

			// передать отчётность
            string id = ws.Submit(ReportName, buffer);
			// получить уведомление
			WebClient wc = new WebClient();
			string notificationURL = NotificationUrl + id;
			wc.DownloadFile(notificationURL, DstNotificationFileName);
			Status = wc.ResponseHeaders["FFMS-notification-status"];
		}
	}
}
