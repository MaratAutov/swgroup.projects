﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DocumentsSubmitterUI
{
	public partial class UploadForm : Form
	{
		public UploadForm()
		{
			InitializeComponent();
		}

		private void UploadForm_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				string[] filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
				if (filePaths.Length == 1)
				{
					e.Effect = DragDropEffects.Copy;
					return;
				}
			}
			e.Effect = DragDropEffects.None;
		}

		private void UploadForm_DragDrop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				var filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
				// Set the file name
				// we checked the length in DragEnter already
				Action<string> @delegate = SetSourceFile;
				BeginInvoke(@delegate, filePaths[0]);
				
			}
		}

		private void SetSourceFile(string s)
		{
			tbFile.Text = s;
			tbName.Text = Path.GetFileNameWithoutExtension(s);
			tbNotification.Text = Path.ChangeExtension(s, "Уведомление1.zip");
		}

		

		private void btnSubmit_Click(object sender, EventArgs e)
		{
			Array.ForEach(GetInputControls(), (c) => c.Enabled = false);
			btnSubmit.Enabled = false;
			progressBar.Visible = true;

			var uploader = new Uploader
			               	{
			               		SrcFileName = tbFile.Text,
			               		DstNotificationFileName = tbNotification.Text,
			               		ReportName = tbName.Text
			               	};

			MethodInvoker d = uploader.Execute;
			d.BeginInvoke((r) => Invoke((Action<IAsyncResult>)SubmitResult, r), d);
		}



		private void SubmitResult(IAsyncResult result)
		{
			MethodInvoker d = (MethodInvoker) result.AsyncState;
			progressBar.Visible = false;
			try
			{
				d.EndInvoke(result);
				panelResult.Visible = true;
			}
			catch (Exception e)
			{
				MessageBox.Show(this, "Произошла ошибка при передаче файла.\r\n" + e.Message,
				                "Файл не передан", MessageBoxButtons.OK, MessageBoxIcon.Error);
				btnSubmit.Enabled = true;
			}
		}

	
		private Control[] GetInputControls()
		{
			return new[] { tbName, tbNotification, tbFile };
		}

		private void btnReset_Click(object sender, EventArgs e)
		{
			Array.ForEach(GetInputControls(), (c) => { c.Enabled = true;
			                                         	c.Text = String.Empty; });
			btnSubmit.Enabled = true;
			panelResult.Visible = false;
		}

		private void btnChooseFile_Click(object sender, EventArgs e)
		{
			if (openFileDialogSrc.ShowDialog(this) == DialogResult.OK)
			{
				SetSourceFile(openFileDialogSrc.FileName);
			}
		}
	}
}
