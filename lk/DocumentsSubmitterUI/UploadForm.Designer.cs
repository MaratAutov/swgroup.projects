﻿namespace DocumentsSubmitterUI
{
    partial class UploadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.linkLabelFileName = new System.Windows.Forms.LinkLabel();
			this.label1 = new System.Windows.Forms.Label();
			this.gbSuccess = new System.Windows.Forms.GroupBox();
			this.btnReset = new System.Windows.Forms.Button();
			this.btnSubmit = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbName = new System.Windows.Forms.TextBox();
			this.tbFile = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
			this.btnChooseFile = new System.Windows.Forms.Button();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.tbNotification = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.panelResult = new System.Windows.Forms.Panel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.openFileDialogSrc = new System.Windows.Forms.OpenFileDialog();
			this.gbSuccess.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.panelResult.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// linkLabelFileName
			// 
			this.linkLabelFileName.AutoSize = true;
			this.linkLabelFileName.Location = new System.Drawing.Point(188, 35);
			this.linkLabelFileName.Name = "linkLabelFileName";
			this.linkLabelFileName.Size = new System.Drawing.Size(135, 13);
			this.linkLabelFileName.TabIndex = 1;
			this.linkLabelFileName.TabStop = true;
			this.linkLabelFileName.Text = "Открыть папку с файлом";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(85, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(238, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Документ передан. Уведомление сохранено.";
			// 
			// gbSuccess
			// 
			this.gbSuccess.Controls.Add(this.progressBar);
			this.gbSuccess.Controls.Add(this.panelResult);
			this.gbSuccess.Controls.Add(this.label5);
			this.gbSuccess.Controls.Add(this.tbNotification);
			this.gbSuccess.Controls.Add(this.button1);
			this.gbSuccess.Location = new System.Drawing.Point(6, 137);
			this.gbSuccess.Name = "gbSuccess";
			this.gbSuccess.Size = new System.Drawing.Size(550, 162);
			this.gbSuccess.TabIndex = 3;
			this.gbSuccess.TabStop = false;
			this.gbSuccess.Text = "Уведомления";
			// 
			// btnReset
			// 
			this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnReset.Location = new System.Drawing.Point(373, 308);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(75, 23);
			this.btnReset.TabIndex = 3;
			this.btnReset.Text = "Сброс";
			this.btnReset.UseVisualStyleBackColor = true;
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// btnSubmit
			// 
			this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnSubmit.Location = new System.Drawing.Point(454, 308);
			this.btnSubmit.Name = "btnSubmit";
			this.btnSubmit.Size = new System.Drawing.Size(102, 23);
			this.btnSubmit.TabIndex = 2;
			this.btnSubmit.Text = "Загрузить";
			this.btnSubmit.UseVisualStyleBackColor = true;
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.tbName);
			this.groupBox1.Controls.Add(this.tbFile);
			this.groupBox1.Controls.Add(this.flowLayoutPanel3);
			this.groupBox1.Controls.Add(this.btnChooseFile);
			this.groupBox1.Controls.Add(this.flowLayoutPanel1);
			this.groupBox1.Location = new System.Drawing.Point(6, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(550, 119);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Файл отчетности";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 86);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(143, 13);
			this.label2.TabIndex = 10;
			this.label2.Text = "Наименование документа:";
			// 
			// tbName
			// 
			this.tbName.Location = new System.Drawing.Point(187, 86);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(357, 20);
			this.tbName.TabIndex = 9;
			// 
			// tbFile
			// 
			this.tbFile.Location = new System.Drawing.Point(187, 21);
			this.tbFile.Name = "tbFile";
			this.tbFile.Size = new System.Drawing.Size(266, 20);
			this.tbFile.TabIndex = 5;
			// 
			// flowLayoutPanel3
			// 
			this.flowLayoutPanel3.AutoSize = true;
			this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 16);
			this.flowLayoutPanel3.Name = "flowLayoutPanel3";
			this.flowLayoutPanel3.Size = new System.Drawing.Size(544, 0);
			this.flowLayoutPanel3.TabIndex = 8;
			// 
			// btnChooseFile
			// 
			this.btnChooseFile.Location = new System.Drawing.Point(459, 20);
			this.btnChooseFile.Name = "btnChooseFile";
			this.btnChooseFile.Size = new System.Drawing.Size(75, 23);
			this.btnChooseFile.TabIndex = 6;
			this.btnChooseFile.Text = "Обзор...";
			this.btnChooseFile.UseVisualStyleBackColor = true;
			this.btnChooseFile.Click += new System.EventHandler(this.btnChooseFile_Click);
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.AutoSize = true;
			this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(544, 0);
			this.flowLayoutPanel1.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(168, 13);
			this.label3.TabIndex = 10;
			this.label3.Text = "Файл отчетности для загрузки:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(39, 48);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(454, 35);
			this.label4.TabIndex = 11;
			this.label4.Text = "Файл отчетности должен быть подписан ЭЦП участника рынка и упакован в zip-архив. " +
				"Подробнее см...";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(5, 28);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(143, 13);
			this.label5.TabIndex = 13;
			this.label5.Text = "Сохранить отчетность как:";
			// 
			// tbNotification
			// 
			this.tbNotification.Location = new System.Drawing.Point(187, 25);
			this.tbNotification.Name = "tbNotification";
			this.tbNotification.Size = new System.Drawing.Size(268, 20);
			this.tbNotification.TabIndex = 11;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(461, 24);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 12;
			this.button1.Text = "Обзор...";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// panelResult
			// 
			this.panelResult.Controls.Add(this.pictureBox1);
			this.panelResult.Controls.Add(this.label1);
			this.panelResult.Controls.Add(this.linkLabelFileName);
			this.panelResult.Location = new System.Drawing.Point(89, 51);
			this.panelResult.Name = "panelResult";
			this.panelResult.Size = new System.Drawing.Size(353, 100);
			this.panelResult.TabIndex = 14;
			this.panelResult.Visible = false;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new System.Drawing.Point(4, 9);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(75, 57);
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(219, 87);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(100, 36);
			this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.progressBar.TabIndex = 15;
			this.progressBar.Visible = false;
			// 
			// openFileDialogSrc
			// 
			this.openFileDialogSrc.Filter = "Zip-Архивы|*.zip|Все файлы|*.*";
			// 
			// UploadForm
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(568, 343);
			this.Controls.Add(this.btnReset);
			this.Controls.Add(this.btnSubmit);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.gbSuccess);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "UploadForm";
			this.Text = "АРМ Загрузки Отчетности ФСФР";
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.UploadForm_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.UploadForm_DragEnter);
			this.gbSuccess.ResumeLayout(false);
			this.gbSuccess.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.panelResult.ResumeLayout(false);
			this.panelResult.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.Button btnSubmit;
		private System.Windows.Forms.Button btnReset;
		private System.Windows.Forms.GroupBox gbSuccess;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.LinkLabel linkLabelFileName;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.TextBox tbFile;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
		private System.Windows.Forms.Button btnChooseFile;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbNotification;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panelResult;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ProgressBar progressBar;
		private System.Windows.Forms.OpenFileDialog openFileDialogSrc;
    }
}