$(function () {
    if ($('.date').length) {
        $('.date').datepicker();
    }

    if ($('table').length) {
        $('table')
            .find('tbody tr:odd')
            .addClass('odd');
    }
});

function showEgrul(url, inn, opts) {
    $.post(
        url,
        { 'inn': inn },
        function (data) {
            $('#egrul-info').html(data);
        }
    );

    $('#egrul-info')
        .attr('title', opts.title)
        .removeClass('report-info')
        .addClass('report-actions')
        .dialog({ width: opts.width, height: opts.height });
};