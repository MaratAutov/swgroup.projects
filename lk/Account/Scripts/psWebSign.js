﻿var PLUGIN_VERSION = "0.4"

function _marshalError(errcode) {

    if (errcode.message)
        return _marshalError(errcode.message);
    _marshalErrorImpl(errcode, "");
}

function _marshalErrorImpl(errcode, nativeErrorDescription) {
    if (errcode == "PSWS_INT_USERCANCELLED")
        return null;
    //throw { code: errcode, description: wsc_geterrorstr(errcode) };
    throw "Компонент ЭЦП сообщил об ошибке: " + wsc_geterrorstr(errcode);
}

function wsc_signFile(signer, fileSelector, certificateSelector, signParams) {
    wsc_init(signer);
    try {
        var f = signer.signFile(
                fileSelector.title,
                fileSelector.filters,

                certificateSelector.title,
                certificateSelector.noUI,
                certificateSelector.attributes,

                signParams.includeCert,
                signParams.includeCertChain,
                signParams.includeTimestamp);
        if (f == null)
            return null;
        fileSelector.filename = f.filename;
        return f.body;
              
    }
    catch (errcode) {
        return _marshalError(errcode);
    }
}


function wsc_signData(signer, data, certificateSelector, signParams) {
    wsc_init(signer);
    try {
        return signer.signData(
            data,
            
            certificateSelector.title,
            certificateSelector.noUI,
            certificateSelector.attributes,
            
            signParams.includeCert,
            signParams.includeCertChain,
            signParams.includeTimestamp);
    }
    catch (errcode) {
       return _marshalError(errcode);
    }

}

function wsc_displayCertificate(certBase64) {
    wsc_init(signer);
    try {
        return signer.displayCertificate(certBase64);
    }
    catch (errcode) {
        return _marshalError(errcode);
    }
}

function wsc_checkVersion(signer) {
    if (!signer)
        throw "Ошибка страницы: компонент не найден";
    if (!signer.version)
        throw "Компонент не установлен. Обновите страницу и попробуйте выполнить операцию заново; если проблема не исчезнет, обратитесь в службу поддержки.";
    if (signer.version != PLUGIN_VERSION)
        throw "Неверная версия компонента (" + signer.version + ")";
}

function wsc_geterrorstr(errcode) {
    alert(errcode);
    if (errcode == "PSWS_INT_USERCANCELLED")
        return "Операция отменена пользователем";
    if (errcode == "PSWS_INT_NOTSUPPORTED")
        return "Запрошенная операция не поддерживается";
    if (errcode == "PSWS_FILE_GLE_5")
        return "Не удается открыть файл: отказано в доступе";
    if (errcode == "PSWS_FILE_GLE_20")
        return "Не удается открыть файл: он занят другим процессом. Убедитесь, что файл не открыт в редакторе";
    //            if (errcode.match('GLE_5$'))
    //                return "Отказано в доступе (" + errcode + ")";
    if (errcode == "PSWS_SIGN_GLE_80091002")
        return "Не удается выполнить подписание: выбранный сертификат указывает на неподдерживаемый алгоритм шифрования. Выберите другой сертификат или убедитесь, что требуемый криптопровайдер установлен корректно";
    
    
    return "Внутренняя ошибка (" + errcode + ")";

}

function wsc_init(o)
{
    wsc_checkVersion(o);
    if (navigator.appName == "Microsoft Internet Explorer")
        o.fbStringEncoding = "default";
    else
        o.fbStringEncoding = "utf-8";
}

function encode_utf8(s) {
    return unescape(encodeURIComponent(s));
}

function decode_utf8(s) {
    return decodeURIComponent(escape(s));
}

function wsc_renderObject(id, location, binUrlPart) {
        var pluginLoc = location;
        var attrs = '';
        var innerHtml = '';
        if (navigator.appName == "Microsoft Internet Explorer") {
            attrs = "codebase='/" + binUrlPart + "/PSWebSignComponent.cab'";
        }
        else {
            attrs = "codebase='/" + binUrlPart + "/PSWebSignComponent.xpi'";
            // innerHtml = "<param name='pluginurl' value='bin/PSWebSignComponent.xpi'/>";
        }

        pluginLoc.innerHTML = "<object id='"+ id +"' type='application/x-pswebsigncomponent' " + attrs + " style='width: 0px ! important; height: 0px ! important'>" + innerHtml + "</object>"
     //   alert(pluginLoc.innerHTML);
    }