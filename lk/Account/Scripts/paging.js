function setSorting(e) {
    $e = $(e);
    $('.SortingField-value').val($e.attr('id'));
    if ($e.hasClass('desc'))
        $('.SortingType-value').val('asc');
    else
        $('.SortingType-value').val('desc');
}

function setSize(e) {
    $e = $(e);
    $('.PageSize-value').val($e.text());
    $('.Page-value').val(0);
}

function goBack() {
    $('.Page-value').val(parseInt($('.Page-value').val()) - 1);
}

function goNext() {
    $('.Page-value').val(parseInt($('.Page-value').val()) + 1);
}

function goFirst() {
    $('.Page-value').val(0);
}

function goTo(page) {
    $('.Page-value').val(parseInt(page) - 1);
}

function clearFilters(filter) {
    if (!filter)
        $('.filter-container input').val('');
    else
        $('#' + filter).val('');
}