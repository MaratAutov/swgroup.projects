﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Profile;
using Account.DAO;
using Account.Models;
using Account.Tools;
using Account.Security;
using NLog;
using System.Configuration;


namespace Account.Controllers
{
    
    public class HomeController : SecureController
    {
        private Logger _logger = LogManager.GetLogger("HomeController");

        [Authorize]
        public ActionResult Index()
        {
            if (User != null)
            {
                if (User.Individual != null)
                {
                    return RedirectToAction("RequisitesIn", new { id = User.Individual.Login });
                }
                else if (User.Account != null)
                {
                    if (User.Account.IsAdmin)
                        return RedirectToAction("ParticipantsUr");

                    return RedirectToAction("Outgoing", new { id = User.Account.Login });
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            } 
        }
        
        private static void InitParticipantsUrTable(
            FilterTableModel<UserAccountModel, UserAccount> table,
            bool active)
        {
            table.Visible = !active;
            if (table.DataModel == null)
            {
                table.DataModel = new UserAccountModel()
                {
                    IsActive = active
                };
                table.SortingField = "Login";
                table.SortingType = "desc";
            }
            if (table.DataList == null)
                table.DataList = new PagedList<UserAccount>();
            
            table.DataList.DataQuery = UserService.GetAll(
                table.DataModel.Login,
                table.DataModel.Company,
                table.DataModel.INN,
                table.DataModel.OGRN,
                table.DataModel.IsActive,
                table.DataModel.CreateDate,
                table.SortingField,
                table.SortingType
            );
        }

        private static void InitParticipantsFizTable(
            FilterTableModel<UserIndividualModel, UserIndividual> table,
            bool active)
        {
            if (table.DataModel == null)
            {
                table.DataModel = new UserIndividualModel()
                {
                    IsActive = active
                };
                table.SortingField = "Login";
                table.SortingType = "desc";
            }
            if (table.DataList == null)
                table.DataList = new PagedList<UserIndividual>();

            table.DataList.DataQuery = UserIndividualService.GetAll(
                table.DataModel.Login,
                table.DataModel.FirstName,
                table.DataModel.SecondName,
                table.DataModel.MiddleName,
                table.DataModel.IsActive,
                table.DataModel.BirthDate,
                table.SortingField,
                table.SortingType
            );
        }

        [NonAction]
        public void InitOutgoingTable(int userId, string year, FilterTableModel<PackageModel, PackageFullInfo> table)
        {
            if (table.DataModel == null)
            {
                table.DataModel = new PackageModel();
                table.SortingField = "UploadTime";
                table.SortingType = "desc";
            }
            if (table.DataList == null)
                table.DataList = new PagedList<PackageFullInfo>();

            
            table.DataList.DataQuery = PackageService.GetAll(
                userId,
                year,
                table.DataModel.UploadTime,
                table.DataModel.Name,
                table.DataModel.IncomingNumber,
                table.DataModel.IncomingDate,
                table.DataModel.SedNumber,
                table.DataModel.SedDate,
                table.SortingField,
                table.SortingType,
                ConfigurationManager.AppSettings["IsShowImported"] == "True"
            );
           
        }

        private static void InitIncomingTable(int userId, FilterTableModel<DocumentModel, Document> table,bool visibleSender)
        {
            table.Visible = visibleSender;

            if (table.DataModel == null)
            {
                table.DataModel = new DocumentModel();
                table.SortingField = "UploadTime";
                table.SortingType = "desc";
            }
            if (table.DataList == null)
                table.DataList = new PagedList<Document>();

            table.DataList.DataQuery = DocumentService.Get(userId, table.DataModel.UploadTime, table.DataModel.Name, table.DataModel.SenderINN, table.DataModel.SenderName,table.DataModel.ReadTime,
                table.SortingField, table.SortingType);
        }

        private static void InitOutgoingDocTable(int userId, FilterTableModel<DocumentOutgoingModel, Document> table,bool visibleSender)
        {
            table.Visible = visibleSender;

            if (table.DataModel == null)
            {
                table.DataModel = new DocumentOutgoingModel();
                table.SortingField = "UploadTime";
                table.SortingType = "desc";
            }
            if (table.DataList == null)
                table.DataList = new PagedList<Document>();

            table.DataList.DataQuery = DocumentService.GetOutgoing(userId, table.DataModel.Name, table.DataModel.ReceiverINN, table.DataModel.ReceiverName,
                table.DataModel.UploadTime, table.SortingField, table.SortingType );
        }

        [AllowAdminOnly]
        public ActionResult ParticipantsUr(FilterTableModel<UserAccountModel, UserAccount> table)
        {
            InitParticipantsUrTable(table, true);
            return View(table);
        }

        [AllowAdminOnly]
        public ActionResult ParticipantsUrInactive(FilterTableModel<UserAccountModel, UserAccount> table)
        {
            InitParticipantsUrTable(table, false);
            return View("ParticipantsUr", table);
        }

        [AllowAdminOnly]
        public ActionResult ParticipantsFiz(FilterTableModel<UserIndividualModel, UserIndividual> table)
        {
            InitParticipantsFizTable(table, true);
            return View(table);
        }

        [AllowAdminOnly]
        public ActionResult ParticipantsFizInactive(FilterTableModel<UserIndividualModel, UserIndividual> table)
        {
            InitParticipantsFizTable(table, false);
            return View("ParticipantsFiz", table);
        }

        [AllowSelfOrAdmin]
        public ActionResult Requisites(string id)
        {
            UserAccount acc = null;
            UserIndividual ac_in = null;

            if (User.IsUr)
            {
                if (string.IsNullOrEmpty(id) || id == User.Account.Login)
                    acc = User.Account;
                else
                    acc = UserService.Get(id);
                return View(acc);
            }
            else
            {
                if (string.IsNullOrEmpty(id) || id == User.Individual.Login)
                    ac_in = User.Individual;
                else
                    ac_in = UserIndividualService.Get(id);
                return View(ac_in);
            }
        }

        [AllowSelfOrAdmin]
        public ActionResult RequisitesIn(string id)
        {           
            UserIndividual ac_in = null;

           
                if (string.IsNullOrEmpty(id) || ((User.Individual!=null) && (id == User.Individual.Login)))
                    ac_in = User.Individual;
                else
                    ac_in = UserIndividualService.Get(id);
                return View(ac_in);
           
        }

        [AllowSelfOrAdmin]
        public ActionResult OutgoingDoc(string id, FilterTableModel<DocumentOutgoingModel, Document> table)
        {
            try
            {
                UserAccount acc = UserService.Get(id);

                string userName = Session["Impersonated"] != null ?
                           Session["Impersonated"].ToString() :
                           User.Account.Login;
                UserAccount user = UserService.Get(userName);

                if (user.IsAdmin)

                    InitOutgoingDocTable(acc.ID, table, true);
                else
                    InitOutgoingDocTable(acc.ID, table, false);

                return View(table);
            }
            catch
            {
                InitOutgoingDocTable(-1, table, false);

                return View(table);
            }
        }

        [AllowSelfOrAdmin]
        public ActionResult Incoming(string id, FilterTableModel<DocumentModel, Document> table)
        {
            try
            {
                UserAccount acc = UserService.Get(id);

                string userName = Session["Impersonated"] != null ?
                           Session["Impersonated"].ToString() :
                           User.Account.Login;
                UserAccount user = UserService.Get(userName);

                if (user.IsAdmin)
                    InitIncomingTable(acc.ID, table,true);
                else
                    InitIncomingTable(acc.ID, table, false);

                return View(table);
            }
            catch
            {
                InitIncomingTable(-1, table,false);

                return View(table);
            }
        }

        [AllowSelfOrAdmin]
        public ActionResult Outgoing(string id, string year, FilterTableModel<PackageModel, PackageFullInfo> table)
        {
            try
            {
                UserAccount acc = UserService.Get(id);
                InitOutgoingTable(acc.ID, year, table);
                ViewData["outgoing-year"] = year ?? "";

                return View(table);
            }
            catch
            {
                InitOutgoingTable(-1, year, table);
                ViewData["outgoing-year"] = year ?? "";

                return View(table);
            }

            
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            _logger.Error(filterContext.Exception);
            base.OnException(filterContext);
        }

        [AllowSelfOrAdminDownload]
        [HttpGet]
        public ActionResult Download(string id)
        {
            try
            {
                Package p = PackageService.GetPackage(int.Parse(id));
                Blob blob = PackageService.GetBlob(p.BlobID);
                string name = p.Name;
                if (!name.ToLower().EndsWith(".zip"))
                    name = p.Name + ".zip";
                return File(blob.Body, "application/zip", name);
            }
            catch (Exception ex)
            {
                _logger.Error("Error downloading package id: {0}; Exception: {1}", id, ex);
                throw;
            }
        }

        [AllowSelfOrAdminDownload]
        [HttpGet]
        public ActionResult DownloadArchiev(string id)
        {
            try
            {
                PackageFilesInfo p = PackageService.GetPackageFilesInfo(int.Parse(id));
                string path = MailStoreHelper.GetAbsoluteFileName(p.ProcessedDocumentUrl);
                string name = p.Name;
                if (String.IsNullOrEmpty(name))
                    name = "package.zip";
                else if (!name.ToLower().EndsWith(".zip"))
                    name = p.Name + ".zip";
                return File(System.IO.File.ReadAllBytes(path), "application/zip", name);
            }
            catch (Exception ex)
            {
                _logger.Error("Error downloading package id: {0}; Exception: {1}", id, ex);
                throw;
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult DownloadDoc(string id)
        {
            Document p = DocumentService.GetDocument(int.Parse(id));
            Blob blob = DocumentService.GetBlob(p.BlobID);
            if (p.ReadTime == null)
            DocumentService.UpdateReadTime(p.ID);
            return File(blob.Body, "application/zip", p.Name);          
            
        }      

        [Authorize]
        [HttpGet]
        public ActionResult WriteResponse(int id)
        {

            string userName = Session["Impersonated"] != null ?
                           Session["Impersonated"].ToString() :
                           User.Account.Login;

            IdResponse = id;
            return RedirectToAction("Response", new
            {
                id = userName
            });
        }

        [HttpGet]
        public ActionResult Notification(string id)
        {
            int packageID;            
            if (!int.TryParse(id, out packageID))
            {
                _logger.Error("Error downloading notification for package id: {0}", id);
                throw new HttpException(404, "Page not found");                
            }

            var packInfo = PackageService.GetFullInfo(packageID);

            Status status = StatusService.Get((int)packInfo.CurrentStatusID);
            if ((status.NotificationBlobID != null) && (status.NotificationBlobID != 0))
            {
                Blob b = BlobServise.Get((int)status.NotificationBlobID);
                var result = new NotificationResult(b.Body, "application/zip", packInfo.Name + ".уведомление.zip", StatusService.GetSPOStatusString(packInfo.CurrentStatusCode));
                return result;
            }
            else
            {
                var data = NotificationHelper.Build(packageID);

                var result = new NotificationResult(data.Notification, "application/zip", data.FileName + ".уведомление.zip", data.Status);
                return result;
            }
        }

        [HttpGet]
        public ActionResult InitialNotification(string id)
        {
            int packageID;
            if (!int.TryParse(id, out packageID))
                throw new HttpException(404, "Page not found");

            var packInfo = PackageService.GetFullInfo(packageID);

            Status status = StatusService.Get((int)packInfo.InitialStatusID);

            if ((status.NotificationBlobID != null) && (status.NotificationBlobID != 0))
            {
                Blob b = BlobServise.Get((int)status.NotificationBlobID);
                var result = new NotificationResult(b.Body, "application/zip", packInfo.Name + ".уведомление.zip", StatusService.GetSPOStatusString((int)packInfo.InitialStatusCode));
                return result;
            }
            else
            {
                var data = NotificationHelper.Build(packageID, null, true);
                var result = new NotificationResult(data.Notification, "application/zip", data.FileName + ".уведомление.zip", data.Status);
                return result;
            }
        }

        [AllowSelfOrAdmin]
        [HttpGet]
        public ActionResult Upload(string id)
        {
            return View(new UploadModel());
        }

        [AllowSelfOrAdmin]
        [HttpGet]
        public ActionResult Send(string id)
        {
            return View(new SendModel());
        }


        public static int IdResponse
        {
            get;
            set;
        }
        
        [Authorize]
        [HttpGet]
        public new ActionResult Response(string id)
        {
            string referer = Request.ServerVariables["HTTP_REFERER"];
            if (referer != null)
            return View(new ResponseModel());
            else return RedirectToAction("Outgoing", new { id = User.Account.Login });
        }

        [Authorize]
        [HttpPost]
        public ActionResult Upload(UploadModel model)
        {
            if( model.Report == null || model.Name == null )
                return View(model);

            try
            {
                string userName = Session["Impersonated"] != null ?
                         Session["Impersonated"].ToString() :
                         User.Account.Login;

                var user = UserService.Get(userName);
                if (user == null) {
                    throw new InvalidOperationException("User Error");
                }
                int userID = user.ID;

                var stream = model.Report.InputStream;
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, (int)stream.Length);

                try {
                    using (var cl = new SPOService.SPOServiceSoapClient()) {
                        cl.UploadPackage(
                            model.Name,
                            data,
                            userID,
                            DateTime.Now,
                            false,
                            null
                        );
                    }
                } catch (Exception ex) {
                    _logger.Error("Error sending package to SPO; Exception: {0}", ex);
                    string id = Guid.NewGuid().ToString();
                    System.IO.File.WriteAllBytes(
                        "C:\\DelayedUpload\\" + id + ".data",
                        data
                    );
                    System.IO.File.WriteAllText(
                        "C:\\DelayedUpload\\" + id + ".info",
                        string.Format(
                            "{1}{0}{2}{0}UserID={3}{0}{4}{0}{5}",
                            Environment.NewLine,
                            model.Report.FileName,
                            model.Name,
                            userID,
                            DateTime.Now,
                            ex
                        )
                    );
                }
                

                return RedirectToAction("Outgoing", new {
                    id = userName
                });
            }
            catch( Exception exc )
            {
                ModelState.AddModelError(
                    "UploadModel",
                    exc.Message
                );
            }
            return View(model);
        }

        public void SendDocument(string filename, byte[] data,int userID,int userSendID,string inn)
        {
            try
            {
                using (var cl = new SPOService.SPOServiceSoapClient())
                {
                    cl.UploadDocument(
                        filename,
                        data,
                        userID,
                        DateTime.Now,
                        userSendID
                    );
                }
            }
            catch (Exception)
            {
                string id = Guid.NewGuid().ToString();
                System.IO.File.WriteAllBytes(
                    "C:\\DelayedUploadDocument\\" + id + ".data",
                    data
                );
                System.IO.File.WriteAllText(
                    "C:\\DelayedUploadDocument\\" + id + ".info",
                    string.Format(
                        "{1}{0}{2}{0}{3}{0}{4}",
                        Environment.NewLine,
                        filename,
                        inn,
                        userID,
                        DateTime.Now
                    )
                );
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult Send(SendModel model)
        {
            if (model.Report == null || model.INN == null)
                return View(model);

            try
            {
                string userName = Session["Impersonated"] != null ?
                         Session["Impersonated"].ToString() :
                         User.Account.Login;

                var user = UserService.Get(userName);
                if (user == null)
                {
                    throw new InvalidOperationException("User Error");
                }
                int userID = user.ID;

                var user_send = UserService.GetByInn(model.INN);
                if (user_send == null)
                {
                    throw new InvalidOperationException("Участника с таким ИНН не существует");
                }
                int userSendID = user_send.ID;

                var stream = model.Report.InputStream;
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, (int)stream.Length);

                SendDocument(model.Report.FileName, data, userID, user_send.ID,model.INN);

                return RedirectToAction("OutgoingDoc", new
                {
                    id = userName
                });
            }
            catch (Exception exc)
            {
                ModelState.AddModelError(
                    "SendModel",
                    exc.Message
                );
            }
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public new ActionResult Response(ResponseModel model)
        {
            if (model.Report == null)
                return View(model);

            try
            {
                string userName = Session["Impersonated"] != null ?
                         Session["Impersonated"].ToString() :
                         User.Account.Login;

                var user = UserService.Get(userName);
                if (user == null)
                {
                    throw new InvalidOperationException("User Error");
                }
                int userID = user.ID;
                

                var stream = model.Report.InputStream;
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, (int)stream.Length);

                SendDocument(model.Report.FileName, data, userID, IdResponse, "");

                return RedirectToAction("Incoming", new
                {
                    id = userName
                });
            }
            catch (Exception exc)
            {
                ModelState.AddModelError(
                    "ResponseModel",
                    exc.Message
                );
            }
            return View(model);
        }

        public ActionResult EgrulData(string inn)
        {
            EgrulService.Egrul[] model = null;
            using (var service = new EgrulService.ReturnOrganizationDataServiceSoapClient())
            {
                 model = service.GetEgrulList(inn);
            }
            return PartialView(model);
        }
    }
}
