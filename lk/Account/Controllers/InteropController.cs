﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Account.Controllers
{
    public class InteropController : Controller
    {
        //
        // GET: /Interop/

        [HttpGet]
        public JsonResult AcquireDelayedUploadDir()
        {
            string Dir = ConfigurationManager.AppSettings["DelayedUploadDir"];
            return Json(Dir, JsonRequestBehavior.AllowGet);
        }

    }
}
