﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Account.Security;
using Account.Attributes;

namespace Account.Controllers
{
    [LoggingFilter]
    public class SecureController : Controller
    {
        protected virtual new CustomPrincipal User
        {
            get
            {
                if( HttpContext.User.Identity.IsAuthenticated )
                    return HttpContext.User as CustomPrincipal;

                else
                    return CustomPrincipal.Anonymous;
            }
        }
    }
}