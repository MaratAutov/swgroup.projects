﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Account.Models;
using Account.DAO;
using Account.Security;

namespace Account.Controllers
{
    [Authorize]
    [ValidateInput(false)]
    public class NewsController : SecureController
    {
        [AllowAnonymous]
        public ActionResult Index(string year, string month)
        {
            if (year == null)
            {
                year = DateTime.Now.Year.ToString();
            }
            DateTime date1 = new DateTime();
            
            if (month != null) date1 = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 01);
            else date1 = new DateTime(Convert.ToInt32(year), 01, 01);
            
            var news = NewsService.GetAll(
                year,
                month,
                date1,
                null,
                "DatePosted",
                "desc"
            );
            if (Session["NewsTiming"] == null)
            {
                var timing = new Dictionary<int, List<int>>();
                foreach (var rec in NewsService.GetTiming()) {
                    if (!timing.ContainsKey(rec.Year)) {
                        timing.Add(rec.Year, new List<int>());
                    }
                    if (!timing[rec.Year].Contains(rec.Month)) {
                        timing[rec.Year].Add(rec.Month);
                    }
                }
                Session["NewsTiming"] = timing;
            }
            return PartialView(news);
        }

        public ActionResult Manage(FilterTableModel<NewsModel, News> table)
        {
            if (!User.Account.IsAdmin)
                throw new HttpException(403, "Доступ запрещен");

            InitNewsTable(table);
            return View(table);
        }

        [HttpGet]
        [AllowAdminOnly]
        public ActionResult Add()
        {
            return View(new NewsModel());
        }

        [HttpPost]
        [AllowAdminOnly]
        public ActionResult Add(NewsModel m)
        {
            NewsService.Add(User.Account.ID, m.Body);
            return RedirectToAction("Manage");
        }

        [HttpGet]
        [AllowAdminOnly]
        public ActionResult Edit(string id)
        {
            var news = new NewsModel();
            news.From(NewsService.Get(id));
            return View(news);
        }

        [HttpPost]
        [AllowAdminOnly]
        public ActionResult Edit(NewsModel m)
        {
            try
            {
                NewsService.Update(
                    m.ID,
                    User.Account.ID,
                    m.Body
                );
                return RedirectToAction("Manage");
            }
            catch (ArgumentException aex)
            {
                throw new HttpException(404, aex.Message);
            }
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            NewsService.Delete(id);
            return RedirectToAction("Manage");
        }

        public static void InitNewsTable(FilterTableModel<NewsModel, News> table)
        {
            if (table.DataModel == null)
            {
                table.DataModel = new NewsModel();
                table.SortingField = "DatePosted";
                table.SortingType = "desc";
            }
            if (table.DataList == null)
                table.DataList = new PagedList<News>();

            table.DataList.DataQuery = NewsService.GetAll(
                null,
                null,
                table.DataModel.DatePosted,
                table.DataModel.Body,
                table.SortingField,
                table.SortingType
            );
        }
    }
}
