﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Script.Serialization;
using System.Configuration;
using Account.Filters;
using Account.Models;
using Account.Security;
using Account.DAO;
using Account.Tools;
using System.IO;
using NLog;


namespace Account.Controllers
{
    [Authorize]
    public class AccountController : SecureController
    {
        //
        // GET: /Account/Login

        private Logger _logger = LogManager.GetLogger("AccountController");

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            _logger.Debug("Connecting user from brouser: {0}; Version: {1}; from IP: {2};", HttpContext.Request.Browser.Browser, HttpContext.Request.Browser.Version, HttpContext.Request.ServerVariables["REMOTE_ADDR"]);
            returnUrl = returnUrl ?? "/Home/Index";
            if ((User.Identity.IsAuthenticated) && ((User.Account!=null) || (User.Individual!= null)))
                return Redirect(returnUrl);

            ViewBag.ReturnUrl = returnUrl;
          
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        private void RedirectAndCookie(bool remember,string username,UserAccount user,string usr)
        {
            JavaScriptSerializer serializer =
                            new JavaScriptSerializer();
            string userData = serializer.Serialize(user);

            DateTime expires = DateTime.Now.AddDays(1);
            if (remember)
            {
                expires = DateTime.Now.AddYears(1);
            }

            FormsAuthenticationTicket authTicket =
                new FormsAuthenticationTicket(
                     1,
                     username,
                     DateTime.Now,
                     expires,
                     remember,
                     userData
                );

            

            string encTicket = FormsAuthentication.Encrypt(
                authTicket
            );

            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            HttpCookie faCookieUr = new HttpCookie("IsUr", usr);

            FormsAuthentication.SetAuthCookie(username, true);
            faCookie.Expires = authTicket.Expiration; 

            Response.Cookies.Add(faCookie);
            Response.Cookies.Add(faCookieUr);
        }

        private bool _userExist;

        public bool UserExist
        {
            get
            {
                return _userExist;
            }
            set
            {
                _userExist = value;
            }
        }


        public UserAccount UsersExist(bool fiz,string username,string password)
        {
            UserExist = false;

            UserAccount user = new UserAccount();

            if (!fiz)
            {
                user = UserService.Get(
                    username, password
                );

                if (user != null)
                {
                    UserExist = true;                    
                }                
            }
            else
            {
                UserIndividual user2 = UserIndividualService.Get(
                    username, password
                );

                user.IsActive = user2.IsActive;
                user.Login = user2.Login;
                user.Password = user2.Password;
                user.INN = user2.UC_INN;
                user.FIO = user2.SecondName + " " + user2.MiddleName + " " + user2.FirstName;

                if (user2 != null)
                {
                    UserExist = true;                   
                }                
            }
            return user;
        }
        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            _logger.Debug("Logging user from brouser: {0}; Version: {1}; from IP: {2};", HttpContext.Request.Browser.Browser, HttpContext.Request.Browser.Version, HttpContext.Request.ServerVariables["REMOTE_ADDR"]);
            _logger.Debug("User model: {0}", model);
            bool isActiv = false;
            string s = returnUrl;
            if (ModelState.IsValid)
            {

                try
                {
                    UserAccount user = UsersExist(model.IAmFiz, model.UserName, model.Password);

                    if (UserExist)
                    {
                        if (user.IsActive)
                        {
                            isActiv = true;
                        }
                    }

                    if ((UserExist) && (isActiv))
                    {
                        if (model.IAmFiz)
                            RedirectAndCookie(model.RememberMe, model.UserName, user, "0");
                        else if (!model.IAmFiz)
                            RedirectAndCookie(model.RememberMe, model.UserName, user, "1");
                        return RedirectToAction("Index", "Home");
                    }
                }
                catch (Exception ex)
                {
                    _logger.DebugException("Error logging user IP: " + HttpContext.Request.ServerVariables["REMOTE_ADDR"], ex);
                }
            }

            if (!UserExist)
            {
                ModelState.AddModelError("", "Неверное имя пользователя или пароль.");                
            }
            else if (!isActiv)
            {
                ModelState.AddModelError("", "Пользователь не зарегистрирован.");               
            }

            return View(model);            
        }

        //
        // Post: /Account/Logout

        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();            
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult ResetPasswordUr()
        {
            var acc = new UserAccountModel();
            acc.Password = Membership.GeneratePassword(8, 1);
            return PartialView("_ResetPasswordPartialUr", acc);
        }

        [HttpPost]
        public ActionResult ResetPasswordFiz()
        {
            var acc = new UserIndividualModel();
            acc.Password = Membership.GeneratePassword(8, 1);
            return PartialView("_ResetPasswordPartialFiz", acc);
        }

        //
        // GET: /Account/RegisterUr

        [AllowAdminOnly]
        [HttpGet]
        public ActionResult RegisterUr()
        {
            UserAccountModel newAccount = new UserAccountModel()
            {
                Password = Membership.GeneratePassword(8, 1),
                IsActive = true
            };
            return View(newAccount);
        }

        //
        // POST: /Account/RegisterUr

        [AllowAdminOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterUr(UserAccountModel model)
        {
            var dups = UserService.Exists(model.Login, model.INN, model.OGRN);
            foreach (var u in dups)
            {
                if (u.Login == model.Login)
                    ModelState.AddModelError("Login", "Данный логин уже зарегистрирован");
                if (u.INN == model.INN)
                    ModelState.AddModelError("INN", "Данный ИНН уже зарегистрирован");
                if (u.OGRN == model.OGRN)
                    ModelState.AddModelError("OGRN", "Данный ОГРН уже зарегистрирован");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    UserService.Add(
                        model.Login,
                        model.Password,
                        model.Company,
                        model.Phone,
                        model.FIO,
                        model.INN,
                        model.OGRN,
                        model.IsActive,
                        false
                    );
                    return RedirectToAction("ParticipantsUr", "Home");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }
            return View(model);
        }

        [AllowSelfOrAdmin]
        [HttpGet]
        public ActionResult EditUr(string id)
        {
            UserAccount acc = UserService.Get(id);
            return View(new UserAccountModel(acc));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUr(UserAccountModel model)
        {
            try
            {
                bool isActive;
                UserAccount acc = UserService.Get(model.Ident);
                isActive = acc.IsActive;

                UserService.Update(
                    model.Ident,
                    model.Login,
                    model.Password,
                    model.Company,
                    model.Phone,
                    model.FIO,
                    model.INN,
                    model.OGRN,
                    model.IsActive
                );

                if ((!isActive) && (model.IsActive))
                {
                    using (var cl = new MailService.MailServiceSoapClient())
                    {

                        cl.EnqueueForSend(new Tools.MailService.OutMessage()
                        {
                            From = ConfigurationManager.AppSettings["NotificationEmail"],
                            To = model.Login,
                            Subject = "Ваша учетная запись активирована",
                            Body = "Ваша учетная запись активирована",
                            IsHtml = true
                        });

                    }
                }
                else if (ORM.GetHash(model.Password) != model.OldPassword)
                {
                    using (var cl = new MailService.MailServiceSoapClient())
                    {
                        cl.EnqueueForSend(new Tools.MailService.OutMessage()
                        {
                            From = ConfigurationManager.AppSettings["NotificationEmail"],
                            To = model.Login,
                            Subject = "Портал СБРФР изменение пароля",
                            Body = String.Format(@"Уважаемый пользователь,<br><br>

                                Пароль для входа в личный кабинет был изменен
                                Новый пароль: {0}<br><br>

                                С уважением,
                                Администрация портала СБРФР", model.Password),
                            IsHtml = true
                        });
                    }
                }

                
                
                return RedirectToAction(
                    "Requisites", "Home", new { id = model.Login }
                );
            }
            catch
            {
                ModelState.AddModelError(
                   "UserAccountModel",
                   "Непредвиденная ошибка обработки данных."
               );
            }
            return View(model);
        }

        [AllowSelfOrAdmin]
        [HttpGet]
        public ActionResult EditFiz(string id)
        {
            UserIndividual acc = UserIndividualService.Get(id);
            return View(new UserIndividualModel(acc));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditFiz(UserIndividualModel model)
        {
            try
            {
                bool isActive;               
                UserIndividual acc = UserIndividualService.Get(model.Login);
                isActive = acc.IsActive;

                UserIndividualService.Update(
                    model.Login,
                    model.Password,
                    model.IsActive,
                    model.FirstName,
                    model.SecondName,
                    model.MiddleName,
                    model.BirthDate,
                    model.UC_Name,
                    model.UC_INN,
                    model.Info
                );


                if ((!isActive) && (model.IsActive))
                {
                    using (var cl = new MailService.MailServiceSoapClient())
                    {

                        cl.EnqueueForSend(new Tools.MailService.OutMessage()
                        {
                            From = ConfigurationManager.AppSettings["NotificationEmail"],
                            To = model.Login,
                            Subject = "Ваша учетная запись активирована",                            
                            Body = "Ваша учетная запись активирована",
                            IsHtml = true
                        });

                    }
                }

                return RedirectToAction(
                    "RequisitesIn", "Home", new { id = model.Login });
            }
            catch
            {
                ModelState.AddModelError(
                   "UserIndividualModel",
                   "Непредвиденная ошибка обработки данных."
               );
            }
            return View(model);
        }

        [AllowAdminOnly]
        [HttpPost]
        public ActionResult DeleteUr(string id, string returnUrl)
        {
            if (User.Account.Login == id)
            {
                // Delete self ?
            }
            else
                UserService.Delete(id);

            return Redirect(returnUrl);
        }

        [AllowAdminOnly]
        [HttpPost]
        public ActionResult DeleteFiz(string id, string returnUrl)
        {
            if (User.Account.Login == id)
            {
                // Delete self ?
            }
            else
                UserIndividualService.Delete(id);

            return Redirect(returnUrl);
        }

        //
        // GET: /Account/RegisterFiz

        [AllowAdminOnly]
        [HttpGet]
        public ActionResult RegisterFiz()
        {
            UserIndividualModel newAccount = new UserIndividualModel()
            {
                Password = Membership.GeneratePassword(8, 1),
                IsActive = true
            };
            return View(newAccount);
        }

        //
        // POST: /Account/RegisterFiz

        [AllowAdminOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterFiz(UserIndividualModel model)
        {
            var dups = UserIndividualService.Exists(model.Login, model.Info);
            foreach (var u in dups)
            {
                if (u.Login == model.Login) {
                    ModelState.AddModelError("Login", "Данный логин уже зарегистрирован");
                } else {
                    ModelState.AddModelError("Info.Passport_Number", "Данные паспортные данные уже зарегистрированы");
                    ModelState.AddModelError("Info.Passport_Seria", "Данные паспортные данные уже зарегистрированы");
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    UserIndividualService.Add(
                        model.Login,
                        model.Password,
                        model.FirstName,
                        model.SecondName,
                        model.MiddleName,
                        model.BirthDate,
                        model.UC_Name,
                        model.UC_INN,
                        model.Info,
                        model.IsActive
                    );
                    return RedirectToAction("ParticipantsFiz", "Home");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult RegistrationRequest()
        {
            return View();
        }     


        [AllowAnonymous]
        [HttpGet]
        public ActionResult RegistrationRequestUr()
        {
            return View(new RegistrationRequestUrModel());
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegistrationRequestUr(RegistrationRequestUrModel model)
        {
            ValidateInn(model.INN, "INN");
            ValidateOgrn(model.OGRN, "OGRN");

            var dups = UserService.Exists(model.Login, model.INN, model.OGRN);
            foreach (var u in dups)
            {
                if (u.Login == model.Login)
                    ModelState.AddModelError("Login", "Данный логин уже зарегистрирован");
                if (u.INN == model.INN)
                    ModelState.AddModelError("INN", "Данный ИНН уже зарегистрирован");
                if (u.OGRN == model.OGRN)
                    ModelState.AddModelError("OGRN", "Данный ОГРН уже зарегистрирован");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    UserService.Add(
                        model.Login,
                        model.Password,
                        model.Company,
                        model.Phone,
                        model.FIO,
                        model.INN,
                        model.OGRN,
                        false,
                        false
                    );
                }
                catch(Exception ex)
                {
                    ModelState.AddModelError(
                       "RegistrationRequestModel",
                       "Непредвиденная ошибка обработки данных."
                   );
                    _logger.Debug(string.Format("Запрос на регистраицю ЮЛ - ошибка сохранения пользователя {0} - {1}", model.Login, ex.Message));
                    return View(model);
                    
                }

                MailService.MailServiceSoapClient cl_accept = null;
                try
                {
                    cl_accept = new MailService.MailServiceSoapClient();
                    cl_accept.EnqueueForSend(new Tools.MailService.OutMessage()
                    {
                        From = ConfigurationManager.AppSettings["NotificationEmail"],
                        To = model.Login,
                        Subject = "Ваша заявка принята к рассмотрению",
                        Body = string.Format(
                            MailTemplates.RegistrationRequestAcceptedUr,
                            model.Company
                        ),
                        IsHtml = true
                    });
                    _logger.Debug(string.Format("Сообщение для {0} отправлено", model.Login));

                }
                catch (Exception ex)
                {
                    _logger.Debug(ex.Message);
                }
                finally
                {
                    if (cl_accept != null) ((IDisposable)cl_accept).Dispose(); 
                }

                MailService.MailServiceSoapClient cl_notify = null;
                try
                {
                    cl_notify = new MailService.MailServiceSoapClient();
                    cl_notify.EnqueueForSend(new Tools.MailService.OutMessage()
                    {
                        From = ConfigurationManager.AppSettings["NotificationEmail"],
                        To = ConfigurationManager.AppSettings["RegistrationRequestsEmail"],
                        Subject = "Запрос на регистрацию юридического лица",
                        Body = string.Format(
                            MailTemplates.RegistrationRequestNotifyAdminUr,
                            model.Company
                        ),
                        IsHtml = true
                    });
                    _logger.Debug(string.Format("Запрос на регистрацию юридического лица от {0} отправлен", model.Login));

                }
                catch (Exception ex)
                {
                    _logger.Debug(ex.Message);
                }
                finally
                {
                    if (cl_notify != null) ((IDisposable)cl_notify).Dispose();
                }

                return View("RegistrationAcceptedUr", model);                
            }

            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult RegistrationRequestFiz()
        {
            return View(new UserIndividualModel());
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegistrationRequestFiz(UserIndividualModel model)
        {
            model.Info.Passport_Seria = model.Info.Passport_Seria
                .Trim().Replace(" ", "");

            model.Info.Passport_Number = model.Info.Passport_Number
                .Trim().Replace(" ", "");

            ValidateNumeric(model.Info.Passport_Seria, 4, "Info.Passport_Seria");
            ValidateNumeric(model.Info.Passport_Number, 6, "Info.Passport_Number");

            var dups = UserIndividualService.Exists(model.Login, model.Info);
            foreach (var u in dups)
            {
                if (u.Login == model.Login) {
                    ModelState.AddModelError("Login", "Данный логин уже зарегистрирован");
                } else {
                    ModelState.AddModelError("Info.Passport_Number", "Данные паспорнтные данные уже зарегистрированы");
                    ModelState.AddModelError("Info.Passport_Seria", "Данные паспорнтные данные уже зарегистрированы");
                }
            }
           
            if (ModelState.IsValid)
            {
                try
                {
                    UserIndividualService.Add(
                        model.Login,
                        model.Password,
                        model.FirstName,
                        model.SecondName,
                        model.MiddleName,
                        model.BirthDate,                        
                        model.UC_Name,
                        model.UC_INN,
                        model.Info,
                        false
                    );

                    using (var cl = new MailService.MailServiceSoapClient())
                    {

                        cl.EnqueueForSend(new Tools.MailService.OutMessage()
                        {
                            From = ConfigurationManager.AppSettings["NotificationEmail"],
                            To = model.Login,
                            Subject = "Ваша заявка принята к рассмотрению",
                            Body = string.Format(
                                MailTemplates.RegistrationRequestAcceptedFiz,
                                model.FirstName + " " + model.MiddleName + " " + model.SecondName
                            ),
                            IsHtml = true
                        });



                        cl.EnqueueForSend(new Tools.MailService.OutMessage()
                        {
                            From = ConfigurationManager.AppSettings["NotificationEmail"],
                            To = ConfigurationManager.AppSettings["RegistrationRequestsEmail"],
                            Subject = "Запрос на регистрацию физического лица",
                            Body = string.Format(
                                MailTemplates.RegistrationRequestNotifyAdminFiz,
                                model.FirstName + " " + model.MiddleName + " " + model.SecondName
                            ),
                            IsHtml = true
                        });
                    }

                    return View("RegistrationAcceptedFiz", model);
                }
                catch
                {
                    ModelState.AddModelError(
                       "UserIndividualModel",
                       "Непредвиденная ошибка обработки данных."
                   );
                }
            }
            return View(model);
        }

        [AllowSelfOrAdmin]
        [HttpGet]
        public ActionResult ChangePasswordUr(string id)
        {
            return View(new LocalPasswordModel() {
                Login = id
            });
        }

        [HttpPost]
        public ActionResult ChangePasswordUr(LocalPasswordModel model)
        {
            if (ModelState.IsValid)
            {                
                UserAccount user = UserService.Get(
                    model.Login, model.OldPassword
                );
                if (user != null)
                {
                    try
                    {
                        UserService.ChangePassword(
                            model.Login, model.NewPassword
                        );
                        
                        if (ConfigurationManager.AppSettings["NotificationEmail"] != null)
                        { 
                                using (var cl = new MailService.MailServiceSoapClient())
                                {  
                                        cl.EnqueueForSend(new Tools.MailService.OutMessage()
                                        {
                                            From = ConfigurationManager.AppSettings["NotificationEmail"],
                                            To = model.Login,
                                            Subject = "Портал СБРФР изменение пароля",
                                            Body = String.Format(@"Уважаемый пользователь,<br><br>

                                    Пароль для входа в личный кабинет был изменен
                                    Новый пароль: {0}<br><br>

                                    С уважением,
                                    Администрация портала СБРФР", model.NewPassword),
                                            IsHtml = true
                                        });                                       
                                    
                                }                           
                            
                        }
                        return RedirectToAction("Requisites", "Home", new { id = model.Login });
                    }
                    catch
                    {
                        ModelState.AddModelError(
                           "LocalPasswordModel",
                           "Непредвиденная ошибка обработки данных."
                       );
                    }
                }
                else
                {
                    ModelState.AddModelError(
                       "OldPassword",
                       "Введен неверный пароль"
                   );
                }
            }
            
            return View(model);            
        }

        [HttpPost]
        public ActionResult ChangePasswordFiz(LocalPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                UserIndividual user = UserIndividualService.Get(
                    model.Login, model.OldPassword
                );
                if (user != null)
                {
                    try
                    {
                        UserIndividualService.ChangePassword(
                            model.Login, model.NewPassword
                        );

                        if (ConfigurationManager.AppSettings["NotificationEmail"] != null)
                        {
                            using (var cl = new MailService.MailServiceSoapClient())
                            {
                                cl.EnqueueForSend(new Tools.MailService.OutMessage()
                                {
                                    From = ConfigurationManager.AppSettings["NotificationEmail"],
                                    To = model.Login,
                                    Subject = "Портал СБРФР изменение пароля",
                                    Body = String.Format(@"Уважаемый пользователь,<br><br>

                                Пароль для входа в личный кабинет был изменен
                                Новый пароль: {0}<br><br>

                                С уважением,
                                Администрация портала СБРФР", model.NewPassword),
                                    IsHtml = true
                                });
                            }
                        }

                        return RedirectToAction("RequisitesIn", "Home", new { id = model.Login });
                    }
                    catch
                    {
                        ModelState.AddModelError(
                           "LocalPasswordModel",
                           "Непредвиденная ошибка обработки данных."
                       );
                    }
                }
                else
                {
                    ModelState.AddModelError(
                       "OldPassword",
                       "Введен неверный пароль"
                   );
                }
            }

            return View(model);
        }


        [AllowSelfOrAdmin]
        [HttpGet]
        public ActionResult ChangePasswordFiz(string id)
        {
            return View(new LocalPasswordModel()
            {
                Login = id
            });
        }


        [AllowAdminOnly]
        public ActionResult Impersonate(string id)
        {
            if (id == User.Account.Login)
            {
                Session.Remove("Impersonated");
                return RedirectToAction("Index", "Home");
            }
            else
            {
                Session.Add("Impersonated", id);
                return RedirectToAction("Outgoing", "Home", new { id = id });
            }            
        }

        [AllowSelfOrAdmin]
        [HttpPost]
        public ActionResult ChangeNotificationUr(string id, bool enabled)
        {
            UserService.ChangeNotification(id, enabled);
            return Json(null);
        }

        [AllowSelfOrAdmin]
        [HttpGet]
        public ActionResult InfoFiz(string id)
        {
            var model = UserIndividualService.Get(id);
            return View(model);
        }

        [NonAction]
        private void ValidateInn(string value, string fieldName)
        {
            if (string.IsNullOrWhiteSpace(value) || value.Length != 10)
                return;

            if (!ChekInnUridicalPerson(value))
            {
                ModelState.AddModelError(fieldName, "Проверьте правильность введенного значения");
            }
        }

        [NonAction]
        private void ValidateOgrn(string value, string fieldName)
        {
            if (string.IsNullOrWhiteSpace(value) || value.Length != 13)
                return;

            if (!CheckOgrn(value))
            {
                ModelState.AddModelError(fieldName, "Проверьте правильность введенного значения");
            }
        }

        [NonAction]
        public bool ChekInnUridicalPerson(string strInn)
        {
            int[] koeficents = new int[] { 2, 4, 10, 3, 5, 9, 4, 6, 8, 0 };
            int summ = 0;

            for (int i = 0; i < 10; i++)
            {
                int current;
                if (int.TryParse(strInn[i].ToString(), out current))
                    summ += current * koeficents[i];
                else
                    return false;
            }

            int controlIndex = summ % 11;
            if (controlIndex > 9) controlIndex = controlIndex % 10;

            int lastIndex = int.Parse((strInn.ToCharArray())[9].ToString());
            return (controlIndex == lastIndex);
        }

        [NonAction]
        public bool CheckOgrn(string strOgrn)
        {
            foreach (char item in strOgrn.ToCharArray())
            {
                int tmp;
                if (!int.TryParse(item.ToString(), out tmp)) return false;
            }

            string chekSumm = strOgrn.Substring(0, strOgrn.Length - 1);
            int chekIndex = int.Parse((strOgrn.ToCharArray())[strOgrn.Length - 1].ToString());

            int result = Convert.ToInt32(Int64.Parse(chekSumm) % 11);
            if (result == 10) result = 0;

            return (result == chekIndex);
        }

        [NonAction]
        protected void ValidateNumeric(string str, int len, string field)
        {
            Regex seriaRegex = new Regex(@"^\d{" + len.ToString() + "}$");
            if (!seriaRegex.IsMatch(str))
                ModelState.AddModelError(
                    field,
                    "Неверное значение"
                );
        }
    }
}
