﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;
using Account.Security;

namespace Account.Attributes
{
    public class LoggingFilterAttribute : ActionFilterAttribute
    {
        static Logger _logger = LogManager.GetLogger("User Actions");

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                _logger.Debug("Request; User: {0}; IP: {1}; Request: {2}/{3}; {4}", (filterContext.HttpContext.User ?? new CustomPrincipal()).Identity.Name,
                    filterContext.HttpContext.Request.ServerVariables["REMOTE_ADDR"], filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName,
                    GetParametersStr(filterContext.ActionParameters));
            }
            catch (Exception ex)
            {
                _logger.Error("Cannot log user action, exception: {0}", ex);
            }
            base.OnActionExecuting(filterContext);
        }

        string GetParametersStr(IDictionary<string, object> parameters)
        {
            string result = "parameters: { ";
            result += string.Join(";", parameters.Select(x => x.Key + "=" + x.Value));
            return result += " }";
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            try
            {
                if (filterContext.Exception != null)
                    _logger.Error("Thrown exception; User: {0}; IP: {1}; Request: {2}; Exception {3}", (filterContext.HttpContext.User ?? new CustomPrincipal()).Identity.Name,
                        filterContext.HttpContext.Request.ServerVariables["REMOTE_ADDR"], filterContext.ActionDescriptor.ActionName, filterContext.Exception);
            }
            catch (Exception ex)
            {
                _logger.Error("Cannot log user action when thrown exception, exception: {0}", ex);
            }
            base.OnActionExecuted(filterContext);
        }
    }
}