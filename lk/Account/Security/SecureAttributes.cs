﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Account.DAO;
using System.Web.Profile;
using System.Web.Routing;


namespace Account.Security
{
    public class AllowSelfOrAdminAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {            
            string id = filterContext.ActionParameters["id"].ToString();
            try
            {
                CustomPrincipal User = (CustomPrincipal)filterContext.HttpContext.User;
                if (User.IsUr)
                {
                    if (id != User.Account.Login && !User.Account.IsAdmin)
                        throw new HttpException(403, "Доступ запрещен");
                }
            }
            catch
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "Login"
                }));
            } 

            base.OnActionExecuting(filterContext);
        }
    }

    public class AllowSelfOrAdminDownloadAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string id = filterContext.ActionParameters["id"].ToString();

            try
            {
                CustomPrincipal User = (CustomPrincipal)filterContext.HttpContext.User;
                var package = PackageService.GetPackage(Convert.ToInt32(id));
                if (User.IsUr)
                {
                    if (package.OwnerID != User.Account.ID && !User.Account.IsAdmin)
                        throw new HttpException(403, "Доступ запрещен");
                }
            }
            catch
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "Login"
                }));
            } 

            base.OnActionExecuting(filterContext);
        }
    }

    public class AllowAdminOnlyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                CustomPrincipal User = (CustomPrincipal)filterContext.HttpContext.User;

                if (!User.Account.IsAdmin)
                    throw new HttpException(403, "Доступ запрещен");
            }
            catch
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "Login"
                }));
            }

            base.OnActionExecuting(filterContext);
        }
    }   
}