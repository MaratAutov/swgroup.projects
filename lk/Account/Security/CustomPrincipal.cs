﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

using Account.DAO;


namespace Account.Security
{
    /// <summary>
    /// Данный класс используется добавляет возможность обращаться к данным
    /// профиля пользователя хранящемуся в базе напрямую через объект User.
    /// </summary>
    public class CustomPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role) { return false; }

        public bool IsUr { get; set; }

        public CustomPrincipal()
        {
            this.Identity = new GenericIdentity("");
        }

        public CustomPrincipal(string name)
        {
            this.Identity = new GenericIdentity(name);
        }

        public static CustomPrincipal Anonymous
        {
            get
            {
                return new CustomPrincipal();
            }
        }

        public UserAccount Account { get; set; }

        public UserIndividual Individual { get; set; }
    }
}