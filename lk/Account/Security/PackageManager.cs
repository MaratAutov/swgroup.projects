﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Configuration;

using ICSharpCode.SharpZipLib.Zip;
using Progresoft.Accounts.ElectroSignature;
using NLog;

using Account.Models;
using Account.Filters;
using Account.DAO;
using Account.SEDService;
using Account.Tools;


namespace Account {

/// <summary>
/// Summary description for PackageManager
/// </summary>
public static class PackageManager
{
    public static Logger log;
    /// <summary>
    /// Root directory for Xsd model templates used for XTDD validation
    /// </summary>
    private static string _xsdRootDir;

    /// <summary>
    /// Root directory for storing uploaded reports that passed validation
    /// </summary>
    private static string _archiveDir;

    static PackageManager()
    {
        string logFile = ConfigurationManager.AppSettings["LogFile"];
        log = NLogger.GetLog("PackageManager", logFile);

        _xsdRootDir = Path.GetFullPath(LoadConf("XsdRootDir"));
        _CheckExistense(
            _xsdRootDir,
            true,
            "Неверно задана или отсутствует конфигурация пути файлов XSD\n{0}",
            _xsdRootDir
        );

        _archiveDir = LoadConf("ArchiveDir");
    }

    /// <summary>
    /// Verify uploaded package
    /// 
    /// Raises:
    ///     InvalidPackageException
    ///     ConfigurationErrorsException
    /// </summary>
    /// <param name="fName">package file name with extension</param>
    /// <param name="stream">package data stream</param>
    public static int Process(string fName, Stream stream, string name, int ownerId)
    {
        log.Debug(
            "Начали проверку '{0}' [{1} bytes]",
            fName,
            stream.Length
        );
        int streamLen = (int)stream.Length;
        byte[] report = new byte[streamLen];
        stream.Read(report, 0, streamLen);
        stream.Position = 0;

        IReportPackage package = ReportPackageManager.Load(fName, stream);
        byte[] data = package.SignedData;
        byte[] body = package.ReportBody;
        byte[] sign = package.ReportSign;
        log.Debug(
            "{0} :: Пакет успешно открыт как :: {1} " +
            "[Отчет: {2} bytes, Подпись: {3} bytes]",
            fName,
            package.GetType().Name,
            body.Length,
            sign.Length
        );
        
        #warning Signature verification commented
        //*
        VerificationResult res = SignatureEngine.Instance.Verify(
            data, sign, package.SignerExt
        );
        //*/
        if (res.Status == SignatureStatus.SignatureError && res.ErrorMessage.Contains("Неправильное значение хеша"))
            res.Status = SignatureStatus.Valid;

        //*
        if (res.Status != SignatureStatus.Valid)
            throw new InvalidPackageException( res.ErrorMessage );
        //*/

        string version;
        string externalId;
        string model;
        string type;

        VerifyXtddValid(
            fName, body,
            out version,
            out externalId,
            out model, 
            out type
        );

        // Done processing? Think about others - restore initial stream position.
        stream.Seek(0, SeekOrigin.Begin);
        log.Debug("{0} :: ПАКЕТ ВАЛИДЕН", fName);

        Package dbPackage = PackageService.GetOrCreate(
            name,
            report,
            ownerId,
            res.SingatureType,
            version,
            externalId,
            model,
            type
        );
        #warning Duplicate Package exception disabled
        /*
        if (dbPackage.Existing)
            throw new InvalidPackageException(
                "Данный пакет отчетности уже загружен"
            );
        //*/
        
        Store(fName, stream, dbPackage.ID.ToString());

        using (SEDServiceSoapClient cl = new SEDServiceSoapClient())
        {
            cl.EnqueueForProcessing(
                new InteropPackage()
                {
                    Id = dbPackage.ID,
                    XmlData = package.ReportBody,
                    ExternalId = externalId,
                    Model = model,
                    Type = type
                }
            );
        }

        return dbPackage.ID;
        //throw new InvalidPackageException("Пакет верифицирован, будем посылать его в SED как сделаем SED-Сервис :)");
    }

    /// <summary>
    /// Store package in archive
    /// 
    /// Raises:
    ///     ConfigurationErrorsException
    /// </summary>
    /// <param name="fName">uploaded package name</param>
    /// <param name="stream">uploaded package data stream</param>
    /// <param name="uuid">uploaded package processing ID</param>
    private static void Store(string fName, Stream stream, string uuid)
    {
        log.Debug(
            "{0} :: Начали сохранение пакета :: {1}",
            fName,
            uuid
        );

        string archiveRoot = Path.GetFullPath(_archiveDir);
        DateTime today = DateTime.Now;
        
        string targetDir = Path.Combine(
            archiveRoot,
            today.Year.ToString(),
            today.Month.ToString(),
            today.Day.ToString(),
            uuid
        );
        if (!Directory.Exists(targetDir))
        {
            try
            {
                Directory.CreateDirectory(targetDir);
            }
            catch( Exception exc )
            {
                string msg = string.Format(
                    "Не удалось создать архивную директорию: {0}",
                    targetDir
                );
                ConfigurationErrorsException e = 
                    new ConfigurationErrorsException(msg, exc);
            }
        }

        string targetFile = Path.Combine(targetDir, fName + ".bkp");
        try
        {
            using (FileStream fStream = File.Create(targetFile))
            {
                int len = (int)stream.Length;
                byte[] buffer = new byte[len];
                stream.Read(buffer, 0, len);
                fStream.Write(buffer, 0, len);
            }
        }
        catch( Exception exc )
        {
            string msg = string.Format(
                "Не удалось сохранить файл в архиве: {0}",
                targetFile
            );
            ConfigurationErrorsException e =
                new ConfigurationErrorsException(msg, exc);            
        }
        finally
        {
            // Done processing? Think about others - restore initial stream position.
            stream.Seek(0, SeekOrigin.Begin);
        }
        log.Debug(
            "{0} :: Пакет успешно сохранен на диск :: {1}",
            fName,
            uuid
        );
    }

    /// <summary>
    /// Verification for XTDD validness using XSD scheme
    /// Scheme is choosed using XTDD root element attributes:
    /// "appVersion" - version of supported reports
    /// "NamespaceURI" - should end with one of the following values:
    ///     АЛ / ЗПУ / ЛКИ / ПУРЦБ / Регистраторы / УММ / УРКИ / УСИО
    /// Root element name is used to choose concrete XSD scheme file
    /// 
    /// E.g. following root element:
    ///     <av:ПакетСООУРКИ031 ... appVersion="2.16.2" xmlns:av="http://example/УРКИ">
    /// will be validated with:
    ///     ...\2.16.2\УРКИ\УРКИ_ПакетСООУРКИ031.xsd
    /// 
    /// Raises:
    ///     ConfigurationErrorsException
    ///     InvalidPackageException
    /// </summary>
    /// <param name="data">XTDD report file represented with byte array</param>
    private static void VerifyXtddValid(
        string fName,
        byte[] data,
        out string version,
        out string externalId,
        out string model,
        out string type)
    {
        version = null;
        externalId = null;
        model = null;
        type = null;

        log.Debug(
            "{0} :: Начали верификацию",
            fName
        );

        using( MemoryStream stream = new MemoryStream(data) )
        {
            string xsdFolder    = null;
            string xsdScheme    = null;
            string tgtNamespace = null;

            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(stream);
            }
            catch( XmlException exc )
            {
                string msg = string.Format(
                    "XTDD: {0}", exc.Message
                );
                throw new InvalidPackageException(msg, exc);
            }
            XmlNode node = xmlDoc.DocumentElement;

            tgtNamespace = node.NamespaceURI;
            xsdFolder    = tgtNamespace.Split('/').Last();
            xsdScheme    = node.LocalName;
            externalId   = _GetAttributeValue(node, "externalId", true);
            version   = _GetAttributeValue(node, "appVersion");
            log.Debug(
                "{0} :: Readed XTDD root element :: [appVersion={1}, " +
                "xsdFolder={2}, xsdScheme={3}]",
                fName,
                version,
                xsdFolder,
                xsdScheme
            );

            string xsdVersionRoot = Path.Combine(_xsdRootDir, version);
            _CheckExistense(
                xsdVersionRoot,
                false,
                "Заданная версия отчета не поддерживается: {0}",
                version
            );

            string xsdSchemeRoot = Path.Combine(xsdVersionRoot, xsdFolder);
            _CheckExistense(
                xsdSchemeRoot,
                false,
                "Задано неизвестное пространство имен в XTDD файле: {0} ",
                xsdSchemeRoot
            );
            model = xsdFolder;

            string xsdSchemeFinal = Path.Combine(
                xsdSchemeRoot,
                string.Format("{0}_{1}.xsd", xsdFolder, xsdScheme)
            );
            _CheckExistense(
                xsdSchemeFinal,
                false,
                "Задан неизвестный тип отчета в XTDD файле: {0} ",
                string.Format("{0}_{1}", xsdFolder, xsdScheme)
            );
            type = xsdScheme;

            try
            {
                xmlDoc.Schemas.Add(tgtNamespace, xsdSchemeFinal);
            }
            catch( XmlSchemaException exc )
            {
                string msg = string.Format(
                    "XSD схема: {0}", exc.Message
                );
                throw new InvalidPackageException(msg, exc);
            }
            xmlDoc.Validate(new ValidationEventHandler(ValidationCallBack), node);
        }
    }

    /// <summary>
    /// Callback for XTDD validation
    /// 
    /// Raises:
    ///     InvalidPackageException
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    private static void ValidationCallBack(object sender, ValidationEventArgs args)
    {
        if( args.Severity == XmlSeverityType.Error )
            throw new InvalidPackageException(args.Message);
    }

    /// <summary>
    /// Gets XmlNode Attribute value or raise exception
    /// 
    /// Raises:
    ///     InvalidPackageException
    /// </summary>
    /// <param name="node">xml node to search in</param>
    /// <param name="name">attribute name</param>
    /// <returns>@string attribute value</returns>
    private static string _GetAttributeValue(XmlNode node, string name, bool safe=false)
    {
        if (node.Attributes[name] == null)
        {
            if (!safe)
                throw new InvalidPackageException(string.Format(
                    "Переменная '{0}' не найдена в корневом элементе",
                    name
                ));
            return null;
        }
        return node.Attributes[name].Value;
    }

    /// <summary>
    /// Check for file or directory existence
    /// 
    /// Raises:
    ///     ConfigurationErrorsException
    ///     InvalidPackageException
    /// </summary>
    /// <param name="path">file or directory path to chech</param>
    /// <param name="isConfigDir">
    /// if isConfigDir is 'true' ConfigurationErrorsException
    /// will be raised if dir not exists, else
    /// InvalidPackageException will be raised
    /// </param>
    /// <param name="errorMsg">error message in case of exception</param>
    /// <param name="formatArgs">format args for errorMsg</param>
    private static void _CheckExistense(string path, bool isConfigDir, string errorMsg, params string[] formatArgs)
    {
        if(path == null)
            throw new ConfigurationErrorsException(errorMsg);

        if (Directory.Exists(path) || File.Exists(path))
            return;

        string msg = string.Format(
            errorMsg,
            formatArgs
        );
        if (isConfigDir)
            throw new ConfigurationErrorsException(msg);
        else
            throw new InvalidPackageException(msg);
    }

    private static string LoadConf(string var)
    {
        return ConfigurationManager.AppSettings.Get(var);
    }

    public static void InitOutgoingTable(int userId, string year, FilterTableModel<PackageModel, PackageFullInfo> table)
    {
        if (table.DataModel == null)
        {
            table.DataModel = new PackageModel();
            table.SortingField = "UploadTime";
            table.SortingType = "desc";
        }
        if (table.DataList == null)
            table.DataList = new PagedList<PackageFullInfo>();

        table.DataList.DataQuery = PackageService.GetAll(
            userId,
            year,
            table.DataModel.UploadTime,
            table.DataModel.Name,
            table.DataModel.IncomingNumber,
            table.DataModel.IncomingDate,
            table.DataModel.SedNumber,
            table.DataModel.SedDate,
            table.SortingField,
            table.SortingType
        );
    }
}

}