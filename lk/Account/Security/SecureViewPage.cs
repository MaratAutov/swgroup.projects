﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Account.Security
{
    /// <summary>
    /// Этот класс позволяет заменяет User на CustomPrincipal внутри Razor
    /// вьюшек не имеющих модели, что позволяет легко обращаться к данным
    /// о текущем пользователе.
    /// </summary>
    public abstract class SecureViewPage : WebViewPage
    {
        public virtual new CustomPrincipal User
        {
            get { return base.User as CustomPrincipal; }
        }
    }

    /// <summary>
    /// Этот класс позволяет заменяет User на CustomPrincipal внутри Razor
    /// вьюшек имеющих модель.
    /// </summary>
    public abstract class SecureViewPage<TModel> : WebViewPage<TModel>
    {
        public virtual new CustomPrincipal User
        {
            get { return base.User as CustomPrincipal; }
        }
    }
}