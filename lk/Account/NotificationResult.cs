﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Account
{
    public class NotificationResult : FileContentResult
    {
        
        /// <summary>
        /// Статус обработки пакета
        /// </summary>
        private string Status;
        public NotificationResult(byte[] fileContents, string contentType,
            string fileName, string status)
            : base(fileContents, contentType)
        {
            base.FileDownloadName = fileName;
            this.Status = status;
        }
        public override void ExecuteResult(System.Web.Mvc.ControllerContext context)
        {
            context.HttpContext.Response.AppendHeader("FFMS-notification-status", Status);
            base.ExecuteResult(context);
        }
    }
}