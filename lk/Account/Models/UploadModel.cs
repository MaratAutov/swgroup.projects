﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Account.Models
{
    public class UploadModel
    {
        [Required]
        [Display(Name = "Файл документа")]
        public HttpPostedFileBase Report { get; set; }

        [Required]
        [Display(Name = "Наименование документа")]
        public string Name { get; set; }

        public override string ToString()
        {
            return String.Format("Upload File Name: {0};", Name);
        }
    }
}