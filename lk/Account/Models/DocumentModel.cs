﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Profile;
using System.ComponentModel.DataAnnotations;
using Account.DAO;
using Account.Filters;

namespace Account.Models
{
    public class DocumentModel : DAO.DAOObject
    {
        public int ID { get; set; }
        
        public int SenderId { get; set; }

        [FilterTable(Label = "Принят", Order = 1, Filter = FilterType.DateTimePicker)]
        public DateTime UploadTime { get; set; }

        [FilterTable(Label = "Наименование", Order = 2, DisplayPattern = "HtmlNamePattern")]
        public string Name { get; set; }

        [FilterTable(Label = "ИНН отправителя", Order = 4, Sortable = false)]
        public string SenderINN { get; set; }

        [FilterTable(Label = "Наименование отправителя", Order = 5, Sortable = false)]
        public string SenderName { get; set; }

        [FilterTable(Label = " ", Order = 6, DisplayPattern = "HtmlResponsePattern")]
        public string Response { get; set; }

        [FilterTable(Label = "Прочитано", Order = 3, Filter = FilterType.CheckBox, FilterLabel = "Только не прочитанные")]       
        public string ReadTime { get; set; }

        

        public void From(object objDAO)
        {
            var doc = (DAO.Document)objDAO;
            
            ID = doc.ID;
            Name = doc.Name;
            Response = "Ответить";
            UploadTime = doc.UploadTime;
            ReadTime = doc.ReadTime.HasValue ? doc.ReadTime.Value.ToString() : "-";
            SenderId = doc.OwnerID;
            
            UserAccount user = UserService.Get(doc.OwnerID);
            if (user != null)
            {
                SenderINN = user.INN;
                SenderName = user.Company;
            }
            else
            {
                SenderINN = "";
                SenderName = "";
            }
        }

        public string HtmlNamePattern
        {
            get
            {
                return string.Format(
                    "<a href='/Home/DownloadDoc/{0}'>{1}</a>",
                    ID,
                    Name
                );
            }
        }

        public string HtmlResponsePattern
        {
            get
            {
                return string.Format(
                    "<a href='/Home/WriteResponse/{0}'>Ответить</a>",SenderId);
            }
        }

        public override string ToString()
        {
            return String.Format("Document with Name: {0}; ID: {1}", Name, ID);
        }
    }
}