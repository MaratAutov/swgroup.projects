﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Mvc;
using Account.Filters;

namespace Account.Models
{
    [Serializable]
    public class IndividualProtectedInfo
    {
        [Required]
        [Display(Name = "Серия")]
        [FilterTable(Label = "Серия паспорта", Order = 6, Sortable=false, Insider="Info")]
        public string Passport_Seria { get; set; }

        [Required]
        [Display(Name = "Номер")]
        [FilterTable(Label = "Номер паспорта", Order = 7, Sortable = false, Insider = "Info")]
        public string Passport_Number { get; set; }

        [Required]
        [Display(Name = "Кем выдан")]
        public string Passport_GrantedBy { get; set; }

        [Required]
        [Display(Name = "Когда выдан")]
        public DateTime Passport_GrantedDate { get; set; }

        public override string ToString()
        {
            return String.Format("Passport info number: {0};", Passport_Number);
        }
    }   

    public class UserIndividualModel : DAO.DAOObject
    {
        [Required]
        [RegularExpression(@"^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-zA-Z0-9]{1}[a-zA-Z0-9\-]{0,62}[a-zA-Z0-9]{1})|[a-zA-Z])\.)+[a-zA-Z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$", ErrorMessage = "Неправильный e-mail адрес")]
        [Display(Name = "Логин")]
        [FilterTable(Label = "Логин", Order = 1, DisplayPattern = "HtmlLoginPattern")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        [StringLength(32, ErrorMessage = "Пароль слишком короткий (минимум {2} символов)", MinimumLength = 6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Повторный ввод пароля")]
        public string PasswordConfirm { get; set; }

        [Required]
        [Display(Name = "Имя")]
        [FilterTable(Label = "Имя", Order = 2)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Фамилия")]
        [FilterTable(Label = "Фамилия", Order = 3)]
        public string SecondName { get; set; }

        [Required]
        [Display(Name = "Отчество")]
        [FilterTable(Label = "Отчество", Order = 4)]
        public string MiddleName { get; set; }

        [Required]
        [Display(Name = "Дата рождения")]
        [FilterTable(Label = "Дата рождения", Order = 5, Filter = FilterType.DatePicker)]
        public DateTime BirthDate { get; set; }

        [FilterTable(IsContainer = true)]
        public IndividualProtectedInfo Info { get; set; }

        [Display(Name = "Наименование УЦ")]
        public string UC_Name { get; set; }

        [Display(Name = "ИНН УЦ")]
        public string UC_INN { get; set; }

        [Display(Name = "Активный")]
        [FilterTable(Label = "Статус", Order = 8, Sortable = false, Filter = FilterType.None, BoolValues = new string[] { "Не активный", "Активный" })]
        public bool IsActive { get; set; }

        public UserIndividualModel()
        {
            Info = new IndividualProtectedInfo();
        }

        public UserIndividualModel(DAO.UserIndividual user)
        {
            From(user);
        }

        public void From(object obj)
        {
            var user = (DAO.UserIndividual)obj;

            Login = user.Login;
            Password = user.Password;
            FirstName = user.FirstName;
            SecondName = user.SecondName;
            MiddleName = user.MiddleName;
            BirthDate = user.BirthDate;
            IsActive = user.IsActive;
            UC_INN = user.UC_INN;
            UC_Name = user.UC_Name;
            Info = (IndividualProtectedInfo)DAO.UserIndividualService.Decode(user.Info);
        }

        public string HtmlLoginPattern
        {
            get
            {
                return string.Format(
                    @"<a class=""user-name"" href=""#"">{0}</a>
                    <nav class=""edit-nav""><a class=""edit-nav-item edit-info"" href=""/Account/InfoFiz/{0}"" title=""Информация""></a>
                    <a class=""edit-nav-item edit-edit"" href=""/Account/EditFiz/{0}"" title=""Редактировать""></a>
                    <a class=""edit-nav-item edit-remove"" href=""#"" title=""Удалить"" onclick=""confirmDelete('{0}')""></a></nav>",
                    Login
                );
            }
        }

        public override string ToString()
        {
            return String.Format("Fiz user FirstName: {0}; Login: {1}", FirstName, Login);
        }
    }
}