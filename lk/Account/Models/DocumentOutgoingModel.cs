﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Profile;
using System.ComponentModel.DataAnnotations;
using Account.DAO;
using Account.Filters;

namespace Account.Models
{
    public class DocumentOutgoingModel : DAO.DAOObject
    {
        public int ID { get; set; }
        
        public int ReceiverId { get; set; }

        [FilterTable(Label = "Отправлен", Order = 1, Filter = FilterType.DateTimePicker)]
        public DateTime UploadTime { get; set; }

        [FilterTable(Label = "Наименование", Order = 2, DisplayPattern = "HtmlNamePattern")]
        public string Name { get; set; }

        [FilterTable(Label = "ИНН получателя", Order = 4, Sortable = false)]
        public string ReceiverINN { get; set; }

        [FilterTable(Label = "Наименование получателя", Order = 5, Sortable = false)]
        public string ReceiverName { get; set; }      

        
        public void From(object objDAO)
        {
            var doc = (DAO.Document)objDAO;
            
            ID = doc.ID;
            Name = doc.Name;
            UploadTime = doc.UploadTime;           
            ReceiverId = doc.OwnerID;


            UserAccount user = UserService.GetReceiverByDocID(doc.ID);
            if (user != null)
            {
                ReceiverINN = user.INN;
                ReceiverName = user.Company;
            }
            else
            {
                ReceiverINN = "";
                ReceiverName = "";
            }
        }

        public string HtmlNamePattern
        {
            get
            {
                return string.Format(
                    "<a href='/Home/DownloadDoc/{0}'>{1}</a>",
                    ID,
                    Name
                );
            }
        }

        public override string ToString()
        {
            return String.Format("Outgoing document with Name : {0}; ID: {1}", Name, ID);
        }
    }
}