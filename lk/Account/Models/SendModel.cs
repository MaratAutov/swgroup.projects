﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Account.Models
{
    public class SendModel
    {
        [Required]
        [Display(Name = "Файл документа")]
        public HttpPostedFileBase Report { get; set; }

        [Required]
        [Display(Name = "ИНН участника")]
        public string INN { get; set; }

        public override string ToString()
        {
            return String.Format("Sended File Name: {0}; INN: {1}", Report.FileName, INN);
        }
    }
}