﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

using Account.Filters;


namespace Account.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage="- обязательное поле")]
        [Display(Name = "Пользователь")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "- обязательное поле")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }

        [Display(Name = "Физическое лицо")]
        public bool IAmFiz { get; set; }

        public override string ToString()
        {
            return String.Format("Login: {0}; RememberMe: {1}, Fiz: {2}", UserName, RememberMe, IAmFiz);
        }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(32, ErrorMessage = "Пароль слишком короткий (минимум {2} символов)", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Повторный ввод пароля")]
        [Compare("NewPassword", ErrorMessage = "Введеные пароли не совпадают")]
        public string ConfirmPassword { get; set; }

        public string Login { get; set; }
    }

    public class UserAccountModel : DAO.DAOObject
    {
        [Required]
        [Display(Name = "ID")]
        public int Ident { get; set; }
        
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Логин")]
        [FilterTable(Label = "Логин", Order = 1, DisplayPattern = "HtmlLoginPattern")]
        public string Login { get; set; }

        [Required]
        [FilterTable(Label = "Дата создания", Order = 6, Filter = FilterType.DateTimePicker)]
        public DateTime CreateDate { get; set; }

        [Required]
        [StringLength(32, ErrorMessage = "Пароль слишком короткий (минимум {2} символов)", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        public string OldPassword { get; set; }

        [Required]
        [Display(Name = "Активный")]
        [FilterTable(Label = "Статус", Order = 5, Sortable = false, Filter = FilterType.None, BoolValues=new string[] {"Не активный", "Активный"})]
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "Наименование")]
        [FilterTable(Label = "Наименование участника", Order = 2)]
        public string Company { get; set; }

        [Required]
        [Display(Name = "Контактный телефон")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "ФИО контактного лица")]
        public string FIO { get; set; }

        [Required]
        [Display(Name = "ИНН")]
        [StringLength(10, MinimumLength=10)]
        [FilterTable(Label = "ИНН", Order = 3)]
        public string INN { get; set; }

        [Required]
        [Display(Name = "ОГРН")]
        [StringLength(13, MinimumLength = 13)]
        [FilterTable(Label = "ОГРН", Order = 4)]
        public string OGRN { get; set; }

        [Display(Name = "Получать почтовые уведомления")]
        public bool EnableNotification { get; set; }

        public UserAccountModel()
        {
           
        }
        
        public UserAccountModel(DAO.UserAccount acc)
        {
            From(acc);
        }

        public void From(object obj)
        {
            var acc = (DAO.UserAccount)obj;
            Ident = acc.ID;
            Login = acc.Login;
            Password = acc.Password;
            OldPassword = acc.Password;
            Company = acc.Company;
            IsActive = acc.IsActive;
            Phone = acc.Phone;
            FIO = acc.FIO;
            INN = acc.INN;
            OGRN = acc.OGRN;
            EnableNotification = acc.EnableNotification;
            CreateDate = acc.CreateDate;
        }

        public string HtmlLoginPattern
        {
            get
            {
                return string.Format(
                    @"<a class=""user-name"" href=""/Account/Impersonate/{0}"">{0}</a>
                    <nav class=""edit-nav""><a class=""edit-nav-item edit-info"" href=""/Home/Requisites/{0}"" title=""Информация""></a>
                    <a class=""edit-nav-item edit-edit"" href=""/Account/EditUr/{0}"" title=""Редактировать""></a>
                    <a class=""edit-nav-item edit-remove"" href=""#"" title=""Удалить"" onclick=""confirmDelete('{0}')""></a></nav>",
                    Login
                );
            }
        }

        public override string ToString()
        {
            return String.Format("UserAccountModel: {0}", Login);
        }
    }
}
