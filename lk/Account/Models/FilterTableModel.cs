﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Account.Filters;


namespace Account.Models
{
    public class FilterTableModel<TModel, TModelDAO>
    {
        public PagedList<TModelDAO> DataList { get; set; }
        public TModel DataModel { get; set; }

        public string SortingField { get; set; }
        public string SortingType { get; set; }
        public bool Visible { get; set; }

        public override string ToString()
        {
            return String.Format("FilterTableModel Sorting Field: {0}; Type: {1}", SortingField, SortingType);
        }
    }

    public class PagedList<TModelDAO>
    {
        public int MaxVisiblePage = 10;
        public List<int> PageSizing = new List<int>()
        {
            10, 25, 50            
        };

        public IEnumerable<TModelDAO> DataQuery;
        
        public int Page
        {
            get;
            set;
        }
        public int PageSize { get; set; }

        public PagedList()
        {
            Page = 0;
            PageSize = PageSizing[0];
        }

        public List<TModelDAO> GetCurrentPageList()
        {
            if (Page > PageCount)
                Page = PageCount - 1;
            else if (Page < 0)
                Page = 0;
            return DataQuery.Skip(Page * PageSize).Take(PageSize).ToList();
        }

        public int PageCount
        {
            get
            {
                return (int)Math.Ceiling((double)RecordCount / PageSize);
            }
        }

        private int _recordCount = -1;
        public int RecordCount
        {
            get
            {
                if (_recordCount == -1 && DataQuery != null)
                    _recordCount = DataQuery.Count();
                return _recordCount;
            }
        }
    }
}