﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using Account.Filters;

namespace Account.Models
{
    public class NewsModel : DAO.DAOObject
    {
        public int ID { get; set; }

        [FilterTable(Label = "Дата публикации", Order = 1, Filter = FilterType.DateTimePicker)]
        public DateTime DatePosted { get; set; }

        [FilterTable(Label = "Текст", Order = 2, DisplayPattern="HtmlBodyPattern")]
        public string Body { get; set; }

        public void From(object objDAO)
        {
            var news = (DAO.News)objDAO;

            ID = news.ID;
            Body = news.Body;
            DatePosted = news.DatePosted;
        }

        public string HtmlBodyPattern
        {
            get {
                return string.Format(
                    @"<nav class=""edit-nav""><a class=""edit-nav-item edit-edit"" href=""/News/Edit/{0}"" title=""Редактировать""></a>
                    <a class=""edit-nav-item edit-remove"" href=""#"" title=""Удалить"" onclick=""confirmDelete({0})""></a></nav>{1}",
                    ID,
                    Body
                );
            }
        }

        public override string ToString()
        {
            return String.Format("News ID: {0}; Body: {1}", ID, Body);
        }
    }
}