﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Account.Models
{
    public class ResponseModel
    {
        [Required]
        [Display(Name = "Файл документа")]
        public HttpPostedFileBase Report { get; set; }

        public override string ToString()
        {
            return String.Format("File response Name: {0}", Report.FileName);
        }
    }
}