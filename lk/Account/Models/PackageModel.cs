﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using Account.Filters;
using Account.DAO;


namespace Account.Models
{
    public class PackageModel : DAOObject
    {
        public int ID { get; set; }

        [FilterTable(Label = "Дата и время приема документа", Order = 1, Filter = FilterType.DateTimePicker)]
        public DateTime UploadTime { get; set; }

        [FilterTable(Label = "Наименование файла пакета", Order = 2, DisplayPattern = "HtmlNamePattern")]
        public string Name { get; set; }

        [FilterTable(Label = "Исходящий номер участника", Order = 3)]
        public string IncomingNumber { get; set; }

        [FilterTable(Label = "Дата создания документа", Order = 4, Filter = FilterType.DateTimePicker)]
        public DateTime IncomingDate { get; set; }

        [FilterTable(Label = "Входящий номер СБРФР", Order = 5)]
        public string SedNumber { get; set; }

        [FilterTable(Label = "Дата регистрации в СБРФР", Order = 6, Filter = FilterType.DateTimePicker)]
        public DateTime SedDate { get; set; }

        [FilterTable(Label = "Текущий статус", Order = 7, Filter=FilterType.None, Sortable=false, DisplayPattern = "HtmlStatusPattern")]
        public string CurrentStatusInfo { get; set; }

        [FilterTable(Label = "Cтатус проверки ЭП", Order = 8, Filter = FilterType.None, Sortable = false, DisplayPattern = "HtmlInitialStatusPattern")]
        public string InitialStatusInfo { get; set; }

        public int CurrentStatusCode { get; set; }
        public int InitialStatusCode { get; set; }
        public bool IsImported { get; set; }


        public void From(object objDAO)
        {
            var package = (DAO.PackageFullInfo)objDAO;

            ID = package.ID;
            UploadTime = package.UploadTime;
            Name = package.Name;
            IncomingNumber = package.IncomingNumber;
            if (package.IncomingDate.HasValue)
                IncomingDate = package.IncomingDate.Value;
            SedNumber = package.SedNumber;
            if (package.SedDate.HasValue)
                SedDate = package.SedDate.Value;
            CurrentStatusInfo = package.CurrentStatusInfo;
            CurrentStatusCode = package.CurrentStatusCode;
            InitialStatusInfo = package.InitialStatusInfo;
            InitialStatusCode = package.InitialStatusCode;
            IsImported = package.IsImported ?? false;
        }

        public string HtmlNamePattern {
            get
            {
                if (IsImported)
                {
                    return string.Format(
                        "<a href='/Home/DownloadArchiev/{0}'>{1}</a>",
                        ID,
                        Name
                    );
                }
                else
                {
                    return string.Format(
                        "<a href='/Home/Download/{0}'>{1}</a>",
                        ID,
                        Name
                    );
                }
            }
        }

        public string HtmlStatusPattern {
            get
            {
                if (IsImported)
                {
                    return StatusService.GetLkStatusString(
                            CurrentStatusCode
                        );
                }
                else
                {
                    return string.Format(
                        "<a href='/Home/Notification/{0}'>{1}</a>",
                        ID,
                        StatusService.GetLkStatusString(
                            CurrentStatusCode
                        )
                    );
                }
            }
        }

        public string HtmlInitialStatusPattern
        {
            get
            {
                if (IsImported)
                {
                    return StatusService.GetLkStatusString(
                            InitialStatusCode
                        );
                }
                else
                {
                    return string.Format(
                        "<a href='/Home/InitialNotification/{0}'>{1}</a>",
                        ID,
                        StatusService.GetLkStatusString(
                            InitialStatusCode
                        )
                    );
                }
            }
        }

        public override string ToString()
        {
            return String.Format("Package ID: {0}; Name: {1}", ID, Name);
        }
    }
}