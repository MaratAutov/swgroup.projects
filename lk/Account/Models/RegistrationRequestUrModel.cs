﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Account.Models
{
    public class RegistrationRequestUrModel
    {
        [Required]
        [Display(Name = "Наименование")]
        public string Company { get; set; }

        [Required]
        [RegularExpression(@"^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-zA-Z0-9]{1}[a-zA-Z0-9\-]{0,62}[a-zA-Z0-9]{1})|[a-zA-Z])\.)+[a-zA-Z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$", ErrorMessage = "Неправильный e-mail адрес")]
        [Display(Name = "Логин")]
        public string Login { get; set; }

        [Required]
        [Display(Name = "Контактный телефон")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "ФИО контактного лица")]
        public string FIO { get; set; }

        [Required]
        [Display(Name = "ИНН")]
        [StringLength(10, MinimumLength = 10, ErrorMessage="Поле ИНН должно содержать ровно 10 цифр")]
        public string INN { get; set; }

        [Required]
        [Display(Name = "ОГРН")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "Поле ОГРН должно содержать ровно 13 цифр")]
        public string OGRN { get; set; }

        [Required]
        [StringLength(32, ErrorMessage = "Пароль слишком короткий (минимум {2} символов)", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Повторный ввод пароля")]
        [Compare("Password", ErrorMessage = "Введеные пароли не совпадают")]
        public string PasswordConfirmation { get; set; }

        [Display(Name = "У меня установлен сертификат АЦП")]
        public bool IsCertificateInstalled { get; set; }

        public string SignedData { get; set; }
        public string Signature { get; set; }

        public override string ToString()
        {
            return String.Format("Registration request INN: {0}; Login: {1}", INN, Login);
        }
    }
}