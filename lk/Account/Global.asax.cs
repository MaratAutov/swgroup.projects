﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.Script.Serialization;
using NLog;
using Account.DAO;
using Account.Security;

namespace Account
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }
        
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            UserAccount acc;
            bool isUr = false;
            HttpCookie UrCookie = Request.Cookies["IsUr"];
            if (UrCookie != null)
            {
                if (UrCookie.Value == "1") isUr = true;
                else isUr = false;
            }


            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                if ((authCookie.Value != null) && (authCookie.Value != ""))
                {
                    try
                    {
                        if (isUr)
                        {
                            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            acc = serializer.Deserialize<UserAccount>(authTicket.UserData);
                            if (acc == null)
                                return;

                            CustomPrincipal user = new CustomPrincipal(User.Identity.Name);
                            user.Account = acc;
                            user.IsUr = true;
                            if (String.IsNullOrEmpty(user.Account.Login))
                                return;
                            HttpContext.Current.User = user;
                        }
                        else
                        {
                            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            acc = serializer.Deserialize<UserAccount>(authTicket.UserData);
                            if (acc == null)
                                return;

                            CustomPrincipal user = new CustomPrincipal(User.Identity.Name);
                            
                            UserIndividual ac = UserIndividualService.Get(acc.Login);
                            user.Individual = ac;
                            user.IsUr = false;
                            HttpContext.Current.User = user;
                        }

                    }
                    catch(Exception ex)
                    {
                        Logger _logger = LogManager.GetLogger("AccountController");                        
                        _logger.DebugException("Error logging user IP: " + HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], ex);
                        return;
                    }
                }
            }
        }
    }
}