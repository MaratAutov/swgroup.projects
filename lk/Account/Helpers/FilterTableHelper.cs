﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Account.Models;
using Account.Filters;
using Account.DAO;
using System.Configuration;

namespace Account.Helpers
{
    public class FilterTableHelper
    {
        public static HtmlString FilterTable<TModel, TModelDAO>(FilterTableModel<TModel, TModelDAO> fTable)
        {
            var props = fTable.DataModel.GetType().GetProperties().Where(
                p => Attribute.IsDefined(p, typeof(FilterTableAttribute))
            ).ToList();
            var obj_props = new Dictionary<object, List<PropertyInfo>>();
            foreach (var p in props)
            {
                var attr = (FilterTableAttribute)Attribute.GetCustomAttribute(
                    p, typeof(FilterTableAttribute)
                );
                if (attr.IsContainer)
                {
                    object _fVal = p.GetValue(fTable.DataModel, null);
                    var _props = _fVal.GetType().GetProperties().Where(
                        _p => Attribute.IsDefined(_p, typeof(FilterTableAttribute))
                    );
                    if (!obj_props.ContainsKey(_fVal))
                        obj_props.Add(_fVal, new List<PropertyInfo>());
                    obj_props[_fVal].AddRange(_props);
                }
                else
                {
                    if (!obj_props.ContainsKey(fTable.DataModel))
                        obj_props.Add(fTable.DataModel, new List<PropertyInfo>());
                    obj_props[fTable.DataModel].Add(p);
                }
            }

            List<FilterTableAttribute> attrList
                = new List<FilterTableAttribute>();

            string headerRow = "<table class=\"table-layout\" cellspacing=\"0\"><thead><tr class=\"label_row\">";
            string filterRow = "<tr class=\"form in_row\">";
            foreach (var item in obj_props)
            {
                foreach (var p in item.Value)
                {
                    var attr = (FilterTableAttribute)Attribute.GetCustomAttribute(
                        p, typeof(FilterTableAttribute)
                    );
                    attr.Field = p.Name;
                    object fVal = p.GetValue(item.Key, null);
                    if (fVal == null)
                        attr.FieldValue = "";
                    else if (fVal.GetType().Name == "DateTime")
                    {
                        
                            DateTime date = Convert.ToDateTime(fVal);
                            attr.FieldValue = date == DateTime.MinValue ? "" : date.ToShortDateString();                        
                        
                    }
                    else if (attr.FilterLabel == "Только не прочитанные")
                    {
                        attr.FieldValue = fVal.ToString();
                    }

                    else
                        attr.FieldValue = fVal.ToString();

                    attrList.Add(attr);
                }
            }
            attrList.Sort((a, b) => a.Order.CompareTo(b.Order));

            foreach (var attr in attrList)
            {
                string header;

                if ((attr.Label == "ИНН отправителя") && (fTable.Visible == false))
                {
                    header = "<span> </span>";
                }
                else if ((attr.Label == "Дата создания") && (fTable.Visible == false))
                {
                    //header = "<span></span>";
                    header = "";
                }
                else
                    if ((attr.Label == "Наименование отправителя") && (fTable.Visible == false))
                    {
                        header = "<span> </span>";
                    }
                    else
                    {
                        if (attr.Sortable)
                        {
                            string classes = "";
                            if (attr.Field == fTable.SortingField)
                                classes = fTable.SortingType;
                            header = string.Format(
                                @"<a class=""sorted {0}""
                                 onclick=""setSorting(this)""
                                 href=""javascript:$('form').submit()""
                                 id=""{1}""><span>{2}</span></a>",
                                classes, attr.Field, attr.Label
                            );
                        }
                        else
                        {
                            header = string.Format("<span>{0}</span>", attr.Label);
                        }
                    }
                if ((attr.Label == "ИНН отправителя") && (fTable.Visible == false))
                {
                    //filterRow += "<th style='display:none'></th>";
                    filterRow += "<th></th>";
                }
                else if ((attr.Label == "Наименование отправителя") && (fTable.Visible == false))
                {
                    //filterRow += "<th style='display:none'></th>";
                    filterRow += "<th></th>";
                }
                else if ((attr.Label == "Дата создания") && (fTable.Visible == false))
                {
                    //filterRow += "<th style='display:none'></th>";
                    filterRow += "<th></th>";
                }
                else if (attr.Label == " ")
                {
                    //filterRow += "<th style='display:none'></th>";
                    filterRow += "<th></th>";
                }
                else
                    if (attr.Filter == FilterType.TextInput)
                    {
                        string fieldName = attr.Insider == null ? attr.Field : attr.Insider + "." + attr.Field;
                        filterRow += string.Format(
                            @"<th><div class=""filter-container""><input id=""DataModel_{0}""
                                         name=""DataModel.{0}""
                                         type=""text""
                                         value=""{1}""><div class=""filter-clear-single"" onclick=""clearFilters('DataModel_{0}')"">X</div></div></th>",
                            fieldName, attr.FieldValue
                        );
                    }
                    else if (attr.Filter == FilterType.DateTimePicker || attr.Filter == FilterType.DatePicker)
                    {
                        string fieldName = attr.Insider == null ? attr.Field : attr.Insider + "." + attr.Field;
                        filterRow += string.Format(
                                @"<th><div class=""filter-container""><input id=""DataModel_{0}""
                                         name=""DataModel.{0}""
                                         type=""text""
                                         value=""{1}"" class=""date"" maxlength=""10""><div class=""filter-clear-single"" onclick=""clearFilters('DataModel_{0}')"">X</div></div></th>",
                                fieldName, attr.FieldValue
                            );
                    }
                    else if (attr.Filter == FilterType.CheckBox)
                    {
                        string fieldName = attr.Insider == null ? attr.Field : attr.Insider + "." + attr.Field;
                        filterRow += string.Format(
                            @"<th><div class=""filter-container""><input id=""DataModel_{0}""
                                         name=""DataModel.{0}""
                                         type=""checkbox"" {1}>{2}</div></th>",
                            fieldName, string.IsNullOrEmpty(attr.FieldValue) ? "" : "checked=\"checked\"", attr.FilterLabel
                        );
                    }
                    else
                        filterRow += "<th></th>";

                headerRow += string.Format(
                    "<th>{0}</th>", header
                );
            }
            headerRow += "<th></th></tr>";
            filterRow += "<th style=\"text-align: right\"><div class=\"filter-clear\" onclick=\"clearFilters()\">X</div><input class=\"submit-btn\" type=\"submit\" value=\"\"></th></tr></thead>";

            string result = headerRow + filterRow + "<tbody>";

            foreach (TModelDAO objDAO in fTable.DataList.GetCurrentPageList())
            {
                var obj = ModelFromDAO<TModel>(objDAO);
                bool isValid = true;
                string tmp_result = "<tr>";
                foreach (var attr in attrList)
                {
                    PropertyInfo p = null;
                    object val = null;
                    object ins = null;
                    string ins_filter_val = null;
                    string pattern = "{0}";

                    var objType = obj.GetType();
                    if (attr.Insider == null)
                    {
                        p = objType.GetProperty(attr.Field);
                        val = p.GetValue(obj, null);
                        if (attr.DisplayPattern != null)
                        {
                            var _p = objType.GetProperty(attr.DisplayPattern);
                            var _val = _p.GetValue(obj, null);
                            pattern = _val.ToString();                                                      

                            for(int i = 0; i < pattern.Length; i++)
                            {
                                if (pattern[i].ToString() == "{")
                                {
                                    pattern = pattern.Insert(i, "{");
                                    i++;
                                }
                                else if (pattern[i].ToString() == "}")
                                {
                                    pattern = pattern.Insert(i, "}");
                                    i++;
                                }
                            }
                        }
                    }
                    else
                    {
                        ins = objType.GetProperty(attr.Insider).GetValue(obj, null);
                        if (ins != null)
                        {
                            p = ins.GetType().GetProperty(attr.Field);
                            val = p.GetValue(ins, null);

                            if (attr.DisplayPattern != null)
                            {
                                var _p = ins.GetType().GetProperty(attr.DisplayPattern);
                                var _val = _p.GetValue(obj, null);
                                pattern = _val.ToString();
                            }

                            var _fobj = fTable.DataModel;
                            var _fobj_ins = _fobj.GetType().GetProperty(attr.Insider).GetValue(_fobj, null);
                            var _ins_fval = _fobj_ins.GetType().GetProperty(attr.Field).GetValue(_fobj_ins, null);
                            ins_filter_val = _ins_fval == null ? null : _ins_fval.ToString();
                        }
                    }

                    string value = "";
                    if (val == null || (val.GetType().Name == "DateTime" && Convert.ToDateTime(val) == DateTime.MinValue))
                        value = "-";
                    else if (attr.Filter == FilterType.DatePicker)
                        value = Convert.ToDateTime(val).ToShortDateString();
                    else if (attr.BoolValues == null)
                        value = val.ToString();
                    else
                        value = attr.BoolValues[Convert.ToInt32(val)];

                    if (ins_filter_val != null && !value.Contains(ins_filter_val))
                        isValid = false;



                    if (((attr.Label == "ИНН отправителя") || (attr.Label == "Наименование отправителя") || (attr.Label == "Дата создания")) && (fTable.Visible == false))
                    {
                        tmp_result += "<td style='display:none'>" + string.Format(pattern, value) + "</td>";
                    }
                    else
                        tmp_result += "<td>" + string.Format(pattern, value) + "</td>";
                }
                tmp_result += "</tr>";

                if (isValid)
                    result += tmp_result;
            }

            result += "</tbody></table>";

            return new HtmlString(result);
        }

        public static HtmlString FilterTablePaging<TModel, TModelDAO>(FilterTableModel<TModel, TModelDAO> fTable)
        {
            string result = "<div class=\"sizes\"><span class=\"sizes-label\">Записей на странице:</span>";
            for (int i = 0; i < fTable.DataList.PageSizing.Count; ++i)
            {
                string selected = "";
                string link = "<a onclick=\"setSize(this)\" href=\"javascript:$('form').submit()\">{0}</a>";
                if (fTable.DataList.PageSize == fTable.DataList.PageSizing[i])
                {
                    selected = "selected";
                    link = "{0}";
                }

                result += string.Format(
                    "<span class=\"button {0}\"> {1} </span>",
                    selected,
                    string.Format(link, fTable.DataList.PageSizing[i])
                );
            }

            result += string.Format(
                "<div class=\"total\">Всего: {0}</div></div>",
                fTable.DataList.RecordCount
            );

            result += "<div class=\"arrows\"><p>";
            if (fTable.DataList.Page == 0)
                result += "<span>&larr; В начало</span>";
            else
                result += "<a onclick=\"goFirst()\" href=\"javascript:$('form').submit()\">&larr; В начало</a>";
            if (fTable.DataList.Page == fTable.DataList.PageCount - 1 || fTable.DataList.PageCount == 0)
                result += "<span>В конец &rarr;</span>";
            else
                result += string.Format("<a onclick=\"goTo({0})\" href=\"javascript:$('form').submit()\">В конец &rarr;</a>", fTable.DataList.PageCount);
            result += "</p></div>";

            result += "<div class=\"pager\"><div class=\"pages\">";
            int startPageIndex = 0;
            int endPageIndex = fTable.DataList.PageCount > fTable.DataList.MaxVisiblePage ? fTable.DataList.MaxVisiblePage : fTable.DataList.PageCount;
            if (fTable.DataList.Page > endPageIndex - 1)
            {
                endPageIndex = fTable.DataList.Page + 1;
                startPageIndex = endPageIndex - fTable.DataList.MaxVisiblePage;
                if (startPageIndex < 0)
                    startPageIndex = 0;
            }
            if (startPageIndex > 0)
                result += "<span><a style=\"text-decoration: none\" onclick=\"goBack()\" href=\"javascript:$('form').submit()\">...</a></span>";
            for (int i = startPageIndex; i < endPageIndex; ++i)
            {
                bool isCurrent = i == fTable.DataList.Page;
                result += string.Format(
                    "<span class=\"button {0}\">{1}</span>",
                     isCurrent ? "selected" : "",
                     !isCurrent ?
                        string.Format("<a onclick=\"goTo({0})\" href=\"javascript:$('form').submit()\">{0}</a>", i + 1) :
                        (i + 1).ToString()

                );
            }
            if (fTable.DataList.Page < fTable.DataList.PageCount - 1 && endPageIndex != fTable.DataList.PageCount)
                result += "<span><a style=\"text-decoration: none\" onclick=\"goNext()\" href=\"javascript:$('form').submit()\">...</a></span>";
            result += "</div></div>";

            return new HtmlString(result);
        }

        public static TModel ModelFromDAO<TModel>(object objDAO)
        {
            TModel obj = Activator.CreateInstance<TModel>();
            (obj as DAO.DAOObject).From(objDAO);
            return obj;
        }

        public static void InitOutgoingTable(int userId, string year, FilterTableModel<PackageModel, PackageFullInfo> table)
        {
            if (table.DataModel == null)
            {
                table.DataModel = new PackageModel();
                table.SortingField = "UploadTime";
                table.SortingType = "desc";
            }
            if (table.DataList == null)
                table.DataList = new PagedList<PackageFullInfo>();

            table.DataList.DataQuery = PackageService.GetAll(
                userId,
                year,
                table.DataModel.UploadTime,
                table.DataModel.Name,
                table.DataModel.IncomingNumber,
                table.DataModel.IncomingDate,
                table.DataModel.SedNumber,
                table.DataModel.SedDate,
                table.SortingField,
                table.SortingType,
                ConfigurationManager.AppSettings["IsShowImported"] == "True"
            );
        }
    }
}