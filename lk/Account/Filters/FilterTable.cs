﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Account.Filters
{
    public enum FilterType
    {
        None,
        TextInput,
        DatePicker,
        DateTimePicker,
        CheckBox
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class FilterTableAttribute : Attribute
    {
        public bool Visible { get; set; }
        public string Label { get; set; }
        public string FilterLabel { get; set; }
        public string DisplayPattern { get; set; }
        public bool Sortable { get; set; }
        public bool IsContainer { get; set; }
        public FilterType Filter { get; set; }
        public int Order { get; set; }
        public string[] BoolValues;
        public string Insider;
        public string Field;
        public string FieldValue;
        public bool IsRead { get; set; }

        public FilterTableAttribute()
        {
            Sortable = true;
            Filter = FilterType.TextInput;
            DisplayPattern = null;
        }
    }
}