﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Account
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");            
            routes.IgnoreRoute("Home/Notification/{id}", new { id = @"((?:[a-z]|[0-9])*(?:(?:[0-9][a-z])|(?:[a-z][0-9]))+(?:[0-9]|[a-z])*)" });
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Outgoing",
                url: "Home/Outgoing/{id}/{year}",
                defaults: new { controller = "Home", action = "Outgoing", id = UrlParameter.Optional, year = UrlParameter.Optional }
            );

            
        }
    }
}