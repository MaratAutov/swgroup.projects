﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Account.Security;
using Account.DAO;
using Account.Controllers;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Account.Models;
using System.Data;
using ServiceStack.OrmLite;
using System.Xml;
using System.Reflection;

namespace Account.Tests
{
    [TestClass]
    public class AccountDAOTestDocumentService
    {
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            ClassInitialize.AddUserAccount();
        }

        [TestMethod]
        public void TestDocumentServiceGet()
        {
            List<Document> list = null;
            list = DocumentService.Get(ClassInitialize.GetUserId());
            Assert.AreNotEqual(null, list, "Ошибка");
        }       
    }
}
