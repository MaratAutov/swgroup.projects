﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Account.Security;
using Account.DAO;
using Account.Controllers;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Account.Models;
using System.Data;
using ServiceStack.OrmLite;
using System.Xml;
using System.Reflection;

namespace Account.Tests
{
    static class ClassInitialize
    {
        public static int GetUserId()
        {
            string userName = "k.mauglii@gmail.com";
            var user = UserService.Get(userName);
            return user.ID;
        }

        public static void AddUserAccount()
        {
            UserAccount user = new UserAccount();
            using (var db = ORM.db)
            {
                user = db.Select<UserAccount>(
                    u => u.Login == "k.mauglii@gmail.com"
                ).SingleOrDefault();
            }

            if (user == null)
            {
                using (var db = ORM.db)
                {
                    db.Insert(new UserAccount()
                    {
                        Login = "k.mauglii@gmail.com",
                        Password = ORM.GetHash("111111"),
                        IsActive = true,
                        IsAdmin = true,
                        Company = "",
                        FIO = "k.mauglii@gmail.com",
                        Phone = "",
                        INN = "7707083893",
                        OGRN = "1027700132195",
                        CreateDate = DateTime.Now
                    });
                }
            }
        }
    }
}
