﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Account.Security;
using Account.DAO;
using Account.Controllers;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Account.Models;
using System.Data;
using ServiceStack.OrmLite;
using System.Xml;
using System.Reflection;

namespace Account.Tests
{
    [TestClass]
    public class AccountDAOTestStatusService
    {      

        [TestMethod]
        public void TestStatusService_GetStatusString()
        {
            string msg = StatusService.GetStatusString(0, false);

            Assert.AreEqual("Пакет принят на обработку", msg, "Ошибка");
        }

        [TestMethod]
        public void TestStatusService_Get()
        {
            Status status = StatusService.Get(1);

            Assert.AreNotEqual(null, status, "Ошибка");
        }


        [TestMethod]
        [DeploymentItem("TestUpload.txt")]
        public void TestStatusService_UpdateStatusForPackage()
        {
            //StatusService.UpdateStatusForPackage(5670, PrimaryStatus.Accepted, SecondaryStatus.UNSET);

            FileStream fileStream = new FileStream("TestUpload.txt", FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[fileStream.Length];
            fileStream.Read(buffer, 0, (int)fileStream.Length);

            try
            {
                using (var cl = new SPOService.SPOServiceSoapClient())
                {
                    cl.UploadPackage(
                        "TestUpload.txt",
                        buffer,
                        ClassInitialize.GetUserId(),
                        DateTime.Now,
                        false,
                        null
                    );
                }
            }
            catch (Exception)
            {
                ;
            }
        }

        [TestMethod]
        public void TestStatusService_GetStats()
        {
            DateTime dt = new DateTime(2000, 01, 01);
            List<StatusStats> list = StatusService.GetStats(dt, DateTime.Now);
            Assert.AreNotEqual(null, list.Count, "Ошибка");
        }
    }
}
