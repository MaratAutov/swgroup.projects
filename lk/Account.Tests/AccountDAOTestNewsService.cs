﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Account.Security;
using Account.DAO;
using Account.Controllers;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Account.Models;
using System.Data;
using ServiceStack.OrmLite;
using System.Xml;
using System.Reflection;

namespace Account.Tests
{
    [TestClass]
    public class AccountDAOTestNewsService
    { 
        public static string NewsBody
        {
            get
            {
                return "TestNewService";
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            News news = NewsService.GetByBody(NewsBody);
            if (news == null)
            {
                NewsService.Add(ClassInitialize.GetUserId(), NewsBody);
            }
        }

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            ClassInitialize.AddUserAccount();  

            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                db.Delete<News>(
                    n => n.Body == NewsBody
                );
                trans.Commit();
            }
        }       

        [TestMethod]
        public void TestNewServiceDelete()
        {
            NewsService.Delete(NewsService.GetByBody(NewsBody).ID);
            News news = NewsService.GetByBody(NewsBody);
            Assert.AreEqual(null, news, "Ошибка");
        }

        [TestMethod]
        public void TestNewServiceUpdate()
        {
            NewsService.Update(NewsService.GetByBody(NewsBody).ID, ClassInitialize.GetUserId(), "TestNewService Update");
            NewsService.Update(NewsService.GetByBody("TestNewService Update").ID, ClassInitialize.GetUserId(), NewsBody);
            News news = NewsService.GetByBody(NewsBody);
            Assert.AreNotEqual(null, news.ID, "Ошибка");
        }

        [TestMethod]
        public void TestNewServiceGetAll()
        {
            var news = NewsService.GetAll(
                    DateTime.Now.Year.ToString(),
                    DateTime.Now.Month.ToString(),
                    DateTime.MinValue,
                    null,
                    "DatePosted",
                    "desc"
                );
            Assert.AreNotEqual(0, news.Count, "Ошибка");
        }

        [TestMethod]
        public void TestNewServiceGetTiming()
        {
            List<NewsTiming> list = NewsService.GetTiming();
            Assert.AreNotEqual(null, list.Count, "Ошибка");
        }

        [TestMethod]
        public void TestNewServiceGet()
        {            
            News news = NewsService.Get(NewsService.GetByBody(NewsBody).ID.ToString());
            Assert.AreNotEqual(null, news, "Ошибка");
        }        
    }
}
