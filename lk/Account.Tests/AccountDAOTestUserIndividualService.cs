﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Account.Security;
using Account.DAO;
using Account.Controllers;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Account.Models;
using System.Data;
using ServiceStack.OrmLite;
using System.Xml;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;


namespace Account.Tests
{
    [TestClass]
    public class AccountDAOTestUserIndividualService
    {
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            UserIndividual user = new UserIndividual();
            using (var db = ORM.db)
            {
                user = db.Select<UserIndividual>(
                    u => u.Login == "heaven13-88@mail.ru"
                ).SingleOrDefault();
            }

            if (user == null)
            {
                DateTime dt = new DateTime(2013, 04, 11);
                UserIndividualService.Add("heaven13-88@mail.ru", "111111", "", "", "", dt, "", "", "info", true);               
            }
        }

        [TestMethod]
        public void TestGetLoginPassword()
        {
            UserIndividual user = null;
            user = UserIndividualService.Get("heaven13-88@mail.ru", "111111");
            Assert.AreNotEqual(null, user, "Ошибка");
        }

        [TestMethod]
        public void TestGetLogin()
        {
            UserIndividual user = null;
            user = UserIndividualService.Get("heaven13-88@mail.ru");
            Assert.AreNotEqual(null, user, "Ошибка");
        }

        [TestMethod]
        public void TestExist()
        {
            IEnumerable<UserIndividual> user = null;
            user = UserIndividualService.Exists("heaven13-88@mail.ru", "7707083893");
            Assert.AreNotEqual(null, user, "Ошибка");
        }

        [TestMethod]
        public void TestGetAll()
        {
            List<UserIndividual> list = null;
            DateTime dt = new DateTime(2013, 04, 11, 00, 00, 00);
            list = UserIndividualService.GetAll("heaven13-88@mail.ru", "", "", "", true, dt, "Login", "desc");
            Assert.AreNotEqual(null, list, "Ошибка");
        }       

        [TestMethod]
        public void TestDeleteAndInsert()
        {
            UserIndividualService.Delete("heaven13-88@mail.ru");

            UserIndividual user = null;
            user = UserIndividualService.Get("heaven13-88@mail.ru");
            Assert.AreEqual(null, user, "Ошибка");

            DateTime dt = new DateTime(2013, 04, 11);
            
            UserIndividualService.Add("heaven13-88@mail.ru", "111111", "", "", "", dt, "","","info", true);

            UserIndividual user_exist = null;
            user_exist = UserIndividualService.Get("heaven13-88@mail.ru");
            Assert.AreNotEqual(null, user_exist, "Ошибка");
        }

        [TestMethod]
        public void TestUpdate()
        {
            DateTime dt = new DateTime(2013, 04, 11);
            UserIndividualService.Update("heaven13-88@mail.ru", "111111", true, "heaven13-88@mail.ru", "", "", dt, "", "", "");

            UserIndividual user = null;
            user = UserIndividualService.Get("heaven13-88@mail.ru");
            Assert.AreEqual("heaven13-88@mail.ru", user.FirstName, "Ошибка");

            UserIndividualService.Update("heaven13-88@mail.ru", "111111", true, "", "", "", dt, "", "", "");

            user = UserIndividualService.Get("heaven13-88@mail.ru");
            Assert.AreEqual("", user.FirstName, "Ошибка");
        }

        [TestMethod]
        public void TestChangePassword()
        {
            UserIndividualService.ChangePassword("heaven13-88@mail.ru", "222222");
            UserIndividual user = null;
            user = UserIndividualService.Get("heaven13-88@mail.ru");
            Assert.AreEqual(ORM.GetHash("222222"), user.Password, "Ошибка");

            UserIndividualService.ChangePassword("heaven13-88@mail.ru", "111111");
            user = UserIndividualService.Get("heaven13-88@mail.ru");
            Assert.AreEqual(ORM.GetHash("111111"), user.Password, "Ошибка");
        }

        [TestMethod]
        public void TestDecodeUncode()
        {
            byte[] test = UserIndividualService.Encode("test");
            Assert.AreEqual("test", UserIndividualService.Decode(test), "Ошибка");
        }
    }
}
