﻿using SPO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using ServiceCore;
using System.Text;
using Account.DAO;
using Account.Tools;

namespace Account.Tests
{
    
    
    /// <summary>
    ///Это класс теста для ProcessPackage2LotusTest, в котором должны
    ///находиться все модульные тесты ProcessPackage2LotusTest
    ///</summary>
    [TestClass()]
    public class ProcessPackage2LotusTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Тест обработки ошибок от СЭД, тест издевается над статусами одного из пакетов базе
        ///</summary>
        [TestMethod()]
        public void processSedResponseTest()
        {
            SPO.SPOService service = new SPO.SPOService(); 
            ProcessPackage2Lotus target = new ProcessPackage2Lotus(service); 
            SedResponse response = new SedResponse(); 
            ServiceCoreInfo info = new ServiceCoreInfo();
            Package package = PackageService.GetAll().FindLast(x => 1 == 1);
            response.Subject = string.Format("{0}/{1}", package.ID, DateTime.Now);
            response.XmlData = Encoding.UTF8.GetBytes(@"<?xml version=""1.0"" encoding=""UTF-8""?>
                <env:Fault xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">
                <faultcode>502</faultcode>
                <faultstring>Документ с таким исходящим номером уже существует.</faultstring>
                </env:Fault>".ToCharArray());
            target.processSedResponse(response, info);
            var result = PackageService.GetFullInfo(package.ID);
            Assert.AreEqual("{ERROR_PACKAGE_ALREADY_REGISTERED} Документ с таким исходящим номером уже существует.", result.CurrentStatusInfo);
            UpdateStatusPackage.Build(
                                package.ID,
                                PrimaryStatus.SED_Awaiting
                            );
            //StatusService.UpdateStatusForPackage(
            //                    package.ID,
            //                    PrimaryStatus.SED_Awaiting
            //                );
            response.XmlData = Encoding.UTF8.GetBytes(@"<?xml version=""1.0"" encoding=""UTF-8""?>
                <env:Fault xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">
                <faultcode>601</faultcode>
                <faultstring>Документ с таким исходящим номером уже существует.</faultstring>
                </env:Fault>".ToCharArray());
            target.processSedResponse(response, info);
            var result1 = PackageService.GetFullInfo(package.ID);
            Assert.AreEqual((int)PrimaryStatus.SED_Awaiting, result1.CurrentStatusCode);
            Assert.AreEqual("", result1.CurrentStatusInfo);
        }
    }
}
