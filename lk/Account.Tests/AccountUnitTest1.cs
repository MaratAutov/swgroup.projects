﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Account.Security;
using Account.DAO;
using Account.Controllers;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Account.Models;
using System.Data;
using ServiceStack.OrmLite;
using System.Xml;
using System.Reflection;
using Account.Tools;

namespace Account.Tests
{
    /// <summary>
    /// Сводное описание для AccountUnitTest1
    /// </summary>
    [TestClass]
    public class AccountUnitTest1
    {       

        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }



        #region Дополнительные атрибуты тестирования
        //
        // При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        // ClassInitialize используется для выполнения кода до запуска первого теста в классе
         [ClassInitialize()]
         public static void MyClassInitialize(TestContext testContext)
         {
             ClassInitialize.AddUserAccount();            
         }
        //
        // ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // TestInitialize используется для выполнения кода перед запуском каждого теста 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // TestCleanup используется для выполнения кода после завершения каждого теста
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
        

        [TestMethod]
        public void TestLogin()
        {
            AccountController controller = new AccountController();
            UserAccount user2 = new UserAccount();                     

            UserAccount user = controller.UsersExist(false, "k.mauglii@gmail.com", "111111");
            Assert.AreNotEqual(null, user, "Пользователь не зарегистрирован.");
            
            bool active = true;
            if (user != null)
            {
                Assert.AreEqual(active, user.IsActive, "Пользователь не активен.");
            }            
        }

        [TestMethod]
        public void TestAccountChekInnUridicalPerson()
        {
            AccountController controller = new AccountController();
            Assert.AreEqual(true, controller.ChekInnUridicalPerson("7707083893"), "Проверьте правильность введенного значения");
        }

        [TestMethod]
        public void TestAccountChekOgrn()
        {
            AccountController controller = new AccountController();
            Assert.AreEqual(true, controller.CheckOgrn("1027700132195"), "Проверьте правильность введенного значения");
        }

        private int _uploadFileId;
        public int UploadFileId
        {
            get
            {
                return _uploadFileId;
            }
            set
            {
                _uploadFileId = value;
            }
        }
   
        [TestMethod]
        [DeploymentItem("TestUpload.txt")]
        public void TestUpload()
        {
            //HomeController controller = new HomeController();
            //FileStream fileStream = new FileStream("TestUpload.txt", FileMode.Open, FileAccess.Read);
            
            //MemoryFile file = new MemoryFile(fileStream, "text/xml", "TestUpload.txt");
            //UploadModel model = new UploadModel();
            //model.Report = file;
            //model.Name = "TestUpload.txt";
            
            //bool success = Upload(model);
            //fileStream.Close();
            //Assert.AreEqual(true, success, "Файл не загружен."); 
        }

        [TestMethod]
        [DeploymentItem("TestUpload.txt")]
        public void TestDownload()
        {
            HomeController controller = new HomeController();
            FileStream fileStream = new FileStream("TestUpload.txt", FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[fileStream.Length];
            fileStream.Read(buffer, 0, (int)fileStream.Length);

           // Package package = PackageService.GetOrCreate("TestUpload.txt", buffer, ClassInitialize.GetUserId(), DateTime.Now, false, null);
            Package package = UpdateStatusPackage.Create("TestUpload.txt", buffer, ClassInitialize.GetUserId(), DateTime.Now, false, null);
            //int id = package.ID;
            //ActionResult result = controller.Download(id.ToString());
            //Assert.AreNotEqual(null, result, "Не удалось сохранить файл");
        }

        

        [TestMethod]
        public void TestViewNews()
        {
            FilterTableModel<NewsModel, News> table = new FilterTableModel<NewsModel, News>();
            NewsController.InitNewsTable(table);
            Assert.AreNotEqual(null, table.DataList, "Ошибка");           
        }

        [TestMethod]
        public void TestDeleteNews()
        {

        }       

        public bool Upload(UploadModel model)
        {            
            if (model.Report == null || model.Name == null)
                return false;

            try
            {
                int userID = ClassInitialize.GetUserId();
                var stream = model.Report.InputStream;
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, (int)stream.Length);

                try
                {                    
                    var clspo = new SPOService.SPOServiceSoapClient();
                    clspo.UploadPackage(
                            model.Name,
                            data,
                            userID,
                            DateTime.Now,
                            false,
                            null
                        );
                    return true;
                }
                catch (Exception)
                {
                    string id = Guid.NewGuid().ToString();
                    System.IO.File.WriteAllBytes(
                        "C:\\DelayedUpload\\" + id + ".data",
                        data
                    );
                    System.IO.File.WriteAllText(
                        "C:\\DelayedUpload\\" + id + ".info",
                        string.Format(
                            "{1}{0}{2}{0}{3}{0}{4}",
                            Environment.NewLine,
                            model.Report.FileName,
                            model.Name,
                            userID,
                            DateTime.Now
                        )
                    );
                    return true;
                }               
            }
            catch
            {
                return false;
            }
        }
    }

    class MemoryFile : HttpPostedFileBase
    {
        Stream stream;
        string contentType;
        string fileName;

        public MemoryFile(Stream stream, string contentType, string fileName)
        {
            this.stream = stream;
            this.contentType = contentType;
            this.fileName = fileName;
        }

        public override int ContentLength
        {
            get { return (int)stream.Length; }
        }

        public override string ContentType
        {
            get { return contentType; }
        }

        public override string FileName
        {
            get { return fileName; }
        }

        public override Stream InputStream
        {
            get { return stream; }
        }

        public override void SaveAs(string filename)
        {
            using (var file = File.Open(filename, FileMode.CreateNew))
                stream.CopyTo(file);
        }
    }

}
