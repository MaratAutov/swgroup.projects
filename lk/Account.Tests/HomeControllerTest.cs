﻿using Account.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Web.Mvc;
using System.Net;
using Account.DAO;
using System.Collections.Generic;
using System.IO;

namespace Account.Tests
{
    
    
    /// <summary>
    ///Это класс теста для HomeControllerTest, в котором должны
    ///находиться все модульные тесты HomeControllerTest
    ///</summary>
    [TestClass()]
    public class HomeControllerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        List<string> statuses = new List<string>() { "Processing", "SignatureCorrect", "Accepted", "IncomingNumberAssotiated",  "PackageNotRegistered",
                            "PackageCorrupted", "SignatureError" };

        /// <summary>
        ///Тест для Notification
        ///</summary>
        // TODO: убедитесь, что в атрибуте UrlToTest содержится URL-адрес страницы ASP.NET (например: 
        // http://.../Default.aspx). Это требуется для выполнения модульного теста на веб-сервере 
        // при тестировании страницы, веб-службы или службы WCF.
        [TestMethod()]
        //[HostType("ASP.NET")]
        //[AspNetDevelopmentServerHost("C:\\Users\\Evgeniy\\projects\\СПО\\Source\\checkout_10102013\\Account", "/")]
        //[UrlToTest("http://localhost:3375/")]
        public void NotificationTest()
        {
            WebClient wc = new WebClient();
            IList<Package> packages = PackageService.GetAll();
            if (packages.Count > 0)
            {
                string notificationURL = string.Format("http://localhost:3375/Home/Notification/{0}", packages[packages.Count - 1].ID);
                string temppath = Path.GetTempFileName();
                wc.DownloadFile(notificationURL, temppath);
                string Status = wc.ResponseHeaders["FFMS-notification-status"];
                Assert.AreNotEqual(Status, null);
                Assert.AreNotEqual(Status, "");
                bool exists = statuses.Contains(Status);
                Assert.AreEqual(true, exists);
            }
        }
    }
}
