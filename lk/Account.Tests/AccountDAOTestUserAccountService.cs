﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Account.Security;
using Account.DAO;
using Account.Controllers;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Account.Models;
using System.Data;
using ServiceStack.OrmLite;
using System.Xml;
using System.Reflection;

namespace Account.Tests
{
    [TestClass]
    public class AccountDAOTestUserAccountService
    {
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            ClassInitialize.AddUserAccount();
        }

        [TestMethod]
        public void TestGetLoginPassword()
        {
            UserAccount user = null;
            user = UserService.Get("k.mauglii@gmail.com", "111111");
            Assert.AreNotEqual(null, user, "Ошибка");
        }

        [TestMethod]
        public void TestGetLogin()
        {
            UserAccount user = null;
            user = UserService.Get("k.mauglii@gmail.com");
            Assert.AreNotEqual(null, user, "Ошибка");
        }

        [TestMethod]
        public void TestExist()
        {
            IEnumerable<UserAccount> user = null;
            user = UserService.Exists("k.mauglii@gmail.com", "7707083893", "1027700132195");
            Assert.AreNotEqual(null, user, "Ошибка");
        }

        [TestMethod]
        public void TestGetAll()
        {
            List<UserAccount> list = null;
            list = UserService.GetAll ("k.mauglii@gmail.com", "", "7707083893", "1027700132195", true, DateTime.MinValue, "Login","desc");
            Assert.AreNotEqual(null, list, "Ошибка");
        }

        [TestMethod]
        public void TestDeleteAndInsert()
        {
            

            UserService.Add("test34343233","111111", "", "", "test123456@gmail.com","7707083893", "1027700132195", true, true);

            UserAccount user_exist = null;
            user_exist = UserService.Get("test34343233");
            Assert.AreNotEqual(null, user_exist, "Ошибка");

            UserService.Delete("test34343233");

            UserAccount user = null;
            user = UserService.Get("test34343233");
            Assert.AreEqual(null, user, "Ошибка");
        }

        [TestMethod]
        public void TestUpdate()
        {
            UserService.Update("k.mauglii@gmail.com", "111111", "company", "", "k.mauglii@gmail.com", "7707083893", "1027700132195",true);

            UserAccount user = null;
            user = UserService.Get("k.mauglii@gmail.com");
            Assert.AreEqual("company", user.Company, "Ошибка");

            UserService.Update("k.mauglii@gmail.com", "111111", "", "", "k.mauglii@gmail.com", "7707083893", "1027700132195", true);

            user = UserService.Get("k.mauglii@gmail.com");
            Assert.AreEqual("", user.Company, "Ошибка");
        }

        [TestMethod]
        public void TestChangePassword()
        {
            UserService.ChangePassword("k.mauglii@gmail.com", "222222");
            UserAccount user = null;
            user = UserService.Get("k.mauglii@gmail.com");
            Assert.AreEqual(ORM.GetHash("222222"), user.Password, "Ошибка");

            UserService.ChangePassword("k.mauglii@gmail.com", "111111");            
            user = UserService.Get("k.mauglii@gmail.com");
            Assert.AreEqual(ORM.GetHash("111111"), user.Password, "Ошибка");
        }

        [TestMethod]
        public void TestChangeNotification()
        {
            UserService.ChangeNotification("k.mauglii@gmail.com", true);
            UserAccount user = null;
            user = UserService.Get("k.mauglii@gmail.com");
            Assert.AreEqual(true, user.EnableNotification, "Ошибка");
        }
    }
}
