﻿using SPO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Collections.Generic;
using SPO.MailService;
using Account.DAO;
using System.Linq;
using Account.Tools;

using ServiceStack.OrmLite;
using System.Text;
using System.Xml;
using Account.Tools.MailService;


namespace Account.Tests
{
    
    
    /// <summary>
    ///Это класс теста для LetterGeneratorTest, в котором должны
    ///находиться все модульные тесты LetterGeneratorTest
    ///</summary>
    [TestClass()]
    public class LetterGeneratorTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Тест для ComposeMessage
        ///</summary>
        // TODO: убедитесь, что в атрибуте UrlToTest содержится URL-адрес страницы ASP.NET (например: 
        // http://.../Default.aspx). Это требуется для выполнения модульного теста на веб-сервере 
        // при тестировании страницы, веб-службы или службы WCF.
        [TestMethod()]
        //[HostType("ASP.NET")]
        //[AspNetDevelopmentServerHost("C:\\Users\\Evgeniy\\projects\\СПО\\Source\\ckeckout_29082013_trunk\\Account.SPOService", "/")]
        //[UrlToTest("http://localhost:37446/")]
        public void ComposeMessageTest()
        {
            Package dbPackage = PackageService.GetAll().FirstOrDefault(); // TODO: инициализация подходящего значения
            var packman = new PackageManager();
            byte[] data = null;
            using (var db = ORM.db)
            {
                Blob b = db.Select<Blob>(
                    p => p.ID == dbPackage.BlobID
                ).SingleOrDefault();
                data = b.Body;
            }

            packman.Open(dbPackage.Name, data);
            packman.VerifySignature();
            packman.VerifyXtdd();
            InteropPackage package = new InteropPackage()
            {
                Id = dbPackage.ID,
                XmlData = packman.ReportPackage.ReportBody,
                ExternalId = packman.ExternalId,
                Model = packman.Model,
                Type = packman.Type,
                Version = packman.Version
            };
            LetterGenerator target = new LetterGenerator(package);
            target.RequestTemplatePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\Account.Tests\\request_template.xml");
            Dictionary<string, string> info = target.CreateInfo(); ; // TODO: инициализация подходящего значения
            
            
            OutMessage msg = target.ComposeMessage(info);
            int pos = msg.Subject.IndexOf(':');
            string outmsg = msg.Subject.Substring(pos + 1, 5);
            Assert.AreEqual("Test_", outmsg);

            string xml = Encoding.UTF8.GetString(msg.Attachments[0].Data);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            var node = doc.SelectSingleNode(@"RegInputDoc/docNum");

            Assert.AreEqual("Test_", node.FirstChild.Value.Substring(0,5));

        }
    }
}
