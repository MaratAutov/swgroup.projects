﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Account.Security;
using Account.DAO;
using Account.Controllers;
using System.Web;
using System.Web.Mvc;
using System.Web;
using System.IO;
using Account.Models;
using Account.Security;
using System.Data;
using ServiceStack.OrmLite;
using System.Xml;
using System.Reflection;

namespace Account.Tests
{
    [TestClass]
    public class AccountsDAOTest
    {
        public int GetUserId()
        {
            string userName = "k.mauglii@gmail.com";
            var user = UserService.Get(userName);
            return user.ID;
        }

        public static string NewsBody
        {
            get
            {
                return "TestNewService";
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            News news = NewsService.GetByBody(NewsBody);
            if (news == null)
            {
                NewsService.Add(GetUserId(), NewsBody);
            }
        }

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            UserAccount user = new UserAccount();
            using (var db = ORM.db)
            {
                user = db.Select<UserAccount>(
                    u => u.Login == "k.mauglii@gmail.com"
                ).SingleOrDefault();
            }

            if (user == null)
            {
                using (var db = ORM.db)
                {
                    db.Insert(new UserAccount()
                    {
                        Login = "k.mauglii@gmail.com",
                        Password = ORM.GetHash("111111"),
                        IsActive = true,
                        IsAdmin = true,
                        Company = "",
                        FIO = "k.mauglii@gmail.com",
                        Phone = "",
                        INN = "",
                        OGRN = "",
                        CreateDate = DateTime.Now
                    });
                }
            }

            using (var db = ORM.db)
            using (var trans = db.OpenTransaction())
            {
                db.Delete<News>(
                    n => n.Body == NewsBody
                );
                trans.Commit();
            }
        }       

        [TestMethod]
        public void TestNewServiceDelete()
        {
            NewsService.Delete(NewsService.GetByBody(NewsBody).ID);
            News news = NewsService.GetByBody(NewsBody);
            Assert.AreEqual(null, news, "Ошибка");
        }

        [TestMethod]
        public void TestNewServiceUpdate()
        {           
            NewsService.Update(NewsService.GetByBody(NewsBody).ID, GetUserId(), "TestNewService Update");
            NewsService.Update(NewsService.GetByBody("TestNewService Update").ID, GetUserId(), NewsBody);
            News news = NewsService.GetByBody(NewsBody);
            Assert.AreNotEqual(null, news.ID, "Ошибка");
        }

        [TestMethod]
        public void TestNewServiceGetAll()
        {
            var news = NewsService.GetAll(
                    DateTime.Now.Year.ToString(),
                    DateTime.Now.Month.ToString(),
                    DateTime.MinValue,
                    null,
                    "DatePosted",
                    "desc"
                );
            Assert.AreNotEqual(0, news.Count, "Ошибка");
        }

        [TestMethod]
        public void TestNewServiceGetTiming()
        {
            List<NewsTiming> list = NewsService.GetTiming();
            Assert.AreNotEqual(null, list.Count, "Ошибка");
        }

        [TestMethod]
        public void TestNewServiceGet()
        {            
            News news = NewsService.Get(NewsService.GetByBody(NewsBody).ID.ToString());
            Assert.AreNotEqual(null, news, "Ошибка");
        }

        [TestMethod]
        public void TestStatusService_GetStatusString()
        {
            string msg = StatusService.GetStatusString(0, false);
            
            Assert.AreEqual("Пакет принят на обработку", msg, "Ошибка");
        }

        [TestMethod]
        public void TestStatusService_Get()
        {
            Status status = StatusService.Get(1);

            Assert.AreNotEqual(null, status, "Ошибка");
        }




    }
}
